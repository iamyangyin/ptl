#ifndef KLPTMETHOD
#define KLPTMETHOD

#include "PTV.hpp"
#include "STB.hpp"
#include "ENS.hpp"

namespace KLPT{

    void IPR_OnResidualImage(OptMethod*, int, std::vector<Particle_ptr>&, int,
                            const std::vector<Camera_ptr>&, PTV*, OptMethod*);

    void IPR_OnResidualImage(OptMethod*, int, std::vector<Particle_ptr>&, int);

    inline void genericSearch(OptMethod* opt, int it_seq_cnt, const std::vector<Camera_ptr>& cam, PTV* ptv, OptMethod* opt_singlestep,
                                bool rm_outlier_flag, bool no_ipr_res_flag){
        
        opt->setkMax(opt->opt_max_iter());

        for(int k=1; k<opt->opt_max_iter()+1; k++){

            opt->core(it_seq_cnt, k, cam, rm_outlier_flag);

            if(!opt->ipr_full_flag()){
                cout<<"#####Triangulate particles on residual image"<<endl;
                if(no_ipr_res_flag)
                    IPR_OnResidualImage(opt, it_seq_cnt, opt->get_particles_field(), k);
                else
                    IPR_OnResidualImage(opt, it_seq_cnt, opt->get_particles_field(), k, cam, ptv, opt_singlestep);

                opt->postIPRResidual(it_seq_cnt, k, cam);
            }
            else{
                opt->postIPRFull(it_seq_cnt);
            }

            opt->printTrackInformation(it_seq_cnt);

            if(opt->checkConvergence(k, opt->opt_max_iter()))   break;
        }

        opt->assertVelocityQualityCheck(it_seq_cnt, opt->get_particles_field());
    }

    inline void runOptimization(OptMethod* opt, int it_seq_cnt, const std::vector<Camera_ptr>& cam, PTV* ptv, OptMethod* opt_singlestep,
                                bool rm_outlier_flag = false, bool no_ipr_res_flag = false){

        if(opt->warm_start_flag())
            opt->selectWarmStart(it_seq_cnt, cam);
        else
            opt->setlMax(0);

        if(opt->ipr_full_flag())
            genericSearch(opt, it_seq_cnt, cam, nullptr, nullptr, false, false);
        else
            genericSearch(opt, it_seq_cnt, cam, ptv, opt_singlestep, rm_outlier_flag, no_ipr_res_flag);
    }

    inline bool IPR_PTV_pipeline(PTV* ptv, int it_seq, const std::vector<Camera_ptr>& cam, bool reduced_flag, int iter,
                            bool dup_flag, bool save_part_flag=false, const std::string& bkgdir=""){

        bool do_flag=true;

        cout<<"#################### P"<<(reduced_flag? 3:4)<<" Re-triangulation iteration "<<iter<<"#################"<<endl;
        cout<<"Find peak..."<<endl;
        ptv->findParticleImagePeak();

        cout<<"Stereo Matching and triangulation..."<<endl;
        if(ptv->stereoMatching(cam, reduced_flag)){
            if(iter==1 && save_part_flag){
                cout<<"save Triangulated particle at the first iteration to file"<<endl;
                ptv->writeTriangulatedParticlesToFile(it_seq, bkgdir);
            }
            ptv->setTriangulatedParticleField(it_seq, dup_flag);
        }
        else
            do_flag=false;

        return do_flag;
    }

    inline void IPR_OPT_pipeline(PTV* ptv, OptMethod* opt_singlestep, int it_seq, bool do_flag, const std::vector<Camera_ptr>& cam,
                         int iter=0, bool set_int_flag=true, bool save_Ires_flag=false, const std::string& imgdir=""){

        if(do_flag){
            opt_singlestep->setn_particles(ptv->particles_bkg().size());

            cout<<"Initialize IPR"<<endl;
            opt_singlestep->initialization(ptv->get_particles_bkg(), it_seq);

            cout<<"Prepare IPR"<<endl;
            opt_singlestep->prepare(it_seq, cam);

            if(iter!=0 && save_Ires_flag){
                for(auto& ic: cam) ic->saveImageResidual(it_seq, "Ires0"+std::to_string(iter), imgdir);
            }

            cout<<"Run IPR"<<endl;
            runOptimization(opt_singlestep, it_seq, cam, nullptr, nullptr);

            if(iter!=0 && save_Ires_flag){
                for(auto& ic: cam) ic->saveImageResidual(it_seq, "Iresf"+std::to_string(iter), imgdir);
            }

            cout<<"Number of deleted particles in total: "<<Particle::num_deleted_tot_<<endl;

            cout<<"Set data for next Re-projection iteration"<<endl;
            ptv->setIresToIrec(cam);

            cout<<"Set particles fields built by IPR"<<endl;
            ptv->setIPRParticleField(opt_singlestep->get_particles_field());

            cout<<"Reconstruct "<<ptv->particles_bkg().size()<<" particles so far, containing deleted particles"<<endl;
        }

        cout<<"Prep for next Re-projection iteration"<<endl;
        opt_singlestep->clearParticleData();
        opt_singlestep->clear();
        ptv->clearForNextIteration();
        if(set_int_flag) ptv->setmin_intensity_threshold(Camera::max_intensity_value_);
    }

    /// Deal with real images read particles from Davis

    inline void IPR_OnResidualImage(OptMethod* opt, int it_seq_cnt, std::vector<Particle_ptr>& particles_tgt, int k){

        int n_new_particles_cnt=0;
        int n_append_particles_cnt=0;
        
        if(k==1){
            std::vector<Particle_ptr> particles_add;
            for(const auto& pt_new: opt->particles_new()){
                assert(pt_new->length()==2);
                assert(pt_new->it_seq_deb()==it_seq_cnt-2);
                assert(pt_new->it_seq_fin()==it_seq_cnt-1);
                       
                auto pc_kminus2 = pt_new->getPreviousOfPreviousXcoord(it_seq_cnt);
                auto pc_kminus1 = pt_new->getPreviousXcoord(it_seq_cnt);

                auto threshold = 0.2*cv::norm(opt->dXc_px());

                auto it = std::find_if( std::begin(particles_tgt), 
                                        std::end(particles_tgt), 
                                        [=](const auto& pt){    if(pt->is_ontrack() && pt->length()>=3){
                                                                    if(cv::norm(pt->getPreviousOfPreviousXcoord(it_seq_cnt) - pc_kminus2) < threshold)
                                                                        return cv::norm(pt->getPreviousXcoord(it_seq_cnt) - pc_kminus1) < threshold;
                                                                    else
                                                                        return false;
                                                                }
                                                                else
                                                                    return false;
                                                            } 
                                        );

                if(it==std::end(particles_tgt)){
                    particles_add.push_back( std::make_unique<Particle>( *pt_new ) );
                }
            }

            for(const auto& pt_add: particles_add){
                particles_tgt.push_back( std::make_unique<Particle>( *pt_add ) );
                particles_tgt.back()->checkIfParticleAppearsOnSnapshot(it_seq_cnt, opt->Xc_top(), opt->Xc_bottom());

                particles_tgt.back()->markAsToBeTriangulated();
                particles_tgt.back()->setis_to_be_tracked_flag_as(false);
                particles_tgt.back()->setis_tracked_flag_as(false);

                n_new_particles_cnt++;
            }

            //for Xcoord.size()==2
            opt->predictParticleUsingLinearPredictor(it_seq_cnt, particles_tgt);
        }

        opt->push_backn_append_particles(n_append_particles_cnt);
        opt->push_backn_new_particles(n_new_particles_cnt);

        if(k==opt->kMax()){
            cout<<"Delete particles found in previous snapshot but can not be appended at current snapshot!"<<endl;
            for(auto& pt: particles_tgt){
                if(pt->is_ontrack() && !pt->is_tracked() && pt->is_to_be_triangulated()){
                    pt->markAsGhost();
                    pt->deleteData(true);
                    pt->setit_seq_fin(it_seq_cnt-1);
                }
            }
            utils::showInfoOnDeletedParticles();
        }

        cout<<"Number of append particles:"; 
        for(auto n_n_p: opt->n_append_particles()) cout<<" "<<n_n_p;
        cout<<endl;
        cout<<"Number of new particles:   ";
        for(auto n_n_p: opt->n_new_particles())    cout<<" "<<n_n_p;
        cout<<endl;
    }

    /// Deal with real images

    inline void IPR_OnResidualImage(OptMethod* opt, int it_seq_cnt, std::vector<Particle_ptr>& particles_tgt, int k,
                                const std::vector<Camera_ptr>& cam, PTV* ptv, OptMethod* opt_singlestep){

        ptv->clearForNextIteration();
        ptv->clearParticleData();
        ptv->setIresToIrec(cam);

        if(k!=1){
            ptv->setmin_intensity_threshold(Camera::max_intensity_value_);
        }

        int k_addNew = opt->addNewParticles_before_final_iteration_flag() ? opt->kMax()-1 : opt->kMax()/2;

        auto do_flag = opt->use_stb_or_ens_in_lapiv_tr_flag() ? ( k_addNew==opt->kMax()/2 && k>3 ? (ptv->check_if_min_intensity_threshold_reaches_minimum(Camera::max_intensity_value_) ? false : true) 
                                                                                                 : true) 
                                                        : true;

        if(do_flag){
            auto reduced_flag = opt->ptv_stereoMatching_reduced_flag() && k>opt->kMax()/2;
            cout<<(reduced_flag ? "reduced" : "")<<endl;
            if(reduced_flag){
                ptv->setsearch_threshold(0.5);
                ptv->setWieneke_threshold(0.5);
            }

            auto dup_flag = opt->ptv_FrameField_duplicate_check_flag() || k!=1;
            auto set_int_flag = opt->ptv_min_intensity_threshold_flag() && k!=1;

            auto dor_flag = IPR_PTV_pipeline(ptv, it_seq_cnt, cam, reduced_flag, k, dup_flag);
            if(opt_singlestep!=nullptr){
                IPR_OPT_pipeline(ptv, opt_singlestep, it_seq_cnt, dor_flag, cam, k, set_int_flag);
            }

            cout<<"set triangulated particle field"<<endl
                <<"Before fill, spawned_particles_in_one_frame_.size() "<<opt->spawned_particles_in_one_frame_cloud()->size()<<endl;
            ptv->setFrameField(opt->spawned_particles_in_one_frame_cloud().get());
            cout<<"After  fill, spawned_particles_in_one_frame_.size() "<<opt->spawned_particles_in_one_frame_cloud()->size()<<endl;

            //for Xcoord.size()==2
            // if(k==1) opt->predictParticleUsingLinearPredictor(it_seq_cnt, particles_tgt);

            if(!opt->use_stb_or_ens_in_lapiv_tr_flag()){
                //for Xcoord.size()==1 and 2
                opt->predictParticlesUsingNeighboringInformation(it_seq_cnt, particles_tgt);
                opt->appendParticles(it_seq_cnt, particles_tgt);
                // opt->predictParticlesUsingNeighboringInformationLegacy(it_seq_cnt, particles_tgt);
                // opt->appendParticlesLegacy(it_seq_cnt, particles_tgt);
            }

            auto add_new_flag = opt->use_stb_or_ens_in_lapiv_tr_flag() ? 
                                ptv->check_if_min_intensity_threshold_reaches_minimum(Camera::max_intensity_value_) && k>2 :
                                k==k_addNew;

            if(add_new_flag) opt->addNewParticles(it_seq_cnt, particles_tgt);
            else             opt->push_backn_new_particles(0);
        }
        else{
            cout<<"Dummy pass !!!"<<endl;
            opt->push_backn_append_particles(0);
            opt->push_backn_new_particles(0);
        }

        if(k==opt->kMax()){
            cout<<"Delete particles found in previous snapshot but can not be appended at current snapshot!"<<endl;
            for(auto& pt: particles_tgt){
                if(pt->is_ontrack() && !pt->is_tracked() && pt->is_to_be_triangulated()){
                    pt->markAsGhost();
                    pt->deleteData(true);
                    pt->setit_seq_fin(it_seq_cnt-1);
                }
            }
            utils::showInfoOnDeletedParticles();
        }

        cout<<"Number of append particles:"; 
        for(auto n_n_p: opt->n_append_particles()) cout<<" "<<n_n_p;
        cout<<endl;
        cout<<"Number of new particles:   ";
        for(auto n_n_p: opt->n_new_particles())    cout<<" "<<n_n_p;
        cout<<endl;
    }
}
#endif