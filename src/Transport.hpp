#ifndef TRANSPORT
#define TRANSPORT

#include "Utils.hpp"

class Transport
{
    public:
        enum class TimeScheme_Type    				{Euler, Heun};
        enum class VelocityInterpolation_Type		{TriLinear, AGW, TriCubic};
        enum class ParticleSearch_Type				{box, radius, voxel};

	private:
		int n_particles_;

		bool 						stationary_flag_;
		VelocityInterpolation_Type	velocity_to_scatter_interp_type_;

		cv::Point3d 		 Xc_top_;
		cv::Point3d 		 Xc_bottom_;

		std::vector<double>  xgrid_;
		std::vector<double>  ygrid_;
		std::vector<double>  zgrid_;

		cv::Point3i			 nXc_;
		cv::Point3d          dXc_;

		TimeScheme_Type	     time_scheme_;
		double 				 dt_;
		double 				 dtDyn_;
		double 				 dtObs_;
		double 				 tDeb_;

		int 				 nVelocityFiles_;
		int 				 nSnapshots_;
		
		double 				 tTotal_;

		double 				 E_sd_;
		double 				 E_max_;
		double 				 E_min_;

		std::vector<cv::Point3d> 				grid_coords_;
		std::vector<cv::Point3d> 				velocity_fields_;  

		std::vector<MatrixXd> 	 U_ongrid3D_;
		std::vector<MatrixXd> 	 V_ongrid3D_;
		std::vector<MatrixXd> 	 W_ongrid3D_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dx_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dx_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dx_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dy_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dy_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dy_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dz_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dxdx_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdx_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdx_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dxdy_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdy_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdy_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dxdz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdz_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dydy_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dydy_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dydy_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dydz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dydz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dydz_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dzdz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dzdz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dzdz_;

		std::vector<MatrixXd> 	 dU_ongrid3D_dxdydz_;
		std::vector<MatrixXd> 	 dV_ongrid3D_dxdydz_;
		std::vector<MatrixXd> 	 dW_ongrid3D_dxdydz_;

		VelocityInterpolation_Type	velocity_warping_type_;

		double 				     r_sq_from_scattered_;

		cv::Point3d 	    	 velo_max_;
		cv::Point3d				 velo_min_;

		std::vector<cv::Point3d> 			particles_coords_;
        std::vector<Particle_ptr>  			particles_ref_;

		std::vector<double>  xgridc_;
		std::vector<double>  ygridc_;
		std::vector<double>  zgridc_;

		cv::Point3d          dXc_minimal_;

	    pcl::PointCloud<pcl::PointXYZL>::Ptr 					particles_cloud_ = pcl::make_shared<pcl::PointCloud<pcl::PointXYZL>>();
        pcl::octree::OctreePointCloudSearch<pcl::PointXYZL> 	particles_octree_ = pcl::octree::OctreePointCloudSearch<pcl::PointXYZL>(1.);

        double 					 search_radius_in_mm_=0.;

    public:
    	Transport();

    	~Transport();

    	Transport(bool, VelocityInterpolation_Type, const cv::Point3d&, const cv::Point3d&, const cv::Point3i&, TimeScheme_Type, double, double, double, double, int, double, double, double);
		
		Transport(bool, const cv::Point3d&, const cv::Point3d&, const cv::Point3i&);

		Transport(bool, VelocityInterpolation_Type, const cv::Point3d&, const cv::Point3d&, const cv::Point3i&, TimeScheme_Type, double, double, VelocityInterpolation_Type velocity_warping_type=Transport::VelocityInterpolation_Type::TriLinear);

		/// set initial particle position
		void setRandomParticles(int, const cv::Point3d&);

		/// generate synthetic particle tracks
		void predictSyntheticParticlePositionFieldFromScratch(const std::string&, int starting_index=0);
		
		/// generate background particle tracks
		void predictParticlePositionField(const std::vector<Particle_ptr>&, int, bool repredict_flag=false) const;

		void predictParticlePositionFieldWithKnownVelocity(const std::vector<Particle_ptr>&, const cv::Point3d&, int) const;
		
		void makeGrids(void);

		void setVelocityOnGrids(const std::vector<cv::Point3d>&);

		void getAvgVelocityFields(void);

		void setparticles_octree(const std::vector<Particle_ptr>&, int);

		std::vector<int> findParticlesInCurrentPatch(const cv::Point3d&, const ParticleSearch_Type& particle_search_type);

		void makeVelocityToVector(void);

		void saveReferenceDataNPZ(const std::string&);

		cv::Point3d integrateDtUsingEulerianVelocityFields(const cv::Point3d&) const;

		const int& nVelocityFiles(void) const { return nVelocityFiles_; }
		const std::vector<cv::Point3d>& velocity_fields(void) const { return velocity_fields_; }
		const std::vector<cv::Point3d>& grid_coords(void) const { return grid_coords_; }
		const cv::Point3i& nXc(void) const { return nXc_; }
		const cv::Point3d& dXc(void) const { return dXc_; }
		const cv::Point3d& dXc_minimal(void) const { return dXc_minimal_; }
		
		const std::vector<MatrixXd>& U_ongrid3D(void) const { return U_ongrid3D_; }
		const std::vector<MatrixXd>& V_ongrid3D(void) const { return V_ongrid3D_; }
		const std::vector<MatrixXd>& W_ongrid3D(void) const { return W_ongrid3D_; }

		const std::vector<double>& xgrid(void) const { return xgrid_; }
		const std::vector<double>& ygrid(void) const { return ygrid_; }
		const std::vector<double>& zgrid(void) const { return zgrid_; }

		const std::vector<double>& xgridc(void) const { return xgridc_; }
		const std::vector<double>& ygridc(void) const { return ygridc_; }
		const std::vector<double>& zgridc(void) const { return zgridc_; }

		const cv::Point3d velo_max(void) const { return velo_max_; }
		const cv::Point3d velo_min(void) const { return velo_min_; }

		void setvelocity_fields(const std::vector<cv::Point3d>& new_velocity_fields){ velocity_fields_=new_velocity_fields; }
		void setnXc(const cv::Point3i& nXc){ nXc_=nXc; };

		void makeBaseCoarseGrid(void);
		bool makeCoarseToFineGrid(double, int, std::vector<cv::Point3d>&);
		
		void applyMedianFilter(bool blur=false, bool medianBlur=false, bool grad_regul=false, bool GaussianBlur=true);

		void extractDivergenceFreeVelocityField(void);

		void readVelocityFieldsFromTXTFile(const std::string&, bool starting_flag=false);
		void readVelocityFieldsFromNPZFile(const std::string&, bool starting_flag=false);

		void saveVelocityFieldsToVTKFile(const std::string&) const;
		void saveVelocityFieldsToNPZFile(const std::string&) const;

		void setsearch_radius_in_mm(double search_radius_in_mm_arg){ 
			search_radius_in_mm_ = std::max( search_radius_in_mm_arg, dXc_.x/2. );
			cout<<"search_radius_in_mm_    "<<search_radius_in_mm_<<endl;
		}

	private:
		cv::Point3d interpolateVelocityFieldOnParticleCoordinates(const cv::Point3d&) const;

		cv::Point3d trilinearInterpolate(const cv::Point3d&) const;
		cv::Point3d AGWInterpolate(const cv::Point3d&) const;
		cv::Point3d tricubicInterpolate(const cv::Point3d&) const;

		void imposeBoundaryCondition(cv::Point3d&) const;

		cv::Point3d doEulerScheme(const cv::Point3d&) const;
		cv::Point3d doHeunScheme(const cv::Point3d&) const;

		double assignIntensity(double);

		void QualityCheck(std::vector<MatrixXd>&);

		void findNeighbors(const std::string&, double, int&, int&, double&, double&) const;

		double tricubicInterpolateScalar(const std::string&, const std::vector<cv::Point3d>&, const cv::Point3d&) const;

		void computeVelocityDerivatives(void);
};
#endif