#ifndef CAMERA
#define CAMERA

#include "Utils.hpp"

/// Camera class
class Camera
{
	friend class Calibration;
    friend class Dataset;
    friend class LAVISION;
    friend class LPTCHAL;

    public:
        enum class Model_Type                 {Pinhole, Polynomial, PinholeAndPolynomial};
        enum class Polynomial_Type            {ZPlane, SingleLegacy, Single};
        enum class psf_Type                   {Interpolation, ErrorFunction};
        enum class sumParticle_Type           {All, Detected};

    public:
        static psf_Type                 psf_type_;
        static std::vector<double>      xgrid_psf_, ygrid_psf_, zgrid_psf_;
        static cv::Point3d              dXc_psf_;
        static int                      max_intensity_value_;
        static int                      opencv_mat_type_;

	private:
		Model_Type  model_type_;
        int         n_cam_;
        int         i_cam_;
        int         n_width_;
        int         n_height_;
        int         n_width_mask_;
        int         n_height_mask_;
        double      PixelPerMmFactor_=1.;

        int         n_width_mask_ph_;
        int         n_height_mask_ph_;
        double      PixelPerMmFactor_ph_;

        ///Polynomial
        int                         z_order_;						    
        Polynomial_Type             polynomial_type_;

        int                         n_coef_;

        cv::Point3d                 normalizationFactor_=cv::Point3d(1,1,1);        			
        VectorXd                    a_;				
        VectorXd                    b_;

        ///number of Z plane
        int                         nZ_;                    
        ///position of Z plane in voxel unit and world frame
        std::vector<double>         ZPosZ_;                
        ///origin of world frame 
        std::vector<cv::Point2d>    originZ_;              
        std::vector<cv::Point2d>    normalizationFactorZ_;
        std::vector<VectorXd>       aZ_;
        std::vector<VectorXd>       bZ_;

        double 					f_;
        double					k_;
        double					l_;
        double 					beta_;
        double 					alpha_;
        double                  Z0_;

        double                  noise_ratio_=0;

        ///Pinhole
        cv::Mat 				cameraMatrix_;  
        cv::Mat 				distCoeffs_;
        std::vector<double>     ZViews_;
        std::vector<cv::Mat> 	rvecs_;
        std::vector<cv::Mat> 	tvecs_;
        cv::Mat 				R_;
        cv::Mat                 T_;

        std::vector<cv::Mat>    R_betw_cams_;
        std::vector<cv::Mat>    T_betw_cams_;
        std::vector<cv::Mat>    E_betw_cams_;
        std::vector<cv::Mat>    F_betw_cams_;

        cv::Point3i                       nXcPSF_;
        std::vector<Eigen::Vector4f>      theta_;

        cv::Mat                           Irec_;
        cv::Mat                           Iproj_;
        cv::Mat                           Ires_;
        cv::Point3d                       dXc_px_;
        
        int                      num_samples_tot_=0;
        cv::Point3d              deltaX_=cv::Point3d(0,0,1);              

	public:
		Camera();

		~Camera();

        Camera(Model_Type, int, int);

        Camera(Model_Type, int, int, int, int, int, double, double, double, double, double, const cv::Mat&, double noise_ratio=0);

        Camera(Model_Type, int, int, int, int, int, double, double);

        void                setPSFGrids(const cv::Point3d&, const cv::Point3d&);
        void                setPSFelement(const Eigen::Vector4f&, int);
        Eigen::Vector4f     getPSFelement(const cv::Point3d&);

		cv::Point2d         mappingFunction(const cv::Point3d&, bool verbal=false, bool force_pinhole_flag=false) const;

        void printMappingFunctionInfo(void);

		void checkSensitivityOfMappingFunction(const cv::Point3d&, double);

        void saveParamsToYamlFile(const std::string&, bool preproc_flag=false);

        void readParamsYaml(const std::string&, bool preproc_flag=false, bool pert_otf_flag=false);

		void readRecord(int, const std::string&, bool preproc_flag=true);

        void preprocessRecords(const std::vector<int>&, int, const std::string&, bool high_pass_flag=false, bool distort_original_image_flag=false,
                                int subtract_sliding_minimum_size=5, int subtract_constant_pixel_value=10, int multiply_pixel_factor=1.,
                                bool Gaussian_smothing_flag=false, bool Sharpending_flag=false,
                                bool mask_flag=false, const cv::Point3d& Xc_top=cv::Point3d(0,0,0), const cv::Point3d& Xc_bottom=cv::Point3d(0,0,0));

		double computeGlobalResidual(int);

		void computeImageResidual(int, bool ipr_flag=false);

		void saveImageResidual(int, const std::string&, const std::string&);

		void saveImageProjection(int, const std::string&, const std::string&, bool write_bkg_flag=false);

        cv::Mat sumParticleImage(const std::vector<Particle_ptr>&, sumParticle_Type sumParticle_type, int it_seq_cnt=0);

		const Model_Type& model_type(void) const { return model_type_; }
		const int& n_cam(void) const { return n_cam_; }
		const int& i_cam(void) const { return i_cam_; }
		const int& n_width(void) const { return n_width_; }
		const int& n_height(void) const { return n_height_; }
   
        const Polynomial_Type& polynomial_type(void) const { return polynomial_type_; }

        const double& PixelPerMmFactor(void) const { return PixelPerMmFactor_; }

        const cv::Point3d& normalizationFactor(void) const { return normalizationFactor_; }
        const VectorXd& a(void) const { return a_; }
        const VectorXd& b(void) const { return b_; }

        const std::vector<double>& ZPosZ(void) const { return ZPosZ_; }         
        const std::vector<cv::Point2d>& originZ(void) const { return originZ_; }
        const std::vector<cv::Point2d>& normalizationFactorZ(void) const { return normalizationFactorZ_; }
        const std::vector<VectorXd>& aZ(void) const { return aZ_; }
        const std::vector<VectorXd>& bZ(void) const { return bZ_; }
        
		const cv::Mat& cameraMatrix(void) const { return cameraMatrix_; }
		const cv::Mat& distCoeffs(void) const { return distCoeffs_; }
        const std::vector<cv::Mat>& rvecs(void) const {	return rvecs_; }
        const std::vector<cv::Mat>& tvecs(void) const {	return tvecs_; }
        
        const std::vector<double>& ZViews(void) const { return ZViews_; } 

        const cv::Mat& 				R(void) const { return R_; }
        const cv::Mat&              T(void) const { return T_; }

        const std::vector<cv::Mat>& R_betw_cams(void) const { return R_betw_cams_; }
        const std::vector<cv::Mat>& T_betw_cams(void) const { return T_betw_cams_; }
        const std::vector<cv::Mat>& E_betw_cams(void) const { return E_betw_cams_; }
        const std::vector<cv::Mat>& F_betw_cams(void) const { return F_betw_cams_; }

        const cv::Point3i& nXcPSF(void) const { return nXcPSF_; }
		const std::vector<Eigen::Vector4f>& PSF(void) const { return theta_; }
		const int& z_order(void) const { return z_order_; }
		const int& n_coef(void) const { return n_coef_; }
		const cv::Mat& Irec(void) const { return Irec_; }
		const cv::Mat& Iproj(void) const { return Iproj_; }
		const cv::Mat& Ires(void) const { return Ires_; }
		const cv::Point3d& dXc_px(void) const { return dXc_px_; }
        const cv::Point3d& deltaX(void) const { return deltaX_; }

        void setIprojElement(const cv::Mat& Iproj_t){ Iproj_t.copyTo(Iproj_); };

        void setPSF(const std::vector<Eigen::Vector4f>& theta){ 
            if(!theta_.empty()) theta_.clear();
            theta_=theta; 
        };

        void clearIproj(void){ Iproj_.release(); }; 
        void clearIres(void){ Ires_.release(); };

        void setIrec(const cv::Mat& img){ Irec_.release(); Irec_ = img.clone(); }
        void setIresToIrec(void){ Irec_ = Ires_.clone(); }

        void clearRTEF(void){
            if(!R_betw_cams_.empty()) R_betw_cams_.clear();
            if(!T_betw_cams_.empty()) T_betw_cams_.clear();
            if(!E_betw_cams_.empty()) E_betw_cams_.clear();
            if(!F_betw_cams_.empty()) F_betw_cams_.clear();
        }

    private:
        void readParamsPolynomial(cv::FileStorage&);
        void readParamsPinhole(cv::FileStorage&);
        void readParamsStereo(cv::FileStorage&);
        void readParamsPSF(cv::FileStorage&, bool pert_otf_flag=false);
        void savePolynomialParamsToFile(cv::FileStorage&);
        void savePinholeParamsToFile(cv::FileStorage&);
        void saveStereoParamsToFile(cv::FileStorage&);
        void savePSFParamsToFile(cv::FileStorage&);
};

typedef std::unique_ptr<Camera> Camera_ptr;
#endif