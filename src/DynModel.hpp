#ifndef DYNMODEL
#define DYNMODEL

#include "Transport.hpp"

///DynModel class
class DynModel
{
    public:
        enum class DynModel_Type       {LinearPolynomial, WienerFilter, TransportModel, LCSTracker};

	private:
        DynModel_Type               dynmodel_type_;
        std::unique_ptr<Transport>  transportRecon_;

	public:
    	DynModel();

    	~DynModel();

    	DynModel(DynModel_Type);

        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, int);
        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, const std::vector<cv::Point3d>&, int, bool repredict_flag);
        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, const cv::Point3d&, int);
        void predictNextPaticlesPosition(const std::vector<Particle_ptr>&, int, bool repredict_flag);

        void settransportRecon(bool stationary_flag, const cv::Point3d& Xc_top_, const cv::Point3d& Xc_bottom_, const cv::Point3i& nXc_data){
                         transportRecon_ = std::make_unique<Transport>(stationary_flag, Xc_top_, Xc_bottom_, nXc_data); }
                         
        void settransportRecon(bool stationary_flag, Transport::VelocityInterpolation_Type velocity_to_scatter_interp_type, const cv::Point3d& Xc_top_, const cv::Point3d& Xc_bottom_, const cv::Point3i& nXc_recon, Transport::TimeScheme_Type time_scheme, double dt, double dtObs, Transport::VelocityInterpolation_Type velocity_warping_type){
                         transportRecon_ = std::make_unique<Transport>(stationary_flag, velocity_to_scatter_interp_type, Xc_top_, Xc_bottom_, nXc_recon, time_scheme, dt, dtObs, velocity_warping_type); }

        const std::unique_ptr<Transport>& transportRecon(void) const { return transportRecon_; }

    private:
        void LinearPolynomialPredictorForSingleParticle(Particle*, int);
        void WienerFilterPredictorForSingleParticle(Particle*, int);
};
#endif