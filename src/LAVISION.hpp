#ifndef LAVISION_H
#define LAVISION_H

#include "Dataset.hpp"

class LAVISION: public Dataset
{
	public:
		LAVISION();

		~LAVISION();

		void setIMGData(const std::string&, int, int, const std::vector<Camera_ptr>&, const std::string&) final;

		void readOTF(const std::string&, Camera*) final;
		void readPolynomialMF(const std::string&, Camera*) final;
		void readPinholeMF(const std::string&, Camera*) final;

		void readMarkPositionTable(const std::string&, int,
								std::vector<std::vector<std::vector<cv::Point3d>>>&,
								std::vector<std::vector<std::vector<cv::Point2d>>>&, int, int) final;

		void setRigIMGData(const std::string&, int, const std::string&) final;

	private:
		void readIMGData(const std::string&, int, int, const std::string&);
		void readRigIMGData(const std::string&, int, const std::string&);
		void writeIMGTxtDataToPng(int, int, const std::vector<Camera_ptr>&, const std::string&);
	 	void writeRigIMGTxtDataToPng(int, const std::string&); 
};
#endif