#ifndef SYNTHETIC_H
#define SYNTHETIC_H

#include "Dataset.hpp"

class SYNTHETIC: public Dataset
{
	private:

	public:
		SYNTHETIC();

		~SYNTHETIC();

		void setIMGData(const std::string&, int, int, const std::vector<Camera_ptr>&, const std::string&) final;

		void readMarkPositionTable(const std::string&, int,
								std::vector<std::vector<std::vector<cv::Point3d>>>&,
								std::vector<std::vector<std::vector<cv::Point2d>>>&, int, int) final;
};
#endif