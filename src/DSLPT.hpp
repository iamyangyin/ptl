#ifndef DSLPTMETHOD
#define DSLPTMETHOD

#include "PTV.hpp"
#include "Dataset.hpp"
#include "Evaluation.hpp"

typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RowMatrixXf;
using namespace torch::indexing;

class DSLPT : public PTV
{
    public:
        enum class Stereo_Matching_Type    {BM_cpu, SGBM_cpu, BM_cuda, ConstantSpaceBP_cuda};
        enum class Stereo_Cost_Type        {Grey, Census, DeepFeature};

    private:
        Stereo_Matching_Type    stereo_matching_type_;
        int                     disparity_min_ = 0;
        int                     disparity_max_;
        int                     BM_win_size_;

        Stereo_Cost_Type        stereo_cost_type_;

        cv::Mat                 homo1_;
        cv::Mat                 homo2_; 

        cv::Mat                 rect1_;
        cv::Mat                 rect2_; 

        int                     half_block_size_;

        std::vector<std::vector<std::vector<float>>> cost_volume_;

    public:
        DSLPT();

        ~DSLPT();

        DSLPT(SubPixelMethod_Type, double, double, 
            double, double, double,
            double, double, bool, double,
            const cv::Point3d&, const cv::Point3d&, 
            const cv::Point3d&, const std::vector<int>&, 
            Stereo_Matching_Type, int, int, Stereo_Cost_Type);

        void doTriangulationUsingTwoPoints(const std::vector<Camera_ptr>&,
                                           const std::vector<cv::Point2d>&,
                                           const std::vector<cv::Point2d>&,
                                           std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&);

        void doCorrespondentSearchOn3rdAnd4thCameras(const pcl::PointCloud<pcl::PointXYZI>&,
                                                const std::vector<Camera_ptr>&,
                                                std::vector<std::vector<cv::Point2d>>&, 
                                                std::vector<std::vector<cv::Point2d>>&);

        void doTriangulationUsingLeastSquare(int, const std::vector<Camera_ptr>&,
                                            const std::vector<cv::Point2d>&, 
                                            const std::vector<cv::Point2d>&,
                                            const std::vector<std::vector<cv::Point2d>>&,
                                            const std::vector<std::vector<cv::Point2d>>&,
                                            std::vector<Particle_ptr>&,
                                            std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>&,
                                            bool init_flag=false);

        bool applyTracker(const cv::Rect&,
                            const cv::Point2d&,
                            const cv::Mat&,
                            const cv::Mat&,
                            cv::Point2d&,
                            cv::Rect&,
                            const cv::Point2f& offset=cv::Point2d(0,0));

        cv::Mat doDisparityComputationUsingCuda(const cv::Mat&, const cv::Mat&);
        cv::Mat doDisparityComputationUsingCPU(const cv::Mat&, const cv::Mat&);

        void getHorizontalCameraPairHomographyMatrix(const std::vector<std::vector<cv::Point2f>>&,
                                                    const std::vector<std::vector<cv::Point2f>>&,
                                                    int, const std::vector<Camera_ptr>&);

        void getImagePeaks(int, int, const std::vector<Camera_ptr>&, const cv::Mat&, const cv::Mat&);

        cv::Mat computeDisparity(const std::vector<Camera_ptr>&, cv::Mat&, cv::Mat&,
                             bool visu_flag=false, bool save_flag=false, const std::string& imgdir="", int it_seq=0);

        void searchCorrespondantOn3rdAnd4thCameras(int, const std::vector<Camera_ptr>&,
                                                    const std::vector<cv::Point2d>&, const std::vector<cv::Point2d>&,
                                                    std::vector<Particle_ptr>&,
                                                    bool init_flag=false, bool eval_flag=false, 
                                                    const std::vector<Particle_ptr>& particles_bkg=std::vector<Particle_ptr>());

        cv::Mat doDisparityComputationUsingSGBM(const cv::Mat&, const cv::Mat&);
        cv::Mat doDisparityComputationUsingBM(const cv::Mat&, const cv::Mat&);
        void fillCostVolume(const cv::Mat&, const cv::Mat&);
        void winnerTakeAll(const cv::Mat&, cv::Mat&);
        float computeCostAtExactPixelAndRange(uint32*, uint32*, int, int, int, int);
        float computeCostAtExactPixelAndRange(const cv::Mat&, const cv::Mat&, int, int, int);
        // void getHorizontalCameraPairHomographyMatrix(const std::vector<Camera_ptr>&, int, double);
        // void getHorizontalCameraPairHomographyMatrix(const std::vector<Camera_ptr>&);
        // void initUndistortRectifyMapUsingRectifiedMatrix(const std::vector<Camera_ptr>&,
        //                                             cv::Mat&, cv::Mat&, cv::Mat&, cv::Mat&);
};
#endif