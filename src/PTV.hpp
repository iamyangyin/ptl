#ifndef PTVMODULE
#define PTVMODULE

#include "Camera.hpp"
#include "stdafx.h"
#include "Triangulation.hpp"
#include "Track.hpp"

/// PTV class
class PTV
{
    public:   
        /// Enum parameters
        enum class SubPixelMethod_Type        {WeightedAverage, Gaussian}; // 0: Use WeightedAverage Method, 1: Use Gaussian Method

    public:
        /// Parameters
        SubPixelMethod_Type             subPixel_type_;
        double                          initial_min_intensity_threshold_;
        double                          filter_threshold_;
        double                          dilate_threshold_;
        double                          dilate_surrounding_checker_threshold_;
        double                          dilate_surrounding_difference_threshold_ ;

        double                          search_threshold_;
        double                          Wieneke_threshold_;     

        bool                            verbal_flag_=false;

        double                          min_intensity_ratio_;
        double                          min_intensity_threshold_ ;

        double                          triangulation_tolerance_ratio_=0.00001;
        double                          triangulation_tolerance_at_bounadary_;

        double                          duplicate_ratio_doLeastSquareTriangulation_ = 1.;
        double                          duplicate_ratio_setTriangulatedParticleField_ = 1.;
        double                          duplicate_ratio_setFrameField_ = 1.;

        cv::Point3d                     Xc_top_;
        cv::Point3d                     Xc_bottom_;
        cv::Point3d                     dXc_px_; 
        cv::Point3d                     bad_point_;

        int                             idx1_;
        int                             idx2_;
        int                             idx3_;
        int                             idx4_;

        bool                            iterative_triangulation_flag_=false;

        bool                            lsq_triangulation_flag_=true;

        bool                            nonlinear_triangulation_flag_=false;
        bool                            nonlinear_lsq_triangulation_flag_=false;

    private:        
        std::vector<cv::Mat>                                    Irec_;
        std::vector<cv::Mat_<double>>                           Irec_float_;

        std::vector<pcl::PointCloud<pcl::PointXY>::Ptr>         PeakValues_;
        
#ifdef INTELTBB
        tbb::concurrent_vector<pcl::PointXYZI>                  CorrespondentWorldPoints_;
        std::vector<tbb::concurrent_vector<cv::Point2d>>        CorrespondentImagePoints_;
#else
        std::vector<pcl::PointXYZI>                             CorrespondentWorldPoints_;
        std::vector<std::vector<cv::Point2d>>                   CorrespondentImagePoints_;
#endif

        double                                          ImageHeight_;
        double                                          ImageWidth_; 
        std::vector<cv::Mat>                            Irec_pyrDown_;

        std::vector<Particle_ptr>                       particles_bkg_;

        std::vector<std::vector<cv::Point2d>>           Points_xy_;
        std::vector<pcl::PointCloud<pcl::PointXY>::Ptr> Points_xy_cloud_;
        std::vector<pcl::KdTreeFLANN<pcl::PointXY>>     Points_xy_kdtree_;

    public:
        PTV();

        PTV(SubPixelMethod_Type, double, double, 
            double, double, double,
            double, double, bool, double,
            const cv::Point3d&, const cv::Point3d&, 
            const cv::Point3d& dXc_px=cv::Point3d(0,0,0), const std::vector<int>& cam_seq=std::vector<int>{0,1,2,3}, 
            bool iterative_triangulation_flag=false, bool lsq_triangulation_flag=true);

        ~PTV();

        void readRecordsFromCamera(const std::vector<Camera_ptr>&);

        void getImagePyramids(void);

        void getMaxPixelCoords(void);

        void getSubPixelCoords(double duplicate_threshold_px);

        void findParticleImagePeak(double duplicate_threshold_px=0.01);

        bool stereoMatching(const std::vector<Camera_ptr>&, bool reduced_IPR_flag=false, bool pos_only_flag=false);
        
        void writeTriangulatedParticlesToFile(int, const std::string&);

        void writeIPRParticlesToFile(int, const std::string&, const std::string&);

        const std::vector<Particle_ptr>& particles_bkg(void) const { return particles_bkg_; }

        std::vector<Particle_ptr>& get_particles_bkg(void) { return particles_bkg_; }

        const std::vector<std::vector<cv::Point2d>>& Points_xy(void) const { return Points_xy_; }

        double calibrateSingleParticleIntensity(const std::vector<Camera_ptr>&, const cv::Point3d&, const cv::Point2d&, const cv::Point2d&,
                                                                                            const cv::Point2d&, const cv::Point2d&);

        void setFrameField(pcl::PointCloud<pcl::PointXYZI>*);

        void setTriangulatedParticleField(int, bool duplicate_check_flag=false, bool reduced_flag=false);

        void setIPRParticleField(std::vector<Particle_ptr>& particles_IPR_field){
            particles_bkg_ = std::move( particles_IPR_field );
        }

        void clearParticleData(void){
            std::vector<Particle_ptr>().swap(particles_bkg_);
        }

        void clearForNextIteration(void){
            PeakValues_.clear();
            std::vector<std::vector<cv::Point2d>>().swap(Points_xy_);
            Points_xy_cloud_.clear();
            Points_xy_kdtree_.clear();
#ifdef INTELTBB
            tbb::concurrent_vector<pcl::PointXYZI>().swap(CorrespondentWorldPoints_);
            std::vector<tbb::concurrent_vector<cv::Point2d>>().swap(CorrespondentImagePoints_);
#else
            std::vector<pcl::PointXYZI>().swap(CorrespondentWorldPoints_);
            std::vector<std::vector<cv::Point2d>>().swap(CorrespondentImagePoints_);
#endif
        }

        void clearDataForNextTimeLevel(void){
            this->clearParticleData();
            this->clearForNextIteration();
            if(!Irec_.empty()) Irec_.clear();
            if(!Irec_float_.empty()) Irec_float_.clear();
            if(!Irec_pyrDown_.empty()) Irec_pyrDown_.clear();
            this->resetmin_intensity_threshold();
        }

        void setIresToIrec(const std::vector<Camera_ptr>& cam){
            if(!Irec_.empty())       Irec_.clear();
            if(!Irec_float_.empty()) Irec_float_.clear();

            ImageHeight_ = cam[0]->Irec().size().height;
            ImageWidth_  = cam[0]->Irec().size().width;

            for(const auto& ic: cam){
                auto Ires = ic->Ires().clone();
                Irec_.push_back( Ires );

                cv::Mat_<double> image_float;
                Ires.convertTo(image_float, CV_64FC1);

                Irec_float_.push_back(image_float);
            }
        }

        void resetmin_intensity_threshold(void){ min_intensity_threshold_=initial_min_intensity_threshold_; } 

        void setmin_intensity_threshold(double max_intensity, double ratio=0.5){ 
            min_intensity_threshold_*= ratio; 
            if(min_intensity_threshold_<min_intensity_ratio_*max_intensity)
                min_intensity_threshold_=min_intensity_ratio_*max_intensity;
        }

        bool check_if_min_intensity_threshold_reaches_minimum(double max_intensity){ return min_intensity_threshold_<=min_intensity_ratio_*max_intensity; }

        void setinitial_min_intensity_threshold(double initial_min_intensity_threshold){ initial_min_intensity_threshold_=initial_min_intensity_threshold; } 

        void setWieneke_threshold(double Wieneke_threshold){ Wieneke_threshold_=Wieneke_threshold; }
        void setsearch_threshold(double search_threshold){ search_threshold_=search_threshold; }
    
        void setiterative_triangulation_flag(bool iterative_triangulation_flag){ iterative_triangulation_flag_=iterative_triangulation_flag; }
        void setlsq_triangulation_flag(bool lsq_triangulation_flag){ lsq_triangulation_flag_=lsq_triangulation_flag; }

        const int& idx1(void) const { return idx1_; }
        const int& idx2(void) const { return idx2_; }
        const int& idx3(void) const { return idx3_; }
        const int& idx4(void) const { return idx4_; }

#ifdef INTELTBB
        const tbb::concurrent_vector<pcl::PointXYZI>&           CorrespondentWorldPoints(void) const { return CorrespondentWorldPoints_; }
        const std::vector<tbb::concurrent_vector<cv::Point2d>>& CorrespondentImagePoints(void) const { return CorrespondentImagePoints_; }
#else
        const std::vector<pcl::PointXYZI>&                      CorrespondentWorldPoints(void) const { return CorrespondentWorldPoints_; }
        const std::vector<std::vector<cv::Point2d>>&            CorrespondentImagePoints(void) const { return CorrespondentImagePoints_; }
#endif

        std::vector<cv::Point2d> CorrespondentSearcher(int, const cv::Point2d&);

    private:         
        bool findPointsOnEpipolarWithoutDrawingLine(Camera*, Camera*, const cv::Point2d&, const std::vector<cv::Point2d>&, std::vector<cv::Point2d>&,
                                                    const std::vector<cv::Point2d>&, std::vector<cv::Point2d>&);

        bool WienekeMethod(const std::vector<cv::Point3d>&, const std::vector<cv::Point2d>&, const std::vector<Camera_ptr>&, int, int, bool,
                            std::vector<cv::Point2d>&, std::vector<std::vector<cv::Point2d>>&, std::vector<std::vector<cv::Point2d>>&, std::vector<cv::Point3d>&, const std::vector<cv::Point2d>&);

        bool doTwoPointsTriangulation(const std::vector<Camera_ptr>&, const cv::Point2d&, const std::vector<cv::Point2d>&, int, int, std::vector<cv::Point3d>&, std::vector<cv::Point2d>&,
                                        const cv::Point2d&, const std::vector<cv::Point2d>&, std::vector<cv::Point2d>&);

        bool doLeastSquareTriangulation(const std::vector<Camera_ptr>&, int, int, int, int,
                                        const cv::Point2d&, 
                                        const std::vector<cv::Point2d>&,
                                        const std::vector<std::vector<cv::Point2d>>&,
                                        const std::vector<std::vector<cv::Point2d>>&,
                                        const cv::Point2d&,
                                        const std::vector<cv::Point3d>&,
                                        std::vector<std::vector<cv::Point3d>>&);

        void useDilate(void);
        void useFilter(void);
        
        void packResultsForCurrentParticle(const cv::Point2d&,
                                            const std::vector<cv::Point2d>&,
                                            const std::vector<std::vector<cv::Point2d>>&,
                                            const std::vector<std::vector<cv::Point2d>>&,
                                            const std::vector<std::vector<cv::Point3d>>&);

        void packResultsForCurrentParticle(const cv::Point2d&,
                                            const std::vector<cv::Point2d>&,
                                            const std::vector<std::vector<cv::Point2d>>&,
                                            const std::vector<std::vector<cv::Point2d>>&,
                                            const std::vector<cv::Point3d>&);

        std::vector<Particle_ptr> 
        addParticlesFromPoolOctree(int, const std::vector<pcl::PointXYZI>&, 
                                    double resolution=5.);

        pcl::PointCloud<pcl::PointXYZI>
        addParticlesFromPoolOctree(const pcl::PointCloud<pcl::PointXYZI>&, double resolution=5.);

        std::vector<pcl::PointXYZI> removeDuplicates(void);
};
#endif