#ifndef CALIBRATION
#define CALIBRATION

#include "Dataset.hpp"

/// Calibration class
class Calibration
{
    public:
    	/// Calibration source 
    	enum class Source_Type	   {FromVirtualCamera, FromLaVision, FromLPTChallenge};
    	enum class View_mode_Type  {ZVary, ZZero};
    	/// 0: Use hacker, 1: 1x1, 2: 5x5x2
        enum class OTFCalib_Type   {Hacker, Constant, Variate}; 

	private:
		Source_Type			 source_;
		View_mode_Type		 view_mode_=View_mode_Type::ZVary;
		int 				 n_cam_;

		/// The limit of domain of interest in physical unit
		cv::Point3d 		 Xc_top_;
		cv::Point3d 		 Xc_bottom_;

		/// Number of template points on rig
		cv::Point2i			 nXc_rig_;

		/// Number of views of rig
		int 				 nViews_;

		std::vector<double>  ZViews_;

		bool 				 Right_handed_flag_;

		/// Interval between two template points on rig
		cv::Point3d 		 dXc_rig_;
		
		/// Vector of vector (Z, XY) storing the coordinate of template points in double
    	std::vector<std::vector<cv::Point3d>> 				Xcoord_obj_;

		/// Vector of vector of vector (Z, XY) storing the coordinate of template points in double
    	std::vector<std::vector<std::vector<cv::Point3d>>> 	Xcoord_obj_cams_;

		/// Vector of vector of vector (n_cam, Z, XY) storing the coordinate of template points in double
    	std::vector<std::vector<std::vector<cv::Point2d>>>  xcoord_img_;
	
	public:
		Calibration();

		~Calibration();

		Calibration(Source_Type, int, const cv::Point3d&, const cv::Point3d&, const cv::Point2i& nXc_rig=cv::Point2i(0,0), int nViews=0, bool Right_handed_flag=true);
		
		const int nViews(void) const { return nViews_; }

		std::vector<std::vector<std::vector<cv::Point3d>>>& get_Xcoord_obj_cams(void) { return Xcoord_obj_cams_; }
		std::vector<std::vector<std::vector<cv::Point2d>>>& get_xcoord_img(void) { return xcoord_img_; }
		
		void setXcoord_obj_cams(const std::vector<std::vector<std::vector<cv::Point3d>>>& Xcoord_obj_cams){ Xcoord_obj_cams_=Xcoord_obj_cams; }
		void setxcoord_img(const std::vector<std::vector<std::vector<cv::Point2d>>>& xcoord_img){ xcoord_img_=xcoord_img; }

		void clearXcoord_obj_cams(void){ if(!Xcoord_obj_cams_.empty()) Xcoord_obj_cams_.clear(); }
		void clearxcoord_img(void)     { if(!xcoord_img_.empty())      xcoord_img_.clear(); }
		
		void setnViews(int nViews){ 
			nViews_=nViews; 	
			cout<<"calib->nViews_ "<<nViews_<<endl;
		}

		void setZViews(const std::vector<double>& ZViews){ 
			ZViews_=ZViews; 
			this->setnViews(ZViews_.size());
		}

		void generate3DGridsInObjectSpace(void);
		void generate3DPlanerPointsInObjectSpace(void);

		void generate2DGridsInImageSpaceFromKnownCamera(Camera*, int view_zero=0);
		void generate2DGridsInImageSpaceFromCalibratedCamera(const Camera&, bool distort_flag=true);
		void generate2DGridsInImageSpaceFromKnownPolynomial(Camera*);

        void saveMarkPositionTable(const std::string&);

		void computeMappingFunctionError(const Camera&, bool pinhole_flag=false);

		void setPSFParams(Camera*, double);

		void readPSFParams(const std::string&, Camera*, Dataset*);

		void getPolynomialModelParameters(Camera*, double PixelPerMmFactor=1., const cv::Point3d& normalizationFactor=cv::Point3d(1,1,1));
		void getPolynomialModelParameters(const std::string&, Camera*, Dataset*);

		void getPinholeModelParameters(Camera*, int opencv_calib_flag, int view_zero=0, double f=0, double k=0);
		void getPinholeModelParameters(const std::string&, Camera*, Dataset*);

		void getStereoCalibrationParameters(Camera*, Camera*);

		void checkDomainBoundaryOnImageSpace(const Camera&, const cv::Point3d&, const cv::Point3d&);

		void testSamplePoints(Camera*, int view_zero, bool zer_Z_flag=true, bool verbal=false);

		void computeEssentialMatrix(const cv::Mat&, const cv::Mat&, const cv::Mat&, const cv::Mat&, cv::Mat&);

		void collectSamplePointsForVSC(int, Camera*, const std::vector<cv::Point2d>&, const std::vector<Particle_ptr>&,
	                                    double, std::vector<cv::Point3d>&, std::vector<cv::Point2d>&);
	    void volumeSelfCalibration(Camera*, const std::vector<cv::Point3d>&, const std::vector<cv::Point2d>&, double, const cv::Point3d&);

		std::vector<std::vector<int>> getIdxInEachSubBlock(const std::vector<cv::Point3d>&);

		void collectSamplePointsForOTF(
#ifdef INTELTBB
        const tbb::concurrent_vector<pcl::PointXYZI>&,
        const std::vector<tbb::concurrent_vector<cv::Point2d>>&,
#else
        const std::vector<pcl::PointXYZI>&,
        const std::vector<std::vector<cv::Point2d>>&,
#endif
        std::vector<cv::Point3d>&,
        std::vector<std::vector<cv::Point2d>>&);

		void calibrateOTF(Camera*, const OTFCalib_Type&, const std::vector<cv::Point3d>&, const std::vector<cv::Point2d>&);
		void testOTF(const std::vector<Camera_ptr>&, int, const std::vector<int>&, const std::vector<cv::Point3d>&, const std::vector<std::vector<cv::Point2d>>&, const cv::Point3d&);

		void getFundamentalMatrix(int, Camera*, Camera*, const std::vector<Particle_ptr>&);
		void computeFundamentalMatrixError(int, Camera*, Camera*, const std::vector<Particle_ptr>&, std::vector<std::vector<double>>&);

	private:
		std::vector<cv::Mat_<double>> projectFromWorldFrameToCameraFrame(const std::vector<cv::Mat_<double>>&, const cv::Mat&, const cv::Mat&);
		std::vector<cv::Mat_<double>> projectFromCameraFrameToNormalizedImageFrame(const std::vector<cv::Mat_<double>>&);
		std::vector<cv::Mat_<double>> distortPoints(const std::vector<cv::Mat_<double>>&, const cv::Mat&);
		std::vector<cv::Mat_<double>> projectFramNormalizedImageFrameToFocalImageFrame(const std::vector<cv::Mat_<double>>&, const cv::Mat&);

		double calibrateOTFIntensitySingleZoneLMSolver(const Eigen::Vector4f&, const Camera&, const std::vector<int>&, const std::vector<cv::Point2d>&);
		Eigen::Vector4f calibrateOTFRadiusSingleZoneLMSolver(const Eigen::Vector4f&, const Camera&, const std::vector<int>&, const std::vector<cv::Point2d>&);

		int opencv_calib_flag_;
};

///change obj coordinates from double to float as some opencv functions only take float input

inline std::vector<std::vector<cv::Point3f>> changeXcoordDoubleToFloat(const std::vector<std::vector<cv::Point3d>>& Xcoord_objd, bool zero_Z_flag=true){

    std::vector<std::vector<cv::Point3f>>               Xcoord_objf;

    for(int z=0; z<Xcoord_objd.size(); z++){
        std::vector<cv::Point3f>  Xcoord_grid_v;
        for(auto& Xc: Xcoord_objd.at(z)){
            if(zero_Z_flag)
                Xcoord_grid_v.push_back( cv::Point3f( static_cast<float>( Xc.x ),
                                                      static_cast<float>( Xc.y ),
                                                      0. ) );
            else
                Xcoord_grid_v.push_back( cv::Point3f( static_cast<float>( Xc.x ),
                                                      static_cast<float>( Xc.y ),
                                                      static_cast<float>( Xc.z ) ) );
        }
        Xcoord_objf.push_back( Xcoord_grid_v );
    }
    
    return Xcoord_objf;
}

///save as above

inline std::vector<std::vector<cv::Point2f>> changexcoordDoubleToFloat(const std::vector<std::vector<cv::Point2d>>& xcoord_imgd_icam){

    std::vector<std::vector<cv::Point2f>>               xcoord_imgf_icam;

    for(int z=0; z<xcoord_imgd_icam.size(); z++){
        std::vector<cv::Point2f>  xcoord_grid_v;
        for(auto& xc: xcoord_imgd_icam.at(z)){
            xcoord_grid_v.push_back( cv::Point2f( static_cast<float>( xc.x ),
                                                  static_cast<float>( xc.y ) ) );
        }
        xcoord_imgf_icam.push_back( xcoord_grid_v );
    }

    return xcoord_imgf_icam;
}
#endif