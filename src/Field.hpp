#ifndef FIELD
#define FIELD

#include <iostream>
#include <vector>
#include "fftw3.h"
#include <assert.h>
#include "boost/random.hpp"
#include "boost/random/uniform_real_distribution.hpp"

#include <Eigen/Core>
#include "opencv2/core.hpp"

#include "RW.h"
#include "VectorField.h"
#include "GreensFunction.h"
#include "Poisson.h"
#include "HHD.h"

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;

namespace field{

	inline void newtonfunc2D(double& f,double& g,double& f1,double& g1,double& f2,double& g2,double r1,double r2,int n1,int n2,double dx,double dy,double rx,double ry)
	{
		const double pi=3.141592653589;

		int l,p,l_off,p_off;
			double pi2,e,kappa,kappa2,lambda,lambda2;

			pi2=2.0*pi;

	    kappa=pi2/(double(n1)*dx);
	    kappa2=std::pow(kappa,2);

	    lambda=pi2/(double(n2)*dy);
	    lambda2=std::pow(lambda,2);

	    f=0.0; g=0.0; f1=0.0; g1=0.0; f2=0.0; g2=0.0;

		for(l=0;l<(n1/2*2);l++)
		{
			l_off=l-n1/2+1;
		    for(p=0;p<(n2/2*2);p++)
		    {
	    		p_off=p-n2/2+1;

	    		if(p==n2/2-1 && l==n1/2-1) continue;

	 	   		e=std::exp( -2.0*( kappa2*double(l_off*l_off)/std::pow(r1,2) + lambda2*double(p_off*p_off)/std::pow(r2,2) ) );

	    		f=f + e * ( cos(kappa *double(l_off)*rx) - std::exp(-1.0) );
	  			g=g + e * ( cos(lambda*double(p_off)*ry) - std::exp(-1.0) );

			    f1=f1 + e * (4.0*kappa2 *double(l_off*l_off)/std::pow(r1,3)) * ( std::cos(kappa *double(l_off)*rx) - std::exp(-1.0) );
			    g1=g1 + e * (4.0*kappa2 *double(l_off*l_off)/std::pow(r1,3)) * ( std::cos(lambda*double(p_off)*ry) - std::exp(-1.0) );
			    f2=f2 + e * (4.0*lambda2*double(p_off*p_off)/std::pow(r2,3)) * ( std::cos(kappa *double(l_off)*rx) - std::exp(-1.0) );
	  	        g2=g2 + e * (4.0*lambda2*double(p_off*p_off)/std::pow(r2,3)) * ( std::cos(lambda*double(p_off)*ry) - std::exp(-1.0) );

	    	}
	    }

	}

	inline void newton2D(double& r1, double& r2, int n1, int n2, double dx, double dy, double rx, double ry, bool& lconv)
	{
		const double eps=1e-5;
		double inc1,inc2,err1,err2;
	    int i,j;
	    double f,g,f1,g1,f2,g2;

	    double r1ini,r2ini,gamma;

	    r1ini=r1;
	    r2ini=r2;
	    gamma=1.25;

	    lconv=false;

	    for(j=1;j<=10;j++){
	    	r1=double(j)*r1ini;
	    	r2=double(j)*r2ini;
	  		gamma=gamma-0.25/double(j);

	  		for(i=0;i<100;i++){

	  			newtonfunc2D(f,g,f1,g1,f2,g2,r1,r2,n1,n2,dx,dy,rx,ry);

		        inc1 =  (f*g2-f2*g)/(f1*g2-f2*g1);
		        inc2 =  (f1*g-f*g1)/(f1*g2-f2*g1);

		        r1 = r1 - gamma*inc1;
		        r2 = r2 - gamma*inc2;

		        r1=std::max(r1,0.000001);
		        r2=std::max(r2,0.000001);

		        r1=std::min(r1,1.0);
		        r2=std::min(r2,1.0);

		        err1 = inc1/(std::abs(r1)+eps);
		        err2 = inc2/(std::abs(r2)+eps);
	  		
	      		if(std::abs(err1)+std::abs(err2) < eps)
	      		{
	      			lconv=true;
	      			break;
	      		}	
	      	}

	      	if (lconv) break;

	    }

	    if (!lconv) 
	    {
	    	cout<<"Newton did not converge."<<endl;
	        cout<<"Probably an error in input parameters."<<endl;
	        cout<<"Is the dx resolving the rx scale?"<<endl;
	    }

	}

	inline void pseudo2dM(MatrixXd& res, int nx, int ny, int lde, double rx, double ry, double dx, double dy, int n1, int n2, double theta, int noise_const=1)
	{
		double r1,r2,c;

		fftw_plan plan;
		int l,p,j,l_off,p_off;
		double kappa2,lambda2,kappa,lambda;
	 	double pi2,deltak,summ;
	  double a11tmp,a22tmp,a11,a22,a12,torad;
	  
	  double*   Amat;

	  static boost::mt19937 rng;
	  static boost::random::uniform_real_distribution<> nd(0,1);

	  const double   pi=3.141592653589;
	  bool   		   cnv;
	  double 		   e;

	  if(lde<1)      std::cerr << "pseudo2D: error lde < 1\n"<<endl;
		if(rx <= 0.0)  std::cerr << "pseudo2D: error, rx <= 0.0\n"<<endl;
		if(ry <= 0.0)  std::cerr << "pseudo2D: error, ry <= 0.0\n"<<endl;
		if(n1 < nx)    std::cerr << "pseudo2D: n1 < nx\n"<<endl;
		if(n2 < ny)    std::cerr << "pseudo2D: n2 < ny\n"<<endl;

	  double *fampl,*phi,*y;
	  fftw_complex *x;

	  assert(res.rows()==nx*ny);
	  assert(res.cols()==lde);

	  Amat  = (double*) malloc(nx*ny*lde*sizeof(double));

	  fampl = (double*) malloc(n1*(n2+2)*sizeof(double));
	  phi   = (double*) malloc(n1*(n2/2+1)*sizeof(double));
	  y     = (double*) malloc(n1*n2*sizeof(double));
	  x     = (fftw_complex*) fftw_malloc(n1*(n2/2+1)*sizeof(fftw_complex));

	  pi2=2.0*pi;
	  deltak=std::pow(pi2,2)/(double(n1*n2)*dx*dy);
		kappa=pi2/(double(n1)*dx);
		kappa2=std::pow(kappa,2);
		lambda=pi2/(double(n2)*dy);
		lambda2=std::pow(lambda,2);

		if(y!=NULL)
		{
			free(y);
			y = (double*) malloc(n1*n2*sizeof(double));
		} 

		plan=fftw_plan_dft_c2r_2d(n1,n2,x,y,FFTW_ESTIMATE);

		r1=3.0/rx;
		r2=3.0/ry;

		newton2D(r1,r2,n1,n2,dx,dy,rx,ry,cnv);

		if (!cnv) 
		{
			std::cerr<<"newton did not converge"<<endl;
			std::abort();
		}

		summ=0.0;
		for(l=0;l<n1;l++){
			l_off=l-n1/2;
	  		for(p=0;p<n2;p++){
	  			p_off=p-n2/2;
			    summ=summ+std::exp(-2.0*(kappa2*double(l_off*l_off)/std::pow(r1,2) + lambda2*double(p_off*p_off)/std::pow(r2,2)));
	  		}
	  	}

	  	summ--;

	  	c=std::sqrt(1.0/(deltak*summ));

	  	// Rotation to angle theta
	    a11tmp=1.0/std::pow(r1,2);
	    a22tmp=1.0/std::pow(r2,2);
	    torad=pi/180.0;
	    a11=a11tmp*std::pow(std::cos(theta*torad),2) + a22tmp*std::pow(std::sin(theta*torad),2);
	    a22=a11tmp*std::pow(std::sin(theta*torad),2) + a22tmp*std::pow(std::cos(theta*torad),2);
	    a12=(a22tmp-a11tmp)*std::cos(theta*torad)*std::sin(theta*torad);

	    for(j=0;j<lde;j++)
	    {
		  	// Calculating the random wave phases
		    for(l=0;l<n1;l++)
		    {
		    	for(p=0;p<n2/2+1;p++)
		    	{
		    		double temp=nd(rng);
				    phi[p+(n2/2+1)*l]=pi2*temp;
		    	}
		    }

		    // Calculating the wave amplitues
	        for(l=0;l<n1;l++)
	        {
	        	// if (l<n1/2+1)
	        	l_off=l;
	        	// else
	        	// l_off=l-n1;

			    for(p=0;p<n2/2+1;p++)
			    {
			    	p_off=p;

			        e=std::exp(-( a11*kappa2*double(l_off*l_off) + 2.0*a12*kappa*lambda*double(l_off*p_off) + a22*lambda2*double(p_off*p_off) ));

			        fampl[2*(p+(n2/2+1)*l)]  =e*std::cos(phi[p+(n2/2+1)*l])*std::sqrt(deltak)*c;
			        fampl[1+2*(p+(n2/2+1)*l)]=e*std::sin(phi[p+(n2/2+1)*l])*std::sqrt(deltak)*c;
			        
		    	}
			}
		    fampl[0]=0.0;
		    fampl[1]=0.0;

		    for(l=0;l<n1/2;l++)
		    {
		    	for(p=0;p<n2/2+1;p++) 
	        	{
	        		x[p+(n2/2+1)*l][0]=fampl[2*(p+(n2/2+1)*l)];
	        		x[p+(n2/2+1)*l][1]=fampl[1+2*(p+(n2/2+1)*l)];
	        	}
	        }

		    for(l=n1/2;l<n1;l++)
		    {
	  	        for(p=0;p<n2/2+1;p++) 
	  	        {
		        	x[p+(n2/2+1)*l][0]=fampl[2*(p+(n2/2+1)*l)];
		        	x[p+(n2/2+1)*l][1]=fampl[1+2*(p+(n2/2+1)*l)];
		        	//Symmetric this section by inverting the index 
		        	// x[p+(n2/2+1)*l][0]=fampl[2*(p+(n2/2+1)*(n1-1-l))];
		        	// x[p+(n2/2+1)*l][1]=fampl[1+2*(p+(n2/2+1)*(n1-1-l))];		        	
	  	        }
		    }
	   
	  		fftw_execute(plan);

	      	for(l=0;l<nx;l++)
	      	{
	      		for(p=0;p<ny;p++)
	      		{
		         	// Amat[j+lde*(p+ny*l)]=y[p+n2*l];
		         	// Amat[p+ny*l+nx*ny*j]=y[p+n2*l];
		         	Amat[l+nx*p+nx*ny*j]=y[l+n1*p];
	      		}
	      	}

	    }

	    Eigen::Map<MatrixXd> Amat_eigen(Amat,nx*ny,lde);

		res=Amat_eigen*noise_const;

		free ( fampl );
	    free ( phi );
		free ( y );
		fftw_free ( x );

	    fftw_destroy_plan(plan);

	}

	inline void getDivergenceFreeFieldFromHelmholtzDecomposition(const std::vector<MatrixXd>& ux, const std::vector<MatrixXd>& uy, const std::vector<MatrixXd>& uz, const cv::Point3i& nXc, const cv::Point3d& dXc,
	                                                      std::vector<MatrixXd>& ux_res, std::vector<MatrixXd>& uy_res, std::vector<MatrixXd>& uz_res, std::vector<MatrixXd>& div_res, std::vector<MatrixXd>& mag_res,
	                                                      std::vector<MatrixXd>& ux_res_irr, std::vector<MatrixXd>& uy_res_irr, std::vector<MatrixXd>& uz_res_irr,
	                                                      std::vector<MatrixXd>& ux_res_har, std::vector<MatrixXd>& uy_res_har, std::vector<MatrixXd>& uz_res_har){

	    RGrid rgrid(nXc.x, nXc.y, nXc.z, dXc.x, dXc.y, dXc.z);

	    VectorField<float> vfield(ux, uy, uz, rgrid.dim);

	    vfield.need_magnitudes(rgrid);
	    vfield.need_divcurl(rgrid);
	    vfield.show_stats("vfield");

	    naturalHHD<float> nhhd(vfield, rgrid, 1);

	    VectorField<float> d, r, h;

	    r.compute_as_curl_field(nhhd.Ru, nhhd.Rv, nhhd.Rw, rgrid);
	    r.need_magnitudes(rgrid);
	    r.need_divcurl(rgrid);
	    r.show_stats("r");

	    r.utoEigenMatVec(ux_res,rgrid);
	    r.vtoEigenMatVec(uy_res,rgrid);
	    r.wtoEigenMatVec(uz_res,rgrid);
	    r.divtoEigenMatVec(div_res,rgrid);
	    r.magtoEigenMatVec(mag_res,rgrid);

	    d.compute_as_gradient_field(nhhd.D, rgrid);
	    d.need_magnitudes(rgrid);
	    d.need_divcurl(rgrid);
	    d.show_stats("d");

	    h.compute_as_harmonic_field(vfield, d, r);
	    h.need_magnitudes(rgrid);
	    h.need_divcurl(rgrid);
	    h.show_stats("h");

	    d.utoEigenMatVec(ux_res_irr,rgrid);
	    d.utoEigenMatVec(uy_res_irr,rgrid);
	    d.utoEigenMatVec(uz_res_irr,rgrid);

	    h.utoEigenMatVec(ux_res_har,rgrid);
	    h.utoEigenMatVec(uy_res_har,rgrid);
	    h.utoEigenMatVec(uz_res_har,rgrid);
	}

	inline void getDivergenceFreeFieldFromHelmholtzDecomposition(const std::vector<MatrixXd>& ux, const std::vector<MatrixXd>& uy, const std::vector<MatrixXd>& uz, const cv::Point3i& nXc, const cv::Point3d& dXc,
	                                                      std::vector<MatrixXd>& ux_res, std::vector<MatrixXd>& uy_res, std::vector<MatrixXd>& uz_res, std::vector<MatrixXd>& div_res, std::vector<MatrixXd>& mag_res){

	    RGrid rgrid=RGrid(nXc.x, nXc.y, nXc.z, dXc.x, dXc.y, dXc.z);

	    VectorField<float> vfield(ux, uy, uz, rgrid.dim);

	    vfield.need_magnitudes(rgrid);
	    vfield.need_divcurl(rgrid);
	    vfield.show_stats("vfield");

	    naturalHHD<float> nhhd(vfield, rgrid, 1);

	    VectorField<float> r;

	    r.compute_as_curl_field(nhhd.Ru, nhhd.Rv, nhhd.Rw, rgrid);

	    r.need_magnitudes(rgrid);

	    r.need_divcurl(rgrid);

	    r.show_stats("r");

	    r.utoEigenMatVec(ux_res,rgrid);
	    r.vtoEigenMatVec(uy_res,rgrid);
	    r.wtoEigenMatVec(uz_res,rgrid);
	    r.divtoEigenMatVec(div_res,rgrid);
	    r.magtoEigenMatVec(mag_res,rgrid);
	}

	inline void normalizeDivergenceFreeField(std::vector<MatrixXd>& ux, std::vector<MatrixXd>& uy, std::vector<MatrixXd>& uz, const std::vector<MatrixXd>& mag_res){

	    double max_mag=0.0, max_mag_old=0.0;

	    for(int k=0; k<mag_res.size(); k++){
	        max_mag=mag_res[k].cwiseAbs().maxCoeff();
	        if(k==0)
	            max_mag_old=max_mag;
	        else
	            if(max_mag > max_mag_old)
	                max_mag_old=max_mag;
	            else
	                max_mag=max_mag_old;
	    }

	    for(int k=0; k<ux.size(); k++){
	        ux[k]/=max_mag;
	        uy[k]/=max_mag;
	        uz[k]/=max_mag;
	    }
	}

	inline std::vector<MatrixXd> computedFdx(const std::vector<MatrixXd>& F){
		std::vector<MatrixXd> dFdx;

		int nRows = F[0].rows();
		int nCols = F[0].cols();

		for(int z=0; z<F.size(); z++){
			MatrixXd dF_2D_dx(nRows, nCols);

			dF_2D_dx.block(0, 1, nRows, nCols-2) = (F[z].rightCols(nCols-2) - F[z].leftCols(nCols-2) )/2.;
			dF_2D_dx.leftCols(1)  = dF_2D_dx.block(0,1,nRows,1);
			dF_2D_dx.rightCols(1) = dF_2D_dx.block(0,nCols-2,nRows,1);

			dFdx.push_back(dF_2D_dx);
		}

		return dFdx;
	}

	inline std::vector<MatrixXd> computedFdy(const std::vector<MatrixXd>& F){
		std::vector<MatrixXd> dFdy;

		int nRows = F[0].rows();
		int nCols = F[0].cols();

		for(int z=0; z<F.size(); z++){
			MatrixXd dF_2D_dy(nRows, nCols);

			dF_2D_dy.block(1, 0, nRows-2, nCols) = (F[z].bottomRows(nRows-2) - F[z].topRows(nRows-2) )/2.;
			dF_2D_dy.topRows(1) = dF_2D_dy.block(1,0,1,nCols);
			dF_2D_dy.bottomRows(1) = dF_2D_dy.block(nRows,0,1,nCols);

			dFdy.push_back(dF_2D_dy);
		}

		return dFdy;
	}

	inline std::vector<MatrixXd> computedFdz(const std::vector<MatrixXd>& F){
	    int nRows = F[0].rows();
	    int nCols = F[0].cols();
	    int nZ = F.size();

		std::vector<MatrixXd> dFdz(nZ, MatrixXd::Zero(nRows, nCols));

		for(int z=1; z<F.size()-1; z++){
			dFdz[z] = (F[z+1] - F[z-1])/2.;
		}
		dFdz[0]    = dFdz[1];
		dFdz[nZ-2] = dFdz[nZ-1];

		return dFdz;
	}
}

#endif