#ifndef PARTICLE
#define PARTICLE

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <memory>
#include <math.h>
#include <cassert>
#include <utility>
#include <algorithm>
#include <optional>
#include <iomanip>
#include <mutex>
#include <unordered_map>
#include <chrono>

#include "boost/format.hpp"
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include "boost/filesystem.hpp"
#undef BOOST_NO_CXX11_SCOPED_ENUMS

#include <Eigen/Core>
#include <Eigen/Cholesky>
#include <Eigen/LU>
#include <Eigen/Dense>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/core/eigen.hpp"
#include "opencv2/ml.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/ximgproc.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudastereo.hpp>
#include <opencv2/tracking.hpp>

#ifdef INTELTBB
#include "tbb/tbb.h"
#include "tbb/parallel_for.h"
#include "tbb/concurrent_vector.h"
#endif

#include <vtkSmartPointer.h>
#include <vtkHexahedron.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkXMLMultiBlockDataWriter.h>

#include <vtkImageData.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkStructuredGrid.h>
#include <vtkStructuredGridWriter.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridWriter.h>

#include "ReadIMX.h"
#include "ReadIM7.h"
// #include "zlib.h"
#include "tinyxml2.h"

#include "tricubic.h"

#include "interpolation.h"
using namespace alglib;

#include <pcl/point_cloud.h>
#include <pcl/octree/octree_pointcloud.h>
#include <pcl/octree/octree_search.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>
#include <pcl/filters/extract_indices.h>

#include "ceres/ceres.h"

#include "kcftracker.hpp"

#include "SemiGlobalMatching.h"
#include "sgm_util.h"
#include "sgm_types.h"

#include "cnpy.h"

#include <torch/torch.h>
#include <torch/script.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::MatrixXf;
using Eigen::VectorXf;

using std::cout;
using std::endl;

using Clock=std::chrono::high_resolution_clock;

class Camera;

typedef std::unique_ptr<Camera> Camera_ptr;

inline bool checkParticleIsOutOfRange(const cv::Point3d& Xc, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, double tolerance=1e-5){
    return ( Xc.x - Xc_top.x > tolerance || Xc.x - Xc_bottom.x < -1.*tolerance ||
             Xc.y - Xc_top.y > tolerance || Xc.y - Xc_bottom.y < -1.*tolerance ||
             Xc.z - Xc_top.z > tolerance || Xc.z - Xc_bottom.z < -1.*tolerance );
}

class Particle
{
    public:
        static double E_maximum_;
        static double E_minimum_;

        static bool   delete_verbal_guard_;
        static int    num_deleted_;
        static int    num_deleted_tot_;

        enum class Velocity_scheme_Type {FiniteDifference, CurveFitDerivative};

    private:
        bool     is_deleted_=false;
        bool     is_ontrack_=true;
        bool     is_lost_=false;
        bool     is_terminated_=false;
        bool     is_triangulated_=false;
        bool     is_to_be_triangulated_=false;
        bool     is_tracked_=false;
        bool     is_to_be_tracked_=false;
        bool     is_ghost_=false;
        bool     is_trained_=false;            
        bool     is_new_=false;
        bool     is_with_predictor_=true;

        int      part_index_;
        int      it_seq_deb_;
        int      it_seq_fin_;
        std::vector< double >                        Ev_;
        std::vector< cv::Point3d >                   Xcoord_;
        std::vector< std::vector<cv::Point2d> >      xcoord_;
        ///small local patch particle image
        std::vector< std::vector<cv::Mat> >          img_;
        //.position of local patch particle image in camera image space (x,y,width,height)
        std::vector< std::vector<cv::Rect> >         imgPos_;       
        std::vector< std::vector<cv::Mat> >          imgRespar_;
        std::vector< std::vector<cv::Rect> >         imgResparPos_;
        std::vector< std::vector<cv::Mat> >          imgFeature_;
        MatrixXd                                     hessian_;
        MatrixXd                                     rhs_;
        std::vector< cv::Point3d >                   V_;
        std::vector< cv::Point3d >                   acc_;

        std::vector< int >                           on_image_;

        std::vector< std::vector<uint32_t> >         censusFeature_;

        double                                       intensity_variation_ratio_=1.;

        bool     is_side_=false;
        int      part_index_main_;

        double   regul_const_=0.;

    public:
        Particle();

        ~Particle();

        ///dummy constructor
        Particle(int, int, int);

        Particle(int, int, int, double); 

        Particle(int, int, int, const std::vector<double>&);

        Particle(int, int, int, const std::vector<cv::Point3d>&, const std::vector<double>&);

        Particle(int, int, int, const std::vector<cv::Point3d>&, const std::vector<cv::Point3d>&, const std::vector<double>&);

        Particle(int, int, int, const cv::Point2d&, const cv::Point2d&);

        Particle(int, int, int, const cv::Point2d&, const cv::Point2d&, const cv::Point2d&, const cv::Point2d&);

        void checkIfParticleAppearsOnSnapshot(int it_seq_cnt){
            is_ontrack_ = is_ghost_ ? false : ( (it_seq_cnt < it_seq_deb_ || it_seq_cnt > it_seq_fin_) ? false : true );
        }

        void checkIfParticleAppearsOnSnapshot(int it_seq_cnt, int it_seq_fin_syn){
            is_ontrack_ = ( (it_seq_cnt < it_seq_deb_ || it_seq_cnt > it_seq_fin_syn) ? false : true );
        }

        void checkIfParticleAppearsOnSnapshot(int it_seq_cnt, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){
            is_ontrack_ = is_deleted_ ? false : ( checkParticleIsOutOfRange(Xcoord_.back(), Xc_top, Xc_bottom) ? false : true );
        }

        void deleteData(bool save_memory_flag=true, bool verbal_flag=false){
            is_deleted_=true;
            is_ontrack_=false;
            if(verbal_flag){
                if(delete_verbal_guard_){
                    cout<<"delete particle ";
                    delete_verbal_guard_=false;
                }
                cout<<part_index_<<" ";
            }
            num_deleted_++;
            
            if(save_memory_flag){
                std::vector< double >().swap(Ev_);
                std::vector< cv::Point3d >().swap(Xcoord_);
                std::vector< cv::Point3d >().swap(V_);
            }

            std::vector< std::vector<cv::Point2d> >().swap(xcoord_);        
            std::vector< std::vector<cv::Mat> >().swap(img_);
            std::vector< std::vector<cv::Rect> >().swap(imgPos_);
            std::vector< std::vector<cv::Mat> >().swap(imgRespar_);
            std::vector< std::vector<cv::Rect> >().swap(imgResparPos_);
            std::vector< std::vector<cv::Mat> >().swap(imgFeature_);
            std::vector< int >().swap(on_image_);
        }

        void projectToImageSpace(int, const std::vector<Camera_ptr>&, bool synthetic_img_flag=false);

        void markAsLost(void){ is_lost_=true; }
        void markAsFoundLost(void){ is_lost_=false; }
        void markAsTerminated(void){ is_terminated_=true; }
        void markAsTrained(void){ is_trained_=true; }
        void markAsNonTrained(void){ is_trained_=false; }
        void markAsTriangulated(void){ is_triangulated_=true; is_to_be_triangulated_=false; }
        void markAsToBeTriangulated(void){ is_to_be_triangulated_=true; is_triangulated_=false; }
        void markAsTracked(void){ is_tracked_=true; }
        void markAsToBeTracked(void){ is_to_be_tracked_=true; }
        void markAsNew(void){ is_new_=true; }
        void markAsGhost(void){ is_ghost_=true; }
        void markAsSide(int part_index_main){ is_side_=true; part_index_main_ = part_index_main; }
        void markAsNonSide(void){ is_side_=false; part_index_main_ = part_index_; }

        const bool& is_deleted(void)            const { return is_deleted_; }
        const bool& is_ontrack(void)            const { return is_ontrack_; }
        const bool& is_lost(void)               const { return is_lost_; }
        const bool& is_terminated(void)         const { return is_terminated_; }
        const bool& is_trained(void)            const { return is_trained_; }
        const bool& is_triangulated(void)       const { return is_triangulated_; }
        const bool& is_to_be_triangulated(void) const { return is_to_be_triangulated_; }
        const bool& is_tracked(void)            const { return is_tracked_; }
        const bool& is_to_be_tracked(void)      const { return is_to_be_tracked_; }
        const bool& is_new(void)                const { return is_new_; }
        const bool& is_ghost(void)              const { return is_ghost_; }
        const bool& is_with_predictor(void)     const { return is_with_predictor_; }
        const bool& is_side(void)               const { return is_side_; }

        void setis_ontrack_flag_as(const bool& is_ontrack){ is_ontrack_=is_ontrack; } 
        void setis_to_be_tracked_flag_as(const bool& is_to_be_tracked){ is_to_be_tracked_=is_to_be_tracked; } 
        void setis_tracked_flag_as(const bool& is_tracked){ is_tracked_=is_tracked; } 
        void setis_triangulated_flag_as(const bool& is_triangulated){ is_triangulated_=is_triangulated; is_to_be_triangulated_=!is_triangulated; } 
        void setis_deleted_flag_as(const bool& is_deleted){ is_deleted_=is_deleted; } 
        void setis_ghost_flag_as(const bool& is_ghost){ is_ghost_=is_ghost; } 
        void setis_lost_flag_as(const bool& is_lost){ is_lost_=is_lost; } 
        void setis_new_flag_as(const bool& is_new){ is_new_=is_new; } 
        void setis_with_predictor_as(const bool& is_with_predictor){ is_with_predictor_=is_with_predictor; }
        void setis_side_flag_as(const bool& is_side){ is_side_=is_side; }

        const int& part_index(void) const { return part_index_; }
        const int& it_seq_deb(void) const { return it_seq_deb_; }
        const int& it_seq_fin(void) const { return it_seq_fin_; }
        const double& intensity_variation_ratio(void) const { return intensity_variation_ratio_; }
        const double& regul_const(void) const {return regul_const_; }

        const int  length(bool check_content=true) { return check_content ? Xcoord_.size() : it_seq_fin_-it_seq_deb_+1; }
        const int  ID(void) { return part_index_; }
        const int  length(int i_cam) { return xcoord_[i_cam].size(); }

        const std::vector< double >&                    Ev(void)           const { return Ev_; }
        const std::vector< cv::Point3d >&               Xcoord(void)       const { return Xcoord_; }
        const std::vector< std::vector<cv::Point2d> >&  xcoord(void)       const { return xcoord_; }        
        const std::vector< std::vector<cv::Mat> >&      img(void)          const { return img_; }
        const std::vector< std::vector<cv::Rect> >&     imgPos(void)       const { return imgPos_; }
        const std::vector< std::vector<cv::Mat> >&      imgRespar(void)    const { return imgRespar_; }
        const std::vector< std::vector<cv::Rect> >&     imgResparPos(void) const { return imgResparPos_; }
        const std::vector< std::vector<cv::Mat> >&      imgFeature(void)        const { return imgFeature_; }
        const std::vector< int >&                       on_image(void)     const { return on_image_; }

        const MatrixXd& hessian(void) const { return hessian_; }
        const MatrixXd& rhs(void) const { return rhs_; }

        const std::vector<cv::Point3d>& V(void)   const { return V_; }
        const std::vector<cv::Point3d>& acc(void) const { return acc_; }

        void setimgPos(const std::vector< std::vector<cv::Rect> >& imgPos){ imgPos_=imgPos; }
        void setimgPos(const cv::Rect& imgPos, int i_cam, int it_img){ imgPos_.at(i_cam).at(it_img)=imgPos; }

        void setID(int ID){ part_index_=ID; }
        void setit_seq_fin(int it_seq_fin){ it_seq_fin_=it_seq_fin; }
        void setXcoord(const std::vector<cv::Point3d>& Xcoord){ Xcoord_=Xcoord; }
        void setxcoord(const std::vector< std::vector<cv::Point2d> >& xcoord){ xcoord_=xcoord; }
        void setEv(const std::vector<double>& Ev){ Ev_=Ev; }
        void setintensity_variation_ratio(double intensity_variation_ratio){ intensity_variation_ratio_=intensity_variation_ratio; }
        void setregul_const(double regul_const){ regul_const_=regul_const; }
        void setV(const std::vector<cv::Point3d>& V){ V_=V; }
        void setacc(const std::vector<cv::Point3d>& acc){ acc_=acc; }

        void push_backEvElement(double E, int it_seq){ Ev_.push_back(E); }

        void push_backxcoordElement(const cv::Point2d& xc1, const cv::Point2d& xc2, const cv::Point2d& xc3, const cv::Point2d& xc4){
            xcoord_[0].push_back(xc1);
            xcoord_[1].push_back(xc2);
            xcoord_[2].push_back(xc3);
            xcoord_[3].push_back(xc4);
        };

        void computeVelocity(int, double, Velocity_scheme_Type, bool full_velo_flag=true);
        
        void assignInitialIntensity(int);

        double sumResidualForParticlep(int, int, int);

        void push_backXcoordElement(const cv::Point3d& Xc, int it_seq){ Xcoord_.push_back(Xc); }
        void push_backXcoordElement(const cv::Point3d&& Xc, int it_seq){ Xcoord_.push_back(Xc); }

        void push_backxcoordElement(const cv::Point2d& xc, int i_cam, int it_seq){
            xcoord_[i_cam].push_back(xc);
        }

        void push_backVElement(const cv::Point3d& V, int it_seq){ V_.push_back(V); }
        void push_backVElement(const cv::Point3d&& V, int it_seq){ V_.push_back(V); }

        void push_backimgResparElement(const std::vector<cv::Mat>& imgRespar_cam){ imgRespar_.push_back(imgRespar_cam); }
        void push_backimgResparPosElement(const std::vector<cv::Rect>& imgResparPos_cam){ imgResparPos_.push_back(imgResparPos_cam); }

        void push_backimgFeatureElement(const std::vector<cv::Mat>& imgFeature_cam){ imgFeature_.push_back(imgFeature_cam); }
        void push_backimgFeatureElementForiCam(const cv::Mat& imgFeature_icam, int i_cam){ imgFeature_.at(i_cam).push_back(imgFeature_icam); }

        void push_backcensusFeatureElement(const std::vector<uint32_t>& censusFeature_cam){ censusFeature_.push_back(censusFeature_cam); }

        void sethessian(const MatrixXd& hessian){ hessian_=hessian; }
        void setrhs(const MatrixXd& rhs){ rhs_=rhs; }
        
        void setEvElement(double E, int it_seq_cnt){
            if(Ev_.size()>=it_seq_cnt-it_seq_deb_+1)
                Ev_.at(it_seq_cnt-it_seq_deb_)=E;
            else
                cout<<"Ev size "<<Ev_.size()<<" + it_seq_deb_ "<<it_seq_deb_
                    <<" not enough for current it_seq_cnt "<<it_seq_cnt
                    <<" for particle "<<part_index_<<", consider use push_back."<<endl;
        }

        void setxcoordElement(const cv::Point2d& xc, int i_cam, int it_seq_cnt){
            assert(!xcoord_.empty());
            if(xcoord_[i_cam].size()>=it_seq_cnt-it_seq_deb_+1)
                xcoord_[i_cam].at(it_seq_cnt-it_seq_deb_) = xc;
            else
                cout<<"xcoord at cam "<<i_cam<<" size "<<xcoord_[i_cam].size()<<" + it_seq_deb_ "<<it_seq_deb_
                    <<" not enough for current it_seq_cnt "<<it_seq_cnt
                    <<" for particle "<<part_index_<<", consider use push_back."<<endl;
        }

        void setXcoordElement(const cv::Point3d& Xc, int it_seq_cnt){ 
            if(Xcoord_.size()>=it_seq_cnt-it_seq_deb_+1)
                Xcoord_.at(it_seq_cnt-it_seq_deb_) = Xc; 
            else
                cout<<"Xcoord size "<<Xcoord_.size()<<" + it_seq_deb_ "<<it_seq_deb_
                    <<" not enough for current it_seq_cnt "<<it_seq_cnt
                    <<" for particle "<<part_index_<<", consider use push_back."<<endl;
        }

        void setXofXcoordElement(double XcX, int it_seq_cnt){ 
            if(Xcoord_.size()+it_seq_deb_-1==it_seq_cnt)
                Xcoord_.back().x = XcX; 
            else
                cout<<"Xcoord size "<<Xcoord_.size()
                    <<" not enough for current it_seq_cnt "<<it_seq_cnt
                    <<" for particle "<<part_index_<<", consider use push_back."<<endl;
        }

        void setYofXcoordElement(double XcY, int it_seq_cnt){ 
            if(Xcoord_.size()+it_seq_deb_-1==it_seq_cnt)
                Xcoord_.back().y = XcY; 
            else
                cout<<"Xcoord size "<<Xcoord_.size()
                    <<" not enough for current it_seq_cnt "<<it_seq_cnt
                    <<" for particle "<<part_index_<<", consider use push_back."<<endl;
        }

        void setZofXcoordElement(double XcZ, int it_seq_cnt){ 
            if(Xcoord_.size()+it_seq_deb_-1==it_seq_cnt)
                Xcoord_.back().z = XcZ; 
            else
                cout<<"Xcoord size "<<Xcoord_.size()
                    <<" not enough for current it_seq_cnt "<<it_seq_cnt
                    <<" for particle "<<part_index_<<", consider use push_back."<<endl;
        }

        void pop_back_Xcoord(void){
            Xcoord_.pop_back();
        }

        const cv::Point3d getCurrentXcoord(int it_seq_cnt) const{
            return Xcoord_.at(it_seq_cnt - it_seq_deb_);
        }

        const cv::Point3d getPreviousXcoord(int it_seq_cnt) const{
            return Xcoord_.at(it_seq_cnt - it_seq_deb_ - 1);
        }

        const cv::Point3d getPreviousOfPreviousXcoord(int it_seq_cnt) const{
            return Xcoord_.at(it_seq_cnt - it_seq_deb_ - 2);
        }

        const cv::Point2d getCurrentxcoord(int i_cam, int it_seq_cnt) const{
            return xcoord_[i_cam].at(it_seq_cnt - it_seq_deb_);
        }

        double getCurrentE(int it_seq_cnt) const{
            return Ev_.at(it_seq_cnt - it_seq_deb_);
        }

        double getPreviousE(int it_seq_cnt) const{
            return Ev_.at(it_seq_cnt - it_seq_deb_ -1);
        }

        const cv::Point3d getCurrentV(int it_seq_cnt) const{
            return V_.at(it_seq_cnt - it_seq_deb_);
        }

        const cv::Point3d getPreviousV(int it_seq_cnt) const{
            return V_.at(it_seq_cnt - it_seq_deb_ - 1);
        }

        const cv::Point3d getPreviousOfPreviousV(int it_seq_cnt) const{
            return V_.at(it_seq_cnt - it_seq_deb_ - 2);
        }

        void clearimgAndimgPos(void){
            if(!img_.empty())       img_.clear();
            if(!imgPos_.empty())    imgPos_.clear();
        }

        void clearimgFeature(void){ 
            if(!imgFeature_.empty())      imgFeature_.clear(); 
        }
        
        void clearimgRespar(void){
            if(!imgRespar_.empty())       imgRespar_.clear();
        }

        void clearimgResparPos(void){
            if(!imgResparPos_.empty())    imgResparPos_.clear();
        }

        void printSummary(void){
            cout<<"part_index_            "<<part_index_<<endl
                <<"Xcoord_.size()         "<<Xcoord_.size()<<endl
                <<"Ev_.size()             "<<Ev_.size()<<endl
                <<"V_.size()              "<<V_.size()<<endl
                <<"it_seq_deb_            "<<it_seq_deb_<<endl
                <<"it_seq_fin_            "<<it_seq_fin_<<endl
                <<"is_ontrack_            "<<is_ontrack_<<endl
                <<"is_deleted_            "<<is_deleted_<<endl
                <<"is_triangulated_       "<<is_triangulated_<<endl
                <<"is_to_be_triangulated_ "<<is_to_be_triangulated_<<endl
                <<"is_tracked_            "<<is_tracked_<<endl
                <<"is_to_be_tracked_      "<<is_to_be_tracked_<<endl
                <<"is_new_                "<<is_new_<<endl
                <<"is_terminated_         "<<is_terminated_<<endl
                <<"is_lost_               "<<is_lost_<<endl
                <<"is_ghost_              "<<is_ghost_<<endl
                <<"is_with_predictor_     "<<is_with_predictor_<<endl
                <<"is_side_               "<<is_side_<<endl;
            if(is_side_)
                cout<<"part_index_main_       "<<part_index_main_<<endl;
        }

        void printID(void){ cout<<"part_index "<<part_index_<<endl; }

        void printLastNXcoord(int N){
            int num=std::min(N, this->length());
            if(num<N) cout<<"Only "<<num<<" snapshot available!"<<endl;
            cout<<"Xc ";
            for(int i=0; i<num; i++) cout<<" "<<Xcoord_.rbegin()[i];
            cout<<endl;
        }

        void printLastNV(int N){
            int num=std::min(N, this->length());
            if(num<N) cout<<"Only "<<num<<" snapshot available!"<<endl;
            cout<<"V  ";
            for(int i=0; i<num; i++) cout<<" "<<V_.rbegin()[i];
            cout<<endl;
        }

        void printLastNEv(int N){
            int num=std::min(N, this->length());
            if(num<N) cout<<"Only "<<num<<" snapshot available!"<<endl;
            cout<<"Ev ";
            for(int i=0; i<num; i++) cout<<" "<<Ev_.rbegin()[i];
            cout<<endl;
        }

        void clearV(void){ std::vector< cv::Point3d >().swap(V_); }

    private:
        void projectToImageSpaceCore(const std::vector<Camera_ptr>&, const cv::Point3d&, double, bool synthetic_img_flag=false);
};

typedef std::unique_ptr<Particle> Particle_ptr;

inline cv::Point2d pcl2cv(const pcl::PointXY&   pt_pcl){ return cv::Point2d(pt_pcl.x, pt_pcl.y); }
inline cv::Point3d pcl2cv(const pcl::PointXYZ&  pt_pcl){ return cv::Point3d(pt_pcl.x, pt_pcl.y, pt_pcl.z); }
inline cv::Point3d pcl2cv(const pcl::PointXYZI& pt_pcl){ return cv::Point3d(pt_pcl.x, pt_pcl.y, pt_pcl.z); }
inline cv::Point3d pcl2cv(const pcl::PointXYZHSV& pt_pcl, bool velo=true){ if(velo) return cv::Point3d(pt_pcl.h, pt_pcl.s, pt_pcl.v); 
                                                                           else     return cv::Point3d(pt_pcl.x, pt_pcl.y, pt_pcl.z); }

inline pcl::PointXY     cv2pcl(const cv::Point2d& pt_cv)                         { return pcl::PointXY({     static_cast<float>(pt_cv.x), static_cast<float>(pt_cv.y)}); }
inline pcl::PointXYZ    cv2pcl(const cv::Point3d& pt_cv)                         { return pcl::PointXYZ(     static_cast<float>(pt_cv.x), static_cast<float>(pt_cv.y), static_cast<float>(pt_cv.z)); }
inline pcl::PointXYZI   cv2pcl(const cv::Point3d& pt_cv, double intensity)       { return pcl::PointXYZI({{  static_cast<float>(pt_cv.x), static_cast<float>(pt_cv.y), static_cast<float>(pt_cv.z)}, {static_cast<float>(intensity)}}); }
inline pcl::PointXYZHSV cv2pcl(const cv::Point3d& pt_cv, const cv::Point3d& v_cv){ return pcl::PointXYZHSV({{static_cast<float>(pt_cv.x), static_cast<float>(pt_cv.y), static_cast<float>(pt_cv.z)}, 
                                                                                                            {static_cast<float>(v_cv.x),  static_cast<float>(v_cv.y),  static_cast<float>(v_cv.z)}}); }
/// prediction and smoothing using 3rd order polynomial

inline double polynomialFit1D(const VectorXd& X, bool smoothing_flag=false, int Nsample=0, int Nbasis=0){

    assert(X.size()>=3);

    int size = Nsample==0 ? X.size() : std::min(static_cast<int>(X.size()), Nsample);

    int order = Nbasis==0 ? (size>=4 ? 4 : 3) : Nbasis;

    VectorXd Xsub(size);
    Xsub = X.tail(size);

    VectorXd T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    real_1d_array x, y;
    x.setcontent(size, T.data());
    y.setcontent(size, Xsub.data());

    ae_int_t info;
    barycentricinterpolant p;
    polynomialfitreport rep;

    polynomialfit(x, y, order, info, p, rep);

    int pt_pred = smoothing_flag ? size : size+1;

    auto pred = barycentriccalc(p, static_cast<double>(pt_pred));

    return pred;
}

inline double polynomialFit1D(const VectorXd& X, const VectorXd& Xprime, int Nsample=0, int Nbasis=0){

    assert(X.size()>=3);

    int size = Nsample==0 ? X.size() : std::min(static_cast<int>(X.size()), Nsample);

    int order = Nbasis==0 ? (size>=4 ? 4 : 3) : Nbasis;

    VectorXd Xsub(size);
    Xsub = X.tail(size);

    VectorXd T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    VectorXd W = VectorXd::Ones(size);
    VectorXd XC(Xprime.size());
    VectorXd YC(Xprime.size());
    XC[0] = T[size-1];
    YC[0] = Xprime[0];

    real_1d_array x, y, w, xc, yc;
    x.setcontent(size, T.data());
    y.setcontent(size, Xsub.data());
    w.setcontent(size, W.data());
    xc.setcontent(static_cast<int>(Xprime.size()), XC.data());
    yc.setcontent(static_cast<int>(Xprime.size()), YC.data());

    std::vector<long int> DC{1};
    integer_1d_array dc;
    dc.setcontent(static_cast<int>(Xprime.size()), &DC[0]);

    ae_int_t info;
    barycentricinterpolant p;
    polynomialfitreport rep;

    polynomialfitwc(x, y, w, xc, yc, dc, order, info, p, rep);

    int pt_pred = size+1;

    auto pred = barycentriccalc(p, static_cast<double>(pt_pred));

    return pred;
}

inline VectorXd polynomialLinearFit1D(const VectorXd& X){

    int size = 4;
    int order = 2;

    VectorXd Xsub(size);
    Xsub = X.tail(size);

    VectorXd Xfit(size), T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    real_1d_array x, y;
    x.setcontent(size, T.data());
    y.setcontent(size, Xsub.data());

    ae_int_t info;
    barycentricinterpolant p;
    polynomialfitreport rep;

    polynomialfit(x, y, order, info, p, rep);

    for(int i=0; i<size; i++){
        Xfit[i] = barycentriccalc(p, T[i]);
    }

    return Xfit;
}

/// compute velocity and acceleration using 3rd order polynomial

inline void polynomialFit1D(const VectorXd& X, VectorXd& Vel, VectorXd& Acc){

    assert(X.size()>=3);

    int size = X.size();
    int order = size>=4 ? 4 : 3;

    VectorXd T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    real_1d_array x, y;
    x.setcontent(size, T.data());
    y.setcontent(size, X.data());

    ae_int_t info;
    barycentricinterpolant p;
    polynomialfitreport rep;

    polynomialfit(x, y, order, info, p, rep);

    double f, df, d2f;
    for(int t=1; t<=size; t++){
        barycentricdiff2(p, static_cast<double>(t), f, df, d2f);
        Vel[t-1] = df;
        Acc[t-1] = d2f;
    }
}

/// prediction using cubic spline

inline double splineFit1D(const VectorXd& X){

    int size = X.size();

    VectorXd Xfit(size), T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    real_1d_array x, y;
    x.setcontent(size, T.data());
    y.setcontent(size, X.data());

    spline1dinterpolant s;
    spline1dfitreport rep;

    int nodes = size+4;

    spline1dfit(x, y, nodes, 0, s, rep);

    int pt_pred = size+1;

    auto pred = spline1dcalc(s, static_cast<double>(pt_pred));
            
    return pred;
}

/// smoothing using cubic spline 

inline VectorXd splineFit1D(const VectorXd& X, double lambda){

    int size = X.size();

    VectorXd Xfit(size), T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    real_1d_array x, y;
    x.setcontent(size, T.data());
    y.setcontent(size, X.data());

    spline1dinterpolant s;
    spline1dfitreport rep;

    int nodes = size+4;

    spline1dfit(x, y, nodes, lambda, s, rep);

    for(int i=1; i<size+1; i++){
        Xfit[i-1] = spline1dcalc(s, static_cast<double>(i));
    }
    
    return Xfit;
}

/// smoothing using cubic spline and compute derivatives

inline VectorXd splineFit1D(const VectorXd& X, double lambda, int nodes, VectorXd& Vel, VectorXd& Acc){

    int size = X.size();

    VectorXd Xfit(size), T;
    T.setLinSpaced(size, 1.0, static_cast<double>(size));

    real_1d_array x, y;
    x.setcontent(size, T.data());
    y.setcontent(size, X.data());

    spline1dinterpolant s;
    spline1dfitreport rep;

    spline1dfit(x, y, nodes, lambda, s, rep);

    double f, df, d2f;
    for(int i=1; i<size+1; i++){
        spline1ddiff(s, static_cast<double>(i), f, df, d2f);
        Xfit[i-1] = f;
        Vel[i-1]  = df;
        Acc[i-1]  = d2f;
    }
    
    return Xfit;
}

/// prediction using Wiener filter

inline double WienerLMSFit1D(VectorXd X, int Norder){
    bool shift_label =false;
    double shift = 10;

    if( fabs(X[Norder])<1 ){
        shift_label = true;
        X = X+shift*VectorXd::Ones(X.size());
    }

    double sum = 0;
    for (int i = 0; i < Norder; ++i) {
        sum = sum + X[i] * X[i];
    }
    double step = 1. / sum;

    VectorXd filter_param=VectorXd::Zero(Norder);

    double pred = 0;
    for (unsigned int i = 0; i < Norder; ++i) {
        pred = pred + filter_param[i] * X[i];
    }

    double err = X[Norder] - pred;

    while(fabs(err)>1e-8){
        for (unsigned int i = 0; i < Norder; ++i) {
            filter_param[i] = filter_param[i] + step * X[i] * err;
        }

        pred = 0;
        for (unsigned int i = 0; i < Norder; ++i) {
            pred = pred + filter_param[i] * X[i];
        }

        err = X[Norder] - pred;
    }

    pred = 0;
    for (unsigned int i = 0; i < Norder; ++i) {
        pred = pred + filter_param[i] * X[i + 1];
    }

    return (shift_label? pred - shift : pred);
}

#endif