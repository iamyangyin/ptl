#include "../LAVISION.hpp"

LAVISION::LAVISION(){}

LAVISION::~LAVISION(){}

void LAVISION::setIMGData(const std::string& img_file_pref, int it_deb, int it_tot, const std::vector<Camera_ptr>& cam, const std::string& img_dir){

	this->readIMGData(img_file_pref, it_deb, it_tot, img_dir);
	this->writeIMGTxtDataToPng(it_deb, it_tot, cam, img_dir);
}

void LAVISION::readIMGData(const std::string& img_file_pref, int it_deb, int it_tot, const std::string& img_dir){

	std::string img_file_suff=".im7";
	std::string res_file_pref="img_file";
	
	auto   imgdir_txt=img_dir+"/txtimg/";
    if(!boost::filesystem::exists(imgdir_txt)){
        boost::filesystem::create_directories(imgdir_txt);
    }

	for(int it=it_deb; it<=it_tot; it++){
		std::string img_file=str(boost::format("%1$s%2$05d%3$s") % img_file_pref % it % img_file_suff);
		std::string res_file=str(boost::format("%1$s/%2$s%3$05d") % imgdir_txt % res_file_pref % it );
		this->readIM7XFile(img_file, res_file);
	}
}

void LAVISION::writeIMGTxtDataToPng(int it_deb, int it_tot, const std::vector<Camera_ptr>& cam, const std::string& img_dir){ 

	std::string line;
	cv::Mat_<unsigned short> img;
	
	int n_width, n_height, n_cam, x;

	auto   imgdir_txt=img_dir+"/txtimg/";
	std::string res_file_pref="img_file";

	for(auto& ic :cam){
		if(!ic->Iproj().empty()) ic->clearIproj();
		for(int it_rec=it_deb; it_rec<=it_tot; it_rec++ ){

			img.create(ic->n_height_, ic->n_width_);

			std::string res_file=str(boost::format("%1$s/%2$s%3$05dcam%4$d.txt") % imgdir_txt % res_file_pref % it_rec % ic->i_cam_ );

			std::ifstream  resFile(res_file);
			if(resFile.is_open()){

				std::string firstline;
				std::getline(resFile, firstline);
				std::istringstream isfirstline(firstline);
				if(isfirstline >> n_width >> n_width >> n_height >> n_cam){
					assert(ic->n_width_==n_width);
					assert(ic->n_height_==n_height);
				}

				int row=0;
				while(std::getline(resFile, line)){

					std::istringstream is(line);

					int col=0;
					while(is >> x){
						img.at<unsigned short>(row,col)=x;
						col++;
					}

					row++;
				}
			}
			else{
				std::cerr<<"Error opening image data file "<<res_file<<endl;
				std::abort();
			}

			ic->setIprojElement(img);

			ic->saveImageProjection(it_rec, "cam", img_dir, true);

			img.release();
			resFile.close();
		}
	}
}

void LAVISION::setRigIMGData(const std::string& img_file_pref, int ic, const std::string& img_dir){

	this->readRigIMGData(img_file_pref, ic, img_dir);
	this->writeRigIMGTxtDataToPng(ic, img_dir);
}

void LAVISION::readRigIMGData(const std::string& img_file_pref, int ic, const std::string& img_dir){

	std::string img_file_suff=".im7";
	std::string res_file_pref="rigimg_file";
	
	auto   imgdir_txt=img_dir+"/rigtxtimg/";
    if(!boost::filesystem::exists(imgdir_txt)){
        boost::filesystem::create_directories(imgdir_txt);
    }

	for(int it=1; it<=4; it++){
		std::string img_file=str(boost::format("%1$s%2$05d%3$s") % img_file_pref % it % img_file_suff);
		std::string res_file=str(boost::format("%1$s/%2$s%3$05dcam%4%") % imgdir_txt % res_file_pref % it % ic);
		this->readIM7XFile(img_file, res_file);
	}
}

void LAVISION::writeRigIMGTxtDataToPng(int ic, const std::string& img_dir){ 

	std::string line;
	cv::Mat_<float> img;
	
	int n_width, n_height, i_cam; 
	float x;

	auto   imgdir_txt=img_dir+"/rigtxtimg/";
	std::string res_file_pref="rigimg_file";

	auto   rigimgdir=img_dir+"/rigimg/";
    if(!boost::filesystem::exists(rigimgdir)){
        boost::filesystem::create_directories(rigimgdir);
    }

	for(int it_rec=1; it_rec<=4; it_rec++ ){

		std::string res_file=str(boost::format("%1$s/%2$s%3$05dcam%4%cam0.txt") % imgdir_txt % res_file_pref % it_rec % ic);

		std::ifstream  resFile(res_file);
		if(resFile.is_open()){

			std::string firstline;
			std::getline(resFile, firstline);
			std::istringstream isfirstline(firstline);
			
			isfirstline >> n_width >> n_width >> n_height >> i_cam;

			img.create(n_height, n_width);

			int row=0;
			while(std::getline(resFile, line)){

				std::istringstream is(line);

				int col=0;
				while(is >> x){
					img.at<float>(row,col)=x;
					col++;
				}

				row++;
			}

		    std::string fullname=str(boost::format("%1%/cam%2%_%3$03d.bmp") % rigimgdir % ic % it_rec );   
		    cv::imwrite(fullname, img);

			img.release();
		}
		else{
			std::cerr<<"Error opening image data file "<<res_file<<endl;
			std::abort();
		}

		resFile.close();
	}
}

void LAVISION::readOTF(const std::string& otf_file, Camera* cam){

	tinyxml2::XMLDocument MFdoc;
	tinyxml2::XMLError eResult=MFdoc.LoadFile(otf_file.c_str());

	Eigen::Vector4f theta_ijk;

	if(eResult==0){

	    tinyxml2::XMLElement* root = MFdoc.FirstChildElement("root");

	    if(root){
	    	cout<<"read succeeded"<<endl;

			tinyxml2::XMLElement* CSI=root->FirstChildElement("DisparityMapSize");

			CSI=CSI->NextSiblingElement()->NextSiblingElement()->NextSiblingElement()->NextSiblingElement();

			for(int i=0; i<cam->i_cam_; i++)
				CSI=CSI->NextSiblingElement();

			assert(utils::constchar2int(CSI->Attribute("index"))==cam->i_cam_);

			cout<<utils::constchar2int(CSI->Attribute("index"))+1<<" th camera"<<endl;

			tinyxml2::XMLElement* CSII_first=CSI->FirstChildElement("CameraNumber");
			
			assert(cam->theta_.empty());

			for(tinyxml2::XMLElement* subdomain = CSII_first->NextSiblingElement(); subdomain != NULL; subdomain = subdomain->NextSiblingElement()){
				int j=3;
				for(tinyxml2::XMLElement* CSIII=subdomain->LastChildElement("OpticalTransferFunction")->LastChildElement("intensity");
					CSIII != NULL; CSIII=CSIII->PreviousSiblingElement()){
					theta_ijk[j]=utils::constchar2double(CSIII->Attribute("value"));
					if(j==3) 	theta_ijk[j]=theta_ijk[j];
					if(j==2) 	theta_ijk[j]=theta_ijk[j];
					if(j==1)	theta_ijk[j]=0; 
					if(j==0) 	theta_ijk[j]=theta_ijk[j];
					j--;
					if(j<0) break;
				}
				cam->theta_.push_back(theta_ijk);
			}

			tinyxml2::XMLElement* CSII_last=CSI->LastChildElement("Subvolume");
			int num_subvolume=utils::constchar2int(CSII_last->Attribute("index"))+1;

			int nX, nY, nZ;
			tinyxml2::XMLElement* subdomain_size=CSII_last->FirstChildElement("SubVolumePosition")->FirstChildElement("Width");
			nX=utils::constchar2int(subdomain_size->Attribute("value"))+1;
			subdomain_size=subdomain_size->NextSiblingElement();
			nY=utils::constchar2int(subdomain_size->Attribute("value"))+1;
			subdomain_size=subdomain_size->NextSiblingElement();
			nZ=utils::constchar2int(subdomain_size->Attribute("value"))+1;

			assert(nX*nY*nZ==num_subvolume);
			cam->nXcPSF_=cv::Point3i(nX, nY, nZ);
			cout<<"cam->nXcPSF_ "<<cam->nXcPSF_<<endl;
	    }
	    else
	    	cout<<"failed"<<endl;
	}
	else
		cout<<"open failed"<<endl;
}

void LAVISION::readPinholeMF(const std::string& mf_file, Camera* cam){

	tinyxml2::XMLDocument MFdoc;
	tinyxml2::XMLError eResult=MFdoc.LoadFile(mf_file.c_str());

	if(eResult==0){
	    auto root = MFdoc.FirstChildElement("Calibration");

	    if(root){
	    	cout<<"read succeeded"<<endl;

			auto CSI=root->FirstChildElement("CoordinateSystemsForEachView")
											->FirstChildElement("CoordinateSystem")
											->NextSiblingElement()
											->FirstChildElement("CoordinateMapper");

			for(int i=0; i<cam->i_cam_; i++)
				CSI=CSI->NextSiblingElement();

			assert(cam->i_cam_+1==utils::constchar2int(CSI->Attribute("CameraIdentifier")));

			cout<<CSI->Attribute("CameraIdentifier")<<" th camera"<<endl;

			std::string mf_type = CSI->Attribute("Type");
			if(mf_type.compare("PinholeOpenCV")==0){
				auto CSII=CSI->FirstChildElement("PinholeParameters")
								->FirstChildElement("CommonParameters");

				auto CSIV=CSII->FirstChildElement("FitError")->NextSiblingElement()->NextSiblingElement();		
				cam->n_width_mask_ph_=utils::constchar2int(CSIV->Attribute("Width"));
				cam->n_height_mask_ph_=utils::constchar2int(CSIV->Attribute("Height"));

				CSIV=CSIV->NextSiblingElement()->NextSiblingElement();
				cam->PixelPerMmFactor_ph_=utils::constchar2double(CSIV->Attribute("Value"));

				CSII=CSII->NextSiblingElement();
				CSIV=CSII->FirstChildElement("SensorPixelSizeMm");
				cam->k_=1./utils::constchar2double(CSIV->Attribute("Value"));
				cam->l_=cam->k_;

				CSIV=CSIV->NextSiblingElement();
				cam->f_=utils::constchar2double(CSIV->Attribute("x"));

				CSIV=CSIV->NextSiblingElement()->NextSiblingElement();
				double k1 = utils::constchar2double(CSIV->Attribute("radialDistortionCoefficient1"));
				double k2 = utils::constchar2double(CSIV->Attribute("radialDistortionCoefficient2"));
				double k3 = utils::constchar2double(CSIV->Attribute("radialDistortionCoefficient3"));
				double k4 = utils::constchar2double(CSIV->Attribute("radialDistortionCoefficient4"));
				double k5 = utils::constchar2double(CSIV->Attribute("radialDistortionCoefficient5"));
				double k6 = utils::constchar2double(CSIV->Attribute("radialDistortionCoefficient6"));

				CSIV=CSIV->NextSiblingElement();
				double p1 = utils::constchar2double(CSIV->Attribute("tangentialDistortionCoefficient1"));
				double p2 = utils::constchar2double(CSIV->Attribute("tangentialDistortionCoefficient2"));

				cam->distCoeffs_ = (cv::Mat_<double>(4, 1)<< k1, k2, p1, p2);

				CSIV=CSIV->NextSiblingElement();
				double principalPoint_x=utils::constchar2double(CSIV->Attribute("x"));
				double principalPoint_y=utils::constchar2double(CSIV->Attribute("y"));

				cam->cameraMatrix_ = cv::Mat::eye(3, 3, CV_64F);

			    cam->cameraMatrix_.at<double>(0,0)=cam->k_*cam->f_;
			    cam->cameraMatrix_.at<double>(1,1)=cam->l_*cam->f_;
			    cam->cameraMatrix_.at<double>(0,2)=principalPoint_x;
			    cam->cameraMatrix_.at<double>(1,2)=principalPoint_y;

				CSII=CSII->NextSiblingElement();
				CSIV=CSII->FirstChildElement("TranslationMm");
				cam->T_=(cv::Mat_<double>(3,1)<< utils::constchar2double(CSIV->Attribute("Tx")),
												 utils::constchar2double(CSIV->Attribute("Ty")),
												 utils::constchar2double(CSIV->Attribute("Tz")));

				CSIV=CSIV->NextSiblingElement();
				cam->R_=(cv::Mat_<double>(3,1)<< utils::constchar2double(CSIV->Attribute("Rx")),
												 utils::constchar2double(CSIV->Attribute("Ry")),
												 utils::constchar2double(CSIV->Attribute("Rz")));

				// CSII=CSII->NextSiblingElement()->NextSiblingElement();
				// CSIV=CSII->FirstChildElement("LinearScaleX");
			}
		}
		else
	    	cout<<"failed"<<endl;
	}
	else
		cout<<"failed open"<<endl;
}

void LAVISION::readPolynomialMF(const std::string& mf_file, Camera* cam){

	tinyxml2::XMLDocument MFdoc;
	tinyxml2::XMLError eResult=MFdoc.LoadFile(mf_file.c_str());

	if(eResult==0){
	    auto root = MFdoc.FirstChildElement("Calibration");

	    if(root){
	    	cout<<"read succeeded"<<endl;

			auto CSI=root->FirstChildElement("CoordinateSystemsForEachView")->FirstChildElement("CoordinateSystem")
											->FirstChildElement("CoordinateMapper");

			for(int i=0; i<cam->i_cam_; i++)
				CSI=CSI->NextSiblingElement();

			assert(cam->i_cam_+1==utils::constchar2int(CSI->Attribute("CameraIdentifier")));

			cout<<CSI->Attribute("CameraIdentifier")<<" th camera"<<endl;

			std::string mf_type = CSI->Attribute("Type");
			if(mf_type.compare("Polynomial3rdOrder")==0){
				auto CSII=CSI->FirstChildElement("PolynomialParameters")
											->FirstChildElement("PolynomialModelSubType")
											->NextSiblingElement();
				
				auto CSIV=CSII->FirstChildElement("FitError")->NextSiblingElement();
				std::string right_handed = CSIV->Attribute("Value");
				// cam->Right_handed_flag_=(right_handed.compare(RightHanded)==0);

				CSIV=CSIV->NextSiblingElement();		
				cam->n_width_mask_=utils::constchar2int(CSIV->Attribute("Width"));
				cam->n_height_mask_=utils::constchar2int(CSIV->Attribute("Height"));

				CSIV=CSIV->NextSiblingElement();
				cam->n_width_=utils::constchar2int(CSIV->Attribute("Width"));
				cam->n_height_=utils::constchar2int(CSIV->Attribute("Height"));

				CSIV=CSIV->NextSiblingElement();
				cam->PixelPerMmFactor_=utils::constchar2double(CSIV->Attribute("Value"));

				CSII=CSII->NextSiblingElement()
						->NextSiblingElement()
						->NextSiblingElement()
						->NextSiblingElement();
				
				assert(cam->ZPosZ_.empty());
				assert(cam->aZ_.empty());
				assert(cam->bZ_.empty());

				int nZ=0;
				VectorXd a,b;
				a.resize(cam->n_coef_);
				b.resize(cam->n_coef_);

				for(auto planeZ=CSII; planeZ != NULL; planeZ=planeZ->NextSiblingElement()){
					auto CSIII=planeZ->FirstChildElement("ZPosition");
					cam->ZPosZ_.push_back(utils::constchar2double(CSIII->Attribute("Value")));

					CSIII=CSIII->NextSiblingElement();
					cam->originZ_.push_back( cv::Point2d( utils::constchar2double(CSIII->Attribute("s_o")),
											 			  utils::constchar2double(CSIII->Attribute("t_o")) ) );

					CSIII=CSIII->NextSiblingElement();
					cam->normalizationFactorZ_.push_back( cv::Point2d( utils::constchar2double(CSIII->Attribute("nx")),
											 			 			   utils::constchar2double(CSIII->Attribute("ny")) ) );

					CSIII=CSIII->NextSiblingElement()->FirstChildElement("CoefficientsA");

					a[0]=utils::constchar2double(CSIII->Attribute("a_o"));
					a[1]=utils::constchar2double(CSIII->Attribute("a_s"));
					a[2]=utils::constchar2double(CSIII->Attribute("a_s2"));
					a[3]=utils::constchar2double(CSIII->Attribute("a_s3"));
					a[4]=utils::constchar2double(CSIII->Attribute("a_t"));
					a[5]=utils::constchar2double(CSIII->Attribute("a_t2"));
					a[6]=utils::constchar2double(CSIII->Attribute("a_t3"));
					a[7]=utils::constchar2double(CSIII->Attribute("a_st"));
					a[8]=utils::constchar2double(CSIII->Attribute("a_s2t"));
					a[9]=utils::constchar2double(CSIII->Attribute("a_st2"));

					CSIII=CSIII->NextSiblingElement();

					b[0]=utils::constchar2double(CSIII->Attribute("b_o"));
					b[1]=utils::constchar2double(CSIII->Attribute("b_s"));
					b[2]=utils::constchar2double(CSIII->Attribute("b_s2"));
					b[3]=utils::constchar2double(CSIII->Attribute("b_s3"));
					b[4]=utils::constchar2double(CSIII->Attribute("b_t"));
					b[5]=utils::constchar2double(CSIII->Attribute("b_t2"));
					b[6]=utils::constchar2double(CSIII->Attribute("b_t3"));
					b[7]=utils::constchar2double(CSIII->Attribute("b_st"));
					b[8]=utils::constchar2double(CSIII->Attribute("b_s2t"));
					b[9]=utils::constchar2double(CSIII->Attribute("b_st2"));

					cam->aZ_.push_back(a);
					cam->bZ_.push_back(b);

					nZ++;
				}
				cam->nZ_=nZ;
				cout<<"We have "<<cam->nZ_<<" planes model"<<endl;

				assert(cam->aZ_.size()==cam->nZ_);
				assert(cam->bZ_.size()==cam->nZ_);
			}
	    }
	    else
	    	cout<<"failed"<<endl;
	}
	else
		cout<<"failed open"<<endl;
}

void LAVISION::readMarkPositionTable(const std::string& calib_file, int nViews,
									std::vector<std::vector<std::vector<cv::Point3d>>>& Xcoord_obj_cams,
									std::vector<std::vector<std::vector<cv::Point2d>>>& xcoord_img, int nx, int ny){

	tinyxml2::XMLDocument MFdoc;
	tinyxml2::XMLError eResult=MFdoc.LoadFile(calib_file.c_str());

	if(eResult==0){

	    auto root = MFdoc.FirstChildElement("MarkTable");

	    if(root){
	    	cout<<"read succeeded"<<endl;

			for(auto CSI=root->FirstChildElement("Camera"); CSI != NULL; CSI=CSI->NextSiblingElement()){
				std::vector<std::vector<cv::Point3d>> Xcoord_per_cam;
				std::vector<std::vector<cv::Point2d>> xcoord_per_cam;
				double view1=-1;
				for(auto CSII=CSI->FirstChildElement("View"); CSII != NULL; CSII=CSII->NextSiblingElement()){
					std::vector<cv::Point3d> Xcoord_per_view1, Xcoord_per_view2;
					std::vector<cv::Point2d> xcoord_per_view1, xcoord_per_view2;
					for(auto CSIII=CSII->FirstChildElement("Mark"); CSIII != NULL; CSIII=CSIII->NextSiblingElement()){
						auto CSIV=CSIII->LastChildElement("WorldPos");
						auto Xc = cv::Point3d( utils::constchar2double(CSIV->Attribute("x")),
												utils::constchar2double(CSIV->Attribute("y")),
												utils::constchar2double(CSIV->Attribute("z")));

						if(Xcoord_per_view1.empty()) view1 = Xc.z;

						CSIV=CSIV->PreviousSiblingElement();
						auto xc = cv::Point2d( utils::constchar2double(CSIV->Attribute("x")),
												utils::constchar2double(CSIV->Attribute("y")));

						if(Xc.z==view1){
							Xcoord_per_view1.push_back(Xc);
							xcoord_per_view1.push_back(xc);
						}
						else{
							Xcoord_per_view2.push_back(Xc);
							xcoord_per_view2.push_back(xc);
						}
					}
					Xcoord_per_cam.push_back(Xcoord_per_view1);
					Xcoord_per_cam.push_back(Xcoord_per_view2);
					xcoord_per_cam.push_back(xcoord_per_view1);
					xcoord_per_cam.push_back(xcoord_per_view2);
				}
				Xcoord_obj_cams.push_back(Xcoord_per_cam);
				xcoord_img.push_back(xcoord_per_cam);
			}
	    }
	    else
	    	cout<<"failed"<<endl;
	}
	else
		cout<<"open failed"<<endl;
}

