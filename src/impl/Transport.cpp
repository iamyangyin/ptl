#include "../Transport.hpp"

Transport::Transport(){}

Transport::~Transport(){}

/// recexe version

Transport::Transport(bool stationary_flag, VelocityInterpolation_Type velocity_to_scatter_interp_type, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, const cv::Point3i& nXc_data,
					 TimeScheme_Type time_scheme, double dt, double dtDyn, double dtObs, double tDeb, int nSnapshots, double E_sd, double E_max, double E_min)
					: stationary_flag_(stationary_flag), velocity_to_scatter_interp_type_(velocity_to_scatter_interp_type), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), nXc_(nXc_data), 
						time_scheme_(time_scheme), dt_(dt), dtDyn_(dtDyn), dtObs_(dtObs), tDeb_(tDeb), nSnapshots_(nSnapshots), E_sd_(E_sd), E_max_(E_max), E_min_(E_min){
	tTotal_=(nSnapshots_-1)*dtObs_;
	nVelocityFiles_=std::round(tTotal_/dtDyn_)+1;
}

/// optexe version

Transport::Transport(bool stationary_flag, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, const cv::Point3i& nXc)
                    : stationary_flag_(stationary_flag), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), nXc_(nXc){}

/// lapivexe version

Transport::Transport(bool stationary_flag, VelocityInterpolation_Type velocity_to_scatter_interp_type, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, const cv::Point3i& nXc,
					 TimeScheme_Type time_scheme, double dt, double dtObs, VelocityInterpolation_Type velocity_warping_type)
					: stationary_flag_(stationary_flag), velocity_to_scatter_interp_type_(velocity_to_scatter_interp_type), Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), nXc_(nXc), 
						time_scheme_(time_scheme), dt_(dt), dtObs_(dtObs), velocity_warping_type_(velocity_warping_type){}

void Transport::setRandomParticles(int n_part, const cv::Point3d& dXc_px){

	int n_generated=0;
	while(n_generated < n_part){

		auto Xcpx=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		auto Ycpx=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
		auto Zcpx=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);

		auto valid_flag=true;
		for(const auto& pc: particles_coords_){
			if(std::abs(pc.x-Xcpx)<dXc_px.x && std::abs(pc.y-Ycpx)<dXc_px.y && std::abs(pc.z-Zcpx)<dXc_px.z){
				valid_flag=false;
				cout<<"Regenerate! ";
			}
		}

		if(valid_flag){
			n_generated++;
			particles_coords_.push_back( cv::Point3d(Xcpx, Ycpx, Zcpx) );
		}

		// auto it = std::find_if(particles_coords_.begin(), particles_coords_.end(), 
		// 				[=](auto& pc){ return std::abs(pc.x-Xcpx)<dXc_px.x &&
		// 									  std::abs(pc.y-Ycpx)<dXc_px.y &&
		// 									  std::abs(pc.z-Zcpx)<dXc_px.z; });

		// if(it!=particles_coords_.end()){
		// 	cout<<"Regenerate! ";
		// }
		// else{
		// 	n_generated++;
		// 	particles_coords_.push_back( cv::Point3d(Xcpx, Ycpx, Zcpx) );
		// }

		cout<<"n_generated "<<n_generated<<endl;
	}

	n_particles_=particles_coords_.size();
	cout<<"particle numbers: "<<n_particles_<<endl;
}

void Transport::saveReferenceDataNPZ(const std::string& obsdir){

	std::string refdir(obsdir+"/ref");
    if(!boost::filesystem::exists(refdir))
        boost::filesystem::create_directories(refdir);

    for(int it=1; it<=nSnapshots_; it++){
		auto refPhaseFile=str(boost::format("%1%/ref_it%2$03d.npz") % refdir % it );
		utils::saveFrameDataNPZ(it, refPhaseFile, particles_ref_);
    }
}

void Transport::predictParticlePositionFieldWithKnownVelocity(const std::vector<Particle_ptr>& particles_ana_in,
																const cv::Point3d& velo, int it_seq_cnt) const{

    for(const auto& p_in : particles_ana_in){
        if(p_in->is_ontrack() && !p_in->Xcoord().empty()){
            auto pc = p_in->getPreviousXcoord(it_seq_cnt);

            auto pc_n = pc + dtObs_*velo;
            utils::maintainPositionInRange(pc_n, Xc_top_, Xc_bottom_);
			
			p_in->setXcoordElement(pc_n, it_seq_cnt);
	 		p_in->setit_seq_fin(it_seq_cnt);
        }
    }
}

void Transport::predictParticlePositionField(const std::vector<Particle_ptr>& particles_ana_in, 
											int it_seq_cnt, bool repredict_flag) const{

	for(const auto& p_in : particles_ana_in){
	    if(p_in->is_ontrack() && !p_in->Xcoord().empty()){
			auto pc = p_in->getPreviousXcoord(it_seq_cnt);

			for(int i=0; i<static_cast<int>(std::round(dtObs_/dt_)); i++){
				auto pc_n = this->integrateDtUsingEulerianVelocityFields(pc);
				if( std::abs(pc_n.x-pc.x) > (Xc_top_.x-Xc_bottom_.x)/2 ||
					std::abs(pc_n.y-pc.y) > (Xc_top_.y-Xc_bottom_.y)/2 ||
					std::abs(pc_n.z-pc.z) > (Xc_top_.z-Xc_bottom_.z)/2 )
					break;
				else
					pc = pc_n;
			}
            utils::maintainPositionInRange(pc, Xc_top_, Xc_bottom_);

			if(repredict_flag) p_in->setXcoordElement(pc, it_seq_cnt);
			else               p_in->push_backXcoordElement(pc, it_seq_cnt);
	 		p_in->setit_seq_fin(it_seq_cnt);
		}
	}
}

void Transport::predictSyntheticParticlePositionFieldFromScratch(const std::string& DataFile, 
																 int starting_index){

	double tFin=tDeb_+tTotal_;
	double tRatio=std::round(dtDyn_/dt_);
	double n_tVec=std::round(tFin/dt_)+1;
	int    itdeb=static_cast<int>(std::round(tDeb_/dt_));

	std::string time_scheme_str = (time_scheme_==Transport::TimeScheme_Type::Euler ? "Euler" : "Heun");

	cout<<str(boost::format("Transport particles from time 0 to time %1$5d.") % tFin)<<endl;
	cout<<str(boost::format("with time scheme %1% and time step %2$.5f.")    % time_scheme_str % dt_)<<endl;
	cout<<str(boost::format("The observation starts from %1% and is taken every %2$.5f.")   % tDeb_ % dtObs_)<<endl;
	cout<<str(boost::format("with time separation %1$.5f and dynamic time separation %2$.5f.") % dtObs_ % dtDyn_)<<endl;
	cout<<str(boost::format("amounts total %1$5d. snapshots") % nSnapshots_)<<endl;

	VectorXd tVec=VectorXd::LinSpaced(n_tVec, 0.0, tFin);

	int 					 n_part=n_particles_;
	int 					 time_level, time_level_pre=-1;
	int 					 it_seq_fin=1;

	if(stationary_flag_){
		cout<<"read stationary velocity field"<<endl;
		this->readVelocityFieldsFromTXTFile(DataFile, true);
		this->setVelocityOnGrids(velocity_fields_);
	}

	for(int i=0; i<tVec.size()-1; i++){

		if(!stationary_flag_){
			int  pos=DataFile.find(".txt");
			auto DataFilePrefix=DataFile.substr(0,pos);

			time_level=i/tRatio;
			if(time_level!=time_level_pre){
				cout<<"read velocity field at time_level "<<time_level<<endl;
				std::string velocityFile=str(boost::format("%1%%2$04d.txt") % DataFilePrefix % time_level );
				velocity_fields_.clear();

				this->readVelocityFieldsFromTXTFile(velocityFile, time_level==starting_index);
				this->setVelocityOnGrids(velocity_fields_);
				time_level_pre=time_level;
			}
		}

		if(velocity_to_scatter_interp_type_==Transport::VelocityInterpolation_Type::TriCubic) this->computeVelocityDerivatives();

		std::vector<cv::Point3d> particles_coords_next;
		for(const auto& pc: particles_coords_){
			particles_coords_next.push_back( this->integrateDtUsingEulerianVelocityFields(pc) );
		}

		if(i>=itdeb){

			if(i==itdeb){
				for(int p=0; p<n_particles_; p++){
					particles_ref_.push_back( std::make_unique<Particle>( p, it_seq_fin, it_seq_fin, std::vector<cv::Point3d>{particles_coords_[p]},
																									 std::vector<double>{this->assignIntensity(particles_coords_[p].z)} ) );
				}
			}

            for(int p=0; p<n_part; p++){
                if(particles_ref_[p]->is_ontrack()){
                    if( std::abs(particles_coords_next[p].x-particles_ref_[p]->Xcoord().back().x) > (Xc_top_.x-Xc_bottom_.x)/2 ||
                        std::abs(particles_coords_next[p].y-particles_ref_[p]->Xcoord().back().y) > (Xc_top_.y-Xc_bottom_.y)/2 ||
                        std::abs(particles_coords_next[p].z-particles_ref_[p]->Xcoord().back().z) > (Xc_top_.z-Xc_bottom_.z)/2 ){

                        particles_ref_[p]->deleteData(false);

                        particles_coords_next.push_back(particles_coords_next[p]);
                        
                        particles_ref_.push_back( std::make_unique<Particle>( particles_ref_.back()->ID()+1, it_seq_fin+1, it_seq_fin+1 ) );
                        n_particles_++;
                    }
                }
            }
 		    utils::showInfoOnDeletedParticles();

			if(std::abs(std::remainder(tVec(i+1), dtObs_))<=1e-6){
				it_seq_fin++;
				for(int p=0; p<n_particles_; p++){
					if(particles_ref_[p]->is_ontrack()){
						particles_ref_[p]->push_backXcoordElement(particles_coords_next[p], it_seq_fin);
						particles_ref_[p]->push_backEvElement(this->assignIntensity(particles_coords_next[p].z), it_seq_fin);
						particles_ref_[p]->setit_seq_fin(it_seq_fin);
					}
				}
                n_part=n_particles_;
			}
    
            particles_coords_=particles_coords_next;
		}
	
	}
	cout<<"After prediction, particle numbers "<<n_particles_<<endl;
}

cv::Point3d Transport::interpolateVelocityFieldOnParticleCoordinates(const cv::Point3d& pc) const{

	cv::Point3d velo;
	switch (velocity_to_scatter_interp_type_){
		case Transport::VelocityInterpolation_Type::TriLinear:
			velo = trilinearInterpolate(pc);
			break;
		case Transport::VelocityInterpolation_Type::AGW:
			velo = AGWInterpolate(pc);
			break;
		case Transport::VelocityInterpolation_Type::TriCubic:
			velo = trilinearInterpolate(pc);
			break;
		default:
	    	cout<<"other interpolation scheme not supported!!!"<<endl; 
	    	std::abort();
			break;
	}
	return velo;
}

cv::Point3d Transport::integrateDtUsingEulerianVelocityFields(const cv::Point3d& pc) const{

	cv::Point3d pc_n;
    switch (time_scheme_){           
        case Transport::TimeScheme_Type::Euler:           
			pc_n = this->doEulerScheme(pc);
        	break;
        case Transport::TimeScheme_Type::Heun:            
			pc_n = this->doHeunScheme(pc);
        	break;
    }
	return pc_n;
}

cv::Point3d Transport::doEulerScheme(const cv::Point3d& pc) const{

	auto velo = this->interpolateVelocityFieldOnParticleCoordinates(pc);
	if(isnan(velo.x) || isnan(velo.y) || isnan(velo.z)) cout<<"velo "<<velo<<endl;

	auto pc_n = pc + dt_*velo;
	this->imposeBoundaryCondition(pc_n);

	return pc_n;
}

cv::Point3d Transport::doHeunScheme(const cv::Point3d& pc) const{

	auto velo = this->interpolateVelocityFieldOnParticleCoordinates(pc);
	auto pc_n = pc + dt_*velo;
	this->imposeBoundaryCondition(pc_n);

	auto velo_inter = this->interpolateVelocityFieldOnParticleCoordinates(pc_n);
	pc_n = pc + dt_*0.5*(velo+velo_inter);
	this->imposeBoundaryCondition(pc_n);

	return pc_n;
}

void Transport::imposeBoundaryCondition(cv::Point3d& pc_n) const{

	if(pc_n.x > Xc_top_.x){
		pc_n.x -= Xc_top_.x-Xc_bottom_.x;
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}
	else if(pc_n.x < Xc_bottom_.x){
		pc_n.x += Xc_top_.x-Xc_bottom_.x;
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}

	if(pc_n.y > Xc_top_.y){
		pc_n.y -= Xc_top_.y-Xc_bottom_.y;
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}
	else if(pc_n.y < Xc_bottom_.y){
		pc_n.y += Xc_top_.y-Xc_bottom_.y;
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.z=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
	}

	if(pc_n.z > Xc_top_.z){
		pc_n.z -= Xc_top_.z-Xc_bottom_.z;
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
	}
	else if(pc_n.z < Xc_bottom_.z){
		pc_n.z += Xc_top_.z-Xc_bottom_.z;	
		pc_n.x=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
		pc_n.y=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
	}

	assert(pc_n.x >= Xc_bottom_.x && pc_n.x <= Xc_top_.x);
	assert(pc_n.y >= Xc_bottom_.y && pc_n.y <= Xc_top_.y);
	assert(pc_n.z >= Xc_bottom_.z && pc_n.z <= Xc_top_.z);
}

double Transport::assignIntensity(double Z){
	double Zt=Xc_top_.z;
	double Zb=Xc_bottom_.z;
	double Zm=(Zt+Zb)/2.;

	double E = E_min_*(Z-Zm)*(Z-Zt)/((Zb-Zm)*(Zb-Zt))
				+E_max_*(Z-Zb)*(Z-Zt)/((Zm-Zb)*(Zm-Zt))
				+E_min_*(Z-Zb)*(Z-Zm)/((Zt-Zb)*(Zt-Zm));

	return std::min( E_max_+E_sd_*3., 
				std::max(0., E+E_sd_*Random::StandardNormalRandomNumber(0.0))+E_sd_*Random::StandardNormalRandomNumber(0.0) );
}

cv::Point3d Transport::AGWInterpolate(const cv::Point3d& pc) const{

	double r_sq_to_scattered = 1.5*cv::norm(dXc_);

	double xl,xr,yl,yr,zl,zr;
	int xl_idx,xr_idx,yl_idx,yr_idx,zl_idx,zr_idx;

	this->findNeighbors("X", pc.x, xl_idx, xr_idx, xl, xr);
	this->findNeighbors("Y", pc.y, yl_idx, yr_idx, yl, yr);
	this->findNeighbors("Z", pc.z, zl_idx, zr_idx, zl, zr);

	std::vector<cv::Point3d> pts_idx{cv::Point3d(xl_idx, yl_idx, zl_idx),
									 cv::Point3d(xl_idx, yl_idx, zr_idx),
									 cv::Point3d(xl_idx, yr_idx, zl_idx),
									 cv::Point3d(xl_idx, yr_idx, zr_idx),
									 cv::Point3d(xr_idx, yl_idx, zl_idx),
									 cv::Point3d(xr_idx, yl_idx, zr_idx),
									 cv::Point3d(xr_idx, yr_idx, zl_idx),
									 cv::Point3d(xr_idx, yr_idx, zr_idx)};

	double sum_alpha=0;
    cv::Point3d sum_velo(0., 0., 0.);

	for(auto& pt_idx: pts_idx){
	    auto alpha=std::exp( -std::pow(cv::norm( pc - cv::Point3d(xgrid_[pt_idx.x], ygrid_[pt_idx.y], zgrid_[pt_idx.z]) ), 2)/r_sq_to_scattered );
        sum_velo+=alpha*cv::Point3d( U_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x), V_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x), W_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x) );
        sum_alpha+=alpha;
	}

	return sum_velo/sum_alpha;
}

void Transport::findNeighbors(const std::string& direction_flag, double pc_x, int& xl_idx, int& xr_idx, double& xl, double& xr) const{

	std::vector<double> xgrid;
	int 				nXc_x=0;
	double 				dXc_x=0;	

	if(direction_flag=="X"){
		xgrid = xgrid_;
		dXc_x = dXc_.x;
		nXc_x = nXc_.x;
	}
	else if(direction_flag=="Y"){
		xgrid = ygrid_;
		dXc_x = dXc_.y;
		nXc_x = nXc_.y;
	}
	else if(direction_flag=="Z"){
		xgrid = zgrid_;
		dXc_x = dXc_.z;
		nXc_x = nXc_.z;
	}

	if(pc_x<xgrid.front())		xl_idx=0;
	else if(pc_x>xgrid.back())	xl_idx=nXc_x-2;
	else{
		auto pxl=std::find_if(std::begin(xgrid), std::end(xgrid), [=](const double x) { return pc_x - x < dXc_x; } );
		if(pxl==std::end(xgrid)-1) pxl--;
		xl_idx=std::distance(std::begin(xgrid), pxl);
	}

	xr_idx=xl_idx+1;
	xl=xgrid[xl_idx];
	xr=xgrid[xr_idx];
}

//http://paulbourke.net/miscellaneous/interpolation/
cv::Point3d Transport::trilinearInterpolate(const cv::Point3d& pc) const{

	double xl,xr,yl,yr,zl,zr;
	int xl_idx,xr_idx,yl_idx,yr_idx,zl_idx,zr_idx;

	this->findNeighbors("X", pc.x, xl_idx, xr_idx, xl, xr);
	this->findNeighbors("Y", pc.y, yl_idx, yr_idx, yl, yr);
	this->findNeighbors("Z", pc.z, zl_idx, zr_idx, zl, zr);

	auto x=(pc.x-xl)/(xr-xl);
	auto y=(pc.y-yl)/(yr-yl);
	auto z=(pc.z-zl)/(zr-zl);

	auto U = U_ongrid3D_[zl_idx](yl_idx,xl_idx)*(1-x)*(1-y)*(1-z)+
			 U_ongrid3D_[zl_idx](yl_idx,xr_idx)*x*(1-y)*(1-z)+
			 U_ongrid3D_[zl_idx](yr_idx,xl_idx)*(1-x)*y*(1-z)+
			 U_ongrid3D_[zr_idx](yl_idx,xl_idx)*(1-x)*(1-y)*z+
			 U_ongrid3D_[zr_idx](yl_idx,xr_idx)*x*(1-y)*z+
			 U_ongrid3D_[zr_idx](yr_idx,xl_idx)*(1-x)*y*z+
			 U_ongrid3D_[zl_idx](yr_idx,xr_idx)*x*y*(1-z)+
			 U_ongrid3D_[zr_idx](yr_idx,xr_idx)*x*y*z;

	auto V = V_ongrid3D_[zl_idx](yl_idx,xl_idx)*(1-x)*(1-y)*(1-z)+
			 V_ongrid3D_[zl_idx](yl_idx,xr_idx)*x*(1-y)*(1-z)+
			 V_ongrid3D_[zl_idx](yr_idx,xl_idx)*(1-x)*y*(1-z)+
			 V_ongrid3D_[zr_idx](yl_idx,xl_idx)*(1-x)*(1-y)*z+
			 V_ongrid3D_[zr_idx](yl_idx,xr_idx)*x*(1-y)*z+
			 V_ongrid3D_[zr_idx](yr_idx,xl_idx)*(1-x)*y*z+
			 V_ongrid3D_[zl_idx](yr_idx,xr_idx)*x*y*(1-z)+
			 V_ongrid3D_[zr_idx](yr_idx,xr_idx)*x*y*z;
	
	auto W = W_ongrid3D_[zl_idx](yl_idx,xl_idx)*(1-x)*(1-y)*(1-z)+
			 W_ongrid3D_[zl_idx](yl_idx,xr_idx)*x*(1-y)*(1-z)+
			 W_ongrid3D_[zl_idx](yr_idx,xl_idx)*(1-x)*y*(1-z)+
			 W_ongrid3D_[zr_idx](yl_idx,xl_idx)*(1-x)*(1-y)*z+
			 W_ongrid3D_[zr_idx](yl_idx,xr_idx)*x*(1-y)*z+
			 W_ongrid3D_[zr_idx](yr_idx,xl_idx)*(1-x)*y*z+
			 W_ongrid3D_[zl_idx](yr_idx,xr_idx)*x*y*(1-z)+
			 W_ongrid3D_[zr_idx](yr_idx,xr_idx)*x*y*z;		 

	return cv::Point3d(U,V,W);
}

void Transport::makeGrids(void){

	dXc_.x=(Xc_top_.x-Xc_bottom_.x)/static_cast<double>(nXc_.x-1);
	dXc_.y=(Xc_top_.y-Xc_bottom_.y)/static_cast<double>(nXc_.y-1);
	dXc_.z=(Xc_top_.z-Xc_bottom_.z)/static_cast<double>(nXc_.z-1);

 	xgrid_.resize(nXc_.x);
    ygrid_.resize(nXc_.y);
    zgrid_.resize(nXc_.z);
    std::iota(xgrid_.begin(), xgrid_.end(), 0);
    std::iota(ygrid_.begin(), ygrid_.end(), 0);
    std::iota(zgrid_.begin(), zgrid_.end(), 0);
 	std::for_each(xgrid_.begin(), xgrid_.end(), [=](double &x){ x=Xc_bottom_.x+dXc_.x*x; });
 	std::for_each(ygrid_.begin(), ygrid_.end(), [=](double &y){ y=Xc_bottom_.y+dXc_.y*y; });
 	std::for_each(zgrid_.begin(), zgrid_.end(), [=](double &z){ z=Xc_bottom_.z+dXc_.z*z; });
}

void Transport::makeBaseCoarseGrid(void){

	dXc_.x=(Xc_top_.x-Xc_bottom_.x)/static_cast<double>(nXc_.x);
	dXc_.y=(Xc_top_.y-Xc_bottom_.y)/static_cast<double>(nXc_.y);
	dXc_.z=(Xc_top_.z-Xc_bottom_.z)/static_cast<double>(nXc_.z);

	dXc_minimal_ = dXc_;

 	xgridc_.resize(nXc_.x+1);
    ygridc_.resize(nXc_.y+1);
    zgridc_.resize(nXc_.z+1);
    std::iota(xgridc_.begin(), xgridc_.end(), 0);
    std::iota(ygridc_.begin(), ygridc_.end(), 0);
    std::iota(zgridc_.begin(), zgridc_.end(), 0);
 	std::for_each(xgridc_.begin(), xgridc_.end(), [=](double &x){ x=Xc_bottom_.x+dXc_.x*x; });
 	std::for_each(ygridc_.begin(), ygridc_.end(), [=](double &y){ y=Xc_bottom_.y+dXc_.y*y; });
 	std::for_each(zgridc_.begin(), zgridc_.end(), [=](double &z){ z=Xc_bottom_.z+dXc_.z*z; });

 	xgrid_.resize(nXc_.x);
    ygrid_.resize(nXc_.y);
    zgrid_.resize(nXc_.z);
    std::iota(xgrid_.begin(), xgrid_.end(), 0);
    std::iota(ygrid_.begin(), ygrid_.end(), 0);
    std::iota(zgrid_.begin(), zgrid_.end(), 0);
 	std::for_each(xgrid_.begin(), xgrid_.end(), [=](double &x){ x=xgridc_[0]+dXc_.x*(x+0.5); });
 	std::for_each(ygrid_.begin(), ygrid_.end(), [=](double &y){ y=ygridc_[0]+dXc_.y*(y+0.5); });
 	std::for_each(zgrid_.begin(), zgrid_.end(), [=](double &z){ z=zgridc_[0]+dXc_.z*(z+0.5); });
}

bool Transport::makeCoarseToFineGrid(double downsampling_ratio, int max_nXc_x,
									 std::vector<cv::Point3d>& velocity_fields){

	//make finer grid based on coarser grid
	auto dXc_fine = dXc_*downsampling_ratio;
	auto nXc_fine = cv::Point3i(static_cast<int>( std::round( (Xc_top_.x-Xc_bottom_.x)/dXc_fine.x ) ), 
								static_cast<int>( std::round( (Xc_top_.y-Xc_bottom_.y)/dXc_fine.y ) ),
								static_cast<int>( std::round( (Xc_top_.z-Xc_bottom_.z)/dXc_fine.z ) ));

	if(nXc_fine.x <= max_nXc_x){

		if(!velocity_fields.empty()) velocity_fields.clear();

		std::vector<double> xgrid_fine(nXc_fine.x+1),
							ygrid_fine(nXc_fine.y+1),
							zgrid_fine(nXc_fine.z+1),
							xgridc_fine(nXc_fine.x),
							ygridc_fine(nXc_fine.y),
							zgridc_fine(nXc_fine.z);

		xgridc_fine[0]=Xc_bottom_.x;
		ygridc_fine[0]=Xc_bottom_.y;
		zgridc_fine[0]=Xc_bottom_.z;

		for(int i=1; i<xgridc_fine.size(); i++)		xgridc_fine[i]=xgridc_fine[i-1]+dXc_fine.x;
		for(int i=1; i<ygridc_fine.size(); i++)		ygridc_fine[i]=ygridc_fine[i-1]+dXc_fine.y;
		for(int i=1; i<zgridc_fine.size(); i++)		zgridc_fine[i]=zgridc_fine[i-1]+dXc_fine.z;

		xgrid_fine[0]=xgridc_fine[0]+dXc_fine.x/2.;
		ygrid_fine[0]=ygridc_fine[0]+dXc_fine.y/2.;
		zgrid_fine[0]=zgridc_fine[0]+dXc_fine.z/2.;

		for(int i=1; i<xgrid_fine.size(); i++)		xgrid_fine[i]=xgrid_fine[i-1]+dXc_fine.x;
		for(int i=1; i<ygrid_fine.size(); i++)		ygrid_fine[i]=ygrid_fine[i-1]+dXc_fine.y;
		for(int i=1; i<zgrid_fine.size(); i++)		zgrid_fine[i]=zgrid_fine[i-1]+dXc_fine.z;

		if(velocity_warping_type_==Transport::VelocityInterpolation_Type::TriCubic) this->computeVelocityDerivatives();

		for(int k=0; k<nXc_fine.z; k++){
			for(int j=0; j<nXc_fine.y; j++){
				for(int i=0; i<nXc_fine.x; i++){
                    cv::Point3d velo;
                    if(velocity_warping_type_==Transport::VelocityInterpolation_Type::TriLinear){
					    velo = this->trilinearInterpolate(cv::Point3d(xgrid_fine[i], 
																	  ygrid_fine[j], 
																	  zgrid_fine[k]));
                    }
                    else if(velocity_warping_type_==Transport::VelocityInterpolation_Type::AGW){
    					velo =  this->AGWInterpolate(cv::Point3d(xgrid_fine[i], 
    															  ygrid_fine[j], 
    															  zgrid_fine[k]));
                    }
                    else if(velocity_warping_type_==Transport::VelocityInterpolation_Type::TriCubic){
    				    velo =  this->tricubicInterpolate(cv::Point3d(xgrid_fine[i], 
																	  ygrid_fine[j], 
																	  zgrid_fine[k]));
                    }
					velocity_fields.push_back( velo );
				}
			}
		}

		nXc_ = nXc_fine;
		dXc_ = dXc_fine;
		dXc_minimal_ = dXc_fine;

		xgrid_ = xgrid_fine;
		ygrid_ = ygrid_fine;
		zgrid_ = zgrid_fine;

		xgridc_ = xgridc_fine;
		ygridc_ = ygridc_fine;
		zgridc_ = zgridc_fine;

	    cout<<"grid finer"<<endl
	    	<<"nXc_    "<<nXc_<<endl;
		cout<<"xgrid_ ";
		for(auto x : xgrid_)  cout<<x<<" ";
		cout<<endl;
		cout<<"ygrid_ ";
		for(auto x : ygrid_)  cout<<x<<" ";
		cout<<endl;
		cout<<"zgrid_ ";
		for(auto x : zgrid_)  cout<<x<<" ";
		cout<<endl;
		cout<<"xgridc_ ";
		for(auto x : xgridc_) cout<<x<<" ";
		cout<<endl;
		cout<<"ygridc_ ";
		for(auto x : ygridc_) cout<<x<<" ";
		cout<<endl;
		cout<<"zgridc_ ";
		for(auto x : zgridc_) cout<<x<<" ";
		cout<<endl;

		return false;
	}
	else
		return true;
}

void Transport::applyMedianFilter(bool blur, bool medianBlur, bool grad_regul, bool GaussianBlur){

	cout<<"Apply Median filter"<<endl;

	int median_kernel_size = 3;
	double alpha = 1;
	int gaussian_kernel_size = 3;
	int blur_kernel_shape = 15;

	std::vector<MatrixXd> U_ongrid3D(U_ongrid3D_), V_ongrid3D(V_ongrid3D_), W_ongrid3D(W_ongrid3D_);

    U_ongrid3D_.clear();
    V_ongrid3D_.clear();
    W_ongrid3D_.clear();

	for(int k=0; k<nXc_.z; k++){

		cv::Mat_<double> U_ongrid2D_cv_in,    V_ongrid2D_cv_in,    W_ongrid2D_cv_in;
		cv::Mat_<float>  U_ongrid2D_cv_float, V_ongrid2D_cv_float, W_ongrid2D_cv_float;
		cv::Mat_<float>  U_ongrid2D_cv_blur,  V_ongrid2D_cv_blur,  W_ongrid2D_cv_blur;
		cv::Mat_<float>  U_ongrid2D_cv_mf,    V_ongrid2D_cv_mf,    W_ongrid2D_cv_mf;
		cv::Mat_<float>  U_ongrid2D_cv_smth,  V_ongrid2D_cv_smth,  W_ongrid2D_cv_smth;
		cv::Mat_<float>  U_ongrid2D_cv_gb,    V_ongrid2D_cv_gb,    W_ongrid2D_cv_gb;
	    cv::Mat_<double> U_ongrid2D_cv_out,   V_ongrid2D_cv_out,   W_ongrid2D_cv_out;
	    MatrixXd 		 U_ongrid2D_out,      V_ongrid2D_out, 	   W_ongrid2D_out;

		//Eigen to CV	
		cv::eigen2cv(U_ongrid3D[k], U_ongrid2D_cv_in);
		cv::eigen2cv(V_ongrid3D[k], V_ongrid2D_cv_in);
		cv::eigen2cv(W_ongrid3D[k], W_ongrid2D_cv_in);

		//CV_64FC1 to CV_32FC1
		U_ongrid2D_cv_in.convertTo(U_ongrid2D_cv_float, CV_32FC1);
		V_ongrid2D_cv_in.convertTo(V_ongrid2D_cv_float, CV_32FC1);
		W_ongrid2D_cv_in.convertTo(W_ongrid2D_cv_float, CV_32FC1);

		if(blur){
			cv::blur(U_ongrid2D_cv_float, U_ongrid2D_cv_blur, cv::Size(blur_kernel_shape, blur_kernel_shape));
			cv::blur(V_ongrid2D_cv_float, V_ongrid2D_cv_blur, cv::Size(blur_kernel_shape, blur_kernel_shape));
			cv::blur(W_ongrid2D_cv_float, W_ongrid2D_cv_blur, cv::Size(blur_kernel_shape, blur_kernel_shape));
		}
		else{
		    U_ongrid2D_cv_blur = U_ongrid2D_cv_float.clone();
		    V_ongrid2D_cv_blur = V_ongrid2D_cv_float.clone();
		    W_ongrid2D_cv_blur = W_ongrid2D_cv_float.clone();
		}

		if(medianBlur){
		    cv::medianBlur(U_ongrid2D_cv_blur, U_ongrid2D_cv_mf, median_kernel_size);
		    cv::medianBlur(V_ongrid2D_cv_blur, V_ongrid2D_cv_mf, median_kernel_size);
		    cv::medianBlur(W_ongrid2D_cv_blur, W_ongrid2D_cv_mf, median_kernel_size);
		}
		else{
		    U_ongrid2D_cv_mf = U_ongrid2D_cv_blur.clone();
		    V_ongrid2D_cv_mf = V_ongrid2D_cv_blur.clone();
		    W_ongrid2D_cv_mf = W_ongrid2D_cv_blur.clone();
		}

		if(grad_regul){
		    U_ongrid2D_cv_smth = U_ongrid2D_cv_float + 4*alpha/(1+4*alpha)*(U_ongrid2D_cv_float- U_ongrid2D_cv_mf);
		    V_ongrid2D_cv_smth = V_ongrid2D_cv_float + 4*alpha/(1+4*alpha)*(V_ongrid2D_cv_float- V_ongrid2D_cv_mf);
		    W_ongrid2D_cv_smth = W_ongrid2D_cv_float + 4*alpha/(1+4*alpha)*(W_ongrid2D_cv_float- W_ongrid2D_cv_mf);
		}
	    else{
		    U_ongrid2D_cv_smth = U_ongrid2D_cv_mf.clone();
		    V_ongrid2D_cv_smth = V_ongrid2D_cv_mf.clone();
		    W_ongrid2D_cv_smth = W_ongrid2D_cv_mf.clone();
		}

	  	if(GaussianBlur){
	  		cv::GaussianBlur(U_ongrid2D_cv_smth, U_ongrid2D_cv_gb, cv::Size(gaussian_kernel_size, gaussian_kernel_size), 0);
		    cv::GaussianBlur(V_ongrid2D_cv_smth, V_ongrid2D_cv_gb, cv::Size(gaussian_kernel_size, gaussian_kernel_size), 0);
		    cv::GaussianBlur(W_ongrid2D_cv_smth, W_ongrid2D_cv_gb, cv::Size(gaussian_kernel_size, gaussian_kernel_size), 0);
	  	}
		else{
		    U_ongrid2D_cv_gb = U_ongrid2D_cv_mf.clone();
		    V_ongrid2D_cv_gb = V_ongrid2D_cv_mf.clone();
		    W_ongrid2D_cv_gb = W_ongrid2D_cv_mf.clone();
		}

		//CV_32FC1 to CV_64FC1
		U_ongrid2D_cv_gb.convertTo(U_ongrid2D_cv_out, CV_64FC1);
		V_ongrid2D_cv_gb.convertTo(V_ongrid2D_cv_out, CV_64FC1);
		W_ongrid2D_cv_gb.convertTo(W_ongrid2D_cv_out, CV_64FC1);

		//CV to Eigen	
   		cv::cv2eigen(U_ongrid2D_cv_out, U_ongrid2D_out);
		cv::cv2eigen(V_ongrid2D_cv_out, V_ongrid2D_out);
		cv::cv2eigen(W_ongrid2D_cv_out, W_ongrid2D_out);

		U_ongrid3D_.push_back(U_ongrid2D_out);
		V_ongrid3D_.push_back(V_ongrid2D_out);
		W_ongrid3D_.push_back(W_ongrid2D_out);
	}
}

void Transport::setVelocityOnGrids(const std::vector<cv::Point3d>& velocity_fields){

	if(!U_ongrid3D_.empty()) U_ongrid3D_.clear();
	if(!V_ongrid3D_.empty()) V_ongrid3D_.clear();
	if(!W_ongrid3D_.empty()) W_ongrid3D_.clear();

	MatrixXd U_ongrid2D, V_ongrid2D, W_ongrid2D;

	U_ongrid2D.resize(nXc_.y, nXc_.x);
	V_ongrid2D.resize(nXc_.y, nXc_.x);
	W_ongrid2D.resize(nXc_.y, nXc_.x);

	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				U_ongrid2D(j,i)=velocity_fields_[k*nXc_.x*nXc_.y+j*nXc_.x+i].x;
				V_ongrid2D(j,i)=velocity_fields_[k*nXc_.x*nXc_.y+j*nXc_.x+i].y;
				W_ongrid2D(j,i)=velocity_fields_[k*nXc_.x*nXc_.y+j*nXc_.x+i].z;
			}
		}
		U_ongrid3D_.push_back(U_ongrid2D);
		V_ongrid3D_.push_back(V_ongrid2D);
		W_ongrid3D_.push_back(W_ongrid2D);
	}
}

void Transport::setparticles_octree(const std::vector<Particle_ptr>& particles_ini, int it_seq_cnt){

    if(!particles_cloud_->empty()) particles_cloud_->clear();

    auto pt_d4 = pcl::PointXYZL();

    for(const auto& pc: particles_ini){
        if(pc->is_ontrack() && pc->Xcoord().size()>=2){
            auto pt=pc->getPreviousXcoord(it_seq_cnt);
            pt_d4.x = pt.x;
            pt_d4.y = pt.y;
            pt_d4.z = pt.z;
            pt_d4.label = pc->ID();
    		particles_cloud_->push_back(pt_d4);
    	}
	}

    particles_octree_=pcl::octree::OctreePointCloudSearch<pcl::PointXYZL>(dXc_.x);
    particles_octree_.defineBoundingBox(Xc_bottom_.x, Xc_bottom_.y, Xc_bottom_.z, Xc_top_.x, Xc_top_.y, Xc_top_.z);	
    particles_octree_.setInputCloud(particles_cloud_);
    particles_octree_.addPointsFromInputCloud();
}

std::vector<int> Transport::findParticlesInCurrentPatch(const cv::Point3d& patch_center, const ParticleSearch_Type& particle_search_type){

    std::vector<int> part_index_vec_cloud, part_index_vec;
    std::vector<float> sqr_distance;                                                                                              
    auto searchPoint = pcl::PointXYZL();
    searchPoint.x = patch_center.x;
    searchPoint.y = patch_center.y;
    searchPoint.z = patch_center.z;

    switch(particle_search_type){
    	case Transport::ParticleSearch_Type::box:
    	{
		    Eigen::Vector3f box_lb, box_ub;

		    box_lb[0] = patch_center.x-dXc_.x/2.;
		    box_lb[1] = patch_center.y-dXc_.y/2.;
		    box_lb[2] = patch_center.z-dXc_.z/2.;

		    box_ub[0] = patch_center.x+dXc_.x/2.;
		    box_ub[1] = patch_center.y+dXc_.y/2.;
		    box_ub[2] = patch_center.z+dXc_.z/2.;
		    
		    particles_octree_.boxSearch(box_lb, box_ub, part_index_vec_cloud);
		    break;
    	}
    	case Transport::ParticleSearch_Type::radius:
		{
		    particles_octree_.radiusSearch(searchPoint, search_radius_in_mm_, part_index_vec_cloud, sqr_distance);
		    break;
    	}
    	case Transport::ParticleSearch_Type::voxel:
		{
		    particles_octree_.voxelSearch(searchPoint, part_index_vec_cloud);
		    break;
    	}
    }

    for(auto& p: part_index_vec_cloud)
    	part_index_vec.push_back((*particles_cloud_)[p].label);

    return part_index_vec;
}

void Transport::makeVelocityToVector(void){
	if(!velocity_fields_.empty()) velocity_fields_.clear();

	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				velocity_fields_.push_back(cv::Point3d( U_ongrid3D_[k](j,i), V_ongrid3D_[k](j,i), W_ongrid3D_[k](j,i)));
			}
		}
	}
}

void Transport::getAvgVelocityFields(void){

    cout<<"Quality check and interpolation:"<<endl;
    this->QualityCheck(U_ongrid3D_);
    this->QualityCheck(V_ongrid3D_);
    this->QualityCheck(W_ongrid3D_);
}

void Transport::QualityCheck(std::vector<MatrixXd>& V_component){

    for(int k=0; k<nXc_.z; k++){
        for(int j=0; j<nXc_.y; j++){
            for(int i=0; i<nXc_.x; i++){
    			if(V_component[k](j,i)==0){
    				if(i>0 && i<nXc_.x-1 && j>0 && j<nXc_.y-1)
	    				V_component[k](j,i)=(V_component[k](j-1,i)+V_component[k](j+1,i)+V_component[k](j,i+1)+V_component[k](j,i-1))/4.;
	    			else if(i==0 && j>0 && j<nXc_.y-1)
	    				V_component[k](j,i)=(V_component[k](j-1,i)+V_component[k](j+1,i)+V_component[k](j,i+1))/3.;
	    			else if(i==nXc_.x-1 && j>0 && j<nXc_.y-1)
	    				V_component[k](j,i)=(V_component[k](j-1,i)+V_component[k](j+1,i)+V_component[k](j,i-1))/3.;
	    			else if(j==0 && i>0 && i<nXc_.x-1)
	    				V_component[k](j,i)=(V_component[k](j+1,i)+V_component[k](j,i+1)+V_component[k](j,i-1))/3.;
	    			else if(j==nXc_.y-1 && i>0 && i<nXc_.x-1)
	    				V_component[k](j,i)=(V_component[k](j-1,i)+V_component[k](j,i+1)+V_component[k](j,i-1))/3.;
	    			else if(i==0 && j==0)
	    				V_component[k](j,i)=(V_component[k](j,i+1)+V_component[k](j+1,i))/2.;
	    			else if(i==0 && j==nXc_.y-1)
	    				V_component[k](j,i)=(V_component[k](j,i+1)+V_component[k](j-1,i))/2.;
	    			else if(i==nXc_.x-1 && j==0)
	    				V_component[k](j,i)=(V_component[k](j,i-1)+V_component[k](j+1,i))/2.;
	    			else if(i==nXc_.x-1 && j==nXc_.y-1)
	    				V_component[k](j,i)=(V_component[k](j,i-1)+V_component[k](j-1,i))/2.;  		
    			}
    		}
    	}
    }
}

void Transport::readVelocityFieldsFromTXTFile(const std::string& DataFile, bool starting_flag){

	std::ifstream veloFile(DataFile);

	std::vector<std::string> x_v, y_v, z_v, u_v, v_v, w_v;
	std::string x, y, z, u, v, w, line;

	if(veloFile.is_open()){
		while(std::getline(veloFile, line)){
			std::string::size_type nc=line.find("#");
			if(nc != std::string::npos)
				line.erase(nc);

			std::istringstream is(line);
			if(is >> x >> y >> z >> u >> v >> w){
				x_v.push_back(x);
				y_v.push_back(y);
				z_v.push_back(z);
				u_v.push_back(u);
				v_v.push_back(v);
				w_v.push_back(w);
			}
		}
	}
	else{
		std::cerr<<"Error opening velocity data file "<<DataFile<<endl;
		std::abort();
	}

	if(!velocity_fields_.empty()) velocity_fields_.clear();

	cout<<"Grid size in each direction: "<<nXc_<<endl;
	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				if(starting_flag) grid_coords_.push_back(cv::Point3d( std::stod(x_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(y_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(z_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]) ));
				velocity_fields_.push_back(cv::Point3d( std::stod(u_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(v_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]), std::stod(w_v[k*nXc_.x*nXc_.y+j*nXc_.x+i]) ));
			}
		}
	}

	if(starting_flag){
		velo_max_=cv::Point3d(std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);
		velo_min_=cv::Point3d(std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);

		cout<<"Velo max "<<velo_max_<<endl
			<<"Velo min "<<velo_min_<<endl;
	}
}

void Transport::readVelocityFieldsFromNPZFile(const std::string& DataFile, bool starting_flag){

    auto load_npz = cnpy::npz_load(DataFile);

    auto X_v = load_npz["X"].as_vec<double>();
    auto Y_v = load_npz["Y"].as_vec<double>();
    auto Z_v = load_npz["Z"].as_vec<double>();
    auto U_v = load_npz["U"].as_vec<double>();
    auto V_v = load_npz["V"].as_vec<double>();
    auto W_v = load_npz["W"].as_vec<double>();

	if(!velocity_fields_.empty()) velocity_fields_.clear();

	cout<<"Grid size in each direction: "<<nXc_<<endl;
	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				if(starting_flag) grid_coords_.push_back(cv::Point3d( X_v[k*nXc_.x*nXc_.y+j*nXc_.x+i], Y_v[k*nXc_.x*nXc_.y+j*nXc_.x+i], Z_v[k*nXc_.x*nXc_.y+j*nXc_.x+i] ));
				velocity_fields_.push_back(cv::Point3d( U_v[k*nXc_.x*nXc_.y+j*nXc_.x+i], V_v[k*nXc_.x*nXc_.y+j*nXc_.x+i], W_v[k*nXc_.x*nXc_.y+j*nXc_.x+i] ));
			}
		}
	}

	if(starting_flag){
		velo_max_=cv::Point3d(std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::max_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);
		velo_min_=cv::Point3d(std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_x)->x,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_y)->y,
							  std::min_element(velocity_fields_.begin(), velocity_fields_.end(), utils::compare_z)->z);

		cout<<"Velo max "<<velo_max_<<endl
			<<"Velo min "<<velo_min_<<endl;
	}
}

void Transport::saveVelocityFieldsToNPZFile(const std::string& res_file) const{

    std::vector<double> X_vec, Y_vec, Z_vec, U_vec, V_vec, W_vec;

    for(int k=0; k<zgrid_.size(); k++){
        for(int j=0; j<ygrid_.size(); j++){
            for(int i=0; i<xgrid_.size(); i++){
            	X_vec.push_back( xgrid_[i] );
            	Y_vec.push_back( ygrid_[j] );
            	Z_vec.push_back( zgrid_[k] );
            	U_vec.push_back( U_ongrid3D_[k](j,i) );
            	V_vec.push_back( V_ongrid3D_[k](j,i) );
            	W_vec.push_back( W_ongrid3D_[k](j,i) );
            }
        }
	}

    auto NX = static_cast<long unsigned int>(X_vec.size());
    cnpy::npz_save(res_file, "X", &X_vec[0], {NX,1}, "w");
    cnpy::npz_save(res_file, "Y", &Y_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "Z", &Z_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "U", &U_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "V", &V_vec[0], {NX,1}, "a");
    cnpy::npz_save(res_file, "W", &W_vec[0], {NX,1}, "a");
}


void Transport::saveVelocityFieldsToVTKFile(const std::string& res_file) const{

    auto nz=zgrid_.size();
    auto ny=ygrid_.size();
    auto nx=xgrid_.size();
    auto dXc=dXc_minimal_;

    auto grid = vtkSmartPointer<vtkDoubleArray>::New();
    grid->SetNumberOfComponents(3);
    grid->SetNumberOfTuples(nx*ny*nz);
    grid->SetName("Grid");

    auto velocity = vtkSmartPointer<vtkDoubleArray>::New();
    velocity->SetNumberOfComponents(3);
    velocity->SetNumberOfTuples(nx*ny*nz);
    velocity->SetName("Velocity");

    for(int k=0; k<nz; k++){
        for(int j=0; j<ny; j++){
            for(int i=0; i<nx; i++){
                velocity->SetTuple3(k*nx*ny+j*nx+i,
                                    this->U_ongrid3D_[k](j,i),
                                    this->V_ongrid3D_[k](j,i),
                                    this->W_ongrid3D_[k](j,i));
            }
        }
    }

    auto dataSet = vtkSmartPointer<vtkImageData>::New();
    dataSet->SetExtent(0, nx-1, 0, ny-1, 0, nz-1);
    dataSet->SetOrigin(0, 0, 0);
    dataSet->SetSpacing(dXc.x, dXc.y, dXc.z);
    dataSet->GetPointData()->AddArray(velocity);
    dataSet->GetPointData()->AddArray(grid);

    auto writer = vtkSmartPointer<vtkXMLImageDataWriter>::New();
    writer->SetFileName(res_file.c_str());
    writer->SetInputData(dataSet);
    writer->Write();
}


void Transport::computeVelocityDerivatives(void){

	//using central scheme
	if(!dU_ongrid3D_dx_.empty()) dU_ongrid3D_dx_.clear();
	if(!dV_ongrid3D_dx_.empty()) dV_ongrid3D_dx_.clear();
	if(!dW_ongrid3D_dx_.empty()) dW_ongrid3D_dx_.clear();
	if(!dU_ongrid3D_dy_.empty()) dU_ongrid3D_dy_.clear();
	if(!dV_ongrid3D_dy_.empty()) dV_ongrid3D_dy_.clear();
	if(!dW_ongrid3D_dy_.empty()) dW_ongrid3D_dy_.clear();
	if(!dU_ongrid3D_dz_.empty()) dU_ongrid3D_dz_.clear();
	if(!dV_ongrid3D_dz_.empty()) dV_ongrid3D_dz_.clear();
	if(!dW_ongrid3D_dz_.empty()) dW_ongrid3D_dz_.clear();

	if(!dU_ongrid3D_dxdy_.empty()) dU_ongrid3D_dxdy_.clear();
	if(!dV_ongrid3D_dxdy_.empty()) dV_ongrid3D_dxdy_.clear();
	if(!dW_ongrid3D_dxdy_.empty()) dW_ongrid3D_dxdy_.clear();
	if(!dU_ongrid3D_dxdz_.empty()) dU_ongrid3D_dxdz_.clear();
	if(!dV_ongrid3D_dxdz_.empty()) dV_ongrid3D_dxdz_.clear();
	if(!dW_ongrid3D_dxdz_.empty()) dW_ongrid3D_dxdz_.clear();
	if(!dU_ongrid3D_dydz_.empty()) dU_ongrid3D_dydz_.clear();
	if(!dV_ongrid3D_dydz_.empty()) dV_ongrid3D_dydz_.clear();
	if(!dW_ongrid3D_dydz_.empty()) dW_ongrid3D_dydz_.clear();

	if(!dU_ongrid3D_dxdydz_.empty()) dU_ongrid3D_dxdydz_.clear();
	if(!dV_ongrid3D_dxdydz_.empty()) dV_ongrid3D_dxdydz_.clear();
	if(!dW_ongrid3D_dxdydz_.empty()) dW_ongrid3D_dxdydz_.clear();

	dU_ongrid3D_dx_ = field::computedFdx( U_ongrid3D_ );
	dV_ongrid3D_dx_ = field::computedFdx( V_ongrid3D_ );
	dW_ongrid3D_dx_ = field::computedFdx( W_ongrid3D_ );
	dU_ongrid3D_dy_ = field::computedFdy( U_ongrid3D_ );
	dV_ongrid3D_dy_ = field::computedFdy( V_ongrid3D_ );
	dW_ongrid3D_dy_ = field::computedFdy( W_ongrid3D_ );
	dU_ongrid3D_dz_ = field::computedFdz( U_ongrid3D_ );
	dV_ongrid3D_dz_ = field::computedFdz( V_ongrid3D_ );
	dW_ongrid3D_dz_ = field::computedFdz( W_ongrid3D_ );

	dU_ongrid3D_dxdy_ = field::computedFdy( dU_ongrid3D_dx_ );
	dV_ongrid3D_dxdy_ = field::computedFdy( dV_ongrid3D_dx_ );
	dW_ongrid3D_dxdy_ = field::computedFdy( dW_ongrid3D_dx_ );
	dU_ongrid3D_dxdz_ = field::computedFdz( dU_ongrid3D_dx_ );
	dV_ongrid3D_dxdz_ = field::computedFdz( dV_ongrid3D_dx_ );
	dW_ongrid3D_dxdz_ = field::computedFdz( dW_ongrid3D_dx_ );
	dU_ongrid3D_dydz_ = field::computedFdz( dU_ongrid3D_dy_ );
	dV_ongrid3D_dydz_ = field::computedFdz( dV_ongrid3D_dy_ );
	dW_ongrid3D_dydz_ = field::computedFdz( dW_ongrid3D_dy_ );

	dU_ongrid3D_dxdydz_ = field::computedFdz( dU_ongrid3D_dxdy_ );
	dV_ongrid3D_dxdydz_ = field::computedFdz( dV_ongrid3D_dxdy_ );
	dW_ongrid3D_dxdydz_ = field::computedFdz( dW_ongrid3D_dxdy_ );
}

cv::Point3d Transport::tricubicInterpolate(const cv::Point3d& pc) const{

	double xl,xr,yl,yr,zl,zr;
	int xl_idx,xr_idx,yl_idx,yr_idx,zl_idx,zr_idx;

	this->findNeighbors("X", pc.x, xl_idx, xr_idx, xl, xr);
	this->findNeighbors("Y", pc.y, yl_idx, yr_idx, yl, yr);
	this->findNeighbors("Z", pc.z, zl_idx, zr_idx, zl, zr);

    std::vector<cv::Point3d> pts_idx{cv::Point3d(xl_idx, yl_idx, zl_idx),
                                     cv::Point3d(xr_idx, yl_idx, zl_idx),
                                     cv::Point3d(xl_idx, yr_idx, zl_idx),
                                     cv::Point3d(xr_idx, yr_idx, zl_idx),
                                     cv::Point3d(xl_idx, yl_idx, zr_idx),
                                     cv::Point3d(xr_idx, yl_idx, zr_idx),
                                     cv::Point3d(xl_idx, yr_idx, zr_idx),
                                     cv::Point3d(xr_idx, yr_idx, zr_idx)};

    return cv::Point3d( this->tricubicInterpolateScalar("U", pts_idx, pc),
                        this->tricubicInterpolateScalar("V", pts_idx, pc),
                        this->tricubicInterpolateScalar("W", pts_idx, pc) );
}

double Transport::tricubicInterpolateScalar(const std::string& component_flag, const std::vector<cv::Point3d>& pts_idx, const cv::Point3d& pc) const{

    double a[64];

	double fval[8],
		   dfdxval[8],
		   dfdyval[8],
		   dfdzval[8],
		   d2fdxdyval[8],
		   d2fdxdzval[8],
		   d2fdydzval[8],
		   d3fdxdydzval[8];

    int i=0;				
    if(component_flag=="U"){
        for(auto& pt_idx: pts_idx){
            fval[i]         = U_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdxval[i]      = dU_ongrid3D_dx_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdyval[i]      = dU_ongrid3D_dy_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdzval[i]      = dU_ongrid3D_dz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdxdyval[i]   = dU_ongrid3D_dxdy_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdxdzval[i]   = dU_ongrid3D_dxdz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdydzval[i]   = dU_ongrid3D_dydz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d3fdxdydzval[i] = dU_ongrid3D_dxdydz_[pt_idx.z](pt_idx.y,pt_idx.x);
            i++;
        }
    }					 
    else if(component_flag=="V"){
        for(auto& pt_idx: pts_idx){
            fval[i]         = V_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdxval[i]      = dV_ongrid3D_dx_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdyval[i]      = dV_ongrid3D_dy_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdzval[i]      = dV_ongrid3D_dz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdxdyval[i]   = dV_ongrid3D_dxdy_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdxdzval[i]   = dV_ongrid3D_dxdz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdydzval[i]   = dV_ongrid3D_dydz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d3fdxdydzval[i] = dV_ongrid3D_dxdydz_[pt_idx.z](pt_idx.y,pt_idx.x);
            i++;
        }
    }       
    else if(component_flag=="W"){
        for(auto& pt_idx: pts_idx){
            fval[i]         = W_ongrid3D_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdxval[i]      = dW_ongrid3D_dx_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdyval[i]      = dW_ongrid3D_dy_[pt_idx.z](pt_idx.y,pt_idx.x);
            dfdzval[i]      = dW_ongrid3D_dz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdxdyval[i]   = dW_ongrid3D_dxdy_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdxdzval[i]   = dW_ongrid3D_dxdz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d2fdydzval[i]   = dW_ongrid3D_dydz_[pt_idx.z](pt_idx.y,pt_idx.x);
            d3fdxdydzval[i] = dW_ongrid3D_dxdydz_[pt_idx.z](pt_idx.y,pt_idx.x);
            i++;
        }
    }

    tricubic_get_coeff(a,fval,dfdxval,dfdyval,dfdzval,d2fdxdyval,d2fdxdzval,d2fdydzval,d3fdxdydzval);

    //x_normalized in (0,1)
    double x_normalized = (pc.x - xgrid_[pts_idx[0].x])/dXc_.x;
    double y_normalized = (pc.y - ygrid_[pts_idx[0].y])/dXc_.y;
    double z_normalized = (pc.z - zgrid_[pts_idx[0].z])/dXc_.z;

    return tricubic_eval(a, x_normalized, y_normalized, z_normalized);
}

void Transport::extractDivergenceFreeVelocityField(void){

    std::vector<MatrixXd> Vx_3D, Vy_3D, Vz_3D;

    for(auto& U_plane: U_ongrid3D_) Vx_3D.push_back(U_plane.transpose());
    for(auto& U_plane: V_ongrid3D_) Vy_3D.push_back(U_plane.transpose());
    for(auto& U_plane: W_ongrid3D_) Vz_3D.push_back(U_plane.transpose());

	std::vector<MatrixXd> Vx_divfree, Vy_divfree, Vz_divfree, div_3D, mag_3D,
						  Vx_irr, Vy_irr, Vz_irr, Vx_har, Vy_har, Vz_har;

    field::getDivergenceFreeFieldFromHelmholtzDecomposition(Vx_3D, Vy_3D, Vz_3D, nXc_, dXc_minimal_,
                                                    Vx_divfree, Vy_divfree, Vz_divfree, div_3D, mag_3D,
                                                    Vx_irr, Vy_irr, Vz_irr, Vx_har, Vy_har, Vz_har);

    field::normalizeDivergenceFreeField(Vx_divfree, Vy_divfree, Vz_divfree, mag_3D);

    cout<<"Not working so far"<<endl;
    std::abort();
}
