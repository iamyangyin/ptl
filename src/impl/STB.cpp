#include "../STB.hpp"

STB::STB(){}

STB::~STB(){}

/// STB version

STB::STB(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag, 
        int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, 
        double pixel_lost, double dtObs, 
        const cv::Point3d& dXc_px, const std::string& res_path, int eval_window,
        bool initial_shake_flag, int initial_shake_max_iter, double initial_shake_pixel, double initial_shake_relaxfactor,
        int fine_shake_max_iter, double fine_shake_pixel, double fine_shake_relaxfactor)
        :OptMethod(method_type, control_vector_type, intensity_control_flag, 
                   n_part, n_part_per_snapshot, Xc_top, Xc_bottom, 
                   pixel_lost, dtObs,
                   dXc_px, res_path, eval_window, 
                   initial_shake_flag, initial_shake_max_iter, initial_shake_pixel, initial_shake_relaxfactor),
        fine_shake_max_iter_(fine_shake_max_iter), fine_shake_pixel_(fine_shake_pixel), fine_shake_relaxfactor_(fine_shake_relaxfactor)
        {
            ipr_full_flag_=false;
        }

/// IPR version

STB::STB(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag, 
        int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, 
        const cv::Point3d& dXc_px, const std::string& res_path, int eval_window,
        int fine_shake_max_iter, double fine_shake_pixel, double fine_shake_relaxfactor)
        :OptMethod(method_type, control_vector_type, intensity_control_flag, 
                   n_part, n_part_per_snapshot, Xc_top, Xc_bottom,
                   dXc_px, res_path, eval_window),
        fine_shake_max_iter_(fine_shake_max_iter), fine_shake_pixel_(fine_shake_pixel), fine_shake_relaxfactor_(fine_shake_relaxfactor)
        {
            ipr_full_flag_=true;
        }

void STB::initialization(std::vector<Particle_ptr>& particles_ana, int it_seq_cnt){

    if(!ipr_full_flag_)
        utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ana, Xc_top_, Xc_bottom_);

    this->setintensity_threshold(particles_ana);
    particles_stb_ = std::move( particles_ana );
}

void STB::predict(DynModel* dynmodel, int it_seq_cnt, int it_fct_deb){

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_stb_, Xc_top_, Xc_bottom_);

    cout<<"Before position prediction"<<endl;
    dynmodel->predictNextPaticlesPosition(particles_stb_, it_seq_cnt);
    cout<<"After position prediction"<<endl;

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_stb_, Xc_top_, Xc_bottom_);

    for(const auto& pt: particles_stb_){
        if(pt->is_ontrack() && pt->is_to_be_tracked() && !pt->is_side()){
            pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference);
        }
    }

    cout<<"Before intensity prediction"<<endl;
    this->predictIntensity(it_seq_cnt);
    cout<<"After intensity prediction"<<endl;
}

void STB::prepare(int it_seq_cnt, const std::vector<Camera_ptr>& cam, int dummy){

    this->projectParticlesToImageSpace(it_seq_cnt, particles_stb_, cam);

    this->processingResidualImage(cam, 0, particles_stb_, it_seq_cnt, true, true);
    this->processingImageResidualParticle(particles_stb_, it_seq_cnt, cam);

    if(!ipr_full_flag_){
        this->setres_parti(warm_start_max_iter_, it_seq_cnt, particles_stb_, cam);
    }

    this->printTrackInformation(it_seq_cnt);

    n_new_particles_.push_back(0);
    n_append_particles_.push_back(0);

    fine_shake_ratio_ = cv::Point3d(fine_shake_pixel_, fine_shake_pixel_, fine_shake_pixel_);
    resf_prev_.assign(n_particles_, 0.);
    res_partf_.assign(n_particles_, 0.);
}

void STB::selectWarmStart(int it_seq_cnt, const std::vector<Camera_ptr>& cam){

    lMax_=warm_start_max_iter_;

    this->doWarmStart(it_seq_cnt, cam, particles_stb_);

    this->projectParticlesToImageSpace(it_seq_cnt, particles_stb_, cam);
    for(int p=0; p<particles_stb_.size(); p++){
        if(particles_stb_[p]->is_ontrack() && this->checkIfParticleIsToBeOptimized(particles_stb_[p].get()))
            resf_prev_[p]=particles_stb_[p]->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_);
    }
}

void STB::core(int it_seq_cnt, int k, const std::vector<Camera_ptr>& cam,
                bool rm_outlier_flag){

    if(k>kMax_-3) fine_shake_ratio_ = cv::Point3d(fine_shake_ratio_valve_, fine_shake_ratio_valve_, fine_shake_ratio_valve_);

    cout<<"-----------------------------------------------------"<<endl;
    cout<<"Fine Shake iteration      : "<<k<<endl;
    cout<<"residual begin iteration  : "<<res_glob_.at(k+lMax_-1)<<endl;
    cout<<"fine_shake_ratio          : "<<fine_shake_ratio_<<endl;

    this->updateParticles(it_seq_cnt, particles_stb_, cam, resf_prev_);

    if(rm_outlier_flag){
        this->removeOutlier(it_seq_cnt, particles_stb_, k);
    }

    this->processingResidualImage(cam, k, particles_stb_, it_seq_cnt, false);

    cout<<"residual after position correction, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;

    this->processingImageResidualParticle(particles_stb_, it_seq_cnt, cam);

    if(intensity_control_flag_){
        for(int p=0; p<particles_stb_.size(); p++)
            if(particles_stb_[p]->is_ontrack() && this->checkIfParticleIsToBeOptimized(particles_stb_[p].get()))
                res_partf_[p]=this->correctIntensityMARTlike(particles_stb_[p].get(), it_seq_cnt, cam);

        this->processingResidualImage(cam, k, particles_stb_, it_seq_cnt, false);

        cout<<"residual after intensity correction, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;

        this->processingImageResidualParticle(particles_stb_, it_seq_cnt, cam);

        this->removeParticlesThatAreLostOrGhost(it_seq_cnt, particles_stb_);
    }
}

void STB::postIPRResidual(int it_seq_cnt, int k, const std::vector<Camera_ptr>& cam){

    this->projectParticlesToImageSpace(it_seq_cnt, particles_stb_, cam);

    this->processingResidualImage(cam, k, particles_stb_, it_seq_cnt, false);

    cout<<"residual after triangulation on residual, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;

    this->processingImageResidualParticle(particles_stb_, it_seq_cnt, cam);

    if(n_new_particles_.back() != 0){
        int n_created_particles = n_new_particles_.back();

        std::vector<double>      resf_prev_new(n_new_particles_.back(), 0.);

        resf_prev_.insert(std::end(resf_prev_), std::begin(resf_prev_new), std::end(resf_prev_new));

        for(int n=0; n<n_new_particles_.back(); n++){
            particles_stb_[n_particles_+n]->clearimgAndimgPos();
            particles_stb_[n_particles_+n]->projectToImageSpace(it_seq_cnt, cam);
            res_partf_.push_back( particles_stb_[n_particles_+n]->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_) );
        }

        assert(particles_stb_.size()==n_particles_+n_new_particles_.back());
        n_particles_=particles_stb_.size();
    }
    assert(resf_prev_.size()==particles_stb_.size());

    cout<<"Processing lost particles"<<endl;
    this->processingLostParticles(it_seq_cnt, particles_stb_, cam);
}

void STB::update(Particle* particle_tgt, int idx, int it_seq_cnt, const std::vector<Camera_ptr>& cam, double& resf_prev_idx){

    if(particle_tgt->is_ontrack() && this->checkIfParticleIsToBeOptimized(particle_tgt)){

        res_partf_[idx] = resf_prev_idx;
        res_partf_[idx] = this->quadraticFit(particle_tgt, res_partf_[idx], "X", dXc_px_.x*fine_shake_ratio_.x, it_seq_cnt, cam);
        res_partf_[idx] = this->quadraticFit(particle_tgt, res_partf_[idx], "Y", dXc_px_.y*fine_shake_ratio_.y, it_seq_cnt, cam);
        res_partf_[idx] = this->quadraticFit(particle_tgt, res_partf_[idx], "Z", dXc_px_.z*fine_shake_ratio_.z, it_seq_cnt, cam);

        if(res_partf_[idx]<resf_prev_idx)     resf_prev_idx=res_partf_[idx];

        if(!ipr_full_flag_ && particle_tgt->is_to_be_tracked())
            particle_tgt->markAsTracked();  
    }
}

double STB::quadraticFit(Particle* particle_p, double resX, const std::string& direction_flag, double dX, int it_seq_cnt, const std::vector<Camera_ptr>& cam, bool verbal){

    double ref_coord=0.0, resXm, resXp, Xopt, resAtXopt;

    if(!direction_flag.compare("X"))
        ref_coord=particle_p->getCurrentXcoord(it_seq_cnt).x;
    else if(!direction_flag.compare("Y"))
        ref_coord=particle_p->getCurrentXcoord(it_seq_cnt).y;
    else if(!direction_flag.compare("Z"))
        ref_coord=particle_p->getCurrentXcoord(it_seq_cnt).z;

    if(!direction_flag.compare("X"))
        particle_p->setXofXcoordElement(ref_coord-dX, it_seq_cnt);
    else if(!direction_flag.compare("Y"))
        particle_p->setYofXcoordElement(ref_coord-dX, it_seq_cnt);
    else if(!direction_flag.compare("Z"))
        particle_p->setZofXcoordElement(ref_coord-dX, it_seq_cnt);

    particle_p->clearimgAndimgPos();
    particle_p->projectToImageSpace(it_seq_cnt, cam);
    resXm=particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_);

    if(!direction_flag.compare("X"))
        particle_p->setXofXcoordElement(ref_coord+dX, it_seq_cnt);
    else if(!direction_flag.compare("Y"))
        particle_p->setYofXcoordElement(ref_coord+dX, it_seq_cnt);
    else if(!direction_flag.compare("Z"))
        particle_p->setZofXcoordElement(ref_coord+dX, it_seq_cnt);

    particle_p->clearimgAndimgPos();
    particle_p->projectToImageSpace(it_seq_cnt, cam);
    resXp=particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_);

    cv::Mat_<double> w;
    cv::Mat_<double> A=(cv::Mat_<double>(3,3)<<1.0, ref_coord-dX, std::pow(ref_coord-dX,2), 1.0, ref_coord, std::pow(ref_coord,2), 1.0, ref_coord+dX, std::pow(ref_coord+dX,2));
    cv::Mat_<double> b=(cv::Mat_<double>(3,1)<<resXm, resX, resXp);

    cv::solve(A, b, w);

    if(w(2)>1e-6){
        Xopt=-w(1)/(2.0*w(2));
        if(Xopt>ref_coord+dX)
            Xopt=ref_coord+dX;
        else if(Xopt<ref_coord-dX)
            Xopt=ref_coord-dX;
    }
    else{
        if(resXm<resXp)
            Xopt=ref_coord-dX;
        else
            Xopt=ref_coord+dX;
    }

    if(!direction_flag.compare("X"))
        particle_p->setXofXcoordElement(Xopt, it_seq_cnt);
    else if(!direction_flag.compare("Y"))
        particle_p->setYofXcoordElement(Xopt, it_seq_cnt);
    else if(!direction_flag.compare("Z"))
        particle_p->setZofXcoordElement(Xopt, it_seq_cnt);

    cv::Point3d Xcopt=particle_p->Xcoord().back();

    utils::maintainPositionInRange(Xcopt, Xc_top_, Xc_bottom_);

    particle_p->setXcoordElement(Xcopt, it_seq_cnt);

    particle_p->clearimgAndimgPos();
    particle_p->projectToImageSpace(it_seq_cnt, cam);
    resAtXopt=particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_);

    if(verbal)
        cout<<"residual resAtXopt      :"<<resAtXopt<<endl;

    if(resX<resAtXopt){
        Xopt=ref_coord;
        resAtXopt=resX;
        if(verbal)
            cout<<"res "<<direction_flag<<" bigger than res0, revert back!"<<endl;
    }

    if(Xopt==ref_coord){
        if(!direction_flag.compare("X"))
            particle_p->setXofXcoordElement(Xopt, it_seq_cnt);
        else if(!direction_flag.compare("Y"))
            particle_p->setYofXcoordElement(Xopt, it_seq_cnt);
        else if(!direction_flag.compare("Z"))
            particle_p->setZofXcoordElement(Xopt, it_seq_cnt);

        particle_p->clearimgAndimgPos();
        particle_p->projectToImageSpace(it_seq_cnt, cam);
        resAtXopt=particle_p->sumResidualForParticlep(it_seq_cnt, cam.size(), eval_window_fine_search_);
    }

    return resAtXopt;
}
