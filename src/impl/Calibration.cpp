#include "../Calibration.hpp"

//TODO
class OTFCostFunctor {
    public:
        OTFCostFunctor(int width, int height, const cv::Mat& Imgi_real_float_i, const cv::Point2d& xc_i, double E):
            width_(width), height_(height), Imgi_real_float_i_(Imgi_real_float_i), xc_i_(xc_i), E_(E) {}

        bool operator()(const double* const x, double* residual) const {
            if(x[0]>0 && x[0]<1 && x[1]>0 && x[1]<1 && x[2]>0 && x[2]<1){
                
                Eigen::Vector4f psf;
                psf<<float(x[0]), 0, float(x[1]), float(x[2]); 

                cv::Mat  Imgi_smpl_i;
                utils::getImageSmpl(psf, xc_i_, E_, width_, height_, Imgi_smpl_i);

                cv::Mat Ires_i;
                cv::subtract(Imgi_real_float_i_, Imgi_smpl_i, Ires_i);

                residual[0] = double( std::pow(cv::norm(Ires_i),2) );
                return true;
            }
            else
                return false;
        }

    private:
        int         width_;
        int         height_;
        cv::Mat     Imgi_real_float_i_;
        cv::Point2d xc_i_;
        double      E_;
};

class OTFIntensityCostFunctor {
    public:
        OTFIntensityCostFunctor(float i, float j, int width, int height, const cv::Mat& Imgi_real_float_i, const cv::Point2d& xc_i, double E):
            i_(i), j_(j), width_(width), height_(height), Imgi_real_float_i_(Imgi_real_float_i), xc_i_(xc_i), E_(E) {}

        bool operator()(const double* const x, double* residual) const {
            if(x[0]>1000 && x[0]<65535){
                Eigen::Vector4f psf;
                psf<<i_, 0, j_, float(x[0]); 

                cv::Mat  Imgi_smpl_i;
                utils::getImageSmpl(psf, xc_i_, E_, width_, height_, Imgi_smpl_i);

                cv::Mat Ires_i;
                cv::subtract(Imgi_real_float_i_, Imgi_smpl_i, Ires_i);

                residual[0] = double( std::pow(cv::norm(Ires_i),2) );
                return true;
            }
            else
                return false;
        }

    private:
        float       i_;
        float       j_;
        int         width_;
        int         height_;
        cv::Mat     Imgi_real_float_i_;
        cv::Point2d xc_i_;
        double      E_;
};

class OTFRadiusCostFunctor {
    public:
        OTFRadiusCostFunctor(float k, int width, int height, const cv::Mat& Imgi_real_float_i, const cv::Point2d& xc_i, double E):
            k_(k), width_(width), height_(height), Imgi_real_float_i_(Imgi_real_float_i), xc_i_(xc_i), E_(E) {}

        bool operator()(const double* const x, double* residual) const {
            if(x[0]>0.9 && x[0]<9 && x[1]>0.9 && x[1]<9){
                Eigen::Vector4f psf;
                psf<<float(x[0]), 0, float(x[1]), k_; 

                cv::Mat  Imgi_smpl_i;
                utils::getImageSmpl(psf, xc_i_, E_, width_, height_, Imgi_smpl_i);

                cv::Mat Ires_i;
                cv::subtract(Imgi_real_float_i_, Imgi_smpl_i, Ires_i);

                residual[0] = double( std::pow(cv::norm(Ires_i),2) );
                return true;
            }
            else
                return false;
        }

    private:
        float       k_;
        int         width_;
        int         height_;
        cv::Mat     Imgi_real_float_i_;
        cv::Point2d xc_i_;
        double      E_;
};

Calibration::Calibration(){}

Calibration::~Calibration(){}

Calibration::Calibration(Source_Type source, int n_cam, 
                         const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, 
                         const cv::Point2i& nXc_rig, int nViews, bool Right_handed_flag)
                        : source_(source), n_cam_(n_cam), 
                          Xc_top_(Xc_top), Xc_bottom_(Xc_bottom), nXc_rig_(nXc_rig), nViews_(nViews), Right_handed_flag_(Right_handed_flag) {}

void Calibration::generate3DGridsInObjectSpace(void){

    cv::Point3d Xc_top_round(Xc_top_), Xc_bottom_round(Xc_bottom_);

    dXc_rig_.x=(Xc_top_round.x-Xc_bottom_round.x)/static_cast<double>(nXc_rig_.x-1);
    dXc_rig_.y=(Xc_top_round.y-Xc_bottom_round.y)/static_cast<double>(nXc_rig_.y-1);
    dXc_rig_.z=(Xc_top_round.z-Xc_bottom_round.z)/static_cast<double>(nViews_-1);

    cout<<"dXc_rig_"<<endl<<dXc_rig_<<endl;

    VectorXd Xgrid, Ygrid;

    Xgrid.setLinSpaced(nXc_rig_.x, Xc_bottom_round.x , Xc_top_round.x);
    Ygrid.setLinSpaced(nXc_rig_.y, Xc_bottom_round.y , Xc_top_round.y);

    cout<<"Xgrid"<<endl<<Xgrid.transpose()<<endl;
    cout<<"Ygrid"<<endl<<Ygrid.transpose()<<endl;

    MatrixXd XXgrid,YYgrid;
    XXgrid=Xgrid.transpose().replicate(nXc_rig_.y, 1);
    YYgrid=Ygrid.replicate(1, nXc_rig_.x);

    ZViews_.resize(nViews_);
    std::iota(ZViews_.begin(), ZViews_.end(), 0);
    std::for_each(ZViews_.begin(), ZViews_.end(), [=](double &Z){ Z=Xc_bottom_round.z+dXc_rig_.z*Z; });

    cout<<"ZViews_"<<endl;
    for(auto& Z: ZViews_) cout<<" "<<Z;
    cout<<endl;

    for(int z=0; z<nViews_; z++){
        std::vector<cv::Point3d> Xcoord_grid_v;
        for(int i=0; i<nXc_rig_.x; i++){
            for(int j=0; j<nXc_rig_.y; j++){
                Xcoord_grid_v.push_back( cv::Point3d( XXgrid(j,i),
                                                      YYgrid(j,i),
                                                      ZViews_.at(z)) );
            }
        }

        Xcoord_obj_.push_back( Xcoord_grid_v );
    }

    Xcoord_obj_cams_.assign(n_cam_, Xcoord_obj_);
}

void Calibration::getPolynomialModelParameters(const std::string& mf_file, Camera* cam, Dataset* dataset){
    assert(cam->z_order_==2);
    cam->n_coef_=10;
    cam->polynomial_type_=Camera::Polynomial_Type::ZPlane;

    cam->num_samples_tot_=0;
    if(nXc_rig_.x*nXc_rig_.y!=0){
        cam->num_samples_tot_=nXc_rig_.x*nXc_rig_.y*nViews_;
    }
    else{
        for(int z=0; z<nViews_; z++){
            cam->num_samples_tot_+=Xcoord_obj_cams_.at(cam->i_cam_).at(z).size();
        }
    }
    cout<<"Total "<<cam->num_samples_tot_<<" points"<<endl;

    dataset->readPolynomialMF(mf_file, cam);
}

void Calibration::getFundamentalMatrix(int it_seq, Camera* cam1, Camera* cam2, 
                                        const std::vector<Particle_ptr>& particles_vsc){

    if(cam1->i_cam_==cam2->i_cam_){
        cam1->F_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
    }
    else{
        cout<<"get Fundamental Matrix"<<endl;

        std::vector<cv::Point2f> ProjPoints_idx1, ProjPoints_idx2;

        int num_train_set = 50;
        for(int i=0; i<num_train_set; i++){
            auto xc1 = cam1->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            auto xc2 = cam2->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            ProjPoints_idx1.push_back( cv::Point2f(float(xc1.x), float(xc1.y)) );
            ProjPoints_idx2.push_back( cv::Point2f(float(xc2.x), float(xc2.y)) );
        }

        std::vector<cv::Point2f>   xc1_u, xc2_u;
        cv::undistortPoints(ProjPoints_idx1, xc1_u, cam1->cameraMatrix(), cam1->distCoeffs(), cv::noArray(), cam1->cameraMatrix());
        cv::undistortPoints(ProjPoints_idx2, xc2_u, cam2->cameraMatrix(), cam2->distCoeffs(), cv::noArray(), cam2->cameraMatrix());

        cam1->F_betw_cams_.push_back( cv::findFundamentalMat( xc1_u, xc2_u, cv::FM_RANSAC, 1, 0.99) );
    }
}

void Calibration::computeFundamentalMatrixError(int it_seq, Camera* cam1, Camera* cam2,
                                        const std::vector<Particle_ptr>& particles_vsc,
                                        std::vector<std::vector<double>>& err_betw_cams){

    bool undistort_flag = (cv::norm(cam1->distCoeffs())<1e-3 && cv::norm(cam2->distCoeffs())<1e-3);

    if(cam1->i_cam_!=cam2->i_cam_){
        cout<<"Between "<<cam1->i_cam_<<"th camera and "<<cam2->i_cam_<<"th camera ";

        std::vector<cv::Point2d>    Points_idx1_d, Points_idx1_u;
        std::vector<cv::Point2d>    Points_idx2_d, Points_idx2_u;

        for(int i=0; i<particles_vsc.size(); i++){
            auto xc1 = cam1->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            auto xc2 = cam2->mappingFunction(particles_vsc[i]->getCurrentXcoord(it_seq));
            
            Points_idx1_d.push_back(xc1);
            Points_idx2_d.push_back(xc2);
        }
            
        if(undistort_flag){
            cout<<"Ignore small distortion ####################"<<endl;
            Points_idx1_u = Points_idx1_d;
            Points_idx2_u = Points_idx2_d;
        }
        else{
            cv::undistortPoints(Points_idx1_d, Points_idx1_u, cam1->cameraMatrix(), cam1->distCoeffs(), cv::noArray(), cam1->cameraMatrix()); 
            cv::undistortPoints(Points_idx2_d, Points_idx2_u, cam2->cameraMatrix(), cam2->distCoeffs(), cv::noArray(), cam2->cameraMatrix()); 
        }   

        // cv::Mat_<double> epiline12 = cam1->F_betw_cams().at(cam2->i_cam())*homo_xc1;
        // double epi_norm = sqrt(epiline12(0)*epiline12(0)+epiline12(1)*epiline12(1));
        // auto   err_dist = abs(homo_xc2.dot(epiline12))/epi_norm;

        cv::Mat F12_f;
        cam1->F_betw_cams().at(cam2->i_cam()).convertTo(F12_f, CV_32F);

        std::vector<cv::Point2f> Point_idx1_f_u;
        for(auto& p: Points_idx1_u){
            Point_idx1_f_u.push_back(cv::Point2f(float(p.x), float(p.y)));
        }

        cv::Mat epiline12_vec;
        cv::computeCorrespondEpilines(Point_idx1_f_u, 1, F12_f, epiline12_vec);

        double err_dist_sum=0;
        for(int i=0; i<particles_vsc.size(); i++){
            auto vec3f = epiline12_vec.at<cv::Vec3f>(0,i);
            cv::Mat_<double> epiline_double = (cv::Mat_<double>(3,1) << double(vec3f(0)), double(vec3f(1)), double(vec3f(2))); 
            cv::Mat_<double> homo_xc2 = (cv::Mat_<double>(3,1)<< Points_idx2_u[i].x, Points_idx2_u[i].y, 1);
            auto err_dist = abs(homo_xc2.dot(epiline_double));
            err_dist_sum += err_dist;
        }
        cout<<"err dist 2D(px) "<<err_dist_sum/static_cast<double>(particles_vsc.size())<<endl;

        err_betw_cams[cam1->i_cam_][cam2->i_cam_] += err_dist_sum/static_cast<double>(particles_vsc.size());
    }

}

void Calibration::collectSamplePointsForVSC(int it_seq, Camera* cam,
                                            const std::vector<cv::Point2d>& PTVPoints,
                                            const std::vector<Particle_ptr>& particles_vsc,
                                            double max_threshold,
                                            std::vector<cv::Point3d>& obj, std::vector<cv::Point2d>& img){

    cout<<"For cam "<<cam->i_cam_<<endl;

    std::vector<cv::Point2d> ProjPoints;
    for(const auto& pv: particles_vsc){
        ProjPoints.push_back( cam->mappingFunction(pv->getCurrentXcoord(it_seq)) );
    }

    auto ProjPoints_cloud = pcl::make_shared<pcl::PointCloud<pcl::PointXY>>();
    auto pt_d2 = pcl::PointXY();
    for(const auto& pt: ProjPoints){
        pt_d2.x = pt.x;
        pt_d2.y = pt.y;
        ProjPoints_cloud->push_back(pt_d2);
    }

    auto ProjPoints_cloud_kdtree = pcl::KdTreeFLANN<pcl::PointXY>();
    ProjPoints_cloud_kdtree.setInputCloud(ProjPoints_cloud);

    std::vector<int> VSCPoints_flag(particles_vsc.size(), 1);

    double max_threshold_vsc = 0.01;

    cout<<"start with max_threshold_vsc ";
    while(max_threshold_vsc<=max_threshold){
        cout<<" "<<max_threshold_vsc;
        for(const auto& xc: PTVPoints){

            pcl::PointXY searchPoint;
            searchPoint.x = xc.x;
            searchPoint.y = xc.y;

            std::vector<int>   pointIdxNKNSearch(1);
            std::vector<float> pointNKNSquaredDistance(1);

            if( ProjPoints_cloud_kdtree.nearestKSearch(searchPoint, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
            {
                if(sqrt(pointNKNSquaredDistance[0])<max_threshold_vsc){
                    if(VSCPoints_flag[pointIdxNKNSearch[0]] != 0){
                        VSCPoints_flag[pointIdxNKNSearch[0]] = 0;
                        ProjPoints[pointIdxNKNSearch[0]] = xc;
                    }
                }
            }
        }

        max_threshold_vsc*=2;

        if(std::count(VSCPoints_flag.begin(), VSCPoints_flag.end(), 0) > 0.8*particles_vsc.size()){
            max_threshold_vsc = 10*max_threshold;
        }
    }
    cout<<endl;

    double err=0.;
    for(int i=0; i<ProjPoints.size(); i++){
        if( VSCPoints_flag[i] == 0 ){
            obj.push_back(particles_vsc[i]->getCurrentXcoord(it_seq));
            img.push_back(ProjPoints[i]);
        }
    }
}

void Calibration::volumeSelfCalibration(Camera* cam, const std::vector<cv::Point3d>& obj, const std::vector<cv::Point2d>& img, 
                                        double PixelPerMmFactor, const cv::Point3d& normalizationFactor){


    double err=0.;
    for(int i=0; i<obj.size(); i++){
        err += cv::norm( img[i] - cam->mappingFunction(obj[i]) );
    }
    cam->num_samples_tot_ = obj.size();
    cout<<"Sample points in VSC: "<<cam->num_samples_tot_<<endl;
    cout<<"Before VSC, mean error is "<<err/static_cast<double>(cam->num_samples_tot_)<<" px"<<endl; 

    if(cam->z_order_==2)        cam->n_coef_=19;
    else if(cam->z_order_==3)   cam->n_coef_=20;

    if(cam->num_samples_tot_>=cam->n_coef_){

        cam->polynomial_type_     = Camera::Polynomial_Type::Single;
        cam->PixelPerMmFactor_    = PixelPerMmFactor;
        cam->normalizationFactor_ = normalizationFactor;

        cv::Mat_<double> design_mat,b_x,b_y,wx,wy,design_vec;

        design_mat.create(cam->num_samples_tot_, cam->n_coef_);

        int num_sample=0;
        for(const auto& Xc: obj){

            auto kX=cam->PixelPerMmFactor_*Xc.x;
            auto kY=cam->PixelPerMmFactor_*Xc.y;
            auto kZ=cam->PixelPerMmFactor_*Xc.z;

            auto X=kX/cam->normalizationFactor_.x;
            auto Y=kY/cam->normalizationFactor_.y;
            auto Z=kZ/cam->normalizationFactor_.z;

            if(cam->n_coef_==19)
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                                                                std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2));
            else
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z,
                                                                std::pow(Y,2)*Z, Xc.x*std::pow(Z,2), Y*std::pow(Z,2), std::pow(Z,3));
            
            design_vec.copyTo( design_mat.row(num_sample) );
            num_sample++;
        }

        b_x.create(cam->num_samples_tot_, 1);
        b_y.create(cam->num_samples_tot_, 1);
        for(int i=0; i<obj.size(); i++){
            auto xc = cv::Point2d(obj[i].x, obj[i].y)*cam->PixelPerMmFactor_ - img[i];
            b_x(i)=xc.x;
            b_y(i)=xc.y;
        }

        wx.create(cam->n_coef_, 1);
        wy.create(cam->n_coef_, 1);

        cv::solve(design_mat, b_x, wx, cv::DECOMP_SVD);
        cv::solve(design_mat, b_y, wy, cv::DECOMP_SVD);

        VectorXd delta_a, delta_b;

        cv::cv2eigen(wx, delta_a);
        cv::cv2eigen(wy, delta_b);

        cam->a_ = delta_a;
        cam->b_ = delta_b;

        err=0.;
        for(int i=0; i<obj.size(); i++){
            err += cv::norm( img[i] - cam->mappingFunction(obj[i]) );
        }
        cout<<"After  VSC, mean error is "<<err/static_cast<double>(cam->num_samples_tot_)<<" px"<<endl; 
    }
    else{
        cout<<"Too few points"<<endl;
        std::abort();
    }
}

std::vector<std::vector<int>>
Calibration::getIdxInEachSubBlock(const std::vector<cv::Point3d>& obj){

    std::vector<double> xgrid_psf(Camera::xgrid_psf_), ygrid_psf(Camera::ygrid_psf_), zgrid_psf(Camera::zgrid_psf_);
    cv::Point3d dXc_psf(Camera::dXc_psf_);

    int num_block=(xgrid_psf.size()-1)*(ygrid_psf.size()-1)*(zgrid_psf.size()-1);
    std::vector<std::vector<int>> idx_block(num_block);

    for(int j=0; j<obj.size(); j++){
        auto Xc = obj[j];
        utils::maintainPositionInRange(Xc, Xc_top_, Xc_bottom_);

        auto pxl=find_if(std::begin(xgrid_psf), std::end(xgrid_psf), [=](const double& x) { return Xc.x-x<dXc_psf.x; } );
        auto pyl=find_if(std::begin(ygrid_psf), std::end(ygrid_psf), [=](const double& y) { return Xc.y-y<dXc_psf.y; } );
        auto pzl=find_if(std::begin(zgrid_psf), std::end(zgrid_psf), [=](const double& z) { return Xc.z-z<dXc_psf.z; } );

        if(pxl==std::end(xgrid_psf)-1) pxl--;
        if(pyl==std::end(ygrid_psf)-1) pyl--;
        if(pzl==std::end(zgrid_psf)-1) pzl--;

        auto xl_idx=std::distance(std::begin(xgrid_psf), pxl);
        auto yl_idx=std::distance(std::begin(ygrid_psf), pyl);
        auto zl_idx=std::distance(std::begin(zgrid_psf), pzl);

        int psf_idx=zl_idx*(xgrid_psf.size()-1)*(ygrid_psf.size()-1) + yl_idx*(xgrid_psf.size()-1) + xl_idx;
        idx_block.at(psf_idx).push_back(j);
    }

    return idx_block;
}

void Calibration::collectSamplePointsForOTF(
#ifdef INTELTBB
        const tbb::concurrent_vector<pcl::PointXYZI>&                  CorrespondentWorldPoints,
        const std::vector<tbb::concurrent_vector<cv::Point2d>>&        CorrespondentImagePoints,
#else
        const std::vector<pcl::PointXYZI>&                             CorrespondentWorldPoints,
        const std::vector<std::vector<cv::Point2d>>&                   CorrespondentImagePoints,
#endif
        std::vector<cv::Point3d>& obj,
        std::vector<std::vector<cv::Point2d>>& img_cams){

    for(int j=0; j<CorrespondentWorldPoints.size(); j++){
        obj.push_back( pcl2cv(CorrespondentWorldPoints[j]) );
        img_cams[0].push_back(CorrespondentImagePoints[0][j]);
        img_cams[1].push_back(CorrespondentImagePoints[1][j]);
        img_cams[2].push_back(CorrespondentImagePoints[2][j]);
        img_cams[3].push_back(CorrespondentImagePoints[3][j]);
    }
}


void Calibration::calibrateOTF(Camera* cam, const OTFCalib_Type& otf_calib_type,
                                const std::vector<cv::Point3d>& obj, const std::vector<cv::Point2d>& img){

    auto idx_block = this->getIdxInEachSubBlock(obj);

    std::vector<int> idx_total;
    if(otf_calib_type==Calibration::OTFCalib_Type::Constant){
        for(int j=0; j<obj.size(); j++){
            idx_total.push_back(j);
        }
    }

    const int i_cam = cam->i_cam();
    cout<<"Calibrate for camera "<<i_cam<<" ########################################################################"<<endl;

    if(obj.size()>100){

        Eigen::Vector4f psf_ini;
        psf_ini << 6,0,6,3000;

        for(int i=0; i<idx_block.size(); i++){
            cout<<"psf idx "<<i<<" #####################################################################"<<endl;
            cout<<"number of particles in current PSF block "<<idx_block[i].size()<<endl;

            auto psf_elem = psf_ini;

            for(int k=0; k<3; k++){
                cout<<"####k "<<k<<" #####Calibrate OTF radius ...#############################"<<endl;
                auto psf_opt = this->calibrateOTFRadiusSingleZoneLMSolver(psf_elem, *cam, idx_block[i], img);
                psf_elem = psf_opt;

                cout<<"####k "<<k<<" #####Calibrate OTF intensity ...##########################"<<endl;
                auto thetai3 = this->calibrateOTFIntensitySingleZoneLMSolver(psf_elem, *cam, idx_block[i], img);
                psf_elem[3] = thetai3;
            
                cam->setPSFelement(psf_elem, i);
            }
        }
    }
    else{
        std::cerr<<"Too much or too few particles for OTF calibration!!!"<<endl;
        std::abort();
    }
}

Eigen::Vector4f Calibration::calibrateOTFRadiusSingleZoneLMSolver(const Eigen::Vector4f& psf_elem, const Camera& cam,
                                                                const std::vector<int>& Idx_InBlock,
                                                                const std::vector<cv::Point2d>& img){

    double initial_x[2]={double(psf_elem[0]), double(psf_elem[2])};
    double x[2]={double(psf_elem[0]), double(psf_elem[2])};

    ceres::Problem problem;

    cv::Mat Irec=cam.Irec().clone();
    cv::Mat_<double> Irec_float;
    Irec.convertTo(Irec_float, CV_64FC1);

    for(int i=0; i<Idx_InBlock.size(); i++){
        auto xc_i=img[ Idx_InBlock[i] ];    

        cv::Mat Imgi_real_i = utils::getLocalPatch(Irec_float, xc_i, cam.n_width(), cam.n_height());

        cv::Mat Imgi_real_float_i;
        Imgi_real_i.convertTo(Imgi_real_float_i, CV_32FC1);

        double E = 1.;

        problem.AddResidualBlock( new ceres::NumericDiffCostFunction<OTFRadiusCostFunctor, ceres::CENTRAL, 1, 2>(
                                        new OTFRadiusCostFunctor(   psf_elem[3], 
                                                                    cam.n_width(),
                                                                    cam.n_height(),
                                                                    Imgi_real_float_i,
                                                                    xc_i,
                                                                    E ) ),
                                 // new ceres::TrivialLoss(),
                                 new ceres::CauchyLoss(0.5),
                                 &x[0]);
    }

    ceres::Solver::Options options;
    // options.minimizer_progress_to_stdout = true;
    options.minimizer_type = ceres::LINE_SEARCH;
    // options.use_nonmonotonic_steps = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    // cout << summary.BriefReport() << "\n";
    // cout << summary.FullReport() << "\n";
    cout << "x0 : " << initial_x[0] << " -> " << x[0] << "\n";
    cout << "x1 : " << initial_x[1] << " -> " << x[1] << "\n";

    auto psf_opt = psf_elem;
    psf_opt[0] = x[0];
    psf_opt[2] = x[1];
    return psf_opt;
}                

double Calibration::calibrateOTFIntensitySingleZoneLMSolver(const Eigen::Vector4f& psf_elem, const Camera& cam,
                                                            const std::vector<int>& Idx_InBlock, 
                                                            const std::vector<cv::Point2d>& img){

    double initial_x=psf_elem[3];
    double x=initial_x;

    ceres::Problem problem;

    auto Irec=cam.Irec().clone();
    cv::Mat_<double> Irec_float;
    Irec.convertTo(Irec_float, CV_64FC1);

    for(int i=0; i<Idx_InBlock.size(); i++){
        auto xc_i=img[ Idx_InBlock[i] ];    

        cv::Mat Imgi_real_i = utils::getLocalPatch(Irec_float, xc_i, cam.n_width(), cam.n_height());

        cv::Mat Imgi_real_float_i;
        Imgi_real_i.convertTo(Imgi_real_float_i, CV_32FC1);

        double E = 1.;

        problem.AddResidualBlock( new ceres::NumericDiffCostFunction<OTFIntensityCostFunctor, ceres::CENTRAL, 1, 1>(
                                        new OTFIntensityCostFunctor(    psf_elem[0],
                                                                        psf_elem[2], 
                                                                        cam.n_width(),
                                                                        cam.n_height(),
                                                                        Imgi_real_float_i,
                                                                        xc_i,
                                                                        E ) ),
                                 // new ceres::TrivialLoss(),
                                 new ceres::CauchyLoss(0.5),
                                 &x);
    }

    ceres::Solver::Options options;
    // options.minimizer_progress_to_stdout = true;
    options.minimizer_type = ceres::LINE_SEARCH;
    // options.use_nonmonotonic_steps = true;
        
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    // cout << summary.BriefReport() << "\n";
    // cout << summary.FullReport() << "\n";
    cout << "x : " << initial_x << " -> " << x << "\n";
    return x;
}

void Calibration::testOTF(const std::vector<Camera_ptr>& cam, int num_samples, const std::vector<int>& cam_seq,
                            const std::vector<cv::Point3d>& obj,
                            const std::vector<std::vector<cv::Point2d>>& img_cams, const cv::Point3d& dXc_px){

    std::vector<int> sample_idx;
    for(int j=0; j<num_samples; j++){
        sample_idx.push_back( int(Random::UniformDistributionNumber(0, obj.size()-1)) );
    }

    double err=0;
    for(int j=0; j<sample_idx.size(); j++){
        auto Xc = obj[sample_idx[j]];
        cout<<"################################Xc "<<Xc<<endl;

        for(int i_cam=0; i_cam<cam.size(); i_cam++){

            int ic=0;
            if(i_cam==cam_seq[0])      ic=0;
            else if(i_cam==cam_seq[1]) ic=1;
            else if(i_cam==cam_seq[2]) ic=2;
            else if(i_cam==cam_seq[3]) ic=3;

            auto xc_real = img_cams[ic][sample_idx[j]];
            auto xc_proj = cam[i_cam]->mappingFunction(Xc);

            cout<<"####################### i_cam "<<i_cam<<endl
                <<"xc_real "<<xc_real<<endl
                <<"xc_proj "<<xc_proj<<endl;

            err += cv::norm(xc_real-xc_proj)/cv::norm(dXc_px);

            auto psf_elem = cam[i_cam]->getPSFelement(Xc);
            cout<<"psf_elem "<<psf_elem.transpose()<<endl;

            double E = 1.;
            cv::Mat  Imgi_smpl_i;
            utils::getImageSmpl(psf_elem, xc_proj, E, cam[i_cam]->n_width(),  cam[i_cam]->n_height(),
                                Imgi_smpl_i);

            auto Irec=cam[i_cam]->Irec().clone();
            cv::Mat_<double> Irec_float;
            Irec.convertTo(Irec_float, CV_64FC1);
            cv::Mat Imgi_real_i = utils::getLocalPatch(Irec_float, xc_real, cam[i_cam]->n_width(), cam[i_cam]->n_height());

            cout<<"Imgi_real_i"<<endl<<Imgi_real_i<<endl
                <<"Imgi_smpl_i"<<endl<<Imgi_smpl_i<<endl;

        }
    }
}

void Calibration::getPolynomialModelParameters(Camera* cam, double PixelPerMmFactor, const cv::Point3d& normalizationFactor){

    if(cam->z_order_==2)        cam->n_coef_=19;
    else if(cam->z_order_==3)   cam->n_coef_=20;
    else                        std::cerr<<"Calibration::getPolynomialModelParameters z_order not supported (only 2 & 3)"<<endl;

    cam->polynomial_type_ = Camera::Polynomial_Type::Single;

    cv::Mat_<double> design_mat,b_x,b_y,wx,wy;
    cv::Mat design_vec;

    cam->num_samples_tot_=0;
    std::vector<int> num_samples_views;
    if(nXc_rig_.x*nXc_rig_.y!=0){
        cam->num_samples_tot_=nXc_rig_.x*nXc_rig_.y*nViews_;
        num_samples_views.assign(nViews_, nXc_rig_.x*nXc_rig_.y);
    }
    else{
        for(int z=0; z<nViews_; z++){
            num_samples_views.push_back(Xcoord_obj_cams_.at(cam->i_cam_).at(z).size());
            cout<<"View "<<z<<" has "<<num_samples_views.back()<<" sample points"<<endl;
            cam->num_samples_tot_+=num_samples_views.back();
        }
    }
    cout<<"Total "<<cam->num_samples_tot_<<" points"<<endl;

    cam->PixelPerMmFactor_ = PixelPerMmFactor;
    cam->normalizationFactor_ = normalizationFactor;

    design_mat.create(cam->num_samples_tot_, cam->n_coef_);

    int num_samples=0;
    for(int z=0; z<nViews_; z++){
        for(int i=0; i<Xcoord_obj_cams_.at(cam->i_cam_).at(z).size(); i++){
            auto Xc=Xcoord_obj_cams_.at(cam->i_cam_).at(z).at(i);
            
            auto kX=cam->PixelPerMmFactor_*Xc.x;
            auto kY=cam->PixelPerMmFactor_*Xc.y;
            auto kZ=cam->PixelPerMmFactor_*Xc.z;

            auto X=kX/cam->normalizationFactor_.x;
            auto Y=kY/cam->normalizationFactor_.y;
            auto Z=kZ/cam->normalizationFactor_.z;

            if(cam->n_coef_==19)
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                                                                std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2));
            else
                design_vec=(cv::Mat_<double>(1,cam->n_coef_) << 1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                                                                X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y,
                                                                X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z,
                                                                std::pow(Y,2)*Z, X*std::pow(Z,2), Xc.y*std::pow(Z,2), std::pow(Z,3));
            
            design_vec.copyTo( design_mat.row(num_samples+i) );
        }
        num_samples+=num_samples_views[z];
    }
        
    num_samples=0;
    b_x.create(cam->num_samples_tot_, 1);
    b_y.create(cam->num_samples_tot_, 1);
    for(int z=0; z<nViews_; z++){
        for(int i=0; i<xcoord_img_.at(cam->i_cam_).at(z).size(); i++){
            b_x(num_samples+i) = cam->PixelPerMmFactor_*Xcoord_obj_cams_[cam->i_cam_][z][i].x - xcoord_img_[cam->i_cam_][z][i].x;
            b_y(num_samples+i) = cam->PixelPerMmFactor_*Xcoord_obj_cams_[cam->i_cam_][z][i].y - xcoord_img_[cam->i_cam_][z][i].y;
        }
        num_samples+=num_samples_views[z];
    }

    wx.create(cam->n_coef_, 1);
    wy.create(cam->n_coef_, 1);

    cv::solve(design_mat, b_x, wx, cv::DECOMP_SVD);
    cv::solve(design_mat, b_y, wy, cv::DECOMP_SVD);

    cv::cv2eigen(wx, cam->a_);
    cv::cv2eigen(wy, cam->b_);
}

void Calibration::generate2DGridsInImageSpaceFromKnownCamera(Camera* cam, int view_zero){
    
    auto n_width_center=cam->n_width_/2;
    auto n_height_center=cam->n_height_/2;

    double LX=Xc_top_.x-Xc_bottom_.x;
    double LY=Xc_top_.y-Xc_bottom_.y;
    double LZ=Xc_top_.z-Xc_bottom_.z;

    cv::Mat rMat0;
    double theta=0.;
    if(cam->i_cam_==0 || cam->i_cam_==1)
        theta=cam->beta_/180.*M_PI;
    else if(cam->i_cam_==2 || cam->i_cam_==3)
        theta=cam->alpha_/180.*M_PI;

    if(cam->i_cam_==0 || cam->i_cam_==1){//Rotation about Y axis
        rMat0=(cv::Mat_<double>(3,3)<< cos(theta), 0, sin(theta),
                                      0, 1, 0,
                                      -sin(theta), 0, cos(theta));
    }
    else if(cam->i_cam_==2 || cam->i_cam_==3){//Rotation about X axis
        rMat0=(cv::Mat_<double>(3,3)<< 1, 0, 0,
                                      0, cos(theta), -sin(theta),
                                      0, sin(theta), cos(theta));
    }

    cv::Mat rMar_righthand;
    if(Right_handed_flag_)
        rMar_righthand=(cv::Mat_<double>(3,3)<< 1,  0,  0,
                                                0, -1,  0,
                                                0,  0, -1);
    else
        rMar_righthand=cv::Mat::eye(3, 3, CV_64F);

    /*
        *********************************************************
       *                                                       **
      *                                                       * *
     *                                                       *  * 
    *********************************************************   *
    *                                                       *   *
    *                                                       *   *
    *                   Y                                   *   *
    *                   ^                                   *   *
    *                   |                                   *   *
                        |                                   *   *
    Y                   |                                   *   *
                        | Right-handed                      *   *
    ^                   o-------> X                         *   *
    |                  /                                    *   *   
    |  /Z             /                                     *  *        
    | /             Z/                                      * *     
    |/  Left-handed                                         **              
    o-------> X *********************************************
    */
    //center volume
    // cv::Mat_<double> tVec0 = (cv::Mat_<double>(3,1)<< -LX/2.-Xc_bottom_.x, -LY/2.-Xc_bottom_.y, -LZ/2.-Xc_bottom_.z);
    // cv::Mat_<double> tVec0 = (cv::Mat_<double>(3,1)<< -LX/2.-Xc_bottom_.x, -LY/2.-Xc_bottom_.y, -LZ-Xc_bottom_.z);
    cv::Mat_<double> tVec0 = (cv::Mat_<double>(3,1)<< -LX/2.-Xc_bottom_.x, -LY/2.-Xc_bottom_.y, -Xc_bottom_.z);
    cv::Mat_<double> tVec1 = (cv::Mat_<double>(3,1)<< 0, 0, cam->Z0_);
    cv::Mat_<double> tVec2 = rMat0*tVec0 + tVec1;

    cv::Mat_<double> tVec = rMar_righthand*tVec2;
    cv::Mat_<double> rMat = rMar_righthand*rMat0;    
    cv::Mat rVec;
    cv::Rodrigues(rMat, rVec);

    cout<<"tVec0"<<endl<<tVec0<<endl;
    cout<<"tVec1"<<endl<<tVec1<<endl;
    cout<<"rMat0"<<endl<<rMat0<<endl;
    cout<<"tVec2"<<endl<<tVec2<<endl;
    cout<<"rMat"<<endl<<rMat<<endl;
    cout<<"rVec"<<endl<<rVec<<endl;
    cout<<"tVec"<<endl<<tVec<<endl;

    cv::Mat cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
    cameraMatrix.at<double>(0,0)=cam->k_*cam->f_;
    cameraMatrix.at<double>(1,1)=cam->l_*cam->f_;
    cameraMatrix.at<double>(0,2)=n_width_center;
    cameraMatrix.at<double>(1,2)=n_height_center;

    cv::Mat distCoeffs;
    if(!cam->distCoeffs_.empty()){
        assert(cam->distCoeffs_.at<double>(2)==0 && cam->distCoeffs_.at<double>(3)==0);
        distCoeffs = cam->distCoeffs_.clone();
    }
    else
        distCoeffs = cv::Mat::zeros(4 ,1, CV_64F); 

    std::vector<cv::Mat> rVecs, tVecs;
    std::vector<std::vector<cv::Point2d>>  xcoord_img_icam;

    if(view_mode_==View_mode_Type::ZVary){
    //mode Z vary with identical R&T
        rVecs.assign(nViews_, rVec.clone());
        tVecs.assign(nViews_, tVec.clone());
    }
    else if(view_mode_==View_mode_Type::ZZero){
    //mode Z=0 with vary R&T
        rVecs.push_back(rVec.clone());
        tVecs.push_back(tVec.clone());
        for(int z=1; z<nViews_; z++){
            rVecs.push_back(rVec.clone());

            cv::Mat_<double> tVec0_z = (cv::Mat_<double>(3,1)<< -LX/2.-Xc_bottom_.x, -LY/2.-Xc_bottom_.y, -LZ/2.-Xc_bottom_.z+dXc_rig_.z*z);
            cv::Mat_<double> tVec2_z = rMat0*tVec0_z + tVec1;
            cv::Mat_<double> tVec_z  = rMar_righthand*tVec2_z;

            tVecs.push_back(tVec_z.clone());
        }
    }

    cam->cameraMatrix_ = cameraMatrix;
    cam->distCoeffs_ = distCoeffs;
    cam->rvecs_=rVecs;
    cam->tvecs_=tVecs;
    cam->R_=rVecs[view_zero];
    cam->T_=tVecs[view_zero];

    for(int z=0; z<nViews_; z++){
        std::vector<cv::Point3d> Xcoord_grid_v;
        std::vector<cv::Point2d> xcoord_grid_v;
        if(view_mode_==View_mode_Type::ZVary)
            Xcoord_grid_v = Xcoord_obj_[z];
        else if(view_mode_==View_mode_Type::ZZero){
            for(auto& Xc: Xcoord_obj_.at(z))
                Xcoord_grid_v.push_back( cv::Point3d( Xc.x, Xc.y, 0. ) );
        }
        cv::projectPoints(Xcoord_grid_v, rVecs[z], tVecs[z], cameraMatrix, distCoeffs, xcoord_grid_v);
        xcoord_img_icam.push_back( xcoord_grid_v );
    }

    xcoord_img_.push_back(xcoord_img_icam);

    //Testing whether the projection out of domain by manually coded pinhole model
    cv::Mat_<double> XWc0 = (cv::Mat_<double>(3,1)<< Xc_bottom_.x, Xc_bottom_.y, Xc_bottom_.z);
    cv::Mat_<double> XWc1 = (cv::Mat_<double>(3,1)<< Xc_top_.x,    Xc_bottom_.y, Xc_bottom_.z);
    cv::Mat_<double> XWc2 = (cv::Mat_<double>(3,1)<< Xc_bottom_.x, Xc_top_.y,    Xc_bottom_.z);
    cv::Mat_<double> XWc3 = (cv::Mat_<double>(3,1)<< Xc_top_.x,    Xc_top_.y,    Xc_bottom_.z);
    cv::Mat_<double> XWc4 = (cv::Mat_<double>(3,1)<< Xc_bottom_.x, Xc_bottom_.y, Xc_top_.z);
    cv::Mat_<double> XWc5 = (cv::Mat_<double>(3,1)<< Xc_top_.x,    Xc_bottom_.y, Xc_top_.z);
    cv::Mat_<double> XWc6 = (cv::Mat_<double>(3,1)<< Xc_bottom_.x, Xc_top_.y,    Xc_top_.z);
    cv::Mat_<double> XWc7 = (cv::Mat_<double>(3,1)<< Xc_top_.x,    Xc_top_.y,    Xc_top_.z);

    std::vector<cv::Mat_<double>> XW{XWc0, XWc1, XWc2, XWc3, XWc4, XWc5, XWc6, XWc7};
    std::vector<cv::Mat_<double>> XC, xc_u, xc_d, xi;

    XC   = this->projectFromWorldFrameToCameraFrame(XW, rMat, tVecs[0]);
    xc_u = this->projectFromCameraFrameToNormalizedImageFrame(XC);
    xc_d = this->distortPoints(xc_u, distCoeffs);
    xi   = this->projectFramNormalizedImageFrameToFocalImageFrame(xc_d, cameraMatrix);

    cout<<str(boost::format("Back Plane  3D O_world | 3D O_camera | 2D O_camera_undistort | 2D O_camera_distort | 2D O_image"))<<endl;
    cout<<str(boost::format("---------------------------------------------------------------"))<<endl;
    cout<<str(boost::format("Bottomleft  %1% %2% %3% %4% %5%") %  XW[0].t() % XC[0].t() % xc_u[0].t() % xc_d[0].t() % xi[0].t() )<<endl;
    cout<<str(boost::format("Bottomright %1% %2% %3% %4% %5%") %  XW[1].t() % XC[1].t() % xc_u[1].t() % xc_d[1].t() % xi[1].t() )<<endl;
    cout<<str(boost::format("Topleft     %1% %2% %3% %4% %5%") %  XW[2].t() % XC[2].t() % xc_u[2].t() % xc_d[2].t() % xi[2].t() )<<endl;
    cout<<str(boost::format("Topright    %1% %2% %3% %4% %5%") %  XW[3].t() % XC[3].t() % xc_u[3].t() % xc_d[3].t() % xi[3].t() )<<endl;
    cout<<str(boost::format("Front Plane 3D O_world | 3D O_camera | 2D O_camera_undistort | 2D O_camera_distort | 2D O_image"))<<endl;
    cout<<str(boost::format("---------------------------------------------------------------"))<<endl;
    cout<<str(boost::format("Bottomleft  %1% %2% %3% %4% %5%") %  XW[4].t() % XC[4].t() % xc_u[4].t() % xc_d[4].t() % xi[4].t() )<<endl;
    cout<<str(boost::format("Bottomright %1% %2% %3% %4% %5%") %  XW[5].t() % XC[5].t() % xc_u[5].t() % xc_d[5].t() % xi[5].t() )<<endl;
    cout<<str(boost::format("Topleft     %1% %2% %3% %4% %5%") %  XW[6].t() % XC[6].t() % xc_u[6].t() % xc_d[6].t() % xi[6].t() )<<endl;
    cout<<str(boost::format("Topright    %1% %2% %3% %4% %5%") %  XW[7].t() % XC[7].t() % xc_u[7].t() % xc_d[7].t() % xi[7].t() )<<endl;

    for(auto pi: xi) assert(pi(0)>0 && pi(0)<cam.n_width_-1 &&  pi(1)>0 &&  pi(1)<cam.n_height_-1);
}

std::vector<cv::Mat_<double>> Calibration::projectFromWorldFrameToCameraFrame(const std::vector<cv::Mat_<double>>& XW, const cv::Mat& rMat, const cv::Mat& tVec){
    std::vector<cv::Mat_<double>> XC;
    for(auto X: XW){
        cv::Mat C = rMat*X+tVec;
        XC.push_back(C);
    }
    return XC;
}

std::vector<cv::Mat_<double>> Calibration::projectFromCameraFrameToNormalizedImageFrame(const std::vector<cv::Mat_<double>>& XC){
    std::vector<cv::Mat_<double>> xc_u;
    for(auto X: XC){
        auto xu=X(0)/X(2);
        auto yu=X(1)/X(2);

        cv::Mat_<double> pu=(cv::Mat_<double>(2,1)<< xu, yu);
        xc_u.push_back(pu);
    }
    return xc_u;
}

std::vector<cv::Mat_<double>> Calibration::distortPoints(const std::vector<cv::Mat_<double>>& xc, const cv::Mat& distCoeffs){
    std::vector<cv::Mat_<double>> xc_d;
    for(auto x: xc){
        double r_sq = x(0)*x(0)+x(1)*x(1);

        auto xd = x(0)*(1. + distCoeffs.at<double>(0)*r_sq + distCoeffs.at<double>(3)*std::pow(r_sq,2) + distCoeffs.at<double>(4)*std::pow(r_sq,3));
        auto yd = x(1)*(1. + distCoeffs.at<double>(0)*r_sq + distCoeffs.at<double>(3)*std::pow(r_sq,2) + distCoeffs.at<double>(4)*std::pow(r_sq,3));

        cv::Mat_<double> pd=(cv::Mat_<double>(2,1)<< xd, yd);
        xc_d.push_back(pd);
    }
    return xc_d;
}

std::vector<cv::Mat_<double>> Calibration::projectFramNormalizedImageFrameToFocalImageFrame(const std::vector<cv::Mat_<double>>& xc, const cv::Mat& cameraMatrix){
    std::vector<cv::Mat_<double>> xi;
    for(auto x: xc){
        auto ix=cameraMatrix.at<double>(0,0)*x(0)+cameraMatrix.at<double>(0,2);
        auto iy=cameraMatrix.at<double>(1,1)*x(1)+cameraMatrix.at<double>(1,2);

        cv::Mat_<double> pi=(cv::Mat_<double>(2,1)<< ix, iy);
        xi.push_back(pi);
    }
    return xi;
}

void Calibration::generate2DGridsInImageSpaceFromCalibratedCamera(const Camera& cam, bool distort_flag){

    std::vector<std::vector<cv::Point2d>>  xcoord_img_icam;
    std::vector<cv::Point3d> Xcoord_obj_zeroZ;

    cv::Mat distCoeffs = (distort_flag ? cam.distCoeffs_ : 
                                        (cv::Mat_<double>(8, 1)<< 0, 0, 0, 0, 0, 0, 0, 0) );

    for(int z=0; z<nViews_; z++){
        std::vector<cv::Point2d> xcoord_grid_v;
        for(auto& Xc: Xcoord_obj_cams_[cam.i_cam_][z]){
            Xcoord_obj_zeroZ.push_back(cv::Point3d(Xc.x, Xc.y, 0.));
        }
        cv::projectPoints(Xcoord_obj_cams_[cam.i_cam_][z], cam.rvecs_[z], cam.tvecs_[z], cam.cameraMatrix_, distCoeffs, xcoord_grid_v);
        xcoord_img_icam.push_back( xcoord_grid_v );
    }

    xcoord_img_.push_back(xcoord_img_icam);
}

void Calibration::generate2DGridsInImageSpaceFromKnownPolynomial(Camera* cam){

    std::vector<std::vector<cv::Point2d>>  xcoord_img_icam;

    for(int z=0; z<nViews_; z++){
        std::vector<cv::Point2d> xcoord_grid_v;
        for(auto Xc: Xcoord_obj_cams_[cam->i_cam_][z]){
            xcoord_grid_v.push_back(cam->mappingFunction(Xc));
        }
        xcoord_img_icam.push_back(xcoord_grid_v);
    }

    xcoord_img_.push_back(xcoord_img_icam);

    cam->num_samples_tot_=nXc_rig_.x*nXc_rig_.y*nViews_;
}

void Calibration::checkDomainBoundaryOnImageSpace(const Camera& cam, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){

    auto X0c0 = cv::Point3d(Xc_bottom.x, Xc_bottom.y, Xc_bottom.z);
    auto X0c1 = cv::Point3d(Xc_top.x,    Xc_bottom.y, Xc_bottom.z);
    auto X0c2 = cv::Point3d(Xc_bottom.x, Xc_top.y,    Xc_bottom.z);
    auto X0c3 = cv::Point3d(Xc_top.x,    Xc_top.y,    Xc_bottom.z);

    auto XZc0 = cv::Point3d(Xc_bottom.x, Xc_bottom.y, Xc_top.z);
    auto XZc1 = cv::Point3d(Xc_top.x,    Xc_bottom.y, Xc_top.z);
    auto XZc2 = cv::Point3d(Xc_bottom.x, Xc_top.y,    Xc_top.z);
    auto XZc3 = cv::Point3d(Xc_top.x,    Xc_top.y,    Xc_top.z);

    auto x0c0 = cam.mappingFunction(X0c0);
    auto x0c1 = cam.mappingFunction(X0c1);
    auto x0c2 = cam.mappingFunction(X0c2);
    auto x0c3 = cam.mappingFunction(X0c3);

    auto xZc0 = cam.mappingFunction(XZc0);
    auto xZc1 = cam.mappingFunction(XZc1);
    auto xZc2 = cam.mappingFunction(XZc2);
    auto xZc3 = cam.mappingFunction(XZc3);

    cout<<str(boost::format("            3D O_world | 2D O_image"))<<endl;
    cout<<str(boost::format("---------------------------------------------------------------"))<<endl;
    cout<<str(boost::format("Bottomleft  %1% %2%") %  X0c0 % x0c0 )<<endl;
    cout<<str(boost::format("Bottomright %1% %2%") %  X0c1 % x0c1 )<<endl;
    cout<<str(boost::format("Topleft     %1% %2%") %  X0c2 % x0c2 )<<endl;
    cout<<str(boost::format("Topright    %1% %2%") %  X0c3 % x0c3 )<<endl;

    cout<<str(boost::format("            3D O_world | 2D O_image"))<<endl;
    cout<<str(boost::format("---------------------------------------------------------------"))<<endl;
    cout<<str(boost::format("Bottomleft  %1% %2%") %  XZc0 % xZc0 )<<endl;
    cout<<str(boost::format("Bottomright %1% %2%") %  XZc1 % xZc1 )<<endl;
    cout<<str(boost::format("Topleft     %1% %2%") %  XZc2 % xZc2 )<<endl;
    cout<<str(boost::format("Topright    %1% %2%") %  XZc3 % xZc3 )<<endl;
}

void Calibration::generate3DPlanerPointsInObjectSpace(void){

    cv::Point3d Xc_top_round, Xc_bottom_round;
    Xc_top_round   =Xc_top_;
    Xc_bottom_round=Xc_bottom_;

    dXc_rig_.x=(Xc_top_round.x-Xc_bottom_round.x)/static_cast<double>(nXc_rig_.x-1);
    dXc_rig_.y=(Xc_top_round.y-Xc_bottom_round.y)/static_cast<double>(nXc_rig_.y-1);
    dXc_rig_.z=(Xc_top_round.z-Xc_bottom_round.z)/static_cast<double>(nViews_-1);

    cout<<"dXc_rig_"<<endl<<dXc_rig_<<endl;

    VectorXd Xgrid, Ygrid;

    Xgrid.setLinSpaced(nXc_rig_.x, 0., Xc_top_round.x-Xc_bottom_round.x);
    Ygrid.setLinSpaced(nXc_rig_.y, 0., Xc_top_round.y-Xc_bottom_round.y);

    cout<<"Xgrid"<<endl<<Xgrid.transpose()<<endl;
    cout<<"Ygrid"<<endl<<Ygrid.transpose()<<endl;

    MatrixXd XXgrid,YYgrid;
    XXgrid=Xgrid.transpose().replicate(nXc_rig_.y, 1);
    YYgrid=Ygrid.replicate(1, nXc_rig_.x);

    std::vector<cv::Point3d> Xcoord_grid_v;
    double theta=0.;

    //Rotation about Y axis
    // rMat=(cv::Mat_<double>(3,3)<< cos(theta), 0, sin(theta),
    //                            0, 1, 0,
    //                            -sin(theta), 0, cos(theta));
    //Rotation about X axis
    // rMat=(cv::Mat_<double>(3,3)<< 1, 0, 0,
    //                        0, cos(theta), -sin(theta),
    //                        0, sin(theta), cos(theta));

    MatrixXd XXgrid_rot, YYgrid_rot, ZZgrid_rot;

    VectorXd  theta_eigen1, theta_eigen2, theta_eigen3;
    theta_eigen1.setLinSpaced(7, 0, 60);
    theta_eigen2.setLinSpaced(7, 0, 30);
    theta_eigen3.setLinSpaced(7, 120, 180);

    for(int v=0; v<theta_eigen1.size(); v++){

        theta = theta_eigen1(v)/180.*M_PI;
        
        XXgrid_rot = cos(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.x;
        YYgrid_rot = YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.y;
        ZZgrid_rot = sin(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.z;

        for(int i=0; i<nXc_rig_.x; i++)
            for(int j=0; j<nXc_rig_.y; j++)
                Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

        Xcoord_obj_.push_back( Xcoord_grid_v );
        Xcoord_grid_v.clear();
    }

    nViews_ = Xcoord_obj_.size();
    Xcoord_obj_cams_.assign(n_cam_, Xcoord_obj_);
}

void Calibration::getPinholeModelParameters(const std::string& mf_file, Camera* cam, Dataset* dataset){
    dataset->readPinholeMF(mf_file, cam);

    cout<<"cam->cameraMatrix_"<<endl<<cam->cameraMatrix_<<endl
        <<"cam->distCoeffs_"<<endl<<cam->distCoeffs_<<endl
        <<"cam->R_"<<endl<<cam->R_<<endl
        <<"cam->T_"<<endl<<cam->T_<<endl;

    cam->rvecs_.assign(nViews_, cam->R_);
    cam->tvecs_.assign(nViews_, cam->T_);   
}

void Calibration::getPinholeModelParameters(Camera* cam, int opencv_calib_flag, int view_zero, double f, double k){
    
    double skew=0.;

   if(f!=0 && k!=0){
        cam->f_=f;
        cam->k_=k;
    }
    
    cv::Mat cameraMatrix;
    cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
    cameraMatrix.at<double>(0,0)=cam->k_*cam->f_;
    cameraMatrix.at<double>(1,1)=cam->k_*cam->f_;
    cameraMatrix.at<double>(0,1)=skew;
    cameraMatrix.at<double>(0,2)=cam->n_width_/2;
    cameraMatrix.at<double>(1,2)=cam->n_height_/2;

    cv::Mat distCoeffs = (cv::Mat_<double>(4, 1)<< 0, 0, 0, 0);

    cout<<"Prior"<<endl;
    cout<<"cameraMatrix"<<endl<<cameraMatrix<<endl
        <<"distCoeffs"<<endl<<distCoeffs<<endl;

    cam->num_samples_tot_=0;
    if(nXc_rig_.x*nXc_rig_.y!=0){
        cam->num_samples_tot_=nXc_rig_.x*nXc_rig_.y*nViews_;
    }
    else{
        for(int z=0; z<nViews_; z++){
            cam->num_samples_tot_+=Xcoord_obj_cams_.at(cam->i_cam_).at(z).size();
        }
    }
    cout<<"Total "<<cam->num_samples_tot_<<" points"<<endl;

    std::vector<cv::Mat> rVec, tVec;
    cv::Mat stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors;

    auto Xcoord_objf=changeXcoordDoubleToFloat(Xcoord_obj_cams_.at(cam->i_cam_));
    auto xcoord_imgf_icam=changexcoordDoubleToFloat(xcoord_img_.at(cam->i_cam_));

    opencv_calib_flag_ = opencv_calib_flag;

    double res=cv::calibrateCamera(Xcoord_objf, xcoord_imgf_icam, cv::Size(cam->n_width_, cam->n_height_),
                        cameraMatrix, distCoeffs, rVec, tVec,
                        stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors,
                        opencv_calib_flag_|cv::CALIB_USE_INTRINSIC_GUESS,
                        cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 120, 1e-12) );

    cam->cameraMatrix_=cameraMatrix;
    cam->distCoeffs_=distCoeffs;

    cam->ZViews_=ZViews_;

    cam->rvecs_=rVec;
    cam->tvecs_=tVec;

    cout<<"Posterior"<<endl;
    cout<<"cam->cameraMatrix_"<<endl<<cam->cameraMatrix_<<endl
        <<"cam->distCoeffs_"<<endl<<cam->distCoeffs_<<endl;

    for(int v=0; v<nViews_; v++){
        cout<<"View "<<v<<endl;
        cout<<"cam->rvec "<<endl<<cam->rvecs_[v]<<endl;
        cv::Mat rmat;
        cv::Rodrigues(cam->rvecs_[v], rmat);
        cout<<"rmat "<<endl<<rmat<<endl;
        cout<<"cam->tvec "<<endl<<cam->tvecs_[v]<<endl; 
    }

    cout<<"ZViews_[view_zero] "<<ZViews_[view_zero]<<endl;

    cam->R_=rVec[view_zero];
    cam->T_=tVec[view_zero];

    cout<<"Calibration mean error "<<res<<endl;
    cout<<"perViewErrors"<<endl<<perViewErrors<<endl;

    this->testSamplePoints(cam, view_zero, true, false);
    this->testSamplePoints(cam, view_zero, false, false);
}

void Calibration::testSamplePoints(Camera* cam, int view_zero, bool zero_Z_flag, bool verbal){
    
    cout<<"####################################test sample points"<<endl;
    double err=0.;
    for(int z=0; z<nViews_; z++){
        double err_view=0;
        if(verbal) cout<<"View "<<z<<endl;
        for(int i=0; i<Xcoord_obj_cams_[cam->i_cam_][z].size(); i++){
            auto Xc = Xcoord_obj_cams_[cam->i_cam_][z][i];

            std::vector<cv::Point3d> p3_in_cv;
            std::vector<cv::Point2d> p2_out;

            if(zero_Z_flag) 
                p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) );
            else{
                if(z==view_zero) p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + ZViews_[z]*cam->deltaX_ );
                // else              p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + (ZViews_[z]-ZViews_[z-1])*cam->deltaX_ );
                else{
                    if(z==0)     p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + (ZViews_[z]-ZViews_[1])*cam->deltaX_ );
                    else         p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) + (ZViews_[z]-ZViews_[z-1])*cam->deltaX_ );
                }
            }

            if(zero_Z_flag) 
                cv::projectPoints(p3_in_cv, cam->rvecs_[z], cam->tvecs_[z], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
            else{
                if(z==view_zero) cv::projectPoints(p3_in_cv, cam->R_, cam->T_, cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                // else              cv::projectPoints(p3_in_cv, cam->rvecs_[z-1], cam->tvecs_[z-1], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                else{
                    if(z==0)     cv::projectPoints(p3_in_cv, cam->rvecs_[1], cam->tvecs_[1], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                    else         cv::projectPoints(p3_in_cv, cam->rvecs_[z-1], cam->tvecs_[z-1], cam->cameraMatrix_, cam->distCoeffs_, p2_out);
                }
            }

            auto xc = xcoord_img_[cam->i_cam_][z][i];
            auto xc_pinh = (true ? p2_out[0] : cam->mappingFunction(Xc));

            err+=cv::norm(xc_pinh-xc);
            err_view+=cv::norm(xc_pinh-xc);

            if(verbal)  cout<<"Xc "<<p3_in_cv[0]<<"xc "<<xc<<" xc_pinh "<<xc_pinh<<endl;
        }
    
        cout<<"perViewError "<<ZViews_[z]<<" : "<<err_view/static_cast<double>(Xcoord_obj_cams_[cam->i_cam_][z].size())<<endl;
    }

    cout<<"Calibration mean error "<<err/static_cast<double>(cam->num_samples_tot_)<<endl;
    if(!zero_Z_flag && err/static_cast<double>(cam->num_samples_tot_) < 1) cout<<"smaller than 1"<<endl;
}

void Calibration::getStereoCalibrationParameters(Camera* cam1, Camera* cam2){

    if(cam1->i_cam_==cam2->i_cam_){
        cam1->R_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
        cam1->T_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
        cam1->E_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
        cam1->F_betw_cams_.push_back(cv::Mat::zeros(cv::Size(1,1), CV_32FC1));
    }
    else{
        cv::Mat  Camera12Rotation, Camera12Translation, EssentialMatrix, FundementalMatrix;

       // if(source_==Calibration::FromVirtualCamera || source_==Calibration::FromLPTChallenge){
            //Note: stereocalibrate only takes float32 types
        auto Xcoord_objf=changeXcoordDoubleToFloat(Xcoord_obj_cams_.at(cam1->i_cam_));
        auto xcoord_imgf_icam1=changexcoordDoubleToFloat(xcoord_img_.at(cam1->i_cam_));
        auto xcoord_imgf_icam2=changexcoordDoubleToFloat(xcoord_img_.at(cam2->i_cam_));

        double rms = cv::stereoCalibrate(Xcoord_objf, xcoord_imgf_icam1, xcoord_imgf_icam2, 
                                cam1->cameraMatrix_, cam1->distCoeffs_, cam2->cameraMatrix_, cam2->distCoeffs_, 
                                cv::Size(cam1->n_width_, cam1->n_height_), 
                                Camera12Rotation, Camera12Translation, EssentialMatrix, FundementalMatrix,
                                opencv_calib_flag_|cv::CALIB_FIX_INTRINSIC|cv::CALIB_FIX_FOCAL_LENGTH,
                                cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 60, 1e-6));

        cout<<"StereoRotation"<<endl
            <<Camera12Rotation<<endl
            <<"StereoTranslation"<<endl
            <<Camera12Translation<<endl
            <<"EssentialMatrix"<<endl
            <<EssentialMatrix<<endl
            <<"FundementalMatrix"<<endl
            <<FundementalMatrix<<endl;

            //Manuel computing
            // cv::Mat EssentialMatrix2, FundementalMatrix2, rmat1, rmat2;
            // cv::Rodrigues(cam1->R_, rmat1);
            // cv::Rodrigues(cam2->R_, rmat2);

            // this->computeEssentialMatrix(rmat1, rmat2, cam1->T_, cam2->T_, EssentialMatrix2);

            // FundementalMatrix2   = cam2->cameraMatrix_.inv().t()*EssentialMatrix2*cam1->cameraMatrix_.inv();
            // FundementalMatrix2   = FundementalMatrix2*1./FundementalMatrix2.at<double>(2,2);

            // cout<<"EssentialMatrix2"<<endl
            //  <<EssentialMatrix2<<endl
            //  <<"FundementalMatrix2"<<endl
            //  <<FundementalMatrix2<<endl;
            //Manuel computing 
        // }
        // else{
        //     cv::Mat rmat1, rmat2;
        //     cv::Rodrigues(cam1->R_, rmat1);
        //     cv::Rodrigues(cam2->R_, rmat2);

        //     this->computeEssentialMatrix(rmat1, rmat2, cam1->T_, cam2->T_, EssentialMatrix);

        //     FundementalMatrix = cam2->cameraMatrix_.inv().t()*EssentialMatrix*cam1->cameraMatrix_.inv();
        //     FundementalMatrix = FundementalMatrix*1./FundementalMatrix.at<double>(2,2); 
        // }

        cam1->R_betw_cams_.push_back( Camera12Rotation );
        cam1->T_betw_cams_.push_back( Camera12Translation );
        cam1->E_betw_cams_.push_back( EssentialMatrix );
        cam1->F_betw_cams_.push_back( FundementalMatrix );
    }
}

void Calibration::computeEssentialMatrix(const cv::Mat& rmat1, const cv::Mat& rmat2, const cv::Mat& t1, const cv::Mat& t2, cv::Mat& EssentialMatrix){

    cv::Mat Camera12Rotation    = rmat2*rmat1.inv();
    cv::Mat Camera12Translation = t2 - Camera12Rotation*t1;

    cv::Mat TT=(cv::Mat_<double>(3,3)<< 0, -Camera12Translation.at<double>(2), Camera12Translation.at<double>(1),
                                        Camera12Translation.at<double>(2), 0, -Camera12Translation.at<double>(0),
                                        -Camera12Translation.at<double>(1), Camera12Translation.at<double>(0), 0); 

    EssentialMatrix = TT*Camera12Rotation;
}

void Calibration::setPSFParams(Camera* cam, double theta){

    Eigen::Vector4f theta_ijk;
    theta_ijk << theta, 0, theta, 1;
    theta_ijk[3] = Camera::psf_type_==Camera::psf_Type::ErrorFunction ? 1.05 : 1.;

    int nX=5, nY=5, nZ=2;
    cam->nXcPSF_=cv::Point3i(nX, nY, nZ);
    cam->theta_.assign(nX*nY*nZ, theta_ijk);
}

void Calibration::readPSFParams(const std::string& otf_file, Camera* cam, Dataset* dataset){

    dataset->readOTF(otf_file, cam);
}

void Calibration::computeMappingFunctionError(const Camera& cam, bool pinhole_flag){
    
    double err=0.;
    for(int z=0; z<nViews_; z++){
        for(int i=0; i<Xcoord_obj_cams_.at(cam.i_cam_).at(z).size(); i++){
            auto Xc = Xcoord_obj_cams_[cam.i_cam_][z][i];
            auto xc = cam.mappingFunction(Xc, false, pinhole_flag);
            err+=cv::norm(xc - xcoord_img_[cam.i_cam_][z][i]);
        }
    }

    cout<<"Mapping function mean error "<<err/static_cast<double>(cam.num_samples_tot_)<<endl;
}

void Calibration::saveMarkPositionTable(const std::string& dstdir){

    std::string calibdir(dstdir+"/calib");
    if(!boost::filesystem::exists(calibdir)){
        boost::filesystem::create_directories(calibdir);
    }

    std::vector<double> x1_v, y1_v, x2_v, y2_v, x3_v, y3_v, x4_v, y4_v, X_v, Y_v, Z_v;

    auto calibFileName=str(boost::format("%1%/calib_points.npz") % calibdir );

    for(int z=0; z<nViews_; z++){
        for(int i=0; i<nXc_rig_.x; i++){
            for(int j=0; j<nXc_rig_.y; j++){
                auto Xc = Xcoord_obj_cams_[0][z][i*nXc_rig_.y+j];
                auto xc = xcoord_img_[0][z][i*nXc_rig_.y+j];
                X_v.push_back(Xc.x);
                Y_v.push_back(Xc.y);
                Z_v.push_back(Xc.z);
                x1_v.push_back(xc.x);
                y1_v.push_back(xc.y);
                xc = xcoord_img_[1][z][i*nXc_rig_.y+j];
                x2_v.push_back(xc.x);
                y2_v.push_back(xc.y);
                xc = xcoord_img_[2][z][i*nXc_rig_.y+j];
                x3_v.push_back(xc.x);
                y4_v.push_back(xc.y);
                xc = xcoord_img_[3][z][i*nXc_rig_.y+j];
                x4_v.push_back(xc.x);
                y4_v.push_back(xc.y);
            }
        }
    }

    auto NX = static_cast<long unsigned int>(X_v.size());
    cnpy::npz_save(calibFileName, "X", &X_v[0], {NX,1}, "w");
    cnpy::npz_save(calibFileName, "Y", &Y_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "Z", &Z_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x1", &x1_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y1", &y1_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x2", &x2_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y2", &y2_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x3", &x3_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y3", &y4_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "x4", &x4_v[0], {NX,1}, "a");
    cnpy::npz_save(calibFileName, "y4", &y4_v[0], {NX,1}, "a");
}
