#include "../LAPIV.hpp"

LAPIV::LAPIV(){}

LAPIV::~LAPIV(){}

LAPIV::LAPIV(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag,
        int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
        double pixel_lost, double dtObs, 
        const cv::Point3d& dXc_px, const std::string& res_path, int eval_window,
        int n_ens, double regul_const, int outer_max_iter, bool local_anlysis_flag, 
        bool resample_flag, const cv::Point3d& sigmaXcRatio, double sigmaE, bool joint_intensity_flag,
        const cv::Point3d& sigmaV, double corr_length)
        :ENS(method_type, control_vector_type, intensity_control_flag,
             n_part, n_part_per_snapshot, Xc_top, Xc_bottom,
             pixel_lost, dtObs,
             dXc_px, res_path, eval_window,
             false, 0, 0, 0,
             n_ens, regul_const, outer_max_iter, local_anlysis_flag,
             resample_flag, sigmaXcRatio, sigmaE, joint_intensity_flag),
             sigmaV_(sigmaV), corr_length_(corr_length)
        {
            auto res_velo_path=res_path+"/velo";
            if(!boost::filesystem::exists(res_velo_path))
                boost::filesystem::create_directories(res_velo_path);
        }

void LAPIV::initialization(std::vector<Particle_ptr>& particles_ana, 
                          int it_seq_cnt, const std::vector<cv::Point3d>& velocity_fields_bkg, 
                          const cv::Point3i& nXc, const cv::Point3d& dXc, const cv::Point3d& dXc_minimal){

    if(!particles_ens_ana_.empty())     particles_ens_ana_.clear();
    if(!velocity_fields_ens_.empty())   velocity_fields_ens_.clear();
    if(!res_glob_.empty())              res_glob_.clear();

    n_particles_=particles_ana.size();
    nXc_=nXc;
    dXc_=dXc;
    dXc_minimal_=dXc_minimal;

    cout<<"particles_ana.size()       "<<particles_ana.size()<<endl
        <<"current grid size          "<<nXc_<<endl
        <<"velocity_fields_bkg.size() "<<velocity_fields_bkg.size()<<endl;

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ana, Xc_top_, Xc_bottom_, true);

    this->initializeEnsembleCore(particles_ana, false, false);
    this->initializeEnsembleVelocity(velocity_fields_bkg);
}

void LAPIV::initializeEnsembleVelocity(const std::vector<cv::Point3d>& velocity_fields){

    for(int n=0; n<n_ens_; n++){
        velocity_fields_ens_.push_back(this->pertubeEnsembleVelocity(velocity_fields));
    }
    velocity_fields_ens_.push_back(velocity_fields);
}

std::vector<cv::Point3d>
LAPIV::pertubeEnsembleVelocity(const std::vector<cv::Point3d>& velocity_fields){

    std::vector<MatrixXd> noiseVx_3D, noiseVy_3D, noiseVz_3D;
    utils::generateCorrelatedNoiseField(nXc_, dXc_minimal_, corr_length_, noiseVx_3D, noiseVy_3D, noiseVz_3D);

    std::vector<cv::Point3d> velocity_fields_pert;
    for(int k=0; k<nXc_.z; k++){
        for(int j=0; j<nXc_.y; j++){
            for(int i=0; i<nXc_.x; i++){
                velocity_fields_pert.push_back( velocity_fields[k*nXc_.x*nXc_.y+j*nXc_.x+i] + 
                                                    cv::Point3d(sigmaV_grid_cnt_.x*noiseVx_3D[k](i,j),
                                                                sigmaV_grid_cnt_.y*noiseVy_3D[k](i,j),
                                                                sigmaV_grid_cnt_.z*noiseVz_3D[k](i,j)) );
            }
        }
    }
    return velocity_fields_pert;
}

void LAPIV::setVelocityNoiseLevel(int grid_rstrt, int grid_num){
    
    int relax_grid = 5;

    sigmaV_grid_     = (grid_num==grid_rstrt ? sigmaV_: sigmaV_*sigmaV_ratio_);
    sigmaV_grid_     = (grid_num<=relax_grid ? sigmaV_grid_: sigmaV_grid_*std::pow(sigmaV_relaxfactor_, grid_num-relax_grid));
    sigmaV_grid_cnt_ = sigmaV_grid_;
    cout<<"Ensemble velocity noise for current grid is "<<sigmaV_grid_cnt_<<endl;
}

void LAPIV::setregul_const(double regul_const_init, int grid_rstrt, int grid_num){

    int    relax_grid = 5;
    double regul_const_max = 0.1;

    regul_const_ = (grid_num==grid_rstrt ? regul_const_init : regul_const_init*10.);
    regul_const_ = (grid_num<=relax_grid ? regul_const_: regul_const_*std::pow(5, grid_num-relax_grid));
    regul_const_ = std::min(regul_const_, regul_const_max);
    cout<<"Regul const for current grid is "<<regul_const_<<endl;
}

void LAPIV::predict(DynModel* dynmodel, int it_seq_cnt){

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ens_ana_.back(), Xc_top_, Xc_bottom_, true);
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);

    cout<<"Before position prediction"<<endl;
    for(int n=0; n<particles_ens_ana_.size(); n++){
        dynmodel->predictNextPaticlesPosition(particles_ens_ana_[n], velocity_fields_ens_[n], it_seq_cnt, false);
    }
    cout<<"After position prediction"<<endl;

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ens_ana_.back(), Xc_top_, Xc_bottom_, true);
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);
    this->maintainPositionInRange(it_seq_cnt);

    cout<<"Before intensity prediction"<<endl;
    this->predictIntensity(it_seq_cnt);
    this->pertubeEnsembleIntensity(it_seq_cnt, false, true);
    cout<<"After intensity prediction"<<endl;
}

void LAPIV::run(int it_seq_cnt, const std::vector<Camera_ptr>& cam,
                DynModel* dynmodel, bool ghost_removal_flag){
    
    assert(warm_start_flag_==false);

    lMax_=0;
    kMax_=outer_max_iter_;

    //set grid center vector
    std::vector<std::pair<int, cv::Point3d>> xyzgrid_tbb;
    for(int l=0; l<nXc_.z; l++){
        for(int j=0; j<nXc_.y; j++){
            for(int i=0; i<nXc_.x; i++){
                auto grid_center = cv::Point3d(  dynmodel->transportRecon()->xgrid()[i],
                                                  dynmodel->transportRecon()->ygrid()[j],
                                                  dynmodel->transportRecon()->zgrid()[l] );
                xyzgrid_tbb.push_back( std::pair<int, cv::Point3d>( l*nXc_.x*nXc_.y + j*nXc_.x + i, grid_center) );
            }
        }
    }

    //make particle pool
    if(!particles_field_map_.empty()) particles_field_map_.clear();
    for(const auto& pe: particles_ens_ana_.back()){
        particles_field_map_.insert({pe->ID(), std::make_unique<Particle>(*pe)});
    }

    std::vector<std::vector<int>>          part_index_vec_all_grids(xyzgrid_tbb.size(), std::vector<int>());
#ifdef INTELTBB
    tbb::parallel_for( tbb::blocked_range<int>(0, xyzgrid_tbb.size()),
            [&](const tbb::blocked_range<int>& r){ for(int i=r.begin(); i!=r.end(); ++i){
#else
    for(int i=0; i<xyzgrid_tbb.size(); i++){
#endif           
        part_index_vec_all_grids[i] = dynmodel->transportRecon()->findParticlesInCurrentPatch(xyzgrid_tbb[i].second, Transport::ParticleSearch_Type::box);
        if(part_index_vec_all_grids[i].size()<=5){
            part_index_vec_all_grids[i].clear();
            part_index_vec_all_grids[i]=dynmodel->transportRecon()->findParticlesInCurrentPatch(xyzgrid_tbb[i].second, Transport::ParticleSearch_Type::radius);
        }
    }
#ifdef INTELTBB
    });
#endif

    for(int k=1; k<outer_max_iter_+1; k++){

        cout<<"-----------------------------------------------------"<<endl;
        cout<<"Outer loop iteration    : "<<k<<endl;
        cout<<"residual begin ite      : "<<res_glob_.back()<<endl;

#ifdef INTELTBB
        tbb::parallel_for( tbb::blocked_range<int>(0, xyzgrid_tbb.size()),
                    [&](const tbb::blocked_range<int>& r){ for(int i=r.begin(); i!=r.end(); ++i){
#else
        for(int i=0; i<xyzgrid_tbb.size(); i++){
#endif
            auto [particles_ens_per_grid, velo_ens_per_grid] = this->initializeEnsemblePerGrid(part_index_vec_all_grids, xyzgrid_tbb[i].first);

            this->predictEnsemblePerGrid(dynmodel, particles_ens_per_grid, velo_ens_per_grid, it_seq_cnt);

            this->projectEnsemblePerGridToImageSpace(particles_ens_per_grid, it_seq_cnt, cam);

            this->updatePerGrid(particles_ens_per_grid, velo_ens_per_grid, xyzgrid_tbb[i].first, cam);
        }
#ifdef INTELTBB
        });
#endif     
        if(resample_flag_){ this->resampleEnsembleVelocity(); }

        this->updateEnsembleOptimalCoordinates(dynmodel, it_seq_cnt);
        
        this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::JointMean, cam);
        this->processingResidualImage(cam, k, particles_ens_ana_.back(), it_seq_cnt, true);
        cout<<"residual after position correction, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;

        //ghost_removal_flag starts when the velocity field is more or less stabilized
        if(intensity_control_flag_ && ghost_removal_flag){
            this->removeGhost(it_seq_cnt, k, cam, true);
        }
        
        if(this->checkConvergence(k, outer_max_iter_, 5e-3))   break;   
    }    
}

std::pair<std::vector<std::vector<Particle_ptr>>, std::vector<cv::Point3d>>
LAPIV::initializeEnsemblePerGrid(const std::vector<std::vector<int>>& part_index_vec_all_grids, int grid_idx){

    std::vector<std::vector<Particle_ptr>> particles_ens_per_grid;
    std::vector<cv::Point3d> velo_ens;

    for(int n=0; n<=n_ens_; n++){
        particles_ens_per_grid.push_back( std::vector<Particle_ptr>() );
        for(const auto& p_i: part_index_vec_all_grids[grid_idx]){
            auto p_cp = std::make_unique<Particle>( *(particles_field_map_[p_i]) );
            // if(p_cp->is_ontrack() && this->checkIfParticleIsToBeOptimized(p_cp.get())){
            //     auto Xcoord_np=p_cp->Xcoord();
            //     for(auto& Xc_np : Xcoord_np){ utils::maintainPositionInRange(Xc_np, Xc_top_, Xc_bottom_); }
            //     particles_ens_per_grid.back().push_back( std::make_unique<Particle>( p_cp->part_index(), p_cp->it_seq_deb(), p_cp->it_seq_fin(), Xcoord_np, p_cp->Ev() ) );
            // }
            // else
            particles_ens_per_grid.back().push_back( std::move(p_cp) );
        }

        velo_ens.push_back( velocity_fields_ens_[n][grid_idx] );
    }

    return std::make_pair( std::move(particles_ens_per_grid), velo_ens);
}

void LAPIV::predictEnsemblePerGrid(DynModel* dynmodel, const std::vector<std::vector<Particle_ptr>>& particles_ens_per_grid, 
                                    const std::vector<cv::Point3d>& velo_ens, int it_seq_cnt){

    for(int i=0; i<particles_ens_per_grid.size(); i++){
        dynmodel->predictNextPaticlesPosition(particles_ens_per_grid[i], velo_ens[i], it_seq_cnt);
    }
}

void LAPIV::projectEnsemblePerGridToImageSpace(const std::vector<std::vector<Particle_ptr>>& particles_ens_per_grid,
                                                int it_seq_cnt, const std::vector<Camera_ptr>& cam){

    for(int i=0; i<particles_ens_per_grid.size(); ++i){
        this->projectParticlesToImageSpace(it_seq_cnt, particles_ens_per_grid[i], cam);
    }
}

void LAPIV::getRawFeaturePerGrid(const std::vector<std::vector<Particle_ptr>>& particles_ens_per_grid,
                                const std::vector<Camera_ptr>& cam){

    for(int p=0; p<particles_ens_per_grid.back().size(); ++p){
        if(particles_ens_per_grid.back()[p]->is_ontrack() && particles_ens_per_grid.back()[p]->Xcoord().size()>=2)
            this->computeCenteredImageLocalAnomalyMatrix(p, cam, particles_ens_per_grid);
    }
}

void LAPIV::getLocalLinearFormOfCostFunctionPerGrid(const std::vector<std::vector<Particle_ptr>>& particles_ens_per_grid,
                                                    const std::vector<Camera_ptr>& cam){

    for(int p=0; p<particles_ens_per_grid.back().size(); ++p){
        if(particles_ens_per_grid.back()[p]->is_ontrack() && particles_ens_per_grid.back()[p]->Xcoord().size()>=2){
            this->getRightHandSide(p, cam, particles_ens_per_grid);
            this->getHessian(p, cam, particles_ens_per_grid);
        }
    }
}

void LAPIV::updatePerGrid(const std::vector<std::vector<Particle_ptr>>& particles_ens_per_grid,
                         const std::vector<cv::Point3d>& velo_ens, int grid_idx, const std::vector<Camera_ptr>& cam){

    this->getRawFeaturePerGrid(particles_ens_per_grid, cam);

    this->getLocalLinearFormOfCostFunctionPerGrid(particles_ens_per_grid, cam);

    MatrixXd hessian, rhs;
    hessian.setZero(n_ens_, n_ens_);
    rhs.setZero(n_ens_, 1);

    for(const auto& p_g : particles_ens_per_grid.back()){
        hessian += p_g->hessian() - regul_const_*MatrixXd::Identity(n_ens_, n_ens_); 
        rhs += p_g->rhs();                      
    }
    hessian=hessian+regul_const_*MatrixXd::Identity(n_ens_, n_ens_);

    VectorXd weight =  this->solve(hessian, rhs);

    if(this->updateLocalEnsembleOptimalVelocityFieldsForCurrentPatch(weight, hessian, velo_ens, grid_idx)) 
        cout<<"abnormal Vx with "<<particles_ens_per_grid.back().size()<<" nearest particles"<<endl;  
}

bool LAPIV::updateLocalEnsembleOptimalVelocityFieldsForCurrentPatch(const VectorXd& gamma, const MatrixXd& hessian, 
                                                                    const std::vector<cv::Point3d>& velo_ens, int grid_idx){

    auto velo = velo_ens.back();
    for(int n=0; n<n_ens_; n++){ velo+=gamma(n)*( velo_ens[n] - velo_ens.back() ); }
    velocity_fields_ens_.back()[grid_idx] = velo;

    Eigen::JacobiSVD<MatrixXd> svd(hessian, Eigen::ComputeThinU | Eigen::ComputeThinV);
    VectorXd temp=svd.singularValues().array().pow(-0.5);
    MatrixXd TM=svd.matrixU()*(temp.asDiagonal())*svd.matrixV();

    for(int n=0; n<n_ens_; n++){
        auto velo_n=velo;
        for(int m=0; m<n_ens_; m++){ velo_n+=TM(n,m)*( velo_ens.at(m) - velo_ens.back() ); }
        velocity_fields_ens_[n][grid_idx]=velo_n;
    }

    return (velo.x > 2. || velo.x < -1); //|| (velo.y > 1. || velo.y < -1) || (velo.z > 1. || velo.z < -1);
}

void LAPIV::updateEnsembleOptimalCoordinates(DynModel* dynmodel, int it_seq_cnt){
        
    cout<<"updateEnsembleOptimalCoordinates member ";
    for(int n=0; n<particles_ens_ana_.size(); n++){
        cout<<" "<<n;
        dynmodel->predictNextPaticlesPosition(particles_ens_ana_.at(n), velocity_fields_ens_.at(n), it_seq_cnt, true);
    }
    cout<<endl;
    this->maintainPositionInRange(it_seq_cnt);
}

void LAPIV::resampleEnsembleVelocity(void){
    cout<<"Re-sample Velocity Ensemble!!!"<<endl;
    cout<<"sigmaV_grid_cnt_ "<<sigmaV_grid_cnt_<<endl;
    assert(velocity_fields_ens_.size()==n_ens_+1);

    for(int n=0; n<n_ens_; n++){
        assert(velocity_fields_ens_[n].size()==nXc_.x*nXc_.y*nXc_.z);
        auto velocity_fields_ens_n_pert = this->pertubeEnsembleVelocity(velocity_fields_ens_[n]);
        velocity_fields_ens_.at(n) = velocity_fields_ens_n_pert;
    }
}

void LAPIV::postProcessing(std::vector<Particle_ptr>& particles_ana,
                            int it_seq_cnt, const std::vector<Camera_ptr>& cam, DynModel* dynmodel){
    
    cout<<"particles_ens_ana_.size() "<<particles_ens_ana_.size()<<endl;
    particles_ens_ana_.back() = std::move(particles_ana);

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ens_ana_.back(), Xc_top_, Xc_bottom_, true);
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);

    dynmodel->predictNextPaticlesPosition(particles_ens_ana_.back(), it_seq_cnt, true);

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ens_ana_.back(), Xc_top_, Xc_bottom_, true);
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);
    this->maintainPositionInRange(it_seq_cnt);

    this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::JointMean, cam);
    this->processingResidualImage(cam, res_glob_.size(), particles_ens_ana_.back(), it_seq_cnt, true);
}

///non-member function

void trimParticles(std::vector<Particle_ptr>& particles_tgt, int it_seq, bool rm_length_one_flag){

    cout<<"particles_tgt.size() before trim: "<<particles_tgt.size()<<endl;

    std::vector<Particle_ptr> particles_dst;

    int min_length = rm_length_one_flag ? 2 : 1;

    for(const auto& pd: particles_tgt){
        pd->checkIfParticleAppearsOnSnapshot(it_seq);
        if(pd->is_ontrack() && pd->length()>=min_length){
            particles_dst.push_back( std::make_unique<Particle>( pd->ID(), it_seq, it_seq, std::vector<cv::Point3d>{pd->Xcoord()[it_seq-pd->it_seq_deb()]}, std::vector<double>{pd->Ev()[it_seq-pd->it_seq_deb()]} ) );
            particles_dst.back()->markAsTriangulated();
        }
    }

    std::vector<Particle_ptr>().swap(particles_tgt);
    particles_tgt = std::move(particles_dst);

    cout<<"particles_tgt.size() after  trim: "<<particles_tgt.size()<<endl;
}

std::vector<cv::Point3d> generateBackgroundVelocityFields(const cv::Point3d& meanVb, const cv::Point3d& sigmaVb, const cv::Point3i& nXc, const cv::Point3d& dXc_minimal, double corr_length){

    std::vector<cv::Point3d> velocity_fields_bkg;

    std::vector<MatrixXd> noiseVx_3D, noiseVy_3D, noiseVz_3D;
    utils::generateCorrelatedNoiseField(nXc, dXc_minimal, corr_length, noiseVx_3D, noiseVy_3D, noiseVz_3D);

    for(int k=0; k<nXc.z; k++){                 
        for(int j=0; j<nXc.y; j++){
            for(int i=0; i<nXc.x; i++){                 
                if(cv::norm(sigmaVb-cv::Point3d(0,0,0))<1e-9)
                    velocity_fields_bkg.push_back( meanVb );
                else
                    velocity_fields_bkg.push_back( meanVb + cv::Point3d(sigmaVb.x*noiseVx_3D[k](i,j), sigmaVb.y*noiseVy_3D[k](i,j), sigmaVb.z*noiseVz_3D[k](i,j)) );
            }
        }
    }

    return velocity_fields_bkg;
}
