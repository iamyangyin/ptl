#include "../DSLPT.hpp"

DSLPT::DSLPT(){}

DSLPT::~DSLPT(){}

DSLPT::DSLPT(SubPixelMethod_Type subPixel_type, double initial_min_intensity_threshold, double filter_threshold, 
        double dilate_threshold, double dilate_surrounding_checker_threshold, double dilate_surrounding_difference_threshold,
        double search_threshold, double Wieneke_threshold, bool verbal_flag, double min_intensity_ratio, 
        const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, const cv::Point3d& dXc_px,
        const std::vector<int>& cam_seq,
        Stereo_Matching_Type stereo_matching_type, int disparity_max, int BM_win_size, Stereo_Cost_Type stereo_cost_type)
        :PTV(subPixel_type, initial_min_intensity_threshold, filter_threshold,
            dilate_threshold, dilate_surrounding_checker_threshold, dilate_surrounding_difference_threshold,
            search_threshold, Wieneke_threshold, verbal_flag, min_intensity_ratio,
            Xc_top, Xc_bottom, dXc_px, 
            cam_seq),
        stereo_matching_type_(stereo_matching_type), disparity_max_(disparity_max), 
        BM_win_size_(BM_win_size), stereo_cost_type_(stereo_cost_type){ half_block_size_=BM_win_size_/2; }

void DSLPT::doTriangulationUsingTwoPoints(const std::vector<Camera_ptr>& cam,
                                            const std::vector<cv::Point2d>& matched_pt1_u, 
                                            const std::vector<cv::Point2d>& matched_pt2_u,
                                            std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_recon_frames){

    particles_recon_frames.push_back(pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>());
    for(int i=0; i<matched_pt1_u.size(); i++){
        particles_recon_frames.back()->push_back( cv2pcl(triang::triangulateTwoPoints(cam, idx1_, idx2_, matched_pt1_u[i], matched_pt2_u[i]), 1) );
    }
}

void DSLPT::doCorrespondentSearchOn3rdAnd4thCameras(const pcl::PointCloud<pcl::PointXYZI>& particles_recon_frame,
                                                const std::vector<Camera_ptr>& cam,
                                                std::vector<std::vector<cv::Point2d>>& pc3_orig, 
                                                std::vector<std::vector<cv::Point2d>>& pc4_orig){

    this->setWieneke_threshold(2.);
    for(auto& pt: particles_recon_frame.points){
        auto pt_cv = cv::Point3d(pt.x, pt.y, pt.z);
        auto idx3_found = this->CorrespondentSearcher(idx3_, cam[idx3_]->mappingFunction(pt_cv));
        auto idx4_found = this->CorrespondentSearcher(idx4_, cam[idx4_]->mappingFunction(pt_cv));
        pc3_orig.push_back(idx3_found);
        pc4_orig.push_back(idx4_found);
    }
}

void DSLPT::doTriangulationUsingLeastSquare(int it_seq, 
                                            const std::vector<Camera_ptr>& cam,
                                            const std::vector<cv::Point2d>& matched_pt1_u, 
                                            const std::vector<cv::Point2d>& matched_pt2_u,
                                            const std::vector<std::vector<cv::Point2d>>& pc3_orig,
                                            const std::vector<std::vector<cv::Point2d>>& pc4_orig,
                                            std::vector<Particle_ptr>& particles_warped,
                                            std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_recon_frames,
                                            bool init_flag){

    particles_recon_frames.push_back(pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>());

    for(int i=0; i<matched_pt1_u.size(); i++){
        if(!pc3_orig[i].empty() && !pc4_orig[i].empty()){
            std::vector<cv::Point2d> pc3_u, pc4_u;
            cv::undistortPoints(pc3_orig[i], pc3_u, cam[idx3_]->cameraMatrix(), cam[idx3_]->distCoeffs(), cv::noArray(), cam[idx3_]->cameraMatrix()); 
            cv::undistortPoints(pc4_orig[i], pc4_u, cam[idx4_]->cameraMatrix(), cam[idx4_]->distCoeffs(), cv::noArray(), cam[idx4_]->cameraMatrix());
            auto Xc = triang::triangulateThreeOrFourPoints(cam, idx1_, idx2_, idx3_, idx4_, 
                                                        matched_pt1_u[i], matched_pt2_u[i],
                                                        pc3_u[0], pc4_u[0]);

            particles_recon_frames.back()->push_back( cv2pcl(Xc, 1.) );

            if(init_flag){
                particles_warped.push_back( std::make_unique<Particle>(i, it_seq, it_seq, 
                                                                        matched_pt1_u[i], matched_pt2_u[i],
                                                                        pc3_u[0], pc4_u[0]) );
            }
            else{
                particles_warped[i]->push_backxcoordElement(pc3_u[0], idx3_, it_seq);
                particles_warped[i]->push_backxcoordElement(pc4_u[0], idx4_, it_seq);
            }
        }
        else{
            if(!pc3_orig[i].empty()){
                std::vector<cv::Point2d> pc3_u;
                cv::undistortPoints(pc3_orig[i], pc3_u, cam[idx3_]->cameraMatrix(), cam[idx3_]->distCoeffs(), cv::noArray(), cam[idx3_]->cameraMatrix()); 
                auto Xc = triang::triangulateThreeOrFourPoints(cam, idx1_, idx2_, idx3_, -1, 
                                                            matched_pt1_u[i], matched_pt2_u[i],
                                                            pc3_u[0]);

                particles_recon_frames.back()->push_back( cv2pcl(Xc, 1.) );

                if(init_flag){
                    particles_warped.push_back( std::make_unique<Particle>(i, it_seq, it_seq, 
                                                                            matched_pt1_u[i], matched_pt2_u[i],
                                                                            pc3_u[0], cv::Point2d(NAN, NAN)) );
                }
                else{
                    particles_warped[i]->push_backxcoordElement(pc3_u[0], idx3_, it_seq);
                    particles_warped[i]->push_backxcoordElement(cv::Point2d(NAN, NAN), idx4_, it_seq);
                }
            }
            else if(!pc4_orig[i].empty()){
                std::vector<cv::Point2d> pc4_u;
                cv::undistortPoints(pc4_orig[i], pc4_u, cam[idx4_]->cameraMatrix(), cam[idx4_]->distCoeffs(), cv::noArray(), cam[idx4_]->cameraMatrix()); 
                auto Xc = triang::triangulateThreeOrFourPoints(cam, idx1_, idx2_, idx4_, -1, 
                                                            matched_pt1_u[i], matched_pt2_u[i],
                                                            pc4_u[0]);
                
                particles_recon_frames.back()->push_back( cv2pcl(Xc, 1.) );

                if(init_flag){
                    particles_warped.push_back( std::make_unique<Particle>(i, it_seq, it_seq, 
                                                                            matched_pt1_u[i], matched_pt2_u[i],
                                                                            pc4_u[0], cv::Point2d(NAN, NAN)) );
                }
                else{
                    particles_warped[i]->push_backxcoordElement(cv::Point2d(NAN, NAN), idx3_, it_seq);
                    particles_warped[i]->push_backxcoordElement(pc4_u[0], idx4_, it_seq);
                }
            }
            else{
               if(init_flag){
                    particles_warped.push_back( std::make_unique<Particle>(i, it_seq, it_seq, 
                                                                            matched_pt1_u[i], matched_pt2_u[i],
                                                                            cv::Point2d(NAN, NAN), cv::Point2d(NAN, NAN)) );
                }
                else{
                    particles_warped[i]->push_backxcoordElement(cv::Point2d(NAN, NAN), idx3_, it_seq);
                    particles_warped[i]->push_backxcoordElement(cv::Point2d(NAN, NAN), idx4_, it_seq);
                }
            }
        }
    }

}

bool DSLPT::applyTracker(const cv::Rect& roi,
                        const cv::Point2d& pc_src,
                        const cv::Mat& img_src,
                        const cv::Mat& img_tgt,
                        cv::Point2d& pc_tgt,
                        cv::Rect& roi_tgt,
                        const cv::Point2f& offset){

    bool temporal_track_succeed_flag = false;

    auto kcf_tracker_temporal = KCFTracker(false, true, false, false);
    kcf_tracker_temporal.init(roi, img_src);
    roi_tgt = kcf_tracker_temporal.update(img_tgt, offset);

    auto patch = img_tgt(roi_tgt).clone();

    double min_res, max_res;
    cv::Point2i maxLoc;
    cv::minMaxLoc(patch, &min_res, &max_res, nullptr, &maxLoc);
    cv::Rect patch_roi;

    if(maxLoc.x==0 || maxLoc.y==0 || maxLoc.x==patch.cols-1 || maxLoc.y==patch.rows-1){
        patch.at<uchar>(maxLoc)=uchar(min_res);
        cv::Point2i maxLoc2=maxLoc;
        cv::minMaxLoc(patch, &min_res, &max_res, nullptr, &maxLoc);
        if(maxLoc2.x==0 || maxLoc2.y==0 || maxLoc2.x==patch.cols-1 || maxLoc2.y==patch.rows-1){
            patch_roi=cv::Rect(roi_tgt.x+maxLoc.x-1, roi_tgt.y+maxLoc.y-1, 3, 3);
        }
        else{
            patch_roi=cv::Rect(roi_tgt.x+maxLoc2.x-1, roi_tgt.y+maxLoc2.y-1, 3, 3);
        }
    }
    else{
        patch_roi=cv::Rect(roi_tgt.x+maxLoc.x-1, roi_tgt.y+maxLoc.y-1, 3, 3);
    }

    auto patch3x3 = img_tgt(patch_roi).clone();

    auto xc = utils::applyGaussianFitToPatch(patch3x3);
    pc_tgt = xc + cv::Point2d(patch_roi.x, patch_roi.y);

    if(isnan(pc_tgt.x) || isnan(pc_tgt.y) || std::abs(pc_tgt.y-pc_src.y)>1.){
        cout<<"For point "<<pc_src<<" ##################"<<endl;
        cout<<"roi "<<roi<<endl;
        cout<<"img_src(roi)"<<endl<<img_src(roi)<<endl;
        cout<<"roi_tgt "<<roi_tgt<<endl;
        cout<<"img_tgt(roi_tgt)"<<endl<<img_tgt(roi_tgt)<<endl;
        cout<<"xc     "<<xc<<endl;
        cout<<"pc_tgt "<<pc_tgt<<endl;
        cout<<"patch"<<endl<<patch<<endl;
        cout<<"patch3x3"<<endl<<patch3x3<<endl;
    }
    else
        temporal_track_succeed_flag = true;

    return temporal_track_succeed_flag;
}

cv::Mat DSLPT::doDisparityComputationUsingSGBM(const cv::Mat& img1_warp, const cv::Mat& img2_warp){

    cv::Mat img_left=img1_warp.clone(), img_right=img2_warp.clone();

    const sint32 width = static_cast<uint32>(img_left.cols);
    const sint32 height = static_cast<uint32>(img_left.rows);

    auto bytes_left = new uint8[width * height];
    auto bytes_right = new uint8[width * height];
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bytes_left[i * width + j] = img_left.at<uint8>(i, j);
            bytes_right[i * width + j] = img_right.at<uint8>(i, j);
        }
    }

    SemiGlobalMatching::SGMOption sgm_option;
    sgm_option.num_paths = 4;
    sgm_option.min_disparity = disparity_min_;
    sgm_option.max_disparity = disparity_max_;
    sgm_option.census_size = SemiGlobalMatching::Census5x5;
    sgm_option.is_check_lr = true;
    sgm_option.lrcheck_thres = 1.0f;
    sgm_option.is_check_unique = true;
    sgm_option.uniqueness_ratio = 0;
    sgm_option.is_remove_speckles = false;
    sgm_option.min_speckle_aera = 50;
    sgm_option.p1 = 0;
    sgm_option.p2_init = 0;
    sgm_option.is_fill_holes = false;

    SemiGlobalMatching sgm;

    printf("SGM Initializing...\n");
    if (!sgm.Initialize(width, height, sgm_option)) {
        std::cout << "SGM初始化失败！" << std::endl;
        std::abort();
    }

    printf("SGM Matching...\n");
    auto disparity_c = new float32[uint32(width * height)]();
    if (!sgm.Match(bytes_left, bytes_right, disparity_c)) {
        std::cout << "SGM匹配失败！" << std::endl;
        std::abort();
    }

    cv::Mat disparity = cv::Mat(height, width, CV_8UC1);
    float min_disp = width, max_disp = -width;
    for (sint32 i = 0; i < height; i++) {
        for (sint32 j = 0; j < width; j++) {
            const float32 disp = disparity_c[i * width + j];
            if (disp != Invalid_Float) {
                min_disp = std::min(min_disp, disp);
                max_disp = std::max(max_disp, disp);
            }
        }
    }
    for (sint32 i = 0; i < height; i++) {
        for (sint32 j = 0; j < width; j++) {
            const float32 disp = disparity_c[i * width + j];
            if (disp == Invalid_Float) {
                disparity.data[i * width + j] = 0;
            }
            else {
                disparity.data[i * width + j] = static_cast<uchar>((disp - min_disp) / (max_disp - min_disp) * 255);
            }
        }
    }

    SAFE_DELETE(disparity_c);
    SAFE_DELETE(bytes_left);
    SAFE_DELETE(bytes_right);

    return disparity;
}

float DSLPT::computeCostAtExactPixelAndRange(uint32* census_left, uint32* census_right, int i, int j, int range, int imgL_cols){
    const auto& census_val_l = census_left[i * imgL_cols + j];
    const auto& census_val_r = census_right[i * imgL_cols + j - range];
    auto cost = sgm_util::Hamming32(census_val_l, census_val_r);
    return static_cast<float>(cost);
}

float DSLPT::computeCostAtExactPixelAndRange(const cv::Mat& imgL, const cv::Mat& imgR, int i, int j, int range){
    float cost = 0;
    // cv::Mat result;
    // cv::matchTemplate(imgL(cv::Rect(j-half_block_size_, i-half_block_size_, BM_win_size_, BM_win_size_)),
    //                     imgR(cv::Rect(min(max(0, j-half_block_size_-range), imgL.cols-1), i-half_block_size_, BM_win_size_, BM_win_size_)),
    //                     result, cv::TM_SQDIFF);
    // auto q_diff = static_cast<float>(result.at<uchar>(0,0));
    // cost += std::exp( -1*sq_diff );  
    for(int l_r = -half_block_size_ + i; l_r <= half_block_size_ + i; l_r++){
        for(int l_c = -half_block_size_ + j; l_c <= half_block_size_ + j; l_c++){
            auto sq_diff = static_cast<float>( std::pow( imgL.at<uchar>(l_r, l_c) - imgR.at<uchar>(l_r, min(max(0, l_c-range), imgL.cols-1)), 2 ) );
            cost += std::exp( -1*sq_diff );
        }
    }
    return cost;
}

void DSLPT::fillCostVolume(const cv::Mat& imgL, const cv::Mat& imgR){
    uint32* censusL=nullptr;
    uint32* censusR=nullptr;
    torch::jit::script::Module net;

    if(stereo_cost_type_==Stereo_Cost_Type::Census){
        auto bytesL = new uint8[imgL.cols * imgL.rows];
        auto bytesR = new uint8[imgL.cols * imgL.rows];
        for (int i = 0; i < imgL.rows; i++) {
            for (int j = 0; j < imgL.cols; j++) {
                bytesL[i * imgL.cols + j] = imgL.at<uint8>(i, j);
                bytesR[i * imgL.cols + j] = imgR.at<uint8>(i, j);
            }
        }
        censusL = new uint32[imgL.cols * imgL.rows]();
        censusR = new uint32[imgL.cols * imgL.rows]();
        sgm_util::census_transform_5x5(bytesL, censusL, imgL.cols, imgL.rows);
        sgm_util::census_transform_5x5(bytesR, censusR, imgL.cols, imgL.rows);
        SAFE_DELETE(bytesL);
        SAFE_DELETE(bytesR);    
    }
    else if(stereo_cost_type_==Stereo_Cost_Type::DeepFeature){
        auto path_to_pt = "/media/yin/Data/stereo_matching_pytorch/traced_net_model_pretrained.pt";
        net = torch::jit::load(path_to_pt, at::kCUDA);
    }

    if(stereo_cost_type_==Stereo_Cost_Type::Census || stereo_cost_type_==Stereo_Cost_Type::Grey){
        for(int i=0+half_block_size_; i<imgL.rows-half_block_size_; i++){
            for(int j=0+half_block_size_; j<imgL.cols-half_block_size_; j++){
                if(imgL.at<uchar>(i,j) != 0){
                    for(int range=disparity_min_; range<disparity_max_; range++){
                        if(stereo_cost_type_==Stereo_Cost_Type::Census){
                            cost_volume_[i][j][range-disparity_min_] = computeCostAtExactPixelAndRange(censusL, censusR, i, j, range, imgL.cols);
                        }
                        else if(stereo_cost_type_==Stereo_Cost_Type::Grey){
                            cost_volume_[i][j][range-disparity_min_] = computeCostAtExactPixelAndRange(imgL, imgR, i, j, range);
                        }
                    }
                }
            }
        }
    }
    else if(stereo_cost_type_==Stereo_Cost_Type::DeepFeature){

        RowMatrixXf inputL_data, inputR_data;
        int num_patches = (imgL.rows-2*half_block_size_)*(imgL.cols-2*half_block_size_);
        inputL_data.resize(num_patches, BM_win_size_*BM_win_size_);
        inputR_data.resize(num_patches, BM_win_size_*BM_win_size_);

        for(int i=0; i<=imgL.rows-BM_win_size_; i++){
            for(int j=0; j<=imgL.cols-BM_win_size_; j++){

                auto imgL_patch = imgL(cv::Rect(j, i, BM_win_size_, BM_win_size_));
                auto imgR_patch = imgR(cv::Rect(j, i, BM_win_size_, BM_win_size_));

                cv::Mat imgL_patch_f, imgR_patch_f;
                imgL_patch.convertTo(imgL_patch_f, CV_32F);
                imgR_patch.convertTo(imgR_patch_f, CV_32F);

                Eigen::MatrixXf imgL_patch_eigen, imgR_patch_eigen;
                cv::cv2eigen(imgL_patch_f, imgL_patch_eigen);
                cv::cv2eigen(imgR_patch_f, imgR_patch_eigen);

                RowMatrixXf imgL_patch_eigen_rowmajor(imgL_patch_eigen), imgR_patch_eigen_rowmajor(imgR_patch_eigen);

                imgL_patch_eigen_rowmajor.resize(1, BM_win_size_*BM_win_size_);
                imgR_patch_eigen_rowmajor.resize(1, BM_win_size_*BM_win_size_);

                inputL_data.row(i*(imgL.cols-BM_win_size_+1) + j) = imgL_patch_eigen_rowmajor;
                inputR_data.row(i*(imgL.cols-BM_win_size_+1) + j) = imgR_patch_eigen_rowmajor;
            }
        }

        int rows = inputL_data.rows();
        int cols = inputL_data.cols();

        auto inputL = torch::from_blob(inputL_data.data(), {rows, cols}).to(torch::kCUDA);   
        auto inputR = torch::from_blob(inputR_data.data(), {rows, cols}).to(torch::kCUDA);

        std::vector<torch::jit::IValue> inputsL, inputsR;
        inputsL.push_back(inputL.view({1, rows, BM_win_size_, BM_win_size_}));
        inputsR.push_back(inputR.view({1, rows, BM_win_size_, BM_win_size_}));

        auto deepL = net.forward(inputsL).toTensor().cpu();
        auto deepR = net.forward(inputsR).toTensor().cpu();

        for(int i=0+half_block_size_; i<imgL.rows-half_block_size_; i++){
            for(int j=0+half_block_size_; j<imgL.cols-half_block_size_; j++){
                if(imgL.at<uchar>(i,j) != 0){
                    int px_idx = (i-half_block_size_)*(imgL.cols-2*half_block_size_)+j-half_block_size_;
                    auto deepL_patch = deepL.select(0, px_idx);

                    for(int range=disparity_min_; range<disparity_max_; range++){
                        if(j-range+disparity_min_>=half_block_size_){
                            auto deepR_patch = deepR.select(0, px_idx-range+disparity_min_);

                            auto p_score  = torch::mean( torch::norm(deepL_patch - deepR_patch) );

                            // cout<<"i "<<i<<" jL "<<j<<" jR "<<j-range<<" p_score "<<p_score<<endl;

                            cost_volume_[i][j][range-disparity_min_] = p_score.item<float>();
                        }
                    }
                }
            }
        }
    }

    if(stereo_cost_type_==Stereo_Cost_Type::Census){
        SAFE_DELETE(censusL);
        SAFE_DELETE(censusR);
    }
}

void DSLPT::winnerTakeAll(const cv::Mat& imgL, cv::Mat& disp_img){
    for(int i=0; i<disp_img.rows; i++){
        for(int j=0; j<disp_img.cols; j++){
            if(imgL.at<uchar>(i,j) != 0){
                auto [winner_cost_iter, max_iter] = std::minmax_element(cost_volume_[i][j].begin(), cost_volume_[i][j].end());
                uchar winner_disp = (uchar)std::distance(std::begin(cost_volume_[i][j]), winner_cost_iter);
                cv::Vec3b winners_disp = cv::Vec3b(winner_disp,winner_disp,winner_disp);

                int width=2;
                //find two more candidates
                *winner_cost_iter = *max_iter;
                if(winner_disp>=width)                           *(winner_cost_iter-1) = *max_iter;
                if(winner_disp<=cost_volume_[i][j].size()-1-width) *(winner_cost_iter+1) = *max_iter;
                auto winner_cost_iter_bis = std::min_element(cost_volume_[i][j].begin(), cost_volume_[i][j].end());
                uchar winner_disp_bis = (uchar)std::distance(std::begin(cost_volume_[i][j]), winner_cost_iter_bis);

                *winner_cost_iter_bis = *max_iter;
                if(winner_disp_bis>=width)                           *(winner_cost_iter_bis-1) = *max_iter;
                if(winner_disp_bis<=cost_volume_[i][j].size()-1-width) *(winner_cost_iter_bis+1) = *max_iter;
                auto winner_cost_iter_ter = std::min_element(cost_volume_[i][j].begin(), cost_volume_[i][j].end());
                uchar winner_disp_ter = (uchar)std::distance(std::begin(cost_volume_[i][j]), winner_cost_iter_ter);

                winners_disp[1] = winner_disp_bis;
                winners_disp[2] = winner_disp_ter;

                disp_img.at<cv::Vec3b>(i,j) = winners_disp;
            }
            else{
                disp_img.at<cv::Vec3b>(i,j) = cv::Vec3b(0,0,0);
            }
        }
    }
}

cv::Mat DSLPT::doDisparityComputationUsingBM(const cv::Mat& img1_warp, const cv::Mat& img2_warp){

    cv::Mat img_left=img1_warp.clone(), img_right=img2_warp.clone();
    int ndisp = disparity_max_ - disparity_min_;

    if(!cost_volume_.empty()){ cost_volume_.clear(); }

    float init_cost_max = 1;
    cost_volume_.assign(img1_warp.rows, std::vector<std::vector<float>>(img1_warp.cols, std::vector<float>(ndisp, init_cost_max)));

    this->fillCostVolume(img_left, img_right);

    cv::Mat disp_img = cv::Mat::zeros(img_left.rows, img_left.cols, CV_8UC3);
    this->winnerTakeAll(img_left, disp_img);

    return disp_img;
}

cv::Mat DSLPT::doDisparityComputationUsingCPU(const cv::Mat& img1_warp, const cv::Mat& img2_warp){

    cv::Mat imgL=img1_warp.clone(), imgR=img2_warp.clone(), disparity;

    if(stereo_matching_type_==Stereo_Matching_Type::BM_cpu){
        disparity = this->doDisparityComputationUsingBM(imgL, imgR);
    }
    else if(stereo_matching_type_==Stereo_Matching_Type::SGBM_cpu){
        disparity = this->doDisparityComputationUsingSGBM(imgL, imgR);
    }

    return disparity;
}

cv::Mat DSLPT::doDisparityComputationUsingCuda(const cv::Mat& img1_warp, const cv::Mat& img2_warp){

    cv::Mat imgL=img1_warp.clone(), imgR=img2_warp.clone(), disparity(imgL.size(), CV_32FC1);
    cv::cuda::GpuMat d_left, d_right;
    d_left.upload(imgL);
    d_right.upload(imgR);

    cv::Ptr<cv::StereoMatcher> matcher;
    cv::cuda::GpuMat d_disp(imgL.size(), CV_32FC1);
    if(stereo_matching_type_==DSLPT::Stereo_Matching_Type::BM_cuda){
        matcher = cv::cuda::createStereoBM(disparity_max_, BM_win_size_);
        matcher->compute(d_left, d_right, d_disp);
    }
    else if(stereo_matching_type_==DSLPT::Stereo_Matching_Type::ConstantSpaceBP_cuda){
        matcher = cv::cuda::createStereoConstantSpaceBP(disparity_max_, 8, 1, 4);
        matcher->compute(d_right, d_left, d_disp);
    }
    d_disp.download(disparity);

    return disparity;
}

cv::Mat DSLPT::computeDisparity(const std::vector<Camera_ptr>& cam,
                                cv::Mat& img1_warp, cv::Mat& img2_warp,
                                bool visu_flag, bool save_flag, const std::string& imgDir, int it_seq){

    auto img1 = cam[idx1_]->Irec().clone();
    auto img2 = cam[idx2_]->Irec().clone(); 

    cv::Mat img1_u, img2_u;
    cv::undistort(img1, img1_u, cam[idx1_]->cameraMatrix(),  cam[idx1_]->distCoeffs());
    cv::undistort(img2, img2_u, cam[idx2_]->cameraMatrix(),  cam[idx2_]->distCoeffs());

    cv::warpPerspective(img1_u, img1_warp, homo1_, cv::Size(cam[idx1_]->n_width(), cam[idx1_]->n_height()));
    cv::warpPerspective(img2_u, img2_warp, homo2_, cv::Size(cam[idx2_]->n_width(), cam[idx2_]->n_height()));

    cv::Mat disparity;
    if(stereo_matching_type_==Stereo_Matching_Type::BM_cuda || stereo_matching_type_==Stereo_Matching_Type::ConstantSpaceBP_cuda)
        disparity = this->doDisparityComputationUsingCuda(img1_warp, img2_warp);
    else if(stereo_matching_type_==Stereo_Matching_Type::BM_cpu|| stereo_matching_type_==Stereo_Matching_Type::SGBM_cpu)
        disparity = this->doDisparityComputationUsingCPU(img1_warp, img2_warp);

    cv::Mat disparity_view, disparity_save;
    if(disparity.type()==CV_8UC1){
        double min, max;
        cv::minMaxLoc(disparity, &min, &max);
        disparity.convertTo(disparity_view, CV_8UC1, 255.0/(max-min), -min);
        disparity_save = disparity.clone();
    }
    else if(disparity.type()==CV_8UC3){
        cv::Mat disparity_channels[3];
        cv::split(disparity, disparity_channels);
        double min, max;
        cv::minMaxLoc(disparity_channels[0], &min, &max);
        disparity_channels[0].convertTo(disparity_view, CV_8UC1, 255.0/(max-min), -min);
        disparity_save = disparity_channels[0].clone();
    }

    if(visu_flag){
        cv::namedWindow( "ic1_img_warp", cv::WINDOW_AUTOSIZE );
        cv::imshow( "ic1_img_warp", img1_warp );
        cv::waitKey(0);    

        cv::namedWindow( "ic2_img_warp", cv::WINDOW_AUTOSIZE );
        cv::imshow( "ic2_img_warp", img2_warp );
        cv::waitKey(0);    

        cv::namedWindow( "disparity_view", cv::WINDOW_AUTOSIZE );
        cv::imshow( "disparity_view", disparity_view );
        cv::waitKey(0);    
    }

    if(save_flag){
        auto mvs_dir=str(boost::format("%1%/cam%2%_mvs") % imgDir % idx1_);
        auto fullname=str(boost::format("%1%/cam_c%2%_it%3$03d.png") % mvs_dir % idx1_ % it_seq );
        cv::imwrite(fullname, img1_warp, utils::compression_params);

        mvs_dir=str(boost::format("%1%/cam%2%_mvs") % imgDir % idx2_);
        fullname=str(boost::format("%1%/cam_c%2%_it%3$03d.png") % mvs_dir % idx2_ % it_seq );
        cv::imwrite(fullname, img2_warp, utils::compression_params);

        mvs_dir=str(boost::format("%1%/cam%2%_mvs") % imgDir % idx1_);
        fullname=str(boost::format("%1%/disparity_c%2%_it%3$03d.png") % mvs_dir % idx1_ % it_seq );
        cv::imwrite(fullname, disparity, utils::compression_params);  
    }

    return disparity;
}

void DSLPT::getHorizontalCameraPairHomographyMatrix(const std::vector<std::vector<cv::Point2f>>& xcoord_img_ic1,
        											const std::vector<std::vector<cv::Point2f>>& xcoord_img_ic2,
        											int nViews, const std::vector<Camera_ptr>& cam){

    cout<<"between camera "<<idx1_<<" and "<<idx2_<<endl;
    std::vector<cv::Point2f> xcoord_imgf_ic1_concat, xcoord_imgf_ic2_concat;
    for(int z=0; z<nViews; z++){
        for(auto& xc: xcoord_img_ic1[z]){
            xcoord_imgf_ic1_concat.push_back(xc);
        }
        for(auto& xc: xcoord_img_ic2[z]){
            xcoord_imgf_ic2_concat.push_back(xc);
        }
    }

    cv::Mat F12 = cv::findFundamentalMat(xcoord_imgf_ic1_concat, xcoord_imgf_ic2_concat);
    // cv::Mat F12 = cam[ic1]->F_betw_cams()[ic2];

    cv::stereoRectifyUncalibrated(xcoord_imgf_ic1_concat, xcoord_imgf_ic2_concat, 
                                F12, cv::Size(cam[idx1_]->n_width(), cam[idx1_]->n_height()),
                                homo1_, homo2_, 1.);

    cout<<"homo1_"<<endl<<homo1_<<endl;
    cout<<"homo2_"<<endl<<homo2_<<endl;

    // rect1_ = cam[idx1_]->cameraMatrix().inv()*H1*cam[idx1_]->cameraMatrix();
    // rect2_ = cam[idx2_]->cameraMatrix().inv()*H2*cam[idx2_]->cameraMatrix();
}

void DSLPT::getImagePeaks(int ic1, int ic2, const std::vector<Camera_ptr>& cam,
					const cv::Mat& img1, const cv::Mat& img2){

    cam[ic1]->setIrec(img1);
	cam[ic2]->setIrec(img2);
	this->clearDataForNextTimeLevel();
	this->readRecordsFromCamera(cam);
	this->findParticleImagePeak(0.5);
}

void DSLPT::searchCorrespondantOn3rdAnd4thCameras(int it_seq,
                            const std::vector<Camera_ptr>& cam,
                            const std::vector<cv::Point2d>& matched_pt1_warped, 
                            const std::vector<cv::Point2d>& matched_pt2_warped,
                            std::vector<Particle_ptr>& particles_warped,
                            bool init_flag,
                            bool eval_flag,
                            const std::vector<Particle_ptr>& particles_bkg){

    cout<<"Do reverse perspectiveTransform"<<endl;
    std::vector<cv::Point2d> matched_pt1_u, matched_pt2_u;
    cv::perspectiveTransform(matched_pt1_warped, matched_pt1_u, homo1_.inv());
    cv::perspectiveTransform(matched_pt2_warped, matched_pt2_u, homo2_.inv());

    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_recon_frames;
    this->doTriangulationUsingTwoPoints(cam, matched_pt1_u, matched_pt2_u, particles_recon_frames);

    if(eval_flag){
        cout<<"Evaluate Against Reference Particle Data"<<endl;
        eval::evaluateIPRParticles(std::vector<int>{it_seq}, dXc_px_, cam, Xc_top_, Xc_bottom_, particles_recon_frames, particles_bkg);
    }

    cout<<"Do least square triangulation"<<endl;
    auto img3 = cam[idx3_]->Irec().clone();
    auto img4 = cam[idx4_]->Irec().clone(); 
    this->getImagePeaks(idx3_, idx4_, cam, img3, img4);

    std::vector<std::vector<cv::Point2d>> pc3_orig, pc4_orig;
    this->doCorrespondentSearchOn3rdAnd4thCameras(*particles_recon_frames[0], cam, pc3_orig, pc4_orig);

    this->doTriangulationUsingLeastSquare(it_seq, cam, matched_pt1_u, matched_pt2_u, pc3_orig, pc4_orig, 
                                        particles_warped, particles_recon_frames, init_flag);

    if(eval_flag){
        cout<<"Evaluate Against Reference Particle Data"<<endl;
        eval::evaluateIPRParticles(std::vector<int>{it_seq}, dXc_px_, cam, Xc_top_, Xc_bottom_, particles_recon_frames, particles_bkg);
    }
}

// void DSLPT::getHorizontalCameraPairHomographyMatrix(const std::vector<Camera_ptr>& cam, int flag, double alpha){

//     //flag=0 horizontal, 1 vertical
//     auto flag_l = (flag==0 || flag==1) ? flag : cv::CALIB_ZERO_DISPARITY;

//     cv::Mat P1, P2, Q;
//     cv::stereoRectify(  cam[idx1_]->cameraMatrix(), cam[idx1_]->distCoeffs(),
//                         cam[idx2_]->cameraMatrix(), cam[idx2_]->distCoeffs(), 
//                         cv::Size(cam[idx1_]->n_width(), cam[idx1_]->n_height()), 
//                         cam[idx1_]->R_betw_cams()[idx2_], cam[idx1_]->T_betw_cams()[idx2_], 
//                         rect1_, rect2_, P1, P2, Q, flag_l, alpha);
// }

// void DSLPT::getHorizontalCameraPairHomographyMatrix(const std::vector<Camera_ptr>& cam){

//     double theta=-15./180.*M_PI;
//     cv::Mat_<double> half_R=(cv::Mat_<double>(3,3)<< cos(theta), 0, sin(theta),
//                                           0, 1, 0,
//                                           -sin(theta), 0, cos(theta));
    
//     auto v1 = cam[idx1_]->T_betw_cams()[idx2_];
//     cv::Mat_<double> v2=(cv::Mat_<double>(3,1)<< -v1.at<double>(1), v1.at<double>(0), 0);
//     auto v3 = v1.cross(v2);

//     cv::Mat new_rMat;
//     new_rMat.create(3,3,CV_64FC1);
//     new_rMat.row(0) = v1.t()/cv::norm(v1);
//     new_rMat.row(1) = v2.t()/cv::norm(v2);
//     new_rMat.row(2) = v3.t()/cv::norm(v3);

//     rect1_ = new_rMat;
//     rect2_ = cam[idx1_]->R_betw_cams()[idx2_]*new_rMat;

//     homo1_ = cam[idx1_]->cameraMatrix()*Rectify1*cam[idx1_]->cameraMatrix().inv();
//     homo2_ = cam[idx2_]->cameraMatrix()*Rectify2*cam[idx2_]->cameraMatrix().inv();
// }

// void DSLPT::initUndistortRectifyMapUsingRectifiedMatrix(const std::vector<Camera_ptr>& cam,
//                                             cv::Mat& map1_x, cv::Mat& map1_y, cv::Mat& map2_x, cv::Mat& map2_y){

//     cv::initUndistortRectifyMap(cam[idx1_]->cameraMatrix(), cam[idx1_]->distCoeffs(), 
//                                 rect1_,
//                                 cam[idx1_]->cameraMatrix(),                     
//                                 cv::Size(cam[idx1_]->n_width(), cam[idx1_]->n_height()),
//                                 CV_32FC1,
//                                 map1_x, map1_y);

//     cv::initUndistortRectifyMap(cam[idx2_]->cameraMatrix(), cam[idx2_]->distCoeffs(), 
//                                 rect2_,
//                                 cam[idx2_]->cameraMatrix(),                                
//                                 cv::Size(cam[idx2_]->n_width(), cam[idx2_]->n_height()),
//                                 CV_32FC1,
//                                 map2_x, map2_y);
// }