#include "../SYNTHETIC.hpp"

SYNTHETIC::SYNTHETIC(){}

SYNTHETIC::~SYNTHETIC(){}

void SYNTHETIC::setIMGData(const std::string& obs_dir, int it_deb, int it_tot, const std::vector<Camera_ptr>& cam, const std::string& img_dir){
    std::string src_path, dst_path;
    for(int it=it_deb; it<=it_tot; it++){
        for(int i_cam=0; i_cam<cam.size(); i_cam++){
            src_path=str(boost::format("%1%/cam%2%/cam_c%2%_it%3$03d.png") % obs_dir % i_cam % it );
            dst_path=str(boost::format("%1%/cam%2%/cam_c%2%_it%3$03d.png") % img_dir % i_cam % it );
            // boost::filesystem::create_symlink(src_path, dst_path);
            boost::filesystem::copy_file(src_path, dst_path, boost::filesystem::copy_option::overwrite_if_exists);
        }
    }
}

void SYNTHETIC::readMarkPositionTable(const std::string& calib_file, int nViews,
                                    std::vector<std::vector<std::vector<cv::Point3d>>>& Xcoord_obj_cams,
                                    std::vector<std::vector<std::vector<cv::Point2d>>>& xcoord_img,
                                    int nXc_x_rig, int nXc_y_rig){

    auto load_npz = cnpy::npz_load(calib_file);

    auto X_v = load_npz["X"].as_vec<double>();
    auto Y_v = load_npz["Y"].as_vec<double>();
    auto Z_v = load_npz["Z"].as_vec<double>();
    auto x1_v = load_npz["x1"].as_vec<double>();
    auto y1_v = load_npz["y1"].as_vec<double>();
    auto x2_v = load_npz["x2"].as_vec<double>();
    auto y2_v = load_npz["y2"].as_vec<double>();
    auto x3_v = load_npz["x3"].as_vec<double>();
    auto y3_v = load_npz["y3"].as_vec<double>();
    auto x4_v = load_npz["x4"].as_vec<double>();
    auto y4_v = load_npz["y4"].as_vec<double>();

    cout<<"nViews "<<nViews<<endl;
    cout<<"nXc_x_rig "<<nXc_x_rig<<endl;
    cout<<"nXc_y_rig "<<nXc_y_rig<<endl;

    cout<<"X_v.size() "<<X_v.size()<<endl;

    std::vector<std::vector<cv::Point3d>> Xcoord_per_cam(nViews, std::vector<cv::Point3d>());
    std::vector<std::vector<cv::Point2d>> xcoord_per_cam1(nViews, std::vector<cv::Point2d>()),
                                          xcoord_per_cam2(nViews, std::vector<cv::Point2d>()), 
                                          xcoord_per_cam3(nViews, std::vector<cv::Point2d>()),
                                          xcoord_per_cam4(nViews, std::vector<cv::Point2d>());

    for(int z=0; z<nViews; z++){
        for(int i=0; i<nXc_x_rig; i++){
            for(int j=0; j<nXc_y_rig; j++){
                int idx = j+i*nXc_y_rig+z*nXc_x_rig*nXc_y_rig;

                Xcoord_per_cam[z].push_back( cv::Point3d(X_v[idx], Y_v[idx], Z_v[idx]) );
                xcoord_per_cam1[z].push_back( cv::Point2d(x1_v[idx], y1_v[idx]) );
                xcoord_per_cam2[z].push_back( cv::Point2d(x2_v[idx], y2_v[idx]) );
                xcoord_per_cam3[z].push_back( cv::Point2d(x3_v[idx], y3_v[idx]) );
                xcoord_per_cam4[z].push_back( cv::Point2d(x4_v[idx], y4_v[idx]) );
            }
        }
    }

    cout<<"Xcoord_per_cam.size() "<<Xcoord_per_cam.size()<<endl;
    cout<<"xcoord_per_cam1.size() "<<xcoord_per_cam1.size()<<endl;
    cout<<"xcoord_per_cam1[0].size() "<<xcoord_per_cam1[0].size()<<endl;
    cout<<"xcoord_per_cam1.back().size() "<<xcoord_per_cam1.back().size()<<endl;

    Xcoord_obj_cams.assign(4, Xcoord_per_cam);
    xcoord_img.push_back(xcoord_per_cam1);
    xcoord_img.push_back(xcoord_per_cam2);
    xcoord_img.push_back(xcoord_per_cam3);
    xcoord_img.push_back(xcoord_per_cam4);
}