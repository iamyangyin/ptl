#include "../Camera.hpp"

Camera::psf_Type    Camera::psf_type_;
std::vector<double> Camera::xgrid_psf_, Camera::ygrid_psf_, Camera::zgrid_psf_;
cv::Point3d         Camera::dXc_psf_=cv::Point3d(0,0,0);
int                 Camera::max_intensity_value_=0;
int                 Camera::opencv_mat_type_=0;

Camera::Camera(){}

Camera::~Camera(){}

Camera::Camera(Model_Type model_type, int n_cam, int i_cam): 
                model_type_(model_type), n_cam_(n_cam), i_cam_(i_cam){}

Camera::Camera(Model_Type model_type, int n_cam, int i_cam, int n_width_img, int n_height_img, int z_order, double f, double k, double beta, double alpha, double Z0, const cv::Mat& distCoeffs, double noise_ratio):
                model_type_(model_type), n_cam_(n_cam), i_cam_(i_cam), n_width_(n_width_img), n_height_(n_height_img), z_order_(z_order), f_(f), k_(k), l_(k), beta_(beta), alpha_(alpha), Z0_(Z0), noise_ratio_(noise_ratio){      
                    distCoeffs_=distCoeffs.clone();
                }

Camera::Camera(Model_Type model_type, int n_cam, int i_cam, int n_width_img, int n_height_img, int z_order, double f, double k):
                model_type_(model_type), n_cam_(n_cam), i_cam_(i_cam), n_width_(n_width_img), n_height_(n_height_img), z_order_(z_order), f_(f), k_(k), l_(k){}

void Camera::setPSFGrids(const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){

    VectorXd xgrid_psf, ygrid_psf, zgrid_psf;

    xgrid_psf.setLinSpaced(nXcPSF_.x+1, Xc_bottom.x, Xc_top.x);
    ygrid_psf.setLinSpaced(nXcPSF_.y+1, Xc_bottom.y, Xc_top.y);
    zgrid_psf.setLinSpaced(nXcPSF_.z+1, Xc_bottom.z, Xc_top.z);

    cout<<"xgrid_psf "<<xgrid_psf.transpose().format(utils::CleanFmt)<<endl
        <<"ygrid_psf "<<ygrid_psf.transpose().format(utils::CleanFmt)<<endl
        <<"zgrid_psf "<<zgrid_psf.transpose().format(utils::CleanFmt)<<endl;

    dXc_psf_.x=(Xc_top.x-Xc_bottom.x)/(nXcPSF_.x);
    dXc_psf_.y=(Xc_top.y-Xc_bottom.y)/(nXcPSF_.y);
    dXc_psf_.z=(Xc_top.z-Xc_bottom.z)/(nXcPSF_.z);

    xgrid_psf_.resize(xgrid_psf.size());
    VectorXd::Map(&xgrid_psf_[0], xgrid_psf.size())=xgrid_psf;
    ygrid_psf_.resize(ygrid_psf.size());
    VectorXd::Map(&ygrid_psf_[0], ygrid_psf.size())=ygrid_psf;
    zgrid_psf_.resize(zgrid_psf.size());
    VectorXd::Map(&zgrid_psf_[0], zgrid_psf.size())=zgrid_psf;
}

void Camera::setPSFelement(const Eigen::Vector4f& psf_elem, int psf_idx){
    theta_[psf_idx] = psf_elem;
}

Eigen::Vector4f Camera::getPSFelement(const cv::Point3d& Xc){

    auto pxl=find_if(std::begin(xgrid_psf_), std::end(xgrid_psf_), [=](const double& x) { return Xc.x-x<dXc_psf_.x; } );
    auto pyl=find_if(std::begin(ygrid_psf_), std::end(ygrid_psf_), [=](const double& y) { return Xc.y-y<dXc_psf_.y; } );
    auto pzl=find_if(std::begin(zgrid_psf_), std::end(zgrid_psf_), [=](const double& z) { return Xc.z-z<dXc_psf_.z; } );

    if(pxl==std::end(xgrid_psf_)-1) pxl--;
    if(pyl==std::end(ygrid_psf_)-1) pyl--;
    if(pzl==std::end(zgrid_psf_)-1) pzl--;

    auto xl_idx=std::distance(std::begin(xgrid_psf_), pxl);
    auto yl_idx=std::distance(std::begin(ygrid_psf_), pyl);
    auto zl_idx=std::distance(std::begin(zgrid_psf_), pzl);

    int psf_idx=xl_idx*(ygrid_psf_.size()-1)*(zgrid_psf_.size()-1) + yl_idx*(zgrid_psf_.size()-1) + zl_idx;
    return this->theta_[psf_idx]; 
}

cv::Point2d Camera::mappingFunction(const cv::Point3d& Xc, bool verbal, bool force_pinhold_flag) const{
    
    cv::Point2d xc;

    if( (model_type_==Camera::Model_Type::Polynomial || model_type_==Camera::Model_Type::PinholeAndPolynomial) && !force_pinhold_flag ){    

        VectorXd design_mat(n_coef_);
        if(polynomial_type_==Camera::Polynomial_Type::Single){

            if(verbal){
                cout<<"###########################################"<<endl;
                cout<<"Xc "<<Xc<<endl;
            }

            auto kX=PixelPerMmFactor_*Xc.x;
            auto kY=PixelPerMmFactor_*Xc.y;
            auto kZ=PixelPerMmFactor_*Xc.z;

            if(verbal) cout<<"kX "<<kX<<" kY "<<kY<<" kZ "<<kZ<<endl;

            auto X=kX/normalizationFactor_.x;
            auto Y=kY/normalizationFactor_.y;
            auto Z=kZ/normalizationFactor_.z;

            if(verbal) cout<<"X "<<X<<" Y "<<Y<<" Z "<<Z<<endl;

            if(z_order_==3)
                design_mat<<1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                            X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y, 
                            X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                            std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2), std::pow(Z,3);
            else if(z_order_==2)
                design_mat<<1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                            X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y, 
                            X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                            std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2);

            xc.x=kX-a_.dot(design_mat);
            xc.y=kY-b_.dot(design_mat);

            if(verbal) cout<<"xc "<<xc<<endl;
        }
        else if(polynomial_type_==Camera::Polynomial_Type::SingleLegacy){

            if(verbal){
                cout<<"###########################################"<<endl;
                cout<<"Xc "<<Xc<<endl;
            }

            double X=Xc.x, Y=Xc.y, Z=Xc.z;

            if(z_order_==3)
                design_mat<<1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                            X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y, 
                            X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                            std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2), std::pow(Z,3);
            else if(z_order_==2)
                design_mat<<1, X, Y, Z, std::pow(X,2), X*Y, std::pow(Y,2), 
                            X*Z, Y*Z, std::pow(Z,2), std::pow(X,3), std::pow(X,2)*Y, 
                            X*std::pow(Y,2), std::pow(Y,3), std::pow(X,2)*Z, X*Y*Z, 
                            std::pow(Y,2)*Z, X*std::pow(Z,2), Y*std::pow(Z,2);

            xc.x=a_.dot(design_mat);
            xc.y=b_.dot(design_mat);

            if(verbal) cout<<"xc "<<xc<<endl;
        }
        else if(polynomial_type_==Camera::Polynomial_Type::ZPlane){
        
            if(verbal){
                cout<<"###########################################"<<endl;
                cout<<"Xc "<<Xc<<endl;
            }

            auto kX=PixelPerMmFactor_*Xc.x;
            auto kY=PixelPerMmFactor_*Xc.y;
            auto kZ=PixelPerMmFactor_*Xc.z;

            if(verbal) cout<<str(boost::format("kX %1%, kY %2%, kZ %3%") % kX % kY % kZ )<<endl;

            auto Xv=kX+originZ_[0].x;
            auto Yv=-kY+originZ_[0].y;
            auto Zv=kZ;

            if(verbal) cout<<str(boost::format("so %1%, to %2%") % originZ_[0].x % originZ_[0].y )<<endl;           
            if(verbal) cout<<str(boost::format("Xv %1%, Yv %2%, Zv %3%") % Xv % Yv % Zv )<<endl;

            std::vector<double> dx_Z, dy_Z;

            auto s=2.*kX/normalizationFactorZ_[0].x;
            auto t=2.*(-kY)/normalizationFactorZ_[0].y;

            if(verbal) cout<<str(boost::format("s %1%, t %2%") % s % t )<<endl;

            for(int i=0; i<nZ_; i++){
                design_mat<<1, s, std::pow(s,2), std::pow(s,3),
                            t, std::pow(t,2), std::pow(t,3),
                            s*t, std::pow(s,2)*t, std::pow(t,2)*s; 

                dx_Z.push_back(aZ_[i].dot(design_mat));
                dy_Z.push_back(bZ_[i].dot(design_mat));
            }

            double dx, dy;

            if(nZ_==2){
                auto Z_ratio=(Zv-ZPosZ_[0])/(ZPosZ_[1]-ZPosZ_[0]);

                dx=dx_Z[0]+(dx_Z[1]-dx_Z[0])*Z_ratio;
                dy=dy_Z[0]+(dy_Z[1]-dy_Z[0])*Z_ratio;
            }
            else if(nZ_==3){
                auto Z_ratio_0=(Zv-ZPosZ_[1])*(Zv-ZPosZ_[2])/((ZPosZ_[0]-ZPosZ_[1])*(ZPosZ_[0]-ZPosZ_[2]));
                auto Z_ratio_1=(Zv-ZPosZ_[0])*(Zv-ZPosZ_[2])/((ZPosZ_[1]-ZPosZ_[0])*(ZPosZ_[1]-ZPosZ_[2]));
                auto Z_ratio_2=(Zv-ZPosZ_[0])*(Zv-ZPosZ_[1])/((ZPosZ_[2]-ZPosZ_[0])*(ZPosZ_[2]-ZPosZ_[1]));

                dx=dx_Z[0]*Z_ratio_0+dx_Z[1]*Z_ratio_1+dx_Z[2]*Z_ratio_2;
                dy=dy_Z[0]*Z_ratio_0+dy_Z[1]*Z_ratio_1+dy_Z[2]*Z_ratio_2;
            }
            else{
                std::abort();
            }

            if(verbal) cout<<str(boost::format("dx %1%, dy %2%") % dx % dy )<<endl;

            xc.x=Xv-dx;
            xc.y=Yv-dy;

            if(verbal) cout<<"xc "<<xc<<endl;
        }
    }

    if(force_pinhold_flag || model_type_==Camera::Model_Type::Pinhole){
        auto Xc_m = cv::Point3d(Xc.x, Xc.y, 0.) + Xc.z*deltaX_;

        if(ZViews_.empty()){
            std::vector<cv::Point2d> p2_out;
            cv::projectPoints(std::vector<cv::Point3d>{Xc_m}, rvecs_[0], tvecs_[0], cameraMatrix_, distCoeffs_, p2_out);
            xc=p2_out[0];
        }
        else{
            int idx_l=0;
            if(Xc.z<ZViews_[0] || Xc.z>=ZViews_.back()){
                if(Xc.z<ZViews_[0])             idx_l = 0;
                else if(Xc.z>=ZViews_.back())   idx_l = ZViews_.size()-2;
            }
            else{
                std::vector<std::pair<double, double>> ZViews_pairs;
                for(int i=0; i<ZViews_.size()-1; i++)
                    ZViews_pairs.push_back(std::pair<double, double>(ZViews_[i], ZViews_[i+1]));

                auto it = std::find_if(ZViews_pairs.begin(), ZViews_pairs.end(), [=](const auto& Z_pair){ return Xc.z>=Z_pair.first && Xc.z<Z_pair.second; });
                idx_l = std::distance(std::begin(ZViews_pairs), it);
            }
            int idx_r = idx_l+1;

            std::vector<cv::Point2d> p2_out;
            Xc_m.z = 0;
            cv::projectPoints(std::vector<cv::Point3d>{Xc_m}, rvecs_[idx_l], tvecs_[idx_l], cameraMatrix_, distCoeffs_, p2_out);
            auto xc_l=p2_out[0];

            p2_out.clear();
            cv::projectPoints(std::vector<cv::Point3d>{Xc_m}, rvecs_[idx_r], tvecs_[idx_r], cameraMatrix_, distCoeffs_, p2_out);
            auto xc_r=p2_out[0];

            auto Z_ratio=(Xc.z - ZViews_[idx_l])/(ZViews_[idx_r]-ZViews_[idx_l]);
            xc = cv::Point2d( xc_l.x+(xc_r.x-xc_l.x)*Z_ratio,
                              xc_l.y+(xc_r.y-xc_l.y)*Z_ratio );
        }
    }

    return xc;
}

void Camera::printMappingFunctionInfo(void){

    if(model_type_==Camera::Model_Type::Polynomial || model_type_==Camera::Model_Type::PinholeAndPolynomial){   

        cout<<"polynomial_type_ "<<static_cast<int>(polynomial_type_)<<endl;
        if(polynomial_type_==Camera::Polynomial_Type::Single){
            cout<<"a_"<<endl<<a_<<endl
                <<"b_"<<endl<<b_<<endl;
        }
        else if(polynomial_type_==Camera::Polynomial_Type::ZPlane){
            cout<<"PixelPerMmFactor_ "<<PixelPerMmFactor_<<endl
                <<"nZ_ "<<nZ_<<endl;
            for(int nz=0; nz<nZ_; nz++){
                cout<<"nz "<<nz<<endl;
                cout<<"ZPosZ_ "<<ZPosZ_[nz]<<endl;
                cout<<"originZ_ "<<originZ_[nz]<<endl;
                cout<<"normalizationFactorZ_ "<<normalizationFactorZ_[nz]<<endl;
                cout<<"aZ_"<<endl<<aZ_[nz]<<endl;
                cout<<"bZ_"<<endl<<bZ_[nz]<<endl;
            }
        }

    }
}

void Camera::checkSensitivityOfMappingFunction(const cv::Point3d& Xc, double character_length){
    cv::Point2d xc=this->mappingFunction(Xc);

    cv::Point2d xcx, xcy, xcz;
    VectorXd dx,dy,dz;
    double dXc_px=0, dYc_px=0, dZc_px=0;

    int    nx=1001;
    double cut_ratio=0.05;
    double xy_ceil=0.1;
    double z_ceil=0.2;

    dx.setLinSpaced(nx, 0, cut_ratio*character_length);
    dy.setLinSpaced(nx, 0, cut_ratio*character_length);
    dz.setLinSpaced(nx, 0, cut_ratio*character_length);

    for(int i=0; i<nx; i++){
        xcx=this->mappingFunction(cv::Point3d(Xc.x+dx(i), Xc.y, Xc.z));
        if(std::abs(std::abs(xcx.x-xc.x)-1.0)<xy_ceil){
            dXc_px=dx(i);
            break;
        }
    }

    for(int i=0; i<nx; i++){
        xcy=this->mappingFunction(cv::Point3d(Xc.x, Xc.y+dy(i), Xc.z));
        if(std::abs(std::abs(xcy.y-xc.y)-1.0)<xy_ceil){
            dYc_px=dy(i);
            break;
        }
    }

    for(int i=0; i<nx; i++){
        xcz=this->mappingFunction(cv::Point3d(Xc.x, Xc.y, Xc.z+dz(i)));
        if(std::abs(cv::norm(xcz-xc)-1.0) < z_ceil){
            dZc_px=dz(i);
            break;
        }
    }

    if(dXc_px==0) std::cerr<<"error in dX_px checkSensitivityOfMappingFunction"<<endl;
    if(dYc_px==0) std::cerr<<"error in dY_px checkSensitivityOfMappingFunction"<<endl;
    if(dZc_px==0) std::cerr<<"error in dZ_px checkSensitivityOfMappingFunction"<<endl;

    dXc_px_.x=dXc_px;
    dXc_px_.y=dYc_px;
    dXc_px_.z=dZc_px;
}

cv::Mat Camera::sumParticleImage(const std::vector<Particle_ptr>& particles_tgt, sumParticle_Type sumParticle_type, int it_seq_cnt){
    
    cv::Mat Iproj=cv::Mat::zeros(n_height_, n_width_, opencv_mat_type_);

    int it_img=0;
    bool is_considered=true;

    for(auto& po : particles_tgt){

        switch(sumParticle_type){
            case Camera::sumParticle_Type::All:
                it_img=it_seq_cnt-po->it_seq_deb();
                is_considered=true;
                break;
            case Camera::sumParticle_Type::Detected:
                it_img=0;
                is_considered = (po->is_to_be_tracked() || po->is_tracked() || po->is_triangulated());
                break;
        }

        cv::Rect pos_BigCanvas = cv::Rect(0,0,0,0);
        if(po->is_ontrack() && is_considered)
        {
            if(po->on_image().at(i_cam_)==1)
            {
                auto pos=po->imgPos().at(i_cam_).at(it_img);

                cv::Rect pos_img;

                utils::getPositionAndSizeOfOverlapedAreaOfOneLocalPatchAndIres(pos, static_cast<int>(pos.width)/2, n_width_, n_height_, pos_img, pos_BigCanvas);

                po->setimgPos( pos_img, i_cam_, it_img );

                if(pos_img.x>n_width_-1 || pos_img.x<-pos_img.width/2 || pos_img.y>n_height_-1 || pos_img.y<-pos_img.height/2 || pos_img.height<0 || pos_img.width<0){
                    std::cerr<<"Camera::sumParticleImage: Iproj(cv::Rect) bad position"<<endl
                            <<"For camera               "<<i_cam_<<endl
                            <<"po->part_index()         "<<po->part_index()<<endl
                            <<"po->Xcoord().size()      "<<po->Xcoord().size()<<endl
                            <<"po->Xcoord().back()      "<<po->Xcoord().back()<<endl
                            <<"po->on_image().at(i_cam) "<<po->on_image().at(i_cam_)<<endl;
                }

                Iproj(pos_BigCanvas) = Iproj(pos_BigCanvas) + po->img().at(i_cam_).at(it_img)(pos_img);
            }

            po->push_backimgResparPosElement( std::vector<cv::Rect>(1, pos_BigCanvas) );
        }
    }

    double sigma_noise = (sumParticle_type==Camera::sumParticle_Type::All ? max_intensity_value_*noise_ratio_ : 0.);

    if(sigma_noise!=0){
        cv::theRNG().state = time(0);
        cv::Mat Iproj_float;
        Iproj.convertTo(Iproj_float, CV_32FC1);
        cv::Mat Inoise=cv::Mat::zeros(n_height_, n_width_, CV_32FC1);
        cv::randn(Inoise, 0.0, sigma_noise);
        cv::add(Iproj_float, Inoise, Iproj_float);
        Iproj_float.convertTo(Iproj, opencv_mat_type_);
    }
    
    return Iproj;
}

void Camera::computeImageResidual(int it_seq_cnt, bool ipr_flag){
    
    cv::Mat Ires, Irec, Iproj;
    Ires.create(n_height_, n_width_, CV_32FC1);

    Irec_.convertTo(Irec, CV_32FC1);
    Iproj_.convertTo(Iproj, CV_32FC1);

    cv::subtract(Irec, Iproj, Ires);

    Ires.copyTo(Ires_);
}

double Camera::computeGlobalResidual(int it_seq_cnt){

    cout<<"cam "<<i_cam_<<" cv::norm( Ires_ ) "<<cv::norm( Ires_ )<<endl;
    return std::pow(cv::norm( Ires_ ), 2);
}

void Camera::saveImageResidual(int it_seq_cnt, const std::string& Ires_name, const std::string& res_path){

    cv::Mat Ires=cv::abs(Ires_);
    Ires.convertTo(Ires, opencv_mat_type_);

    std::string fullname=str(boost::format("%1%/cam%2$d/%3%_c%4%_it%5$03d.png") % res_path % i_cam_ % Ires_name % i_cam_ % it_seq_cnt );
    cv::imwrite(fullname, Ires, utils::compression_params);
}

void Camera::saveImageProjection(int it_seq_cnt, const std::string& Iproj_name, const std::string& res_path, bool write_bkg_flag){

    std::string fullname=str(boost::format("%1%/cam%2$d/%3%_c%4%_it%5$03d.png") % res_path % i_cam_ % Iproj_name % i_cam_ % it_seq_cnt );   
    cv::imwrite(fullname, Iproj_, utils::compression_params);
}

void Camera::savePolynomialParamsToFile(cv::FileStorage& fs){
    fs<<"polynomial_type"<<static_cast<int>(polynomial_type_);
    fs<<"z_order"<<z_order_;
    fs<<"n_coef"<<n_coef_;

    if(polynomial_type_==Camera::Polynomial_Type::Single || polynomial_type_==Camera::Polynomial_Type::SingleLegacy){
        if(polynomial_type_==Camera::Polynomial_Type::Single){
            fs<<"PixelPerMmFactor"<<PixelPerMmFactor_;
            fs<<"normalizationFactor"<<normalizationFactor_;
        }

        fs<<"a"<<"[";
        for(int i=0; i<a_.size(); i++){
            fs<<a_(i);
        }
        fs<<"]";
        fs<<"b"<<"[";
        for(int i=0; i<b_.size(); i++){
            fs<<b_(i);
        }
        fs<<"]";
    }
    else if(polynomial_type_==Camera::Polynomial_Type::ZPlane){
        fs<<"PixelPerMmFactor"<<PixelPerMmFactor_;
        fs<<"nZ"<<nZ_;
        for(int i=0; i<nZ_; i++){
            fs<<str(boost::format("ZPosZ%1$d") % i )<<ZPosZ_[i];
            fs<<str(boost::format("originZ%1$d") % i )<<originZ_[i];
            fs<<str(boost::format("normalizationFactorZ%1$d") % i )<<normalizationFactorZ_[i];

            auto label_a="a"+std::to_string(i);
            fs<<label_a<<"[";
            for(int j=0; j<aZ_[i].size(); j++){
                fs<<aZ_[i](j);
            }
            fs<<"]";
            auto label_b="b"+std::to_string(i);
            fs<<label_b<<"[";
            for(int j=0; j<bZ_[i].size(); j++){
                fs<<bZ_[i](j);
            }
            fs<<"]";
        }
    }
    else{
        std::abort();
    }
}

void Camera::savePinholeParamsToFile(cv::FileStorage& fs){
    fs<<"cameraMatrix"<<cameraMatrix_;
    fs<<"distCoeffs"<<distCoeffs_;
    fs<<"R"<<R_;
    fs<<"T"<<T_;

    fs<<"ZViews"<<"[";
    for(int z=0; z<ZViews_.size(); z++){
        fs<<ZViews_[z];
    }
    fs<<"]";

    fs<<"rvecs"<<"[";
    for(auto& rvec: rvecs_){
        fs<<rvec;
    }
    fs<<"]";
    fs<<"tvecs"<<"[";
    for(auto& tvec: tvecs_){
        fs<<tvec;
    }
    fs<<"]";
}

void Camera::saveStereoParamsToFile(cv::FileStorage& fs){

    fs<<"R_betw_cams"<<"[";
    for(int i=0; i<R_betw_cams_.size(); i++){
        fs<<R_betw_cams_.at(i);
    }
    fs<<"]";
    fs<<"T_betw_cams"<<"[";
    for(int i=0; i<T_betw_cams_.size(); i++){
        fs<<T_betw_cams_.at(i);
    }
    fs<<"]";
    fs<<"E_betw_cams"<<"[";
    for(int i=0; i<E_betw_cams_.size(); i++){
        fs<<E_betw_cams_.at(i);
    }
    fs<<"]";
    fs<<"F_betw_cams"<<"[";
    for(int i=0; i<F_betw_cams_.size(); i++){
        fs<<F_betw_cams_.at(i);
    }
    fs<<"]";
}

void Camera::savePSFParamsToFile(cv::FileStorage& fs){

    fs<<"nXc_PSF"<<nXcPSF_;

    fs<<"theta"<<"[";
    for(int i=0; i<nXcPSF_.x; i++){
        for(int j=0; j<nXcPSF_.y; j++){
            for(int k=0; k<nXcPSF_.z; k++){
                cv::Mat theta_cvmat;
                cv::eigen2cv(theta_[i*nXcPSF_.z*nXcPSF_.y+j*nXcPSF_.z+k], theta_cvmat);
                fs<<theta_cvmat;
            }
        }
    }
    fs<<"]";
}

void Camera::saveParamsToYamlFile(const std::string& imgdir, bool preproc_flag){
    std::string preproc_str=(preproc_flag ? "_preproc" : "");
    cv::FileStorage fs(imgdir+"/camera"+std::to_string(i_cam_+1)+preproc_str+".yaml", cv::FileStorage::WRITE);

    fs<<"n_cam"<<n_cam_;
    fs<<"n_width"<<n_width_;
    fs<<"n_height"<<n_height_;
    fs<<"n_width_mask"<<n_width_mask_;
    fs<<"n_height_mask"<<n_height_mask_;
    fs<<"i_cam"<<i_cam_;
    fs<<"dXc_px"<<dXc_px_;

    if(model_type_==Camera::Model_Type::PinholeAndPolynomial) {this->savePolynomialParamsToFile(fs);}
    this->savePinholeParamsToFile(fs);
    this->savePSFParamsToFile(fs);
    this->saveStereoParamsToFile(fs);

    fs.release();
}

void Camera::readParamsPolynomial(cv::FileStorage& fs){
    polynomial_type_=static_cast<Camera::Polynomial_Type>(static_cast<int>(fs["polynomial_type"]));
    z_order_=fs["z_order"];
    n_coef_=fs["n_coef"];

    double elem;
    if(polynomial_type_==Camera::Polynomial_Type::Single || polynomial_type_==Camera::Polynomial_Type::SingleLegacy){
        if(polynomial_type_==Camera::Polynomial_Type::Single){
            PixelPerMmFactor_=fs["PixelPerMmFactor"];
            fs["normalizationFactor"] >> normalizationFactor_;
        }
        a_.resize(n_coef_);
        b_.resize(n_coef_);

        cv::FileNode fn_a=fs["a"];
        if(fn_a.type()==cv::FileNode::SEQ){
            int i=0;
            for(cv::FileNodeIterator it=fn_a.begin(); it!=fn_a.end();){
                it >> elem;
                a_(i)=elem;
                i++;
            }
        }

        cv::FileNode fn_b=fs["b"];
        if(fn_b.type()==cv::FileNode::SEQ){
            int i=0;
            for(cv::FileNodeIterator it=fn_b.begin(); it!=fn_b.end();){
                it >> elem;
                b_(i)=elem;
                i++;
            }
        }
    }
    else if(polynomial_type_==Camera::Polynomial_Type::ZPlane){
        PixelPerMmFactor_=fs["PixelPerMmFactor"];
        nZ_=fs["nZ"];

        std::string keyname_a,keyname_b;
        VectorXd a,b;
        a.resize(n_coef_);
        b.resize(n_coef_);

        cv::Point2d originZi, normalizationFactorZi;
        double elem;

        for(int i=0; i<nZ_; i++){
            ZPosZ_.push_back(fs["ZPosZ"+std::to_string(i)]);

            fs[str(boost::format("originZ%1$d") % i )] >> originZi; 
            originZ_.push_back(originZi);
            fs[str(boost::format("normalizationFactorZ%1$d") % i )] >> normalizationFactorZi;
            normalizationFactorZ_.push_back(normalizationFactorZi);

            auto a_label="a"+std::to_string(i);
            cv::FileNode fn_a=fs[a_label];
            if(fn_a.type()==cv::FileNode::SEQ){
                int i=0;
                for(cv::FileNodeIterator it=fn_a.begin(); it!=fn_a.end();){
                    it >> elem;
                    a(i)=elem;
                    i++;
                }
                aZ_.push_back(a);
            }

            auto b_label="b"+std::to_string(i);
            cv::FileNode fn_b=fs[b_label];
            if(fn_b.type()==cv::FileNode::SEQ){
                int i=0;
                for(cv::FileNodeIterator it=fn_b.begin(); it!=fn_b.end();){
                    it >> elem;
                    b(i)=elem;
                    i++;
                }
                bZ_.push_back(b);
            }
        }
    }
    else
        std::abort();
}

void Camera::readParamsPinhole(cv::FileStorage& fs){

    fs["cameraMatrix"]>>this->cameraMatrix_;
    fs["distCoeffs"]>>this->distCoeffs_;

    cv::FileNode fn_Z=fs["ZViews"];
    if(fn_Z.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_Z.begin(); it!=fn_Z.end();){
            double elem;
            it >> elem;
            ZViews_.push_back(elem);
        }
    }

    fs["R"]>>this->R_;
    fs["T"]>>this->T_;
    
    cv::FileNode fn_rvec=fs["rvecs"];
    if(fn_rvec.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_rvec.begin(); it!=fn_rvec.end();){
            cv::Mat rvec;
            it >> rvec;
            rvecs_.push_back(rvec);
        }
    }

    cv::FileNode fn_tvec=fs["tvecs"];
    if(fn_tvec.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_tvec.begin(); it!=fn_tvec.end();){
            cv::Mat tvec;
            it >> tvec;
            tvecs_.push_back(tvec);
        }
    }
}

void Camera::readParamsStereo(cv::FileStorage& fs){

    cv::FileNode fn_R=fs["R_betw_cams"];
    if(fn_R.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_R.begin(); it!=fn_R.end();){
            cv::Mat R;
            it >> R;
            R_betw_cams_.push_back(R);
        }
    }
    cv::FileNode fn_T=fs["T_betw_cams"];
    if(fn_T.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_T.begin(); it!=fn_T.end();){
            cv::Mat T;
            it >> T;
            T_betw_cams_.push_back(T);
        }
    }
    cv::FileNode fn_E=fs["E_betw_cams"];
    if(fn_E.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_E.begin(); it!=fn_E.end();){
            cv::Mat R;
            it >> R;
            E_betw_cams_.push_back(R);
        }
    }
    cv::FileNode fn_F=fs["F_betw_cams"];
    if(fn_F.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn_F.begin(); it!=fn_F.end();){
            cv::Mat F;
            it >> F;
            F_betw_cams_.push_back(F);
        }
    }
}

void Camera::readParamsPSF(cv::FileStorage& fs, bool pert_otf_flag){

    fs["nXc_PSF"]>>nXcPSF_;

    if(pert_otf_flag) cout<<"perturb OTF!!! Is this what you want?"<<endl;

    cv::FileNode fn=fs["theta"];
    if(fn.type()==cv::FileNode::SEQ){
        for(cv::FileNodeIterator it=fn.begin(); it!=fn.end();){
            cv::Mat theta_ijk_cvmat;
            Eigen::Vector4f theta_ijk;
            it >> theta_ijk_cvmat;
            cv::cv2eigen(theta_ijk_cvmat, theta_ijk);

            if(pert_otf_flag){
                //lavision_bkgonly
                // theta_ijk(0)=0.8*theta_ijk(0);
                // theta_ijk(2)=0.85*theta_ijk(2);
                // theta_ijk(3)=1.25*theta_ijk(3);
                //lavision_bkgonly

                //lptchal4500
                // theta_ijk(0)=1.1*theta_ijk(0);
                // theta_ijk(2)=1.1*theta_ijk(2);
                // theta_ijk(3)=1.0*theta_ijk(3);
                //lptchal4500

                // lavision_jet
                theta_ijk(0)=1.1*theta_ijk(0);
                theta_ijk(2)=1.1*theta_ijk(2);
                theta_ijk(3)=1.*theta_ijk(3);
                //lavision_jet
            }

            if(psf_type_==Camera::psf_Type::ErrorFunction) assert(theta_ijk(1)==0);
            theta_.push_back(theta_ijk);
        }
    }
}

void Camera::readParamsYaml(const std::string& camdir, bool preproc_flag, bool pert_otf_flag){

    std::string preproc_str=(preproc_flag ? "_preproc" : "");
    std::string camfilename(camdir+"/camera"+std::to_string(i_cam_+1)+preproc_str+".yaml");

    cv::FileStorage fs(camfilename, cv::FileStorage::READ);

    if(fs.isOpened()){
        fs["n_cam"]>>n_cam_;
        fs["n_width"]>>n_width_;
        fs["n_height"]>>n_height_;
        fs["n_width_mask"]>>n_width_mask_;
        fs["n_height_mask"]>>n_height_mask_;
        fs["dXc_px"]>>dXc_px_;

        if(model_type_==Camera::Model_Type::PinholeAndPolynomial) { this->readParamsPolynomial(fs); }
        this->readParamsPinhole(fs);
        this->readParamsPSF(fs, pert_otf_flag);
        this->readParamsStereo(fs);
    }
    else{
        std::cerr<<"camera file does not exist!!!"<<endl;
        std::abort();
    }

    fs.release();
}

void Camera::readRecord(int it_seq, const std::string& imgdir, bool preproc_flag){

    if(Irec_.empty()) Irec_.release();

    std::string preproc_str=(preproc_flag ? "_preproc" : "");

    std::string preproc_dir=str(boost::format("%1%/cam%2%%3%") % imgdir % i_cam_ % preproc_str);
    
    if(!boost::filesystem::exists(preproc_dir)){
        std::cerr<<"Do preprocessing needed!!!"<<endl;
        std::abort();
    }

    std::string imgRecFile=str(boost::format("%1%/cam_c%2%_it%3$03d.png") % preproc_dir % i_cam_ % it_seq );
    
    cv::Mat Irec=cv::imread(imgRecFile, cv::IMREAD_ANYDEPTH);   

    Irec.copyTo(Irec_);
}

void Camera::preprocessRecords(const std::vector<int>& it_bkg, int it_tot_case, const std::string& imgdir, bool high_pass_flag, bool undistort_original_image_flag,
                                int subtract_sliding_minimum_size, int subtract_constant_pixel_value, int multiply_pixel_factor,
                                bool Gaussian_smothing_flag, bool Sharpending_flag,
                                bool mask_flag, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom){

    auto preproc_dir=str(boost::format("%1%/cam%2%_preproc") % imgdir % i_cam_);
    if(!boost::filesystem::exists(preproc_dir))
        boost::filesystem::create_directories(preproc_dir);

    std::vector<cv::Point2i> points;
    if(mask_flag){
        auto x0c0 = this->mappingFunction( cv::Point3d(Xc_bottom.x, Xc_bottom.y, Xc_bottom.z) );
        auto x0c1 = this->mappingFunction( cv::Point3d(Xc_top.x,    Xc_bottom.y, Xc_bottom.z) );
        auto xZc0 = this->mappingFunction( cv::Point3d(Xc_bottom.x, Xc_bottom.y, Xc_top.z) );
        auto xZc1 = this->mappingFunction( cv::Point3d(Xc_top.x,    Xc_bottom.y, Xc_top.z) );
       
        auto x0c2 = this->mappingFunction( cv::Point3d(Xc_bottom.x, Xc_top.y,    Xc_bottom.z) );
        auto x0c3 = this->mappingFunction( cv::Point3d(Xc_top.x,    Xc_top.y,    Xc_bottom.z) );
        auto xZc2 = this->mappingFunction( cv::Point3d(Xc_bottom.x, Xc_top.y,    Xc_top.z) );
        auto xZc3 = this->mappingFunction( cv::Point3d(Xc_top.x,    Xc_top.y,    Xc_top.z) );

        cout<<"x0c0 "<<x0c0<<endl
            <<"x0c1 "<<x0c1<<endl
            <<"xZc0 "<<xZc0<<endl
            <<"xZc1 "<<xZc1<<endl
            <<"x0c2 "<<x0c2<<endl
            <<"x0c3 "<<x0c3<<endl
            <<"xZc2 "<<xZc2<<endl
            <<"xZc3 "<<xZc3<<endl;

        cv::Point2i xc_bottmleft, xc_bottmright, xc_topleft, xc_topright;

        //negative takes large domain, positive takes smaller domain
        int x_band=0, y_band=0;

        if(i_cam_==0 || i_cam_==3){
            xc_bottmleft.x  = (x0c0.x < xZc0.x ? static_cast<int>(std::floor(x0c0.x))+x_band : static_cast<int>(std::floor(xZc0.x))+x_band);
            xc_bottmleft.y  = (x0c0.y > xZc0.y ? static_cast<int>(std::ceil(x0c0.y))-y_band  : static_cast<int>(std::ceil(xZc0.y))-y_band);
            xc_bottmright.x = (x0c1.x > xZc1.x ? static_cast<int>(std::ceil(x0c1.x))-x_band  : static_cast<int>(std::ceil(xZc1.x))-x_band);
            xc_bottmright.y = (x0c1.y > xZc1.y ? static_cast<int>(std::ceil(x0c1.y))-y_band  : static_cast<int>(std::ceil(xZc1.y))-y_band);
            xc_topleft.x    = (x0c2.x < xZc2.x ? static_cast<int>(std::floor(x0c2.x))+x_band : static_cast<int>(std::floor(xZc2.x))+x_band);
            xc_topleft.y    = (x0c2.y < xZc2.y ? static_cast<int>(std::floor(x0c2.y))+y_band : static_cast<int>(std::floor(xZc2.y))+y_band);
            xc_topright.x   = (x0c3.x > xZc3.x ? static_cast<int>(std::ceil(x0c3.x))-x_band  : static_cast<int>(std::ceil(xZc3.x))-x_band);
            xc_topright.y   = (x0c3.y < xZc3.y ? static_cast<int>(std::floor(x0c3.y))+y_band : static_cast<int>(std::floor(xZc3.y))+y_band);
        }
        else{
            xc_bottmleft.x  = (x0c1.x < xZc1.x ? static_cast<int>(std::floor(x0c1.x))+x_band : static_cast<int>(std::floor(xZc1.x))+x_band);
            xc_bottmleft.y  = (x0c1.y > xZc1.y ? static_cast<int>(std::ceil(x0c1.y))-y_band  : static_cast<int>(std::ceil(xZc1.y))-y_band);
            xc_bottmright.x = (x0c0.x > xZc0.x ? static_cast<int>(std::ceil(x0c0.x))-x_band  : static_cast<int>(std::ceil(xZc0.x))-x_band);
            xc_bottmright.y = (x0c0.y > xZc0.y ? static_cast<int>(std::ceil(x0c0.y))-y_band  : static_cast<int>(std::ceil(xZc0.y))-y_band);
            xc_topleft.x    = (x0c3.x < xZc3.x ? static_cast<int>(std::floor(x0c3.x))+x_band : static_cast<int>(std::floor(xZc3.x))+x_band);
            xc_topleft.y    = (x0c3.y < xZc3.y ? static_cast<int>(std::floor(x0c3.y))+y_band : static_cast<int>(std::floor(xZc3.y))+y_band);
            xc_topright.x   = (x0c2.x > xZc2.x ? static_cast<int>(std::ceil(x0c2.x))-x_band  : static_cast<int>(std::ceil(xZc2.x))-x_band);
            xc_topright.y   = (x0c2.y < xZc2.y ? static_cast<int>(std::floor(x0c2.y))+y_band : static_cast<int>(std::floor(xZc2.y))+y_band);
        }
        
        cout<<"xc_bottmleft  "<<xc_bottmleft<<endl
            <<"xc_bottmright "<<xc_bottmright<<endl
            <<"xc_topleft    "<<xc_topleft<<endl
            <<"xc_topright   "<<xc_topright<<endl;

        points=std::vector<cv::Point2i>{xc_bottmleft, xc_bottmright, xc_topright, xc_topleft, xc_bottmleft};
    }

    for(int it_seq=it_bkg[0]; it_seq<it_tot_case+1; ++it_seq){
        std::string imgRecFile=str(boost::format("%1%/cam%2%/cam_c%3%_it%4$03d.png") % imgdir % i_cam_ % i_cam_ % it_seq );
        cout<<"src file "<<imgRecFile<<endl;

        cv::Mat imgRec=cv::imread(imgRecFile, cv::IMREAD_ANYDEPTH);
        cv::Mat imgHighPass;

        if(high_pass_flag){
            cout<<"High pass filter..."<<endl;
            // cv::medianBlur(imgRec, imgHighPass, 3);
            // cv::fastNlMeansDenoising(imgRec, imgHighPass, std::vector<float>{10}, 5, 7, cv::NORM_L1);
            cv::Mat ErodeKernel = cv::Mat_<uchar>::ones(subtract_sliding_minimum_size, subtract_sliding_minimum_size), img_erode, img;
            cv::erode(imgRec, img_erode, ErodeKernel);
            cv::subtract(imgRec, img_erode, img);
            cv::subtract(img, subtract_constant_pixel_value, img);
            imgHighPass = img * multiply_pixel_factor;
        }
        else{
            cout<<"Just copy..."<<endl;
            imgHighPass=imgRec.clone();
        }
        
        cv::Mat imgMasked;
        if(mask_flag){
            cout<<"Put mask..."<<endl;
            cv::Mat mask = cv::Mat::zeros( imgHighPass.size(), CV_8UC1 );
            cv::fillConvexPoly(mask, points, cv::Scalar(255));

            imgMasked = cv::Mat::zeros( imgHighPass.size(), imgHighPass.type() );
            imgHighPass.copyTo(imgMasked, mask);
        }
        else{
            cout<<"Just copy..."<<endl;
            imgMasked=imgHighPass.clone();
        }

        cv::Mat imgUndistort;
        if(undistort_original_image_flag){
            cout<<"Undistort images..."<<endl;
            cv::undistort(imgMasked, imgUndistort, cameraMatrix_, distCoeffs_);
        }
        else{
            cout<<"Just copy..."<<endl;
            imgUndistort=imgMasked.clone();
        }

        std::string fullname=str(boost::format("%1%/cam_c%2%_it%3$03d.png") % preproc_dir % i_cam_ % it_seq );  
        cout<<"dst file "<<fullname<<endl;

        cv::imwrite(fullname, imgUndistort, utils::compression_params);
    }
}