#include "../Evaluation.hpp"

void eval::evaluateOPTTracks(const std::vector<int>& it_fct, const cv::Point3d& dXc_px,
                                    const std::vector<Particle_ptr>& particles_opt, 
                                    const std::vector<Particle_ptr>& particles_ref,
                                    const std::vector<Camera_ptr>& cam){
    
    auto particles_pair_list = findOPTTracksPairs(it_fct, dXc_px, particles_opt, particles_ref);
    computeOPTTracksLength(it_fct, particles_opt, particles_ref, particles_pair_list);
    computeOPTTracksErrors(it_fct, particles_opt, particles_ref, particles_pair_list, cam);
}

void eval::evaluateOPTParticles(const std::vector<int>& it_fct, const cv::Point3d& dXc_px,
                                        const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                        const std::vector<Particle_ptr>& particles_opt, 
                                        const std::vector<Particle_ptr>& particles_ref,
                                        const std::vector<Camera_ptr>& cam){

    evaluateTrackedParticles(it_fct, dXc_px, Xc_top, Xc_bottom, particles_opt, particles_ref);
    cout<<"#####################################################################"<<endl;
    evaluateDetectedTrueParticles(it_fct, dXc_px, Xc_top, Xc_bottom, particles_opt, particles_ref, cam);
}

std::vector<std::pair<int, std::vector<int>>>
eval::findOPTTracksPairs(const std::vector<int>& it_fct, const cv::Point3d& dXc_px,
                                    const std::vector<Particle_ptr>& particles_opt, 
                                    const std::vector<Particle_ptr>& particles_ref){

    std::vector<std::pair<int, std::vector<int>>> particles_pair_list;
    int    d_opt=0;

    double ratio =  1.;

    cout<<"Compare two particle lists"<<endl;

    for(auto ip=particles_ref.begin(); ip<particles_ref.end(); ++ip){

        if( (*ip)->Xcoord().size() >=3 ){

            std::vector<int> d_pairs;

            for(int it_seq = (*ip)->it_seq_deb(); it_seq < (*ip)->it_seq_fin(); it_seq++){

                double err_norm=0., err_norm_pre=0.;

                auto ip_Xc = (*ip)->getCurrentXcoord(it_seq);

                utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_opt);

                auto it = std::find_if(particles_opt.begin(), particles_opt.end(), 
                                        [=](auto& po){  if(po->is_ontrack() && !po->is_trained() && po->it_seq_deb()==it_seq)
                                                            return cv::norm( po->getCurrentXcoord(it_seq) - ip_Xc) < ratio*cv::norm(dXc_px);
                                                        else
                                                            return false; });

                d_opt = std::distance(std::begin(particles_opt), it);

                //Find minimum
                if(it!=particles_opt.end()){      
                    err_norm=cv::norm((*it)->getCurrentXcoord(it_seq) - ip_Xc);
                    err_norm_pre=err_norm;
                }

                while(it!=std::end(particles_opt)){
                    err_norm=cv::norm((*it)->getCurrentXcoord(it_seq) - ip_Xc);
                    if(err_norm<err_norm_pre){
                        d_opt = std::distance(std::begin(particles_opt), it);
                        err_norm_pre=err_norm;
                    }
                    it = std::find_if(++it, particles_opt.end(), 
                                        [=](auto& po){  if(po->is_ontrack() && !po->is_trained() && po->it_seq_deb()==it_seq)
                                                            return cv::norm( po->getCurrentXcoord(it_seq) - ip_Xc) < ratio*cv::norm(dXc_px);
                                                        else
                                                            return false; });
                }
                ////Find minimum

                if(d_opt != particles_opt.size()){
                    if(checkFoundPairInNextFrame(it_seq, it_seq, dXc_px, particles_opt[d_opt].get(), (*ip).get())){
                        d_pairs.push_back(d_opt);
                        it_seq = particles_opt[d_opt]->it_seq_fin(); //Important
                        particles_opt[d_opt]->markAsTrained();
                    }
                }
            }

            if(!d_pairs.empty()){
                int d_ref=std::distance(std::begin(particles_ref), ip);
                particles_pair_list.push_back( std::pair<int, std::vector<int>>(d_ref, d_pairs) );
            }
        }
    }
    
    return particles_pair_list;
}

bool eval::checkFoundPairInNextFrame(int it_seq, int it_base, const cv::Point3d& dXc_px, Particle* particle_opt, Particle* particle_ref){

    particle_opt->checkIfParticleAppearsOnSnapshot(it_seq+1);
    particle_ref->checkIfParticleAppearsOnSnapshot(it_seq+1);

    if(particle_opt->is_ontrack() && particle_ref->is_ontrack())
    {
        if(cv::norm(particle_opt->getCurrentXcoord(it_seq+1) - particle_ref->getCurrentXcoord(it_seq+1)) < 3*cv::norm(dXc_px)){
            if(it_seq - it_base < 1)
                checkFoundPairInNextFrame(it_seq+1, it_base, dXc_px, particle_opt, particle_ref);
            else
                return true;
        }
        else
            return false;
    }
    else
        return false;
}

void eval::computeOPTTracksLength(const std::vector<int>& it_fct, 
                                             const std::vector<Particle_ptr>& particles_opt, 
                                             const std::vector<Particle_ptr>& particles_ref,
                                             const std::vector<std::pair<int, std::vector<int>>>& particles_pair_list){

    int num_equal=0, num_shorter=0, num_longer=0;
    int num_break_tracks_equal=0, num_break_tracks_shorter=0, num_break_tracks_longer=0;
    int total_tracks_ref=0;
    
    std::vector<int> histogram_opt(it_fct.back(), 0), histogram_ref(it_fct.back(), 0);

    for(auto ip=particles_ref.begin(); ip<particles_ref.end(); ++ip)
        if( (*ip)->Xcoord().size() >=3 )
            total_tracks_ref++;

    for(const auto& pr: particles_pair_list){

        int ref_length = std::min(it_fct.back(), particles_ref[pr.first]->it_seq_fin()) - particles_ref[pr.first]->it_seq_deb() + 1;

        int opt_length = 0;
        for(const auto d: pr.second)
            opt_length += particles_opt[d]->Xcoord().size();

        histogram_ref[ref_length-1]++;
        histogram_opt[opt_length-1]++;

        if(opt_length == ref_length){
            num_equal++;
            if(pr.second.size()>1) num_break_tracks_equal++;
        }
        else if(opt_length < ref_length){
            num_shorter++;
            if(pr.second.size()>1) num_break_tracks_shorter++;  
        }    
        else{
            num_longer++;
            if(pr.second.size()>1) num_break_tracks_longer++;
        }                     
    }

    cout<<"########################################################"<<endl;
    cout<<str(boost::format("Total reference tracks >= 3              : %1$6d") % total_tracks_ref)<<endl;
    cout<<str(boost::format("Track length equal number           GOOD : %1$6d in which breaking tracks numbers: %2$6d") % num_equal % num_break_tracks_equal)<<endl;
    cout<<str(boost::format("Track length longer  than reference BAD  : %1$6d in which breaking tracks numbers: %2$6d") % num_longer % num_break_tracks_longer)<<endl;
    cout<<str(boost::format("Track length shorter than reference BAD  : %1$6d in which breaking tracks numbers: %2$6d") % num_shorter % num_break_tracks_shorter)<<endl;

    for(int i=0; i<histogram_opt.size(); i++){
        int length = i+1;
        cout<<str(boost::format("Track length %1$2d, number of opt %2$5d number of ref %3$5d") % length % histogram_opt[i] % histogram_ref[i] )<<endl;
    }
}

void eval::computeOPTTracksErrors(const std::vector<int>& it_fct, 
                                            const std::vector<Particle_ptr>& particles_opt, 
                                            const std::vector<Particle_ptr>& particles_ref, 
                                            const std::vector<std::pair<int,std::vector<int>>>& particles_pair_list,
                                            const std::vector<Camera_ptr>& cam){

    for(int it_seq = it_fct[0]; it_seq < it_fct[0] + it_fct.size(); it_seq++){
        double err_norm_px=0.;
        int    num_detected_true = 0, num_undetected = 0;
        int    d_opt=0;

        std::vector<double> err_norm_px_vec;
        std::vector<int>    histogram(11, 0);

        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_ref);
        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_opt);

        for(const auto& pr: particles_pair_list){

            if(particles_ref[pr.first]->is_ontrack()){

                d_opt=-1;
                for(auto d: pr.second){
                    if(particles_opt[d]->is_ontrack()){
                        d_opt=d;
                        break;
                    }
                }
                if(d_opt==-1) continue;

                if(particles_opt[d_opt]->is_ontrack()){

                    auto Xc_ref = particles_ref[pr.first]->getCurrentXcoord(it_seq);

                    auto Xc_opt = particles_opt[d_opt]->getCurrentXcoord(it_seq);

                    err_norm_px=0.;
                    for(int i_cam=0; i_cam<cam.size(); i_cam++)
                        err_norm_px+=cv::norm(cam.at(i_cam)->mappingFunction(Xc_opt) - cam.at(i_cam)->mappingFunction(Xc_ref))/static_cast<double>(cam.size());

                    if(err_norm_px>1)
                        num_undetected++;
                    else{
                        err_norm_px_vec.push_back(err_norm_px);
                        num_detected_true++;
                    }

                    if(err_norm_px < 0.01)                              histogram[0]++;
                    else if(err_norm_px >= 0.01 && err_norm_px < 0.02)  histogram[1]++;
                    else if(err_norm_px >= 0.02 && err_norm_px < 0.03)  histogram[2]++;
                    else if(err_norm_px >= 0.03 && err_norm_px < 0.04)  histogram[3]++;
                    else if(err_norm_px >= 0.04 && err_norm_px < 0.05)  histogram[4]++;
                    else if(err_norm_px >= 0.05 && err_norm_px < 0.06)  histogram[5]++;
                    else if(err_norm_px >= 0.06 && err_norm_px < 0.07)  histogram[6]++;
                    else if(err_norm_px >= 0.07 && err_norm_px < 0.08)  histogram[7]++;
                    else if(err_norm_px >= 0.08 && err_norm_px < 0.09)  histogram[8]++;
                    else if(err_norm_px >= 0.09 && err_norm_px < 0.1)   histogram[9]++;
                    else                                                histogram[10]++;
                }
            }
        }
        cout<<"########################################################"<<endl;
        cout<<"For time level                             : "<<it_seq<<endl;
        cout<<"Number of detected true particles          : "<<num_detected_true<<endl;
        cout<<"Number of undetected particles             : "<<num_undetected<<endl;
        cout<<"Mean error of detected true particles (px) : "<<std::accumulate(err_norm_px_vec.begin(), err_norm_px_vec.end(), 0.0)/static_cast<double>(num_detected_true)<<endl;
        cout<<"particles error < 0.01                     : "<<histogram[0]<<endl;
        cout<<"particles error < 0.02                     : "<<histogram[1]<<endl;
        cout<<"particles error < 0.03                     : "<<histogram[2]<<endl;
        cout<<"particles error < 0.04                     : "<<histogram[3]<<endl;
        cout<<"particles error < 0.05                     : "<<histogram[4]<<endl;
        cout<<"particles error < 0.06                     : "<<histogram[5]<<endl;
        cout<<"particles error < 0.07                     : "<<histogram[6]<<endl;
        cout<<"particles error < 0.08                     : "<<histogram[7]<<endl;
        cout<<"particles error < 0.09                     : "<<histogram[8]<<endl;
        cout<<"particles error < 0.10                     : "<<histogram[9]<<endl;
        cout<<"particles error > 0.10                     : "<<histogram[10]<<endl;
    }
}

void eval::evaluateTrackedParticles(const std::vector<int>& it_rng, const cv::Point3d& dXc_px,
                                           const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                           const std::vector<Particle_ptr>& particles_tgt, const std::vector<Particle_ptr>& particles_ref){

    evaluateUndetectedTrueParticles(it_rng, dXc_px, Xc_top, Xc_bottom, particles_tgt, particles_ref);
    evaluateTrackedGhostParticles(it_rng, dXc_px, Xc_top, Xc_bottom, particles_tgt, particles_ref);
    // evaluateTotalGhostParticles(it_rng, dXc_px, Xc_top, Xc_bottom, particles_tgt, particles_ref);
}

void eval::evaluateUndetectedTrueParticles(const std::vector<int>& it_rng, const cv::Point3d& dXc_px, 
                                                    const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                                    const std::vector<Particle_ptr>& particles_tgt, const std::vector<Particle_ptr>& particles_ref){

    cout<<"Convert Synthetic Particles Coords To Frame"<<endl
        <<"Synthetic particles in each frame"<<endl;

    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_in_frames_cloud;
    convertParticlesToFrame(it_rng, particles_ref, false, particles_in_frames_cloud);

    cout<<"Search if true particle is found or not"<<endl;

    searchParticleInFrame(it_rng, particles_tgt, cv::norm(dXc_px), false, Xc_top, Xc_bottom, particles_in_frames_cloud);

    std::vector<int> num_undetected(it_rng.size(), 0);
    for(int it_seq=it_rng.at(0); it_seq<it_rng.at(0)+it_rng.size(); it_seq++){
        int it_frame =it_seq - it_rng[0];
        num_undetected[it_frame]=particles_in_frames_cloud[it_frame]->size();
    }

    cout<<"number of undetected true particles : "<<endl;
    for(auto& n: num_undetected)           cout<<n<<endl;
}

void eval::evaluateTrackedGhostParticles(const std::vector<int>& it_rng, const cv::Point3d& dXc_px, 
                                                const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                                const std::vector<Particle_ptr>& particles_tgt, const std::vector<Particle_ptr>& particles_ref){

    cout<<"Convert Reconstructed Particles Tracks (history >= 4) To Frame"<<endl
        <<"Reconstructed particles tracks in each frame"<<endl;

    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_in_frames_cloud;
    convertParticlesToFrame(it_rng, particles_tgt, true, particles_in_frames_cloud);

    cout<<"Search if ghost particle is tracked or not"<<endl;

    searchParticleInFrame(it_rng, particles_ref, cv::norm(dXc_px), false, Xc_top, Xc_bottom, particles_in_frames_cloud);

    std::vector<int> num_tracked_ghost(it_rng.size(), 0);
    for(int it_seq=it_rng.at(0); it_seq<it_rng.at(0)+it_rng.size(); it_seq++){
        int it_frame =it_seq - it_rng[0];
        num_tracked_ghost[it_frame]=particles_in_frames_cloud[it_frame]->size();
    }

    cout<<"number of tracked ghost particles : "<<endl;
    for(auto& n: num_tracked_ghost)      cout<<n<<endl;
}

void eval::evaluateTotalGhostParticles(const std::vector<int>& it_rng, const cv::Point3d& dXc_px, 
                                            const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                            const std::vector<Particle_ptr>& particles_tgt, const std::vector<Particle_ptr>& particles_ref){

    cout<<"Convert Reconstructed Particles Candidates To Frame"<<endl
        <<"Reconstructed particles in each frame"<<endl;

    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_in_frames_cloud;
    convertParticlesToFrame(it_rng, particles_tgt, false, particles_in_frames_cloud);

    cout<<"Search if ghost particle is found or not"<<endl;

    searchParticleInFrame(it_rng, particles_ref, cv::norm(dXc_px), false, Xc_top, Xc_bottom, particles_in_frames_cloud);

    std::vector<int> num_tot_ghost(it_rng.size(), 0);
    for(int it_seq=it_rng.at(0); it_seq<it_rng.at(0)+it_rng.size(); it_seq++){
        int it_frame =it_seq - it_rng[0];
        num_tot_ghost[it_frame]=particles_in_frames_cloud[it_frame]->size();
    }

    cout<<"number of total ghost particles : "<<endl;
    for(auto& n: num_tot_ghost)        cout<<n<<endl;
}

void eval::convertParticlesToFrame(const std::vector<int>& it_rng, const std::vector<Particle_ptr>& particles_tgt, bool track_flag,
                                          std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_in_frames_cloud){

    int track_length=(track_flag ? 4 : 1);

    for(int it_seq=it_rng.at(0); it_seq<it_rng.at(0)+it_rng.size(); it_seq++){

        particles_in_frames_cloud.push_back(pcl::make_shared<pcl::PointCloud<pcl::PointXYZI>>());

        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_tgt);

        for(auto& pt: particles_tgt){
            if(pt->is_ontrack() && pt->Xcoord().size()>=track_length){
                particles_in_frames_cloud.back()->push_back( cv2pcl(pt->getCurrentXcoord(it_seq), pt->getCurrentE(it_seq)) );
            }
        }

        cout<<"it_seq "<<it_seq<<" particle bkg number "<<particles_in_frames_cloud.back()->size()<<endl;
    }
}

void eval::evaluateDetectedTrueParticles(const std::vector<int>& it_rng, const cv::Point3d& dXc_px, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                                    const std::vector<Particle_ptr>& particles_tgt, const std::vector<Particle_ptr>& particles_ref,
                                                    const std::vector<Camera_ptr>& cam){

    cout<<"Convert Synthetic Particles Coords To Frame"<<endl
        <<"Synthetic particles in each frame"<<endl;

    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_in_frames_cloud;
    convertParticlesToFrame(it_rng, particles_ref, false, particles_in_frames_cloud);

    double  threshold = cv::norm(dXc_px);

    for(int it_seq=it_rng.at(0); it_seq<it_rng.at(0)+it_rng.size(); it_seq++){
        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_tgt);

        std::vector<double> err_norm_vec,
                            err_norm_px_vec;
        std::vector<int>    histogram(11, 0);

        int it_frame =it_seq - it_rng[0];

        int    num_detected_true = 0;

        auto particles_in_cnt_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(1.);
        particles_in_cnt_frame_octree.defineBoundingBox(Xc_bottom.x, Xc_bottom.y, Xc_bottom.z, Xc_top.x, Xc_top.y, Xc_top.z); 
        particles_in_cnt_frame_octree.setInputCloud(particles_in_frames_cloud[it_frame]);
        particles_in_cnt_frame_octree.addPointsFromInputCloud();

        for(auto& po: particles_tgt){
            if(po->is_ontrack() && po->Xcoord().size()>=1){
                computeNearestParticleErrorPerFrame(cv2pcl(po->getCurrentXcoord(it_seq), po->getCurrentE(it_seq)), threshold, particles_in_cnt_frame_octree, 
                                                            particles_in_frames_cloud[it_frame], cam,
                                                            num_detected_true, err_norm_vec, err_norm_px_vec, histogram);
            }
        }

        cout<<"################################################################"<<endl;
        cout<<"For time level                             : "<<it_seq<<endl;
        cout<<"Number of detected true particles          : "<<num_detected_true<<endl;
        cout<<"Mean error of detected true particles (mm) : "<<std::accumulate(err_norm_vec.begin(), err_norm_vec.end(), 0.0)/static_cast<double>(num_detected_true)<<endl;
        cout<<"Mean error of detected true particles (px) : "<<std::accumulate(err_norm_px_vec.begin(), err_norm_px_vec.end(), 0.0)/static_cast<double>(num_detected_true)<<endl;
        cout<<"particles error < 0.01                     : "<<histogram[0]<<endl;
        cout<<"particles error < 0.02                     : "<<histogram[1]<<endl;
        cout<<"particles error < 0.03                     : "<<histogram[2]<<endl;
        cout<<"particles error < 0.04                     : "<<histogram[3]<<endl;
        cout<<"particles error < 0.05                     : "<<histogram[4]<<endl;
        cout<<"particles error < 0.06                     : "<<histogram[5]<<endl;
        cout<<"particles error < 0.07                     : "<<histogram[6]<<endl;
        cout<<"particles error < 0.08                     : "<<histogram[7]<<endl;
        cout<<"particles error < 0.09                     : "<<histogram[8]<<endl;
        cout<<"particles error < 0.10                     : "<<histogram[9]<<endl;
        cout<<"particles error > 0.10                     : "<<histogram[10]<<endl;
    }
}

void eval::evaluateInitializedTracks(const std::vector<int>& it_bkg,
                                            const std::vector<Particle_ptr>& particles_tgt, 
                                            const std::vector<Particle_ptr>& particles_ref){

    assert(it_bkg.size()==4);
    int num_cat = it_bkg[0]*it_bkg[1]*it_bkg[2]*it_bkg[3];
    std::vector<int> length_ref(num_cat, 0), length_tgt(num_cat, 0);

    for(auto& pt: particles_tgt){
        if(pt->it_seq_deb() == 1      && pt->it_seq_fin() == 1) length_tgt[0]++;
        else if(pt->it_seq_deb() == 1 && pt->it_seq_fin() == 2) length_tgt[1]++;
        else if(pt->it_seq_deb() == 1 && pt->it_seq_fin() == 3) length_tgt[2]++;
        else if(pt->it_seq_deb() == 1 && pt->it_seq_fin() == 4) length_tgt[3]++;
        else if(pt->it_seq_deb() == 2 && pt->it_seq_fin() == 2) length_tgt[4]++;
        else if(pt->it_seq_deb() == 2 && pt->it_seq_fin() == 3) length_tgt[5]++;
        else if(pt->it_seq_deb() == 2 && pt->it_seq_fin() == 4) length_tgt[6]++;
        else if(pt->it_seq_deb() == 3 && pt->it_seq_fin() == 3) length_tgt[7]++;
        else if(pt->it_seq_deb() == 3 && pt->it_seq_fin() == 4) length_tgt[8]++;
        else if(pt->it_seq_deb() == 4 && pt->it_seq_fin() == 4) length_tgt[9]++;
    }

    for(auto& pt: particles_ref){
        if(pt->it_seq_deb() == 1      && pt->it_seq_fin() == 1) length_ref[0]++;
        else if(pt->it_seq_deb() == 1 && pt->it_seq_fin() == 2) length_ref[1]++;
        else if(pt->it_seq_deb() == 1 && pt->it_seq_fin() == 3) length_ref[2]++;
        else if(pt->it_seq_deb() == 1 && pt->it_seq_fin() >= 4) length_ref[3]++;
        else if(pt->it_seq_deb() == 2 && pt->it_seq_fin() == 2) length_ref[4]++;
        else if(pt->it_seq_deb() == 2 && pt->it_seq_fin() == 3) length_ref[5]++;
        else if(pt->it_seq_deb() == 2 && pt->it_seq_fin() >= 4) length_ref[6]++;
        else if(pt->it_seq_deb() == 3 && pt->it_seq_fin() == 3) length_ref[7]++;
        else if(pt->it_seq_deb() == 3 && pt->it_seq_fin() >= 4) length_ref[8]++;
        else if(pt->it_seq_deb() == 4 && pt->it_seq_fin() >= 4) length_ref[9]++;
    }

    cout<<"####################################################################"<<endl;
    cout<<"######################### Tgt ################ Ref #################"<<endl;
    cout<<str(boost::format("Track 1,1 %1$5d %2$5d") %  length_tgt[0] % length_ref[0] )<<endl;
    cout<<str(boost::format("Track 1,2 %1$5d %2$5d") %  length_tgt[1] % length_ref[1] )<<endl;
    cout<<str(boost::format("Track 1,3 %1$5d %2$5d") %  length_tgt[2] % length_ref[2] )<<endl;
    cout<<str(boost::format("Track 1,4 %1$5d %2$5d") %  length_tgt[3] % length_ref[3] )<<endl;
    cout<<str(boost::format("Track 2,2 %1$5d %2$5d") %  length_tgt[4] % length_ref[4] )<<endl;
    cout<<str(boost::format("Track 2,3 %1$5d %2$5d") %  length_tgt[5] % length_ref[5] )<<endl;
    cout<<str(boost::format("Track 2,4 %1$5d %2$5d") %  length_tgt[6] % length_ref[6] )<<endl;
    cout<<str(boost::format("Track 3,3 %1$5d %2$5d") %  length_tgt[7] % length_ref[7] )<<endl;
    cout<<str(boost::format("Track 3,4 %1$5d %2$5d") %  length_tgt[8] % length_ref[8] )<<endl;
    cout<<str(boost::format("Track 4,4 %1$5d %2$5d") %  length_tgt[9] % length_ref[9] )<<endl;
}

void eval::evaluateIPRParticles(const std::vector<int>& it_bkg, const cv::Point3d& dXc_px, 
                                const std::vector<Camera_ptr>& cam,
                                const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom, 
                                const std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_ipr_in_frames,
                                const std::vector<Particle_ptr>& particles_bkg,
                                double ipr_error_ratio){

    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_in_frames_cloud;
    convertParticlesToFrame(it_bkg, particles_bkg, false, particles_in_frames_cloud);

    cout<<"Search detected true particle"<<endl;

    double  threshold = ipr_error_ratio*cv::norm(dXc_px);

    for(int it_seq=it_bkg.at(0); it_seq<it_bkg.at(0)+it_bkg.size(); it_seq++){

        std::vector<double> err_norm_vec,
                            err_norm_px_vec;
        std::vector<int>    histogram(11, 0);

        int it_frame = it_seq - it_bkg[0];

        int num_detected_true = 0;

        auto particles_in_cnt_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(1.);
        particles_in_cnt_frame_octree.defineBoundingBox(Xc_bottom.x, Xc_bottom.y, Xc_bottom.z, Xc_top.x, Xc_top.y, Xc_top.z); 
        particles_in_cnt_frame_octree.setInputCloud( particles_in_frames_cloud[it_frame] );
        particles_in_cnt_frame_octree.addPointsFromInputCloud();

        for(const auto& pt: particles_ipr_in_frames[it_frame]->points){
            computeNearestParticleErrorPerFrame(pt, threshold, particles_in_cnt_frame_octree, particles_in_frames_cloud[it_frame], cam,
                                                        num_detected_true, err_norm_vec, err_norm_px_vec, histogram);
        }

        cout<<"################################################################"<<endl;
        cout<<"For time level                             : "<<it_seq<<endl;
        cout<<"Number of detected true particles          : "<<num_detected_true<<endl;
        cout<<"Mean error of detected true particles      : "<<std::accumulate(err_norm_vec.begin(), err_norm_vec.end(), 0.0)/static_cast<double>(num_detected_true)<<endl;
        cout<<"Mean error of detected true particles (px) : "<<std::accumulate(err_norm_px_vec.begin(), err_norm_px_vec.end(), 0.0)/static_cast<double>(num_detected_true)<<endl;
        cout<<"particles error < 0.01                     : "<<histogram[0]<<endl;
        cout<<"particles error < 0.02                     : "<<histogram[1]<<endl;
        cout<<"particles error < 0.03                     : "<<histogram[2]<<endl;
        cout<<"particles error < 0.04                     : "<<histogram[3]<<endl;
        cout<<"particles error < 0.05                     : "<<histogram[4]<<endl;
        cout<<"particles error < 0.06                     : "<<histogram[5]<<endl;
        cout<<"particles error < 0.07                     : "<<histogram[6]<<endl;
        cout<<"particles error < 0.08                     : "<<histogram[7]<<endl;
        cout<<"particles error < 0.09                     : "<<histogram[8]<<endl;
        cout<<"particles error < 0.10                     : "<<histogram[9]<<endl;
        cout<<"particles error > 0.10                     : "<<histogram[10]<<endl;
        cout<<"Number of ghost particles                  : "<<particles_ipr_in_frames[it_frame]->size()-num_detected_true<<endl;
    }
}

void eval::searchParticleInFrame(const std::vector<int>& it_rng, const std::vector<Particle_ptr>& particles_origin,
                                        double threshold, bool track_flag, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
                                        std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr>& particles_in_frames_cloud){

    int track_length=(track_flag ? 4 : 1);

    for(int it_seq=it_rng.at(0); it_seq<it_rng.at(0)+it_rng.size(); it_seq++){
        utils::checkIfParticlesAppearOnSnapshot(it_seq, particles_origin);

        int it_frame =it_seq - it_rng[0];

        auto particles_in_cnt_frame_octree = pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(1.);
        particles_in_cnt_frame_octree.defineBoundingBox(Xc_bottom.x, Xc_bottom.y, Xc_bottom.z, Xc_top.x, Xc_top.y, Xc_top.z); 
        particles_in_cnt_frame_octree.setInputCloud(particles_in_frames_cloud[it_frame]);
        particles_in_cnt_frame_octree.addPointsFromInputCloud();

        std::vector<int> idx_rm;

        for(auto& po: particles_origin){
            if(po->is_ontrack() && po->Xcoord().size()>=track_length){
                std::vector<int>     k_indices;
                std::vector<float>   k_sqr_distances;

                particles_in_cnt_frame_octree.nearestKSearch(cv2pcl(po->getCurrentXcoord(it_seq), 0), 1, k_indices, k_sqr_distances);

                if(std::sqrt(k_sqr_distances[0])<threshold){
                    idx_rm.push_back(k_indices[0]);
                }
            }
        }

        for(auto idx: idx_rm){
            if(!particles_in_frames_cloud[it_frame]->empty()){
                particles_in_frames_cloud[it_frame]->erase( particles_in_frames_cloud[it_frame]->begin()+idx );
            }
        }

    }
}

void eval::computeNearestParticleErrorPerFrame(const pcl::PointXYZI& searchPoint, double threshold,
                                                      pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>& particles_octree_in_cnt_frame,
                                                      pcl::PointCloud<pcl::PointXYZI>::Ptr particles_cloud_in_cnt_frame,
                                                      const std::vector<Camera_ptr>& cam,
                                                      int&                 num_detected_true,
                                                      std::vector<double>& err_norm_vec,
                                                      std::vector<double>& err_norm_px_vec,
                                                      std::vector<int>&    histogram){

    auto pt = cv::Point3d(searchPoint.x, searchPoint.y, searchPoint.z);

    std::vector<int>     k_indices;
    std::vector<float>   k_sqr_distances;

    particles_octree_in_cnt_frame.nearestKSearch(searchPoint, 1, k_indices, k_sqr_distances);

    if(std::sqrt(k_sqr_distances[0])<threshold){
        auto pt_pcl_found = particles_cloud_in_cnt_frame->points[ k_indices[0] ];

        auto pt_cv_found = cv::Point3d(pt_pcl_found.x, pt_pcl_found.y, pt_pcl_found.z);

        double err_norm=std::sqrt( k_sqr_distances[0] );

        double err_norm_px=0.;
        for(int i_cam=0; i_cam<cam.size(); i_cam++){
            err_norm_px+=cv::norm(cam.at(i_cam)->mappingFunction(pt) - cam.at(i_cam)->mappingFunction(pt_cv_found))/static_cast<double>(cam.size());
        }

        err_norm_vec.push_back(err_norm);
        err_norm_px_vec.push_back(err_norm_px);

        num_detected_true++;

        if(err_norm_px < 0.01)                              histogram[0]++;
        else if(err_norm_px >= 0.01 && err_norm_px < 0.02)  histogram[1]++;
        else if(err_norm_px >= 0.02 && err_norm_px < 0.03)  histogram[2]++;
        else if(err_norm_px >= 0.03 && err_norm_px < 0.04)  histogram[3]++;
        else if(err_norm_px >= 0.04 && err_norm_px < 0.05)  histogram[4]++;
        else if(err_norm_px >= 0.05 && err_norm_px < 0.06)  histogram[5]++;
        else if(err_norm_px >= 0.06 && err_norm_px < 0.07)  histogram[6]++;
        else if(err_norm_px >= 0.07 && err_norm_px < 0.08)  histogram[7]++;
        else if(err_norm_px >= 0.08 && err_norm_px < 0.09)  histogram[8]++;
        else if(err_norm_px >= 0.09 && err_norm_px < 0.1)   histogram[9]++;
        else                                                histogram[10]++;
    }
}
