#include "../ENS.hpp"

ENS::ENS(){}

ENS::~ENS(){}

/// ENS version

ENS::ENS(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag, 
        int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
        double pixel_lost, double dtObs, 
        const cv::Point3d& dXc_px, const std::string& res_path, int eval_window,
        bool warm_start_flag, int warm_start_max_iter, double warm_start_pixel, double warm_start_relaxfactor,
        int n_ens, double regul_const, int outer_max_iter, bool local_analysis_flag, 
        bool resample_flag, const cv::Point3d& sigmaXcRatio, double sigmaE, bool joint_intensity_flag)
        :OptMethod(method_type, control_vector_type, intensity_control_flag,
                   n_part, n_part_per_snapshot, Xc_top, Xc_bottom, 
                   pixel_lost, dtObs, 
                   dXc_px, res_path, eval_window,
                   warm_start_flag, warm_start_max_iter, warm_start_pixel, warm_start_relaxfactor),
        n_ens_(n_ens), regul_const_(regul_const), outer_max_iter_(outer_max_iter), local_analysis_flag_(local_analysis_flag),
        resample_flag_(resample_flag), sigmaXcRatio_(sigmaXcRatio), sigmaE_(sigmaE), joint_intensity_flag_(joint_intensity_flag)
        {
            ipr_full_flag_=false;
        }

/// IPR version

ENS::ENS(Method_Type method_type, ControlVector_Type control_vector_type, bool intensity_control_flag, 
        int n_part, int n_part_per_snapshot, const cv::Point3d& Xc_top, const cv::Point3d& Xc_bottom,
        const cv::Point3d& dXc_px, const std::string& res_path, int eval_window,
        int n_ens, double regul_const, int ipr_max_iter, bool local_analysis_flag, 
        bool resample_flag, const cv::Point3d& sigmaXcRatio, double sigmaE, bool joint_intensity_flag)
        :OptMethod(method_type, control_vector_type, intensity_control_flag, 
                  n_part, n_part_per_snapshot, Xc_top, Xc_bottom, 
                  dXc_px, res_path, eval_window),
        n_ens_(n_ens), regul_const_(regul_const), outer_max_iter_(ipr_max_iter), local_analysis_flag_(local_analysis_flag),
        resample_flag_(resample_flag), sigmaXcRatio_(sigmaXcRatio), sigmaE_(sigmaE), joint_intensity_flag_(joint_intensity_flag)
        {
            ipr_full_flag_=true;
        }

void ENS::initialization(std::vector<Particle_ptr>& particles_ana, int it_seq_cnt){

    if(!ipr_full_flag_)
        utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ana, Xc_top_, Xc_bottom_);

    this->setintensity_threshold(particles_ana);
    this->initializeEnsembleCore(particles_ana, true, true);

    if(ipr_full_flag_){
        for (auto& particles_n_ens : particles_ens_ana_)
            for (auto& particle_p : particles_n_ens)     
                if(!particle_p->is_triangulated()) particle_p->markAsTriangulated();
    }
}

void ENS::pertubeEnsemblePosition(int it_seq_cnt, bool prediction_flag, bool resample_flag){

    for(int n=0; n<n_ens_; n++){
        int p=0;
        for(auto& particle_p : particles_ens_ana_.at(n))
        {
            if(particle_p->is_ontrack() && particle_p->is_to_be_tracked())
            {
                cv::Point3d noise_Xc(dXc_px_.x*sigmaXcRatio_.x*Random::StandardNormalRandomNumber(0.0),
                                     dXc_px_.y*sigmaXcRatio_.y*Random::StandardNormalRandomNumber(0.0),
                                     dXc_px_.z*sigmaXcRatio_.z*Random::StandardNormalRandomNumber(0.0));

                int  it_seq_set = resample_flag ? it_seq_cnt :
                                                (ipr_full_flag_ ? it_seq_cnt : it_seq_cnt - 1);

                auto Xc_np = particles_ens_ana_.back().at(p)->getCurrentXcoord(it_seq_set) + noise_Xc;
                utils::maintainPositionInRange(Xc_np, Xc_top_, Xc_bottom_);

                if(prediction_flag){
                    particle_p->push_backXcoordElement(Xc_np, it_seq_set);
                    particle_p->setit_seq_fin(it_seq_set);
                }
                else{
                    particle_p->setXcoordElement(Xc_np, it_seq_set);
                }
            }
            p++;
        }
    }
}

void ENS::pertubeEnsembleIntensity(int it_seq_cnt, bool intensity_control_flag, bool prediction_flag, bool resample_flag){

    for(int n=0; n<n_ens_; n++){
        int p=0;
        for(auto& particle_p : particles_ens_ana_.at(n)){
            if(particle_p->is_ontrack() && this->checkIfParticleIsToBeOptimized(particle_p.get())){

                int  it_seq_set = resample_flag ? it_seq_cnt :
                                                (ipr_full_flag_ ? it_seq_cnt : it_seq_cnt - 1);
                
                double  noise_E=0.;    
                if(intensity_control_flag){
                    noise_E=sigmaE_*Random::StandardNormalRandomNumber(0.0)*particles_ens_ana_.back().at(p)->getCurrentE(it_seq_set);
                }
                auto E_np=std::max( intensity_min_threshold_, particles_ens_ana_.back().at(p)->getCurrentE(it_seq_set) + noise_E );

                if(prediction_flag)
                    particle_p->push_backEvElement(E_np, it_seq_set);
                else
                    particle_p->setEvElement(E_np, it_seq_set);
            }
            p++;
        }
    }
}

void ENS::initializeEnsembleCore(std::vector<Particle_ptr>& particles_obj, bool coord_pert_flag, bool intensity_pert_flag){

    for(int n=0; n<n_ens_; n++){
        std::vector<Particle_ptr>  particles_n_ens;
        for(const auto& particle_p : particles_obj){
            if(particle_p->is_ontrack() && this->checkIfParticleIsToBeOptimized(particle_p.get())){

                MatrixXd noiseX_mat, noiseY_mat, noiseZ_mat;

                noiseX_mat=Random::NormalRandomMatrix(1, particle_p->length(), 0.0, coord_pert_flag ? dXc_px_.x*sigmaXcRatio_.x : 0);
                noiseY_mat=Random::NormalRandomMatrix(1, particle_p->length(), 0.0, coord_pert_flag ? dXc_px_.y*sigmaXcRatio_.y : 0);
                noiseZ_mat=Random::NormalRandomMatrix(1, particle_p->length(), 0.0, coord_pert_flag ? dXc_px_.z*sigmaXcRatio_.z : 0);

                std::vector<cv::Point3d>   Xcoord_np;
                for(int it_seq=0; it_seq<particle_p->length(); it_seq++){
                    cv::Point3d noise_Xc(noiseX_mat(0,it_seq), noiseY_mat(0,it_seq), noiseZ_mat(0,it_seq));
                    auto Xc_np=particle_p->Xcoord().at(it_seq) + noise_Xc;
                    utils::maintainPositionInRange(Xc_np, Xc_top_, Xc_bottom_);
                    Xcoord_np.push_back(Xc_np);
                }

                std::vector<double>        Ev_np(particle_p->Ev());
                if(intensity_control_flag_){
                    if(intensity_pert_flag) Ev_np.back()=Ev_np.back()*(1.+sigmaE_*Random::StandardNormalRandomNumber(0.0));
                    Ev_np.back()=std::max( intensity_min_threshold_, Ev_np.back() );
                }    

                particles_n_ens.push_back( std::make_unique<Particle>( particle_p->part_index(), particle_p->it_seq_deb(), particle_p->it_seq_fin(), Xcoord_np, Ev_np ) );
            }
            else{
                particles_n_ens.push_back( std::make_unique<Particle>( *particle_p  ) );
            }
        }
        particles_ens_ana_.push_back( std::move(particles_n_ens) );
    }
    particles_ens_ana_.push_back( std::move(particles_obj) );
}

void ENS::predict(DynModel* dynmodel, int it_seq_cnt, int it_fct_deb){

    if(it_seq_cnt > it_fct_deb && resample_flag_){
        cout<<"Re-sample ensemble"<<endl;
        this->resampleEnsemble(it_seq_cnt);
    }

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ens_ana_.back(), Xc_top_, Xc_bottom_);
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);

    cout<<"Before position prediction"<<endl;  
    for(int i=0; i<particles_ens_ana_.size(); i++){
        dynmodel->predictNextPaticlesPosition(particles_ens_ana_[i], it_seq_cnt);
    }
    cout<<"After position prediction"<<endl;

    utils::checkIfParticlesAppearOnSnapshotInterface(it_seq_cnt, particles_ens_ana_.back(), Xc_top_, Xc_bottom_);
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);
    this->maintainPositionInRange(it_seq_cnt);

    for(const auto& pt: particles_ens_ana_.back()){
        if(pt->is_ontrack() && pt->is_to_be_tracked() && !pt->is_side()){
            pt->computeVelocity(it_seq_cnt, dtObs_, Particle::Velocity_scheme_Type::FiniteDifference);
        }
    }

    cout<<"Before intensity prediction"<<endl;
    this->predictIntensity(it_seq_cnt);
    this->pertubeEnsembleIntensity(it_seq_cnt, intensity_control_flag_, true);
    cout<<"After intensity prediction"<<endl;
}

/// ENS version

void ENS::prepare(int it_seq_cnt, const std::vector<Camera_ptr>& cam, int lapiv_iter){

    this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::JointMean, cam);
    this->processingResidualImage(cam, 0, particles_ens_ana_.back(), it_seq_cnt, true, true, lapiv_iter);
    this->setintensity_threshold(particles_ens_ana_.back());

    if(!ipr_full_flag_ && warm_start_flag_){
        this->processingImageResidualParticle(particles_ens_ana_.back(), it_seq_cnt, cam);
        this->setres_parti(warm_start_max_iter_, it_seq_cnt, particles_ens_ana_.back(), cam);
    }

    this->printTrackInformation(it_seq_cnt);

    n_new_particles_.push_back(0);
    n_append_particles_.push_back(0);

    continue_flag_.assign(n_particles_, 0);
}

void ENS::selectWarmStart(int it_seq_cnt, const std::vector<Camera_ptr>& cam){
    
    lMax_=warm_start_max_iter_;

    this->doWarmStart(it_seq_cnt, cam, particles_ens_ana_.back());

    if(resample_flag_)  this->resampleEnsemble(it_seq_cnt);
}

void ENS::core(int it_seq_cnt, int k, const std::vector<Camera_ptr>& cam, bool rm_outlier_flag){

    cout<<"-----------------------------------------------------"<<endl;
    cout<<"Outer loop iteration     : "<<k<<endl;
    cout<<"residual begin iteration : "<<res_glob_.back()<<endl;

    assert(local_analysis_flag_ && joint_intensity_flag_);

    this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::Joint, cam);

    this->updateParticles(it_seq_cnt, particles_ens_ana_.back(), cam, continue_flag_);

    if(rm_outlier_flag){
        this->removeOutlier(it_seq_cnt, particles_ens_ana_.back(), k);
        this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);
        this->pertubeEnsemblePosition(it_seq_cnt, false, false);
    }
    
    this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::JointMean, cam);
    this->processingResidualImage(cam, k, particles_ens_ana_.back(), it_seq_cnt, false);
    cout<<"residual after position correction, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;

    if(intensity_control_flag_){
        this->removeGhost(it_seq_cnt, k, cam);
    }
}

void ENS::removeGhost(int it_seq_cnt, int k, const std::vector<Camera_ptr>& cam, bool use_lapiv){

    this->processingImageResidualParticle(particles_ens_ana_.back(), it_seq_cnt, cam);
    cout<<"average intensity is before: "<<this->computeAverageIntensity(particles_ens_ana_.back())<<endl;

    for(const auto& pt: particles_ens_ana_.back()){
        if( pt->is_ontrack() && this->checkIfParticleIsToBeOptimized(pt.get())){
            this->rescaleIntensityUsingTarget(pt.get(), it_seq_cnt, cam, use_lapiv);
        }
    }
    cout<<"average intensity is after:  "<<this->computeAverageIntensity(particles_ens_ana_.back())<<endl;

    this->rescaleEnsembleIntensityUsingTarget(it_seq_cnt, use_lapiv);

    this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::JointMean, cam);
    this->processingResidualImage(cam, k, particles_ens_ana_.back(), it_seq_cnt, false);
    cout<<"residual after intensity rescale, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;
    
    this->removeParticlesThatAreLostOrGhost(it_seq_cnt, particles_ens_ana_.back());
    this->checkIfEnsembleParticlesAppearOnSnapshot(it_seq_cnt);
}

void ENS::postIPRResidual(int it_seq_cnt, int k, const std::vector<Camera_ptr>& cam){

    this->projectEnsembleToImageSpace(it_seq_cnt, ENS::EnsembleProjection_Type::JointMean, cam);

    this->processingResidualImage(cam, k, particles_ens_ana_.back(), it_seq_cnt, false);
    
    cout<<"residual after triangulation on residual, iteration "<<k<<"    : "<<res_glob_.at(k+lMax_)<<endl;

    cout<<"Add ensemble for triangulated particles"<<endl;

    this->addEnsembleForTriangulatedParticles(it_seq_cnt);

    if(n_new_particles_.back() != 0){
        std::vector<double> continue_flag_new(n_new_particles_.back(), 0);
        continue_flag_.insert(std::end(continue_flag_), std::begin(continue_flag_new), std::end(continue_flag_new));

        n_particles_=particles_ens_ana_.back().size();
        assert(particles_ens_ana_.back().size()==n_particles_+n_new_particles_.back());
    }
    assert(continue_flag_.size()==particles_ens_ana_.back().size());

    cout<<"Processing lost particles"<<endl;
    this->processingLostParticles(it_seq_cnt, particles_ens_ana_.back(), cam);
}

void ENS::postIPRFull(int it_seq_cnt){

    if(resample_flag_){
        cout<<"Re-sample ensemble after update"<<endl;
        this->resampleEnsemble(it_seq_cnt);
    }
}

void ENS::update(Particle* pt, int idx, int it_seq_cnt, const std::vector<Camera_ptr>& cam, double& continue_flag){

    if(pt->is_ontrack() && this->checkIfParticleIsToBeOptimized(pt)){

        this->getRawFeature(idx, cam);

        this->getLocalLinearFormOfCostFunction(idx, cam);

        if(std::abs(continue_flag)<1e-3){

            VectorXd weight = this->solve(pt->hessian(), pt->rhs());

            continue_flag=this->updateLocalEnsembleOptimalCoordinates(idx, weight, pt->hessian(), it_seq_cnt);
            if(intensity_control_flag_) this->updateLocalEnsembleOptimalIntensities(idx, weight, pt->hessian(), it_seq_cnt);
        }
        
        if(!ipr_full_flag_ && pt->is_to_be_tracked()){ pt->markAsTracked(); }
    }
}

void ENS::getRawFeature(int p, const std::vector<Camera_ptr>& cam){

    this->computeCenteredImageLocalAnomalyMatrix(p, cam, particles_ens_ana_);
}

void ENS::computeCenteredImageLocalAnomalyMatrix(int p, const std::vector<Camera_ptr>& cam,
                                                const std::vector<std::vector<Particle_ptr>>& particles_ens){

    int it_img=0;

    cv::Mat imgFeature_icam_seq, imgnBigCanvas, imgmean, imgmeanBigCanvas;
    std::vector<cv::Mat> imgFeature_icam;
    cv::Rect pos_n, pos_mean, pos_overlap_n, pos_overlap_mean;

    for(int i_cam=0; i_cam<cam.size(); i_cam++){
        assert(particles_ens.back().at(p)->imgPos().at(i_cam).size()==1);
        assert(particles_ens.back().at(p)->img().at(i_cam).size()==1);
    }

    for(auto& particles_n_ens : particles_ens)
        if(particles_n_ens[p]->is_ontrack())
            particles_n_ens[p]->clearimgFeature();

    for(int n=0; n<n_ens_; n++){
        for(int i_cam=0; i_cam<cam.size(); i_cam++){
            if( particles_ens.at(n).at(p)->on_image().at(i_cam)==1 &&
                particles_ens.back().at(p)->on_image().at(i_cam)==1 )
            {
                assert(particles_ens.at(n).at(p)->imgPos().at(i_cam).size()==1);
                assert(particles_ens.at(n).at(p)->img().at(i_cam).size()==1);

                pos_n=particles_ens.at(n).at(p)->imgPos().at(i_cam).at(it_img);
                pos_mean=particles_ens.back().at(p)->imgPos().at(i_cam).at(it_img);

                particles_ens.at(n).at(p)->img().at(i_cam).at(it_img).convertTo(imgnBigCanvas, CV_32FC1);
                particles_ens.back().at(p)->img().at(i_cam).at(it_img).convertTo(imgmean, CV_32FC1);

                imgmeanBigCanvas=cv::Mat::zeros(imgnBigCanvas.size(), CV_32FC1);

                if( std::abs(pos_n.y-pos_mean.y)<std::max(pos_mean.height, pos_n.height) && std::abs(pos_n.x-pos_mean.x)<std::max(pos_mean.width, pos_n.width) ){

                    utils::getPositionAndSizeOfOverlapedAreaOfTwoLocalPatches(pos_n, pos_mean, pos_overlap_n, pos_overlap_mean);

                    if(pos_overlap_n.height!=0 && pos_overlap_n.width!=0)
                        imgmean(pos_overlap_mean).copyTo( imgmeanBigCanvas(pos_overlap_n) );
                }

                imgFeature_icam_seq.create(imgnBigCanvas.size(), CV_32FC1);
                cv::subtract(imgnBigCanvas, imgmeanBigCanvas, imgFeature_icam_seq);
            }
            else
            {
                imgFeature_icam_seq=cv::Mat(1, 1, CV_32FC1, cv::Scalar(0));
            }

            imgFeature_icam.push_back(imgFeature_icam_seq);
            imgFeature_icam_seq.release();

            particles_ens.at(n).at(p)->push_backimgFeatureElement(imgFeature_icam);
            imgFeature_icam.clear();
        }
    }
}

void ENS::getLocalLinearFormOfCostFunction(int p, const std::vector<Camera_ptr>& cam){
                    
    this->getRightHandSide(p, cam, particles_ens_ana_);
    this->getHessian(p, cam, particles_ens_ana_);
}

void ENS::getRightHandSide(int p, const std::vector<Camera_ptr>& cam,
                            const std::vector<std::vector<Particle_ptr>>& particles_ens){

    MatrixXd rhs;
    rhs.setZero(n_ens_, 1);

    double rhsn;

    int it_img=0;

    for(int n=0; n<n_ens_; n++){
        rhsn=0;
        for(int i_cam=0; i_cam<cam.size(); i_cam++)
        {
            if( particles_ens.at(n).at(p)->on_image().at(i_cam)==1 &&
                particles_ens.back().at(p)->on_image().at(i_cam)==1 )
            {
                auto pos_n_img=particles_ens.at(n).at(p)->imgPos().at(i_cam).at(it_img);

                cv::Rect pos_n, pos_n_BigCanvas;

                utils::getPositionAndSizeOfOverlapedAreaOfOneLocalPatchAndIres(pos_n_img, eval_window_fine_search_, cam[i_cam]->n_width(), cam[i_cam]->n_height(), pos_n, pos_n_BigCanvas);

                ///////////////////////////////////////////////////////////////////////////////
                auto img_n = particles_ens.at(n).at(p)->img()[i_cam][0](pos_n).clone();
                auto img_m = cam.at(i_cam)->Irec()(pos_n_BigCanvas).clone();

                auto bytesL = new uint8[img_n.cols * img_n.rows];
                auto bytesR = new uint8[img_n.cols * img_n.rows];
                for (int i = 0; i < img_n.rows; i++) {
                    for (int j = 0; j < img_n.cols; j++) {
                        bytesL[i * img_n.cols + j] = img_n.at<uint8>(i, j);
                        bytesR[i * img_n.cols + j] = img_m.at<uint8>(i, j);
                    }
                }
                auto censusL = new uint32[img_n.cols * img_n.rows]();
                auto censusR = new uint32[img_n.cols * img_n.rows]();
                sgm_util::census_transform_5x5(bytesL, censusL, img_n.cols, img_n.rows);
                sgm_util::census_transform_5x5(bytesR, censusR, img_n.cols, img_n.rows);

                double sum_census_cost=0;
                for(int i=0; i<img_n.rows; i++){
                    for(int j=0; j<img_n.cols; j++){
                        const auto& census_val_l = censusL[i * img_n.cols + j];
                        const auto& census_val_r = censusR[i * img_n.cols + j];
                        auto cost = sgm_util::Hamming32(census_val_l, census_val_r);
                        sum_census_cost -= static_cast<double>(cost);
                    }
                }

                SAFE_DELETE(bytesL);
                SAFE_DELETE(bytesR);    
                SAFE_DELETE(censusL);
                SAFE_DELETE(censusR); 
                //////////////////////////////////////////////////////////////////////////////
                cv::Mat result;
                cv::matchTemplate(particles_ens.at(n).at(p)->imgFeature().at(i_cam).at(it_img)(pos_n),
                                    cam.at(i_cam)->Ires()(pos_n_BigCanvas),
                                    result, cv::TM_CCORR);//or TM_CCOEFF, do not use normalized cost
                rhsn+=static_cast<double>(result.at<float>(0,0)) + census_weight_*std::exp(sum_census_cost);
            }
        }
        rhs(n)=rhsn;
    }

    particles_ens.back().at(p)->setrhs(rhs);
}

void ENS::getHessian(int p, const std::vector<Camera_ptr>& cam,
                    const std::vector<std::vector<Particle_ptr>>& particles_ens){

    MatrixXd hessian;
    hessian.setZero(n_ens_, n_ens_);

    double res=0.;
    cv::Rect  pos_overlap_n, pos_overlap_m;
    cv::Mat imgFeature_nMulm, imgFeature_n, imgFeature_m;     

    int it_img=0;

    for(int n=0; n<n_ens_; n++)
    {
        for(int m=n; m<n_ens_; m++)
        {
            res=0;
            for(int i_cam=0; i_cam<cam.size(); i_cam++)
            {
                if( particles_ens.at(n).at(p)->on_image().at(i_cam)==1 &&
                    particles_ens.at(m).at(p)->on_image().at(i_cam)==1 &&
                    particles_ens.back().at(p)->on_image().at(i_cam)==1 )
                {
                    cv::Rect pos_n=particles_ens.at(n).at(p)->imgPos().at(i_cam).at(it_img);
                    cv::Rect pos_m=particles_ens.at(m).at(p)->imgPos().at(i_cam).at(it_img);

                    imgFeature_n=particles_ens.at(n).at(p)->imgFeature().at(i_cam).at(it_img).clone();
                    imgFeature_m=particles_ens.at(m).at(p)->imgFeature().at(i_cam).at(it_img).clone();

                    cv::Rect eval_window_n = cv::Rect(std::max(0, static_cast<int>(pos_n.width)/2-eval_window_fine_search_), std::max(0, static_cast<int>(pos_n.height)/2-eval_window_fine_search_),
                                                      std::min(2*eval_window_fine_search_+1, pos_n.width), std::min(2*eval_window_fine_search_+1, pos_n.height));

                    cv::Mat imgFeature_mBigCanvas=cv::Mat::zeros(imgFeature_n(eval_window_n).size(), CV_32FC1);

                    cv::Rect pos_n_win = cv::Rect(pos_n.x+std::max(0, static_cast<int>(pos_n.width)/2-eval_window_fine_search_), pos_n.y+std::max(0, static_cast<int>(pos_n.height)/2-eval_window_fine_search_),
                                                  std::min(2*eval_window_fine_search_+1, pos_n.width), std::min(2*eval_window_fine_search_+1, pos_n.height));

                    if( std::abs(pos_n_win.y-pos_m.y)<std::max(pos_m.height, pos_n_win.height) && std::abs(pos_n_win.x-pos_m.x)<std::max(pos_m.width, pos_n_win.width) ){
                        
                        utils::getPositionAndSizeOfOverlapedAreaOfTwoLocalPatches(pos_n_win, pos_m, pos_overlap_n, pos_overlap_m);

                        if(pos_overlap_n.area()!=0){
                            ///////////////////////////////////////////////////////////////////////
                            auto img_n = particles_ens.at(n).at(p)->img()[i_cam][0](eval_window_n).clone();
                            auto img_m = particles_ens.at(m).at(p)->img()[i_cam][0](pos_overlap_m).clone();

                            auto bytesL = new uint8[img_n.cols * img_n.rows];
                            auto bytesR = new uint8[img_n.cols * img_n.rows];
                            for (int i = 0; i < img_n.rows; i++) {
                                for (int j = 0; j < img_n.cols; j++) {
                                    bytesL[i * img_n.cols + j] = img_n.at<uint8>(i, j);
                                    bytesR[i * img_n.cols + j] = img_m.at<uint8>(i, j);
                                }
                            }
                            auto censusL = new uint32[img_n.cols * img_n.rows]();
                            auto censusR = new uint32[img_n.cols * img_n.rows]();
                            sgm_util::census_transform_5x5(bytesL, censusL, img_n.cols, img_n.rows);
                            sgm_util::census_transform_5x5(bytesR, censusR, img_n.cols, img_n.rows);

                            double sum_census_cost=0;
                            for(int i=0; i<img_n.rows; i++){
                                for(int j=0; j<img_n.cols; j++){
                                    const auto& census_val_l = censusL[i * img_n.cols + j];
                                    const auto& census_val_r = censusR[i * img_n.cols + j];
                                    auto cost = sgm_util::Hamming32(census_val_l, census_val_r);
                                    sum_census_cost -= static_cast<double>(cost);
                                }
                            }

                            SAFE_DELETE(bytesL);
                            SAFE_DELETE(bytesR);    
                            SAFE_DELETE(censusL);
                            SAFE_DELETE(censusR);
                            ////////////////////////////////////////////////////////////////////////
                            imgFeature_m(pos_overlap_m).copyTo( imgFeature_mBigCanvas(pos_overlap_n) );
                            
                            cv::Mat result;
                            cv::matchTemplate(imgFeature_n(eval_window_n),
                                                imgFeature_mBigCanvas,
                                                result, cv::TM_CCORR);//or TM_CCOEFF, do not use normalized cost
                            res+=static_cast<double>(result.at<float>(0,0)) + census_weight_*std::exp(sum_census_cost);
                        }
                        else
                            res=0;
                    }
                    else
                        res=0;
                }
                else
                    res=0;

            }
            hessian(n,m)=res;
        }
    }

    hessian.triangularView<Eigen::StrictlyLower>()=hessian.triangularView<Eigen::StrictlyUpper>().transpose();

    double regul_const = (particles_ens.back()[p]->regul_const()==0 ? regul_const_ : particles_ens.back()[p]->regul_const());

    hessian=hessian+regul_const*MatrixXd::Identity(n_ens_, n_ens_);
    particles_ens.back().at(p)->sethessian(hessian);
}

double ENS::updateLocalEnsembleOptimalCoordinates(int p, const VectorXd& weight, const MatrixXd& hessian, int it_seq_cnt){
   
    int it_seq = (ipr_full_flag_ ? 0 : it_seq_cnt - particles_ens_ana_.back().at(p)->it_seq_deb());

    auto dXcopt=cv::Point3d(0.0, 0.0, 0.0);
    for(int n=0; n<n_ens_; n++){
        dXcopt+=( particles_ens_ana_.at(n).at(p)->Xcoord().at(it_seq) - particles_ens_ana_.back().at(p)->Xcoord().at(it_seq) )*weight(n);
    }
    auto Xcopt = particles_ens_ana_.back().at(p)->Xcoord().at(it_seq) + dXcopt;

    utils::maintainPositionInRange(Xcopt, Xc_top_, Xc_bottom_);

    Eigen::JacobiSVD<MatrixXd> svd(hessian, Eigen::ComputeThinU | Eigen::ComputeThinV);

    VectorXd temp=svd.singularValues().array().pow(-0.5);

    MatrixXd TM=svd.matrixU()*(temp.asDiagonal())*svd.matrixV();

    std::vector<cv::Point3d> Xcopt_ens(n_ens_, cv::Point3d(0,0,0));

    cv::Point3d dXcopt_n;

    for(int n=0; n<n_ens_; n++){
        dXcopt_n=cv::Point3d(0.0, 0.0, 0.0);
        for(int m=0; m<n_ens_; m++){
            dXcopt_n+=TM(n,m)*( particles_ens_ana_.at(m).at(p)->Xcoord().at(it_seq) - particles_ens_ana_.back().at(p)->Xcoord().at(it_seq) );
        }

        Xcopt_ens[n] = Xcopt + dXcopt_n;
        utils::maintainPositionInRange(Xcopt_ens[n], Xc_top_, Xc_bottom_);
    }

    for(int n=0; n<n_ens_; n++){
        particles_ens_ana_.at(n).at(p)->setXcoordElement(Xcopt_ens[n], it_seq_cnt);
    }
    
    particles_ens_ana_.back().at(p)->setXcoordElement(Xcopt, it_seq_cnt);

    double sigma=1e-10;;
 
    return (std::abs(dXcopt.x/particles_ens_ana_.back().at(p)->Xcoord().at(it_seq).x)<sigma &&
            std::abs(dXcopt.y/particles_ens_ana_.back().at(p)->Xcoord().at(it_seq).y)<sigma &&
            std::abs(dXcopt.z/particles_ens_ana_.back().at(p)->Xcoord().at(it_seq).z)<sigma) ? 1 : 0;
}

void ENS::updateLocalEnsembleOptimalIntensities(int p, const VectorXd& weight, const MatrixXd& hessian, int it_seq_cnt){

    int it_seq = (ipr_full_flag_ ? 0 : it_seq_cnt - particles_ens_ana_.back().at(p)->it_seq_deb());

    double dE=0.0;
    for(int n=0; n<n_ens_; n++){
        dE+=( particles_ens_ana_.at(n).at(p)->Ev().at(it_seq) - particles_ens_ana_.back().at(p)->Ev().at(it_seq) )*weight(n);
    }
    auto Eopt = std::max(particles_ens_ana_.back().at(p)->Ev().at(it_seq) + dE, intensity_min_threshold_);

    if(Eopt<0){
        particles_ens_ana_.back().at(p)->printSummary();
        for(int n=0; n<n_ens_; n++)
            cout<<"particles_ens_ana_.at(n).at(p)->Ev().at(it_seq) "<<particles_ens_ana_.at(n).at(p)->Ev().at(it_seq)<<endl;
        cout<<"particles_ens_ana_.back().at(p)->Ev().at(it_seq) "<<particles_ens_ana_.back().at(p)->Ev().at(it_seq)<<endl
            <<"dE "<<dE<<endl;
        cout<<"weight "<<weight<<endl;
        std::abort();
    }

    Eigen::JacobiSVD<MatrixXd> svd(hessian, Eigen::ComputeThinU | Eigen::ComputeThinV);
    VectorXd temp=svd.singularValues().array().pow(-0.5);
    MatrixXd TM=svd.matrixU()*(temp.asDiagonal())*svd.matrixV();

    std::vector<double> Eopt_ens(n_ens_, 0.0);

    double dE_n;
    for(int n=0; n<n_ens_; n++){
        dE_n=0.0;
        for(int m=0; m<n_ens_; m++){
            dE_n+=TM(n,m)*( particles_ens_ana_.at(m).at(p)->Ev().at(it_seq) - particles_ens_ana_.back().at(p)->Ev().at(it_seq) );
        }
        Eopt_ens[n] = std::max(Eopt + dE_n, intensity_min_threshold_);
    }

    for(int n=0; n<n_ens_; n++){
        particles_ens_ana_.at(n).at(p)->setEvElement(Eopt_ens[n], it_seq_cnt);
    }
    
    particles_ens_ana_.back().at(p)->setEvElement(Eopt, it_seq_cnt);
}

void ENS::addEnsembleForTriangulatedParticles(int it_seq_cnt){

    cv::Point3d Xc_np;

    for(int n=0; n<n_ens_; n++){
        for(int p=0; p<particles_ens_ana_.back().size(); p++){
            if(particles_ens_ana_.back()[p]->is_deleted() && !particles_ens_ana_.back()[p]->is_ontrack() &&
               !particles_ens_ana_[n][p]->is_deleted()){
                particles_ens_ana_[n][p]->deleteData(true);
                particles_ens_ana_[n][p]->setit_seq_fin(it_seq_cnt-1);
            }

            if(particles_ens_ana_.back()[p]->is_ontrack()){
                // If particles_ens_ana_.back()[p] is new and has not generate an ensemble
                // Or if particles_ens_ana_.back()[p] is side track (one of the possible appended track)
                if(particles_ens_ana_[n].size()<p+1 && particles_ens_ana_.back()[p]->length()<=3){
                    particles_ens_ana_[n].push_back( std::make_unique<Particle>( *(particles_ens_ana_.back()[p]) ) );
                    for(int i=0; i<particles_ens_ana_.back()[p]->length(); i++){
                        Xc_np=particles_ens_ana_[n][p]->Xcoord()[i];// + cv::Point3d( dXc_px_.x*sigmaXcRatio_.x*Random::StandardNormalRandomNumber(0.0),
                                                                    //                dXc_px_.y*sigmaXcRatio_.y*Random::StandardNormalRandomNumber(0.0),
                                                                    //                dXc_px_.z*sigmaXcRatio_.z*Random::StandardNormalRandomNumber(0.0) );
                        utils::maintainPositionInRange(Xc_np, Xc_top_, Xc_bottom_);
                        particles_ens_ana_[n][p]->setXcoordElement(Xc_np, it_seq_cnt-particles_ens_ana_.back()[p]->length()+i+1);
                    }
                }

                // If triangulated particle has been append to particles_ens_ana_.back()[p]
                if(particles_ens_ana_[n][p]->Xcoord().size()!=particles_ens_ana_.back()[p]->Xcoord().size()){
                    Xc_np=particles_ens_ana_.back().at(p)->Xcoord().back() + cv::Point3d( dXc_px_.x*sigmaXcRatio_.x*Random::StandardNormalRandomNumber(0.0),
                                                                                          dXc_px_.y*sigmaXcRatio_.y*Random::StandardNormalRandomNumber(0.0),
                                                                                          dXc_px_.z*sigmaXcRatio_.z*Random::StandardNormalRandomNumber(0.0) );
                    utils::maintainPositionInRange(Xc_np, Xc_top_, Xc_bottom_);
                    particles_ens_ana_[n][p]->push_backXcoordElement(Xc_np, it_seq_cnt);
                }

                if(particles_ens_ana_[n][p]->Ev().size()!=particles_ens_ana_.back()[p]->Ev().size()){
                    particles_ens_ana_[n][p]->push_backEvElement(particles_ens_ana_.back().at(p)->Ev().back(), it_seq_cnt);
                }
            
                particles_ens_ana_[n][p]->setis_ontrack_flag_as(particles_ens_ana_.back()[p]->is_ontrack());
                particles_ens_ana_[n][p]->setis_tracked_flag_as(particles_ens_ana_.back()[p]->is_tracked());
                particles_ens_ana_[n][p]->setis_triangulated_flag_as(particles_ens_ana_.back()[p]->is_triangulated());
                particles_ens_ana_[n][p]->setis_deleted_flag_as(particles_ens_ana_.back()[p]->is_deleted());
                particles_ens_ana_[n][p]->setis_new_flag_as(particles_ens_ana_.back()[p]->is_new());
                particles_ens_ana_[n][p]->setis_lost_flag_as(particles_ens_ana_.back()[p]->is_lost());
            }
        }
        utils::showInfoOnDeletedParticles(false);
    }

    for(int n=0; n<n_ens_; n++){
        assert(particles_ens_ana_[n].size()==particles_ens_ana_.back().size());
        for(int p=0; p<particles_ens_ana_.back().size(); p++){
            if(particles_ens_ana_.back()[p]->is_ontrack()){
                assert(particles_ens_ana_[n][p]->Xcoord().size()==particles_ens_ana_.back()[p]->Xcoord().size());
                assert(particles_ens_ana_[n][p]->Ev().size()==particles_ens_ana_.back()[p]->Ev().size());
            }
        }
    }
}

void ENS::rescaleEnsembleIntensityUsingTarget(int it_seq_cnt, bool just_copy_flag){

    for(int n=0; n<n_ens_; n++){
        for(int p=0; p<particles_ens_ana_.back().size(); p++){
            if(particles_ens_ana_.back()[p]->is_ontrack() && this->checkIfParticleIsToBeOptimized(particles_ens_ana_.back()[p].get())){
                auto E_np = ( just_copy_flag ? particles_ens_ana_.back().at(p)->getCurrentE(it_seq_cnt)
                                             : particles_ens_ana_.back().at(p)->getCurrentE(it_seq_cnt)*(1.+sigmaE_*Random::StandardNormalRandomNumber(0.0)) );
                particles_ens_ana_[n][p]->setEvElement(std::max( intensity_min_threshold_, E_np ), it_seq_cnt);
            }
        }
    }
}

void ENS::resampleEnsemble(int it_seq_cnt){
   
    this->pertubeEnsemblePosition(it_seq_cnt, false, true);
    if(intensity_control_flag_) this->pertubeEnsembleIntensity(it_seq_cnt, intensity_control_flag_, false, true);
}

/// ENS/IPR version

void ENS::projectEnsembleToImageSpace(int it_seq_cnt, EnsembleProjection_Type ens_proj_type, const std::vector<Camera_ptr>& cam){

    switch(ens_proj_type){
        case ENS::EnsembleProjection_Type::Joint:
            cout<<"ensemble projections"<<endl<<"Ensemble member ";
            for(int i=0; i<particles_ens_ana_.size(); ++i){
                cout<<" "<<i;
                this->projectParticlesToImageSpace(it_seq_cnt, particles_ens_ana_[i], cam);
            }
            cout<<endl;
            break;
        case ENS::EnsembleProjection_Type::JointMean:
            cout<<"ensemble mean projections"<<endl;
            this->projectParticlesToImageSpace(it_seq_cnt, particles_ens_ana_.back(), cam);
            break;
    }
}

void ENS::checkIfEnsembleParticlesAppearOnSnapshot(int it_seq_cnt){

    for(int n=0; n<n_ens_; n++){
        for(int p=0; p<particles_ens_ana_.back().size(); p++){
            if(particles_ens_ana_.back()[p]->is_deleted() && !particles_ens_ana_.back()[p]->is_ontrack() && !particles_ens_ana_[n][p]->is_deleted()){
                particles_ens_ana_[n][p]->deleteData(true);
                particles_ens_ana_[n][p]->setit_seq_fin(it_seq_cnt-1);
            }
            else{
                particles_ens_ana_[n][p]->setis_ontrack_flag_as( particles_ens_ana_.back()[p]->is_ontrack() );
                particles_ens_ana_[n][p]->setis_to_be_tracked_flag_as( particles_ens_ana_.back()[p]->is_to_be_tracked() );
                particles_ens_ana_[n][p]->setis_tracked_flag_as( particles_ens_ana_.back()[p]->is_tracked() );
                particles_ens_ana_[n][p]->setis_triangulated_flag_as( particles_ens_ana_.back()[p]->is_triangulated() );
                particles_ens_ana_[n][p]->setis_deleted_flag_as( particles_ens_ana_.back()[p]->is_deleted() );
                particles_ens_ana_[n][p]->setis_side_flag_as( particles_ens_ana_.back()[p]->is_side() );
            }
        }
        utils::showInfoOnDeletedParticles(false);
    }
}

void ENS::maintainPositionInRange(int it_seq_cnt){
    for(auto& particles_n_ens : particles_ens_ana_){
        for(auto& particle_np : particles_n_ens){
            if(particle_np->is_ontrack() && this->checkIfParticleIsToBeOptimized(particle_np.get())){
                auto Xc_np=particle_np->Xcoord().back();
                utils::maintainPositionInRange(Xc_np, Xc_top_, Xc_bottom_);
                particle_np->setXcoordElement(Xc_np, it_seq_cnt);
            }
        }
    }
}

bool ENS::checkConvergence(int iter, int max_iter, double delta_res_threshold){

    bool exit_flag = OptMethod::checkConvergence(iter, max_iter, delta_res_threshold);

    if( all_of(continue_flag_.cbegin(), continue_flag_.cend(), [](const auto& p) { return std::abs(p)>1e-3; }) ){
        cout<<"The location estimation converged at "<<iter<<"th iteration because search radius is too small"<<endl;
        if(warm_start_flag_) lMax_=iter;
        else                 kMax_=iter;
        exit_flag=true;
    }

    return exit_flag;
}
