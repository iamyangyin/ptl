#ifndef DATASET
#define DATASET

#include "Camera.hpp"

class Calibration;

///  Dataset class
class Dataset
{
    public:
        enum class Source_Type      	{FromSynthetic, FromLaVision, FromLPTChallenge};
        enum class Init_Track_Type  	{Hacker, Triangulation};
        enum class Data_Format_Type 	{NPZ, TXT_ONEFILE, TXT};

	protected:
		double PPP_ = 0.;
        std::vector<Particle_ptr> particles_ref_;
        std::vector<Particle_ptr> particles_bkg_;

	public:
		Dataset(){};

		~Dataset(){};

		virtual void setIMGData(const std::string&, int, int, const std::vector<Camera_ptr>&, const std::string&){};

		virtual void readOTF(const std::string&, Camera*){};
		virtual void readPolynomialMF(const std::string&, Camera*){};
		virtual void readPinholeMF(const std::string&, Camera*){};

		virtual void readMarkPositionTable(const std::string&, int, 
											std::vector<std::vector<std::vector<cv::Point3d>>>&,
											std::vector<std::vector<std::vector<cv::Point2d>>>&, int nx_rig=0, int ny_rig=0){};

		virtual	void setRigIMGData(const std::string&, int, const std::string&){};

		void readParticleData(const std::string&, int, int, Data_Format_Type, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);

        void readBackgroundData(const std::string&, const std::vector<int>&, bool, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);
        void readSyntheticData(const std::string&, int, int, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);
        void saveSyntheticData(const std::string&, int, const std::vector<int>&);

		void readBackgroundDataTXT(const std::string&, const std::vector<int>&, bool, const std::optional<cv::Point3d>& Xc_top=std::nullopt, const std::optional<cv::Point3d>& Xc_bottom=std::nullopt);

		void writeIMGPngDataToIM7(const std::string&, int, int, const std::string&, const cv::Point3d&, const cv::Point3d&, double, double);

        const std::vector<Particle_ptr>&           particles_bkg(void) const { return particles_bkg_; }
        const std::vector<Particle_ptr>&           particles_ref(void) const { return particles_ref_; }

        std::vector<Particle_ptr>&                 get_particles_bkg(void) { return particles_bkg_; }
        std::vector<Particle_ptr>&                 get_particles_ref(void) { return particles_ref_; }

    protected:
    	void readParticleDataNPZ(const std::string&, int, int);
     	void readParticleDataTXT(const std::string&, int, int);
      	void readParticleDataTXT_ONEFILE(const std::string&, int, int, const std::optional<cv::Point3d>&, const std::optional<cv::Point3d>&);

		int writeIM7XFile(const std::string&, const std::string&, int, const cv::Point3d&, const cv::Point3d&, double, double);
		int Write_GeneralExample(const std::vector<std::string>&, const std::string&, const cv::Point3d&, const cv::Point3d&, double, double);

		int readIM7XFile(const std::string&, const std::string&);
		int Read_GeneralExample(const char*, const char*, bool, bool, bool);
		bool BufferType_GetVector(const BufferType&, int, int, int, float&, float&, float&);
};

inline int Dataset::readIM7XFile(const std::string& img_file, const std::string& res_file){

	char* p_sFileName=new char[img_file.length()+1];
	char* p_sResultName=new char[res_file.length()+1];

	std::strcpy(p_sFileName, img_file.c_str());
	std::strcpy(p_sResultName, res_file.c_str());

	bool p_bHeader=true, p_bAttributes=false, p_bDisplayMask=false;

	int err = Read_GeneralExample( p_sFileName, p_sResultName, p_bHeader, p_bAttributes, p_bDisplayMask);

	return err;
}

// Return TRUE if the vector exists
inline bool Dataset::BufferType_GetVector(const BufferType& theBuffer, int theX, int theY, int theFrame, float& vx, float& vy, float& vz)
{
	vx=vy=vz = 0;
	if (theBuffer.image_sub_type<=0)
	{	// image
		return false;
	}

	int frameOffset = theFrame * theBuffer.nx * theBuffer.ny * theBuffer.nz;
	int width = theBuffer.nx;
	int height = theBuffer.ny;
	int componentOffset = width * height;
	int mode;

	if (theX<0 || theX>=width || theY<0 || theY>=height || theFrame<0 || theFrame>=theBuffer.nf)
	{	// invalid position
		return false;
	}

	switch (theBuffer.image_sub_type)
	{
		case 0:	// it's an image and no vector buffer
			return false;
		case 1:	//	PIV vector field with header and 4*2D field
			mode = (int) theBuffer.floatArray[ theX + theY*width + frameOffset ];
			if (mode<=0)
			{	// disabled vector
				return true;
			}
			if (mode>4)
			{	// interpolated or filled vector
				mode = 4;
			}
			mode--;
			vx = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*2+1) ];
			vy = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*2+2) ];
			break;
		case 2:	//	simple 2D vector field
			vx = theBuffer.floatArray[ theX + theY*width + frameOffset ];
			vy = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset ];
			break;
		case 3:	// same as 1 + peak ratio
			mode = (int) theBuffer.floatArray[ theX + theY*width + frameOffset ];
			if (mode<=0)
			{	// disabled vector
				return true;
			}
			if (mode>4)
			{	// interpolated or filled vector
				mode = 4;
			}
			mode--;
			vx = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*2+1) ];
			vy = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*2+2) ];
			break;
		case 4:	// simple 3D vector field
			vx = theBuffer.floatArray[ theX + theY*width + frameOffset ];
			vy = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset ];
			vz = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*2 ];
			break;
		case 5:	//	PIV vector field with header and 4*3D field + peak ratio
			mode = (int) theBuffer.floatArray[ theX + theY*width + frameOffset ];
			if (mode<=0)
			{	// disabled vector
				return true;
			}
			if (mode>4)
			{	// interpolated or filled vector
				mode = 4;
			}
			mode--;
			vx = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*3+1) ];
			vy = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*3+2) ];
			vz = theBuffer.floatArray[ theX + theY*width + frameOffset + componentOffset*(mode*3+3) ];
			break;
	}
	return true;
}

inline int Dataset::Read_GeneralExample(const char* p_sFileName, const char* p_sResultName, bool p_bHeader, bool p_bAttributes, bool p_bDisplayMask)
{
	BufferType myBuffer;
	fprintf(stderr,"Reading '%s'...\n",p_sFileName);

	AttributeList *pAttrList = NULL;
	int err =ReadIM7( p_sFileName, &myBuffer, (p_bAttributes ? &pAttrList : NULL) );
	if (err!=0)
	{
		switch (err)
		{
			case IMREAD_ERR_FILEOPEN:
				fprintf(stderr,"Input file '%s' not found!\n",p_sFileName);
				break;
			case IMREAD_ERR_HEADER:
				fprintf(stderr,"Error in header\n");
				break;
			case IMREAD_ERR_FORMAT:
				fprintf(stderr,"Packing format not supported\n");
				break;
			case IMREAD_ERR_DATA:
				fprintf(stderr,"Error while reading data\n");
				break;
			case IMREAD_ERR_MEMORY:
				fprintf(stderr,"Error out of memory\n");
				break;
			case IMREAD_ERR_ATTRIBUTE_INVALID_TYPE:
				fprintf(stderr,"Error invalid attribute type\n");
				break;
			case IMREAD_ERR_ATTRIBUTE_NO_DATA:
				fprintf(stderr,"Error missing attribute data\n");
				break;
			default:
				fprintf(stderr,"Unknown error %i\n",err);
		}
		return 1;
	}
	
	fprintf(stderr,"File-Info: '%s'\n",p_sFileName);
	fprintf(stderr," Size: %i x %i x %i x %i\n", myBuffer.nx, myBuffer.ny, myBuffer.nz, myBuffer.nf );
	fprintf(stderr," X-Scale: %f * x + %f %s (%s)\n", myBuffer.scaleX.factor, myBuffer.scaleX.offset, myBuffer.scaleX.unit, myBuffer.scaleX.description );
	fprintf(stderr," Y-Scale: %f * x + %f %s (%s)\n", myBuffer.scaleY.factor, myBuffer.scaleY.offset, myBuffer.scaleY.unit, myBuffer.scaleY.description );
	fprintf(stderr," I-Scale: %f * x + %f %s (%s)\n", myBuffer.scaleI.factor, myBuffer.scaleI.offset, myBuffer.scaleI.unit, myBuffer.scaleI.description );
	switch (myBuffer.image_sub_type)
	{
		case BUFFER_FORMAT_IMAGE:
			fprintf(stderr," Type: Image\n");
			break;
		case BUFFER_FORMAT_MEMPACKWORD:
			fprintf(stderr," Type: byte Image\n");
			break;
		case BUFFER_FORMAT_FLOAT:
			fprintf(stderr," Type: float Image\n");
			break;
		case BUFFER_FORMAT_WORD:
			fprintf(stderr," Type: word Image\n");
			break;
		case BUFFER_FORMAT_DOUBLE:
			fprintf(stderr," Type: double Image\n");
			break;
		default:
		{
			const char *TypeName[] = { "Image", "2D-PIV-Vector (header, 4x(Vx,Vy))", "2D-Vector (Vx,Vy)", 
												"2D-PIV+p.ratio (header, 4x(Vx,Vy), peakratio)", 
												"3D-Vector (Vx,Vy,Vz)", "3D-Vector+p.ratio (header, 4x(Vx,Vy), peakratio)" };
			fprintf(stderr," Type: %s\n", TypeName[myBuffer.image_sub_type]);
			fprintf(stderr," Grid size: %i\n", myBuffer.vectorGrid);
		}
	}

	fprintf(stderr,"Mask: %s\n", myBuffer.bMaskArray!=NULL ? "exists" : "no mask" );
	if (p_bDisplayMask && myBuffer.bMaskArray)
	{	// print mask values
		int x,y,iFrame;
		for (iFrame=0; iFrame<myBuffer.nf; iFrame++)
		{
			for (y=0; y<myBuffer.ny; y++)
			{
				for (x=0; x<myBuffer.nx; x++)
					fprintf(stderr,"%i ", myBuffer.bMaskArray[iFrame*myBuffer.nx*myBuffer.ny+y*myBuffer.nx+x] ? 1 : 0 );
				fprintf(stderr,"\n");
			}
		}
	}
	
	fprintf(stderr,"\n");
	if (p_sResultName)
	{	// optional write data into txt file
		int x,y,iFrame;
		for (iFrame=0; iFrame<myBuffer.nf; iFrame++)
		{
			std::string p_sResultNameFrame(p_sResultName);
			p_sResultNameFrame=p_sResultNameFrame+"cam"+std::to_string(iFrame)+".txt";
			fprintf(stderr,"Writing '%s'...\n",p_sResultNameFrame.c_str());
			FILE* fout = fopen(p_sResultNameFrame.c_str(),"w");
			if (fout==NULL)
			{
				fprintf(stderr,"Can't create output file!\n");
				DestroyBuffer(&myBuffer);
				if (pAttrList)
					DestroyAttributeList( &pAttrList );
				return 1;
			}

			if (p_bHeader)
			{	// print header line with size of image: <points per line> <width> <height> <frames>
				fprintf(fout,"%i %i %i %i\n", myBuffer.nx, myBuffer.nx, myBuffer.ny, myBuffer.nf );
			}
			if (p_bAttributes)
			{	// print all attributes: <name> = <value>
				AttributeList *pAttr = pAttrList;
				while (pAttr)
				{
					char *ptr;
					while ((ptr = strchr(pAttr->value,'\n')) != 0)
						*ptr = '\t';
					fprintf(fout,"%s = %s\n", pAttr->name, pAttr->value );
					pAttr = pAttr->next;
				}
			}

			for (y=0; y<myBuffer.ny; y++)
			{
				if (myBuffer.image_sub_type > 0)
				{	// vector
					for (x=0; x<myBuffer.nx; x++)
					{
						float vx, vy, vz;
						BufferType_GetVector( myBuffer, x, y, iFrame, vx, vy, vz );
						fprintf(fout,"%i\t%i\t%i\t%g\t%g\t%g\n", x, y, iFrame, vx, vy, vz );
					}
				}
				else
				{	// image
					if (myBuffer.isFloat)
					{	// float data type
						for (x=0; x<myBuffer.nx; x++)
							fprintf(fout,"%10.3f ", myBuffer.floatArray[iFrame*myBuffer.nx*myBuffer.ny+y*myBuffer.nx+x] );
					}
					else
					{	// word data type
						for (x=0; x<myBuffer.nx; x++)
							fprintf(fout,"%i\t", myBuffer.wordArray[iFrame*myBuffer.nx*myBuffer.ny+y*myBuffer.nx+x] );
					}
					fprintf(fout,"\n");
				}
			}
			fclose(fout);
		}
	}

	// now we have to delete the buffer data
	DestroyBuffer(&myBuffer);
	if (pAttrList)
		DestroyAttributeList( &pAttrList );

	return 0;
}

inline void Dataset::writeIMGPngDataToIM7(const std::string& img_file_dir, int it_deb, int it_tot, const std::string& res_file_dir, 
										const cv::Point3d& Factor, const cv::Point3d& Offset, double dtObs, double pixel_size){

	for(int it=it_deb; it<=it_tot; it++){
		this->writeIM7XFile(img_file_dir, res_file_dir, it, Factor, Offset, dtObs, pixel_size);
	}
}

inline int Dataset::writeIM7XFile(const std::string& img_file_dir, const std::string& res_file_dir, int it_seq,
								    const cv::Point3d& Factor, const cv::Point3d& Offset, double dtObs, double pixel_size){

	// bool p_bHeader=true, p_bAttributes=false, p_bDisplayMask=false;
	int n_cam=4;
	std::vector<std::string> img_file_vec;

	for(int i=0; i<n_cam; i++){
		auto img_path = str(boost::format("%1$s/cam%2$d_preproc/") % img_file_dir % i );

        if(!boost::filesystem::exists(img_path)){
        	cout<<img_path<<" does not exist!!!"<<endl;
        	std::abort();
        }

		auto img_file = str(boost::format("%1$scam_c%2$d_it%3$03d.png") % img_path % i % it_seq );
		img_file_vec.push_back(img_file);
	}

	auto res_file=str(boost::format("%1$s/B%2$05d.im7") % res_file_dir % it_seq );

	auto acqTime_seq=(it_seq-1)*dtObs;

	int err = this->Write_GeneralExample( img_file_vec, res_file, Factor, Offset, acqTime_seq, pixel_size );

	return err;
}

inline int Dataset::Write_GeneralExample(const std::vector<std::string>& img_file_vec, const std::string& res_file, 
										const cv::Point3d& Factor, const cv::Point3d& Offset, double acqTime_seq, double pixel_size)
{
	BufferType myBuffer;
	fprintf(stderr,"Read png image buffer\n");

	std::vector<cv::Mat>  img;

	for(auto iter=img_file_vec.begin(); iter<img_file_vec.end(); ++iter){
		cout<<"image path "<<*iter<<endl;
		std::string img_name=*iter;
		auto img_cam=cv::imread(img_name, cv::IMREAD_ANYDEPTH);
		cout<<"img_cam.size() "<<img_cam.size()<<endl;
		img.push_back(img_cam);
	}

	char* p_sIm7ResultName=new char[res_file.length()+1];
	std::strcpy(p_sIm7ResultName, res_file.c_str());

	std::string acqTime_seq_str=str(boost::format("%1$10.6f ms") % acqTime_seq );
	char* p_acqTime_seq_str=new char[acqTime_seq_str.length()+1];
	std::strcpy(p_acqTime_seq_str, acqTime_seq_str.c_str());

	int sizeX = img[0].size().width;
	int sizeY = img[0].size().height;
	int sizeF = img.size();

	cout<<str(boost::format("nx %1%, ny %2%, ncam %3%") % sizeX % sizeY % sizeF )<<endl;

	CreateBuffer( &myBuffer, sizeX, sizeY, 1, sizeF, false, 1, BUFFER_FORMAT_WORD );

	int x,y,fr;
	for (fr=0; fr<sizeF; fr++)
	{
		for (y=0; y<sizeY; y++)
		{
			for (x=0; x<sizeX; x++)
			{
				myBuffer.wordArray[fr*sizeY*sizeX+y*sizeX+x] = img[fr].at<ushort>(y,x);
			}
		}
	}

	// create some test attributes
	AttributeList *myAttributes = NULL;

	std::string frame_scale_Y_str = str(boost::format("%1$10.6f\n%2$10.6f\nmm\n") % Factor.y % Offset.y );
	std::string frame_scale_X_str = str(boost::format("%1$10.6f\n%2$10.6f\nmm\n") % Factor.x % Offset.x );

	char* p_frame_scale_Y_str=new char[frame_scale_Y_str.length()+1];
	std::strcpy(p_frame_scale_Y_str, frame_scale_Y_str.c_str());
	char* p_frame_scale_X_str=new char[frame_scale_X_str.length()+1];
	std::strcpy(p_frame_scale_X_str, frame_scale_X_str.c_str());

	std::string sizeX_str = str(boost::format("%1$4d") % sizeX );
	std::string sizeY_str = str(boost::format("%1$4d") % sizeY );

	char* p_sizeX_str=new char[sizeX_str.length()+1];
	std::strcpy(p_sizeX_str, sizeX_str.c_str());
	char* p_sizeY_str=new char[sizeY_str.length()+1];
	std::strcpy(p_sizeY_str, sizeY_str.c_str());

	std::string pixel_size_str_1 = str(boost::format("%1$.4f mm") % pixel_size );
	std::string pixel_size_str_2 = str(boost::format("%1$.4f\tmm") % pixel_size );

	char* p_pixel_size_str_1=new char[pixel_size_str_1.length()+1];
	std::strcpy(p_pixel_size_str_1, pixel_size_str_1.c_str());
	char* p_pixel_size_str_2=new char[pixel_size_str_2.length()+1];
	std::strcpy(p_pixel_size_str_2, pixel_size_str_2.c_str());

	SetAttribute( &myAttributes, "FrameScaleI3", "1\n0\ncounts\n" );
	SetAttribute( &myAttributes, "FrameScaleZ3", "1\n0\npixel\n" );
	SetAttribute( &myAttributes, "FrameScaleY3", p_frame_scale_Y_str );
	SetAttribute( &myAttributes, "FrameScaleX3", p_frame_scale_X_str );
	SetAttribute( &myAttributes, "AcqTime3", "0 ms" );
	SetAttribute( &myAttributes, "CCDExposureTime3", "1.9 ms" );
	SetAttribute( &myAttributes, "CameraName3", "4: Phantom" );
	SetAttribute( &myAttributes, "CameraMaxNx3", p_sizeX_str );
	SetAttribute( &myAttributes, "CameraMaxNy3", p_sizeY_str );
	SetAttribute( &myAttributes, "CameraMaxIntensity3", "65535" );
	SetAttribute( &myAttributes, "CamPixelSize3", p_pixel_size_str_1 );
	SetAttribute( &myAttributes, "FrameProcessing3", "2" );
	SetAttribute( &myAttributes, "FrameRotation3", "2" );
	SetAttribute( &myAttributes, "CamPixelSize3", p_pixel_size_str_2 );

	SetAttribute( &myAttributes, "FrameScaleI2", "1\n0\ncounts\n" );
	SetAttribute( &myAttributes, "FrameScaleZ2", "1\n0\npixel\n" );
	SetAttribute( &myAttributes, "FrameScaleY2", p_frame_scale_Y_str );
	SetAttribute( &myAttributes, "FrameScaleX2", p_frame_scale_X_str );
	SetAttribute( &myAttributes, "AcqTime2", "0 ms" );
	SetAttribute( &myAttributes, "CCDExposureTime2", "1.9 ms" );
	SetAttribute( &myAttributes, "CameraName2", "3: Phantom" );
	SetAttribute( &myAttributes, "CameraMaxNx2", p_sizeX_str );
	SetAttribute( &myAttributes, "CameraMaxNy2", p_sizeY_str );
	SetAttribute( &myAttributes, "CameraMaxIntensity2", "65535" );
	SetAttribute( &myAttributes, "CamPixelSize2", p_pixel_size_str_1 );
	SetAttribute( &myAttributes, "FrameProcessing2", "2" );
	SetAttribute( &myAttributes, "FrameRotation2", "2" );
	SetAttribute( &myAttributes, "CamPixelSize2", p_pixel_size_str_2 );

	SetAttribute( &myAttributes, "FrameScaleI1", "1\n0\ncounts\n" );
	SetAttribute( &myAttributes, "FrameScaleZ1", "1\n0\npixel\n" );
	SetAttribute( &myAttributes, "FrameScaleY1", p_frame_scale_Y_str );
	SetAttribute( &myAttributes, "FrameScaleX1", p_frame_scale_X_str );
	SetAttribute( &myAttributes, "AcqTime1", "0 ms" );
	SetAttribute( &myAttributes, "CCDExposureTime1", "1.9 ms" );
	SetAttribute( &myAttributes, "CameraName1", "2: Phantom" );
	SetAttribute( &myAttributes, "CameraMaxNx1", p_sizeX_str );
	SetAttribute( &myAttributes, "CameraMaxNy1", p_sizeY_str );
	SetAttribute( &myAttributes, "CameraMaxIntensity1", "65535" );
	SetAttribute( &myAttributes, "CamPixelSize1", p_pixel_size_str_1 );
	SetAttribute( &myAttributes, "FrameProcessing1", "2" );
	SetAttribute( &myAttributes, "FrameRotation1", "2" );
	SetAttribute( &myAttributes, "CamPixelSize1", p_pixel_size_str_2 );

	SetAttribute( &myAttributes, "FrameScaleI0", "1\n0\ncounts\n" );
	SetAttribute( &myAttributes, "FrameScaleZ0", "1\n0\npixel\n" );
	SetAttribute( &myAttributes, "FrameScaleY0", p_frame_scale_Y_str );
	SetAttribute( &myAttributes, "FrameScaleX0", p_frame_scale_X_str );
	SetAttribute( &myAttributes, "AcqTime0", "0 ms" );
	SetAttribute( &myAttributes, "CCDExposureTime0", "1.9 ms" );
	SetAttribute( &myAttributes, "CameraName0", "1: Phantom" );
	SetAttribute( &myAttributes, "CameraMaxNx0", p_sizeX_str );
	SetAttribute( &myAttributes, "CameraMaxNy0", p_sizeY_str );
	SetAttribute( &myAttributes, "CameraMaxIntensity0", "65535" );
	SetAttribute( &myAttributes, "CamPixelSize0", p_pixel_size_str_1 );
	SetAttribute( &myAttributes, "FrameProcessing0", "2" );
	SetAttribute( &myAttributes, "FrameRotation0", "2" );
	SetAttribute( &myAttributes, "CamPixelSize0", p_pixel_size_str_2 );

	// SetAttribute( &myAttributes, "FrameScaleI3", "1\n0\ncounts\n" );
	// SetAttribute( &myAttributes, "FrameScaleZ3", "1\n0\npixel\n" );
	// SetAttribute( &myAttributes, "FrameScaleY3", "-0.0625000\n36.25\nmm\n" );
	// SetAttribute( &myAttributes, "FrameScaleX3", "0.0625000\n-3.75\nmm\n" );
	// SetAttribute( &myAttributes, "AcqTime3", "0 ms" );
	// SetAttribute( &myAttributes, "CCDExposureTime3", "1.9 ms" );
	// SetAttribute( &myAttributes, "CameraName3", "4: Phantom" );
	// SetAttribute( &myAttributes, "CameraMaxNx3", "1280" );
	// SetAttribute( &myAttributes, "CameraMaxNy3", "800" );
	// SetAttribute( &myAttributes, "CameraMaxIntensity3", "65535" );
	// SetAttribute( &myAttributes, "CamPixelSize3", "0.02 mm" );
	// SetAttribute( &myAttributes, "FrameProcessing3", "2" );
	// SetAttribute( &myAttributes, "FrameRotation3", "2" );
	// SetAttribute( &myAttributes, "CamPixelSize3", "0.02\tmm" );

	// SetAttribute( &myAttributes, "FrameScaleI2", "1\n0\ncounts\n" );
	// SetAttribute( &myAttributes, "FrameScaleZ2", "1\n0\npixel\n" );
	// SetAttribute( &myAttributes, "FrameScaleY2", "-0.0625000\n36.25\nmm\n" );
	// SetAttribute( &myAttributes, "FrameScaleX2", "0.0625000\n-3.75\nmm\n" );
	// SetAttribute( &myAttributes, "AcqTime2", "0 ms" );
	// SetAttribute( &myAttributes, "CCDExposureTime2", "1.9 ms" );
	// SetAttribute( &myAttributes, "CameraName2", "3: Phantom" );
	// SetAttribute( &myAttributes, "CameraMaxNx2", "1280" );
	// SetAttribute( &myAttributes, "CameraMaxNy2", "800" );
	// SetAttribute( &myAttributes, "CameraMaxIntensity2", "65535" );
	// SetAttribute( &myAttributes, "CamPixelSize2", "0.02 mm" );
	// SetAttribute( &myAttributes, "FrameProcessing2", "2" );
	// SetAttribute( &myAttributes, "FrameRotation2", "2" );
	// SetAttribute( &myAttributes, "CamPixelSize2", "0.02\tmm" );

	// SetAttribute( &myAttributes, "FrameScaleI1", "1\n0\ncounts\n" );
	// SetAttribute( &myAttributes, "FrameScaleZ1", "1\n0\npixel\n" );
	// SetAttribute( &myAttributes, "FrameScaleY1", "-0.0625000\n36.25\nmm\n" );
	// SetAttribute( &myAttributes, "FrameScaleX1", "0.0625000\n-3.75\nmm\n" );
	// SetAttribute( &myAttributes, "AcqTime1", "0 ms" );
	// SetAttribute( &myAttributes, "CCDExposureTime1", "1.9 ms" );
	// SetAttribute( &myAttributes, "CameraName1", "2: Phantom" );
	// SetAttribute( &myAttributes, "CameraMaxNx1", "1280" );
	// SetAttribute( &myAttributes, "CameraMaxNy1", "800" );
	// SetAttribute( &myAttributes, "CameraMaxIntensity1", "65535" );
	// SetAttribute( &myAttributes, "CamPixelSize1", "0.02 mm" );
	// SetAttribute( &myAttributes, "FrameProcessing1", "2" );
	// SetAttribute( &myAttributes, "FrameRotation1", "2" );
	// SetAttribute( &myAttributes, "CamPixelSize1", "0.02\tmm" );

	// SetAttribute( &myAttributes, "FrameScaleI0", "1\n0\ncounts\n" );
	// SetAttribute( &myAttributes, "FrameScaleZ0", "1\n0\npixel\n" );
	// SetAttribute( &myAttributes, "FrameScaleY0", "-0.0625000\n36.25\nmm\n" );
	// SetAttribute( &myAttributes, "FrameScaleX0", "0.0625000\n-3.75\nmm\n" );
	// SetAttribute( &myAttributes, "AcqTime0", "0 ms" );
	// SetAttribute( &myAttributes, "CCDExposureTime0", "1.9 ms" );
	// SetAttribute( &myAttributes, "CameraName0", "1: Phantom" );
	// SetAttribute( &myAttributes, "CameraMaxNx0", "1280" );
	// SetAttribute( &myAttributes, "CameraMaxNy0", "800" );
	// SetAttribute( &myAttributes, "CameraMaxIntensity0", "65535" );
	// SetAttribute( &myAttributes, "CamPixelSize0", "0.02 mm" );
	// SetAttribute( &myAttributes, "FrameProcessing0", "2" );
	// SetAttribute( &myAttributes, "FrameRotation0", "2" );
	// SetAttribute( &myAttributes, "CamPixelSize0", "0.02\tmm" );

	SetAttribute( &myAttributes, "AcqTimeSeries3", p_acqTime_seq_str );
	SetAttribute( &myAttributes, "AcqTimeSeries2", p_acqTime_seq_str );
	SetAttribute( &myAttributes, "AcqTimeSeries1", p_acqTime_seq_str );
	SetAttribute( &myAttributes, "AcqTimeSeries0", p_acqTime_seq_str );

	SetAttribute( &myAttributes, "_DATE", "01.02.2019");
	SetAttribute( &myAttributes, "_TIME", "00:00:00.000");
	SetAttribute( &myAttributes, "_DaVisVersion", "10.0.5");
	SetAttribute( &myAttributes, "_FrameScale", "1\n0\nframe\n");
	SetAttribute( &myAttributes, "DevDataName0", "Camera 1: Exposure time");
	SetAttribute( &myAttributes, "DevDataAlias0", "Camera 1: Exposure time");
	SetAttribute( &myAttributes, "DevDataScaleI0", "1\n0\nms");
	SetAttribute( &myAttributes, "DevDataClass0", "1");
	SetAttribute( &myAttributes, "DevDataName1", "Camera 2: Exposure time");
	SetAttribute( &myAttributes, "DevDataAlias1", "Camera 2: Exposure time");
	SetAttribute( &myAttributes, "DevDataScaleI1", "1\n0\nms");
	SetAttribute( &myAttributes, "DevDataClass1", "1");
	SetAttribute( &myAttributes, "DevDataName2", "Camera 3: Exposure time");
	SetAttribute( &myAttributes, "DevDataAlias2", "Camera 3: Exposure time");
	SetAttribute( &myAttributes, "DevDataScaleI2", "1\n0\nms");
	SetAttribute( &myAttributes, "DevDataClass2", "1");
	SetAttribute( &myAttributes, "DevDataName3", "Camera 4: Exposure time");
	SetAttribute( &myAttributes, "DevDataAlias3", "Camera 4: Exposure time");
	SetAttribute( &myAttributes, "DevDataScaleI3", "1\n0\nms");
	SetAttribute( &myAttributes, "DevDataClass3", "1");
	SetAttribute( &myAttributes, "DevDataName4", "Recording:RecordingRate");
	SetAttribute( &myAttributes, "DevDataAlias4", "Recording: Recording rate");
	SetAttribute( &myAttributes, "DevDataScaleI4", "1\n0\nHz");
	SetAttribute( &myAttributes, "DevDataClass4", "1");
	SetAttribute( &myAttributes, "AttributeDisplayDeviceData", "SetupDialogBufferAttrDevData(-1)");
	SetAttribute( &myAttributes, "DevDataSources", "5");
	SetAttribute( &myAttributes, "CustomImageTag_Count", "0");
	SetAttribute( &myAttributes, "AttributeDisplayCustomImageTags", "CustomImageTagsDDP_CreateDialog()");

	// create scales
	SetBufferScale( &myBuffer.scaleX, Factor.x, Offset.x, "", "mm" );
	SetBufferScale( &myBuffer.scaleY, Factor.y, Offset.y, "", "mm" );
	SetBufferScale( &myBuffer.scaleI, 1, 0, "", "counts" );

	fprintf(stderr,"Writing '%s'...\n",p_sIm7ResultName);
	int err = WriteIM7( p_sIm7ResultName, false, &myBuffer, myAttributes );

	// now we have to delete the buffer data
	DestroyBuffer(&myBuffer);
	if (myAttributes)
		DestroyAttributeList( &myAttributes );

	return err;
}

inline void Dataset::readParticleData(const std::string& path, int it_deb, int it_tot, Data_Format_Type data_format_type, 
							const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom){
	switch(data_format_type){
		case Data_Format_Type::NPZ: 		this->readParticleDataNPZ(path, it_deb, it_tot); break;
		case Data_Format_Type::TXT_ONEFILE:	this->readParticleDataTXT_ONEFILE(path, it_deb, it_tot, Xc_top, Xc_bottom); break;
		case Data_Format_Type::TXT: 		this->readParticleDataTXT(path, it_deb, it_tot); break;
	}
}

inline void Dataset::readParticleDataNPZ(const std::string& imgdir, int it_deb, int it_tot){

    std::map<int, Particle_ptr> particles_ref_map;

    std::string refdir(imgdir+"/ref");

    for(int it_seq=it_deb; it_seq<=it_tot; it_seq++){
        auto refPhaseFile=str(boost::format("%1%/ref_it%2$03d.npz") % refdir % it_seq );
        utils::readFrameDataNPZ(it_seq, it_deb, refPhaseFile, particles_ref_map);
    }

    if(!particles_ref_.empty()) particles_ref_.clear();
    for(const auto& pr_m: particles_ref_map){
        particles_ref_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
    }

    cout<<"Read fin"<<endl
        <<"TOTAL particle number for ALL TIME STEPS "<<particles_ref_map.size()<<endl;
}

inline void Dataset::readParticleDataTXT(const std::string& data_path, int it_deb, int it_tot){

    if(!particles_ref_.empty()) particles_ref_.clear();

	std::string X, Y, Z, track_id, line;

	int p_id;
    std::vector<int> p_id_vec;
    cv::Point3d Xc;

	cout<<"Read debut ..."<<endl;

	auto PPP_str = PPP_*1000;

	for(int it=it_deb; it<=it_tot; it++){

		int it_seq=it-1;

		auto data_file=str(boost::format("%1$sDA_ppp_0_%2$03d/DA_ppp_0_%2$03d_PartFile_%3$04d.dat") % data_path % PPP_str % it_seq);
		cout<<"data_file "<<data_file<<endl;

		std::ifstream dataFile(data_file);

		it_seq++;
		cout<<"Read snapshot "<<it_seq<<endl;

		if(dataFile.is_open()){
			while(std::getline(dataFile, line)){

				std::string::size_type nc=line.find("Title");
				if(nc != std::string::npos)
					line.clear();

				nc=line.find("Variables");
				if(nc != std::string::npos)
					line.clear();

				nc=line.find("Zone");
				if(nc != std::string::npos)
					line.clear();

				if(!line.empty()){
					std::istringstream is(line);
					if(is >> X >> Y >> Z >> track_id){						
						p_id=std::stoi(track_id);
						Xc=cv::Point3d( std::stod(X), std::stod(Y), std::stod(Z) );

						if(it_seq==it_deb){
							// if(Xc.x >= Lim_bottom.x && Xc.x <= Lim_top.x && Xc.y >= Lim_bottom.y && Xc.y <= Lim_top.y && Xc.z >= Lim_bottom.z && Xc.z <= Lim_top.z){
							p_id_vec.push_back(p_id);
							particles_ref_.push_back( std::make_unique<Particle>( p_id, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{cv::Point3d(0,0,0)}, std::vector<double>{1.} ) );						
							// }
						}
						else{
							auto it=std::find(p_id_vec.begin(), p_id_vec.end(), p_id);
							if(it != p_id_vec.end()){
								auto p_idx=std::distance(p_id_vec.begin(), it);
								particles_ref_[p_idx]->setit_seq_fin(it_seq);
								particles_ref_[p_idx]->push_backXcoordElement(Xc, it_seq);
								particles_ref_[p_idx]->push_backVElement(cv::Point3d(0,0,0), it_seq);
								particles_ref_[p_idx]->push_backEvElement(1., it_seq);
							}
							else{
								// if(Xc.x >= Lim_bottom.x && Xc.x <= Lim_top.x && Xc.y >= Lim_bottom.y && Xc.y <= Lim_top.y && Xc.z >= Lim_bottom.z && Xc.z <= Lim_top.z){
								p_id_vec.push_back(p_id);
								particles_ref_.push_back( std::make_unique<Particle>( p_id, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{cv::Point3d(0,0,0)}, std::vector<double>{1.} ) );						
								// }
							}
						}
					}
				}
			}

		    dataFile.close();
		}
		else{
			std::cerr<<"Error opening data file "<<data_file<<endl;
			std::abort();
		}
	}

	cout<<"Read fin"<<endl
		<<"TOTAL particle number for ALL TIME STEPS "<<p_id_vec.size()<<endl;

	assert(p_id_vec.size()==particles_ref_.size());
}

inline void Dataset::readParticleDataTXT_ONEFILE(const std::string& data_file, int it_deb, int it_tot, 
	                             				const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom){

    if(!particles_ref_.empty()) particles_ref_.clear();

    std::map<int, Particle_ptr> particles_ref_map;

	std::ifstream dataFile(data_file);

	std::string X, Y, Z, I, u, v, w, Vabs, track_id, ax, ay, az, aabs, line;

	int it_seq=it_deb-1;

	int p_part_index;
	double E;
    cv::Point3d Xc, Vp;

	cout<<"Read debut ..."<<endl;

	double Lim_E=0.;

	if(dataFile.is_open()){
		while(std::getline(dataFile, line)){

			std::string::size_type nc=line.find("TITLE");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("VARIABLES");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("STRANDID");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("I=");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("DATAPACKING");
			if(nc != std::string::npos)
				line.clear();

			nc=line.find("ZONE");
			if(nc != std::string::npos){
				it_seq++;
				cout<<"Read snapshot "<<it_seq<<endl;
			}
			
			if(it_seq<=it_tot && !line.empty()){

				std::istringstream is(line);
				if(is >> X >> Y >> Z >> I >> u >> v >> w >> Vabs >> track_id >> ax >> ay >> az >> aabs){

					p_part_index=std::stoi(track_id);
					Xc=cv::Point3d( std::stod(X), std::stod(Y), std::stod(Z) );
					Vp=cv::Point3d( std::stod(u), std::stod(v), std::stod(w) );
					E=std::stod(I);

					if(it_seq==it_deb){
						if(Xc.x >= Xc_bottom.value().x && Xc.x <= Xc_top.value().x && Xc.y >= Xc_bottom.value().y && Xc.y <= Xc_top.value().y && Xc.z >= Xc_bottom.value().z && Xc.z <= Xc_top.value().z && E>=Lim_E){
							particles_ref_map.insert({p_part_index, std::make_unique<Particle>( p_part_index, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{Vp}, std::vector<double>{E} )});
						}
					}
					else{
						auto it=particles_ref_map.find(p_part_index);
						if(it != particles_ref_map.end()){
							it->second->setit_seq_fin(it_seq);
							it->second->push_backXcoordElement(Xc, it_seq);
							it->second->push_backVElement(Vp, it_seq);
							it->second->push_backEvElement(E, it_seq);
						}
						else{
							if(Xc.x >= Xc_bottom.value().x && Xc.x <= Xc_top.value().x && Xc.y >= Xc_bottom.value().y && Xc.y <= Xc_top.value().y && Xc.z >= Xc_bottom.value().z && Xc.z <= Xc_top.value().z && E>=Lim_E){
								particles_ref_map.insert({p_part_index, std::make_unique<Particle>( p_part_index, it_seq, it_seq, std::vector<cv::Point3d>{Xc}, std::vector<cv::Point3d>{Vp}, std::vector<double>{E} )});
							}
						}

					}
				}	
			}
		}
	}
	else{
		std::cerr<<"Error opening data file "<<data_file<<endl;
		std::abort();
	}

	for(const auto& pr_m: particles_ref_map){
		particles_ref_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
	}

	cout<<"Read fin"<<endl
		<<"TOTAL particle number for ALL TIME STEPS "<<particles_ref_map.size()<<endl;
}


inline void Dataset::readSyntheticData(const std::string& imgdir, int it_deb, int it_tot, 
                                     const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom){

    std::map<int, Particle_ptr> particles_ref_map;

    std::string syndir(imgdir+"/syn");

    for(int it_seq=it_deb; it_seq<=it_tot; it_seq++){
        auto synPhaseFile=str(boost::format("%1%/syn_it%2$03d.npz") % syndir % it_seq );
        utils::readFrameDataNPZ(it_seq, it_deb, synPhaseFile, particles_ref_map);
    }

    if(!particles_ref_.empty()) particles_ref_.clear();
    for(const auto& pr_m: particles_ref_map){
        particles_ref_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
    }

    cout<<"total particle: particles_ref_.size() "<<particles_ref_.size()<<endl;

    if(Xc_top && Xc_bottom){
        for(int p=0; p<particles_ref_.size(); p++){
            auto it_doi_out = std::find_if(particles_ref_[p]->Xcoord().begin(), particles_ref_[p]->Xcoord().end(), 
                                            [=](const auto& Xc){ return checkParticleIsOutOfRange(Xc, Xc_top.value(), Xc_bottom.value()); });

            if(it_doi_out!=particles_ref_[p]->Xcoord().end()){
                particles_ref_[p]->deleteData(true);
                particles_ref_[p]->markAsGhost();
            }
        }
    }
}

inline void Dataset::readBackgroundData(const std::string& imgdir, const std::vector<int>& it_bkg, bool reconstructed_flag,
                                     const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom)
{
    std::map<int, Particle_ptr> particles_bkg_map;
    std::string bkgdir(imgdir);
    bkgdir+=(reconstructed_flag ? "/bkg_recon" : "/bkg");

    for(int it_seq=it_bkg.at(0); it_seq<=it_bkg.back(); it_seq++){
        auto bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.npz") % bkgdir % it_seq );
        utils::readFrameDataNPZ(it_seq, it_bkg[0], bkgPhaseFile, particles_bkg_map);
    }

    if(!particles_bkg_.empty()) particles_bkg_.clear();
    for(const auto& pr_m: particles_bkg_map){
        particles_bkg_.push_back( std::make_unique<Particle>( *(pr_m.second) ) );
    }

    cout<<"bkg particles: particles_bkg_.size() "<<particles_bkg_.size()<<endl;

    if(Xc_top && Xc_bottom){
        for(int p=0; p<particles_bkg_.size(); p++){
            auto it_doi_out = std::find_if(particles_bkg_[p]->Xcoord().begin(), particles_bkg_[p]->Xcoord().end(), 
                                            [=](const auto& Xc){ return checkParticleIsOutOfRange(Xc, Xc_top.value(), Xc_bottom.value()); });

            if(it_doi_out!=particles_bkg_[p]->Xcoord().end()){
                particles_bkg_[p]->deleteData(true);
                particles_bkg_[p]->markAsGhost();
            }
        }
    }
}

inline void Dataset::saveSyntheticData(const std::string& imgdir, int it_tot, const std::vector<int>& it_bkg){

    std::string syndir(imgdir+"/syn");
    if(!boost::filesystem::exists(syndir)){
        boost::filesystem::create_directories(syndir);
    }

    std::string bkgdir(imgdir+"/bkg");
    if(!boost::filesystem::exists(bkgdir)){
        boost::filesystem::create_directories(bkgdir);
    }

    for(int it=it_bkg[0]; it<=it_tot; it++){
        auto synPhaseFile=str(boost::format("%1%/syn_it%2$03d.npz") % syndir % it );
        utils::saveFrameDataNPZ(it, synPhaseFile, particles_ref_);

        if(it<=it_bkg.back()){
            auto bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.npz") % bkgdir % it );
            utils::saveFrameDataNPZ(it, bkgPhaseFile, particles_ref_);
        }
    }
}

inline void Dataset::readBackgroundDataTXT(const std::string& imgdir, const std::vector<int>& it_bkg, bool reconstructed_flag,
                                		const std::optional<cv::Point3d>& Xc_top, const std::optional<cv::Point3d>& Xc_bottom)
{
    std::string line;
    std::vector<std::string> par_index_v, it0_v, itf_v, X_v, Y_v, Z_v, E_v;
    std::string par_index, it0, itf, X, Y, Z, E; 

    std::string bkgPhaseFile;
    std::string bkgdir(imgdir);
    bkgdir+=(reconstructed_flag ? "/bkg_recon" : "/bkg");

    int n_particles_bkg=0;

    for(int i=it_bkg.at(0); i<it_bkg.at(0)+it_bkg.size(); i++){
        bkgPhaseFile=str(boost::format("%1%/bkg_it%2$03d.txt") % bkgdir % i );
        std::ifstream bkgFile(bkgPhaseFile);

        if(bkgFile.is_open()){
            while(std::getline(bkgFile, line)){
                std::string::size_type nc=line.find("#");
                if(nc != std::string::npos)
                    line.erase(nc);

                std::istringstream is(line);
                if(is >> par_index >> it0 >> itf >> X >> Y >> Z >> E){
                    par_index_v.push_back(par_index);
                    it0_v.push_back(it0);
                    itf_v.push_back(itf);
                    X_v.push_back(X);
                    Y_v.push_back(Y);
                    Z_v.push_back(Z);
                    E_v.push_back(E);               
                }           
            }
        }
        else{
            std::cerr<<"Error opening background data file "<<bkgPhaseFile<<endl;
            std::abort();
        }

        if(i==it_bkg[0])
            n_particles_bkg=par_index_v.size();
    }

    for(int p=0; p<n_particles_bkg; p++){
        auto p_part_index=std::stoi(par_index_v[p]);
        auto p_it0=std::stoi(it0_v[p]);
        auto p_itf=std::stoi(itf_v[p]);

        std::vector<cv::Point3d> Xcoord, Xcoord_sub;
        std::vector<double>      Ev, Ev_sub;

        for(int it=0; it<it_bkg.size(); it++){
            auto Xc=cv::Point3d( std::stod(X_v[it*n_particles_bkg+p]), std::stod(Y_v[it*n_particles_bkg+p]), std::stod(Z_v[it*n_particles_bkg+p]) );
            Xcoord.push_back(Xc);
            Ev.push_back(std::stod(E_v[it*n_particles_bkg+p]));
        }

        if(p_it0 <= it_bkg.back()){
            auto iter_begin = p_it0-it_bkg.at(0);
            auto iter_end   = std::max(0, it_bkg.back()-p_itf);

            Xcoord_sub.assign(Xcoord.begin()+iter_begin, Xcoord.end()-iter_end);
            Ev_sub.assign(Ev.begin()+iter_begin, Ev.end()-iter_end);
            particles_bkg_.push_back( std::make_unique<Particle>( p_part_index, p_it0, p_itf, Xcoord_sub, Ev_sub ) );
        }
    }

    cout<<"n_particles_bkg "<<n_particles_bkg<<endl;
    cout<<"bkg particles: particles_bkg_.size() "<<particles_bkg_.size()<<endl;

    if(Xc_top && Xc_bottom){
        for(int p=0; p<particles_bkg_.size(); p++){
            auto it_doi_out = std::find_if(particles_bkg_[p]->Xcoord().begin(), particles_bkg_[p]->Xcoord().end(), 
                                            [=](const auto& Xc){ return checkParticleIsOutOfRange(Xc, Xc_top.value(), Xc_bottom.value()); });

            if(it_doi_out!=particles_bkg_[p]->Xcoord().end()){
                particles_bkg_[p]->deleteData(true);
                particles_bkg_[p]->markAsGhost();
            }
        }
    }

}
#endif