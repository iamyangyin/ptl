#ifndef ENSMETHOD
#define ENSMETHOD

#include "OptMethod.hpp"

///ENS class
class ENS: public OptMethod
{
	public:
		enum class EnsembleProjection_Type 	{Joint, JointMean};

	protected:
        int 				n_ens_;
        double 				regul_const_;
        int  				outer_max_iter_;
        bool 				local_analysis_flag_;
		bool 				resample_flag_;
		cv::Point3d 		sigmaXcRatio_;
		double 				sigmaE_;
		bool 				joint_intensity_flag_;

		double 				census_weight_=0;

		std::vector<std::vector<Particle_ptr>>  particles_ens_ana_;
		
		std::vector<double>    continue_flag_;

	public:
		ENS();

        ~ENS();

		ENS(Method_Type, ControlVector_Type, bool, 
	        int, int, const cv::Point3d&, const cv::Point3d&,
	        double, double, 
	        const cv::Point3d&, const std::string&, int,
	        bool, int, double, double,
	        int, double, int, bool, 
	        bool, const cv::Point3d&, double, bool);

		ENS(Method_Type, ControlVector_Type, bool, 
	        int, int, const cv::Point3d&, const cv::Point3d&,
	        const cv::Point3d&, const std::string&, int,
	        int, double, int, bool, 
	        bool, const cv::Point3d&, double, bool);

		void initialization(std::vector<Particle_ptr>&, int) final;

		void predict(DynModel*, int, int) override;

		void prepare(int, const std::vector<Camera_ptr>&, int lapiv_it=0) final;

		void core(int, int, const std::vector<Camera_ptr>&, bool) final;

		void postIPRResidual(int, int, const std::vector<Camera_ptr>&) final;

		void postIPRFull(int);

		bool checkConvergence(int, int, double delta_res_threshold=0.) final;

		void update(Particle*, int, int, const std::vector<Camera_ptr>&, double&) final;

	protected:
		void initializeEnsembleCore(std::vector<Particle_ptr>&, bool, bool);
									
		void resampleEnsemble(int);

		double updateLocalEnsembleOptimalCoordinates(int, const VectorXd&, const MatrixXd&, int);

		void updateLocalEnsembleOptimalIntensities(int, const VectorXd&, const MatrixXd&, int);

		void selectWarmStart(int, const std::vector<Camera_ptr>&);
		
		void projectEnsembleToImageSpace(int, EnsembleProjection_Type, const std::vector<Camera_ptr>&);

		void getRawFeature(int, const std::vector<Camera_ptr>&);

		void computeCenteredImageLocalAnomalyMatrix(int, const std::vector<Camera_ptr>&, const std::vector<std::vector<Particle_ptr>>&);

		void addEnsembleForTriangulatedParticles(int);

		void rescaleEnsembleIntensityUsingTarget(int, bool just_copy_flag=false);

		void getLocalLinearFormOfCostFunction(int, const std::vector<Camera_ptr>&);

		void getRightHandSide(int, const std::vector<Camera_ptr>&, const std::vector<std::vector<Particle_ptr>>&);

		void getHessian(int, const std::vector<Camera_ptr>&, const std::vector<std::vector<Particle_ptr>>&);

		void checkIfEnsembleParticlesAppearOnSnapshot(int); 
		
		virtual void maintainPositionInRange(int);
		
		void removeGhost(int, int, const std::vector<Camera_ptr>&, bool use_lapiv=false);

		void pertubeEnsemblePosition(int, bool prediction_flag=false, bool resample_flag=false);
		void pertubeEnsembleIntensity(int, bool intensity_control_flag, bool prediction_flag=false, bool resample_flag=false);

	public:
		void clearParticleData(void) final {
			std::vector<std::vector<Particle_ptr>>().swap(particles_ens_ana_);
		}
		
		const std::vector<Particle_ptr>& particles_field(void) const final { return particles_ens_ana_.back(); }
        std::vector<Particle_ptr>&  get_particles_field(void) final { return particles_ens_ana_.back(); }

		const int& opt_max_iter(void) const final { return outer_max_iter_; }
};
#endif