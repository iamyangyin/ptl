#ifndef LAPIVMETHOD
#define LAPIVMETHOD

#include "ENS.hpp"

class LAPIV: public ENS
{
	public:
		enum class VelocityOutput_Type 		    {None, Overlap};

	protected:
		std::vector<std::vector<cv::Point3d>> 	velocity_fields_ens_;
		cv::Point3d 							sigmaV_;
		double 									sigmaV_ratio_=0.1;
		cv::Point3d 							sigmaV_grid_;
		cv::Point3d 							sigmaV_grid_cnt_;
		double									sigmaV_relaxfactor_=0.9;
		double 									corr_length_;
		cv::Point3i 							nXc_;
		cv::Point3d 							dXc_;
		cv::Point3d 							dXc_minimal_;

	public:
		LAPIV();

        ~LAPIV();

		LAPIV(Method_Type, ControlVector_Type, bool,
	        int, int, const cv::Point3d&, const cv::Point3d&,
	        double, double, 
	        const cv::Point3d&, const std::string&, int,
	        int, double, int, bool, 
	        bool, const cv::Point3d&, double, bool,
	        const cv::Point3d&, double);

	 	void initialization(std::vector<Particle_ptr>&, int, const std::vector<cv::Point3d>&, const cv::Point3i&, const cv::Point3d&, const cv::Point3d&);

		void predict(DynModel*, int);

		void run(int, const std::vector<Camera_ptr>&, DynModel*, bool ghost_removal_flag=false);

		bool checkIfParticleIsToBeOptimized(Particle* pp) final{
			return (pp->Xcoord().size()>=2);
		}

		void postProcessing(std::vector<Particle_ptr>&, int, const std::vector<Camera_ptr>&, DynModel*);
		
	protected:
		void initializeEnsembleVelocity(const std::vector<cv::Point3d>&);
		void resampleEnsembleVelocity(void);
		
		std::vector<cv::Point3d>
		pertubeEnsembleVelocity(const std::vector<cv::Point3d>&);

		std::pair<std::vector<std::vector<Particle_ptr>>, std::vector<cv::Point3d>> 
		initializeEnsemblePerGrid(const std::vector<std::vector<int>>&, int);

		void predictEnsemblePerGrid(DynModel*, const std::vector<std::vector<Particle_ptr>>&, 
                                    const std::vector<cv::Point3d>&, int);

		void projectEnsemblePerGridToImageSpace(const std::vector<std::vector<Particle_ptr>>&,
												int, const std::vector<Camera_ptr>&);

   		void getRawFeaturePerGrid(const std::vector<std::vector<Particle_ptr>>&,
                                    const std::vector<Camera_ptr>&);

		void getLocalLinearFormOfCostFunctionPerGrid(const std::vector<std::vector<Particle_ptr>>&,
                                                   	const std::vector<Camera_ptr>&);

		void updatePerGrid(const std::vector<std::vector<Particle_ptr>>&,
						    const std::vector<cv::Point3d>&, int, const std::vector<Camera_ptr>&);

		bool updateLocalEnsembleOptimalVelocityFieldsForCurrentPatch(const VectorXd&, const MatrixXd&, const std::vector<cv::Point3d>&, int);

		void updateEnsembleOptimalCoordinates(DynModel*, int);

	public:
		const cv::Point3d&  sigmaV(void) const { return sigmaV_; }
		const std::vector<std::vector<cv::Point3d>>& velocity_fields_ens(void) const { return velocity_fields_ens_; }
		void  setVelocityNoiseLevel(int, int);
        void  setregul_const(double, int, int);
        
	private:
		using ENS::initialization;
		using ENS::predict;
};

std::vector<cv::Point3d> generateBackgroundVelocityFields(const cv::Point3d&, const cv::Point3d&, const cv::Point3i&, const cv::Point3d&, double);
void trimParticles(std::vector<Particle_ptr>&, int, bool rm_length_one_flag=false);

#endif