#include "linsolve.h"
#include "coherent_detection.h"
#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <cmath>
#include <cstring>

#include <cstddef>
#include <cstdlib>
#include "rt_nonfinite.h"
#include "xzgeqp31.h"
#include <cmath>


void linsolve(const double A[32], const double B[8], double C[4])
{
  int jpvt[4];
  double b_A[32];
  double tau[4];
  int rankA;
  double tol;
  double b_B[8];
  int i;
  jpvt[0] = 1;
  jpvt[1] = 2;
  jpvt[2] = 3;
  jpvt[3] = 4;
  std::memcpy(&b_A[0], &A[0], 32U * sizeof(double));
  tau[0] = 0.0;
  tau[1] = 0.0;
  tau[2] = 0.0;
  tau[3] = 0.0;
  qrpf1(b_A, tau, jpvt);
  rankA = 0;
  tol = 1.7763568394002505E-14 * std::abs(b_A[0]);
  while ((rankA < 4) && (!(std::abs(b_A[rankA + (rankA << 3)]) <= tol))) {
    rankA++;
  }

  std::memcpy(&b_B[0], &B[0], 8U * sizeof(double));
  C[0] = 0.0;
  if (tau[0] != 0.0) {
    tol = b_B[0];
    for (i = 2; i < 9; i++) {
      tol += b_A[i - 1] * b_B[i - 1];
    }

    tol *= tau[0];
    if (tol != 0.0) {
      b_B[0] -= tol;
      for (i = 2; i < 9; i++) {
        b_B[i - 1] -= b_A[i - 1] * tol;
      }
    }
  }

  C[1] = 0.0;
  if (tau[1] != 0.0) {
    tol = b_B[1];
    for (i = 3; i < 9; i++) {
      tol += b_A[i + 7] * b_B[i - 1];
    }

    tol *= tau[1];
    if (tol != 0.0) {
      b_B[1] -= tol;
      for (i = 3; i < 9; i++) {
        b_B[i - 1] -= b_A[i + 7] * tol;
      }
    }
  }

  C[2] = 0.0;
  if (tau[2] != 0.0) {
    tol = b_B[2];
    for (i = 4; i < 9; i++) {
      tol += b_A[i + 15] * b_B[i - 1];
    }

    tol *= tau[2];
    if (tol != 0.0) {
      b_B[2] -= tol;
      for (i = 4; i < 9; i++) {
        b_B[i - 1] -= b_A[i + 15] * tol;
      }
    }
  }

  C[3] = 0.0;
  if (tau[3] != 0.0) {
    tol = b_B[3];
    for (i = 5; i < 9; i++) {
      tol += b_A[i + 23] * b_B[i - 1];
    }

    tol *= tau[3];
    if (tol != 0.0) {
      b_B[3] -= tol;
      for (i = 5; i < 9; i++) {
        b_B[i - 1] -= b_A[i + 23] * tol;
      }
    }
  }

  for (i = 0; i < rankA; i++) {
    C[jpvt[i] - 1] = b_B[i];
  }

  for (int j = rankA; j >= 1; j--) {
    int C_tmp;
    int b_C_tmp;
    C_tmp = jpvt[j - 1] - 1;
    b_C_tmp = (j - 1) << 3;
    C[C_tmp] /= b_A[(j + b_C_tmp) - 1];
    for (i = 0; i <= j - 2; i++) {
      C[jpvt[i] - 1] -= C[C_tmp] * b_A[i + b_C_tmp];
    }
  }
}

