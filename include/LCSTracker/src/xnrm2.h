#ifndef XNRM2_H
#define XNRM2_H

#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

extern double xnrm2(int n, const coder::array<double, 2U> &x, int ix0);

#endif

