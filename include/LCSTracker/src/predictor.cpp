#include "predictor.h"
#include "coherent_detection.h"
#include "linsolve.h"
#include "rt_nonfinite.h"
#include <cmath>
#include <cstring>
#include <math.h>

#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <cmath>

#include <iostream>
#include <fstream>      

#include <Eigen/Dense>

using namespace std;

static double rt_powd_snf(double u0, double u1);

static double rt_powd_snf(double u0, double u1)
{
  double y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    double d;
    double d1;
    d = std::abs(u0);
    d1 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (d == 1.0) {
        y = 1.0;
      } else if (d > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d1 == 0.0) {
      y = 1.0;
    } else if (d1 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void predictor(double c, double Pn[3], const coder::array<double, 3U> &P, const
               double Local_Average[21], const double Local_Average_Accel[21],
               double tn)
{
  double A2X[8];
  double A2Y[8];
  double A2Z[8];
  double A2[32];
  int i;
  double AX2n[4];
  double AY2n[4];
  double AZ2n[4];
  double A2_tmp;
  double b_A2_tmp;

  //
  // %%___________3rd order Poly + local Eulerian Velocity + local acceleration + FTLE + Filter___________%%% 
  //              clear A2X A2Y A2Z A2 AX2n AY2n AZ2n;
  std::memset(&A2X[0], 0, 8U * sizeof(double));
  std::memset(&A2Y[0], 0, 8U * sizeof(double));
  std::memset(&A2Z[0], 0, 8U * sizeof(double));
  std::memset(&A2[0], 0, 32U * sizeof(double));
    
  int tn1 = (int) tn;
  i = static_cast<int>((tn + 2.0) + -2.0);
    
  for (int k = tn-4; k < tn+2; k++)
  {
      for (int n = 1; n < 5; n++)
      {
    if (k +1 <= tn)
    {
        
      int A2X_tmp_tmp;
      A2[k] = 1.0;

      A2X_tmp_tmp = static_cast<int>(c) - 1;
      A2X[k-tn1+4] = P[c + P.size(0) * k + 0*tn*P.size(0)];
      A2Y[k-tn1+4] = P[c + P.size(0) * k + 1*tn*P.size(0)];
      A2Z[k-tn1+4] = P[c + P.size(0) * k + 2*tn*P.size(0)];
//        cout<<c + P.size(0) * k+0*tn*P.size(0)<<endl;
//        cout<<P[c + P.size(0) * k+0*tn*P.size(0)]<<endl;

      A2[k + 8] = rt_powd_snf(static_cast<double>(k) + 3.0, 1.0);
        
//      A2X[k] = P[A2X_tmp_tmp + P.size(0) * (k + 2)];
//      A2Y[k] = P[(A2X_tmp_tmp + P.size(0) * (k + 2)) + P.size(0) * 6];
//      A2Z[k] = P[(A2X_tmp_tmp + P.size(0) * (k + 2)) + P.size(0) * 6 * 2];
        
      A2[k + 16] = rt_powd_snf(static_cast<double>(k) + 3.0, 2.0);
        
//      A2X[k] = P[A2X_tmp_tmp + P.size(0) * (k + 2)];
//      A2Y[k] = P[(A2X_tmp_tmp + P.size(0) * (k + 2)) + P.size(0) * 6];
//      A2Z[k] = P[(A2X_tmp_tmp + P.size(0) * (k + 2)) + P.size(0) * 6 * 2];
        
      A2[k + 24] = rt_powd_snf(static_cast<double>(k) + 3.0, 3.0);
        
//      A2X[k] = P[A2X_tmp_tmp + P.size(0) * (k + 2)];
//      A2Y[k] = P[(A2X_tmp_tmp + P.size(0) * (k + 2)) + P.size(0) * 6];
//      A2Z[k] = P[(A2X_tmp_tmp + P.size(0) * (k + 2)) + P.size(0) * 6 * 2];
    }
    else
    {

              int A2X_tmp_tmp_tmp;
              int A2X_tmp_tmp;
              int b_A2X_tmp_tmp_tmp;
              int b_A2X_tmp_tmp;
              A2X_tmp_tmp_tmp = 7;
              A2X_tmp_tmp = 7 - 1;
              A2X[A2X_tmp_tmp] = Local_Average[9];
              A2Y[A2X_tmp_tmp] = Local_Average[10];
              A2Z[A2X_tmp_tmp] = Local_Average[11];
              b_A2X_tmp_tmp_tmp = static_cast<int>(tn + 2.0);
              b_A2X_tmp_tmp = b_A2X_tmp_tmp_tmp - 1;
//              A2X[b_A2X_tmp_tmp] = Local_Average_Accel[9];
//              A2Y[b_A2X_tmp_tmp] = Local_Average_Accel[10];
//              A2Z[b_A2X_tmp_tmp] = Local_Average_Accel[11];
              A2_tmp = 0.0 * rt_powd_snf(tn, -1.0);
            
        //      A2[A2X_tmp_tmp] = A2_tmp;
                A2[4+8*(n-1)]=0;
                A2[5+8*(n-1)]=0;

                A2[6+8*(n-1)] = (n-1) * pow(tn, n-2);
                A2[7+8*(n-1)] = (n-2)*(n-1)*pow(tn, n-3);

        //      A2[b_A2X_tmp_tmp] = 0.0 * rt_powd_snf(tn, -2.0);
            
//                cout<<"n= "<<n<<endl;
            
              A2X[A2X_tmp_tmp] = Local_Average[9];
              A2Y[A2X_tmp_tmp] = Local_Average[10];
              A2Z[A2X_tmp_tmp] = Local_Average[11];
//              A2X[b_A2X_tmp_tmp] = Local_Average_Accel[9];
//              A2Y[b_A2X_tmp_tmp] = Local_Average_Accel[10];
//              A2Z[b_A2X_tmp_tmp] = Local_Average_Accel[11];
              b_A2_tmp = rt_powd_snf(tn, 0.0);
            
              A2X[A2X_tmp_tmp] = Local_Average[9];
              A2Y[A2X_tmp_tmp] = Local_Average[10];
              A2Z[A2X_tmp_tmp] = Local_Average[11];
              A2X[b_A2X_tmp_tmp] = Local_Average_Accel[9];
              A2Y[b_A2X_tmp_tmp] = Local_Average_Accel[10];
              A2Z[b_A2X_tmp_tmp] = Local_Average_Accel[11];
              A2_tmp = rt_powd_snf(tn, 1.0);
            
            
              A2X[A2X_tmp_tmp] = Local_Average[9];
              A2Y[A2X_tmp_tmp] = Local_Average[10];
              A2Z[A2X_tmp_tmp] = Local_Average[11];
              A2X[b_A2X_tmp_tmp] = Local_Average_Accel[9];
              A2Y[b_A2X_tmp_tmp] = Local_Average_Accel[10];
              A2Z[b_A2X_tmp_tmp] = Local_Average_Accel[11];
            

        }
        
    }
  }

  //              AX2n=pinv(A2)*A2X;
  //              AY2n=pinv(A2)*A2Y;
  //              AZ2n=pinv(A2)*A2Z;

  linsolve(A2, A2X, AX2n);
  linsolve(A2, A2Y, AY2n);
  linsolve(A2, A2Z, AZ2n);
    
//    for (int k = 0; k < 4; k++)
//    {
//        //    cout<<A2[k]<<"  "<<A2[k+8]<<"  "<<A2[k+16]<<"  "<<A2[k+24]<<endl;
//                    cout<<AX2n[k]<<endl;
//
//    }
    
  A2_tmp = (tn + 1.0) * (tn + 1.0);
  b_A2_tmp = rt_powd_snf(tn + 1.0, 3.0);
  Pn[0] = ((AX2n[0] + AX2n[1] * (tn + 1.0)) + AX2n[2] * A2_tmp) + AX2n[3] * b_A2_tmp;

  // +AX2n(5,1)*(tn+1)^4; %+AX2n(6,1)*(tn+1)^5;
  Pn[1] = ((AY2n[0] + AY2n[1] * (tn + 1.0)) + AY2n[2] * A2_tmp) + AY2n[3] * b_A2_tmp;

  // +AY2n(5,1)*(tn+1)^4; %+AY2n(6,1)*(tn+1)^5;
  Pn[2] = ((AZ2n[0] + AZ2n[1] * (tn + 1.0)) + AZ2n[2] * A2_tmp) + AZ2n[3] * b_A2_tmp;
    
//    cout<<Pn[0]<<"  "<<P[c]<<endl;
//    cout<<Pn[1]<<"  "<<P[c+1*tn*P.size(0)]<<endl;

    
  // +AZ2n(5,1)*(tn+1)^4; %+AZ2n(6,1)*(tn+1)^5;
  //              Pn(1)
}

