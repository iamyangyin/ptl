//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  xnrm2.h
//
//  Code generation for function 'xnrm2'
//


#ifndef XNRM2_H
#define XNRM2_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
//#include "coherent_detection_types.h"

// Function Declarations
extern double xnrm21(int n, const double x[32], int ix0);

#endif

// End of code generation (xnrm2.h)
