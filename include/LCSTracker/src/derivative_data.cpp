//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_data.cpp
//
//  Code generation for function 'derivative_data'
//


// Include files
#include "derivative_data.h"
#include "derivative.h"
#include "rt_nonfinite.h"

// Variable Definitions
boolean_T isInitialized_derivative = false;

// End of code generation (derivative_data.cpp)
