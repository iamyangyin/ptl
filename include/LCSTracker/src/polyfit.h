
#ifndef POLYFIT_H
#define POLYFIT_H

#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

extern void polyfit(coder::array<double, 2U> &x, const double y[6], double p[4],
                    double S_R_data[], int S_R_size[2], double *S_df, double
                    *S_normr, double mu[2]);

#endif

