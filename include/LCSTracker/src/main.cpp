#include "rt_nonfinite.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include "derivative.h"
#include "coherent_detection.h"


using namespace std;

int n=100;

static coder::array<double, 3U> readtextposition(int n);
static double timestep_n();
static coder::array<double, 2U> argInit_1xUnbounded_real_U(int n);
static void main_coherent_detection(double Pn_input[3]);

static coder::array<double, 2U> argInit_1xUnbounded_real_U(int n)
{
    coder::array<double, 2U> result;
    
    // Set the size of the array.
    // Change this size to the value that the application requires.
    result.set_size(n, 1);
    
    // Loop over the array to initialize each element.
    for (int idx1 = 0; idx1 < result.size(1); idx1++) {
        // Set the value of the array element.
        // Change this value to the value that the application requires.
        result[idx1] = idx1;
    }
    
    return result;
}

static coder::array<double, 3U> readtextposition(int n)
{
  coder::array<double, 3U> result;
    
    
    result.set_size(n, 6, 3);
    
    ifstream text;
    text.open("test.txt");
    if (!text)
    {
        cerr << "Unable to open file datafile.txt"<<endl;
    }
    
    for (int i = 0; i < 3*6*n; i++)
    {
        text >> result[i];
    }
  return result;
}

static double timestep_n()  {return 6.0;}
//static double argInit_real_pl() {return 0.0;}
static void main_coherent_detection(double Pn_input[3])
{
coder::array<double, 2U> particle_prediction;
particle_prediction = argInit_1xUnbounded_real_U(n);
coder::array<double, 3U> P;
coder::array<double, 3U> diff;
coder::array<double, 3U> diff2;

    // Initialize function 'derivative' input arguments.
    // Initialize function input argument 'P'.
    P = readtextposition(n);
    
    // Call the 'derivative'.
    derivative(P, timestep_n(), diff, diff2);  //calculate first and second derivatives
    // particle history
    // timestep
    // first derivative
    // second derivative
    


    for (int j = 0; j < n; j++) // predict from particle no.1 to n
    {
        coherent_detection( P, 6, diff, diff2, j, particle_prediction, Pn_input);
        // 6 = number of timesteps
        // P = particle postions from (t1 - t6)
        // diff = first derivatives
        // diff2 second derivatives
        // particle_prediction = particles to be predicted (only for turb2d)
        //Pn_input = predicted position
    }
}

int main(int, const char * const [])
{
    double Pn_input[3];

    main_coherent_detection(Pn_input);
    
    
  return 0;
}

//static coder::array<double, 3U> argInit_6xUnboundedx3_real_U();
//static coder::array<double, 3U> argInit_Unboundedx6x3_real_U();
//    double result_tmp;
////    cout<<result.size(0)<<endl;
//    result_tmp = timestep_n();
//  // Loop over the array to initialize each element.
//  for (int idx0 = 0; idx0 < result.size(0); idx0++)
//  {
//    for (int idx1 = 0; idx1 < 6; idx1++)
//    {
//      result[idx0 + result.size(0) * idx1] = result_tmp;
//      result[(idx0 + result.size(0) * idx1) + result.size(0) * 6] = result_tmp;
//      result[(idx0 + result.size(0) * idx1) + result.size(0) * 6 * 2] = result_tmp;
//    }
//  }
//coder::array<double, 2U> R;
//coder::array<double, 3U> Local_Average;
//coder::array<double, 3U> Local_Average1;

//double Eig_V[7];
//double Eig_Accel[7];
//double Pn_input[3];

//coder::array<double, 3U> Local_Average_Accel;
//pl_tmp = argInit_real_pl();
//pl = pl_tmp;
//double pl_tmp;
//double pl;
