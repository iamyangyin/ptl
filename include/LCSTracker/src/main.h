
//***********************************************************************
#ifndef MAIN_H
#define MAIN_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

// Function Declarations
extern int main(int argc, const char * const argv[]);

#endif

// End of code generation (main.h)
