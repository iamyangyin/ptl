//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_data.h
//
//  Code generation for function 'derivative_data'
//


#ifndef DERIVATIVE_DATA_H
#define DERIVATIVE_DATA_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

// Variable Declarations
extern boolean_T isInitialized_derivative;

#endif

// End of code generation (derivative_data.h)
