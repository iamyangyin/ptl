// Include files
#include "xzgeqp3.h"
#include "coherent_detection.h"
#include "rt_nonfinite.h"
#include "xnrm21.h"
#include <cmath>
#include <cstring>

// Function Declarations
static double rt_hypotd_snf(double u0, double u1);

// Function Definitions
static double rt_hypotd_snf(double u0, double u1)
{
  double y;
  double a;
  a = std::abs(u0);
  y = std::abs(u1);
  if (a < y) {
    a /= y;
    y *= std::sqrt(a * a + 1.0);
  } else if (a > y) {
    y /= a;
    y = a * std::sqrt(y * y + 1.0);
  } else {
    if (!rtIsNaN(y)) {
      y = a * 1.4142135623730951;
    }
  }

  return y;
}

void qrpf1(double A[32], double tau[4], int jpvt[4])
{
  double work[4];
  double smax;
  double scale;
  int k;
  double absxk;
  double vn1[4];
  double vn2[4];
  double t;
  int itemp;
  int pvt;
  work[0] = 0.0;
  smax = 0.0;
  scale = 3.3121686421112381E-170;
  for (k = 1; k < 9; k++) {
    absxk = std::abs(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * std::sqrt(smax);
  vn1[0] = smax;
  vn2[0] = smax;
  work[1] = 0.0;
  smax = 0.0;
  scale = 3.3121686421112381E-170;
  for (k = 9; k < 17; k++) {
    absxk = std::abs(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * std::sqrt(smax);
  vn1[1] = smax;
  vn2[1] = smax;
  work[2] = 0.0;
  smax = 0.0;
  scale = 3.3121686421112381E-170;
  for (k = 17; k < 25; k++) {
    absxk = std::abs(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * std::sqrt(smax);
  vn1[2] = smax;
  vn2[2] = smax;
  work[3] = 0.0;
  smax = 0.0;
  scale = 3.3121686421112381E-170;
  for (k = 25; k < 33; k++) {
    absxk = std::abs(A[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      smax = smax * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      smax += t * t;
    }
  }

  smax = scale * std::sqrt(smax);
  vn1[3] = smax;
  vn2[3] = smax;
  for (int i = 0; i < 4; i++) {
    int ip1;
    int iy;
    int ii;
    int ix;
    int b_i;
    ip1 = i + 2;
    iy = i << 3;
    ii = iy + i;
    itemp = 4 - i;
    pvt = 0;
    if (4 - i > 1) {
      ix = i;
      smax = std::abs(vn1[i]);
      for (k = 2; k <= itemp; k++) {
        ix++;
        scale = std::abs(vn1[ix]);
        if (scale > smax) {
          pvt = k - 1;
          smax = scale;
        }
      }
    }

    pvt += i;
    if (pvt != i) {
      ix = pvt << 3;
      for (k = 0; k < 8; k++) {
        smax = A[ix];
        A[ix] = A[iy];
        A[iy] = smax;
        ix++;
        iy++;
      }

      itemp = jpvt[pvt];
      jpvt[pvt] = jpvt[i];
      jpvt[i] = itemp;
      vn1[pvt] = vn1[i];
      vn2[pvt] = vn2[i];
    }

    absxk = A[ii];
    itemp = ii + 2;
    tau[i] = 0.0;
    smax = xnrm21(7 - i, A, ii + 2);
    if (smax != 0.0) {
      scale = rt_hypotd_snf(A[ii], smax);
      if (A[ii] >= 0.0) {
        scale = -scale;
      }

      if (std::abs(scale) < 1.0020841800044864E-292) {
        pvt = -1;
        b_i = (ii - i) + 8;
        do {
          pvt++;
          for (k = itemp; k <= b_i; k++) {
            A[k - 1] *= 9.9792015476736E+291;
          }

          scale *= 9.9792015476736E+291;
          absxk *= 9.9792015476736E+291;
        } while (!(std::abs(scale) >= 1.0020841800044864E-292));

        scale = rt_hypotd_snf(absxk, xnrm21(7 - i, A, ii + 2));
        if (absxk >= 0.0) {
          scale = -scale;
        }

        tau[i] = (scale - absxk) / scale;
        smax = 1.0 / (absxk - scale);
        for (k = itemp; k <= b_i; k++) {
          A[k - 1] *= smax;
        }

        for (k = 0; k <= pvt; k++) {
          scale *= 1.0020841800044864E-292;
        }

        absxk = scale;
      } else {
        tau[i] = (scale - A[ii]) / scale;
        smax = 1.0 / (A[ii] - scale);
        b_i = (ii - i) + 8;
        for (k = itemp; k <= b_i; k++) {
          A[k - 1] *= smax;
        }

        absxk = scale;
      }
    }

    A[ii] = absxk;
    if (i + 1 < 4) {
      int lastv;
      int lastc;
      absxk = A[ii];
      A[ii] = 1.0;
      pvt = ii + 9;
      if (tau[i] != 0.0) {
        boolean_T exitg2;
        lastv = 8 - i;
        itemp = (ii - i) + 7;
        while ((lastv > 0) && (A[itemp] == 0.0)) {
          lastv--;
          itemp--;
        }

        lastc = 2 - i;
        exitg2 = false;
        while ((!exitg2) && (lastc + 1 > 0)) {
          int exitg1;
          itemp = (ii + (lastc << 3)) + 8;
          k = itemp;
          do {
            exitg1 = 0;
            if (k + 1 <= itemp + lastv) {
              if (A[k] != 0.0) {
                exitg1 = 1;
              } else {
                k++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        lastv = 0;
        lastc = -1;
      }

      if (lastv > 0) {
        int i1;
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work[0], 0, (lastc + 1) * sizeof(double));
          }

          iy = 0;
          b_i = (ii + (lastc << 3)) + 9;
          for (itemp = pvt; itemp <= b_i; itemp += 8) {
            ix = ii;
            smax = 0.0;
            i1 = (itemp + lastv) - 1;
            for (k = itemp; k <= i1; k++) {
              smax += A[k - 1] * A[ix];
              ix++;
            }

            work[iy] += smax;
            iy++;
          }
        }

        if (!(-tau[i] == 0.0)) {
          itemp = ii;
          pvt = 0;
          for (iy = 0; iy <= lastc; iy++) {
            if (work[pvt] != 0.0) {
              smax = work[pvt] * -tau[i];
              ix = ii;
              b_i = itemp + 9;
              i1 = lastv + itemp;
              for (k = b_i; k <= i1 + 8; k++) {
                A[k - 1] += A[ix] * smax;
                ix++;
              }
            }

            pvt++;
            itemp += 8;
          }
        }
      }

      A[ii] = absxk;
    }

    for (iy = ip1; iy < 5; iy++) {
      itemp = i + ((iy - 1) << 3);
      smax = vn1[iy - 1];
      if (smax != 0.0) {
        scale = std::abs(A[itemp]) / smax;
        scale = 1.0 - scale * scale;
        if (scale < 0.0) {
          scale = 0.0;
        }

        absxk = smax / vn2[iy - 1];
        absxk = scale * (absxk * absxk);
        if (absxk <= 1.4901161193847656E-8) {
          smax = xnrm21(7 - i, A, itemp + 2);
          vn1[iy - 1] = smax;
          vn2[iy - 1] = smax;
        } else {
          vn1[iy - 1] = smax * std::sqrt(scale);
        }
      }
    }
  }
}

// End of code generation (xzgeqp3.cpp)
