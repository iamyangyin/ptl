#ifndef LINSOLVE_H
#define LINSOLVE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
//#include "coherent_detection_types.h"

#include <cstddef>
#include <cstdlib>
#include "rt_nonfinite.h"
#include "xzgeqp31.h"
#include <cmath>

// Function Declarations
extern void linsolve(const double A[32], const double B[8], double C[4]);

#endif

// End of code generation (linsolve.h)
