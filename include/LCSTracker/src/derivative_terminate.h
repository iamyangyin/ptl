//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_terminate.h
//
//  Code generation for function 'derivative_terminate'
//


#ifndef DERIVATIVE_TERMINATE_H
#define DERIVATIVE_TERMINATE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

// Function Declarations
extern void derivative_terminate();

#endif

// End of code generation (derivative_terminate.h)
