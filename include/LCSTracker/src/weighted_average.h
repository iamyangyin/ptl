#ifndef WEIGHTED_AVERAGE_H
#define WEIGHTED_AVERAGE_H

// Include files
//#include <cstddef>
//#include <cstdlib>
#include "rtwtypes.h"
#include "coder_array.h"

#include <cstddef>
#include <cstdlib>
#include "rt_nonfinite.h"
#include "xzgeqp31.h"


// Function Declarations
extern void weighted_average(double c, double Local_Average[21], const double
  Eig_V[7], const coder::array<double, 3U> &diff, const double Eig_Accel[7],
  double Local_Average_Accel[21], const coder::array<double, 3U> &diff2);

#endif

