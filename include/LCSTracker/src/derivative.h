#ifndef DERIVATIVE_H
#define DERIVATIVE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

// Function Declarations
extern void derivative(const coder::array<double, 3U> &P, double tn, coder::
  array<double, 3U> &diff, coder::array<double, 3U> &diff2);

#endif

// End of code generation (derivative.h)
