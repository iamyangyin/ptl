//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_terminate.cpp
//
//  Code generation for function 'derivative_terminate'
//


// Include files
#include "derivative_terminate.h"
#include "derivative.h"
#include "derivative_data.h"
#include "rt_nonfinite.h"

// Function Definitions
void derivative_terminate()
{
  // (no terminate code required)
  isInitialized_derivative = false;
}

// End of code generation (derivative_terminate.cpp)
