#ifndef XZGEQP3_H
#define XZGEQP3_H

#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

extern void qrpf(coder::array<double, 2U> &A, int m, double tau_data[], int
                 jpvt[4]);

#endif

