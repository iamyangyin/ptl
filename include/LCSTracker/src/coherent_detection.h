
#ifndef COHERENT_DETECTION_H
#define COHERENT_DETECTION_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

#include "rt_nonfinite.h"
#include <cstring>
#include "xzgeqp31.h"
#include <cmath>
#include "coder_array.h"

//#include "rtwtypes.h"

//#include "coherent_detection_types.h"

// Function Declarations
extern void coherent_detection( const coder::array<double, 3U> &P,
  double tn, const coder::array<double, 3U> &diff, const coder::array<double, 3U>
  &diff2, double c, const coder::array<double, 2U> &particle_prediction, double
  Pn_input[3]);

#endif

