
#include "coherent_detection.h"
//#include "coherent_detection_data.h"
//#include "coherent_detection_initialize.h"
#include "predictor.h"
#include "rt_nonfinite.h"
#include "weighted_average.h"
#include <cstring>
#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <math.h>       /* log */
//#include <algorithm>

using namespace Eigen;
using namespace std;



// Function Definitions
void coherent_detection( const coder::array<double, 3U> &P, double tn,
  const coder::array<double, 3U> &diff, const coder::array<double, 3U> &diff2,
  double c, const coder::array<double, 2U> &particle_prediction, double
  Pn_input[3])
{
  double Local_Average[21];
  double Local_Average_Accel[21];
  int i;
  coder::array<double, 2U> Eig_max;
  int loop_ub;
    
  double Eig_V[7]={ 0 };
  double Eig_Accel[7]={ 0 };
  double c_a_tmp;
//  if (!isInitialized_coherent_detection) {
//    coherent_detection_initialize();
//  }
    Pn_input[0] = 0.0;
    Pn_input[1] = 0.0;
    Pn_input[2] = 0.0;
    
  // FTLE criteria for acceleration
  // FTLE criteria for velocity
  // Weight FTLE
  // left over particles after shaking
  std::memset(&Local_Average[0], 0, 21U * sizeof(double));
  std::memset(&Local_Average_Accel[0], 0, 21U * sizeof(double));
    

  Eig_max.set_size(particle_prediction.size(1), 2);
  loop_ub = particle_prediction.size(1) << 1;
    
  for (i = 0; i < loop_ub; i++)
  {
    Eig_max[i] = 0.0;
  }

  //  Finding coherent particles
  // 0.245*4.5+((P(c,tn,1)-P(c,tn-1,1))^2);
  // 0.245*4.5+((P(c,tn,2)-P(c,tn-1,2))^2);
  // 0.245*4.5+((P(c,tn,3)-P(c,tn-1,3))^2);
  i = P.size(0) - 1;
    
  for (int d = 0; d <= i; d++)
  {
    int a_tmp;
    int a_tmp_tmp;
    int b_a_tmp;
    double a;
    boolean_T guard1 = false;
    boolean_T guard2 = false;
//    int d_a_tmp;
//    double e_a_tmp;
//    double b_d;
    double d1;double d2;double d3;double d4;double d5;
    double d6;double d7;double d8;double d9;
      
    double R[3];
    double max_R;
    MatrixXd F(3,3);
    MatrixXd FT(3,3);
    MatrixXd FTF(3,3);
//    MatrixXd Check(3,1);

    double   Eigen_max;
    double   Xd[3];
    double   Xc[3];
      
//    double f_a_tmp;
//    int g_a_tmp;

    //  passive condition
    loop_ub = static_cast<int>(c);
    a_tmp = loop_ub - 1;
    a_tmp_tmp = static_cast<int>(tn);
    b_a_tmp = a_tmp_tmp - 1;
    c_a_tmp = P[a_tmp + P.size(0) * b_a_tmp];
      
    a = P[c+(tn-1)*P.size(0)]- P[d+(tn-2)*P.size(0)];
    d1=a * a;


    guard1 = false;
    guard2 = false;
      
//
      R[0] = abs(P[c + P.size(0) * (tn-1) + (0)*tn*P.size(0)]-P[c + P.size(0) * (tn-2) + (0)*tn*P.size(0)]);
      R[1] = abs(P[c + P.size(0) * (tn-1) + (1)*tn*P.size(0)]-P[c + P.size(0) * (tn-2) + (1)*tn*P.size(0)]);
      R[2] = abs(P[c + P.size(0) * (tn-1) + (2)*tn*P.size(0)]-P[c + P.size(0) * (tn-2) + (2)*tn*P.size(0)]);
      
      max_R= max({R[0], R[1], R[2]});
      
      R[0] = R[0] + max_R;
      R[1] = R[1] + max_R;
      R[2] = R[2] + max_R;
      
    if (d1 <= pow(R[0],2.5))
    {

        a=P[c+(tn-1)*P.size(0)+tn*P.size(0)]- P[d+(tn-2)*P.size(0)+tn*P.size(0)];
        
//      b_d = a * a;
        
      d2=a * a;
      if (d2 <= pow(R[1],2.5))
      {

        a = P[c+(tn-1)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-2)*P.size(0)+2*tn*P.size(0)];

        d3 = a * a;
        if ((d3 <= pow(R[2],2.5)) && (d != c))
        {

            a  = P[c+(tn-2)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+0*tn*P.size(0)];
            d4 = a*a;
          if (d4 <= pow(R[0],1.5)) {
              
              a = P[c+(tn-2)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+1*tn*P.size(0)];
              d5= a * a;
            if (d5 <= pow(R[1],1.5))
            {

                a  = P[c+(tn-2)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+2*tn*P.size(0)];
                d6 = a * a;
              if (d6 <= pow(R[2],1.5))
              {

                  a=P[c+(tn-3)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-4)*P.size(0)+0*tn*P.size(0)];
                  d7 = a * a;
                if (d7 <= pow(R[0],1))
                {

                    a=P[c+(tn-3)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-4)*P.size(0)+1*tn*P.size(0)];
                    d8 = a * a;
                  if (d8 <= pow(R[1],1))
                  {

                      a=P[c+(tn-3)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-4)*P.size(0)+2*tn*P.size(0)];
                      d9 = a * a;
                    if (d9 <= pow(R[2],1))
                    {

                        //  passive condition

                        Xd[0] = (diff[ 6 * d+1 +  6 * diff.size(1) * 0]+diff[ 6 * d+4 +   6 * diff.size(1) * 0])/2;
                        Xd[1] = (diff[ 6 * d+1 +  6 * diff.size(1) * 1]+diff[ 6 * d+4 +   6 * diff.size(1) * 1])/2;
                        Xd[2] = (diff[ 6 * d+1 +  6 * diff.size(1) * 2]+diff[ 6 * d+4 +   6 * diff.size(1) * 2])/2;
                        
                        Xc[0] = (diff[ 6 * c+0 +   6 * diff.size(1) * 0]+diff[  6 * c+3 +   6 * diff.size(1) * 0])/2;
                        Xc[1] = (diff[ 6 * c+0 +   6 * diff.size(1) * 1]+diff[  6 * c+3 +   6 * diff.size(1) * 1])/2;
                        Xc[2] = (diff[ 6 * c+0 +   6 * diff.size(1) * 2]+diff[  6 * c+3 +   6 * diff.size(1) * 2])/2;
                        
                        F(0,0)=Xd[0]/Xc[0];     F(1,0)=Xd[1]/Xc[0];     F(2,0)=0; //Xd[2]/Xc[0];
                        F(0,1)=Xd[0]/Xc[1];     F(1,1)=Xd[1]/Xc[1];     F(2,1)=0; //Xd[2]/Xc[1];
                        F(0,2)=0; //Xd[0]/Xc[2];
                        F(1,2)=0; //Xd[1]/Xc[2];/Users/ali.khojasteh/klpt-lapiv/build/CMakeLists.txt
                        F(2,2)=0; //Xd[2]/Xc[2];
                        
                        FT=F.transpose();
                        FTF=FT*F;
                        
                      // ~isnan(FTF)
                      //          [V,D] = eig(FTF);
                        
                        EigenSolver<Eigen::Matrix<double, 3,3> > s(FTF); // the instance s(A) includes the eigensystem

                        MatrixXd Check(3,1);
                        Eigen_max = s.eigenvalues()(0).real();

//                      Eig_max[1] = 0.69314718055994529;
//                      Eig_max[0] = 4.0;
                        Eig_max[0]=Eigen_max;
                        Eig_max[1]=0.5*(log(Eigen_max));

//                        cout << "eigenvalues:" << endl;
//                        cout << Eig_max[1] << endl;
                        
                      if (Eig_max[1] < 2.25)
                      {
//                        c_a_tmp -= P[d + P.size(0) * d_a_tmp];
//                        a =  P[c+(tn-1)*P.size(0)]- P[d+(tn-2)*P.size(0)];

                        Eig_V[0] += 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average[0] += diff[  6 * d] * 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average[1] += diff[  6 * d +   6 * diff.size(1)] * 5.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average[2] += diff[  6 * d +   6 * diff.size(1) * 2] * 5.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                          
                      }

                      if (Eig_max[1] < 2.0)
                      {
                        Eig_Accel[0] += 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average_Accel[0] += diff2[  6 * d] * 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average_Accel[1] += diff2[  6 * d +   6 * diff2.size(1)] * 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average_Accel[2] += diff2[  6 * d +   6 * diff2.size(1) * 2] * 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        //
                      }
                    } else
                    {
                      guard2 = true;
                    }
                  } else
                  {
                    guard2 = true;
                  }
                } else
                {
                  guard2 = true;
                }
              } else
              {
                guard2 = true;
              }
            } else
            {
              guard2 = true;
            }
          } else
          {
            guard2 = true;
          }
        } else
        {
          guard2 = true;
        }
      } else
      {
        guard2 = true;
      }
    } else
    {
      guard2 = true;
    }


      
      
      
      if (guard2)
    {

        a  = P[c+(tn-1)*P.size(0)]- P[d+(tn-1)*P.size(0)];
        d1 = a * a;
        
      if (d1 <= pow(R[0],3))
      {

        a  = P[c+(tn-1)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-1)*P.size(0)+1*tn*P.size(0)];
        d2 = a * a;
        if (d2 <= pow(R[1],3))
        {
            a  = P[c+(tn-1)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-1)*P.size(0)+2*tn*P.size(0)];
            d3 = a * a;
          if ((d3 <= pow(R[2],3)) && (d != c))
          {
              a  = P[c+(tn-2)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-2)*P.size(0)+0*tn*P.size(0)];
              d4 = a * a;
            if (d4 <= pow(R[0],2))
            {
                a  = P[c+(tn-2)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-2)*P.size(0)+1*tn*P.size(0)];
                d5 = a * a;
              if (d5 <= pow(R[1],2))
              {
                  a  = P[c+(tn-2)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-2)*P.size(0)+2*tn*P.size(0)];
                  d6 = a * a;
                if (d6 <= pow(R[2],2))
                {
                    a  = P[c+(tn-3)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+0*tn*P.size(0)];
                    d7 = a * a;
                  if (d7 <= pow(R[0],1.5)) {
                      a  = P[c+(tn-3)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+1*tn*P.size(0)];
                      d8 = a * a;
                    if (d8 <= pow(R[1],1.5))
                    {
                        a  = P[c+(tn-3)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+2*tn*P.size(0)];
                        d9 = a * a;
                      if (d9 <= pow(R[2],1.5)) {

                          //  active condition

                          Xd[0] = (diff[  6 * d+0 +   6 * diff.size(1) * 0]+diff[  6 * d+3 +   6 * diff.size(1) * 0])/2;
                          Xd[1] = (diff[  6 * d+0 +   6 * diff.size(1) * 1]+diff[  6 * d+3 +   6 * diff.size(1) * 1])/2;
                          Xd[2] = (diff[  6 * d+0 +   6 * diff.size(1) * 2]+diff[  6 * d+3 +   6 * diff.size(1) * 2])/2;
                          
                          Xc[0] = (diff[  6 * c+0 +   6 * diff.size(1) * 0]+diff[  6 * c+3 +   6 * diff.size(1) * 0])/2;
                          Xc[1] = (diff[  6 * c+0 +   6 * diff.size(1) * 1]+diff[  6 * c+3 +   6 * diff.size(1) * 1])/2;
                          Xc[2] = (diff[  6 * c+0 +   6 * diff.size(1) * 2]+diff[  6 * c+3 +   6 * diff.size(1) * 2])/2;
                          
                          F(0,0)=Xd[0]/Xc[0];     F(1,0)=Xd[1]/Xc[0];     F(2,0)=0; //Xd[2]/Xc[0];
                          F(0,1)=Xd[0]/Xc[1];     F(1,1)=Xd[1]/Xc[1];     F(2,1)=0; //Xd[2]/Xc[1];
                          F(0,2)=0; //Xd[0]/Xc[2];
                          F(1,2)=0; //Xd[1]/Xc[2];/Users/ali.khojasteh/klpt-lapiv/build/CMakeLists.txt
                          F(2,2)=0; //Xd[2]/Xc[2];
                          
                          FT=F.transpose();
                          FTF=FT*F;
                          
                          // ~isnan(FTF)
                          //          [V,D] = eig(FTF);
                          
                          EigenSolver<Eigen::Matrix<double, 3,3> > s(FTF); // the instance s(A) includes the eigensystem
                          
                          Eigen_max = s.eigenvalues()(0).real();
                          
                          //                      Eig_max[1] = 0.69314718055994529;
                          //                      Eig_max[0] = 4.0;
                          Eig_max[0]=Eigen_max;
                          Eig_max[1]=0.5*(log(Eigen_max));
                          
//                          cout << "eigenvalues:" << endl;
//                          cout << Eig_max[1] << endl;
                          
                        if (Eig_max[1] < 2.5)
                        {
//                          a = (b_d + d1) + d2;

//                          Eig_V[1] += 2.0 / (a + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Eig_V[1] += 2 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average[3] += diff[  6 * d] * 2.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average[4] += diff[  6 * d +   6 * diff.size(1)] * 2.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        Local_Average[5] += diff[  6 * d +   6 * diff.size(1) * 2] * 2.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                            
                        }

                        if (Eig_max[1] < 2.25)
                        {
                          Eig_Accel[1] += 5.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average_Accel[3] += diff2[  6 * d] * 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average_Accel[4] += diff2[  6 * d +   6 * diff2.size(1)] * 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average_Accel[5] += diff2[  6 * d +   6 * diff2.size(1) * 2] * 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                        }
                      } else
                      {
                        guard1 = true;
                      }
                    } else
                    {
                      guard1 = true;
                    }
                  } else
                  {
                    guard1 = true;
                  }
                } else
                {
                  guard1 = true;
                }
              } else
              {
                guard1 = true;
              }
            } else
            {
              guard1 = true;
            }
          } else
          {
            guard1 = true;
          }
        } else
        {
          guard1 = true;
        }
      } else
      {
        guard1 = true;
      }
    }

    if (guard1)
    {
        a  = P[c+(tn-1)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+0*tn*P.size(0)];
        d1 = a * a;
        if (d1 <= pow(R[0],2.5))
        {
            a  = P[c+(tn-1)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+1*tn*P.size(0)];
            d2 = a * a;
        if (d2 <= pow(R[1],2.5))
        {
            a  = P[c+(tn-1)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-3)*P.size(0)+2*tn*P.size(0)];
            d3 = a * a;
          if ((d3 <= pow(R[2],2.5)) && (d != c))
          {
              a  = P[c+(tn-2)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-4)*P.size(0)+0*tn*P.size(0)];
              d4 = a * a;
            if (d4 <= pow(R[0],1.75))
            {
                a  = P[c+(tn-2)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-4)*P.size(0)+1*tn*P.size(0)];
                d5 = a * a;
              if (d5 <= pow(R[1],1.75))
              {
                  a  = P[c+(tn-2)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-4)*P.size(0)+2*tn*P.size(0)];
                  d6 = a * a;
                if (d6 <= pow(R[2],1.75))
                {
                    a  = P[c+(tn-3)*P.size(0)+0*tn*P.size(0)]- P[d+(tn-5)*P.size(0)+0*tn*P.size(0)];
                    d7 = a * a;
                  if (d7 <= pow(R[0],1.5))
                  {
                      a  = P[c+(tn-3)*P.size(0)+1*tn*P.size(0)]- P[d+(tn-5)*P.size(0)+1*tn*P.size(0)];
                      d8 = a * a;
                    if (d8 <= pow(R[1],1.5))
                    {
                        a  = P[c+(tn-3)*P.size(0)+2*tn*P.size(0)]- P[d+(tn-5)*P.size(0)+2*tn*P.size(0)];
                        d9 = a * a;
                      if (d9 <= pow(R[2],1.5))
                      {
                        //  double passive condition
                          
                          Xd[0] = (diff[  6 * d+2 +   6 * diff.size(1) * 0]+diff[  6 * d+5 +   6 * diff.size(1) * 0])/2;
                          Xd[1] = (diff[  6 * d+2 +   6 * diff.size(1) * 1]+diff[  6 * d+5 +   6 * diff.size(1) * 1])/2;
                          Xd[2] = (diff[  6 * d+2 +   6 * diff.size(1) * 2]+diff[  6 * d+5 +   6 * diff.size(1) * 2])/2;
                          
                          Xc[0] = (diff[  6 * c+0 +   6 * diff.size(1) * 0]+diff[  6 * c+3 +   6 * diff.size(1) * 0])/2;
                          Xc[1] = (diff[  6 * c+0 +   6 * diff.size(1) * 1]+diff[  6 * c+3 +   6 * diff.size(1) * 1])/2;
                          Xc[2] = (diff[  6 * c+0 +   6 * diff.size(1) * 2]+diff[  6 * c+3 +   6 * diff.size(1) * 2])/2;
                          
                          F(0,0)=Xd[0]/Xc[0];     F(1,0)=Xd[1]/Xc[0];     F(2,0)=0; //Xd[2]/Xc[0];
                          F(0,1)=Xd[0]/Xc[1];     F(1,1)=Xd[1]/Xc[1];     F(2,1)=0; //Xd[2]/Xc[1];
                          F(0,2)=0; //Xd[0]/Xc[2];
                          F(1,2)=0; //Xd[1]/Xc[2];/Users/ali.khojasteh/klpt-lapiv/build/CMakeLists.txt
                          F(2,2)=0; //Xd[2]/Xc[2];
                          
                          FT=F.transpose();
                          FTF=FT*F;
                          
                          // ~isnan(FTF)
                          //          [V,D] = eig(FTF);
                          
                          EigenSolver<Eigen::Matrix<double, 3,3> > s(FTF); // the instance s(A) includes the eigensystem
                          
                          Eigen_max = s.eigenvalues()(0).real();
                          
                          //                      Eig_max[1] = 0.69314718055994529;
                          //                      Eig_max[0] = 4.0;
                          Eig_max[0]=Eigen_max;
                          Eig_max[1]=0.5*(log(Eigen_max));
                          
//                                                    cout << "eigenvalues:" << endl;
//                                                    cout << Eig_max[1] << endl;
                        
                        if (Eig_max[1] < 2.25)
                        {
                          Eig_V[2] += 2 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average[6] += diff[ 6 * d + 1] * 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average[7] += diff[( 6 * d +  6 * diff.size(1)) + 1] * 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average[8] += diff[( 6 * d +  6 * diff.size(1) * 2) + 1] * 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                        }

                        if (Eig_max[1] < 2.0)
                        {
                          Eig_Accel[2] += 2.0 / (d1 + d2 + d3  + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average_Accel[6] += diff2[ 6 * d + 1] * 2.0 / (d1 + d2 + d3   + 0.1 * (Eig_max[1] * Eig_max[1]));
                          Local_Average_Accel[7] += diff2[( 6 * d +  6 * diff2.size(1)) + 1] * 2.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] *Eig_max[1]));
                          Local_Average_Accel[8] += diff2[( 6 * d +  6 * diff2.size(1) * 2) + 1] * 2.0 / (d1 + d2 + d3 + 0.1 * (Eig_max[1] * Eig_max[1]));
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
    //    cout<<c+1<<endl;
    //    cout<<Local_Average_Accel[9]<<endl;
    //    cout<<Local_Average_Accel[10]<<endl;
    //    cout<<Local_Average_Accel[11]<<endl;
    //    cout<<""<<endl;
    
    //    Local_Average_Accel[9]= -0.0040;
    //    Local_Average_Accel[10]= -0.0065;
    //    Local_Average_Accel[11]= 0;
    //    Local_Average[9] = -0.2229;
    //    Local_Average[10]= -0.2354;
    //    Local_Average[11]= 0;
    
  weighted_average(c, Local_Average, Eig_V, diff, Eig_Accel, Local_Average_Accel,diff2);
  predictor(c, Pn_input, P, Local_Average, Local_Average_Accel, tn);

//    cout<<Pn_input[0]<<"  "<<P[c + P.size(0) * (tn-1) + 0*tn*P.size(0)]<<endl;

    
//    std::cout << "Here is the matrix m:\n" << X << std::endl;

//    X1=(diff(1,c,1)+diff(4,c,1))/2; Y1=(diff(1,c,2)+diff(4,c,2))/2; Z1=(diff(1,c,3)+diff(4,c,3))/2;
//    x1=(diff(2,d,1)+diff(5,d,1))/2; y1=(diff(2,d,2)+diff(5,d,2))/2; z1=(diff(2,d,3)+diff(5,d,3))/2;
//    P[c + P.size(0) * (timestep 0-5) + (xyz 0-1-2)*tn*P.size(0)]
}

