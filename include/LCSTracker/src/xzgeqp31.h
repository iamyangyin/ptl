#ifndef XZGEQP3_H
#define XZGEQP3_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
//#include "coherent_detection_types.h"

// Function Declarations
extern void qrpf1(double A[32], double tau[4], int jpvt[4]);

#endif

// End of code generation (xzgeqp3.h)
