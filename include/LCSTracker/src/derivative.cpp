#include "derivative.h"
#include "polyfit.h"
#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <cmath>

void derivative(const coder::array<double, 3U> &P, double tn, coder::array<
                double, 3U> &diff, coder::array<double, 3U> &diff2)
{
  unsigned int c;
  coder::array<double, 2U> t;
  int loop_ub;
  int i;
  coder::array<double, 2U> b_t;
  double b_P[6];
  double AXn[4];
  double expl_temp_data[16];
  int expl_temp_size[2];
  double expl_temp;
  double b_expl_temp;
  double mu[2];
  double AYn[4];
  double AZn[4];


  c = 0U;
  if (rtIsNaN(tn))
  {
    t.set_size(1, 1);
    t[0] = rtNaN;
  } else if (tn < 1.0)
  {
    t.set_size(1, 0);
  } else if (rtIsInf(tn) && (1.0 == tn))
  {
    t.set_size(1, 1);
    t[0] = rtNaN;
  } else
  {
    loop_ub = static_cast<int>(std::floor(tn - 1.0));
    t.set_size(1, (loop_ub + 1));
    for (i = 0; i <= loop_ub; i++) {
      t[i] = static_cast<double>(i) + 1.0;
    }
  }

  // polyniomial timestep
  diff.set_size(6, P.size(0), 3);
  loop_ub = 6 * P.size(0) * 3;
  for (i = 0; i < loop_ub; i++) {
    diff[i] = 0.0;
  }

  diff2.set_size(6, P.size(0), 3);
  loop_ub = 6 * P.size(0) * 3;
  for (i = 0; i < loop_ub; i++) {
    diff2[i] = 0.0;
  }

  i = P.size(0) - 1;
  for (int b_i = 0; b_i <= i; b_i++) {
    int diff_tmp;
    c++;
    for (diff_tmp = 0; diff_tmp < 6; diff_tmp++) {
      b_P[diff_tmp] = P[(static_cast<int>(c) + P.size(0) * diff_tmp) - 1];
    }

    b_t.set_size(1, t.size(1));
    loop_ub = t.size(0) * t.size(1) - 1;
    for (diff_tmp = 0; diff_tmp <= loop_ub; diff_tmp++) {
      b_t[diff_tmp] = t[diff_tmp];
    }

    polyfit(b_t, b_P, AXn, expl_temp_data, expl_temp_size, &expl_temp,
            &b_expl_temp, mu);
    for (diff_tmp = 0; diff_tmp < 6; diff_tmp++) {
      b_P[diff_tmp] = P[((static_cast<int>(c) + P.size(0) * diff_tmp) + P.size(0)
                         * 6) - 1];
    }

    b_t.set_size(1, t.size(1));
    loop_ub = t.size(0) * t.size(1) - 1;
    for (diff_tmp = 0; diff_tmp <= loop_ub; diff_tmp++) {
      b_t[diff_tmp] = t[diff_tmp];
    }

    polyfit(b_t, b_P, AYn, expl_temp_data, expl_temp_size, &expl_temp,
            &b_expl_temp, mu);
    for (diff_tmp = 0; diff_tmp < 6; diff_tmp++) {
      b_P[diff_tmp] = P[((static_cast<int>(c) + P.size(0) * diff_tmp) + P.size(0)
                         * 6 * 2) - 1];
    }

    b_t.set_size(1, t.size(1));
    loop_ub = t.size(0) * t.size(1) - 1;
    for (diff_tmp = 0; diff_tmp <= loop_ub; diff_tmp++) {
      b_t[diff_tmp] = t[diff_tmp];
    }

    double x_tmp;
    double b_x_tmp;
    double c_x_tmp;
    double d_x_tmp;
    double e_x_tmp;
    double f_x_tmp;
    double g_x_tmp;
    int b_diff_tmp;
    double c_diff_tmp;
    int d_diff_tmp;
    int e_diff_tmp;
    double f_diff_tmp;
    int g_diff_tmp;
    double h_diff_tmp;
    double i_diff_tmp;
    int j_diff_tmp;
    double k_diff_tmp;
    double l_diff_tmp;
    double m_diff_tmp;
    double n_diff_tmp;
    double o_diff_tmp;
    double p_diff_tmp;
    double q_diff_tmp;
    double r_diff_tmp;
    double s_diff_tmp;
    double t_diff_tmp;
    double u_diff_tmp;
    double v_diff_tmp;
    double w_diff_tmp;
    double x_diff_tmp;
    double y_diff_tmp;
    double ab_diff_tmp;
    polyfit(b_t, b_P, AZn, expl_temp_data, expl_temp_size, &expl_temp,
            &b_expl_temp, mu);

    // first derivative at tn
    // first derivative at tn-1
    // first derivative at tn-2
    //
    expl_temp = ((tn - 0.00245) - mu[0]) / mu[1];
    b_expl_temp = (tn - mu[0]) / mu[1];

    // +4*AXn(1,5)*(tn-0.1)^3;
    // +4*AYn(1,5)*(tn-0.1)^3;
    // +4*AZn(1,5)*(tn-0.1)^3;
    x_tmp = (((tn - 1.0) - 0.00245) - mu[0]) / mu[1];
    b_x_tmp = ((tn - 1.0) - mu[0]) / mu[1];

    // +4*AXn(1,5)*(tn-0.1)^3;
    // +4*AYn(1,5)*(tn-0.1)^3;
    // +4*AZn(1,5)*(tn-0.1)^3;
    c_x_tmp = (((tn - 2.0) - 0.00245) - mu[0]) / mu[1];
    d_x_tmp = ((tn - 2.0) - mu[0]) / mu[1];

    // +4*AXn(1,5)*(tn-0.1)^3;
    // +4*AYn(1,5)*(tn-0.1)^3;
    // +4*AZn(1,5)*(tn-0.1)^3;
    // second derivative at tn
    // second derivative at tn-1
    // second derivative at tn-2
    e_x_tmp = ((tn - 0.0049) - mu[0]) / mu[1];
    f_x_tmp = (((tn - 1.0) - 0.0049) - mu[0]) / mu[1];
    g_x_tmp = (((tn - 2.0) - 0.0049) - mu[0]) / mu[1];
    diff_tmp = static_cast<int>(c) - 1;
    loop_ub = static_cast<int>(tn);
    b_diff_tmp = loop_ub - 1;
    c_diff_tmp = P[diff_tmp + P.size(0) * b_diff_tmp];
    d_diff_tmp = static_cast<int>(tn - 1.0) - 1;
    diff[6 * diff_tmp] = c_diff_tmp - P[diff_tmp + P.size(0) * d_diff_tmp];
    e_diff_tmp = loop_ub - 3;
    f_diff_tmp = P[diff_tmp + P.size(0) * e_diff_tmp];
    g_diff_tmp = loop_ub - 2;
    h_diff_tmp = P[diff_tmp + P.size(0) * g_diff_tmp];
    i_diff_tmp = h_diff_tmp - f_diff_tmp;
    diff[6 * diff_tmp + 1] = i_diff_tmp;
    j_diff_tmp = loop_ub - 4;
    k_diff_tmp = P[diff_tmp + P.size(0) * j_diff_tmp];
    l_diff_tmp = f_diff_tmp - k_diff_tmp;
    diff[6 * diff_tmp + 2] = l_diff_tmp;
    diff2[6 * diff_tmp] = ((c_diff_tmp - h_diff_tmp) - h_diff_tmp) + f_diff_tmp;
    diff2[6 * diff_tmp + 1] = (i_diff_tmp - f_diff_tmp) + k_diff_tmp;
    loop_ub -= 5;
    diff2[6 * diff_tmp + 2] = (l_diff_tmp - k_diff_tmp) + P[diff_tmp + P.size(0)
      * loop_ub];
    c_diff_tmp = P[(diff_tmp + P.size(0) * b_diff_tmp) + P.size(0) * 6];
    diff[6 * diff_tmp + 6 * diff.size(1)] = c_diff_tmp - P[(diff_tmp + P.size(0)
      * d_diff_tmp) + P.size(0) * 6];
    f_diff_tmp = P[(diff_tmp + P.size(0) * e_diff_tmp) + P.size(0) * 6];
    h_diff_tmp = P[(diff_tmp + P.size(0) * g_diff_tmp) + P.size(0) * 6];
    i_diff_tmp = h_diff_tmp - f_diff_tmp;
    diff[(6 * diff_tmp + 6 * diff.size(1)) + 1] = i_diff_tmp;
    k_diff_tmp = P[(diff_tmp + P.size(0) * j_diff_tmp) + P.size(0) * 6];
    l_diff_tmp = f_diff_tmp - k_diff_tmp;
    diff[(6 * diff_tmp + 6 * diff.size(1)) + 2] = l_diff_tmp;
    diff2[6 * diff_tmp + 6 * diff2.size(1)] = ((c_diff_tmp - h_diff_tmp) -
      h_diff_tmp) + f_diff_tmp;
    diff2[(6 * diff_tmp + 6 * diff2.size(1)) + 1] = (i_diff_tmp - f_diff_tmp) +
      k_diff_tmp;
    diff2[(6 * diff_tmp + 6 * diff2.size(1)) + 2] = (l_diff_tmp - k_diff_tmp) +
      P[(diff_tmp + P.size(0) * loop_ub) + P.size(0) * 6];
    c_diff_tmp = P[(diff_tmp + P.size(0) * b_diff_tmp) + P.size(0) * 6 * 2];
    diff[6 * diff_tmp + 6 * diff.size(1) * 2] = c_diff_tmp - P[(diff_tmp +
      P.size(0) * d_diff_tmp) + P.size(0) * 6 * 2];
    f_diff_tmp = P[(diff_tmp + P.size(0) * e_diff_tmp) + P.size(0) * 6 * 2];
    h_diff_tmp = P[(diff_tmp + P.size(0) * g_diff_tmp) + P.size(0) * 6 * 2];
    i_diff_tmp = h_diff_tmp - f_diff_tmp;
    diff[(6 * diff_tmp + 6 * diff.size(1) * 2) + 1] = i_diff_tmp;
    k_diff_tmp = P[(diff_tmp + P.size(0) * j_diff_tmp) + P.size(0) * 6 * 2];
    l_diff_tmp = f_diff_tmp - k_diff_tmp;
    diff[(6 * diff_tmp + 6 * diff.size(1) * 2) + 2] = l_diff_tmp;
    m_diff_tmp = expl_temp * (expl_temp * (expl_temp * AXn[0] + AXn[1]) + AXn[2])
      + AXn[3];
    n_diff_tmp = b_expl_temp * (b_expl_temp * (b_expl_temp * AXn[0] + AXn[1]) +
      AXn[2]) + AXn[3];
    o_diff_tmp = expl_temp * (expl_temp * (expl_temp * AYn[0] + AYn[1]) + AYn[2])
      + AYn[3];
    p_diff_tmp = b_expl_temp * (b_expl_temp * (b_expl_temp * AYn[0] + AYn[1]) +
      AYn[2]) + AYn[3];
    q_diff_tmp = expl_temp * (expl_temp * (expl_temp * AZn[0] + AZn[1]) + AZn[2])
      + AZn[3];
    r_diff_tmp = b_expl_temp * (b_expl_temp * (b_expl_temp * AZn[0] + AZn[1]) +
      AZn[2]) + AZn[3];
    s_diff_tmp = x_tmp * (x_tmp * (x_tmp * AXn[0] + AXn[1]) + AXn[2]) + AXn[3];
    t_diff_tmp = b_x_tmp * (b_x_tmp * (b_x_tmp * AXn[0] + AXn[1]) + AXn[2]) +
      AXn[3];
    u_diff_tmp = x_tmp * (x_tmp * (x_tmp * AYn[0] + AYn[1]) + AYn[2]) + AYn[3];
    v_diff_tmp = b_x_tmp * (b_x_tmp * (b_x_tmp * AYn[0] + AYn[1]) + AYn[2]) +
      AYn[3];
    w_diff_tmp = x_tmp * (x_tmp * (x_tmp * AZn[0] + AZn[1]) + AZn[2]) + AZn[3];
    x_tmp = b_x_tmp * (b_x_tmp * (b_x_tmp * AZn[0] + AZn[1]) + AZn[2]) + AZn[3];
    b_x_tmp = c_x_tmp * (c_x_tmp * (c_x_tmp * AXn[0] + AXn[1]) + AXn[2]) + AXn[3];
    x_diff_tmp = d_x_tmp * (d_x_tmp * (d_x_tmp * AXn[0] + AXn[1]) + AXn[2]) +
      AXn[3];
    y_diff_tmp = c_x_tmp * (c_x_tmp * (c_x_tmp * AYn[0] + AYn[1]) + AYn[2]) +
      AYn[3];
    ab_diff_tmp = d_x_tmp * (d_x_tmp * (d_x_tmp * AYn[0] + AYn[1]) + AYn[2]) +
      AYn[3];
    b_expl_temp = c_x_tmp * (c_x_tmp * (c_x_tmp * AZn[0] + AZn[1]) + AZn[2]) +
      AZn[3];
    expl_temp = d_x_tmp * (d_x_tmp * (d_x_tmp * AZn[0] + AZn[1]) + AZn[2]) +
      AZn[3];
    diff2[6 * diff_tmp + 6 * diff2.size(1) * 2] = ((c_diff_tmp - h_diff_tmp) -
      h_diff_tmp) + f_diff_tmp;
    diff2[(6 * diff_tmp + 6 * diff2.size(1) * 2) + 1] = (i_diff_tmp - f_diff_tmp)
      + k_diff_tmp;
    diff2[(6 * diff_tmp + 6 * diff2.size(1) * 2) + 2] = (l_diff_tmp - k_diff_tmp)
      + P[(diff_tmp + P.size(0) * loop_ub) + P.size(0) * 6 * 2];
    diff[6 * diff_tmp + 3] = (n_diff_tmp - m_diff_tmp) / 0.00245;
    diff[(6 * diff_tmp + 6 * diff.size(1)) + 3] = (p_diff_tmp - o_diff_tmp) /
      0.00245;
    diff[(6 * diff_tmp + 6 * diff.size(1) * 2) + 3] = (r_diff_tmp - q_diff_tmp) /
      0.00245;
    diff[6 * diff_tmp + 4] = (t_diff_tmp - s_diff_tmp) / 0.00245;
    diff[(6 * diff_tmp + 6 * diff.size(1)) + 4] = (v_diff_tmp - u_diff_tmp) /
      0.00245;
    diff[(6 * diff_tmp + 6 * diff.size(1) * 2) + 4] = (x_tmp - w_diff_tmp) /
      0.00245;
    diff[6 * diff_tmp + 5] = (x_diff_tmp - b_x_tmp) / 0.00245;
    diff[(6 * diff_tmp + 6 * diff.size(1)) + 5] = (ab_diff_tmp - y_diff_tmp) /
      0.00245;
    diff[(6 * diff_tmp + 6 * diff.size(1) * 2) + 5] = (expl_temp - b_expl_temp) /
      0.00245;
    diff2[6 * diff_tmp + 3] = ((n_diff_tmp - 2.0 * m_diff_tmp) + (e_x_tmp *
      (e_x_tmp * (e_x_tmp * AXn[0] + AXn[1]) + AXn[2]) + AXn[3])) / 0.00245;
    diff2[(6 * diff_tmp + 6 * diff2.size(1)) + 3] = ((p_diff_tmp - 2.0 *
      o_diff_tmp) + (e_x_tmp * (e_x_tmp * (e_x_tmp * AYn[0] + AYn[1]) + AYn[2])
                     + AYn[3])) / 0.00245;
    diff2[(6 * diff_tmp + 6 * diff2.size(1) * 2) + 3] = ((r_diff_tmp - 2.0 *
      q_diff_tmp) + (e_x_tmp * (e_x_tmp * (e_x_tmp * AZn[0] + AZn[1]) + AZn[2])
                     + AZn[3])) / 0.00245;
    diff2[6 * diff_tmp + 4] = ((t_diff_tmp - 2.0 * s_diff_tmp) + (f_x_tmp *
      (f_x_tmp * (f_x_tmp * AXn[0] + AXn[1]) + AXn[2]) + AXn[3])) / 0.00245;
    diff2[(6 * diff_tmp + 6 * diff2.size(1)) + 4] = ((v_diff_tmp - 2.0 *
      u_diff_tmp) + (f_x_tmp * (f_x_tmp * (f_x_tmp * AYn[0] + AYn[1]) + AYn[2])
                     + AYn[3])) / 0.00245;
    diff2[(6 * diff_tmp + 6 * diff2.size(1) * 2) + 4] = ((x_tmp - 2.0 *
      w_diff_tmp) + (f_x_tmp * (f_x_tmp * (f_x_tmp * AZn[0] + AZn[1]) + AZn[2])
                     + AZn[3])) / 0.00245;
    diff2[6 * diff_tmp + 5] = ((x_diff_tmp - 2.0 * b_x_tmp) + (g_x_tmp *
      (g_x_tmp * (g_x_tmp * AXn[0] + AXn[1]) + AXn[2]) + AXn[3])) / 0.00245;
    diff2[(6 * diff_tmp + 6 * diff2.size(1)) + 5] = ((ab_diff_tmp - 2.0 *
      y_diff_tmp) + (g_x_tmp * (g_x_tmp * (g_x_tmp * AYn[0] + AYn[1]) + AYn[2])
                     + AYn[3])) / 0.00245;
    diff2[(6 * diff_tmp + 6 * diff2.size(1) * 2) + 5] = ((expl_temp - 2.0 *
      b_expl_temp) + (g_x_tmp * (g_x_tmp * (g_x_tmp * AZn[0] + AZn[1]) + AZn[2])
                      + AZn[3])) / 0.00245;
  }
}

