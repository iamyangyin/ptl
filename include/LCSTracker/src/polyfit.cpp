
#include "polyfit.h"
#include "derivative.h"
#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <cmath>
#include <cstring>

void polyfit(coder::array<double, 2U> &x, const double y[6], double p[4], double
             S_R_data[], int S_R_size[2], double *S_df, double *S_normr, double
             mu[2])
{
  int vlen;
  double b_y;
  int mn;
  double xbar;
  int i;
  coder::array<double, 1U> absdiff;
  double scale;
  coder::array<double, 2U> V;
  double t;
  int b_mn;
  double r[6];
  coder::array<double, 2U> A;
  double tau_data[4];
  boolean_T guard1 = false;
  int jpvt[4];
  int j;
  int b_i;
  vlen = x.size(1);
  if (x.size(1) == 0) {
    b_y = 0.0;
  } else {
    b_y = x[0];
    for (mn = 2; mn <= vlen; mn++) {
      b_y += x[mn - 1];
    }
  }

  vlen = x.size(1);
  if (x.size(1) == 0) {
    xbar = rtNaN;
  } else if (x.size(1) == 1) {
    if ((!rtIsInf(x[0])) && (!rtIsNaN(x[0]))) {
      xbar = 0.0;
    } else {
      xbar = rtNaN;
    }
  } else {
    xbar = x[0];
    for (mn = 2; mn <= vlen; mn++) {
      xbar += x[mn - 1];
    }

    xbar /= static_cast<double>(x.size(1));
    absdiff.set_size(x.size(1));
    for (mn = 0; mn < vlen; mn++) {
      absdiff[mn] = std::abs(x[mn] - xbar);
    }

    xbar = 0.0;
    scale = 3.3121686421112381E-170;
    vlen = x.size(1);
    for (mn = 0; mn < vlen; mn++) {
      if (absdiff[mn] > scale) {
        t = scale / absdiff[mn];
        xbar = xbar * t * t + 1.0;
        scale = absdiff[mn];
      } else {
        t = absdiff[mn] / scale;
        xbar += t * t;
      }
    }

    xbar = scale * std::sqrt(xbar);
    xbar /= std::sqrt(static_cast<double>(x.size(1)) - 1.0);
  }

  mu[0] = b_y / static_cast<double>(x.size(1));
  mu[1] = xbar;
  i = x.size(0) * x.size(1);
  x.set_size(1, x.size(1));
  vlen = i - 1;
  for (i = 0; i <= vlen; i++) {
    x[i] = (x[i] - mu[0]) / xbar;
  }

  V.set_size(x.size(1), 4);
  if (x.size(1) != 0) {
    i = x.size(1);
    for (mn = 0; mn < i; mn++) {
      V[mn + V.size(0) * 3] = 1.0;
    }

    i = x.size(1);
    for (mn = 0; mn < i; mn++) {
      V[mn + V.size(0) * 2] = x[mn];
    }

    i = x.size(1);
    for (mn = 0; mn < i; mn++) {
      V[mn + V.size(0)] = x[mn] * V[mn + V.size(0) * 2];
    }

    for (mn = 0; mn < i; mn++) {
      V[mn] = x[mn] * V[mn + V.size(0)];
    }
  }

  for (i = 0; i < 6; i++) {
    r[i] = y[i];
  }

  b_mn = V.size(0);
  if (b_mn >= 4) {
    b_mn = 4;
  }

  A.set_size(V.size(0), 4);
  vlen = V.size(0) * V.size(1);
  for (i = 0; i < vlen; i++) {
    A[i] = V[i];
  }

  vlen = V.size(0);
  if (vlen >= 4) {
    vlen = 4;
  }

  if (0 <= vlen - 1) {
    std::memset(&tau_data[0], 0, vlen * sizeof(double));
  }

  guard1 = false;
  if (V.size(0) == 0) {
    guard1 = true;
  } else {
    vlen = V.size(0);
    if (vlen >= 4) {
      vlen = 4;
    }

    if (vlen < 1) {
      guard1 = true;
    } else {
      jpvt[0] = 1;
      jpvt[1] = 2;
      jpvt[2] = 3;
      jpvt[3] = 4;
      qrpf(A, V.size(0), tau_data, jpvt);
    }
  }

  if (guard1) {
    jpvt[0] = 1;
    jpvt[1] = 2;
    jpvt[2] = 3;
    jpvt[3] = 4;
  }

  p[0] = 0.0;
  p[1] = 0.0;
  p[2] = 0.0;
  p[3] = 0.0;
  vlen = A.size(0);
  mn = A.size(0);
  if (mn >= 4) {
    mn = 4;
  }

  for (j = 0; j < mn; j++) {
    if (tau_data[j] != 0.0) {
      xbar = r[j];
      i = j + 2;
      for (b_i = i; b_i <= vlen; b_i++) {
        xbar += A[(b_i + A.size(0) * j) - 1] * r[b_i - 1];
      }

      xbar *= tau_data[j];
      if (xbar != 0.0) {
        r[j] -= xbar;
        i = j + 2;
        for (b_i = i; b_i <= vlen; b_i++) {
          r[b_i - 1] -= A[(b_i + A.size(0) * j) - 1] * xbar;
        }
      }
    }
  }

  for (b_i = 0; b_i < b_mn; b_i++) {
    p[jpvt[b_i] - 1] = r[b_i];
  }

  for (j = b_mn; j >= 1; j--) {
    vlen = jpvt[j - 1] - 1;
    p[vlen] /= A[(j + A.size(0) * (j - 1)) - 1];
    for (b_i = 0; b_i <= j - 2; b_i++) {
      p[jpvt[b_i] - 1] -= p[vlen] * A[b_i + A.size(0) * (j - 1)];
    }
  }

  S_R_size[0] = b_mn;
  S_R_size[1] = 4;
  if (1 < b_mn) {
    i = 0;
  } else {
    i = b_mn - 1;
  }

  for (b_i = 0; b_i <= i; b_i++) {
    S_R_data[b_i] = A[b_i];
  }

  if (2 <= b_mn) {
    std::memset(&S_R_data[1], 0, (b_mn + -1) * sizeof(double));
  }

  if (2 < b_mn) {
    i = 1;
  } else {
    i = b_mn - 1;
  }

  for (b_i = 0; b_i <= i; b_i++) {
    S_R_data[b_i + b_mn] = A[b_i + A.size(0)];
  }

  if (3 <= b_mn) {
    std::memset(&S_R_data[b_mn + 2], 0, (b_mn + -2) * sizeof(double));
  }

  if (3 < b_mn) {
    i = 2;
  } else {
    i = b_mn - 1;
  }

  for (b_i = 0; b_i <= i; b_i++) {
    S_R_data[b_i + b_mn * 2] = A[b_i + A.size(0) * 2];
  }

  if (4 <= b_mn) {
    S_R_data[11] = 0.0;
  }

  i = b_mn - 1;
  for (b_i = 0; b_i <= i; b_i++) {
    S_R_data[b_i + b_mn * 3] = A[b_i + A.size(0) * 3];
  }

  vlen = V.size(0);
  absdiff.set_size(V.size(0));
  for (b_i = 0; b_i < vlen; b_i++) {
    absdiff[b_i] = ((V[b_i] * p[0] + V[V.size(0) + b_i] * p[1]) + V[(V.size(0) <<
      1) + b_i] * p[2]) + V[3 * V.size(0) + b_i] * p[3];
  }

  *S_df = 2.0;
  b_y = 0.0;
  scale = 3.3121686421112381E-170;
  for (mn = 0; mn < 6; mn++) {
    xbar = std::abs(y[mn] - absdiff[mn]);
    if (xbar > scale) {
      t = scale / xbar;
      b_y = b_y * t * t + 1.0;
      scale = xbar;
    } else {
      t = xbar / scale;
      b_y += t * t;
    }
  }

  *S_normr = scale * std::sqrt(b_y);
}

