//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_initialize.h
//
//  Code generation for function 'derivative_initialize'
//


#ifndef DERIVATIVE_INITIALIZE_H
#define DERIVATIVE_INITIALIZE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "derivative_types.h"

// Function Declarations
extern void derivative_initialize();

#endif

// End of code generation (derivative_initialize.h)
