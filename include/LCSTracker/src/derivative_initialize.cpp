//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_initialize.cpp
//
//  Code generation for function 'derivative_initialize'
//


// Include files
#include "derivative_initialize.h"
#include "derivative.h"
#include "derivative_data.h"
#include "rt_nonfinite.h"

// Function Definitions
void derivative_initialize()
{
  rt_InitInfAndNaN();
  isInitialized_derivative = true;
}

// End of code generation (derivative_initialize.cpp)
