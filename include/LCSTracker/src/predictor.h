#ifndef PREDICTOR_H
#define PREDICTOR_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"

#include <cstddef>
#include <cstdlib>
#include "rt_nonfinite.h"
#include "xzgeqp3.h"
#include <cmath>

//#include "coherent_detection_types.h"

// Function Declarations
extern void predictor(double c, double Pn[3], const coder::array<double, 3U> &P,
                      const double Local_Average[21], const double
                      Local_Average_Accel[21], double tn);

#endif

// End of code generation (predictor.h)
