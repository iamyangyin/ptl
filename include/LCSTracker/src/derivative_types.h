//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  derivative_types.h
//
//  Code generation for function 'derivative_types'
//


#ifndef DERIVATIVE_TYPES_H
#define DERIVATIVE_TYPES_H

// Include files
#include "rtwtypes.h"
#include "coder_array.h"
#ifdef _MSC_VER

#pragma warning(push)
#pragma warning(disable : 4251)

#endif

#ifdef _MSC_VER

#pragma warning(pop)

#endif
#endif

// End of code generation (derivative_types.h)
