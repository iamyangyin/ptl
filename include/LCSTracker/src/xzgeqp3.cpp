#include "xzgeqp3.h"
#include "derivative.h"
#include "polyfit.h"
#include "rt_nonfinite.h"
#include "xnrm2.h"
#include <cmath>
#include <cstring>

static double rt_hypotd_snf(double u0, double u1);

static double rt_hypotd_snf(double u0, double u1)
{
  double y;
  double a;
  a = std::abs(u0);
  y = std::abs(u1);
  if (a < y) {
    a /= y;
    y *= std::sqrt(a * a + 1.0);
  } else if (a > y) {
    y /= a;
    y = a * std::sqrt(y * y + 1.0);
  } else {
    if (!rtIsNaN(y)) {
      y = a * 1.4142135623730951;
    }
  }

  return y;
}

void qrpf(coder::array<double, 2U> &A, int m, double tau_data[], int jpvt[4])
{
  int ma;
  int minmn;
  double work[4];
  double smax;
  double vn1[4];
  double vn2[4];
  int pvt;
  double temp2;
  double s;
  int jA;
  ma = A.size(0);
  if (m < 4) {
    minmn = m;
  } else {
    minmn = 4;
  }

  work[0] = 0.0;
  smax = xnrm2(m, A, 1);
  vn1[0] = smax;
  vn2[0] = smax;
  work[1] = 0.0;
  smax = xnrm2(m, A, ma + 1);
  vn1[1] = smax;
  vn2[1] = smax;
  work[2] = 0.0;
  smax = xnrm2(m, A, (ma << 1) + 1);
  vn1[2] = smax;
  vn2[2] = smax;
  work[3] = 0.0;
  smax = xnrm2(m, A, 3 * ma + 1);
  vn1[3] = smax;
  vn2[3] = smax;
  for (int i = 0; i < minmn; i++) {
    int ip1;
    int iy;
    int ii;
    int mmi;
    int itemp;
    int ix;
    int k;
    int b_i;
    ip1 = i + 2;
    iy = i * ma;
    ii = iy + i;
    mmi = m - i;
    itemp = 4 - i;
    pvt = 0;
    if (4 - i > 1) {
      ix = i;
      smax = std::abs(vn1[i]);
      for (k = 2; k <= itemp; k++) {
        ix++;
        s = std::abs(vn1[ix]);
        if (s > smax) {
          pvt = k - 1;
          smax = s;
        }
      }
    }

    pvt += i;
    if (pvt + 1 != i + 1) {
      ix = pvt * ma;
      for (k = 0; k < m; k++) {
        smax = A[ix];
        A[ix] = A[iy];
        A[iy] = smax;
        ix++;
        iy++;
      }

      itemp = jpvt[pvt];
      jpvt[pvt] = jpvt[i];
      jpvt[i] = itemp;
      vn1[pvt] = vn1[i];
      vn2[pvt] = vn2[i];
    }

    if (i + 1 < m) {
      temp2 = A[ii];
      itemp = ii + 2;
      tau_data[i] = 0.0;
      if (mmi > 0) {
        smax = xnrm2(mmi - 1, A, ii + 2);
        if (smax != 0.0) {
          s = rt_hypotd_snf(A[ii], smax);
          if (A[ii] >= 0.0) {
            s = -s;
          }

          if (std::abs(s) < 1.0020841800044864E-292) {
            pvt = -1;
            b_i = ii + mmi;
            do {
              pvt++;
              for (k = itemp; k <= b_i; k++) {
                A[k - 1] = 9.9792015476736E+291 * A[k - 1];
              }

              s *= 9.9792015476736E+291;
              temp2 *= 9.9792015476736E+291;
            } while (!(std::abs(s) >= 1.0020841800044864E-292));

            s = rt_hypotd_snf(temp2, xnrm2(mmi - 1, A, ii + 2));
            if (temp2 >= 0.0) {
              s = -s;
            }

            tau_data[i] = (s - temp2) / s;
            smax = 1.0 / (temp2 - s);
            for (k = itemp; k <= b_i; k++) {
              A[k - 1] = smax * A[k - 1];
            }

            for (k = 0; k <= pvt; k++) {
              s *= 1.0020841800044864E-292;
            }

            temp2 = s;
          } else {
            tau_data[i] = (s - A[ii]) / s;
            smax = 1.0 / (A[ii] - s);
            b_i = ii + mmi;
            for (k = itemp; k <= b_i; k++) {
              A[k - 1] = smax * A[k - 1];
            }

            temp2 = s;
          }
        }
      }

      A[ii] = temp2;
    } else {
      tau_data[i] = 0.0;
    }

    if (i + 1 < 4) {
      int lastv;
      int lastc;
      temp2 = A[ii];
      A[ii] = 1.0;
      jA = (ii + ma) + 1;
      if (tau_data[i] != 0.0) {
        boolean_T exitg2;
        lastv = mmi - 1;
        itemp = (ii + mmi) - 1;
        while ((lastv + 1 > 0) && (A[itemp] == 0.0)) {
          lastv--;
          itemp--;
        }

        lastc = 2 - i;
        exitg2 = false;
        while ((!exitg2) && (lastc + 1 > 0)) {
          int exitg1;
          itemp = jA + lastc * ma;
          k = itemp;
          do {
            exitg1 = 0;
            if (k <= itemp + lastv) {
              if (A[k - 1] != 0.0) {
                exitg1 = 1;
              } else {
                k++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        lastv = -1;
        lastc = -1;
      }

      if (lastv + 1 > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work[0], 0, (lastc + 1) * sizeof(double));
          }

          iy = 0;
          b_i = jA + ma * lastc;
          for (pvt = jA; ma < 0 ? pvt >= b_i : pvt <= b_i; pvt += ma) {
            ix = ii;
            smax = 0.0;
            itemp = pvt + lastv;
            for (k = pvt; k <= itemp; k++) {
              smax += A[k - 1] * A[ix];
              ix++;
            }

            work[iy] += smax;
            iy++;
          }
        }

        if (!(-tau_data[i] == 0.0)) {
          itemp = 0;
          for (k = 0; k <= lastc; k++) {
            if (work[itemp] != 0.0) {
              smax = work[itemp] * -tau_data[i];
              ix = ii;
              b_i = lastv + jA;
              for (pvt = jA; pvt <= b_i; pvt++) {
                A[pvt - 1] = A[pvt - 1] + A[ix] * smax;
                ix++;
              }
            }

            itemp++;
            jA += ma;
          }
        }
      }

      A[ii] = temp2;
    }

    for (k = ip1; k < 5; k++) {
      itemp = i + (k - 1) * ma;
      smax = vn1[k - 1];
      if (smax != 0.0) {
        s = std::abs(A[itemp]) / smax;
        s = 1.0 - s * s;
        if (s < 0.0) {
          s = 0.0;
        }

        temp2 = smax / vn2[k - 1];
        temp2 = s * (temp2 * temp2);
        if (temp2 <= 1.4901161193847656E-8) {
          if (i + 1 < m) {
            smax = xnrm2(mmi - 1, A, itemp + 2);
            vn1[k - 1] = smax;
            vn2[k - 1] = smax;
          } else {
            vn1[k - 1] = 0.0;
            vn2[k - 1] = 0.0;
          }
        } else {
          vn1[k - 1] = smax * std::sqrt(s);
        }
      }
    }
  }
}

