
#include "weighted_average.h"
#include "coherent_detection.h"
#include "predictor.h"
#include "rt_nonfinite.h"

void weighted_average(double c, double Local_Average[21], const double Eig_V[7],
                      const coder::array<double, 3U> &diff, const double
                      Eig_Accel[7], double Local_Average_Accel[21], const coder::
                      array<double, 3U> &diff2)
{
  int C1;
  int C2;
  double C3;
  int i;
  double d;

  // Relaxation factor for Acceleration averaging
  // Relaxation factor for Velocity averaging
  C1 = 0;
  C2 = 0;
  C3 = 0.0;
  if ((Eig_V[0] != 0.0) || (Eig_V[1] != 0.0) || (Eig_V[2] != 0.0)) {
    if (Eig_V[0] != 0.0) {
      Local_Average[0] /= Eig_V[0];
      Local_Average[1] /= Eig_V[0];
      Local_Average[2] /= Eig_V[0];
      C1 = 3;
    }

    if (Eig_V[1] != 0.0) {
      Local_Average[3] /= Eig_V[1];
      Local_Average[4] /= Eig_V[1];
      Local_Average[5] /= Eig_V[1];
      C2 = 3;
    }

    if (Eig_V[2] != 0.0) {
      Local_Average[6] /= Eig_V[2];
      Local_Average[7] /= Eig_V[2];
      Local_Average[8] /= Eig_V[2];
      C3 = 0.75;
    }

    i = static_cast<int>(c) - 1;
    d = (static_cast<double>(C1 + C2) + C3) +  2.2;
    Local_Average[9] = ((( 2.2 * diff[6 * i] + static_cast<double>(C1) *
                          Local_Average[0]) + static_cast<double>(C2) *
                         Local_Average[3]) + C3 * Local_Average[6]) / d;
    Local_Average[10] = ((( 2.2 * diff[6 * i + 6 * diff.size(1)] + static_cast<
      double>(C1) * Local_Average[1]) + static_cast<double>(C2) * Local_Average
                          [4]) + C3 * Local_Average[7]) / d;
    Local_Average[11] = ((( 2.2 * diff[6 * i + 6 * diff.size(1) * 2] +
      static_cast<double>(C1) * Local_Average[2]) + static_cast<double>(C2) *
                          Local_Average[5]) + C3 * Local_Average[8]) / d;
  } else {
    i = static_cast<int>(c) - 1;
    Local_Average[9] = diff[6 * i];
    Local_Average[10] = diff[6 * i + 6 * diff.size(1)];
    Local_Average[11] = diff[6 * i + 6 * diff.size(1) * 2];
  }

  C1 = 0;
  C2 = 0;
  C3 = 0.0;
  if ((Eig_Accel[0] != 0.0) || (Eig_Accel[1] != 0.0) || (Eig_Accel[2] != 0.0)) {
    if (Eig_Accel[0] != 0.0) {
      Local_Average_Accel[0] /= Eig_Accel[0];
      Local_Average_Accel[1] /= Eig_Accel[0];
      Local_Average_Accel[2] /= Eig_Accel[0];
      C1 = 3;
    }

    if (Eig_Accel[1] != 0.0) {
      Local_Average_Accel[3] /= Eig_Accel[1];
      Local_Average_Accel[4] /= Eig_Accel[1];
      Local_Average_Accel[5] /= Eig_Accel[1];
      C2 = 3;
    }

    if (Eig_Accel[2] != 0.0) {
      Local_Average_Accel[6] = Local_Average_Accel[3] / Eig_Accel[2];
      Local_Average_Accel[7] = Local_Average_Accel[4] / Eig_Accel[2];
      Local_Average_Accel[8] = Local_Average_Accel[5] / Eig_Accel[2];
      C3 = 0.75;
    }

    i = static_cast<int>(c) - 1;
    d = (static_cast<double>(C1 + C2) + C3) +  2.2;
    Local_Average_Accel[9] = ((( 2.2 * diff2[6 * i] + static_cast<double>(C1) *
      Local_Average_Accel[0]) + static_cast<double>(C2) * Local_Average_Accel[3])
      + C3 * Local_Average_Accel[6]) / d;
    Local_Average_Accel[10] = ((( 2.2 * diff2[6 * i + 6 * diff2.size(1)] +
      static_cast<double>(C1) * Local_Average_Accel[1]) + static_cast<double>(C2)
      * Local_Average_Accel[4]) + C3 * Local_Average_Accel[7]) / d;
    Local_Average_Accel[11] = ((( 2.2 * diff2[6 * i + 6 * diff2.size(1) * 2] +
      static_cast<double>(C1) * Local_Average_Accel[2]) + static_cast<double>(C2)
      * Local_Average_Accel[5]) + C3 * Local_Average_Accel[8]) / d;
  } else {
    i = static_cast<int>(c) - 1;
    Local_Average_Accel[9] =  diff2[6 * i];
    Local_Average_Accel[10] = diff2[6 * i + 6 * diff2.size(1)];
    Local_Average_Accel[11] = diff2[6 * i + 6 * diff2.size(1) * 2];
  }
}

