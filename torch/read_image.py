from __future__ import print_function, division
import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import cv2

img_path='/Users/yin.yang/STB-ENS-PTV-C++/data/les3900rect/part010000_maxt0015_fct00010_bkg0.0_dtobs0.50/cam0/'

img_name=img_path+'cam0_it001.png'

img1=cv2.imread(img_name)

rows,cols,channels=img1.shape
print('{0}, {1}, {2}'.format(rows, cols, channels))

# cv2.imshow('ImageWindow',img1)
# cv2.waitKey()