#include <torch/torch.h>

#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <memory>
#include "boost/format.hpp"

#include <Eigen/Core>
#include "opencv2/core.hpp"

#include "../src/ENS.hpp"
#include "../src/Particle.hpp"
#include "../src/Camera.hpp"
#include "../src/Observation.hpp"
#include "../src/DynModel.hpp"
#include "../src/Driver.hpp"
#include "../src/LAVISION.hpp"

using std::cout;
using std::endl;

using Eigen::MatrixXd;
using Eigen::VectorXd;

using Clock=std::chrono::high_resolution_clock;

class torch_Driver : public Driver
{
	private:

	public:
		torch_Driver():Driver(){
			method_str_="Torch";
		}

        void initializeObject(void) final {

		    for(int i_cam=0; i_cam<n_cam_; i_cam++)
		        cam_.push_back( std::make_unique<Camera>( opencv_mat_type_, model_type_, n_cam_, i_cam, it_deb_, it_fct_.at(0), noise_ratio_ ) );

		    for(const auto& ic : cam_){
		        ic->readParamsYaml(imgDir_, true);
		        ic->readRecord(it_deb_, imgDir_);
		    }
		}

		struct testNet : torch::nn::Module{
			testNet(){
				// fc1=register_module();
			}

			torch::Tensor forward(torch::Tensor x){
				auto y=x.pow(2);

				// auto x_a=x.accessor<float,1>();

				// auto xx=cv::Point3d(x_a[0], x_a[1], x_a[2]);

				// auto y=torch::tensor({x_a[0], x_a[1], x_a[2]});

				return y;
			}

			// torch::nn::Linear fc1{nullptr};
		};

		void run(void) final {
			// cout<<"Check image record at time level "<<it_fct_[0]<<endl;
		    // cout<<"cam_[0]->Irec()[0].size() "<<endl
		    	// <<cam_[0]->Irec()[0].size()<<endl;

		 //    cv::Mat img_roi=cam_[0]->Irec()[0](cv::Rect(897, 407, 6, 6));

			// cv::Mat img_roi_f;
			// img_roi.convertTo(img_roi_f, CV_32FC1);

		 //    cout<<"img_roi_f"<<endl
		 //    	<<img_roi_f<<endl;

		 //    cout<<"Check torch module"<<endl;

		 //    torch::Tensor img_roi_torch=torch::from_blob(img_roi_f.data, {img_roi_f.rows, img_roi_f.cols}, torch::kFloat);

		 //    cout<<"img_roi_torch"<<endl
		 //    	<<img_roi_torch<<endl;

		 //    int 	input_channels=1;
		 //    int 	output_channels=5;
		 //    int 	kernel_size=3;
		 //    double  sigma=1.;

		 //    auto x_cord=torch::arange(kernel_size);
		 //    cout<<"x_cord"<<endl
		 //    	<<x_cord<<endl;

		 //    auto x_grid=x_cord.repeat(kernel_size);
		 //    x_grid=x_grid.view({kernel_size, kernel_size});
		 //    cout<<"x_grid"<<endl
		 //    	<<x_grid<<endl;

		 //    auto y_grid=x_grid.t();
		 //    auto xy_grid=torch::stack({x_grid, y_grid}, -1); //-1 means add another dimension at the end ( 15, 15 )
		 //    // cout<<"xy_grid"<<endl																	//^0  ^1 ^-1
		 //    // 	<<xy_grid<<endl;

		 //    double mean=(kernel_size-1)/2;
		 //    double variance=std::pow(sigma,2);

		 //    auto gaussian_kernel=(1./(2.*M_PI*variance))*torch::exp(-torch::sum((xy_grid-mean).pow(2), -1)/(2*variance));

		 //    gaussian_kernel=gaussian_kernel/torch::sum(gaussian_kernel);

		 //    gaussian_kernel=gaussian_kernel.view({1, 1, kernel_size, kernel_size});
		 //    gaussian_kernel=gaussian_kernel.repeat({output_channels, input_channels, 1, 1});

		 //    cout<<"gaussian_kernel"<<endl
		 //    	<<gaussian_kernel<<endl;

	  //       auto gaussian_filter = torch::nn::Conv2d(torch::nn::Conv2dOptions(input_channels, output_channels, kernel_size)
	  //       										.padding(0));
	  //       gaussian_filter->weight=gaussian_kernel;
	  //       cout<<"gaussian_filter->weight "<<endl
	  //       	<<gaussian_filter->weight<<endl;

	  //       img_roi_torch=img_roi_torch.view({1, 1, img_roi_torch.size(0), img_roi_torch.size(1)});

	  //       auto output = gaussian_filter->forward( img_roi_torch );

	  //       cout<<"output"<<endl
	  //       	<<output<<endl;

	        auto net=std::make_unique<testNet>();

	        torch::Tensor input_x = torch::randn({1}, torch::requires_grad(true));
	        // torch::Tensor input_x = torch::tensor(at::ArrayRef<double>({1., 1., 1.}), torch::requires_grad(true));

	        cout<<"input_x "<<endl<<input_x<<endl;

	        // torch::optim::SGD optimizer(net->parameters(), 0.01);

         //  	optimizer.zero_grad();

  	      //   torch::Tensor prediction = net->forward( input_x );

	        // cout<<"prediction "<<endl<<prediction<<endl;

  	      //   cout<<"prediction assert "<<endl<<input_x.pow(2)<<endl;

  	      //   torch::Tensor target = torch::randn({3});

	        // cout<<"target "<<endl<<target<<endl;

  	      //   torch::Tensor loss = torch::mse_loss(prediction, target);

	        // cout<<"loss "<<endl<<loss.item<float>()<<endl;

	        // cout<<"loss assert "<<endl<<(prediction-target).pow(2).sum()/prediction.size(0)<<endl;

  	      //   loss.backward();

  	      //   cout<<"gradient wrt input_x "<<endl<<input_x.grad()<<endl;

	        // cout<<"gradient wrt input_x assert "<<endl<<2.*(prediction-target)*2.*input_x/prediction.size(0)<<endl;

	        // cv::Point3d Xc=cv::Point3d(1., 1., 1.);
	        
	        // torch::Tensor Xc_tensor=torch::tensor({Xc.x, Xc.y, Xc.z}, torch::requires_grad(true).dtype(torch::kFloat64));
	        // Xc_tensor=Xc_tensor.view({3});
	        // cout<<"Xc_tensor"<<endl<<Xc_tensor<<endl;

	        // torch::Tensor grad=torch::zeros({3}, torch::dtype(torch::kFloat64));

	        // auto xc=cam_[0]->mappingFunction(Xc, grad);
		}
};

int main(){
	std::unique_ptr<torch_Driver> torch_driver=std::make_unique<torch_Driver>();

	torch_driver->printPara();

	torch_driver->initializeObject();

	auto t_deb=Clock::now();
	torch_driver->run();
	auto t_fin=Clock::now();
	cout<<"CPU time of torch module: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
	
	return 0;
}