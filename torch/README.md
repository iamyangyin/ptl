#How to build with torch: deep learning framework by facebook

in current directory either

- mkdir build

- cd build

- cmake -DCMAKE_PREFIX_PATH=/anaconda3/lib/python3.7/site-packages/torch ..

- make

or

- python firecat.py *casename*
