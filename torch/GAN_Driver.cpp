#include <torch/torch.h>

#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <memory>
#include "boost/format.hpp"

#include <Eigen/Core>
#include "opencv2/core.hpp"

#include "../src/ENS.hpp"
#include "../src/Particle.hpp"
#include "../src/Camera.hpp"
#include "../src/Observation.hpp"
#include "../src/DynModel.hpp"
#include "../src/Driver.hpp"
#include "../src/LAVISION.hpp"

using std::cout;
using std::endl;

using Eigen::MatrixXd;
using Eigen::VectorXd;

using Clock=std::chrono::high_resolution_clock;

class GAN_Driver : public Driver
{
    private:
        OptMethod::Method_Type          method_type_;

        std::unique_ptr<KLPT>           klpt_;

    public:
        GAN_Driver():Driver(){
            method_str_="Torch";

            method_type_=static_cast<OptMethod::Method_Type>(std::stoi(para_map_["opt_method_type"]));

            this->loadPTVParams();
            this->loadIPRParams();
            this->loadSTBParams();
            this->loadENSParams();
        }

        void printPara(void) final {
            Driver::printPara();
            cout<<std::boolalpha;
            cout<<str(boost::format("Ensemble number              is %1$3d.") % n_ens_)<<endl;
        }

        void initializeObject(void) final {
            Driver::initializeObject();
            klpt_=std::make_unique<KLPT>();

            klpt_->setoptmethod( method_type_, control_vector_type_, intensity_control_flag_,
                                obs_->particles_bkg().size(), n_part_per_snapshot_, Xc_top_, Xc_bottom_, 
                                pixel_lost_, dtObs_, 
                                dXc_px_, resDir_, eval_window,
                                warm_start_flag_, warm_start_max_iter_, warm_start_pixel_, warm_start_relaxfactor_,
                                n_ens_, regul_const_, outer_max_iter_, local_analysis_flag_, 
                                resample_flag_, sigmaXcRatio_, sigmaE_, joint_intensity_flag_ );
        }

        void initializeRun(void) final {
        }

        void generator(const int& it_seq){
        }

        void saveResults(int it_seq) final {
            if(save_result_){
                bool overwrite_result_file_flag = (it_seq==it_fct_.front());
                optmethod_->saveGlobalResidualAndMeanError(it_seq, resdir_, overwrite_result_file_flag);
                
                for(auto ic : cam_)
                    ic->saveImageResidual(it_seq, "Iresf", resdir_);
            }
        }

        void clearForNextCycle(void) final {
            optmethod_->clear();
            particles_fct_.clear();
            particles_ana_.clear();
            particles_ens_fct_.clear();
            particles_ens_fct_.shrink_to_fit();
        }

        void run(void) final {
            torch::Device device(torch::kCPU);

            torch::nn::Sequential discriminator(
                // Layer 1
                torch::nn::Conv2d(
                      torch::nn::Conv2dOptions(1, 16, 4).stride(1).padding(0).with_bias(false)),
                torch::nn::Functional(torch::leaky_relu, 0.2),
                // Layer 2
                torch::nn::Conv2d(
                      torch::nn::Conv2dOptions(16, 1, 3).stride(1).padding(0).with_bias(false)),
                torch::nn::Functional(torch::sigmoid)
                );
            discriminator->to(device);

            torch::optim::Adam discriminator_optimizer(
                discriminator->parameters(), torch::optim::AdamOptions(2e-4).beta1(0.5));

            for(auto pt_seq=it_fct_.begin(); pt_seq<it_fct_.end(); ++pt_seq){
                //Run generator
                this->generator(*pt_seq);

                // Prepare real image
                auto it_rec=*pt_seq-it_fct_.front();
                cv::Mat img_real=cam_[0]->Irec()[it_rec];

                Eigen::Vector4i pos_real, pos_overlap_real, pos_n, pos_overlap_n;

                int it_img=0;
                for(auto po: optmethod_->particles_ens_ana().back()){
                    if(po->part_index()==0){

                        auto pos_real=po->imgPos().at(0).at(it_img);

                        auto sub_window=cv::Rect(pos_real(0), pos_real(1), pos_real(2), pos_real(3));

                        // Prepare real image
                        cv::Mat img_real_roi=img_real(sub_window);
                        cv::Mat img_real_roi_f;
                        img_real_roi.convertTo(img_real_roi_f, CV_32FC1);
                        torch::Tensor img_real_roi_torch=torch::from_blob(img_real_roi_f.data, {img_real_roi_f.rows, img_real_roi_f.cols}, torch::kFloat);

                        img_real_roi_torch=img_real_roi_torch.view({1, 1, img_real_roi_torch.size(0), img_real_roi_torch.size(1)});

                        auto real_images=img_real_roi_torch.to(device);

                        cout<<"real_images"<<endl
                            <<real_images<<endl;

                        auto real_datasets=torch::data::datasets::TensorDataset(real_images);
                      
                        cout<<"real_datasets.size()"<<endl
                            <<real_datasets.size().value()<<endl;

                        auto real_labels = torch::ones(1, device);

                        cout<<"real_labels"<<endl
                            <<real_labels<<endl;

                        // Train discriminator with real images.
                        discriminator->zero_grad();
                        torch::Tensor real_output = discriminator->forward(real_images);

                        cout<<"real_output"<<endl
                            <<real_output<<endl;

                        torch::Tensor d_loss_real = torch::binary_cross_entropy(real_output, real_labels);
                        d_loss_real.backward();

                        cout<<str(boost::format("D_loss_real: %1$.4f.") % d_loss_real.item<float>() )<<endl;

                        // // Prepare fake image
                        cv::Mat imgnOnRealBigCanvas, imgn;
                        imgnOnRealBigCanvas=cv::Mat::zeros(img_real_roi_f.size(), CV_32FC1);

                        torch::Tensor fake_images;

                        for(int n=0; n<n_ens_; n++){
                            pos_n=optmethod_->particles_ens_ana().at(n).at(po->part_index())->imgPos().at(0).at(it_img);
                            optmethod_->particles_ens_ana().at(n).at(po->part_index())->img().at(0).at(it_img).convertTo(imgn, CV_32FC1);
                            
                            if( std::abs(pos_n(1)-pos_real(1))<std::max(pos_real(3), pos_n(3)) && std::abs(pos_n(0)-pos_real(0))<std::max(pos_real(2), pos_n(2)) ){

                                optmethod_->getPositionAndSizeOfOverlapedAreaOfTwoLocalPatches(pos_n, pos_real, pos_overlap_n, pos_overlap_real);

                                imgnOnRealBigCanvas=cv::Mat::zeros(img_real_roi_f.size(), CV_32FC1);

                                if(pos_overlap_n(3)!=0 && pos_overlap_n(2)!=0)
                                    imgn(cv::Rect(pos_overlap_n(0), pos_overlap_n(1), pos_overlap_n(2), pos_overlap_n(3)))
                                        .copyTo( imgnOnRealBigCanvas(cv::Rect(pos_overlap_real(0), pos_overlap_real(1), pos_overlap_real(2), pos_overlap_real(3))) );
                            
                                torch::Tensor img_n_on_real_torch=torch::from_blob(imgnOnRealBigCanvas.data, {imgnOnRealBigCanvas.rows, imgnOnRealBigCanvas.cols}, torch::kFloat);
                                img_n_on_real_torch=img_n_on_real_torch.view({1, 1, img_n_on_real_torch.size(0), img_n_on_real_torch.size(1)});

                                if(n==0)
                                    fake_images=img_n_on_real_torch;
                                else
                                    fake_images=torch::cat({fake_images, img_n_on_real_torch}, 0);
                            }
                        }

                        cout<<"fake_images"<<endl
                            <<fake_images<<endl;

                        torch::Tensor fake_labels = torch::zeros(n_ens_, device);

                        cout<<"fake_labels"<<endl
                            <<fake_labels<<endl;

                        // Train discriminator with fake images.
                        torch::Tensor fake_output = discriminator->forward(fake_images);
                       
                        cout<<"fake_output"<<endl
                            <<fake_output<<endl;

                        torch::Tensor d_loss_fake = torch::binary_cross_entropy(fake_output, fake_labels);
                        d_loss_fake.backward();

                        cout<<str(boost::format("D_loss_fake: %1$.4f.") % d_loss_fake.item<float>() )<<endl;

                        torch::Tensor d_loss = d_loss_real + d_loss_fake;
                        discriminator_optimizer.step();

                        cout<<str(boost::format("D_loss: %1$.4f.") % d_loss.item<float>() )<<endl;
                    }
                }
            }
        }
};

int main(){
    std::shared_ptr<GAN_Driver> gan_driver=std::make_shared<GAN_Driver>();

    gan_driver->initializePara();

    gan_driver->printPara();

    gan_driver->initializeObject();

    gan_driver->initializeRun();

    auto t_deb=Clock::now();
    gan_driver->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of torch module: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    
    return 0;
}