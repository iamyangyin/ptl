import torch
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

# input = torch.arange(1, 5, dtype=torch.float32).view(1, 1, 2, 2)
# print(input)
# m = torch.nn.Upsample(scale_factor=2, mode='bilinear')
# output = m(input)
# print(output)

result_dir='/media/yin/Data/stb-ens-ptv-c/result/les3900rect/part030000_maxt0050_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/LAPIVdebug_avg_overlap'
velo_file=result_dir+'/velo/Velocity_ana_001_07_raw_overlap.txt'

f=np.loadtxt(velo_file)
u=f[:,3]
v=f[:,4]
w=f[:,5]

nx=75
ny=25
nz=11

xx, yy, zz = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))
print('xx.shape {}'.format(xx.shape))

UU = np.zeros( xx.shape )
VV = np.zeros( xx.shape )
WW = np.zeros( xx.shape )

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			UU[j,i,k]=u[i+j*nx+k*nx*ny]
			VV[j,i,k]=v[i+j*nx+k*nx*ny]
			WW[j,i,k]=w[i+j*nx+k*nx*ny]

print('UU.shape {}'.format(UU.shape))

upsmpl_functor = torch.nn.Upsample(scale_factor=2, mode='trilinear', align_corners=False)

UU_torch = torch.from_numpy(UU)
UU_torch = UU_torch.view(1, 1, UU_torch.size(0), UU_torch.size(1), UU_torch.size(2))
print(UU_torch.shape)

UU_out = upsmpl_functor(UU_torch)
print(UU_out.shape)

UU_upsmpl = UU_out.numpy();
UU_upsmpl = np.squeeze(UU_upsmpl)
print(UU_upsmpl.shape)

VV_torch = torch.from_numpy(VV)
VV_torch = VV_torch.view(1, 1, VV_torch.size(0), VV_torch.size(1), VV_torch.size(2))
VV_out = upsmpl_functor(VV_torch)
VV_upsmpl = VV_out.numpy();
VV_upsmpl = np.squeeze(VV_upsmpl)
print(VV_upsmpl.shape)

WW_torch = torch.from_numpy(WW)
WW_torch = WW_torch.view(1, 1, WW_torch.size(0), WW_torch.size(1), WW_torch.size(2))
WW_out = upsmpl_functor(WW_torch)
WW_upsmpl = WW_out.numpy();
WW_upsmpl = np.squeeze(WW_upsmpl)
print(WW_upsmpl.shape)

[ny_smpl, nx_smpl, nz_smpl] = UU_upsmpl.shape

pi=3.1415
D=12
Xc_topX=72.5
Xc_topY=22.5
Xc_topZ=11.78098
Xc_bottomX=0
Xc_bottomY=0
Xc_bottomZ=0

dx=(Xc_topX-Xc_bottomX)/(nx_smpl-1)
dy=(Xc_topY-Xc_bottomY)/(ny_smpl-1)
dz=(Xc_topZ-Xc_bottomZ)/(nz_smpl-1)

velo_upsmpl_file=result_dir+'/velo/Velocity_upsmpl_001_07.txt'
with open(velo_upsmpl_file, "w") as the_file:
	the_file.write("#X Y Z NX NY NZ V_X V_Y V_Z\n")
	for k in np.arange(0, nz_smpl, 1):
		for j in np.arange(0, ny_smpl, 1):
			for i in np.arange(0, nx_smpl, 1):
				ln=i+j*nx+k*nx*ny
				X=dx*i
				Y=dy*j
				Z=dz*k
				line="{0:1f} {1:1f} {2:1f} {3:3d} {4:3d} {5:3d} {6:15.15f} {7:15.15f} {8:15.15f}".format(X,Y,Z,i,j,k,UU_upsmpl[j,i,k],VV_upsmpl[j,i,k],WW_upsmpl[j,i,k])
				the_file.write(line+'\n')

# print(UU[:,0,0])
# print(UU_upsmpl[:,0,0])

# fig=plt.figure()
# cmap = plt.get_cmap('hot')
# fig.subplots_adjust(left=0.1,right=0.95,hspace=0.6,wspace=0.25)

# plt.subplot(2,1,1)
# plt.pcolormesh(xx[:,:,1], yy[:,:,1], UU[:,:,1], cmap=cmap)
# plt.title('U')
# # plt.axis('equal')
# plt.axis([0, nx, 0, ny])
# plt.grid(True)
# plt.colorbar()

# xx_smpl, yy_smpl, zz_smpl = np.meshgrid(np.arange(0, nx_smpl, 1), np.arange(0, ny_smpl, 1), np.arange(0, nz_smpl, 1))
# print('xx_smpl.shape {}'.format(xx_smpl.shape))

# plt.subplot(2,1,2)
# plt.pcolormesh(xx_smpl[:,:,1], yy_smpl[:,:,1], UU_upsmpl[:,:,4], cmap=cmap)
# plt.title('V')
# # plt.axis('equal')
# plt.axis([0, nx_smpl, 0, ny_smpl])
# plt.grid(True)
# plt.colorbar()

# plt.subplot(3,1,3)
# plt.pcolormesh(xx[:,:,1], yy[:,:,1], WW[:,:,1], cmap=cmap)
# plt.title('W')
# # plt.axis('equal')
# plt.axis([0, nx, 0, ny])
# plt.grid(True)
# plt.colorbar()

plt.show()