import numpy as np
import os

root_path=os.environ['STBENSPTV_PATH']

num_part='000000'

data_dir=root_path+'/data/lavision/part'+num_part+'_maxt0050_fct00005_bkg0.0_dtobs0.00/txtimg/'

print(data_dir)

for i in range(1,6):

	for j in range (4):
		f=np.loadtxt(data_dir+'img_file0000'+str(i)+'cam'+str(j)+'.txt',skiprows=1)

		print('t {0} cam {1} max {2}'.format(i,j,np.amax(f)))  
