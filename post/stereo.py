import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

path="/media/yin/Data/stb-ens-ptv-c/data/les3900rect/part000100_maxt0050_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/cam"

ic1='0'
ic2='1'

imgL = cv.imread(path+ic1+'/cam_c'+ic1+'_it001.png',-1)
imgR = cv.imread(path+ic2+'/cam_c'+ic2+'_it001.png',-1)

imgL8 = cv.convertScaleAbs(imgL, alpha=1./256.)
imgR8 = cv.convertScaleAbs(imgR, alpha=1./256.)

stereo = cv.StereoBM_create(numDisparities=16, blockSize=15)

disparity = stereo.compute(imgL8,imgR8)
plt.imshow(disparity,'gray')
plt.show()

cv.imwrite('disparity.png',disparity)