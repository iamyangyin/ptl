import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
import argparse

class log_results:
	def __init__(self, num_tracks, track_length):
		self.num_tracks=num_tracks
		self.track_length=track_length

def read_log_file(log_path):

	num_tracks=[]
	track_length=[]

	with open(log_path) as fp:
		for line in fp:
			if line.startswith("Snapshot"):
				lnstr=line.split(":")
				tgt=lnstr[1].strip()
				num_tracks.append(int(tgt))

			if line.startswith("Track_length"):
				lnstr=line.split(":")
				tgt=lnstr[1].strip()
				track_length.append(int(tgt))

	return log_results(num_tracks, track_length)


root_path=os.environ['STBENSPTV_PATH']

res_1 = read_log_file(root_path+'/build/output_lptchal_10000')
res_2 = read_log_file(root_path+'/build/output_lptchal_50000')
res_3 = read_log_file(root_path+'/build/output_lptchal_100000')
res_4 = read_log_file(root_path+'/build/output_lptchal_160000')

maxt=50
itcyc=np.arange(1,maxt+1)

linewidth=3
markersize=20
textfont=24
titlefont=40
tickfont=24
legendfont=15

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)    # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(12,9))

# np_res_1 = np.array(res_1.num_tracks)
# np_res_1 = np.array(res_2.num_tracks)
# np_res_1 = np.array(res_3.num_tracks)
# np_res_1 = np.array(res_4.num_tracks)

# title='Total number of tracks by KLPT for PPP 0.08'

# line0,=plt.plot(itcyc,np_res_1,'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)
# # line0.set_label(r'PPP 0.005')
# # line0.set_label(r'PPP 0.025')
# # line0.set_label(r'PPP 0.05')
# line0.set_label(r'PPP 0.08')

# ax=plt.subplot(111)
# ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
# box=ax.get_position()
# ax.set_position([box.x0, box.y0, box.width, box.height])
# # # ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

# # plt.legend(bbox_to_anchor=(1, 0.8),loc='upper right',prop={'size':legendfont})
# plt.legend(bbox_to_anchor=(1, 0),loc='lower right',prop={'size':legendfont})

# plt.autoscale(enable=True, axis='x', tight=True)

# plt.xlabel('Snapshots')
# plt.ylabel('Total number')
# plt.title(title)

# np_res_1 = np.array(res_1.track_length)
# np_res_1 = np.array(res_2.track_length)
# np_res_1 = np.array(res_3.track_length)
np_res_1 = np.array(res_4.track_length)

print(np.sum(np_res_1[4:]))

# title='Track length statistics for case PPP 0.08'

# line0,=plt.plot(itcyc,np_res_1,'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)
# # line0.set_label(r'PPP 0.005')
# # line0.set_label(r'PPP 0.025')
# # line0.set_label(r'PPP 0.05')
# line0.set_label(r'PPP 0.08')

# ax=plt.subplot(111)
# ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
# box=ax.get_position()
# ax.set_position([box.x0, box.y0, box.width, box.height])
# # ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

# # plt.legend(bbox_to_anchor=(1, 0.8),loc='upper right',prop={'size':legendfont})
# plt.legend(bbox_to_anchor=(0, 1),loc='upper left',prop={'size':legendfont})

# plt.autoscale(enable=True, axis='x', tight=True)

# plt.xlabel('Track length')
# plt.ylabel('Number of tracks')
# plt.title(title)

# #############################################################################
# res_filename='/home/yin/Desktop/num.eps'
# plt.savefig(res_filename, format='eps', dpi=1500, bbox_inches='tight')
# plt.show()