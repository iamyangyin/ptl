import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter

def readResult(result_path, ncyc, plot_init_err_flag):
	f=np.loadtxt(result_path)

	it=f[:,0]
	err_x=f[:,5]
	err_y=f[:,6]

	iter=int(it.size/ncyc)
	errpx=[]

	if plot_init_err_flag:
		errpx_init=0.0
		for i in np.arange(ncyc):
			errpx_init=errpx_init+np.sqrt(np.square(err_x[i*iter+1])+np.square(err_y[i*iter+1]))

		errpx.append(errpx_init/ncyc)

	for i in np.arange(ncyc):
		errpx.append(np.sqrt(np.square(err_x[(i+1)*iter-1])+np.square(err_y[(i+1)*iter-1])))

	return errpx

def readLostParticleNumbers(result_path, n_part):
	f=np.loadtxt(result_path)

	it=f[:,0]
	num_lost=f[:,9]

	iter=int(it.size/ncyc)
	frac_lostpart=[]

	for i in np.arange(ncyc):
		frac_lostpart.append(num_lost[i*iter+11]/float(n_part))

	return frac_lostpart

def readCPUTime(result_path,ncyc):

	cputime=0
	with open(result_path) as f:
		for line in f:
			if 'CPU time of reconstruction with' in line:
				lineword=line.split()
				cputime=lineword[-2]

	return float(cputime)/(float(ncyc)*1000)

ncyc=41
snapshots='50' 
suffix=''
compare_flag=False
plot_init_err_flag=False

case_name='les3900rect'
dtobs=0.25

if dtobs==0.05:
	compare_flag=True

dtobs_n=str('{0:.2f}'.format(dtobs))

part=[]
n_part=[]

if case_name=='les3900':
	part.append('80000')
	part.append('64000')
	part.append('48000')
	part.append('32000')
	part.append('16000')
	part.append('06400')
	n_part.append(80000)
	n_part.append(64000)
	n_part.append(48000)
	n_part.append(32000)
	n_part.append(16000)
	n_part.append(6400)
elif case_name=='dns300':
	part.append('50000')
	part.append('42000')
	part.append('31500')
	part.append('21000')
	part.append('10500')
	part.append('04200')
elif case_name=='les3900rect':
	if dtobs==0.05:
		part.append('80000')
		part.append('60000')
		part.append('50000')
		part.append('40000')
		part.append('30000')
		part.append('20000')
		part.append('10000')
		n_part.append(80000)
		n_part.append(60000)
		n_part.append(50000)
		n_part.append(40000)
		n_part.append(30000)
		n_part.append(20000)
		n_part.append(10000)
	elif dtobs==0.25:
		part.append('60000')
		part.append('40000')
		part.append('10000')
		n_part.append(60000)
		n_part.append(40000)
		n_part.append(10000)

res_path=[]
errpx_stb=[]
errpx_ens=[]

frac_lostpart_stb=[]
frac_lostpart_ens=[]

cpustb=[]
cpuens=[]

n_part_i=0
for ip in part:
	res_path='../result/'+case_name+'/part0'+ip+'_maxt00'+snapshots+'_fct00010_psf1_dtobs'+dtobs_n+suffix

	if dtobs==0.05:
		if ip == '80000':
			px='debug_newk'
		elif ip=='60000':
			px='debug_newk'
		elif ip=='50000':
			px='debug_newk'
		elif ip=='40000':
			px='debug_newk'
		elif ip=='30000':
			px='debug_newk'
		elif ip=='20000':
			px='debug_newk'
		elif ip=='10000':
			px='debug_newk'
	elif dtobs==0.25:
		if ip == '60000':
			px='debug_newk'
		elif ip=='40000':
			px='debug_newk'
		elif ip=='10000':
			px='debug_newk'

	errpx_stb.append(readResult(res_path+'/STB'+px+'/result.txt',ncyc,plot_init_err_flag))

	frac_lostpart_stb.append(readLostParticleNumbers(res_path+'/STB'+px+'/result.txt', n_part[n_part_i]))

	# if dtobs==0.1:
	# 	cpustb.append(readCPUTime(res_path+'/STB'+px+'/output_'+case_name+'_'+ip+'_stb_'+str(dtobs)+'_v'+px,ncyc))

	if dtobs==0.05:
		if ip == '80000':
			px='debug_newk'
		elif ip=='60000':
			px='debug_newk'
		elif ip=='50000':
			px='debug_newk'
		elif ip=='40000':
			px='debug_newk'
		elif ip=='30000':
			px='debug_newk'
		elif ip=='20000':
			px='debug_newk'
		elif ip=='10000':
			px='debug_newk'
	elif dtobs==0.25:
		if ip == '60000':
			px='debug_newk'
		elif ip=='40000':
			px='debug_newk'
		elif ip=='10000':
			px='debug_newk'

	errpx_ens.append(readResult(res_path+'/ENS'+px+'/result.txt',ncyc,plot_init_err_flag))

	frac_lostpart_ens.append(readLostParticleNumbers(res_path+'/ENS'+px+'/result.txt', n_part[n_part_i]))

	# if dtobs==0.1:
	# 	cpuens.append(readCPUTime(res_path+'/ENS'+px+'/output_'+case_name+'_'+ip+'_ens_'+str(dtobs)+'_v'+px,ncyc))

	n_part_i+=1

if plot_init_err_flag:
	itcyc=np.arange(10-1,10+ncyc)
else:
	itcyc=np.arange(10,10+ncyc)

linewidth=4
markersize=15
textfont=24
titlefont=40
tickfont=24
legendfont=24

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)    # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(12,9))

#dtobs=0.05
# line0,=plt.semilogy(itcyc,errpx_stb[0],'k-',lw=linewidth)
# line1,=plt.semilogy(itcyc,errpx_stb[1],'r-',lw=linewidth)
# line2,=plt.semilogy(itcyc,errpx_stb[2],'m-',lw=linewidth)
# line3,=plt.semilogy(itcyc,errpx_stb[3],'b-',lw=linewidth)
# line4,=plt.semilogy(itcyc,errpx_stb[4],'g-',lw=linewidth)
# line5,=plt.semilogy(itcyc,errpx_stb[5],'c-',lw=linewidth)
# line6,=plt.semilogy(itcyc,errpx_stb[6],'y-',lw=linewidth)

# line00,=plt.semilogy(itcyc,errpx_ens[0],'k--',lw=linewidth)
# line11,=plt.semilogy(itcyc,errpx_ens[1],'r--',lw=linewidth)
# line22,=plt.semilogy(itcyc,errpx_ens[2],'m--',lw=linewidth)
# line33,=plt.semilogy(itcyc,errpx_ens[3],'b--',lw=linewidth)
# line44,=plt.semilogy(itcyc,errpx_ens[4],'g--',lw=linewidth)
# line55,=plt.semilogy(itcyc,errpx_ens[5],'c--',lw=linewidth)
# line66,=plt.semilogy(itcyc,errpx_ens[6],'y--',lw=linewidth)

# line0.set_label('STB, 0.2')
# line1.set_label('STB, 0.15')
# line2.set_label('STB, 0.125')
# line3.set_label('STB, 0.1')
# line4.set_label('STB, 0.075')
# line5.set_label('STB, 0.05')
# line6.set_label('STB, 0.025')

# line00.set_label('Ours, 0.2')
# line11.set_label('Ours, 0.15')
# line22.set_label('Ours, 0.125')
# line33.set_label('Ours, 0.1')
# line44.set_label('Ours, 0.075')
# line55.set_label('Ours, 0.05')
# line66.set_label('Ours, 0.025')

#dtobs=0.25
line0,=plt.semilogy(itcyc,errpx_stb[0],'r-',lw=linewidth)
line1,=plt.semilogy(itcyc,errpx_stb[1],'b-',lw=linewidth)
line2,=plt.semilogy(itcyc,errpx_stb[2],'c-',lw=linewidth)

line00,=plt.semilogy(itcyc,errpx_ens[0],'r--',lw=linewidth)
line11,=plt.semilogy(itcyc,errpx_ens[1],'b--',lw=linewidth)
line22,=plt.semilogy(itcyc,errpx_ens[2],'c--',lw=linewidth)

line0.set_label('STB, 0.15')
line1.set_label('STB, 0.1')
line2.set_label('STB, 0.025')

line00.set_label('Ours, 0.15')
line11.set_label('Ours, 0.1')
line22.set_label('Ours, 0.025')

ax=plt.subplot(111)
ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
box=ax.get_position()
# ax.set_position([box.x0, box.y0, box.width, box.height])
ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

# plt.legend(bbox_to_anchor=(0, 1),loc='upper right',prop={'size':legendfont})
plt.legend(bbox_to_anchor=(1, 1),loc='upper left',prop={'size':legendfont})

plt.autoscale(enable=True, axis='x', tight=True)

plt.xlabel('Snapshots')
plt.ylabel('Pixel')
plt.title('Mean Positional Error of $\Delta T_{obs}$='+str(dtobs))
res_filename='/Users/yin.yang/Documents/err_res_snap.eps'
plt.savefig(res_filename, formabt='eps', dpi=1500)
plt.show()

#plot error at last snapshots in terms of different ppp
errpx_stb_last=[]
for i in reversed(errpx_stb):
	errpx_stb_last.append(i[-1])

errpx_ens_last=[]
for i in reversed(errpx_ens):
	errpx_ens_last.append(i[-1])

itppp=[0.025,0.05,0.075,0.1,0.125,0.15,0.2]
itppp_bigobs=[0.025,0.1,0.15]

if compare_flag:

	part_bigobs=[]

	part_bigobs.append('60000')
	part_bigobs.append('40000')
	part_bigobs.append('10000')

	errpx_stb_bigobs=[]
	errpx_ens_bigobs=[]

	for ip in part_bigobs:
		res_path='../result/'+case_name+'/part0'+ip+'_maxt00'+snapshots+'_fct00010_psf1_dtobs0.25'+suffix

		if ip == '60000':
			px='debug_newk'
		elif ip=='40000':
			px='debug_newk'
		elif ip=='10000':
			px='debug_newk'

		errpx_stb_bigobs.append(readResult(res_path+'/STB'+px+'/result.txt',ncyc,plot_init_err_flag))

		if ip == '60000':
			px='debug_newk'
		elif ip=='40000':
			px='debug_newk'
		elif ip=='10000':
			px='debug_newk'

		errpx_ens_bigobs.append(readResult(res_path+'/ENS'+px+'/result.txt',ncyc,plot_init_err_flag))

	errpx_stb_bigobs_last=[]
	for i in reversed(errpx_stb_bigobs):
		errpx_stb_bigobs_last.append(i[-1])

	errpx_ens_bigobs_last=[]
	for i in reversed(errpx_ens_bigobs):
		errpx_ens_bigobs_last.append(i[-1])

# fig=plt.figure(figsize=(11,9))
# line0,=plt.semilogy(itppp,errpx_stb_last,'ko-',lw=linewidth, ms=markersize)
# line00,=plt.semilogy(itppp,errpx_ens_last,'ks:',lw=linewidth, ms=markersize)
# if compare_flag:
# 	line1,=plt.semilogy(itppp_bigobs,errpx_stb_bigobs_last,'ro-',lw=linewidth, ms=markersize)
# 	line10,=plt.semilogy(itppp_bigobs,errpx_ens_bigobs_last,'rs:',lw=linewidth, ms=markersize)

# line0.set_label('STB, $\Delta T_{obs}=$'+str(dtobs))
# line00.set_label('Ours, $\Delta T_{obs}=$'+str(dtobs))
# if compare_flag:
# 	line1.set_label('STB, $\Delta T_{obs}=0.25$')
# 	line10.set_label('Ours, $\Delta T_{obs}=0.25$')

# ax=plt.subplot(111)
# ax.set_xticks(itppp)
# ax.xaxis.set_major_formatter(FormatStrFormatter('%.3f'))
# box=ax.get_position()
# ax.set_position([box.x0, box.y0, box.width*1, box.height])

# plt.legend(bbox_to_anchor=(1, 0),loc='lower right',prop={'size':legendfont})

# plt.xlabel('ppp')
# plt.ylabel('Pixel')
# plt.title('Mean Positional Error')

# res_filename='/Users/yin.yang/Documents/err_last_ss.eps'
# plt.savefig(res_filename, formabt='eps', dpi=1500)
# # plt.autoscale(enable=True, axis='x', tight=True)
# plt.show()

#plot number of lost particles
if dtobs==0.25:

	fig=plt.figure(figsize=(12,9))
	line0,=plt.plot(itcyc,frac_lostpart_stb[0],'r-',lw=linewidth)
	line1,=plt.plot(itcyc,frac_lostpart_stb[1],'b-',lw=linewidth)
	line2,=plt.plot(itcyc,frac_lostpart_stb[2],'c-',lw=linewidth)

	line00,=plt.plot(itcyc,frac_lostpart_ens[0],'r:',lw=linewidth)
	line11,=plt.plot(itcyc,frac_lostpart_ens[1],'b:',lw=linewidth)
	line22,=plt.plot(itcyc,frac_lostpart_ens[2],'c:',lw=linewidth)

	line0.set_label('STB, 0.15')
	line1.set_label('STB, 0.1')
	line2.set_label('STB, 0.025')

	line00.set_label('Ours, 0.125')
	line11.set_label('Ours, 0.1')
	line22.set_label('Ours, 0.025')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

	# plt.legend(loc='upper right',prop={'size':legendfont})
	plt.legend(bbox_to_anchor=(1, 1),loc='upper left',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel('$N_{lost}/N_{total}$')
	plt.title('Fraction of Track Lost Particles of $\Delta T_{obs}$='+str(dtobs))

	res_filename='/Users/yin.yang/Documents/num_losttrack_particle.eps'
	plt.savefig(res_filename, formabt='eps', dpi=1500)
	plt.autoscale(enable=True, axis='x', tight=True)
	plt.show()

#plot CPU time
# if dtobs==0.1:
# 	itppp=[0.01,0.025,0.05,0.075,0.1,0.125]

# 	cpustb_inv=[]
# 	for i in reversed(cpustb):
# 		cpustb_inv.append(i)

# 	cpuens_inv=[]
# 	for i in reversed(cpuens):
# 		cpuens_inv.append(i)

# 	fig=plt.figure(figsize=(11,9))
# 	line0,=plt.plot(itppp,cpustb_inv,'ko-',lw=linewidth, ms=markersize)
# 	line00,=plt.plot(itppp,cpuens_inv,'ks--',lw=linewidth, ms=markersize)

# 	line0.set_label('stb')
# 	line00.set_label('ens')

# 	plt.xticks(itppp)
# 	plt.xlabel('ppp')
# 	plt.ylabel('CPU Time/Second')
# 	plt.legend(loc='upper left',prop={'size':legendfont})

# 	plt.title('CPU time per snapshot')
# 	res_filename='/Users/yin.yang/Documents/cpu_time.eps'
# 	plt.savefig(res_filename, formabt='eps', dpi=1500)
# 	# plt.autoscale(enable=True, axis='x', tight=True)
# 	# plt.autoscale(enable=True, axis='y', tight=True)
# 	plt.show()
