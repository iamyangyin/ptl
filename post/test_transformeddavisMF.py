import numpy as np
import xml.etree.ElementTree as ET
import cv2
from davismf import Camera

n_cam=4
cam=[]

for i in range(n_cam):
	cam.append( Camera(i) )

print('Read Calibration file from LAPIV for Davis')
mp_file="/Users/yin.yang/Documents/ForDavis/Properties/Calibration/Calibration.xml"
# mp_file="/Users/yin.yang/Documents/Tomo_PIV_LASIE/Properties/Calibration/Calibration.xml"

mp_tree=ET.parse(mp_file);
root = mp_tree.getroot()

for child in root.iter('CoordinateMapper'):
	if int(child.attrib['CameraIdentifier'])<=n_cam:
		i_cam=int(child.attrib['CameraIdentifier'])-1
		for a in child.iter('LinearScaleX'):
			alpha_X=float(a.attrib['FactorMmPerPixel'])		
			X_offset=float(a.attrib['OffsetMm'])
		for a in child.iter('LinearScaleY'):
			alpha_Y=float(a.attrib['FactorMmPerPixel'])		
			Y_offset=float(a.attrib['OffsetMm'])
		for a in child.iter('LinearScaleZ'):
			alpha_Z=float(a.attrib['FactorMmPerPixel'])		
			Z_offset=float(a.attrib['OffsetMm'])
		cam[i_cam].assign_scale_para(alpha_X, alpha_Y, alpha_Z, X_offset, Y_offset, Z_offset)

		for pm in child.iter('PolynomialMapping'):
			for a in pm.iter('ZPosition'):
				Z0=float(a.attrib['Value'])
			for a in pm.iter('Origin'):
				so=float(a.attrib['s_o'])
				to=float(a.attrib['t_o'])
			for a in pm.iter('NormalisationFactor'):
				nx=float(a.attrib['nx'])
				ny=float(a.attrib['ny'])	
			for a in pm.iter('CoefficientsA'):
				a_o=float(a.attrib['a_o'])
				a_s=float(a.attrib['a_s'])
				a_s2=float(a.attrib['a_s2'])
				a_s3=float(a.attrib['a_s3'])
				a_t=float(a.attrib['a_t'])
				a_t2=float(a.attrib['a_t2'])
				a_t3=float(a.attrib['a_t3'])
				a_st=float(a.attrib['a_st'])
				a_s2t=float(a.attrib['a_s2t'])
				a_st2=float(a.attrib['a_st2'])
			for a in pm.iter('CoefficientsB'):
				b_o=float(a.attrib['b_o'])
				b_s=float(a.attrib['b_s'])
				b_s2=float(a.attrib['b_s2'])
				b_s3=float(a.attrib['b_s3'])
				b_t=float(a.attrib['b_t'])
				b_t2=float(a.attrib['b_t2'])
				b_t3=float(a.attrib['b_t3'])
				b_st=float(a.attrib['b_st'])
				b_s2t=float(a.attrib['b_s2t'])
				b_st2=float(a.attrib['b_st2'])

			cam[i_cam].assign_mapping_function(Z0, so, to, nx, ny, a_o, a_s, a_s2, a_s3, a_t, a_t2, a_t3, a_st, a_s2t, a_st2, b_o, b_s, b_s2, b_s3, b_t, b_t2, b_t3, b_st, b_s2t, b_st2)

print('camera: {0}'.format(cam[0].i_cam))
print('###########################')
cam[0].print_scale_para()
print('###########################')
cam[0].print_mapping_function()

k_X=1./cam[0].alpha_X
k_Y=1./cam[0].alpha_Y

print('###########################')
print('world origin in voxel coord')
Xv0=cam[0].poly[0].so
Yv0=cam[0].poly[0].to
print('(X_voxel origin {0}, Y_voxel origin {1})'.format(Xv0,Yv0))

Zv=[]
for i in range(len(cam[0].poly)):
	Zv.append(cam[0].poly[i].Z0)
	print("Z_voxel plane {0} {1}".format(i,Zv[i]))

print('world origin in world coord')
Xv0w=Xv0*cam[0].alpha_X + cam[0].X_offset
Yv0w=Yv0*cam[0].alpha_Y + cam[0].Y_offset
print('(X_world origin {0}, Y_world origin {1})'.format(Xv0w,Yv0w))

Zv_w=[]
for i in range(len(cam[0].poly)):
	Zv_w.append(Zv[i]*cam[0].alpha_Z + cam[0].Z_offset)
	print("Z_world plane {0} {1}".format(i,Zv_w[i]))

Xc_topX	= 290.
Xc_topY	= 90.
Xc_topZ	= 47.123890
Xc_bottomX = 0.
Xc_bottomY = 0.
Xc_bottomZ = 0.
nZ_plane = len(cam[0].poly)

Z_grid=np.linspace(Xc_bottomZ, Xc_topZ, num=nZ_plane)
print('Z grid {0}'.format(Z_grid)) 

Xw_r=np.random.uniform(Xc_bottomX,Xc_topX,1)
Yw_r=np.random.uniform(Xc_bottomY,Xc_topY,1)
Zw_r=np.random.uniform(Xc_bottomZ,Xc_topZ,1)
# Xw_r=145
# Yw_r=-150
# Zw_r=Z_grid[0]

print('###########################')
print('(X_world {0}, Y_world {1}, Z_world {2})'.format(Xw_r,Yw_r,Zw_r))
cam[0].project_particle(Xw_r, Yw_r, Zw_r)

print('###########################')
design_mat=np.array([1, Xw_r, Yw_r, Zw_r, Xw_r**2,   	Xw_r*Yw_r,   	Yw_r**2, 
										  Xw_r*Zw_r, 	Yw_r*Zw_r,   	Zw_r**2,
										  Xw_r**3,   	Xw_r**2*Yw_r,	Xw_r*Yw_r**2, Yw_r**3,
										  Xw_r**2*Zw_r, Xw_r*Yw_r*Zw_r, Yw_r**2*Zw_r, Xw_r*Zw_r**2, Yw_r*Zw_r**2, Zw_r**3]);
a_vec=[]
b_vec=[]

mp_file='../data/les3900rect/part010000_maxt0015_fct00010_bkg0.0_dtobs0.50/camera1.yaml'

fs=cv2.FileStorage(mp_file, cv2.FILE_STORAGE_READ)

n_coef=int(fs.getNode("n_coef").real())
a=[]
b=[]
for i in range(1,n_coef+1):
	a.append(fs.getNode('a'+str(i)).real())
	b.append(fs.getNode('b'+str(i)).real())

a_vec.append(a)
b_vec.append(b)

xi=np.dot(design_mat, a_vec[0])
yi=np.dot(design_mat, b_vec[0])
print('Reference projection for camera {0}:'.format(0))
print('(xi {0}, yi {1})'.format(xi,yi))