import matplotlib.pyplot as plt
import numpy as np
import os
from target_file import target_path

small_flag=False
dtobs='0.05'
over_lap='75%'
smooth_flag=True
suffix=''
# suffix='_finer'

if small_flag:
	fig_height=12
	fig_width=12
	bottom_size=0.15
else:
	fig_height=25
	fig_width=6
	bottom_size=0.15
textfont=30
titlefont=50
tickfont=30
legendfont=50

target=target_path(small_flag, dtobs, over_lap, suffix)

smooth_dir=''
if smooth_flag:
	smooth_dir='_smth'

velo_file_ref=target.result_dir+'/velo/Vorticity10ref.txt'
velo_file_bkg=target.result_dir+'/velo/Vorticity10bkg.txt'
velo_file_ana=target.result_dir+'/velo/Vorticity10ana'+smooth_dir+'.txt'

nx=target.nx
ny=target.ny
nz=target.nz

pi=3.1415
if small_flag:
	D=48
	Xc_topX=72
	Xc_topY=72
	Xc_topZ=35.557522
	Xc_bottomX=0
	Xc_bottomY=0
	Xc_bottomZ=19.849556
else:
	D=12
	Xc_topX=72.5
	Xc_topY=22.5
	Xc_topZ=11.78098
	Xc_bottomX=0
	Xc_bottomY=0
	Xc_bottomZ=0

xx, yy = np.meshgrid(np.arange(0, nx, 1)/D, np.arange(0, ny, 1)/D)
print(xx.shape)

f=np.loadtxt(velo_file_ref)
print(f.shape)
x=f[:,0]
y=f[:,1]
z=f[:,2]
u=f[:,10]
v=f[:,11]
w=f[:,12]

f_bkg=np.loadtxt(velo_file_bkg)
u_bkg=f_bkg[:,10]
v_bkg=f_bkg[:,11]
w_bkg=f_bkg[:,12]

f_ana=np.loadtxt(velo_file_ana)
u_ana=f_ana[:,10]
v_ana=f_ana[:,11]
w_ana=f_ana[:,12]

UU = np.zeros( xx.shape )
VV = np.zeros( xx.shape )
WW = np.zeros( xx.shape )
UU_bkg=np.zeros( xx.shape )
VV_bkg=np.zeros( xx.shape )
WW_bkg=np.zeros( xx.shape )
UU_ana=np.zeros( xx.shape )
VV_ana=np.zeros( xx.shape )
WW_ana=np.zeros( xx.shape )

err_min = np.zeros(nz)
err_max = np.zeros(nz)
err_bkg = np.zeros(nz)
err_ana = np.zeros(nz)

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			UU[j,i]=u[i+j*nx+k*nx*ny]
			UU_bkg[j,i]=u_bkg[i+j*nx+k*nx*ny]
			UU_ana[j,i]=u_ana[i+j*nx+k*nx*ny]
			VV[j,i]=v[i+j*nx+k*nx*ny]
			VV_bkg[j,i]=v_bkg[i+j*nx+k*nx*ny]
			VV_ana[j,i]=v_ana[i+j*nx+k*nx*ny]
			WW[j,i]=w[i+j*nx+k*nx*ny]
			WW_bkg[j,i]=w_bkg[i+j*nx+k*nx*ny]
			WW_ana[j,i]=w_ana[i+j*nx+k*nx*ny]

	err_min[k]=np.amin(UU-UU_bkg)
	err_max[k]=np.amax(UU-UU_bkg)
	err_bkg[k]=np.mean(np.absolute(UU-UU_bkg))
	err_ana[k]=np.mean(np.absolute(UU-UU_ana))

err_vmin=np.amin(err_min)
err_vmax=np.amax(err_max)

print('err_vmin {}'.format(err_vmin))
print('err_vmax {}'.format(err_vmax))

err_bkg_avg=np.mean(err_bkg)
err_ana_avg=np.mean(err_ana)

print('err_bkg_avg {}'.format(err_bkg_avg))
print('err_ana_avg {}'.format(err_ana_avg))

k=nz//2

for i in np.arange(0, nx, 1):
	for j in np.arange(0, ny, 1):
		UU[j,i]=u[i+j*nx+k*nx*ny]
		UU_bkg[j,i]=u_bkg[i+j*nx+k*nx*ny]
		UU_ana[j,i]=u_ana[i+j*nx+k*nx*ny]
		VV[j,i]=v[i+j*nx+k*nx*ny]
		VV_bkg[j,i]=v_bkg[i+j*nx+k*nx*ny]
		VV_ana[j,i]=v_ana[i+j*nx+k*nx*ny]
		WW[j,i]=w[i+j*nx+k*nx*ny]
		WW_bkg[j,i]=w_bkg[i+j*nx+k*nx*ny]
		WW_ana[j,i]=w_ana[i+j*nx+k*nx*ny]

Ur=UU
Ub=UU_bkg
Ua=UU_ana

vmin=np.amin(Ur)
vmax=np.amax(Ur)

fig=plt.figure(figsize=(fig_height,fig_width))
cmap = plt.get_cmap('hot')
fig.subplots_adjust(left=0.1,right=0.95,bottom=bottom_size,hspace=0.38,wspace=0.)

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)  # legend fontsize
plt.rc('figure', titlesize=titlefont)  

plt.subplot(1,1,1)
plt.pcolormesh(xx, yy, Ur, cmap=cmap, vmin=vmin, vmax=vmax)
plt.axis('scaled')
plt.colorbar()
plt.title('Ground truth U')
plt.xlabel('X/D')
plt.ylabel('Y/D')
if small_flag:
	plt.xticks([0,0.5,1,1.5])
	plt.yticks([0,0.5,1,1.5])
res_filename='/Users/yin.yang/Documents/velo_ref.eps'
plt.savefig(res_filename, formabt='eps', dpi=1500)

fig=plt.figure(figsize=(fig_height,fig_width))
cmap = plt.get_cmap('hot')
fig.subplots_adjust(left=0.1,right=0.95,bottom=bottom_size,hspace=0.38,wspace=0.)

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)  # legend fontsize
plt.rc('figure', titlesize=titlefont)  

plt.subplot(1,1,1)
plt.pcolormesh(xx, yy, Ua, cmap=cmap, vmin=vmin, vmax=vmax)
plt.axis('scaled')
plt.colorbar()
plt.title('Reconstructed U')
plt.xlabel('X/D')
plt.ylabel('Y/D')
if small_flag:
	plt.xticks([0,0.5,1,1.5])
	plt.yticks([0,0.5,1,1.5])
res_filename='/Users/yin.yang/Documents/velo_ana.eps'
plt.savefig(res_filename, formabt='eps', dpi=1500)

# V_ana_norm=np.sqrt(np.square(UU_ana)+np.square(VV_ana)+np.square(WW_ana))

# fig, ax = plt.subplots(figsize=(fig_height/2.,3))
# cmap = plt.get_cmap('hot')
# fig.subplots_adjust(bottom=bottom_size)

# ax.set_title('Velocity')
# plt.pcolormesh(xx, yy, V_ana_norm, cmap=cmap)
# # plt.grid(True)
# plt.colorbar()

# q = ax.quiver(xx, yy, UU_ana, VV_ana, pivot='mid', scale=5, scale_units='inches')
# # ax.quiverkey(q, X=1.1, Y=0.6, U=0.1, label='Plane Velocity', labelpos='E')

# plt.axis('scaled')
# plt.xlabel('X/D')
# plt.ylabel('Y/D')

k=nz//2

# for k in np.arange(0, nz, 1):
for i in np.arange(0, nx, 1):
	for j in np.arange(0, ny, 1):
		UU[j,i]=u[i+j*nx+k*nx*ny]
		VV[j,i]=v[i+j*nx+k*nx*ny]
		WW[j,i]=w[i+j*nx+k*nx*ny]
		UU_bkg[j,i]=u_bkg[i+j*nx+k*nx*ny]
		UU_ana[j,i]=u_ana[i+j*nx+k*nx*ny]

# fig=plt.figure(figsize=(fig_height,fig_width))
# cmap = plt.get_cmap('Spectral')
# fig.subplots_adjust(left=0.1,right=0.95,bottom=bottom_size,hspace=0.38,wspace=0.25)
# plt.subplot(1,1,1)
# plt.pcolormesh(xx, yy, UU-UU_bkg, cmap=cmap, vmin=err_vmin, vmax=err_vmax)
# plt.axis('scaled')
# plt.title('Error Bkg')
# plt.grid(True)
# plt.xlabel('X/D')
# plt.ylabel('Y/D')
# plt.colorbar()

fig=plt.figure(figsize=(fig_height,fig_width))
cmap = plt.get_cmap('OrRd')
fig.subplots_adjust(left=0.1,right=0.95,bottom=bottom_size,hspace=0.38,wspace=0.25)

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)  # legend fontsize
plt.rc('figure', titlesize=titlefont)  

plt.subplot(1,1,1)
plt.pcolormesh(xx, yy, np.abs(UU-UU_ana), cmap=cmap)
plt.axis('scaled')
plt.colorbar()
plt.title('Error U')
plt.xlabel('X/D')
plt.ylabel('Y/D')
if small_flag:
	plt.xticks([0,0.5,1,1.5])
	plt.yticks([0,0.5,1,1.5])
res_filename='/Users/yin.yang/Documents/velo_err.eps'
plt.savefig(res_filename, formabt='eps', dpi=1500)

plt.show()