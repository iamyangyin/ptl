import numpy as np
import os
import argparse
# from matplotlib.font_manager import FontProperties
# from matplotlib.ticker import FormatStrFormatter
import math
import time

start_time = time.time()

class Particle:
	def __init__(self,part_index):
		self.part_index=part_index
		self.X=-1
		self.Y=-1
		self.Z=-1

	def set_XYZ(self,X,Y,Z):
		self.X=X
		self.Y=Y
		self.Z=Z

	def compute_error_X(self, part_another):
		error_X=abs(self.X-part_another.X)
		return error_X

	def compute_error_Y(self, part_another):
		error_Y=abs(self.Y-part_another.Y)
		return error_Y

	def compute_error_Z(self, part_another):
		error_Z=abs(self.Z-part_another.Z)
		return error_Z

def read_dat_file(dat_file, it_seq, object_only_flag=False, verbal=False):
	start_time = time.time()
	trackid=[]
	particles_from_dat=[]
	if object_only_flag:
		iX=1
		iY=2
		iZ=3
	else:
		iX=9
		iY=10
		iZ=11

	with open(dat_file) as fp:
		for line in fp:
			line=line.strip()
			if line[0].isdigit():
				line=line.split()

				if float(line[3])>1e-6:
					trackid.append(int(line[0]))
					particles_from_dat.append(Particle(int(line[0])))
					particles_from_dat[-1].set_XYZ(float(line[iX]),float(line[iY]),float(line[iZ]))

	print('number of particles read from DATA.DAT file: {}'.format(len(particles_from_dat)))
	print("- read_dat_file {} TIME seconds -".format(time.time() - start_time))
	return particles_from_dat

def read_ref_file_snapshot(ref_path, it_seq, verbal=False):
	start_time = time.time()
	trackid=[]
	particles_ref=[]

	ref_file=ref_path+'/bkg/bkg_it{:03d}.txt'.format(it_seq)
	print(ref_file)

	with open(ref_file) as fp:
		for line in fp:
			line=line.strip()

			if line[0].isdigit():
				line=line.split()

				if line[3]!='-1':
					trackid.append(int(line[0]))
					particles_ref.append(Particle(int(line[0])))
					particles_ref[-1].set_XYZ(float(line[3]),float(line[4]),float(line[5]))

	print('number of particles read from REFERENCE file: {}'.format(len(particles_ref)))
	print("- read_ref_file {} TIME seconds -".format(time.time() - start_time))
	return particles_ref

def read_ref_file(ref_path, it_max, verbal=False):
	start_time = time.time()
	trackid=[]
	particles_ref=[]
	for i in range(it_max):
		it=i+1
		ref_file=ref_path+'/bkg/bkg_it{:03d}.txt'.format(it)

		with open(ref_file) as fp:
			for line in fp:
				line=line.strip()

				if line[0].isdigit():
					line=line.split()

					if it==1 and line[3]!='-1':
						trackid.append(int(line[0]))
						particles_ref.append(Particle(int(line[0])))
						particles_ref[-1].set_XYZ(float(line[3]),float(line[4]),float(line[5]))

	print('number of particles read from REFERENCE file: {}'.format(len(particles_ref)))
	print("- read_ref_file {} TIME seconds -".format(time.time() - start_time))
	return particles_ref

def compare_distance(X, Y, Z, X_another, Y_another, Z_another, error_threshold):
	dist_x_sq=abs(X-X_another)
	if dist_x_sq < error_threshold:
		dist_y_sq=abs(Y-Y_another)
		if dist_y_sq < error_threshold:
			dist_z_sq=abs(Z-Z_another)
			if dist_z_sq < error_threshold:
				return math.sqrt( dist_x_sq**2 + dist_y_sq**2 + dist_z_sq**2 ) < error_threshold
			else:
				return False
		else:
			return False
	else:
		return False

def find_particles_tupel_from_ref(particles_ref, particles_tgt, error_threshold, verbal=False):
	start_time = time.time()
	particles_tuple_list=[]

	list_tgt_paired=[False]*len(particles_tgt)
	list_ref_paired=[False]*len(particles_ref)

	error_threshold_cnt=error_threshold

	while float(len(particles_tuple_list)) < float(len(particles_tgt)) and error_threshold_cnt < 1:
		for num_part_ref in range(len(particles_ref)):
			ref_X=particles_ref[num_part_ref].X
			ref_Y=particles_ref[num_part_ref].Y
			ref_Z=particles_ref[num_part_ref].Z

			for num_part_tgt in range(len(particles_tgt)):

				if not list_tgt_paired[num_part_tgt] and not list_ref_paired[num_part_ref]:
					if compare_distance(particles_tgt[num_part_tgt].X,
								        particles_tgt[num_part_tgt].Y,
									    particles_tgt[num_part_tgt].Z,
				 				   	    ref_X, ref_Y, ref_Z, error_threshold_cnt):

						list_tgt_paired[num_part_tgt]=True
						list_ref_paired[num_part_ref]=True
						particles_tuple_list.append((num_part_tgt, num_part_ref))

		error_threshold_cnt=error_threshold_cnt*2.

	print('number of tupels found: {}'.format(len(particles_tuple_list)))

	if(verbal):
		for tp in particles_tuple_list:
			print('ref {:5d}, X {:15.12f} Y {:15.12f} Z {:15.12f}'.format(tp[1], particles_ref[tp[1]].X, particles_ref[tp[1]].Y, particles_ref[tp[1]].Z))
			print('tgt {:5d}, X {:15.12f} Y {:15.12f} Z {:15.12f}'.format(tp[0], particles_tgt[tp[0]].X, particles_tgt[tp[0]].Y, particles_tgt[tp[0]].Z))

	print("- find_particles_tupel_from_ref {} TIME seconds -".format(time.time() - start_time))
	return particles_tuple_list

def compute_mean_error(particles_tuple_list, particles_tgt, particles_ref, dX_px, verbal=False):
	start_time = time.time()
	mean_error=[]
	mean_error_px=[]
	num_snapshots=1
	num_part_ss=[]

	histogram=[0]*11

	if(verbal):
		for part_tuple in particles_tuple_list:
			print('part_tgt {}, part_ref {}'.format(particles_tgt[part_tuple[0]].part_index, particles_ref[part_tuple[1]].part_index))

	for it_ss in range(num_snapshots):
		it_seq=it_ss
		errorX=0.
		errorY=0.
		errorZ=0.
		num_part=0
		for part_tuple in particles_tuple_list:
			errX=particles_tgt[part_tuple[0]].compute_error_X(particles_ref[part_tuple[1]])
			errY=particles_tgt[part_tuple[0]].compute_error_Y(particles_ref[part_tuple[1]])
			errZ=particles_tgt[part_tuple[0]].compute_error_Z(particles_ref[part_tuple[1]])

			errX_px=errX/dX_px[0]
			errY_px=errY/dX_px[1]
			errZ_px=errZ/dX_px[2]

			err_px=math.sqrt(errX_px**2+errY_px**2+errZ_px**2)

			if err_px < 0.01: 
				histogram[0]+=1
			elif err_px >= 0.01 and err_px < 0.02: 
				histogram[1]+=1
			elif err_px >= 0.02 and err_px < 0.03: 
				histogram[2]+=1
			elif err_px >= 0.03 and err_px < 0.04: 
				histogram[3]+=1
			elif err_px >= 0.04 and err_px < 0.05: 
				histogram[4]+=1
			elif err_px >= 0.05 and err_px < 0.06: 
				histogram[5]+=1
			elif err_px >= 0.06 and err_px < 0.07: 
				histogram[6]+=1
			elif err_px >= 0.07 and err_px < 0.08: 
				histogram[7]+=1
			elif err_px >= 0.08 and err_px < 0.09: 
				histogram[8]+=1
			elif err_px >= 0.09 and err_px < 0.1: 
				histogram[9]+=1
			else: 
				histogram[10]+=1

			if(verbal):
				print('ref {:5d} - tgt {:5d} - err_mm {:15.12f} - err_px {:15.12f}'.format(part_tuple[1], part_tuple[0], math.sqrt(errX**2+errY**2+errZ**2), math.sqrt(errX_px**2+errY_px**2+errZ_px**2)))

			errorX+=errX
			errorY+=errY
			errorZ+=errZ
			num_part+=1

		num_part_ss.append(num_part)

		mean_errorX_it_px=(errorX/num_part)/dX_px[0]
		mean_errorY_it_px=(errorY/num_part)/dX_px[1]
		mean_errorZ_it_px=(errorZ/num_part)/dX_px[2]

		mean_errorX_it=(errorX/num_part)
		mean_errorY_it=(errorY/num_part)
		mean_errorZ_it=(errorZ/num_part)

		mean_error_px.append(math.sqrt(mean_errorX_it_px**2+mean_errorY_it_px**2+mean_errorZ_it_px**2))
		mean_error.append(math.sqrt(mean_errorX_it**2+mean_errorY_it**2+mean_errorZ_it**2))

	print('number parts: {}'.format(num_part_ss))
	print('particles error < 0.01 {}'.format(histogram[0]))
	print('particles error < 0.02 {}'.format(histogram[1]))
	print('particles error < 0.03 {}'.format(histogram[2]))
	print('particles error < 0.04 {}'.format(histogram[3]))
	print('particles error < 0.05 {}'.format(histogram[4]))
	print('particles error < 0.06 {}'.format(histogram[5]))
	print('particles error < 0.07 {}'.format(histogram[6]))
	print('particles error < 0.08 {}'.format(histogram[7]))
	print('particles error < 0.09 {}'.format(histogram[8]))
	print('particles error < 0.10 {}'.format(histogram[9]))
	print('particles error > 0.10 {}'.format(histogram[10]))
	print("- compute_mean_error {} TIME seconds -".format(time.time() - start_time))
	return mean_error, mean_error_px, num_part_ss
