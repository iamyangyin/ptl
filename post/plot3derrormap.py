import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d

def readErrorMap(result_path, ncyc):
	f=np.loadtxt(result_path)
	return f

error_valve=0.01
ncyc=41
snapshots='50' 
case_name='les3900rect'
dtobs='0.25'
px=''
# suffix='_Euler'
suffix=''

part=[]
part.append('20000')

it_seq=[]
it_seq.append('001')
it_seq.append('004')
it_seq.append('007')
it_seq.append('009')

X_t=[]
Y_t=[]
Z_t=[]

for ip in part:
	for it in it_seq:
		res_path='../data/'+case_name+'/part0'+ip+'_maxt00'+snapshots+'_fct00010_psf1_dtobs'+dtobs+suffix
		data=np.loadtxt(res_path+'/bkg/bkg_it'+it+'.txt')

		part_index=data[:,0]
		X_t.append(data[::100,3])
		Y_t.append(data[::100,4])
		Z_t.append(data[::100,5])

fig=plt.figure()
ax = fig.add_subplot(111, projection='3d')
sc = ax.scatter(X_t[0],Z_t[0],Y_t[0])
sc = ax.scatter(X_t[1],Z_t[1],Y_t[1])
sc = ax.scatter(X_t[2],Z_t[2],Y_t[2])
sc = ax.scatter(X_t[3],Z_t[3],Y_t[3])
ax.set_xlim(0,72.5)
ax.set_zlim(0,22.5)
ax.set_ylim(0,11.781)
ax.set_xlabel('X mm')
ax.set_ylabel('Y mm')
ax.set_zlabel('Z mm')
plt.show()

# fig=plt.figure()
# ax = fig.add_subplot(121)
# plt.bar(err_interval_t[0][:],err_interval_count_t[0][:],1.0/numbin)
# ax.set_ylim(1,10000)
# plt.xlabel('Number')
# plt.ylabel('Bin')

# ax = fig.add_subplot(122)
# plt.bar(err_interval_t[1][:],err_interval_count_t[1][:],1.0/numbin)
# ax.set_ylim(1,10000)
# plt.xlabel('Number')
# plt.ylabel('Bin')

# plt.show()

# fig=plt.figure()
# ax = fig.add_subplot(111)
# sc = ax.scatter(err_X_t[0],err_Y_t[0])
# plt.xlabel('Error X')
# plt.ylabel('Error Y')
# plt.show()

# fig=plt.figure()
# # ax = fig.add_subplot(121, projection='3d')
# # sc = ax.scatter(X_t[0],Z_t[0],Y_t[0],c=err_t[0])
# # ax.set_xlim(1,291)
# # ax.set_zlim(1,291)
# # ax.set_ylim(1,48.12389)

# ax = fig.add_subplot(121)
# sc = ax.scatter(X_t[0],Y_t[0],err_t[0])
# # sc = ax.scatter(X_t[0],Z_t[0],err_t[0])
# # sc = ax.scatter(Y_t[0],Z_t[0],err_t[0])
# ax.set_xlim(1,291)
# ax.set_ylim(1,291)
# # fig.colorbar(sc)


# # ax = fig.add_subplot(122, projection='3d')
# # sc = ax.scatter(X_t[1],Z_t[1],Y_t[1],c=err_t[1])
# # ax.set_xlim(1,291)
# # ax.set_zlim(1,291)
# # ax.set_ylim(1,48.12389)

# ax = fig.add_subplot(122)
# sc = ax.scatter(X_t[1],Y_t[1],err_t[1])
# # sc = ax.scatter(X_t[1],Z_t[1],err_t[1])
# # sc = ax.scatter(Y_t[1],Z_t[1],err_t[1])
# ax.set_xlim(1,291)
# ax.set_ylim(1,291)
# # fig.colorbar(sc)

# # ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))

# # box=ax.get_position()
# # ax.set_position([box.x0, box.y0, box.width*1, box.height])
# # plt.legend(loc='upper right',prop={'size':15})
# # plt.legend(bbox_to_anchor=(1, 1),loc='upper left',prop={'size':10})
# # plt.autoscale(enable=True, axis='x', tight=True)

# # plt.xlabel('Snapshots')
# # plt.ylabel('Pixel')

# # plt.title('Mean Positional Error')
# # res_filename='/Users/yin.yang/Documents/3derrormap.eps'
# # plt.savefig(res_filename, formabt='eps', dpi=1500)
# # plt.autoscale(enable=True, axis='x', tight=True)
# plt.show()
