#compare reconstructed particles ENS/STB
#time used:  1000 particles ~ x1
#time used:  5000 particles ~ x10
#time used: 10000 particles ~ x40
#time used: 20000 particles ~ x300
#time used: 30000 particles ~ x400
#time used: 40000 particles ~ x800

import numpy as np
import os
import argparse
import time
from compareTwoListsOfParticles import Particle, read_dat_file, read_ref_file, find_particles_tupel_from_ref, find_particles_new_tupel_from_ref, compute_mean_error

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--casename", default="", help="name case")
parser.add_argument("-n", "--numpart", default="", help="name number of particles")
parser.add_argument("-t", "--stbdir", default="", help="name stb data dir")
parser.add_argument("-s", "--stbtype", default="", help="name stb data type")
parser.add_argument("-f", "--refdir", default="", help="name ref data dir")
parser.add_argument("-e", "--ensdir", default="", help="name ens result dir")
parser.add_argument("-r", "--runindex", default="", help="name ens run index")
parser.add_argument("-o", "--dtobs", default="", help="name dtobs")
args=parser.parse_args()

davis_flag=False
ens_flag=True
stb_flag=True

subset_flag='all' #'no_new',just_new','all'

it_max_case = 15
it_max = 15
it_fct = 10

assert (it_max <= it_max_case)

error_threshold_davis = 1
error_threshold_ens   = 1

case_name = args.casename
num_part = int(args.numpart)
data_dir=args.stbdir
data_suffix=args.stbtype
ref_dir=args.refdir
ens_dir=args.ensdir
dtobs = args.dtobs

if(davis_flag):
	if num_part==1000:
		data_num='01'
	elif num_part==4000:
		data_num='02'
	elif num_part==10000:
		data_num='03'
	elif num_part==20000:
		data_num='04'
	elif num_part==30000:
		data_num='05'
	elif num_part==40000:
		data_num='06'
	elif num_part==50000:
		data_num='07'

	data_dir=data_num+data_dir+'/'

if case_name=='dns300':
	assert(dtobs=='0.01' or dtobs=='0.005')
	dX_px=[0.012, 0.0104, 0.0184]
elif case_name=='les3900rect':
	assert(dtobs=='0.05' or dtobs=='0.25' or dtobs=='0.50')
	dX_px=[0.062, 0.058, 0.106]
	
psf='psf1'
num_part_str='{:06d}'.format(num_part)

root_dir=os.environ['STBENSPTV_PATH']
data_path=root_dir+'/result/'+case_name+'/part'+num_part_str+'_maxt00'+str(it_max_case)+'_fct00010_bkg0.000_'+psf+'_dtobs'+dtobs+ens_dir

if ens_flag:
	ens_run_index='debug'+args.runindex
	ensptv_path=data_path+'/ENS'+ens_run_index
	ensptv_file=ensptv_path+'/result.dat'
	print('LaPIV ens_file\n{}'.format(ensptv_file))

if stb_flag:
	ens_run_index2='debug'+args.runindex
	stbptv_path=data_path+'/STB'+ens_run_index2
	stbptv_file=stbptv_path+'/result.dat'
	print('LaPIV stb_file\n{}'.format(stbptv_file))

ref_path=root_dir+'/data/'+case_name+'/part'+num_part_str+'_maxt0050_fct00010_bkg0.000_'+psf+'_dtobs'+dtobs+ref_dir
print('ref_file\n{}'.format(ref_path))

[particles_ref, list_ref_it_deb, list_ref_it_fin]=read_ref_file(ref_path, it_max, verbal=False)

if ens_flag:
	print('compute LAPTV ENS error')
	[particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin]=read_dat_file(ensptv_file, it_max, verbal=False)
	particles_tuple_lapiv_ref=find_particles_new_tupel_from_ref(num_part, particles_ref, list_ref_it_deb, list_ref_it_fin, particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin,\
															 it_fct, it_max, error_threshold_ens, subset_flag, verbal=False)
	[mean_error_lapiv, mean_error_lapiv_px, num_part_lapiv]=compute_mean_error(particles_tuple_lapiv_ref, particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin,\
														 particles_ref, list_ref_it_deb, list_ref_it_fin, it_fct, it_max, dX_px)
	# print('mean_error_lapiv   : {}'.format(mean_error_lapiv))
	print('mean_error_lapiv_px: {}'.format(mean_error_lapiv_px))

if stb_flag:
	print('compute LAPTV STB error')
	[particles_stb, list_stb_it_deb, list_stb_it_fin]=read_dat_file(stbptv_file, it_max, verbal=False)
	particles_tuple_stb_ref=find_particles_new_tupel_from_ref(num_part, particles_ref, list_ref_it_deb, list_ref_it_fin, particles_stb, list_stb_it_deb, list_stb_it_fin,\
															 it_fct, it_max, error_threshold_ens, subset_flag, verbal=False)
	[mean_error_stb, mean_error_stb_px, num_part_stb]=compute_mean_error(particles_tuple_stb_ref, particles_stb, list_stb_it_deb, list_stb_it_fin,\
														 particles_ref, list_ref_it_deb, list_ref_it_fin, it_fct, it_max, dX_px)
	# print('mean_error_stb   : {}'.format(mean_error_stb))
	print('mean_error_stb_px: {}'.format(mean_error_stb_px))

print("- total {} seconds -".format(time.time() - start_time))
