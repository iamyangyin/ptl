import numpy as np
import os
import argparse
import time
from compareTwoCloudsOfParticles import Particle, read_dat_file, read_ref_file_snapshot, find_particles_tupel_from_ref, compute_mean_error

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--numpart", default="", help="name number of particles")
parser.add_argument("-t", "--timestep", default="", help="name time level")
parser.add_argument("-c", "--casename", default="", help="name case name")
parser.add_argument("-o", "--optmethod", default="", help="name opt method")
args=parser.parse_args()

it_max_case = 50
it_max = 10
it_fct = 5
assert (it_max <= it_max_case)

error_threshold_ptv = 0.01

dtobs = '0.10'

case_name = args.casename
num_part = int(args.numpart)
it_seq = args.timestep
opt_method = args.optmethod

if dtobs=='0.05' or dtobs=='0.25' or dtobs=='0.10':
	if case_name=='les3900rect':
		dX_px=[0.065, 0.058, 0.106]

psf = 'psf1'
num_part_str='{:06d}'.format(num_part)

ref_path='/home/yin/stb-ens-ptv-c/data/'+case_name+'/part'+num_part_str+'_maxt0050_fct00005_bkg0.000_'+psf+'_dtobs'+dtobs
print('ref_path\n{}'.format(ref_path))
particles_ref=read_ref_file_snapshot(ref_path, int(it_seq), verbal=False)

ptv_file_name='Triangulation_it00'+it_seq+'.txt'
object_only_flag=False
ptv_file=ref_path+'/bkg/'+ptv_file_name
print('LaPIV ptv_file\n{}'.format(ptv_file))
particles_ptv=read_dat_file(ptv_file, int(it_seq), object_only_flag=False, verbal=False)

print('compute LAPIV PTV error')
particles_tuple_ptv_ref=find_particles_tupel_from_ref(particles_ref, particles_ptv, error_threshold_ptv, verbal=False)

[mean_error_ptv, mean_error_ptv_px, num_part_ptv]=compute_mean_error(particles_tuple_ptv_ref, particles_ptv, particles_ref, dX_px)

print('mean_error_ptv   : {}'.format(mean_error_ptv))
print('mean_error_ptv_px: {}'.format(mean_error_ptv_px))

ipr_file_name='IPR_it00'+it_seq+'_'+opt_method+'.txt'
object_only_flag=True
ipr_file=ref_path+'/bkg/'+ipr_file_name
print('LaPIV ipr_file\n{}'.format(ipr_file))
particles_ipr=read_dat_file(ipr_file, int(it_seq), object_only_flag=True, verbal=False)

print('compute LAPIV IPR error')
particles_tuple_ipr_ref=find_particles_tupel_from_ref(particles_ref, particles_ipr, error_threshold_ptv, verbal=False)

[mean_error_ipr, mean_error_ipr_px, num_part_ipr]=compute_mean_error(particles_tuple_ipr_ref, particles_ipr, particles_ref, dX_px)

print('mean_error_ipr   : {}'.format(mean_error_ipr))
print('mean_error_ipr_px: {}'.format(mean_error_ipr_px))

print("- total {} seconds -".format(time.time() - start_time))
