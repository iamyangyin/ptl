# trace generated using paraview version 5.6.1
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

result_path = '/Users/yin.yang/STB-ENS-PTV-C++/result/les3900rect/part010000_maxt0020_fct00010_bkg0.000_psf1_dtobs0.25/ENSdebug_newk_v300/'

# create a new 'XML Unstructured Grid Reader'
result0 = XMLUnstructuredGridReader(FileName=[result_path+'result010.vtu', 
											  result_path+'result011.vtu',
											  result_path+'result012.vtu',
											  result_path+'result013.vtu',
											  result_path+'result014.vtu',
											  result_path+'result015.vtu',
											  result_path+'result016.vtu',
											  result_path+'result017.vtu',
											  result_path+'result018.vtu',
											  result_path+'result019.vtu',
											  result_path+'result020.vtu'])
result0.PointArrayStatus = ['TrackID']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [3254, 1264]

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.XAxisUseCustomLabels = 1
renderView1.AxesGrid.XAxisLabels = [0.0, 72.5]
renderView1.AxesGrid.YAxisUseCustomLabels = 1
renderView1.AxesGrid.YAxisLabels = [0.0, 22.5]
renderView1.AxesGrid.ZAxisUseCustomLabels = 1
renderView1.AxesGrid.ZAxisLabels = [0.0, 11.78]

# Properties modified on renderView1
renderView1.CenterAxesVisibility = 1

# set active source
SetActiveSource(result0)

# show data in view
result0Display = Show(result0, renderView1)

# trace defaults for the display properties.
result0Display.Representation = 'Surface'
result0Display.ColorArrayName = [None, '']
result0Display.OSPRayScaleArray = 'TrackID'
result0Display.OSPRayScaleFunction = 'PiecewiseFunction'
result0Display.SelectOrientationVectors = 'None'
result0Display.ScaleFactor = 7.2466660805046565
result0Display.SelectScaleArray = 'None'
result0Display.GlyphType = 'Arrow'
result0Display.GlyphTableIndexArray = 'None'
result0Display.GaussianRadius = 0.3623333040252328
result0Display.SetScaleArray = ['POINTS', 'TrackID']
result0Display.ScaleTransferFunction = 'PiecewiseFunction'
result0Display.OpacityArray = ['POINTS', 'TrackID']
result0Display.OpacityTransferFunction = 'PiecewiseFunction'
result0Display.DataAxesGrid = 'GridAxesRepresentation'
result0Display.SelectionCellLabelFontFile = ''
result0Display.SelectionPointLabelFontFile = ''
result0Display.PolarAxes = 'PolarAxesRepresentation'
result0Display.ScalarOpacityUnitDistance = 76.78709447072426

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
result0Display.DataAxesGrid.XTitleFontFile = ''
result0Display.DataAxesGrid.YTitleFontFile = ''
result0Display.DataAxesGrid.ZTitleFontFile = ''
result0Display.DataAxesGrid.XLabelFontFile = ''
result0Display.DataAxesGrid.YLabelFontFile = ''
result0Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
result0Display.PolarAxes.PolarAxisTitleFontFile = ''
result0Display.PolarAxes.PolarAxisLabelFontFile = ''
result0Display.PolarAxes.LastRadialAxisTextFontFile = ''
result0Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# hide data in view
# Hide(result0, renderView1)

# show data in view
result0Display = Show(result0, renderView1)

# reset view to fit data
renderView1.ResetCamera()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Temporal Particles To Pathlines'
temporalParticlesToPathlines1 = TemporalParticlesToPathlines(Input=result0,
    Selection=None)

# Properties modified on temporalParticlesToPathlines1
temporalParticlesToPathlines1.MaskPoints = 10
temporalParticlesToPathlines1.IdChannelArray = 'TrackID'

# show data in view
temporalParticlesToPathlines1Display = Show(temporalParticlesToPathlines1, renderView1)

# get color transfer function/color map for 'TrailId'
trailIdLUT = GetColorTransferFunction('TrailId')
trailIdLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 355005.0, 0.865003, 0.865003, 0.865003, 710010.0, 0.705882, 0.0156863, 0.14902]
trailIdLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
temporalParticlesToPathlines1Display.Representation = 'Surface'
temporalParticlesToPathlines1Display.ColorArrayName = ['POINTS', 'TrailId']
temporalParticlesToPathlines1Display.DiffuseColor = [1.0, 0.0, 0.0]
temporalParticlesToPathlines1Display.LookupTable = trailIdLUT
temporalParticlesToPathlines1Display.OSPRayScaleArray = 'TrailId'
temporalParticlesToPathlines1Display.OSPRayScaleFunction = 'PiecewiseFunction'
temporalParticlesToPathlines1Display.SelectOrientationVectors = 'None'
temporalParticlesToPathlines1Display.ScaleFactor = 7.235659573040903
temporalParticlesToPathlines1Display.SelectScaleArray = 'TrailId'
temporalParticlesToPathlines1Display.GlyphType = 'Arrow'
temporalParticlesToPathlines1Display.GlyphTableIndexArray = 'TrailId'
temporalParticlesToPathlines1Display.GaussianRadius = 0.3617829786520451
temporalParticlesToPathlines1Display.SetScaleArray = ['POINTS', 'TrailId']
temporalParticlesToPathlines1Display.ScaleTransferFunction = 'PiecewiseFunction'
temporalParticlesToPathlines1Display.OpacityArray = ['POINTS', 'TrailId']
temporalParticlesToPathlines1Display.OpacityTransferFunction = 'PiecewiseFunction'
temporalParticlesToPathlines1Display.DataAxesGrid = 'GridAxesRepresentation'
temporalParticlesToPathlines1Display.SelectionCellLabelFontFile = ''
temporalParticlesToPathlines1Display.SelectionPointLabelFontFile = ''
temporalParticlesToPathlines1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
temporalParticlesToPathlines1Display.DataAxesGrid.XTitleFontFile = ''
temporalParticlesToPathlines1Display.DataAxesGrid.YTitleFontFile = ''
temporalParticlesToPathlines1Display.DataAxesGrid.ZTitleFontFile = ''
temporalParticlesToPathlines1Display.DataAxesGrid.XLabelFontFile = ''
temporalParticlesToPathlines1Display.DataAxesGrid.YLabelFontFile = ''
temporalParticlesToPathlines1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
temporalParticlesToPathlines1Display.PolarAxes.PolarAxisTitleFontFile = ''
temporalParticlesToPathlines1Display.PolarAxes.PolarAxisLabelFontFile = ''
temporalParticlesToPathlines1Display.PolarAxes.LastRadialAxisTextFontFile = ''
temporalParticlesToPathlines1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(result0, renderView1)

# show color bar/color legend
temporalParticlesToPathlines1Display.SetScalarBarVisibility(renderView1, True)

# # show data in view
# temporalParticlesToPathlines1Display_1 = Show(OutputPort(temporalParticlesToPathlines1, 1), renderView1)

# # trace defaults for the display properties.
# temporalParticlesToPathlines1Display_1.Representation = 'Surface'
# temporalParticlesToPathlines1Display_1.ColorArrayName = [None, '']
# temporalParticlesToPathlines1Display_1.DiffuseColor = [1.0, 0.0, 0.0]
# temporalParticlesToPathlines1Display_1.OSPRayScaleArray = 'TrackID'
# temporalParticlesToPathlines1Display_1.OSPRayScaleFunction = 'PiecewiseFunction'
# temporalParticlesToPathlines1Display_1.SelectOrientationVectors = 'None'
# temporalParticlesToPathlines1Display_1.ScaleFactor = 7.235659573040903
# temporalParticlesToPathlines1Display_1.SelectScaleArray = 'None'
# temporalParticlesToPathlines1Display_1.GlyphType = 'Arrow'
# temporalParticlesToPathlines1Display_1.GlyphTableIndexArray = 'None'
# temporalParticlesToPathlines1Display_1.GaussianRadius = 0.3617829786520451
# temporalParticlesToPathlines1Display_1.SetScaleArray = ['POINTS', 'TrackID']
# temporalParticlesToPathlines1Display_1.ScaleTransferFunction = 'PiecewiseFunction'
# temporalParticlesToPathlines1Display_1.OpacityArray = ['POINTS', 'TrackID']
# temporalParticlesToPathlines1Display_1.OpacityTransferFunction = 'PiecewiseFunction'
# temporalParticlesToPathlines1Display_1.DataAxesGrid = 'GridAxesRepresentation'
# temporalParticlesToPathlines1Display_1.SelectionCellLabelFontFile = ''
# temporalParticlesToPathlines1Display_1.SelectionPointLabelFontFile = ''
# temporalParticlesToPathlines1Display_1.PolarAxes = 'PolarAxesRepresentation'

# # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
# temporalParticlesToPathlines1Display_1.DataAxesGrid.XTitleFontFile = ''
# temporalParticlesToPathlines1Display_1.DataAxesGrid.YTitleFontFile = ''
# temporalParticlesToPathlines1Display_1.DataAxesGrid.ZTitleFontFile = ''
# temporalParticlesToPathlines1Display_1.DataAxesGrid.XLabelFontFile = ''
# temporalParticlesToPathlines1Display_1.DataAxesGrid.YLabelFontFile = ''
# temporalParticlesToPathlines1Display_1.DataAxesGrid.ZLabelFontFile = ''

# # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
# temporalParticlesToPathlines1Display_1.PolarAxes.PolarAxisTitleFontFile = ''
# temporalParticlesToPathlines1Display_1.PolarAxes.PolarAxisLabelFontFile = ''
# temporalParticlesToPathlines1Display_1.PolarAxes.LastRadialAxisTextFontFile = ''
# temporalParticlesToPathlines1Display_1.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# # hide data in view
# Hide(result0, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# get opacity transfer function/opacity map for 'TrailId'
trailIdPWF = GetOpacityTransferFunction('TrailId')
trailIdPWF.Points = [0.0, 0.0, 0.5, 0.0, 710010.0, 1.0, 0.5, 0.0]
trailIdPWF.ScalarRangeInitialized = 1

# set scalar coloring
ColorBy(temporalParticlesToPathlines1Display, ('POINTS', 'TrackID'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(trailIdLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
temporalParticlesToPathlines1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
temporalParticlesToPathlines1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'TrackID'
trackIDLUT = GetColorTransferFunction('TrackID')
trackIDLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 355005.0, 0.865003, 0.865003, 0.865003, 710010.0, 0.705882, 0.0156863, 0.14902]
trackIDLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'TrackID'
trackIDPWF = GetOpacityTransferFunction('TrackID')
trackIDPWF.Points = [0.0, 0.0, 0.5, 0.0, 710010.0, 1.0, 0.5, 0.0]
trackIDPWF.ScalarRangeInitialized = 1

# change representation type
temporalParticlesToPathlines1Display.SetRepresentationType('Point Gaussian')

# set active source
SetActiveSource(temporalParticlesToPathlines1)

# reset view to fit data
renderView1.ResetCamera()

animationScene1.Play()

# get color legend/bar for trackIDLUT in view renderView1
trackIDLUTColorBar = GetScalarBar(trackIDLUT, renderView1)
trackIDLUTColorBar.Title = 'TrackID'
trackIDLUTColorBar.ComponentTitle = ''
trackIDLUTColorBar.TitleFontFile = ''
trackIDLUTColorBar.LabelFontFile = ''

# change scalar bar placement
trackIDLUTColorBar.WindowLocation = 'AnyLocation'
trackIDLUTColorBar.Position = [0.7631299083769633, 0.45490506329113917]
trackIDLUTColorBar.ScalarBarLength = 0.33

# change scalar bar placement
trackIDLUTColorBar.Position = [0.8275038060841943, 0.3662974683544303]

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-25.35926198564898, 45.0355061896589, 136.25583482834938]
renderView1.CameraFocalPoint = [36.198762071318924, 11.243460144847639, 5.89309601299464]
renderView1.CameraViewUp = [0.11890912619115601, 0.9733365685393115, -0.19615438830822493]
renderView1.CameraParallelScale = 38.32422000066921

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).