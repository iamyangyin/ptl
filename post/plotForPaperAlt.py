import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
import argparse
from post_results import log_results, read_log_file

parser = argparse.ArgumentParser()
parser.add_argument("plottype", help="give the name of the plot, currently support error, undetected, trackedghost, totalghost")

args=parser.parse_args()

plottype=args.plottype

root_path=os.environ['STBENSPTV_PATH']

dtobs_str='0.5'
dtobs = float(dtobs_str)
dtobs_str='{:.2f}'.format(dtobs)

maxt_str = '50'
maxt = int(maxt_str)
maxt_str='{:04d}'.format(maxt)

num_part_str='20000'
num_part = int(num_part_str)
num_part_str='{:06d}'.format(num_part)

thin_str = ''
run_index_stb='debug_davis_OTF_R0.8_I1.1_IT10'
result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_1_20000  = read_log_file(result_dir+'STBtrg'+run_index_stb+'/result'+thin_str+'.log')

thin_str = '_thin'
run_index_stb_inhouse='debug0'
run_index_ens='debug0'
result_dir=root_path+'/result/lavision_bkgonly/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_11_20000 = read_log_file(result_dir+'STBtrg'+run_index_stb_inhouse+'/result'+thin_str+'.log')
ens_1_20000  = read_log_file(result_dir+'ENStrg'+run_index_ens+'/result'+thin_str+'.log')

num_part_str='30000'
num_part = int(num_part_str)
num_part_str='{:06d}'.format(num_part)

thin_str = '_part'
run_index_stb='debug_davis_OTF_R0.8_I1.1_IT10'
result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_2_30000 = read_log_file(result_dir+'STBtrg'+run_index_stb+'/result'+thin_str+'.log')

thin_str = '_thin'
run_index_stb_inhouse='debug0'
run_index_ens='debug4'
result_dir=root_path+'/result/lavision_bkgonly/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_21_30000 = read_log_file(result_dir+'STBtrg'+run_index_stb_inhouse+'/result'+thin_str+'.log')
ens_2_30000  = read_log_file(result_dir+'ENStrg'+run_index_ens+'/result'+thin_str+'.log')

num_part_str='40000'
num_part = int(num_part_str)
num_part_str='{:06d}'.format(num_part)

thin_str = '_part'
run_index_stb='debug_davis_OTF_R0.8_I1.1_IT10'
result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_3_40000 = read_log_file(result_dir+'STBtrg'+run_index_stb+'/result'+thin_str+'.log')

thin_str = '_thin'
run_index_stb_inhouse='debug0'
run_index_ens='debug4'
result_dir=root_path+'/result/lavision_bkgonly/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_31_40000 = read_log_file(result_dir+'STBtrg'+run_index_stb_inhouse+'/result'+thin_str+'.log')
ens_3_40000  = read_log_file(result_dir+'ENStrg'+run_index_ens+'/result'+thin_str+'.log')

###########################################################################################
# print(sum(stb_1_20000.num_undetected_true_parts[39:44])/(200*5))
# print(sum(stb_11_20000.num_undetected_true_parts[39:44])/(200*5))
# print(sum(ens_1_20000.num_undetected_true_parts[39:44])/(200*5))

# print(sum(stb_2_30000.num_undetected_true_parts[39:44])/(300*5))
# print(sum(stb_21_30000.num_undetected_true_parts[39:44])/(300*5))
# print(sum(ens_2_30000.num_undetected_true_parts[39:44])/(300*5))

# print(sum(stb_3_40000.num_undetected_true_parts[39:44])/(400*5))
# print(sum(stb_31_40000.num_undetected_true_parts[39:44])/(400*5))
# print(sum(ens_3_40000.num_undetected_true_parts[39:44])/(400*5))

print(sum(stb_1_20000.num_tracked_ghost[39:44])/(200*5))
print(sum(stb_11_20000.num_tracked_ghost[39:44])/(200*5))
print(sum(ens_1_20000.num_tracked_ghost[39:44])/(200*5))

print(sum(stb_2_30000.num_tracked_ghost[39:44])/(300*5))
print(sum(stb_21_30000.num_tracked_ghost[39:44])/(300*5))
print(sum(ens_2_30000.num_tracked_ghost[39:44])/(300*5))

print(sum(stb_3_40000.num_tracked_ghost[39:44])/(400*5))
print(sum(stb_31_40000.num_tracked_ghost[39:44])/(400*5))
print(sum(ens_3_40000.num_tracked_ghost[39:44])/(400*5))

###########################################################################################
ncyc=maxt-4
end_idx=None
itcyc=np.arange(5,5+ncyc)

linewidth=3
markersize=20
textfont=24
titlefont=40
tickfont=24
legendfont=15

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)    # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(12,9))

################################################################################
if plottype=="undetected":
	ylabel='Fraction'
	if dtobs==0.1:
		title='Fraction of Undetected Particles, '+r'3$\delta t$'
	else:
		title='Fraction of Undetected Particles, '+r'15$\delta t$'

	np_stb_1_parts_20000 = np.array(stb_1_20000.num_undetected_true_parts)/20000
	np_stb_11_parts_20000 = np.array(stb_11_20000.num_undetected_true_parts)/20000
	np_ens_1_parts_20000 = np.array(ens_1_20000.num_undetected_true_parts)/20000

	line1,=plt.plot(itcyc,np_stb_1_parts_20000[0:end_idx],'k-',lw=linewidth)
	line11,=plt.plot(itcyc,np_stb_11_parts_20000[0:end_idx],'b-',lw=linewidth)
	line1e,=plt.plot(itcyc,np_ens_1_parts_20000[0:end_idx],'r-',lw=linewidth)

	np_stb_2_parts_30000 = np.array(stb_2_30000.num_undetected_true_parts)/30000
	np_stb_21_parts_30000 = np.array(stb_21_30000.num_undetected_true_parts)/30000
	np_ens_2_parts_30000 = np.array(ens_2_30000.num_undetected_true_parts)/30000

	line2,=plt.plot(itcyc,np_stb_2_parts_30000[0:end_idx],'k--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line21,=plt.plot(itcyc,np_stb_21_parts_30000[0:end_idx],'b--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line2e,=plt.plot(itcyc,np_ens_2_parts_30000[0:end_idx],'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)

	np_stb_3_parts_40000 = np.array(stb_3_40000.num_undetected_true_parts)/40000
	np_stb_31_parts_40000 = np.array(stb_31_40000.num_undetected_true_parts)/40000
	np_ens_3_parts_40000 = np.array(ens_3_40000.num_undetected_true_parts)/40000

	line3,=plt.plot(itcyc,np_stb_3_parts_40000[0:end_idx],'k:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line31,=plt.plot(itcyc,np_stb_31_parts_40000[0:end_idx],'b:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line3e,=plt.plot(itcyc,np_ens_3_parts_40000[0:end_idx],'r:',lw=linewidth, markerfacecolor='none', markersize=markersize)
################################################################################
if plottype=="error":
	ylabel='Errors (px)'
	if dtobs==0.1:
		title='Mean error of detected particles, '+r'3$\delta t$'
	else:
		title='Mean error of detected particles, '+r'15$\delta t$'

	np_stb_1_parts_20000 = np.array(stb_1_20000.mean_error_of_detected_true_parts)
	np_stb_11_parts_20000 = np.array(stb_11_20000.mean_error_of_detected_true_parts)
	np_ens_1_parts_20000 = np.array(ens_1_20000.mean_error_of_detected_true_parts)

	line1,=plt.plot(itcyc,np_stb_1_parts_20000[0:end_idx],'k-',lw=linewidth)
	line11,=plt.plot(itcyc,np_stb_11_parts_20000[0:end_idx],'b-',lw=linewidth)
	line1e,=plt.plot(itcyc,np_ens_1_parts_20000[0:end_idx],'r-',lw=linewidth)

	np_stb_2_parts_30000 = np.array(stb_2_30000.mean_error_of_detected_true_parts)
	np_stb_21_parts_30000 = np.array(stb_21_30000.mean_error_of_detected_true_parts)
	np_ens_2_parts_30000 = np.array(ens_2_30000.mean_error_of_detected_true_parts)

	line2,=plt.plot(itcyc,np_stb_2_parts_30000[0:end_idx],'k--',lw=linewidth)
	line21,=plt.plot(itcyc,np_stb_21_parts_30000[0:end_idx],'b--',lw=linewidth)
	line2e,=plt.plot(itcyc,np_ens_2_parts_30000[0:end_idx],'r--',lw=linewidth)

	np_stb_3_parts_40000 = np.array(stb_3_40000.mean_error_of_detected_true_parts)
	np_stb_31_parts_40000 = np.array(stb_31_40000.mean_error_of_detected_true_parts)
	np_ens_3_parts_40000 = np.array(ens_3_40000.mean_error_of_detected_true_parts)

	line3,=plt.plot(itcyc,np_stb_3_parts_40000[0:end_idx],'k:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line31,=plt.plot(itcyc,np_stb_31_parts_40000[0:end_idx],'b:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line3e,=plt.plot(itcyc,np_ens_3_parts_40000[0:end_idx],'r:',lw=linewidth, markerfacecolor='none', markersize=markersize)
#################################################################################
if plottype=="undetected" or plottype=="error":
	line1.set_label(r'PPP 0.05, STB Davis')
	line11.set_label(r'PPP 0.05, STB in-house')
	line1e.set_label(r'PPP 0.05, KLPT')

	line2.set_label(r'PPP 0.075, STB Davis')
	line21.set_label(r'PPP 0.075, STB in-house')
	line2e.set_label(r'PPP 0.075, KLPT')

	line3.set_label(r'PPP 0.1, STB Davis')
	line31.set_label(r'PPP 0.1, STB in-house')
	line3e.set_label(r'PPP 0.1, KLPT')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width, box.height])
	# ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

	if plottype=="error":
		plt.legend(bbox_to_anchor=(1, 1),loc='upper right',prop={'size':legendfont})
	if plottype=="undetected":
		plt.legend(bbox_to_anchor=(1, 1),loc='upper right',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel(ylabel)
	plt.title(title)
###############################################################################
if plottype=="trackedghost":
	if dtobs==0.1:
		title='Fraction of Tracked ghost particles, '+r'3$\delta t$'
	else:
		title='Fraction of Tracked ghost particles, '+r'15$\delta t$'

	np_stb_1_20000 = np.array(stb_1_20000.num_tracked_ghost)/20000
	np_stb_11_20000 = np.array(stb_11_20000.num_tracked_ghost)/20000
	np_ens_1_20000 = np.array(ens_1_20000.num_tracked_ghost)/20000

	np_stb_2_30000 = np.array(stb_2_30000.num_tracked_ghost)/30000
	np_stb_21_30000 = np.array(stb_21_30000.num_tracked_ghost)/30000
	np_ens_2_30000 = np.array(ens_2_30000.num_tracked_ghost)/30000

	np_stb_3_40000 = np.array(stb_3_40000.num_tracked_ghost)/40000
	np_stb_31_40000 = np.array(stb_31_40000.num_tracked_ghost)/40000
	np_ens_3_40000 = np.array(ens_3_40000.num_tracked_ghost)/40000
################################################################################
if plottype=="trackedghost":
	line1,=plt.plot(itcyc,np_stb_1_20000[0:end_idx],'k-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line11,=plt.plot(itcyc,np_stb_11_20000[0:end_idx],'b-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line1e,=plt.plot(itcyc,np_ens_1_20000[0:end_idx],'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)

	line2,=plt.plot(itcyc,np_stb_2_30000[0:end_idx],'k--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line21,=plt.plot(itcyc,np_stb_21_30000[0:end_idx],'b--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line2e,=plt.plot(itcyc,np_ens_2_30000[0:end_idx],'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)

	line3,=plt.plot(itcyc,np_stb_3_40000[0:end_idx],'k:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line31,=plt.plot(itcyc,np_stb_31_40000[0:end_idx],'b:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line3e,=plt.plot(itcyc,np_ens_3_40000[0:end_idx],'r:',lw=linewidth, markerfacecolor='none', markersize=markersize)

	line1.set_label(r'PPP 0.05, STB Davis')
	line11.set_label(r'PPP 0.05, STB in-house')
	line1e.set_label(r'PPP 0.05, KLPT')

	line2.set_label(r'PPP 0.075, STB Davis')
	line21.set_label(r'PPP 0.075, STB in-house')
	line2e.set_label(r'PPP 0.075, KLPT')

	line3.set_label(r'PPP 0.1, STB Davis')
	line31.set_label(r'PPP 0.1, STB in-house')
	line3e.set_label(r'PPP 0.1, KLPT')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width, box.height])
	# ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

	plt.legend(bbox_to_anchor=(1, 1),loc='upper right',prop={'size':legendfont})
	# plt.legend(bbox_to_anchor=(1, 1),loc='upper left',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel('Fraction')
	plt.title(title)
#############################################################################
res_filename='/home/yin/'+dtobs_str+'_'+plottype+thin_str+'_3methods.eps'
plt.savefig(res_filename, format='eps', dpi=1500, bbox_inches='tight')
plt.show()