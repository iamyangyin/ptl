import numpy as np
import xml.etree.ElementTree as ET
import cv2
from davismf import Camera

Xc_topX	= 290.
Xc_topY	= 90.
Xc_topZ	= 47.123890
Xc_bottomX = 0.
Xc_bottomY = 0.
Xc_bottomZ = 0.
nX = 5
nY = 5
nZ = 2

xgrid_psf_ext=np.linspace(Xc_bottomX, Xc_topX, num=nX+1)
ygrid_psf_ext=np.linspace(Xc_bottomY, Xc_topY, num=nY+1)
zgrid_psf_ext=np.linspace(Xc_bottomZ, Xc_topZ, num=nZ+1)

print('xgrid_psf_ext {0}'.format(xgrid_psf_ext))
print('ygrid_psf_ext {0}'.format(ygrid_psf_ext))
print('zgrid_psf_ext {0}'.format(zgrid_psf_ext))

xgrid_psf=np.zeros(nX)
ygrid_psf=np.zeros(nY)
zgrid_psf=np.zeros(nZ)

for i in range(nX):
	xgrid_psf[i]=(xgrid_psf_ext[i+1]+xgrid_psf_ext[i])/2.
	ygrid_psf[i]=(ygrid_psf_ext[i+1]+ygrid_psf_ext[i])/2.

for i in range(nZ):
	zgrid_psf[i]=(zgrid_psf_ext[i+1]+zgrid_psf_ext[i])/2.

print('xgrid_psf {0}'.format(xgrid_psf))
print('ygrid_psf {0}'.format(ygrid_psf))
print('zgrid_psf {0}'.format(zgrid_psf))

n_cam=4
cam=[]

for i in range(n_cam):
	cam.append( Camera(i) )

# k=4. #arbitrary scaliing factor 
# alpha=1./k

X_factor=Xc_topX/200
Y_factor=-Xc_topY/200
Z_factor=(Xc_topZ-Xc_bottomZ)/2.

X_offset=0.
Y_offset=Xc_topY
Z_offset=(Xc_topZ+Xc_bottomZ)/4.

otf_file="/Users/yin.yang/OpticalTransferFunction.xml"
otf_tree=ET.parse(otf_file);
root=otf_tree.getroot()

for child in root.iter('ScaleX'):
	for a in child.iter('Offset'):
		a.attrib['value']=str(X_offset)
	for a in child.iter('Factor'):
		a.attrib['value']=str(X_factor)

for child in root.iter('ScaleY'):
	for a in child.iter('Offset'):
		a.attrib['value']=str(Y_offset)
	for a in child.iter('Factor'):
		a.attrib['value']=str(Y_factor)

for child in root.iter('ScaleZ'):
	for a in child.iter('Offset'):
		a.attrib['value']=str(Z_offset)
	for a in child.iter('Factor'):
		a.attrib['value']=str(Z_factor)

for child in root.iter('Camera'):
	for childchild in child.iter('Subvolume'):
		# for a in childchild.iter('x0'):
		# 	a.attrib['value']="0"
		# for a in childchild.iter('y0'):
		# 	a.attrib['value']="0"
		for a in childchild.iter('a'):
			a.attrib['value']="6"
		for a in childchild.iter('b'):
			a.attrib['value']="0"
		for a in childchild.iter('c'):
			a.attrib['value']="6"
		for a in childchild.iter('intensity'):
			a.attrib['value']="1"

otf_tree.write('/media/yin/DISK_/ForDavis/Properties/OpticalTransferFunction/OpticalTransferFunction.xml')