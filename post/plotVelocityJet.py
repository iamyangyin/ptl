import matplotlib.pyplot as plt
import numpy as np
import os
import matplotlib.cm as cm
from matplotlib.colors import Normalize

root_path=os.environ['STBENSPTV_PATH']

num_part='000000'

dtobs='0.00'

run_index='ENS_Vdebug_0'

result_dir=root_path+'/result/lavisionv/part'+num_part+'_maxt0010_fct00010_bkg0.000_psf1_dtobs'+dtobs+'/'+run_index+'/'

ref_flag=True
ana_flag=not(ref_flag)

cmap = plt.get_cmap('hot')

if ref_flag:
	velo_file_ref=result_dir+'velo/B0009.dat'
	f=np.loadtxt(velo_file_ref)
	print(f.shape)
	x=f[:,0]
	y=f[:,1]
	z=f[:,2]
	u=f[:,3]
	v=f[:,4]
	w=f[:,5]

	nx=25
	ny=64
	nz=38

	half_nz=18

	x_center_plan=x[nx*ny*half_nz:nx*ny*(half_nz+1)]
	y_center_plan=y[nx*ny*half_nz:nx*ny*(half_nz+1)]
	z_center_plan=z[nx*ny*half_nz:nx*ny*(half_nz+1)]

	xx_center_plan=x_center_plan.reshape((ny,nx))
	yy_center_plan=y_center_plan.reshape((ny,nx))
	zz_center_plan=z_center_plan.reshape((ny,nx))

	print(xx_center_plan.shape)
	print(yy_center_plan)
	print(zz_center_plan)

	u_center_plan=u[nx*ny*half_nz:nx*ny*(half_nz+1)]
	v_center_plan=v[nx*ny*half_nz:nx*ny*(half_nz+1)]
	w_center_plan=w[nx*ny*half_nz:nx*ny*(half_nz+1)]

	u_center_plan=u_center_plan.reshape((ny,nx))
	v_center_plan=v_center_plan.reshape((ny,nx))
	w_center_plan=w_center_plan.reshape((ny,nx))
	print(u_center_plan.shape)

	fig, ax = plt.subplots()
	ax.set_title('Velocity')
	
	plt.pcolormesh(xx_center_plan, yy_center_plan, w_center_plan, cmap=cmap)
	# plt.grid(True)
	plt.colorbar()

	q = ax.quiver(xx_center_plan, yy_center_plan, u_center_plan, v_center_plan)
	ax.quiverkey(q, X=1.1, Y=0.6, U=0.1, label='Plane Velocity', labelpos='E')

	plt.axis('scaled')
	plt.xlabel('X[mm]')
	plt.ylabel('Y[mm]')
	plt.show()

elif ana_flag:
	velo_file_ana=result_dir+'velo/Velocity10ana.txt'
	f=np.loadtxt(velo_file_ana)
	print(f.shape)
	x=f[:,0]
	y=f[:,1]
	z=f[:,2]
	u=f[:,3]
	v=f[:,4]
	w=f[:,5]

	# nx=7
	# ny=17
	# nz=3

	nx=25
	ny=65
	nz=9

	half_nz=nz//2
	print(half_nz)

	u_center_plan=u[nx*ny*half_nz:nx*ny*(half_nz+1)]
	v_center_plan=v[nx*ny*half_nz:nx*ny*(half_nz+1)]
	w_center_plan=w[nx*ny*half_nz:nx*ny*(half_nz+1)]

	print(u_center_plan)
	print(v_center_plan)
	print(w_center_plan)

	u_center_plan=u_center_plan.reshape((ny,nx))
	v_center_plan=v_center_plan.reshape((ny,nx))
	w_center_plan=w_center_plan.reshape((ny,nx))
	print(u_center_plan.shape)

	Xc_topX=24.0323
	Xc_topY=22
	Xc_topZ=12.0041
	Xc_bottomX=8
	Xc_bottomY=-19.0475
	Xc_bottomZ=-12.0041

	dXc_X=(Xc_topX-Xc_bottomX)/(nx)
	dXc_Y=(Xc_topY-Xc_bottomY)/(ny)
	dXc_Z=(Xc_topZ-Xc_bottomZ)/(nz)

	Xgrid=np.arange(0,nx+1,1)*dXc_X+Xc_bottomX
	Xgridc=Xgrid+dXc_X/2.
	Xgridc=Xgridc[:-1]
	print(Xgridc)

	Ygrid=np.arange(0,ny+1,1)*dXc_Y+Xc_bottomY
	Ygridc=Ygrid+dXc_Y/2.
	Ygridc=Ygridc[:-1]
	print(Ygridc)

	Zgrid=np.arange(0,nz+1,1)*dXc_Z+Xc_bottomZ
	Zgridc=Zgrid+dXc_Z/2.
	Zgridc=Zgridc[:-1]
	print(Zgridc)

	xx_center_plan, yy_center_plan=np.meshgrid(Xgrid, Ygrid)
	print(xx_center_plan)
	print(yy_center_plan)

	fig, ax = plt.subplots()
	ax.set_title('Velocity')
	
	plt.pcolormesh(xx_center_plan, yy_center_plan, w_center_plan, cmap=cmap)
	# plt.grid(True)
	plt.colorbar()

	q = ax.quiver(xx_center_plan, yy_center_plan, u_center_plan, v_center_plan)
	ax.quiverkey(q, X=1.1, Y=0.6, U=0.1, label='Plane Velocity', labelpos='E')

	plt.axis('scaled')
	plt.xlabel('X[mm]')
	plt.ylabel('Y[mm]')
	plt.show()
