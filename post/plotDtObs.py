import matplotlib.pyplot as plt
import numpy as np
import os
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter

tobs=[0.05,0.1,0.15,0.2,0.25]

stb_avg_with_warm=[0.004923, 0.007398,  0.01135,  0.01702,  0.02383]
ens_avg_with_warm=[0.0001265,0.0004731, 0.001279, 0.001968, 0.003496]

numlost_stb=[0, 36, 291, 1079, 2005]
numlost_ens=[0, 27, 250, 901, 1804]

for i in range(len(numlost_stb)):
	numlost_stb[i]=float(numlost_stb[i])/10000.
	numlost_ens[i]=float(numlost_ens[i])/10000.

linewidth=4
markersize=15
textfont=24
titlefont=40
tickfont=20
legendfont=24

figsize_width=12
figsize_height=12

plt.rc('font', size=textfont)         # controls default text sizes
plt.rc('axes', titlesize=textfont)    # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)   # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)   # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont) # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig, ax=plt.subplots(figsize=(figsize_width,figsize_height))

# ax.xaxis.set_major_formatter(FormatStrFormatter('%.f'))
# ax.yaxis.set_major_formatter(FormatStrFormatter('%.e'))
# ax.yaxis.set_minor_formatter(FormatStrFormatter('%.e'))
plt.xticks(tobs)
ax.set_xlabel('$\Delta T_{obs}$')
ax.set_ylabel('Position errors (px)')
line0,=plt.plot(tobs,stb_avg_with_warm,'k-o',lw=linewidth,markersize=markersize)
line1,=plt.plot(tobs,ens_avg_with_warm,'r-o',lw=linewidth,markersize=markersize)
line0.set_label('STB Position error')
line1.set_label('Ours Position error')
ax.tick_params(axis='y')

ax2=ax.twinx()
ax2.set_ylabel('Fraction of undetected particles')
line2,=plt.plot(tobs,numlost_stb,'k:o',lw=linewidth,markersize=markersize,fillstyle='none')
line3,=plt.plot(tobs,numlost_ens,'r:o',lw=linewidth,markersize=markersize,fillstyle='none')
line2.set_label('STB Fraction of undetected particles')
line3.set_label('Ours Fraction of undetected particles')
ax2.set_yticks([0,0.05,0.1,0.15,0.2])
ax2.tick_params(axis='y')

fig.tight_layout()
ax2.legend(loc='upper left',bbox_to_anchor=(0,1),prop={'size':legendfont})
ax.legend(loc='upper left',bbox_to_anchor=(0,0.85),prop={'size':legendfont})
# box=ax.get_position()
# ax.set_position([box.x0, box.y0, box.width*0.75, box.height])
# plt.autoscale(enable=True, axis='x', tight=True)
# plt.xlim(18,52)

plt.subplots_adjust(left=0.12, bottom=0.07, right=0.9, top=0.95, wspace=None, hspace=None)
res_filename='/Users/yin.yang/Documents/Frac_Undetected.eps'
plt.savefig(res_filename, formabt='eps', dpi=1500)
plt.show()
