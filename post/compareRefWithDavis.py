import numpy as np
import os
import argparse
import time
from compareTwoListsOfParticles import Particle, read_dat_file, read_ref_file, find_particles_tupel_from_ref, compute_mean_error

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--numpart", default="", help="name number of particles")
parser.add_argument("-t", "--stbdir", default="", help="name stb data dir")
parser.add_argument("-s", "--stbtype", default="", help="name stb data type")
parser.add_argument("-f", "--refdir", default="", help="name ref data dir")
parser.add_argument("-e", "--ensdir", default="", help="name ens result dir")
parser.add_argument("-r", "--runindex", default="", help="name ens run index")
args=parser.parse_args()

davis_flag=False
ens_flag=True
stb_flag=False

it_max_case = 10
it_max = 10
it_fct = 10

assert (it_max <= it_max_case)

error_threshold_davis = 0.01
error_threshold_ens   = 0.01

dtobs = '0.05'
psf = '1'

num_part = args.numpart
data_dir=args.stbdir
data_suffix=args.stbtype
ref_dir=args.refdir
ens_dir=args.ensdir

if num_part=='001000':
	data_num='01'
elif num_part=='004000':
	data_num='02'
elif num_part=='010000':
	data_num='03'
elif num_part=='020000':
	data_num='04'
elif num_part=='030000':
	data_num='05'
elif num_part=='040000':
	data_num='06'
elif num_part=='050000':
	data_num='07'

data_dir=data_num+data_dir+'/'

if dtobs=='0.50' or dtobs=='0.10':
	dX_px=[0.26, 0.23, 0.41]
	case_name='old_data'
	psf='bkg0.0'
elif dtobs=='0.05' or dtobs=='0.25':
	dX_px=[0.062, 0.058, 0.106]
	case_name='les3900rect'
	psf='psf'+psf

data_name='ShakeTheBox-tr'+data_suffix

davis_path='/Users/yin.yang/Documents/Ensemble_reduced_Davis/2016-06-24-SOL-CONV_Re1250/CONV-2D_Re1250_Sol_F500/ImgPreproc_'+data_dir
davis_file=davis_path+data_name+'/ExportToTecplot/'+data_name+'.dat'
print('davis_file\n{}'.format(davis_file))

ens_run_index='debug'+args.runindex
ensptv_path='/Users/yin.yang/STB-ENS-PTV-C++/result/lavision_bkgonly/part'+num_part+'_maxt00'+str(it_max_case)+'_fct00010_bkg0.000_'+psf+'_dtobs'+dtobs+ens_dir+'/ENS'+ens_run_index
ensptv_file=ensptv_path+'/ENS.dat'
print('LaPIV ens_file\n{}'.format(ensptv_file))

if stb_flag:
	ens_run_index2='debug_vel5_otf1_111_int1-3_v3'
	# stbptv_path='/Users/yin.yang/STB-ENS-PTV-C++/result/lavision_bkgonly/part'+num_part+'_maxt00'+str(it_max_case)+'_fct00010_bkg0.000_'+psf+'_dtobs'+dtobs+ens_dir+'/STB'+ens_run_index
	stbptv_path='/Users/yin.yang/STB-ENS-PTV-C++/result/lavision_bkgonly/part'+num_part+'_maxt00'+str(it_max_case)+'_fct00010_bkg0.000_'+psf+'_dtobs'+dtobs+ens_dir+'/ENS'+ens_run_index2
	stbptv_file=stbptv_path+'/ENS.dat'
	print('LaPIV stb_file\n{}'.format(stbptv_file))


ref_path='/Users/yin.yang/STB-ENS-PTV-C++/data/'+case_name+'/part'+num_part+'_maxt0050_fct00010_bkg0.000_'+psf+'_dtobs'+dtobs+'_STB'+ref_dir
print('ref_file\n{}'.format(ref_path))

[particles_ref, list_ref_it_deb, list_ref_it_fin]=read_ref_file(ref_path, it_max, verbal=False)

if davis_flag:
	print('compute DAVIS STB error')
	[particles_davis, list_davis_it_deb, list_davis_it_fin]=read_dat_file(davis_file, it_max, verbal=False)
	particles_dict_davis_ref=find_particles_tupel_from_ref(particles_ref, list_ref_it_deb, list_ref_it_fin, particles_davis, list_davis_it_deb, list_davis_it_fin,\
															it_fct, it_max, error_threshold_davis, verbal=False)
	[mean_error_davis, mean_error_davis_px, num_part_davis]=compute_mean_error(particles_dict_davis_ref, particles_davis, list_davis_it_deb, list_davis_it_fin,\
														 particles_ref, list_ref_it_deb, list_ref_it_fin, it_fct, it_max, dX_px)
	# print('mean_error_davis   : {}'.format(mean_error_davis))
	print('mean_error_davis_px: {}'.format(mean_error_davis_px))

if ens_flag:
	print('compute LAPTV ENS error')
	[particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin]=read_dat_file(ensptv_file, it_max, verbal=False)
	particles_tuple_lapiv_ref=find_particles_tupel_from_ref(particles_ref, list_ref_it_deb, list_ref_it_fin, particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin,\
															 it_fct, it_max, error_threshold_ens, verbal=False)
	[mean_error_lapiv, mean_error_lapiv_px, num_part_lapiv]=compute_mean_error(particles_tuple_lapiv_ref, particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin,\
														 particles_ref, list_ref_it_deb, list_ref_it_fin, it_fct, it_max, dX_px)
	# print('mean_error_lapiv   : {}'.format(mean_error_lapiv))
	print('mean_error_lapiv_px: {}'.format(mean_error_lapiv_px))

if stb_flag:
	print('compute LAPTV STB error')
	[particles_stb, list_stb_it_deb, list_stb_it_fin]=read_dat_file(stbptv_file, it_max, verbal=False)
	particles_tuple_stb_ref=find_particles_tupel_from_ref(particles_ref, list_ref_it_deb, list_ref_it_fin, particles_stb, list_stb_it_deb, list_stb_it_fin,\
															 it_fct, it_max, error_threshold_ens, verbal=False)
	[mean_error_stb, mean_error_stb_px, num_part_stb]=compute_mean_error(particles_tuple_stb_ref, particles_stb, list_stb_it_deb, list_stb_it_fin,\
														 particles_ref, list_ref_it_deb, list_ref_it_fin, it_fct, it_max, dX_px)
	# print('mean_error_stb   : {}'.format(mean_error_stb))
	print('mean_error_stb_px: {}'.format(mean_error_stb_px))

print("- total {} seconds -".format(time.time() - start_time))
