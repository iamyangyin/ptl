import numpy as np
import xml.etree.ElementTree as ET
import cv2
from davismf import Camera

## Set Davis 10 mapping function path 

mp_file="/Users/yin.yang/Documents/Tomo_PIV_LASIE/Properties/Calibration/Calibration.xml"

## Do not touch following lines unless you know what you are doing######################
n_cam=4
cam=[]

for i in range(n_cam):
	cam.append( Camera(i) )

print('Read Calibration file from Davis')

mp_tree=ET.parse(mp_file)
root = mp_tree.getroot()

for child in root.iter('CoordinateMapper'):
	if int(child.attrib['CameraIdentifier'])<=n_cam:
		i_cam=int(child.attrib['CameraIdentifier'])-1
		for a in child.iter('LinearScaleX'):
			alpha_X=float(a.attrib['FactorMmPerPixel'])		
			X_offset=float(a.attrib['OffsetMm'])
		for a in child.iter('LinearScaleY'):
			alpha_Y=float(a.attrib['FactorMmPerPixel'])		
			Y_offset=float(a.attrib['OffsetMm'])
		for a in child.iter('LinearScaleZ'):
			alpha_Z=float(a.attrib['FactorMmPerPixel'])		
			Z_offset=float(a.attrib['OffsetMm'])
		cam[i_cam].assign_scale_para(alpha_X, alpha_Y, alpha_Z, X_offset, Y_offset, Z_offset)

		for pm in child.iter('PolynomialMapping'):
			for a in pm.iter('ZPosition'):
				Z0=float(a.attrib['Value'])
			for a in pm.iter('Origin'):
				so=float(a.attrib['s_o'])
				to=float(a.attrib['t_o'])
			for a in pm.iter('NormalisationFactor'):
				nx=float(a.attrib['nx'])
				ny=float(a.attrib['ny'])	
			for a in pm.iter('CoefficientsA'):
				a_o=float(a.attrib['a_o'])
				a_s=float(a.attrib['a_s'])
				a_s2=float(a.attrib['a_s2'])
				a_s3=float(a.attrib['a_s3'])
				a_t=float(a.attrib['a_t'])
				a_t2=float(a.attrib['a_t2'])
				a_t3=float(a.attrib['a_t3'])
				a_st=float(a.attrib['a_st'])
				a_s2t=float(a.attrib['a_s2t'])
				a_st2=float(a.attrib['a_st2'])
			for a in pm.iter('CoefficientsB'):
				b_o=float(a.attrib['b_o'])
				b_s=float(a.attrib['b_s'])
				b_s2=float(a.attrib['b_s2'])
				b_s3=float(a.attrib['b_s3'])
				b_t=float(a.attrib['b_t'])
				b_t2=float(a.attrib['b_t2'])
				b_t3=float(a.attrib['b_t3'])
				b_st=float(a.attrib['b_st'])
				b_s2t=float(a.attrib['b_s2t'])
				b_st2=float(a.attrib['b_st2'])

			cam[i_cam].assign_mapping_function(Z0, so, to, nx, ny, 
												a_o, a_s, a_s2, a_s3, a_t, a_t2, a_t3, a_st, a_s2t, a_st2, 
												b_o, b_s, b_s2, b_s3, b_t, b_t2, b_t3, b_st, b_s2t, b_st2)

xb=0
xt=398
yb=1019
yt=0
zb=-248
zt=248

print('camera {0}'.format(cam[0].i_cam))

Xbottom = cam[2].alpha_X*xb + cam[2].X_offset
Xtop    = cam[2].alpha_X*xt + cam[2].X_offset

Ybottom = cam[2].alpha_Y*yb + cam[2].Y_offset
Ytop    = cam[2].alpha_Y*yt + cam[2].Y_offset

Zbottom = cam[2].alpha_Z*zb + cam[2].Z_offset
Ztop    = cam[2].alpha_Z*zt + cam[2].Z_offset

print('Xbottom {0}, Xtop {1}'.format(Xbottom,Xtop))
print('Ybottom {0}, Ytop {1}'.format(Ybottom,Ytop))
print('Zbottom {0}, Ztop {1}'.format(Zbottom,Ztop))

Xc=[8.381971000000000,-4.096231000000000,0.192742000000000]
X1=Xc[0] 
Y1=Xc[1] 
Z1=Xc[2]

print('(X1 {0}, Y1 {1}, Z1 {2})'.format(X1,Y1,Z1))
for ic in cam:
	print('###########################################')
	ic.project_particle(X1, Y1, Z1, True)

# print('For Optical Transfer Function')

# nX = 5
# nY = 5
# nZ = 2

# xgrid_psf_ext=np.linspace(Xbottom, Xtop, num=nX+1)
# ygrid_psf_ext=np.linspace(Ybottom, Ytop, num=nY+1)
# zgrid_psf_ext=np.linspace(Zbottom, Ztop, num=nZ+1)

# xgrid_psf=np.zeros(nX)
# ygrid_psf=np.zeros(nY)
# zgrid_psf=np.zeros(nZ)

# for i in range(nX):
# 	xgrid_psf[i]=(xgrid_psf_ext[i+1]+xgrid_psf_ext[i])/2.
# 	ygrid_psf[i]=(ygrid_psf_ext[i+1]+ygrid_psf_ext[i])/2.

# for i in range(nZ):
# 	zgrid_psf[i]=(zgrid_psf_ext[i+1]+zgrid_psf_ext[i])/2.

# print('xgrid_psf {0}'.format(xgrid_psf))
# print('ygrid_psf {0}'.format(ygrid_psf))
# print('zgrid_psf {0}'.format(zgrid_psf))

# X1=xgrid_psf[0]
# Y1=ygrid_psf[-1]
# Z1=zgrid_psf[0]

# print('(X1 {0}, Y1 {1}, Z1 {2})'.format(X1,Y1,Z1))
# cam[0].project_particle(X1, Y1, Z1, True)

# X1=xgrid_psf[0]
# Y1=ygrid_psf[-1]
# Z1=zgrid_psf[1]

# print('(X1 {0}, Y1 {1}, Z2 {2})'.format(X1,Y1,Z1))
# cam[0].project_particle(X1, Y1, Z1, True)

# otf_file="/Users/yin.yang/OpticalTransferFunction.xml"
# otf_tree=ET.parse(otf_file);
# root=otf_tree.getroot()

# for child in root.iter('ScaleX'):
# 	for a in child.iter('Offset'):
# 		X_offset=float(a.attrib['value'])
# 	for a in child.iter('Factor'):
# 		X_factor=float(a.attrib['value'])

# for child in root.iter('ScaleY'):
# 	for a in child.iter('Offset'):
# 		Y_offset=float(a.attrib['value'])
# 	for a in child.iter('Factor'):
# 		Y_factor=float(a.attrib['value'])

# for child in root.iter('ScaleZ'):
# 	for a in child.iter('Offset'):
# 		Z_offset=float(a.attrib['value'])
# 	for a in child.iter('Factor'):
# 		Z_factor=float(a.attrib['value'])

# print('X_offset {0}, X_factor {1}'.format(X_offset,X_factor))
# print('Y_offset {0}, Y_factor {1}'.format(Y_offset,Y_factor))
# print('Z_offset {0}, Z_factor {1}'.format(Z_offset,Z_factor))

# xb_otf=0
# xt_otf=200
# yb_otf=200
# yt_otf=0
# zb_otf=0
# zt_otf=200

# Xb_otf=X_factor*xb_otf+X_offset
# Xt_otf=X_factor*xt_otf+X_offset
# Yb_otf=Y_factor*yb_otf+Y_offset
# Yt_otf=Y_factor*yt_otf+Y_offset
# Zb_otf=Z_factor*zb_otf+Z_offset
# Zt_otf=Z_factor*zt_otf+Z_offset

# print('Xb_otf {0}, Xt_otf {1}'.format(Xb_otf,Xt_otf))
# print('Yb_otf {0}, Yt_otf {1}'.format(Yb_otf,Yt_otf))
# print('Zb_otf {0}, Zt_otf {1}'.format(Zb_otf,Zt_otf))

# x0=np.empty([4, 50])
# y0=np.empty([4, 50])
# a=np.empty([4, 50])
# b=np.empty([4, 50])
# c=np.empty([4, 50])
# intensity=np.empty([4, 50])

# i_cam=0
# for child in root.iter('Camera'):
# 	sub_v=0
# 	for childchild in child.iter('Subvolume'):
# 		for aa in childchild.iter('x0'):
# 			x0[i_cam][sub_v]=float(aa.attrib['value'])
# 		for aa in childchild.iter('y0'):
# 			y0[i_cam][sub_v]=float(aa.attrib['value'])
# 		for aa in childchild.iter('a'):
# 			a[i_cam][sub_v]=float(aa.attrib['value'])
# 		for aa in childchild.iter('b'):
# 			b[i_cam][sub_v]=float(aa.attrib['value'])
# 		for aa in childchild.iter('c'):
# 			c[i_cam][sub_v]=float(aa.attrib['value'])
# 		for aa in childchild.iter('intensity'):
# 			intensity[i_cam][sub_v]=float(aa.attrib['value'])
# 		sub_v+=1
# 	i_cam+=1

# for i in range(50):
# 	x0_otf=X_factor*x0[0][i]+X_offset
# 	y0_otf=Y_factor*y0[0][i]+Y_offset
# 	print('x0 {0}, y0 {1}, x0_otf {2}, y0_otf {3}'.format(x0[0][i],y0[0][i],x0_otf,y0_otf))
