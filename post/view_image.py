import cv2
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--icam", default='0')
parser.add_argument("-x", "--xidx", default='0')
parser.add_argument("-y", "--yidx", default='0')

args=parser.parse_args()

img_path = '/media/yin/Data/stb-ens-ptv-c/data/lavision_re3900exp/part015000_maxt0100_fct00005/'
img = img_path+'/cam'+args.icam+'_preproc/cam_c'+args.icam+'_it001.png'

img = cv2.imread(img, -1)

img = img.transpose()

# print(np.amax(img))

x = int(args.xidx)
y = int(args.yidx)

img_roi = img[x:x+5, y:y+5]
print(img_roi)

# for i in range(4):
# 	img = img_path+'/cam'+str(i)+'_preproc/cam_c'+str(i)+'_it001.png'
# 	img = cv2.imread(img, -1)

# 	mean, stddev = cv2.meanStdDev( img )
# 	print('mean {}, stddev {}'.format(mean, stddev))