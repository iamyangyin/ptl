import matplotlib.pyplot as plt
import numpy as np
import os
import cv2
from target_file import target_path

small_flag=False
dtobs='0.05'
over_lap='75%'
smooth_flag=True
suffix=''
# suffix='_coaser_v2'

target=target_path(small_flag, dtobs, over_lap, suffix)

velo_file_ref=target.result_dir+'/velo/Velocity10ref.txt'
velo_file_bkg=target.result_dir+'/velo/Velocity10bkg.txt'
velo_file_ana=target.result_dir+'/velo/Velocity10ana.txt'

nx=target.nx
ny=target.ny
nz=target.nz

xx, yy, zz = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))
print('xx.shape {}'.format(xx.shape))

pi=3.1415
if small_flag:
	D=48
	Xc_topX=72
	Xc_topY=72
	Xc_topZ=35.557522
	Xc_bottomX=0
	Xc_bottomY=0
	Xc_bottomZ=19.849556
else:
	D=12
	Xc_topX=72.5
	Xc_topY=22.5
	Xc_topZ=11.78098
	Xc_bottomX=0
	Xc_bottomY=0
	Xc_bottomZ=0

dx=(Xc_topX-Xc_bottomX)/(nx-1)
dy=(Xc_topY-Xc_bottomY)/(ny-1)
dz=(Xc_topZ-Xc_bottomZ)/(nz-1)

#load velocity data from file
f=np.loadtxt(velo_file_ref)
u=f[:,3]
v=f[:,4]
w=f[:,5]
print('f.shape {}'.format(f.shape))
print('v.shape {}'.format(v.shape))

f_bkg=np.loadtxt(velo_file_bkg)
u_bkg=f_bkg[:,3]
v_bkg=f_bkg[:,4]
w_bkg=f_bkg[:,5]

f_ana=np.loadtxt(velo_file_ana)
u_ana=f_ana[:,3]
v_ana=f_ana[:,4]
w_ana=f_ana[:,5]

UU = np.zeros( xx.shape )
VV = np.zeros( xx.shape )
WW = np.zeros( xx.shape )
UU_bkg=np.zeros( xx.shape )
VV_bkg=np.zeros( xx.shape )
WW_bkg=np.zeros( xx.shape )
UU_ana=np.zeros( xx.shape )
VV_ana=np.zeros( xx.shape )
WW_ana=np.zeros( xx.shape )

print('nx {}, ny {}, nz {}'.format(nx, ny, nz))

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			UU[j,i,k]=u[i+j*nx+k*nx*ny]
			VV[j,i,k]=v[i+j*nx+k*nx*ny]
			WW[j,i,k]=w[i+j*nx+k*nx*ny]
			UU_bkg[j,i,k]=u_bkg[i+j*nx+k*nx*ny]
			VV_bkg[j,i,k]=v_bkg[i+j*nx+k*nx*ny]
			WW_bkg[j,i,k]=w_bkg[i+j*nx+k*nx*ny]
			UU_ana[j,i,k]=u_ana[i+j*nx+k*nx*ny]
			VV_ana[j,i,k]=v_ana[i+j*nx+k*nx*ny]
			WW_ana[j,i,k]=w_ana[i+j*nx+k*nx*ny]

#compute velocity norm
V_norm = np.zeros( xx.shape )
V_ana_norm = np.zeros( xx.shape )
V_bkg_norm = np.zeros( xx.shape )
V_norm=np.sqrt(np.square(UU)+np.square(VV)+np.square(WW))
V_ana_norm=np.sqrt(np.square(UU_ana)+np.square(VV_ana)+np.square(WW_ana))
V_bkg_norm=np.sqrt(np.square(UU_bkg)+np.square(VV_bkg)+np.square(WW_bkg))

#compute smoothed analysis velocity
if smooth_flag:
	UU_ana_smth = np.zeros( xx.shape )
	VV_ana_smth = np.zeros( xx.shape )
	WW_ana_smth = np.zeros( xx.shape )

	alpha=1
	blur_kernel_shape=(15,15)
	gaussian_kernel_shape=(3,3)
	median_kernel_size=3

	for k in np.arange(0, nz, 1):
		UU_ana_plane= UU_ana[:,:,k]
		UU_ana_blur=cv2.blur(UU_ana_plane,blur_kernel_shape)
		# img = UU_ana_plane.astype('float32')
		# UU_ana_blur=cv2.medianBlur(img,median_kernel_size)
		delta_UU_ana=4*alpha/(1+4*alpha)*(UU_ana_plane-UU_ana_blur)
		UU_ana_smth[:,:,k]=UU_ana_plane+delta_UU_ana
		UU_ana_smth[:,:,k]=cv2.GaussianBlur(UU_ana_smth[:,:,k],gaussian_kernel_shape,0)
		# img = UU_ana_smth[:,:,k].astype('float32')
		# UU_ana_smth[:,:,k]=cv2.medianBlur(img,median_kernel_size)

		VV_ana_plane= VV_ana[:,:,k]
		VV_ana_blur=cv2.blur(VV_ana_plane,blur_kernel_shape)
		# img = VV_ana_plane.astype('float32')
		# VV_ana_blur=cv2.medianBlur(img,median_kernel_size)
		delta_VV_ana=4*alpha/(1+4*alpha)*(VV_ana_plane-VV_ana_blur)
		VV_ana_smth[:,:,k]=VV_ana_plane+delta_VV_ana
		VV_ana_smth[:,:,k]=cv2.GaussianBlur(VV_ana_smth[:,:,k],gaussian_kernel_shape,0)
		# img = VV_ana_smth[:,:,k].astype('float32')
		# VV_ana_smth[:,:,k]=cv2.medianBlur(img,median_kernel_size)

		WW_ana_plane= WW_ana[:,:,k]
		WW_ana_blur=cv2.blur(WW_ana_plane,blur_kernel_shape)
		# img = WW_ana_plane.astype('float32')
		# WW_ana_blur=cv2.medianBlur(img,median_kernel_size)
		delta_WW_ana=4*alpha/(1+4*alpha)*(WW_ana_plane-WW_ana_blur)
		WW_ana_smth[:,:,k]=WW_ana_plane+delta_WW_ana
		WW_ana_smth[:,:,k]=cv2.GaussianBlur(WW_ana_smth[:,:,k],gaussian_kernel_shape,0)
		# img = WW_ana_smth[:,:,k].astype('float32')
		# WW_ana_smth[:,:,k]=cv2.medianBlur(img,median_kernel_size)

	V_ana_norm_smth = np.zeros( xx.shape )
	V_ana_norm_smth=np.sqrt(np.square(UU_ana_smth)+np.square(VV_ana_smth)+np.square(WW_ana_smth))

#compute vorticity
wx=np.zeros( xx.shape )
wy=np.zeros( xx.shape )
wz=np.zeros( xx.shape )
wx_bkg=np.zeros( xx.shape )
wy_bkg=np.zeros( xx.shape )
wz_bkg=np.zeros( xx.shape )
wx_ana=np.zeros( xx.shape )
wy_ana=np.zeros( xx.shape )
wz_ana=np.zeros( xx.shape )

Uy=np.zeros( xx.shape )
Uz=np.zeros( xx.shape )
Vx=np.zeros( xx.shape )
Vz=np.zeros( xx.shape )
Wx=np.zeros( xx.shape )
Wy=np.zeros( xx.shape )

for l in range(3):
	if l==0:
		Utest=UU;
		Vtest=VV;
		Wtest=WW;
	elif l==1:
		Utest=UU_bkg;
		Vtest=VV_bkg;
		Wtest=WW_bkg;
	elif l==2:
		Utest=UU_ana;
		Vtest=VV_ana;
		Wtest=WW_ana;

	for k in np.arange(0, nz, 1):
		for i in np.arange(0, nx, 1):
			for j in np.arange(0, ny, 1):
				if i>0:
					Vx[j,i,k]=(Vtest[j,i,k]-Vtest[j,i-1,k])/dx;
					Wx[j,i,k]=(Wtest[j,i,k]-Wtest[j,i-1,k])/dx;
				else:
					Vx[j,0,k]=(Vtest[j,1,k]-Vtest[j,0,k])/dx;
					Wx[j,0,k]=(Wtest[j,1,k]-Wtest[j,0,k])/dx;

				if j>0:
					Uy[j,i,k]=(Utest[j,i,k]-Utest[j-1,i,k])/dy;
					Wy[j,i,k]=(Wtest[j,i,k]-Wtest[j-1,i,k])/dy;
				else:
					Uy[0,i,k]=(Utest[1,i,k]-Utest[0,i,k])/dy;
					Wy[0,i,k]=(Wtest[1,i,k]-Wtest[0,i,k])/dy;

				if k>0:
					Uz[j,i,k]=(Utest[j,i,k]-Utest[j,i,k-1])/dz;
					Vz[j,i,k]=(Vtest[j,i,k]-Vtest[j,i,k-1])/dz;
				else:
					Uz[j,i,0]=(Utest[j,i,1]-Utest[j,i,0])/dz;
					Vz[j,i,0]=(Vtest[j,i,1]-Vtest[j,i,0])/dz;

	if l==0:
		wx=Wy-Vz;
		wy=Uz-Wx;
		wz=Vx-Uy;
	elif l==1:
		wx_bkg=Wy-Vz;
		wy_bkg=Uz-Wx;
		wz_bkg=Vx-Uy;
	elif l==2:
		wx_ana=Wy-Vz;
		wy_ana=Uz-Wx;
		wz_ana=Vx-Uy;

vort_file_ref=target.result_dir+'/velo/Vorticity10ref.txt'
with open(vort_file_ref, "w") as the_file:
	the_file.write("#X Y Z NX NY NZ W_X W_Y W_Z W_n V_X V_Y V_Z V_n\n")
	for k in np.arange(0, nz, 1):
		for j in np.arange(0, ny, 1):
			for i in np.arange(0, nx, 1):
				ln=i+j*nx+k*nx*ny
				X=dx*i/D
				Y=dy*j/D
				Z=dz*k/D
				wn=(wx[j,i,k]**2+wy[j,i,k]**2+wz[j,i,k]**2)**(0.5)
				line="{0:1f} {1:1f} {2:.3f} {3:3f} {4:3f} {5:3f} {6:15.15f} {7:15.15f} {8:15.15f} {9:15.15f} {10:15.15f} {11:15.15f} {12:15.15f} {13:15.15f}".format(X,Y,Z,f[ln,0],f[ln,1],f[ln,2],wx[j,i,k],wy[j,i,k],wz[j,i,k],wn,f[ln,3],f[ln,4],f[ln,5],V_norm[j,i,k])
				the_file.write(line+'\n')

vort_file_bkg=target.result_dir+'/velo/Vorticity10bkg.txt'
with open(vort_file_bkg, "w") as the_file:
	the_file.write("#X Y Z NX NY NZ W_X W_Y W_Z W_n V_X V_Y V_Z V_n\n")
	for k in np.arange(0, nz, 1):
		for j in np.arange(0, ny, 1):
			for i in np.arange(0, nx, 1):
				ln=i+j*nx+k*nx*ny
				X=dx*i/D
				Y=dy*j/D
				Z=dz*k/D
				wn=(wx_bkg[j,i,k]**2+wy_bkg[j,i,k]**2+wz_bkg[j,i,k]**2)**(0.5)
				line="{0:1f} {1:1f} {2:.3f} {3:3f} {4:3f} {5:3f} {6:15.15f} {7:15.15f} {8:15.15f} {9:15.15f} {10:15.15f} {11:15.15f} {12:15.15f} {13:15.15f}".format(X,Y,Z,f_bkg[ln,0],f_bkg[ln,1],f_bkg[ln,2],wx_bkg[j,i,k],wy_bkg[j,i,k],wz_bkg[j,i,k],wn,f_bkg[ln,3],f_bkg[ln,4],f_bkg[ln,5],V_bkg_norm[j,i,k])
				the_file.write(line+'\n')

vort_file_ana=target.result_dir+'/velo/Vorticity10ana.txt'
with open(vort_file_ana, "w") as the_file:
	the_file.write("#X Y Z NX NY NZ W_X W_Y W_Z W_n V_X V_Y V_Z V_n\n")
	for k in np.arange(0, nz, 1):
		for j in np.arange(0, ny, 1):
			for i in np.arange(0, nx, 1):
				ln=i+j*nx+k*nx*ny
				X=dx*i/D
				Y=dy*j/D
				Z=dz*k/D
				wn=(wx_ana[j,i,k]**2+wy_ana[j,i,k]**2+wz_ana[j,i,k]**2)**(0.5)
				line="{0:1f} {1:1f} {2:.3f} {3:3f} {4:3f} {5:3f} {6:15.15f} {7:15.15f} {8:15.15f} {9:15.15f} {10:15.15f} {11:15.15f} {12:15.15f} {13:15.15f}".format(X,Y,Z,f_ana[ln,0],f_ana[ln,1],f_ana[ln,2],wx_ana[j,i,k],wy_ana[j,i,k],wz_ana[j,i,k],wn,f_ana[ln,3],f_ana[ln,4],f_ana[ln,5],V_ana_norm[j,i,k])
				the_file.write(line+'\n')

#compute smoothed vorticity
if smooth_flag:
	wx_ana_smth=np.zeros( xx.shape )
	wy_ana_smth=np.zeros( xx.shape )
	wz_ana_smth=np.zeros( xx.shape )

	Uy_smth=np.zeros( xx.shape )
	Uz_smth=np.zeros( xx.shape )
	Vx_smth=np.zeros( xx.shape )
	Vz_smth=np.zeros( xx.shape )
	Wx_smth=np.zeros( xx.shape )
	Wy_smth=np.zeros( xx.shape )

	Utest=UU_ana_smth;
	Vtest=VV_ana_smth;
	Wtest=WW_ana_smth;

	for k in np.arange(0, nz, 1):
		for i in np.arange(0, nx, 1):
			for j in np.arange(0, ny, 1):
				if i>0:
					Vx_smth[j,i,k]=(Vtest[j,i,k]-Vtest[j,i-1,k])/dx;
					Wx_smth[j,i,k]=(Wtest[j,i,k]-Wtest[j,i-1,k])/dx;
				else:
					Vx_smth[j,0,k]=(Vtest[j,1,k]-Vtest[j,0,k])/dx;
					Wx_smth[j,0,k]=(Wtest[j,1,k]-Wtest[j,0,k])/dx;

				if j>0:
					Uy_smth[j,i,k]=(Utest[j,i,k]-Utest[j-1,i,k])/dy;
					Wy_smth[j,i,k]=(Wtest[j,i,k]-Wtest[j-1,i,k])/dy;
				else:
					Uy_smth[0,i,k]=(Utest[1,i,k]-Utest[0,i,k])/dy;
					Wy_smth[0,i,k]=(Wtest[1,i,k]-Wtest[0,i,k])/dy;

				if k>0:
					Uz_smth[j,i,k]=(Utest[j,i,k]-Utest[j,i,k-1])/dz;
					Vz_smth[j,i,k]=(Vtest[j,i,k]-Vtest[j,i,k-1])/dz;
				else:
					Uz_smth[j,i,0]=(Utest[j,i,1]-Utest[j,i,0])/dz;
					Vz_smth[j,i,0]=(Vtest[j,i,1]-Vtest[j,i,0])/dz;

	wx_ana_smth=Wy_smth-Vz_smth;
	wy_ana_smth=Uz_smth-Wx_smth;
	wz_ana_smth=Vx_smth-Uy_smth;

	vort_file_ana=target.result_dir+'/velo/Vorticity10ana_smth.txt'

	with open(vort_file_ana, "w") as the_file:
		the_file.write("#X Y Z NX NY NZ W_X W_Y W_Z W_n V_X V_Y V_Z V_n\n")
		for k in np.arange(0, nz, 1):
			for j in np.arange(0, ny, 1):
				for i in np.arange(0, nx, 1):
					ln=i+j*nx+k*nx*ny
					X=dx*i/D
					Y=dy*j/D
					Z=dz*k/D
					wn=(wx_ana_smth[j,i,k]**2+wy_ana_smth[j,i,k]**2+wz_ana_smth[j,i,k]**2)**(0.5)
					line="{0:1f} {1:1f} {2:.3f} {3:3f} {4:3f} {5:3f} {6:15.15f} {7:15.15f} {8:15.15f} {9:15.15f} {10:15.15f} {11:15.15f} {12:15.15f} {13:15.15f}".format(X,Y,Z,f_ana[ln,0],f_ana[ln,1],f_ana[ln,2],wx_ana_smth[j,i,k],wy_ana_smth[j,i,k],wz_ana_smth[j,i,k],wn,UU_ana_smth[j,i,k],VV_ana_smth[j,i,k],WW_ana_smth[j,i,k],V_ana_norm_smth[j,i,k])
					the_file.write(line+'\n')
