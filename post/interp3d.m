clear
for it=21:30
    file_name = sprintf('/media/yin/Data/stb-ens-ptv-c/result/lptchal4500/part010000_maxt0050_fct00005/ENSdebug_new_trial_da_velo/Velocity_%03d.txt',it);
    file_name
    velo = importdata(file_name,' ',1);
    velo = table2array( velo(:,1:6) );

    nx=77;
    ny=37;
    nz=17;
    
%     nx=121;
%     ny=57;
%     nz=25;
% 
%     nx=241;
%     ny=117;
%     nz=57;
    
    X=linspace(-47.5, 47.5, nx);
    Y=linspace(-22.5, 22.5, ny);
    Z=linspace(3, 27, nz);

    X
    
    [XX,YY,ZZ]=meshgrid(X,Y,Z);

    for k = 1:nz
        for j = 1:ny
            for i = 1:nx
                U(i,j,k)=velo(i+(j-1)*nx+(k-1)*nx*ny,4);
                V(i,j,k)=velo(i+(j-1)*nx+(k-1)*nx*ny,5);
                W(i,j,k)=velo(i+(j-1)*nx+(k-1)*nx*ny,6);
            end
        end
    end

    P=[2,1,3];
    XX = permute(XX, P);
    YY = permute(YY, P);
    ZZ = permute(ZZ, P);
    U  = permute(U, P);
    V  = permute(V, P);
    W  = permute(W, P);

    FU=griddedInterpolant(XX,YY,ZZ,U,'linear','linear');
    FV=griddedInterpolant(XX,YY,ZZ,V,'linear','linear');
    FW=griddedInterpolant(XX,YY,ZZ,W,'linear','linear');

    nx_fine=145;
    ny_fine=145;
    nz_fine=108;
    Xf=linspace(0, 8, nx_fine);
    Yf=linspace(0, 8, ny_fine);
    Zf=linspace(0, 6, nz_fine);

    [XXf, YYf, ZZf]=meshgrid(Xf, Yf, Zf);
    XXf = permute(XXf, P);
    YYf = permute(YYf, P);
    ZZf = permute(ZZf, P);

    Uf=FU(XXf,YYf,ZZf);
    Vf=FV(XXf,YYf,ZZf);
    Wf=FW(XXf,YYf,ZZf);

    out_name = sprintf('Velo_3%04d.txt',10*(it-1));
    fileID = fopen(out_name,'w');
    fprintf(fileID,'# x y z Vx Vy Vz\n');
    fprintf(fileID,'# nx= 145 ny= 145 nz= 108\n');
    for k = 1:nz_fine
        for i = 1:nx_fine
            for j = 1:ny_fine
                fprintf(fileID,'%8.6e %8.6e %8.6e %8.6e %8.6e %8.6e\n', XXf(j,i,k), YYf(j,i,k), ZZf(j,i,k),...
                                                                        Uf(i,j,k), Vf(i,j,k), Wf(i,j,k));
            end
        end
    end
    fclose(fileID);
end