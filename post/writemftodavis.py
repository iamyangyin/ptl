import numpy as np
import xml.etree.ElementTree as ET
import cv2
import os
from davismf import Camera

n_cam=4
cam=[]
for i in range(n_cam):
	cam.append( Camera(i) )

#Case-specific configuration
case_name='lptchal4500'
num_part='010000'
maxt='0050'

if case_name=='les3900rect':
	dtobs='0.50'
	path_suffix='_Emin1.0_dmt2.4'

	Xc_topX	= 72.5		#290.
	Xc_topY	= 22.5		#90.
	Xc_topZ	= 11.78098	#47.123890
	Xc_bottomX = 0.
	Xc_bottomY = 0.
	Xc_bottomZ = 0.
	nZ_plane = 4

	#arbitrary scaling factor 
	k=16.#4.

	#arbitrary offset factor, between world coordinate and voxel coordinate
	X_offset=-3.75#-15.
	Y_offset=36.25#145. 
	Z_offset=0.

	Z_grid=np.linspace(Xc_bottomZ, Xc_topZ, num=nZ_plane)
	print('Z grid {0}'.format(Z_grid))

	mp_dir='../data/'+case_name+'/part'+num_part+'_maxt'+maxt+'_fct00005_bkg0.000_psf1_dtobs'+dtobs+path_suffix+'/camera';

elif case_name=='lptchal4500':
	dtobs='0.0006'

	Xc_topX	= 50
	Xc_topY	= 25
	Xc_topZ	= 30
	Xc_bottomX = -50
	Xc_bottomY = -25
	Xc_bottomZ = 0
	nZ_plane = 4

	#arbitrary scaling factor 
	k=16.6

	#arbitrary offset factor, between world coordinate and voxel coordinate
	X_offset=-56.0232
	Y_offset=36.144 
	Z_offset=0.

	Z_grid=np.linspace(Xc_bottomZ, Xc_topZ, num=nZ_plane)
	print('Z grid {0}'.format(Z_grid))

	mp_dir='../data/'+case_name+'/part'+num_part+'_maxt'+maxt+'_fct00005_dtobs'+dtobs+'/camera';

print(mp_dir)

k_X=k
k_Y=-k
k_Z=k

alpha_X=1./k_X
alpha_Y=1./k_Y
alpha_Z=1./k_Z

a_vec=[]
b_vec=[]
for i_cam in range(n_cam):
	print('i_cam: {0}'.format(i_cam))
	mp_file=mp_dir+str(i_cam+1)+'.yaml'

	fs=cv2.FileStorage(mp_file, cv2.FILE_STORAGE_READ)

	n_coef=int(fs.getNode("n_coef").real())
	print('n_coef: {0}'.format(n_coef))
	a=[]
	b=[]

	fn_a=fs.getNode("a")
	if fn_a.isSeq():
		for i in range(fn_a.size()):
			a.append(fn_a.at(i).real())

	fn_b=fs.getNode("b")
	if fn_b.isSeq():
		for i in range(fn_b.size()):
			b.append(fn_b.at(i).real())

	a_vec.append(a)
	b_vec.append(b)

	n_width=str(int(fs.getNode("n_width").real()))
	n_height=str(int(fs.getNode("n_height").real()))
	n_width_mask=str(int(fs.getNode("n_width_mask").real()))
	n_height_mask=str(int(fs.getNode("n_height_mask").real()))

	cam[i_cam].assign_scale_para(alpha_X, alpha_Y, alpha_Z, X_offset, Y_offset, Z_offset)

	so=-k_X*X_offset
	to=-k_Y*Y_offset

	nx=2*k_X
	ny=-2*k_Y

	for Z in Z_grid:
		print('Z ',Z)
		alpha0=a[0]+a[3]*Z+a[9]*Z**2
		alpha1=(a[1]+a[7]*Z+a[17]*Z**2)
		alpha2=(a[4]+a[14]*Z)
		alpha3=a[10]
		alpha4=(a[2]+a[8]*Z+a[18]*Z**2)
		alpha5=(a[6]+a[16]*Z)
		alpha6=a[13]
		alpha7=(a[5]+a[15]*Z)
		alpha8=a[11]
		alpha9=a[12]

		aZ = np.array([so-alpha0,
						-(alpha1-k_X),
						-(alpha2),
						-(alpha3),
						-(-alpha4),
						-(alpha5),
						-(-alpha6),
						-(-alpha7),
						-(-alpha8),
						-(alpha9)])

		beta0=b[0]+b[3]*Z+b[9]*Z**2
		beta1=(b[1]+b[7]*Z+b[17]*Z**2)
		beta2=(b[4]+b[14]*Z)
		beta3=b[10]
		beta4=(b[2]+b[8]*Z+b[18]*Z**2)
		beta5=(b[6]+b[16]*Z)
		beta6=b[13]
		beta7=(b[5]+b[15]*Z)
		beta8=b[11]
		beta9=b[12]

		bZ = np.array([to-(beta0),
						-(beta1),
						-(beta2),
						-(beta3),
						-(-beta4+k_Y),
						-(beta5),
						-(-beta6),
						-(-beta7),
						-(-beta8),
						-(beta9)])

		Z0 = k_Z*Z

		cam[i_cam].assign_mapping_function(Z0, so, to, nx , ny, aZ[0], aZ[1], aZ[2], aZ[3], aZ[4], aZ[5], aZ[6], aZ[7], aZ[8], aZ[9],
																bZ[0], bZ[1], bZ[2], bZ[3], bZ[4], bZ[5], bZ[6], bZ[7], bZ[8], bZ[9])

# for ic in cam:
# 	print('cam {}'.format(ic.i_cam))
# 	ic.print_mapping_function()

#Testing 
Xw_r=np.random.uniform(Xc_bottomX,Xc_topX,1)
Yw_r=np.random.uniform(Xc_bottomY,Xc_topY,1)
# Zw_r=np.random.uniform(Xc_bottomZ,Xc_topZ,1)

Xw_r=0
Yw_r=0
Zw_r=Z_grid[1]

print('Object point {0}, {1}, {2}'.format(Xw_r,Yw_r,Zw_r))

if n_coef==19:
	design_mat=np.array([1, Xw_r, Yw_r, Zw_r, Xw_r**2,   	Xw_r*Yw_r,   	Yw_r**2, 
											  Xw_r*Zw_r, 	Yw_r*Zw_r,   	Zw_r**2,
											  Xw_r**3,   	Xw_r**2*Yw_r,	Xw_r*Yw_r**2, Yw_r**3,
											  Xw_r**2*Zw_r, Xw_r*Yw_r*Zw_r, Yw_r**2*Zw_r, Xw_r*Zw_r**2, Yw_r*Zw_r**2]);
elif n_coef==20:
	design_mat=np.array([1, Xw_r, Yw_r, Zw_r, Xw_r**2,   	Xw_r*Yw_r,   	Yw_r**2, 
											  Xw_r*Zw_r, 	Yw_r*Zw_r,   	Zw_r**2,
											  Xw_r**3,   	Xw_r**2*Yw_r,	Xw_r*Yw_r**2, Yw_r**3,
											  Xw_r**2*Zw_r, Xw_r*Yw_r*Zw_r, Yw_r**2*Zw_r, Xw_r*Zw_r**2, Yw_r*Zw_r**2, Zw_r**3]);


for i_cam in range(n_cam):
	cam[i_cam].project_particle(Xw_r, Yw_r, Zw_r, verbal=True)

	xi=np.dot(design_mat, a_vec[i_cam])
	yi=np.dot(design_mat, b_vec[i_cam])
	print('Reference projection for camera {0}:'.format(i_cam))
	print('(xi {0}, yi {1})'.format(xi,yi))

home = os.path.expanduser("~")
mp_file=home+'/Calibration.xml'
mp_tree=ET.parse(mp_file);
root=mp_tree.getroot()
root.attrib['CalibrationIdentifier']='190207_XXXXXX'

for child in root.iter('CoordinateMapper'):
	i_cam=int(child.attrib['CameraIdentifier'])-1
	for a in child.iter('CorrectedImageSize'):
		a.attrib['Width']=n_width 
		a.attrib['Height']=n_height
	for a in child.iter('OriginalImageSize'):
		a.attrib['Width']=n_width 
		a.attrib['Height']=n_height
	for a in child.iter('PixelPerMmFactor'):
		a.attrib['Value']=str(k)
	for a in child.iter('LinearScaleX'):
		a.attrib['FactorMmPerPixel']=str(alpha_X)
		a.attrib['OffsetMm']=str(X_offset)
	for a in child.iter('LinearScaleY'):
		a.attrib['FactorMmPerPixel']=str(alpha_Y)
		a.attrib['OffsetMm']=str(Y_offset)
	for a in child.iter('LinearScaleZ'):
		a.attrib['FactorMmPerPixel']=str(alpha_Z)
		a.attrib['OffsetMm']=str(Z_offset)

	z_plane=0
	for pm in child.iter('PolynomialMapping'):
		for a in pm.iter('ZPosition'):
			a.attrib['Value']=str(cam[i_cam].poly[z_plane].Z0)
		for a in pm.iter('Origin'):
			a.attrib['s_o']=str(cam[i_cam].poly[z_plane].so)
			a.attrib['t_o']=str(cam[i_cam].poly[z_plane].to)
		for a in pm.iter('NormalisationFactor'):
			a.attrib['nx']=str(cam[i_cam].poly[z_plane].nx)
			a.attrib['ny']=str(cam[i_cam].poly[z_plane].ny)	
		for a in pm.iter('CoefficientsA'):
			a.attrib['a_o']=str(cam[i_cam].poly[z_plane].a_o)
			a.attrib['a_s']=str(cam[i_cam].poly[z_plane].a_s)
			a.attrib['a_s2']=str(cam[i_cam].poly[z_plane].a_s2)
			a.attrib['a_s3']=str(cam[i_cam].poly[z_plane].a_s3)
			a.attrib['a_t']=str(cam[i_cam].poly[z_plane].a_t)
			a.attrib['a_t2']=str(cam[i_cam].poly[z_plane].a_t2)
			a.attrib['a_t3']=str(cam[i_cam].poly[z_plane].a_t3)
			a.attrib['a_st']=str(cam[i_cam].poly[z_plane].a_st)
			a.attrib['a_s2t']=str(cam[i_cam].poly[z_plane].a_s2t)
			a.attrib['a_st2']=str(cam[i_cam].poly[z_plane].a_st2)
		for a in pm.iter('CoefficientsB'):
			a.attrib['b_o']=str(cam[i_cam].poly[z_plane].b_o)
			a.attrib['b_s']=str(cam[i_cam].poly[z_plane].b_s)
			a.attrib['b_s2']=str(cam[i_cam].poly[z_plane].b_s2)
			a.attrib['b_s3']=str(cam[i_cam].poly[z_plane].b_s3)
			a.attrib['b_t']=str(cam[i_cam].poly[z_plane].b_t)
			a.attrib['b_t2']=str(cam[i_cam].poly[z_plane].b_t2)
			a.attrib['b_t3']=str(cam[i_cam].poly[z_plane].b_t3)
			a.attrib['b_st']=str(cam[i_cam].poly[z_plane].b_st)
			a.attrib['b_s2t']=str(cam[i_cam].poly[z_plane].b_s2t)
			a.attrib['b_st2']=str(cam[i_cam].poly[z_plane].b_st2)
		z_plane=z_plane+1

root_dir=os.environ['LAVISIONDATA_PATH']
dest_path = root_dir+'ForDavis/'+case_name+'/calib/Calibration.xml'
mp_tree.write(dest_path)
