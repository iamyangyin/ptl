from __future__ import print_function
import os
import gc
import argparse
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR, StepLR
import numpy as np
from torch.utils.data import DataLoader
# from tensorboardX import SummaryWriter
from tqdm import tqdm
import pdb
from scipy import ndimage

# import kaolin as kal

def weights_init(m):
    classname=m.__class__.__name__
    if classname.find('Conv2d') != -1:
        nn.init.kaiming_normal_(m.weight.data)
    if classname.find('Conv1d') != -1:
        nn.init.kaiming_normal_(m.weight.data)

def compute_grad(pts_grid_velo, nXc, dXc):

    B=1

    pts_grid_velo = pts_grid_velo.view(B, nXc[0], nXc[1], nXc[2], -1)

    Uxr = pts_grid_velo[:,1:,:,:,0].contiguous().view(B, 1, nXc[0]-1, nXc[1], nXc[2])
    Uxr = F.pad(Uxr, (0,0,0,0,0,1), 'replicate')
    dUdx = (Uxr.squeeze(1) - pts_grid_velo[:,:,:,:,0])/dXc[0]

    Uyr = pts_grid_velo[:,:,1:,:,0].contiguous().view(B, 1, nXc[0], nXc[1]-1, nXc[2])
    Uyr = F.pad(Uyr, (0,0,0,1,0,0), 'replicate')
    dUdy = (Uyr.squeeze(1) - pts_grid_velo[:,:,:,:,0])/dXc[1]

    Uzr = pts_grid_velo[:,:,:,1:,2].contiguous().view(B, 1, nXc[0], nXc[1], nXc[2]-1)
    Uzr = F.pad(Uzr, (0,1,0,0,0,0), 'replicate')
    dUdz = (Uzr.squeeze(1) - pts_grid_velo[:,:,:,:,0])/dXc[2]

    Vxr = pts_grid_velo[:,1:,:,:,1].contiguous().view(B, 1, nXc[0]-1, nXc[1], nXc[2])
    Vxr = F.pad(Vxr, (0,0,0,0,0,1), 'replicate')
    dVdx = (Vxr.squeeze(1) - pts_grid_velo[:,:,:,:,1])/dXc[0]

    Vyr = pts_grid_velo[:,:,1:,:,1].contiguous().view(B, 1, nXc[0], nXc[1]-1, nXc[2])
    Vyr = F.pad(Vyr, (0,0,0,1,0,0), 'replicate')
    dVdy = (Vyr.squeeze(1) - pts_grid_velo[:,:,:,:,1])/dXc[1]

    Vzr = pts_grid_velo[:,:,:,1:,1].contiguous().view(B, 1, nXc[0], nXc[1], nXc[2]-1)
    Vzr = F.pad(Vzr, (0,1,0,0,0,0), 'replicate')
    dVdz = (Vzr.squeeze(1) - pts_grid_velo[:,:,:,:,1])/dXc[2]

    Wxr = pts_grid_velo[:,1:,:,:,2].contiguous().view(B, 1, nXc[0]-1, nXc[1], nXc[2])
    Wxr = F.pad(Wxr, (0,0,0,0,0,1), 'replicate')
    dWdx = (Wxr.squeeze(1) - pts_grid_velo[:,:,:,:,2])/dXc[0]

    Wyr = pts_grid_velo[:,:,1:,:,2].contiguous().view(B, 1, nXc[0], nXc[1]-1, nXc[2])
    Wyr = F.pad(Wyr, (0,0,0,1,0,0), 'replicate')
    dWdy = (Wyr.squeeze(1) - pts_grid_velo[:,:,:,:,2])/dXc[1]

    Wzr = pts_grid_velo[:,:,:,1:,2].contiguous().view(B, 1, nXc[0], nXc[1], nXc[2]-1)
    Wzr = F.pad(Wzr, (0,1,0,0,0,0), 'replicate')
    dWdz = (Wzr.squeeze(1) - pts_grid_velo[:,:,:,:,2])/dXc[2]

    sq_norm_grad_U = torch.sum( dUdx**2 + dUdy**2 + dUdz**2)
    sq_norm_grad_V = torch.sum( dVdx**2 + dVdy**2 + dVdz**2)
    sq_norm_grad_W = torch.sum( dWdx**2 + dWdy**2 + dWdz**2)

    sq_norm_grad = sq_norm_grad_U + sq_norm_grad_V + sq_norm_grad_W

    return sq_norm_grad/2.

def compute_div(pts_grid_velo, nXc, dXc):

    B=1

    pts_grid_velo = pts_grid_velo.view(B, nXc[0], nXc[1], nXc[2], -1)

    Ur = pts_grid_velo[:,1:,:,:,0].contiguous().view(B, 1, nXc[0]-1, nXc[1], nXc[2])
    Ur = F.pad(Ur, (0,0,0,0,0,1), 'replicate')
    dUdx = (Ur.squeeze(1) - pts_grid_velo[:,:,:,:,0])/dXc[0]

    Vr = pts_grid_velo[:,:,1:,:,1].contiguous().view(B, 1, nXc[0], nXc[1]-1, nXc[2])
    Vr = F.pad(Vr, (0,0,0,1,0,0), 'replicate')
    dVdy = (Vr.squeeze(1) - pts_grid_velo[:,:,:,:,1])/dXc[1]

    Wr = pts_grid_velo[:,:,:,1:,2].contiguous().view(B, 1, nXc[0], nXc[1], nXc[2]-1)
    Wr = F.pad(Wr, (0,1,0,0,0,0), 'replicate')
    dWdz = (Wr.squeeze(1) - pts_grid_velo[:,:,:,:,2])/dXc[2]

    sum_div = dUdx + dVdy + dWdz

    div_vec = torch.sum(sum_div, [1,2,3])

    div = torch.norm(div_vec)

    return div*div/2.

def total_lossfun(flow_pred, flow, nXc, dXc, beta, gamma):

    epe_loss = 0
    div_loss = 0
    grad_loss = 0

    epe_loss = torch.mean(torch.sum((flow_pred - flow) ** 2, 1) / 2.0)
    div_loss = beta*compute_div(flow_pred, nXc, dXc)
    grad_loss = gamma*compute_grad(flow_pred, nXc, dXc)

    tot_loss = epe_loss + div_loss + grad_loss
    return tot_loss, epe_loss, div_loss, grad_loss

class linearRegression(nn.Module):
    def __init__(self, inputSize, outputSize):
        super(linearRegression, self).__init__()
        self.linear = torch.nn.Linear(inputSize, outputSize)

    def forward(self, x):
        out = self.linear(x)
        return out

class simpleGenerator(nn.Module):
    def __init__(self, n_input, nx, ny, nz):
        super(simpleGenerator, self).__init__()
        self.n_input = n_input
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.net = nn.Sequential(
                                 nn.Linear(n_input,nx*ny*nz*3),
                                 nn.ReLU(inplace=True),
                                    )

    def forward(self, x):
        out = self.net(x)
        return out

def inferSize(x, y, z):
    y_diff = y[1:] - y[:-1]
    nx_v = np.where(y_diff!=0)
    nx = nx_v[0][0]+1

    z_diff = z[1:] - z[:-1]
    nxy_v = np.where(z_diff!=0)
    nxy = nxy_v[0][0]+1

    ny = nxy/nx
    nz = x.shape[0]/(nxy)

    return nx, int(ny), int(nz) 

def readTomo(tomo_data, aug_flag=True):

    f=np.loadtxt(tomo_data)
    x=f[:,0]
    y=f[:,1]
    z=f[:,2]

    nx, ny, nz=inferSize(x,y,z)

    print('nXc {} {} {}'.format(nx,ny,nz))

    if aug_flag:
        u=f[:,10]
        v=f[:,11]
        w=f[:,12]
    else:
        u=f[:,3]
        v=f[:,4]
        w=f[:,5]

    Xc_bottomX=x[0]
    Xc_bottomY=y[0]
    Xc_bottomZ=z[0]
    Xc_topX=x[-1]
    Xc_topY=y[-1]
    Xc_topZ=z[-1]

    dx=(Xc_topX-Xc_bottomX)/(nx-1)
    dy=(Xc_topY-Xc_bottomY)/(ny-1)
    dz=(Xc_topZ-Xc_bottomZ)/(nz-1)

    print('dXc {} {} {}'.format(dx,dy,dz))

    xx, yy, zz = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))

    UU = np.zeros( xx.shape )
    VV = np.zeros( xx.shape )
    WW = np.zeros( xx.shape )

    for k in np.arange(0, nz, 1):
        for i in np.arange(0, nx, 1):
            for j in np.arange(0, ny, 1):
                UU[j,i,k]=u[i+j*nx+k*nx*ny]
                VV[j,i,k]=v[i+j*nx+k*nx*ny]
                WW[j,i,k]=w[i+j*nx+k*nx*ny]

    nXc = [nx, ny, nz]
    dXc = [dx, dy, dz]
    flow = torch.zeros(ny, nx, nz, 3)
    flow[:,:,:,0]= torch.from_numpy(UU).float()
    flow[:,:,:,1]= torch.from_numpy(VV).float()
    flow[:,:,:,2]= torch.from_numpy(WW).float()
    flow=flow.permute(1,0,2,3).cuda().contiguous()

    return flow, nXc

def readVelocityAsObs(resDir, it_velo, grid_num):

    # data_name='velo/Velocity_ana_{:03d}_{:02d}_warped.txt'.format(it_velo, grid_num)
    data_name='velo/Velocity_ana_{:03d}_{:02d}_filtered.txt'.format(it_velo, grid_num)
    data_file=os.path.join(resDir, data_name)

    f=np.loadtxt(data_file)
    x=f[:,0]
    y=f[:,1]
    z=f[:,2]

    nx, ny, nz = inferSize(x,y,z)

    print('nXc {} {} {}'.format(nx,ny,nz))

    u=f[:,3]
    v=f[:,4]
    w=f[:,5]

    Xc_bottomX=x[0]
    Xc_bottomY=y[0]
    Xc_bottomZ=z[0]
    Xc_topX=x[-1]
    Xc_topY=y[-1]
    Xc_topZ=z[-1]

    dx=(Xc_topX-Xc_bottomX)/(nx-1)
    dy=(Xc_topY-Xc_bottomY)/(ny-1)
    dz=(Xc_topZ-Xc_bottomZ)/(nz-1)

    print('dXc {} {} {}'.format(dx,dy,dz))

    xx, yy, zz = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))

    XX = np.zeros( xx.shape )
    YY = np.zeros( xx.shape )
    ZZ = np.zeros( xx.shape )
    UU = np.zeros( xx.shape )
    VV = np.zeros( xx.shape )
    WW = np.zeros( xx.shape )

    for k in np.arange(0, nz, 1):
        for i in np.arange(0, nx, 1):
            for j in np.arange(0, ny, 1):
                XX[j,i,k]=x[i+j*nx+k*nx*ny]
                YY[j,i,k]=y[i+j*nx+k*nx*ny]
                ZZ[j,i,k]=z[i+j*nx+k*nx*ny]
                UU[j,i,k]=u[i+j*nx+k*nx*ny]
                VV[j,i,k]=v[i+j*nx+k*nx*ny]
                WW[j,i,k]=w[i+j*nx+k*nx*ny]

    nXc = [nx, ny, nz]
    dXc = [dx, dy, dz]
    flow = torch.zeros(ny, nx, nz, 3)
    flow[:,:,:,0]= torch.from_numpy(UU).float()
    flow[:,:,:,1]= torch.from_numpy(VV).float()
    flow[:,:,:,2]= torch.from_numpy(WW).float()
    flow=flow.permute(1,0,2,3).cuda().contiguous()

    return flow, dXc, nXc, XX, YY, ZZ

def readVelocityAsRef():

    velo_file='/media/yin/Data/Google-Drive/2018-LES3900-yin/velocity_rect.txt'

    nx_ref=291
    ny_ref=91
    nz_ref=16

    f=np.loadtxt(velo_file)
    x=f[:,0]
    y=f[:,1]
    z=f[:,2]
    u=f[:,3]
    v=f[:,4]
    w=f[:,5]

    xx_ref, yy_ref, zz_ref = np.meshgrid(np.arange(0, nx_ref, 1), np.arange(0, ny_ref, 1), np.arange(0, nz_ref, 1))

    UU_ref = np.zeros( xx_ref.shape )
    VV_ref = np.zeros( xx_ref.shape )
    WW_ref = np.zeros( xx_ref.shape )

    for k in np.arange(0, nz_ref, 1):
        for i in np.arange(0, nx_ref, 1):
            for j in np.arange(0, ny_ref, 1):
                UU_ref[j,i,k]=u[i+j*nx_ref+k*nx_ref*ny_ref]
                VV_ref[j,i,k]=v[i+j*nx_ref+k*nx_ref*ny_ref]
                WW_ref[j,i,k]=w[i+j*nx_ref+k*nx_ref*ny_ref]

    flow_ref = torch.zeros(ny_ref, nx_ref, nz_ref, 3)
    flow_ref[:,:,:,0]= torch.from_numpy(UU_ref).float()
    flow_ref[:,:,:,1]= torch.from_numpy(VV_ref).float()
    flow_ref[:,:,:,2]= torch.from_numpy(WW_ref).float()
    flow_ref=flow_ref.permute(1,0,2,3).cuda().contiguous()

    return flow_ref

def trainOnObs(args, flow, nXc, dXc):

    n_velo = nXc[0]*nXc[1]*nXc[2]*3

    n_input = 100

    net = linearRegression(n_input,n_velo).cuda()
    # net = simpleGenerator(n_input,nx,ny,nz).cuda()
    net.apply(weights_init)

    if args.use_sgd:
        print("Use SGD")
        opt = optim.SGD(net.parameters(), lr=args.lr * 100, momentum=args.momentum, weight_decay=1e-4)
    else:
        print("Use Adam")
        opt = optim.Adam(net.parameters(), lr=0.001, weight_decay=1e-4)

    net.train()

    criterion = total_lossfun

    input = torch.randn(1,n_input).cuda().view(1,1,n_input)

    for i in range(args.epochs):
        if i==0 or i==args.epochs-1:
            print('epoch: {}'.format(i))

        opt.zero_grad()

        flow_pred = net(input)

        flow_pred = flow_pred.view(nXc[0],nXc[1],nXc[2],3)

        loss, epe_loss, div_loss, grad_loss = criterion(flow_pred, flow, nXc, dXc, args.beta, args.gamma)
        loss.backward(retain_graph=True)

        opt.step()

        total_loss = loss.item()
        epe_loss_sum  = epe_loss
        div_loss_sum  = div_loss
        grad_loss_sum = grad_loss

        if i==0 or i==args.epochs-1:
            print('total_loss: {}, epe_loss: {}, div_loss: {}, grad_loss: {}'.format(total_loss, epe_loss_sum, div_loss_sum, grad_loss_sum))

    return flow_pred

def packAndSaveVelocity(flow_pred, resDir, it_velo, grid_num, nXc, XX, YY, ZZ):
    
    nx=nXc[0]
    ny=nXc[1]
    nz=nXc[2]

    out_name='velo/Velocity_ana_{:03d}_{:02d}_div.txt'.format(it_velo, grid_num)
    out_file=os.path.join(resDir, out_name)

    with open(out_file, "w") as the_file:
        the_file.write("#X Y Z V_X V_Y V_Z\n")
        for k in np.arange(0, nz, 1):
            for j in np.arange(0, ny, 1):
                for i in np.arange(0, nx, 1):
                    X=XX[j,i,k]
                    Y=YY[j,i,k]
                    Z=ZZ[j,i,k]
                    line="{0:1f} {1:1f} {2:.3f} {3:15.15f} {4:15.15f} {5:15.15f}".format(X,Y,Z,flow_pred[i,j,k,0],flow_pred[i,j,k,1],flow_pred[i,j,k,2])
                    the_file.write(line+'\n')

def computeRMSE(flow_ref, flow, nXc):

    flow_ref_upsmpl = torch.nn.functional.interpolate(flow_ref.view(1,flow_ref.shape[0],flow_ref.shape[1],flow_ref.shape[2],flow_ref.shape[3]),\
                                                    size=nXc, mode='trilinear', align_corners=False)
    flow_ref_upsmpl = flow_ref_upsmpl.squeeze().permute(1,2,3,0)

    mean_err = torch.sqrt( torch.sum( torch.sum( (flow_ref_upsmpl - flow)**2, 3 ) )/(nXc[0]*nXc[1]*nXc[2]) )
    mean_err_xyz = torch.sqrt( torch.sum( (flow_ref_upsmpl - flow)**2, [0,1,2] )/(nXc[0]*nXc[1]*nXc[2]) )

    return mean_err, mean_err_xyz

def applyMedianFilter(flow, use_median):

    if use_median:
        median_kernel_size = (7,7,7)
        flow_median_u = ndimage.median_filter(flow[:,:,:,0].detach().cpu(), median_kernel_size)
        flow_median_v = ndimage.median_filter(flow[:,:,:,1].detach().cpu(), median_kernel_size)
        flow_median_w = ndimage.median_filter(flow[:,:,:,2].detach().cpu(), median_kernel_size)

        flow_median = torch.zeros(flow.shape)
        flow_median[:,:,:,0]= torch.from_numpy(flow_median_u).float()
        flow_median[:,:,:,1]= torch.from_numpy(flow_median_v).float()
        flow_median[:,:,:,2]= torch.from_numpy(flow_median_w).float()
        flow_median = flow_median.cuda()
    else:
        flow_median = flow
    return flow_median

def main():
    parser = argparse.ArgumentParser(description='Point Cloud Registration')
    parser.add_argument('--dropout', type=float, default=0.5, metavar='N',
                        help='Dropout ratio in transformer')
    parser.add_argument('--epochs', type=int, default=250, metavar='N',
                        help='number of episode to train ')
    parser.add_argument('--use_sgd', action='store_true', default=False,
                        help='Use SGD')
    parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
                        help='learning rate (default: 0.001, 0.1 if using sgd)')
    parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                        help='SGD momentum (default: 0.9)')
    parser.add_argument('--no_cuda', action='store_true', default=False,
                        help='enables CUDA training')
    parser.add_argument('--seed', type=int, default=1234, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--beta', type=float, default=0.00001, metavar='BETA',
                        help='weight for div_null loss')
    parser.add_argument('--gamma', type=float, default=0.0000001, metavar='GAMMA',
                        help='weight for grad_norm loss')
    parser.add_argument('--it_velo', type=int, metavar='IT',
                        help='it of velo to read')
    parser.add_argument('--grid_num', type=int, metavar='GRID',
                        help='number of grid to read')
    parser.add_argument('--res_dir', type=str, metavar='RESDIR',
                        help='velo path')
    parser.add_argument('--compare_mode', action='store_true', default=False,
                        help='Compare LAPIV with Tomo, STB-bin and VIC')
    parser.add_argument('--use_median', action='store_true', default=True,
                        help='Use median filter')

    args = parser.parse_args()
    torch.backends.cudnn.deterministic = True
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)
    np.random.seed(args.seed)

    it_velo = args.it_velo
    grid_num = args.grid_num
    res_dir = args.res_dir

    flow, dXc, nXc, XX, YY, ZZ = readVelocityAsObs(res_dir, it_velo, grid_num)

    flow_median = applyMedianFilter(flow, args.use_median)

    flow_pred = trainOnObs(args, flow_median, nXc, dXc)

    flow_ref = readVelocityAsRef()
    flow_ref = flow_ref.permute(3,0,1,2)

    old_mean_err, old_mean_err_xyz = computeRMSE(flow_ref, flow, nXc)
    new_mean_err, new_mean_err_xyz = computeRMSE(flow_ref, flow_pred, nXc)

    print('old error: {}'.format(old_mean_err))
    print('new error: {}'.format(new_mean_err))

    print('old error: U {} V {} W {}'.format(old_mean_err_xyz[0], old_mean_err_xyz[1], old_mean_err_xyz[2]))
    print('new error: U {} V {} W {}'.format(new_mean_err_xyz[0], new_mean_err_xyz[1], new_mean_err_xyz[2]))

    old_div = torch.sqrt(compute_div(flow, nXc, dXc)*2)
    new_div = torch.sqrt(compute_div(flow_pred, nXc, dXc)*2)

    print('old divergence: {}'.format(old_div))
    print('new divergence: {}'.format(new_div))

    old_grad = torch.sqrt(compute_grad(flow, nXc, dXc)*2)
    new_grad = torch.sqrt(compute_grad(flow_pred, nXc, dXc)*2)

    print('old gradient norm: {}'.format(old_grad))
    print('new gradient norm: {}'.format(new_grad))

    if args.compare_mode:
        tomo_path = '/media/yin/Data/Google-Drive/lapiv-30000-0.1/'

        # tomo_data=tomo_path+'tomo32/tomo.txt'
        # flow_tomo, nXc_tomo = readTomo(tomo_data)
        # tomo_mean_err, tomo_mean_err_xyz = computeRMSE(flow_ref, flow_tomo, nXc_tomo)
        # print('tomo error: {}'.format(tomo_mean_err))
        # print('tomo error: U {} V {} W {}'.format(tomo_mean_err_xyz[0], tomo_mean_err_xyz[1], tomo_mean_err_xyz[2]))

        # stb_bin_data=tomo_path+'stb-bin/stb-bin.txt'
        # flow_stb_bin, nXc_stb_bin = readTomo(stb_bin_data)
        # stb_bin_mean_err, stb_bin_mean_err_xyz = computeRMSE(flow_ref, flow_stb_bin, nXc_stb_bin)
        # print('stb bin error: {}'.format(stb_bin_mean_err))
        # print('stb_bin error: U {} V {} W {}'.format(stb_bin_mean_err_xyz[0], stb_bin_mean_err_xyz[1], stb_bin_mean_err_xyz[2]))

        vic_data=tomo_path+'vic-4-tp/vic.txt'
        flow_vic, nXc_vic = readTomo(vic_data)
        vic_mean_err, vic_mean_err_xyz = computeRMSE(flow_ref, flow_vic, nXc_vic)
        print('vic error: {}'.format(vic_mean_err))
        print('vic error: U {} V {} W {}'.format(vic_mean_err_xyz[0], vic_mean_err_xyz[1], vic_mean_err_xyz[2]))

        # klpt_interp_data='/media/yin/Data/stb-ens-ptv-c/result/les3900rect/part030000_maxt0050_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/STBtrgdebug_davis/Velocity_021.txt'
        # # klpt_interp_data='/media/yin/Data/stb-ens-ptv-c/result/les3900rect/part040000_maxt0050_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/STBtrgdebug_davis_OTF_R0.8_I1.1_IT10/Velocity_021.txt'
        # flow_klpt_interp, nXc_klpt_interp = readTomo(klpt_interp_data, False)
        # klpt_interp_mean_err, klpt_interp_mean_err_xyz = computeRMSE(flow_ref, flow_klpt_interp, nXc_klpt_interp)
        # print('klpt interp error: {}'.format(klpt_interp_mean_err))
        # print('klpt error: U {} V {} W {}'.format(klpt_interp_mean_err_xyz[0], klpt_interp_mean_err_xyz[1], klpt_interp_mean_err_xyz[2]))
    else:
        print('pack and save div_null velocity')
        packAndSaveVelocity(flow_pred.detach().cpu(), res_dir, it_velo, grid_num, nXc, XX, YY, ZZ)

if __name__ == '__main__':
    main()