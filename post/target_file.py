import numpy as np
import os

class target_path:
	def __init__(self, small_flag, dtobs, over_lap, suffix):

		root_path=os.environ['STBENSPTV_PATH']

		if not small_flag:
			num_part='040000'
			small='rect'
			if suffix[0:7]=='_coaser':
				nx=37
				ny=12
				nz=7
			else:
				nx=45
				ny=15
				nz=5
		else:
			num_part='020000'
			small='_small'
			nx=19
			ny=19
			nz=2

		if over_lap=='00%':
			self.nx=nx
			self.ny=ny
			self.nz=nz
		elif over_lap=='50%':
			self.nx=nx*2-1
			self.ny=ny*2-1
			self.nz=nz*2-1
		elif over_lap=='75%':
			self.nx=nx*4-3
			self.ny=ny*4-3
			self.nz=nz*4-3

		run_index='ENS_Vdebug_newk_IPR_'

		self.result_dir=root_path+'/result/les3900v'+small+'/part'+num_part+'_maxt0010_fct00010_bkg0.000_psf1_dtobs'+dtobs+'/'+run_index+over_lap+suffix
