import numpy as np
import xml.etree.ElementTree as ET

otf_file="/Users/yin.yang/Documents/SyntheticImageForDavis/10000_0.05_v1_ERRCON_INT1-05/Calibration/OpticalTransferFunction.xml"
otf_tree=ET.parse(otf_file);
root=otf_tree.getroot()

otf=[]

for child in root.iter('Camera'):
	for childchild in child.iter('Subvolume'):
		for a in childchild.iter('intensity'):
			otf.append(float(a.attrib['value']))

otf_vec=np.array(otf[0:50])
print(otf_vec)

otf_mean=np.mean(otf_vec)
otf_var=np.var(otf_vec)

print(otf_vec.shape)
print('mean {}'.format(otf_mean))
print('std  {}'.format(np.sqrt(otf_var)))
