import matplotlib.pyplot as plt
import numpy as np
import os

velo_file='/Users/yin.yang/Documents/2018-LES3900-yin/velocity_rect.txt'

nx=291
ny=91
nz=16

f=np.loadtxt(velo_file)
print(f.shape)
x=f[:,0]
y=f[:,1]
z=f[:,2]
u=f[:,3]
v=f[:,4]
w=f[:,5]

xx, yy = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1))
print(xx.shape)

UU = np.zeros( xx.shape )
VV = np.zeros( xx.shape )
WW = np.zeros( xx.shape )

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			UU[j,i]=u[i+j*nx+k*nx*ny]
			VV[j,i]=v[i+j*nx+k*nx*ny]
			WW[j,i]=w[i+j*nx+k*nx*ny]

fig=plt.figure()
cmap = plt.get_cmap('hot')
fig.subplots_adjust(left=0.1,right=0.95,hspace=0.6,wspace=0.25)

# for k in np.arange(0, nz, 1):
k=9

for i in np.arange(0, nx, 1):
	for j in np.arange(0, ny, 1):
		UU[j,i]=u[i+j*nx+k*nx*ny]
		VV[j,i]=v[i+j*nx+k*nx*ny]
		WW[j,i]=w[i+j*nx+k*nx*ny]

# plt.subplot(nz,3,k*3+1)
plt.subplot(3,1,1)
plt.pcolormesh(xx, yy, UU, cmap=cmap)
plt.title('U')
# plt.axis('equal')
plt.axis([0, 290, 0, 90])
plt.grid(True)
plt.colorbar()

# plt.subplot(nz,3,k*3+2)
plt.subplot(3,1,2)
plt.pcolormesh(xx, yy, VV, cmap=cmap)
plt.title('V')
# plt.axis('equal')
plt.axis([0, 290, 0, 90])
plt.grid(True)
plt.colorbar()

# plt.subplot(nz,3,k*3+3)
plt.subplot(3,1,3)
plt.pcolormesh(xx, yy, WW, cmap=cmap)
plt.title('W')
# plt.axis('equal')
plt.axis([0, 290, 0, 90])
plt.grid(True)
plt.colorbar()


plt.show()