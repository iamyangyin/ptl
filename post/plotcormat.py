import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d
import os

root_path=os.environ['STBENSPTV_PATH']

cormat_file=root_path+"/unit_tests/cormat.txt"
f=np.loadtxt(cormat_file)

nsize=11*17*4
Z=np.reshape(f, (nsize, nsize))

X=np.arange(0,nsize,10)
Y=np.arange(0,nsize,10)
XX, YY = np.meshgrid(X, Y)

ZZ=Z[::10,::10];

# print(XX.shape)
print(ZZ)

fig=plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(XX, YY, ZZ, rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
ax.set_zlim(0, 1.01)

fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
