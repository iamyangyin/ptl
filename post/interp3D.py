from scipy.interpolate import RegularGridInterpolator
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt

root_path=os.environ['STBENSPTV_PATH']

smooth_flag = False

nx0=51
ny0=17
nz0=5
overlap_ratio=0.5

nx=int((nx0-1)/overlap_ratio+1)
ny=int((ny0-1)/overlap_ratio+1)
nz=int((nz0-1)/overlap_ratio+1)

grid_x = np.linspace(7.107843e-1, 7.178922e1, nx)
grid_y = np.linspace(6.617647e-1, 2.183824e1, ny)
grid_z = np.linspace(1.178098e0,  1.060288e1, nz)

# velo_file = root_path+"/result/les3900rect/part040000_maxt0005_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/ENShckdebug/Velocity_005_101x33x9.txt"
# velo_file = root_path+"/result/les3900rect/part040000_maxt0005_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/ENShckdebug/Velocity_005_101x33x9_smth.txt"

# nx=291
# ny=91
# nz=16

# grid_x = np.linspace(0, 72.5, nx)
# grid_y = np.linspace(0, 22.5, ny)
# grid_z = np.linspace(0, 11.78098, nz)

# velo_file = "/media/yin/Data/Google-Drive/2018-LES3900-yin/velocity_rect.txt"
velo_file = "/Users/yin.yang/Google Drive/2018-LES3900-yin/velocity_rect_101x33x9_smth.txt"

f=np.loadtxt(velo_file)

U=np.zeros((nx,ny,nz))
V=np.zeros((nx,ny,nz))
W=np.zeros((nx,ny,nz))

for k in range(nz):
	for j in range(ny):
		for i in range(nx):
			U[i,j,k]=f[i+j*nx+k*nx*ny][3]
			V[i,j,k]=f[i+j*nx+k*nx*ny][4]
			W[i,j,k]=f[i+j*nx+k*nx*ny][5]

xx, yy = np.meshgrid(grid_x, grid_y)

if smooth_flag:
	xx3, yy3, zz3 = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))

	alpha=1
	blur_kernel_shape=(15,15)
	gaussian_kernel_shape=(3,3)
	median_kernel_size=3

	UU_ana_smth = np.zeros( xx3.shape )
	VV_ana_smth = np.zeros( xx3.shape )
	WW_ana_smth = np.zeros( xx3.shape )

	for k in np.arange(0, nz, 1):
		UU_ana_plane= U[:,:,k].transpose()
		# UU_ana_blur=cv2.blur(UU_ana_plane, blur_kernel_shape)
		# img = UU_ana_plane.astype('float32')
		# UU_ana_blur=cv2.medianBlur(img, median_kernel_size)
		# delta_UU_ana=4*alpha/(1+4*alpha)*(UU_ana_plane-UU_ana_blur)
		UU_ana_smth[:,:,k]=UU_ana_plane#+delta_UU_ana
		# UU_ana_smth[:,:,k]=cv2.GaussianBlur(UU_ana_smth[:,:,k], gaussian_kernel_shape, 0)
		img = UU_ana_smth[:,:,k].astype('float32')
		UU_ana_smth[:,:,k]=cv2.medianBlur(img,median_kernel_size)

		VV_ana_plane= V[:,:,k].transpose()
		# VV_ana_blur=cv2.blur(VV_ana_plane, blur_kernel_shape)
		# img = VV_ana_plane.astype('float32')
		# VV_ana_blur=cv2.medianBlur(img,median_kernel_size)
		# delta_VV_ana=4*alpha/(1+4*alpha)*(VV_ana_plane-VV_ana_blur)
		VV_ana_smth[:,:,k]=VV_ana_plane#+delta_VV_ana
		# VV_ana_smth[:,:,k]=cv2.GaussianBlur(VV_ana_smth[:,:,k], gaussian_kernel_shape, 0)
		img = VV_ana_smth[:,:,k].astype('float32')
		VV_ana_smth[:,:,k]=cv2.medianBlur(img,median_kernel_size)

		WW_ana_plane= W[:,:,k].transpose()
		# WW_ana_blur=cv2.blur(WW_ana_plane, blur_kernel_shape)
		# img = WW_ana_plane.astype('float32')
		# WW_ana_blur=cv2.medianBlur(img,median_kernel_size)
		# delta_WW_ana=4*alpha/(1+4*alpha)*(WW_ana_plane-WW_ana_blur)
		WW_ana_smth[:,:,k]=WW_ana_plane#+delta_WW_ana
		# WW_ana_smth[:,:,k]=cv2.GaussianBlur(WW_ana_smth[:,:,k], gaussian_kernel_shape, 0)
		img = WW_ana_smth[:,:,k].astype('float32')
		WW_ana_smth[:,:,k]=cv2.medianBlur(img,median_kernel_size)

	U=UU_ana_smth
	V=VV_ana_smth
	W=WW_ana_smth

	Utgt=U[:,:,0]
else:
	Utgt=U[:,:,0].transpose()

vmin=np.amin(Utgt)
vmax=np.amax(Utgt)

fig=plt.figure(figsize=(10,10))
cmap = plt.get_cmap('hot')
fig.subplots_adjust(left=0.1,right=0.95,bottom=0,hspace=0.38,wspace=0.)

textfont=30
titlefont=50
tickfont=30
legendfont=50

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)  # legend fontsize
plt.rc('figure', titlesize=titlefont)  

plt.subplot(1,1,1)
plt.pcolormesh(xx, yy, Utgt, cmap=cmap, vmin=vmin, vmax=vmax)
plt.axis('scaled')
plt.colorbar()
plt.title('U')
plt.xlabel('X')
plt.ylabel('Y')
plt.show()

# velo_file_smth = root_path+"/result/les3900rect/part040000_maxt0005_fct00005_bkg0.000_psf1_dtobs0.10_Emin1.0_dmt2.4/ENShckdebug/Velocity_005_101x33x9_smth.txt"
# with open(velo_file_smth, 'w') as the_file:
# 	the_file.write("#X Y Z V_X V_Y V_Z\n")
# 	for k in range(nz):
# 		for j in range(ny):
# 			for i in range(nx):
# 				line="{0:10.6e} {1:10.6e} {2:10.6e} {3:10.6e} {4:10.6e} {5:10.6e}".format(grid_x[i],grid_y[j],grid_z[k],U[j,i,k],V[j,i,k],W[j,i,k])
# 				the_file.write(line+'\n')
