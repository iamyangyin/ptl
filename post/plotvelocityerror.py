import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.mplot3d import axes3d

def readVelocityErrorMap(result_path, ncyc):
	f=np.loadtxt(result_path)
	return f

error_valve=0.01
ncyc=41
case_name='les3900v'
dtobs='0.10'
px=''
# suffix='_Euler'
suffix=''

part=[]
# part.append('80000')
# part.append('64000')
# part.append('48000')
# part.append('32000')
# part.append('16000')
part.append('06400')
# part.append('01000')
snapshots='10' 

grid=291*291*16

res_path='../result/'+case_name+'/part0'+part[0]+'_maxt00'+snapshots+'_fct00010_bkg0.0_dtobs'+dtobs+suffix
err_forecast=readVelocityErrorMap(res_path+'/ENS_V0/VelocityError0.txt',ncyc)

error_sum=np.sum(err_forecast[:,6])
print(error_sum/grid)

res_path='../result/'+case_name+'/part0'+part[0]+'_maxt00'+snapshots+'_fct00010_bkg0.0_dtobs'+dtobs+suffix
err_forecast=readVelocityErrorMap(res_path+'/ENS_V0/VelocityError'+snapshots+'.txt',ncyc)

error_sum=np.sum(err_forecast[:,6])
print(error_sum/grid)

# fig=plt.figure()
# ax = fig.add_subplot(111)
# sc = ax.scatter(err_X_t[0],err_Y_t[0])
# plt.xlabel('Error X')
# plt.ylabel('Error Y')
# plt.show()
