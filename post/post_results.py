import numpy as np
import os
import sys

class log_results:
	def __init__(self, num_detected_true_tracks, num_undetected_tracks, mean_error_of_detected_true_tracks, \
						num_undetected_true_parts, num_tracked_ghost, num_tot_ghost, mean_error_of_detected_true_parts):
		self.num_detected_true_tracks=num_detected_true_tracks
		self.num_undetected_tracks=num_undetected_tracks
		self.mean_error_of_detected_true_tracks=mean_error_of_detected_true_tracks
		self.num_undetected_true_parts=num_undetected_true_parts
		self.num_tracked_ghost=num_tracked_ghost
		self.num_tot_ghost=num_tot_ghost
		self.mean_error_of_detected_true_parts=mean_error_of_detected_true_parts

def read_log_file(log_path):

	num_detected_true_tracks=[]
	num_undetected_tracks=[]
	mean_error_of_detected_true_tracks=[]
	num_undetected_true_parts=[]
	num_tracked_ghost=[]
	num_tot_ghost=[]
	mean_error_of_detected_true_parts=[]

	read_track_flag=False
	read_parts_flag=False
	read_num_undetected_true_parts_flag=False
	read_num_tracked_ghost_flag=False
	read_num_tot_ghost_flag=False
	with open(log_path) as fp:
		for line in fp:
			if line.startswith("Compute track information"):
				read_track_flag=True
				read_parts_flag=False

			if read_track_flag:
				if line.startswith("Number of detected true particles"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					num_detected_true_tracks.append(int(tgt))

				if line.startswith("Number of undetected particles"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					num_undetected_tracks.append(int(tgt))

				if line.startswith("Mean error of detected true particles (px)"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					mean_error_of_detected_true_tracks.append(float(tgt))

			if line.startswith("Compute particle information"):
				read_track_flag=False
				read_parts_flag=True

			if line.startswith("number of undetected true particles"):
				read_num_undetected_true_parts_flag = True

			if read_num_undetected_true_parts_flag:
				tgt=line.strip()
				if tgt[0].isdigit():
					num_undetected_true_parts.append(int(tgt))

			if line.startswith("Convert Reconstructed Particles Tracks"):
				read_num_undetected_true_parts_flag = False

			if line.startswith("number of tracked ghost particles"):
				read_num_tracked_ghost_flag = True

			if read_num_tracked_ghost_flag:
				tgt=line.strip()
				if tgt[0].isdigit():
					num_tracked_ghost.append(int(tgt))

			if line.startswith("Convert Reconstructed Particles Candidates To Frame"):
				read_num_tracked_ghost_flag = False

			if line.startswith("number of total ghost particles"):
				read_num_tot_ghost_flag = True

			if read_num_tot_ghost_flag:
				tgt=line.strip()
				if tgt[0].isdigit():
					num_tot_ghost.append(int(tgt))

			if line.startswith("############################################"):
				read_num_tot_ghost_flag = False

			if read_parts_flag:
				if line.startswith("Mean error of detected true particles (px)"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					mean_error_of_detected_true_parts.append(float(tgt))

	return log_results(num_detected_true_tracks, num_undetected_tracks, mean_error_of_detected_true_tracks, 
						num_undetected_true_parts, num_tracked_ghost, num_tot_ghost, mean_error_of_detected_true_parts)
