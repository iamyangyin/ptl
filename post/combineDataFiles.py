it_max = 100

# data_path='/media/yin/Data/stb-ens-ptv-c/result/lptchal4500/part240000_maxt0050_fct00005/LAPIVdebug_new_trial_3_rst30'
# data_path='/media/yin/Data/stb-ens-ptv-c/result/lptchal4500/part320000_maxt0100_fct00005/LAPIVdebug_new_trial_13_rst80'
# data_path='/media/yin/Data/stb-ens-ptv-c/result/lptchal4500/part400000_maxt0100_fct00005/LAPIVdebug_new_trial_15_rst80'

output_name = data_path+'/result.dat';

final_ss_flag = False

with open(output_name, mode='w') as out_file:
	for it in range(1,it_max):
		data_file=data_path+'/result{:03d}.dat'.format(it)

		print('read from result{:03d}.dat'.format(it))

		if it==it_max-1:
			final_ss_flag = True

		with open(data_file) as in_file:
			for line in in_file:
				if not final_ss_flag:
					stop_flag = 'ZONE T="Snapshot {:04d}"'.format(it)
					if stop_flag in line:
						print('stops at '+stop_flag)
						break
					else:
						out_file.write(line);
				else:
					out_file.write(line);
