import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
import argparse

class log_results:
	def __init__(self, num_tracks, track_length):
		self.num_tracks=num_tracks
		self.track_length=track_length

def read_log_file(log_path):

	num_tracks=[]
	track_length=[]

	with open(log_path) as fp:
		for line in fp:
			if line.startswith("Snapshot"):
				lnstr=line.split(":")
				tgt=lnstr[1].strip()
				num_tracks.append(int(tgt))

			if line.startswith("Track_length"):
				lnstr=line.split(":")
				tgt=lnstr[1].strip()
				track_length.append(int(tgt))

	return log_results(num_tracks, track_length)


root_path=os.environ['STBENSPTV_PATH']

res_1 = read_log_file(root_path+'/build/output_jet_davis_totaltrackstat')
res_2 = read_log_file(root_path+'/build/output_jet_klpt_totaltrackstat')

res_long_1 = read_log_file(root_path+'/build/output_jet_davis_longtrackstat')
res_long_2 = read_log_file(root_path+'/build/output_jet_klpt_longtrackstat')

maxt=50
itcyc=np.arange(1,maxt+1)

linewidth=3
markersize=20
textfont=24
titlefont=40
tickfont=24
legendfont=15

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)    # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(12,9))

#####################################################################################################
# np_res_1 = np.array(res_1.num_tracks)
# np_res_2 = np.array(res_2.num_tracks)

# np_res_long_1 = np.array(res_long_1.num_tracks)
# np_res_long_2 = np.array(res_long_2.num_tracks)

# title='Total number of tracks'

# line0,=plt.plot(itcyc,np_res_1,'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)
# line0.set_label(r'STB Davis total tracks')

# line00,=plt.plot(itcyc,np_res_long_1,'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)
# line00.set_label(r'STB Davis total tracks with larger displacements')

# line2,=plt.plot(itcyc,np_res_2,'k-',lw=linewidth, markerfacecolor='none', markersize=markersize)
# line2.set_label(r'KLPT total tracks')

# line02,=plt.plot(itcyc,np_res_long_2,'k--',lw=linewidth, markerfacecolor='none', markersize=markersize)
# line02.set_label(r'KLPT total tracks with larger displacements')

# ax=plt.subplot(111)
# ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
# box=ax.get_position()
# ax.set_position([box.x0, box.y0, box.width, box.height])

# plt.legend(bbox_to_anchor=(1, 0.5),loc='lower right',prop={'size':legendfont})

# plt.autoscale(enable=True, axis='x', tight=True)

# plt.xlabel('Snapshots')
# plt.ylabel('Total number')

####################################################################################################
np_res_1 = np.array(res_1.track_length)
np_res_2 = np.array(res_2.track_length)

np_res_long_1 = np.array(res_long_1.track_length)
np_res_long_2 = np.array(res_long_2.track_length)

title='Track length statistics'

line0,=plt.plot(itcyc,np_res_1,'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)
line0.set_label(r'STB Davis total tracks')

line00,=plt.plot(itcyc,np_res_long_1,'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)
line00.set_label(r'STB Davis total tracks with larger displacements')

line2,=plt.plot(itcyc,np_res_2,'k-',lw=linewidth, markerfacecolor='none', markersize=markersize)
line2.set_label(r'KLPT total tracks')

line02,=plt.plot(itcyc,np_res_long_2,'k--',lw=linewidth, markerfacecolor='none', markersize=markersize)
line02.set_label(r'KLPT total tracks with larger displacements')

ax=plt.subplot(111)
ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
box=ax.get_position()
ax.set_position([box.x0, box.y0, box.width, box.height])

plt.legend(bbox_to_anchor=(0, 1),loc='upper left',prop={'size':legendfont})

plt.autoscale(enable=True, axis='x', tight=True)

plt.xlabel('Track length')
plt.ylabel('Number of tracks')

# #############################################################################
plt.title(title)
res_filename='/home/yin/Desktop/num.eps'
plt.savefig(res_filename, format='eps', dpi=1500, bbox_inches='tight')
plt.show()