import matplotlib.pyplot as plt
import numpy as np

res_path='../result/dns300/part001000_maxt0031_fct00010_bkg0.0_dtobs0.1'

fldname1=res_path+'/STB/result.txt'
f1=np.loadtxt(fldname1)

it1=f1[:,0]
res_glob1=f1[:,1]
err_X1=f1[:,2]
err_Y1=f1[:,3]
err_Z1=f1[:,4]
err_x1=f1[:,5]
err_y1=f1[:,6]

print(err_x1.shape)

fldname2=res_path+'/ENS/result.txt'
f2=np.loadtxt(fldname2)

it2=f2[:,0]
res_glob2=f2[:,1]
err_X2=f2[:,2]
err_Y2=f2[:,3]
err_Z2=f2[:,4]
err_x2=f2[:,5]
err_y2=f2[:,6]

fig=plt.figure()
line1,=plt.semilogy(it1,res_glob1,'r',label='stb',lw=2)
line2,=plt.semilogy(it2,res_glob2,'k',label='ens',lw=2)
plt.xlabel('Iteration')
plt.ylabel('Image residual')
plt.legend(loc='upper right',prop={'size':15})

plt.title('Image residual')
res_filename=res_path+'/glob_res.eps'
plt.savefig(res_filename, formabt='eps', dpi=500)
plt.show()

fig=plt.figure()
fig.subplots_adjust(left=0.1,right=0.95,hspace=0.38,wspace=0.25)
plt.subplot(2,2,1)
line1,=plt.semilogy(it1,err_X1,'r',label='Xstb',lw=2)
line2,=plt.semilogy(it2,err_X2,'k',label='Xens',lw=2)
plt.xlabel('Iteration')
plt.ylabel('Particle Mean Error')
plt.legend(loc='upper right',prop={'size':10})
plt.subplot(2,2,2)
line3,=plt.semilogy(it1,err_Y1,'r',label='Ystb',lw=2)
line4,=plt.semilogy(it2,err_Y2,'k',label='Yens',lw=2)
plt.xlabel('Iteration')
plt.ylabel('Particle Mean Error')
plt.legend(loc='upper right',prop={'size':10})
plt.subplot(2,2,3)
line5,=plt.semilogy(it1,err_Z1,'r',label='Zstb',lw=2)
line6,=plt.semilogy(it2,err_Z2,'k',label='Zens',lw=2)
plt.xlabel('Iteration')
plt.ylabel('Particle Mean Error')
plt.legend(loc='upper right',prop={'size':10})
err_filename=res_path+'/err_mean.eps'
plt.savefig(err_filename, formabt='eps', dpi=500)
plt.show()
