import numpy as np
import os
import argparse
import math
import time

start_time = time.time()

class Particle:
	def __init__(self,part_index):
		self.part_index=part_index
		self.X=[]
		self.Y=[]
		self.Z=[]

	def append_XYZ(self,X,Y,Z):
		self.X.append(X)
		self.Y.append(Y)
		self.Z.append(Z)

	def compute_error_X(self, part_another, it_seq, it_seq_another):
		error_X=abs(self.X[it_seq]-part_another.X[it_seq_another])
		return error_X

	def compute_error_Y(self, part_another, it_seq, it_seq_another):
		error_Y=abs(self.Y[it_seq]-part_another.Y[it_seq_another])
		return error_Y

	def compute_error_Z(self, part_another, it_seq, it_seq_another):
		error_Z=abs(self.Z[it_seq]-part_another.Z[it_seq_another])
		return error_Z

def read_dat_file(dat_file, it_max, verbal=False):
	start_time = time.time()
	trackid=[]
	particles_from_dat=[]
	list_dat_it_deb=[]
	list_dat_it_fin=[]
	it=0
	with open(dat_file) as fp:
		for line in fp:
			line=line.rstrip('\n')
			if 'ZONE T' in line:
				it+=1
				if it>it_max:
					break;

				if verbal:
					print(line)

			if line[0].isdigit() or line[0]=='-':
				line=line.split()

				try:
					part_id=trackid.index(int(line[8]))
				except ValueError:
					part_id=-1

				if part_id==-1:
					trackid.append(int(line[8]))
					particles_from_dat.append(Particle(int(line[8])))
					particles_from_dat[-1].append_XYZ(float(line[0]),float(line[1]),float(line[2]))
					list_dat_it_deb.append(it)
				else:
					particles_from_dat[part_id].append_XYZ(float(line[0]),float(line[1]),float(line[2]))

	num_part_dat=0
	for part in particles_from_dat:
		list_dat_it_fin.append(list_dat_it_deb[num_part_dat]+len(part.X)-1)
		num_part_dat+=1

	print('number of particles read from DATA.DAT file: {}'.format(len(particles_from_dat)))
	print("- read_dat_file {} TIME seconds -".format(time.time() - start_time))
	return particles_from_dat, list_dat_it_deb, list_dat_it_fin

def read_ref_file(ref_path, it_max, verbal=False):
	start_time = time.time()
	trackid=[]
	particles_ref=[]
	list_ref_it_deb=[]
	list_ref_it_fin=[]
	for i in range(it_max):
		it=i+1
		ref_file=ref_path+'/ref/ref_it'+'{:03d}.txt'.format(it)
		with open(ref_file) as fp:
			for line in fp:
				line=line.strip()

				if line[0].isdigit():
					line=line.split()

					if it==1:
						trackid.append(int(line[0]))
						particles_ref.append(Particle(int(line[0])))
						list_ref_it_deb.append(int(line[1]))
						list_ref_it_fin.append(int(line[2]))

						if line[3]!='-1':
							particles_ref[-1].append_XYZ(float(line[3]),float(line[4]),float(line[5]))
					else:
						part_id=trackid.index(int(line[0]))
						if line[3]!='-1':
							particles_ref[part_id].append_XYZ(float(line[3]),float(line[4]),float(line[5]))

	print('number of particles read from REFERENCE file: {}'.format(len(particles_ref)))
	print("- read_ref_file {} TIME seconds -".format(time.time() - start_time))
	return particles_ref, list_ref_it_deb, list_ref_it_fin

def compare_distance(X, Y, Z, X_another, Y_another, Z_another, error_threshold):
	dist_x_sq=abs(X-X_another)
	if dist_x_sq < error_threshold:
		dist_y_sq=abs(Y-Y_another)
		if dist_y_sq < error_threshold:
			dist_z_sq=abs(Z-Z_another)
			if dist_z_sq < error_threshold:
				return math.sqrt( dist_x_sq**2 + dist_y_sq**2 + dist_z_sq**2 ) < error_threshold
			else:
				return False
		else:
			return False
	else:
		return False

def find_particles_new_tupel_from_ref(num_part, particles_ref, list_ref_it_deb, list_ref_it_fin, 
								  	particles_tgt, list_tgt_it_deb, list_tgt_it_fin, 
								  	it_fct, it_max, error_threshold, subset_flag='all', verbal=False):
	start_time = time.time()
	particles_tuple_list=[]

	list_tgt_paired=[False]*len(particles_tgt)

	if subset_flag == 'all' or subset_flag == 'no_new':
		for num_part_ref in range(num_part):
			particles_tuple_list.append((num_part_ref, num_part_ref))

	if subset_flag != 'no_new':
		for num_part_ref in range(len(particles_ref)-num_part):
			num_part_ref_new=num_part+num_part_ref

			it_seq=max(it_fct, list_ref_it_deb[num_part_ref_new])
			while (it_seq<=list_ref_it_fin[num_part_ref_new] and it_seq<=it_max):		
				it_seq_ref=it_seq-list_ref_it_deb[num_part_ref_new]

				ref_X=particles_ref[num_part_ref_new].X[it_seq_ref]
				ref_Y=particles_ref[num_part_ref_new].Y[it_seq_ref]
				ref_Z=particles_ref[num_part_ref_new].Z[it_seq_ref]

				for num_part_tgt in range(len(particles_tgt)-num_part):
					num_part_tgt_new=num_part+num_part_tgt

					it_seq_tgt=it_seq-list_tgt_it_deb[num_part_tgt_new]

					if not list_tgt_paired[num_part_tgt_new]:
						if it_seq>=list_tgt_it_deb[num_part_tgt_new] and it_seq<=list_tgt_it_fin[num_part_tgt_new]\
							 and compare_distance(particles_tgt[num_part_tgt_new].X[it_seq_tgt],
												  particles_tgt[num_part_tgt_new].Y[it_seq_tgt],
												  particles_tgt[num_part_tgt_new].Z[it_seq_tgt],
							 					  ref_X, ref_Y, ref_Z, error_threshold):

							list_tgt_paired[num_part_tgt_new]=True
							particles_tuple_list.append((num_part_tgt_new, num_part_ref_new))

							if list_tgt_it_fin[num_part_tgt_new] < list_ref_it_fin[num_part_ref_new]:
								it_seq=list_tgt_it_fin[num_part_tgt_new]
								break
							else:
								it_seq=list_ref_it_fin[num_part_ref_new]
								break
				it_seq+=1

	print("- find_particles_tupel_from_ref {} TIME seconds -".format(time.time() - start_time))
	return particles_tuple_list

def find_particles_tupel_from_ref(particles_ref, list_ref_it_deb, list_ref_it_fin, 
								  particles_tgt, list_tgt_it_deb, list_tgt_it_fin, 
								  it_fct, it_max, error_threshold, verbal=False):
	start_time = time.time()
	particles_tuple_list=[]

	list_tgt_paired=[False]*len(particles_tgt)

	for num_part_ref in range(len(particles_ref)):
		it_seq=max(it_fct, list_ref_it_deb[num_part_ref])
		while (it_seq<=list_ref_it_fin[num_part_ref] and it_seq<=it_max):		
			it_seq_ref=it_seq-list_ref_it_deb[num_part_ref]

			ref_X=particles_ref[num_part_ref].X[it_seq_ref]
			ref_Y=particles_ref[num_part_ref].Y[it_seq_ref]
			ref_Z=particles_ref[num_part_ref].Z[it_seq_ref]

			for num_part_tgt in range(len(particles_tgt)):
				it_seq_tgt=it_seq-list_tgt_it_deb[num_part_tgt]

				if not list_tgt_paired[num_part_tgt]:
					if it_seq>=list_tgt_it_deb[num_part_tgt] and it_seq<=list_tgt_it_fin[num_part_tgt]\
						 and compare_distance(particles_tgt[num_part_tgt].X[it_seq_tgt],
											  particles_tgt[num_part_tgt].Y[it_seq_tgt],
											  particles_tgt[num_part_tgt].Z[it_seq_tgt],
						 					  ref_X, ref_Y, ref_Z, error_threshold):

						list_tgt_paired[num_part_tgt]=True
						particles_tuple_list.append((num_part_tgt, num_part_ref))

						if list_tgt_it_fin[num_part_tgt] < list_ref_it_fin[num_part_ref]:
							it_seq=list_tgt_it_fin[num_part_tgt]
							break
						else:
							it_seq=list_ref_it_fin[num_part_ref]
							break
			it_seq+=1

	print("- find_particles_tupel_from_ref {} TIME seconds -".format(time.time() - start_time))
	return particles_tuple_list

def checkifistracked(it_deb, it_fin, it_seq):
	return it_seq<=it_fin and it_seq>=it_deb

def compute_mean_error(particles_tuple_list, particles_tgt, list_tgt_it_deb, list_tgt_it_fin, 
											 particles_ref, list_ref_it_deb, list_ref_it_fin, 
											 it_fct, it_max, dX_px):

	start_time = time.time()
	mean_error=[]
	mean_error_px=[]
	num_snapshots=it_max-it_fct+1
	num_part_ss=[]

	# for part_tuple in particles_tuple_list:
		# print('part_tgt {}, part_ref {}'.format(particles_tgt[part_tuple[0]].part_index, particles_ref[part_tuple[1]].part_index))

	for it_ss in range(num_snapshots):
		it_seq=it_ss+it_fct
		errorX=0.
		errorY=0.
		errorZ=0.
		num_part=0
		for part_tuple in particles_tuple_list:
			if checkifistracked(list_tgt_it_deb[part_tuple[0]], list_tgt_it_fin[part_tuple[0]], it_seq) and\
				checkifistracked(list_ref_it_deb[part_tuple[1]], list_ref_it_fin[part_tuple[1]], it_seq):

				it_seq_tgt=it_seq-list_tgt_it_deb[part_tuple[0]]
				it_seq_ref=it_seq-list_ref_it_deb[part_tuple[1]]

				#print('tgt def {} fin {}'.format(list_tgt_it_deb[part_tuple[0]], list_tgt_it_fin[part_tuple[0]]))
				#print('X {}, Y {}, Z{}'.format(particles_tgt[part_tuple[0]].X[it_seq_tgt], particles_tgt[part_tuple[0]].Y[it_seq_tgt], particles_tgt[part_tuple[0]].Z[it_seq_tgt]))
				#print('ref def {} fin {}'.format(list_ref_it_deb[part_tuple[1]], list_ref_it_fin[part_tuple[1]]))
				#print('X {}, Y {}, Z{}'.format(particles_ref[part_tuple[1]].X[it_seq_ref], particles_ref[part_tuple[1]].Y[it_seq_ref], particles_ref[part_tuple[1]].Z[it_seq_ref]))

				errX=particles_tgt[part_tuple[0]].compute_error_X(particles_ref[part_tuple[1]], it_seq_tgt, it_seq_ref)
				errY=particles_tgt[part_tuple[0]].compute_error_Y(particles_ref[part_tuple[1]], it_seq_tgt, it_seq_ref)
				errZ=particles_tgt[part_tuple[0]].compute_error_Z(particles_ref[part_tuple[1]], it_seq_tgt, it_seq_ref)

				errorX+=errX
				errorY+=errY
				errorZ+=errZ
				num_part+=1

		num_part_ss.append(num_part)

		mean_errorX_it_px=(errorX/num_part)/dX_px[0]
		mean_errorY_it_px=(errorY/num_part)/dX_px[1]
		mean_errorZ_it_px=(errorZ/num_part)/dX_px[2]

		mean_errorX_it=(errorX/num_part)
		mean_errorY_it=(errorY/num_part)
		mean_errorZ_it=(errorZ/num_part)

		mean_error_px.append(math.sqrt(mean_errorX_it_px**2+mean_errorY_it_px**2+mean_errorZ_it_px**2))
		mean_error.append(math.sqrt(mean_errorX_it**2+mean_errorY_it**2+mean_errorZ_it**2))

	print('number parts: {}'.format(num_part_ss))
	print("- compute_mean_error {} TIME seconds -".format(time.time() - start_time))
	return mean_error, mean_error_px, num_part_ss
