import numpy as np
import os
import argparse
import time
from compareTwoListsOfParticles import Particle, read_dat_file

start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--casename", default="", help="name case")
parser.add_argument("-t", "--stbdir", default="", help="name stb data dir")
parser.add_argument("-s", "--stbtype", default="", help="name stb data type")
parser.add_argument("-e", "--ensdir", default="", help="name ens result dir")
parser.add_argument("-r", "--runindex", default="", help="name ens run index")
args=parser.parse_args()

davis_flag=True
ens_flag=True

it_max_case = 50
it_max = it_max_case
it_fct = 10

assert (it_max <= it_max_case)

case_name = args.casename
data_dir=args.stbdir
data_suffix=args.stbtype
ens_dir=args.ensdir
	
data_path='/Users/yin.yang/STB-ENS-PTV-C++/result/'+case_name+'/part000000_maxt00'+str(it_max_case)+'_fct00010_bkg0.000_psf1_dtobs1.00'+ens_dir

ens_run_index='debug'+args.runindex
ensptv_path=data_path+'/ENS'+ens_run_index
ensptv_file=ensptv_path+'/result.dat'
print('LaPIV ens_file\n{}'.format(ensptv_file))

davis_file='/Users/yin.yang/Documents/Tomo_PIV_LASIE/2016-06-24-SOL-CONV_Re1250/CONV-2D_Re1250_Sol_F500/ImgPreproc_03/ShakeTheBox_tr_01/ExportToTecplot/ShakeTheBox_tr_01.dat'
print('davis_file\n{}'.format(davis_file))

[particles_davis, list_davis_it_deb, list_davis_it_fin]=read_dat_file(davis_file, it_max, verbal=False)

[particles_lapiv, list_lapiv_it_deb, list_lapiv_it_fin]=read_dat_file(ensptv_file, it_max, verbal=False)

print('############CATEGORY 1################')
ip=0
for pd in particles_davis:
	jp=0
	for pl in particles_lapiv:
		if pd.part_index == pl.part_index:
			assert(list_davis_it_deb[ip]==list_lapiv_it_deb[jp])
			if list_davis_it_fin[ip]>list_lapiv_it_fin[jp]:
				print('davis {}, it_deb {}, it_fin {}'.format(pd.part_index, list_davis_it_deb[ip], list_davis_it_fin[ip]))
				print('lapiv {}, it_deb {}, it_fin {}'.format(pl.part_index, list_lapiv_it_deb[jp], list_lapiv_it_fin[jp]))
		jp=jp+1
	ip=ip+1

print('############CATEGORY 2################')
ip=0
for pd in particles_davis:
	jp=0
	for pl in particles_lapiv:
		if pd.part_index == pl.part_index:
			assert(list_davis_it_deb[ip]==list_lapiv_it_deb[jp])
			if list_davis_it_fin[ip]<list_lapiv_it_fin[jp]:
				print('davis {}, it_deb {}, it_fin {}'.format(pd.part_index, list_davis_it_deb[ip], list_davis_it_fin[ip]))
				print('lapiv {}, it_deb {}, it_fin {}'.format(pl.part_index, list_lapiv_it_deb[jp], list_lapiv_it_fin[jp]))
		jp=jp+1
	ip=ip+1

# print('############CATEGORY 3################')
# ip=0
# for pd in particles_davis:
# 	jp=0
# 	for pl in particles_lapiv:
# 		if pd.part_index == pl.part_index:
# 			assert(list_davis_it_deb[ip]==list_lapiv_it_deb[jp])
# 			if list_davis_it_fin[ip]==list_lapiv_it_fin[jp]:
# 				print('davis {}, it_deb {}, it_fin {}'.format(pd.part_index, list_davis_it_deb[ip], list_davis_it_fin[ip]))
# 				print('lapiv {}, it_deb {}, it_fin {}'.format(pl.part_index, list_lapiv_it_deb[jp], list_lapiv_it_fin[jp]))
# 		jp=jp+1
# 	ip=ip+1

print("- total {} seconds -".format(time.time() - start_time))
