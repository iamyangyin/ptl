import numpy as np
import cv2

def project(Xw, Yw, Zw):

	k_X=4
	k_Y=-k_X

	so=60
	to=580
	print('so {0}, to {1}'.format(so,to))

	Nx=2*k_X
	Ny=-2*k_Y

	a_0=-2.77041988e+01
	a_s=4.89999111e-01
	a_s2=1.76392271e-04

	b_0=-9.42620308e-01
	b_s=9.09412689e-03
	b_s2=-4.50026676e-07
	b_s3=-1.9956265643900767e-15
	b_t=-2.09471180e-02
	b_t2=-1.8773871346411397e-13
	b_t3=9.871660780480518e-16
	b_st=2.02091709e-04
	b_s2t=-2.2501338401606887e-06
	b_st2=-9.403827543053023e-16

	Xp=k_X*Xw+so
	Yp=k_Y*Yw+to

	print('Xp {0}, Yp {1}'.format(Xp,Yp))

	s=2*(Xp-so)/Nx
	t=2*(Yp-to)/Ny

	print('s {0}, t {1}'.format(s,t))

	dx=a_0+a_s*s+a_s2*s**2
	dy=b_0+b_s*s+b_s2*s**2+b_s3*s**3\
		  +b_t*t+b_t2*t**2+b_t3*t**3\
		  +b_st*s*t+b_s2t*s**2*t+b_st2*s*t**2

	print('dx {0}, dy {1}'.format(dx,dy))

	xi=Xp-dx
	yi=Yp-dy

	print('projection on image:\n xi {0}, yi {1}'.format(xi,yi))

Xc_topX	= 290.
Xc_topY	= 90.
Xc_topZ	= 47.123890
Xc_bottomX = 0.
Xc_bottomY = 0.
Xc_bottomZ = 0.
nZ_plane = 4

Z_grid=np.linspace(Xc_bottomZ, Xc_topZ, num=nZ_plane)
print('Z grid {0}'.format(Z_grid)) 

# Xw_r=np.random.uniform(Xc_bottomX,Xc_topX,1)
# Yw_r=np.random.uniform(Xc_bottomY,Xc_topY,1)
Xw_r=193.44912564
Yw_r=14.2275194
Zw_r=Z_grid[0]

print('Object point \nX {0}, Y {1}, Z {2}'.format(Xw_r,Yw_r,Zw_r))

project(Xw_r, Yw_r, Zw_r)

design_mat=np.array([1, Xw_r, Yw_r, Zw_r, Xw_r**2,   	Xw_r*Yw_r,   	Yw_r**2, 
										  Xw_r*Zw_r, 	Yw_r*Zw_r,   	Zw_r**2,
										  Xw_r**3,   	Xw_r**2*Yw_r,	Xw_r*Yw_r**2, Yw_r**3,
										  Xw_r**2*Zw_r, Xw_r*Yw_r*Zw_r, Yw_r**2*Zw_r, Xw_r*Zw_r**2, Yw_r*Zw_r**2, Zw_r**3]);
a_vec=[]
b_vec=[]

mp_file='../data/les3900rect/part001000_maxt0015_fct00010_bkg0.0_dtobs0.50/camera2.yaml'

fs=cv2.FileStorage(mp_file, cv2.FILE_STORAGE_READ)

n_coef=int(fs.getNode("n_coef").real())
a=[]
b=[]
for i in range(1,n_coef+1):
	a.append(fs.getNode('a'+str(i)).real())
	b.append(fs.getNode('b'+str(i)).real())

a_vec.append(a)
b_vec.append(b)

xi=np.dot(design_mat, a_vec[0])
yi=np.dot(design_mat, b_vec[0])
print('Reference projection for camera {0}:'.format(0))
print('(xi {0}, yi {1})'.format(xi,yi))