import matplotlib.pyplot as plt
import numpy as np
import os
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter

sigma=[0.003,0.01,0.03,0.1]
sigma=[50,40,30,20]

stb_avg_with_warm=[0.0057,0.0132,0.0318,0.093]
stb_avg_sans_warm=[0.052,0.056,0.073,0.211]
ens_avg_with_warm=[0.0026,0.0089,0.0266,0.091]
ens_avg_sans_warm=[0.0027,0.0091,0.0267,0.091]

linewidth=4
markersize=15
textfont=24
titlefont=40
tickfont=20
legendfont=24

figsize_width=12
figsize_height=12

plt.rc('font', size=textfont)         # controls default text sizes
plt.rc('axes', titlesize=textfont)    # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)   # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)   # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont) # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(figsize_width,figsize_height))

line0,=plt.semilogy(sigma,stb_avg_with_warm,'k-o',lw=linewidth,markersize=markersize)
line1,=plt.semilogy(sigma,stb_avg_sans_warm,'k:o',lw=linewidth,markersize=markersize)
line2,=plt.semilogy(sigma,ens_avg_with_warm,'r-o',lw=linewidth,markersize=markersize)
line3,=plt.semilogy(sigma,ens_avg_sans_warm,'r:o',lw=linewidth,markersize=markersize)

line0.set_label('STB with warm start')
line1.set_label('STB without warm start')
line2.set_label('Ours with warm start')
line3.set_label('Ours without warm start')

plt.xticks(sigma)
ax=plt.subplot(111)
# ax.xaxis.set_major_formatter(FormatStrFormatter('%.f'))
ax.yaxis.set_major_formatter(FormatStrFormatter('%.e'))
ax.yaxis.set_minor_formatter(FormatStrFormatter('%.e'))
box=ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

plt.legend(loc='lower left',bbox_to_anchor=(0,0),prop={'size':legendfont})
plt.autoscale(enable=True, axis='x', tight=True)

plt.xlabel('PSNR (dB)')
plt.ylabel('Position errors (px)')
plt.title('Position errors in pixel')

plt.xlim(18,52)

plt.subplots_adjust(left=0.12, bottom=0.07, right=0.95, top=0.95, wspace=None, hspace=None)
res_filename='/Users/yin.yang/Documents/Err_Noise.eps'
plt.savefig(res_filename, formabt='eps', dpi=1500)
plt.show()
