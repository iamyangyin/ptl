import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
import argparse

cost_fun_ppp_0_025_ini = np.array([2.88e12, 1.98e12, 1.82e12, 1.64e12, 1.46e12, 1.2e12,  1.08e12, 9.67e11, 8.7e11,  7.96e11, 7.47e11, 7.31e11])
cost_fun_ppp_0_025_fin = np.array([1.73e12, 1.52e12, 1.37e12, 1.03e12, 6.51e11, 3.98e11, 2.15e11, 1.73e11, 1.22e11, 8.50e10, 7.97e10, 7.06e10])

cost_fun_ppp_0_050_ini = np.array([6.25e12, 4.46e12, 4.09e12, 3.65e12, 3.25e12, 2.77e12, 2.38e12, 2.05e12, 1.79e12, 1.59e12, 1.46e12, 1.36e12])
cost_fun_ppp_0_050_fin = np.array([3.96e12, 3.50e12, 3.20e12, 2.55e12, 1.81e12, 1.20e12, 6.96e11, 4.70e11, 2.95e11, 1.96e11, 1.69e11, 1.51e11])

cost_fun_ppp_0_080_ini = np.array([1.03e13, 7.46e12, 6.81e12, 6.21e12, 5.58e12, 4.84e12, 4.21e12, 3.71e12, 3.25e12, 2.88e12, 2.59e12, 2.40e12])
cost_fun_ppp_0_080_fin = np.array([6.64e12, 5.96e12, 5.55e12, 4.66e12, 3.56e12, 2.61e12, 1.67e12, 1.23e12, 8.29e11, 5.77e11, 5.01e11, 4.59e11])

it_ite = 12

linewidth=4
markersize=10
textfont=20
titlefont=40
tickfont=20
legendfont=20

figsize_width=12
figsize_height=12

cost_fun_ppp_0_025 = np.insert( cost_fun_ppp_0_025_fin, 0, cost_fun_ppp_0_025_ini[0] )
cost_fun_ppp_0_050 = np.insert( cost_fun_ppp_0_050_fin, 0, cost_fun_ppp_0_050_ini[0] )
cost_fun_ppp_0_080 = np.insert( cost_fun_ppp_0_080_fin, 0, cost_fun_ppp_0_080_ini[0] )

itcyc=np.arange(0, it_ite+1)

plt.rc('font', size=textfont)         # controls default text sizes
plt.rc('axes', titlesize=textfont)    # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)   # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)   # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont) # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(figsize_width,figsize_height))

line0,=plt.semilogy(itcyc, cost_fun_ppp_0_025, 'b-', lw=linewidth)
line1,=plt.semilogy(itcyc, cost_fun_ppp_0_050, 'r-', lw=linewidth)
line2,=plt.semilogy(itcyc, cost_fun_ppp_0_080, 'k-', lw=linewidth)

line0.set_label('PPP 0.025')
line1.set_label('PPP 0.050')
line2.set_label('PPP 0.080')

ax=plt.subplot(111)
ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
ax.yaxis.set_major_formatter(FormatStrFormatter('%.e'))
ax.yaxis.set_minor_formatter(FormatStrFormatter('%.e'))
box=ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

plt.legend(loc='upper right',bbox_to_anchor=(1,0.9),prop={'size':legendfont})
plt.autoscale(enable=True, axis='x', tight=True)

plt.xlabel('Iterations')
plt.ylabel(r'Cost function values (px$^2$)')
plt.title('Cost function values')

plt.subplots_adjust(left=0.12, bottom=0.07, right=0.95, top=0.95, wspace=None, hspace=None)
# res_filename='/Users/yin.yang/Documents/Davis_VS_LAPIV.eps'
# plt.savefig(res_filename, formabt='eps', dpi=1500)

plt.show()
