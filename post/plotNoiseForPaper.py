import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
import argparse
from post_results import log_results, read_log_file

parser = argparse.ArgumentParser()
parser.add_argument("plottype", help="give the name of the plot, currently support error, undetected, trackedghost, totalghost")

args=parser.parse_args()

plottype=args.plottype

root_path=os.environ['STBENSPTV_PATH']

num_part_str='30000'
num_part = int(num_part_str)
num_part_str='{:06d}'.format(num_part)

maxt_str = '50'
maxt = int(maxt_str)
maxt_str='{:04d}'.format(maxt)

dtobs_str='0.25'
dtobs = float(dtobs_str)
dtobs_str='{:.2f}'.format(dtobs)

bkg_str='0'
bkg = float(bkg_str)
bkg_str='{:.3f}'.format(bkg)

thin_str = '_thin'
run_index='debug0'

result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg'+bkg_str+'_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_1 = read_log_file(result_dir+'STBtrg'+run_index+'/result'+thin_str+'.log')
ens_1 = read_log_file(result_dir+'ENStrg'+run_index+'/result'+thin_str+'.log')

bkg_str='0.01'
bkg = float(bkg_str)
bkg_str='{:.3f}'.format(bkg)

result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg'+bkg_str+'_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_2 = read_log_file(result_dir+'STBtrg'+run_index+'/result'+thin_str+'.log')
ens_2 = read_log_file(result_dir+'ENStrg'+run_index+'/result'+thin_str+'.log')

bkg_str='0.03'
bkg = float(bkg_str)
bkg_str='{:.3f}'.format(bkg)

result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg'+bkg_str+'_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_3 = read_log_file(result_dir+'STBtrg'+run_index+'/result'+thin_str+'.log')
ens_3 = read_log_file(result_dir+'ENStrg'+run_index+'/result'+thin_str+'.log')

bkg_str='0.1'
bkg = float(bkg_str)
bkg_str='{:.3f}'.format(bkg)

run_index_old='debug0'

result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg'+bkg_str+'_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_4 = read_log_file(result_dir+'STBtrg'+run_index+'/result'+thin_str+'.log')
ens_4 = read_log_file(result_dir+'ENStrg'+run_index_old+'/result'+thin_str+'.log')

###########################################################################################
# print(sum(stb_1.mean_error_of_detected_true_parts[39:44])/5)
# print(sum(ens_1.mean_error_of_detected_true_parts[39:44])/5)

# print(sum(stb_2.mean_error_of_detected_true_parts[39:44])/5)
# print(sum(ens_2.mean_error_of_detected_true_parts[39:44])/5)

# print(sum(stb_3.mean_error_of_detected_true_parts[39:44])/5)
# print(sum(ens_3.mean_error_of_detected_true_parts[39:44])/5)

# print(sum(stb_4.mean_error_of_detected_true_parts[39:44])/5)
# print(sum(ens_4.mean_error_of_detected_true_parts[39:44])/5)

# print(sum(stb_1.num_undetected_true_parts[39:44])/(num_part*0.01*5))
# print(sum(ens_1.num_undetected_true_parts[39:44])/(num_part*0.01*5))

# print(sum(stb_2.num_undetected_true_parts[39:44])/(num_part*0.01*5))
# print(sum(ens_2.num_undetected_true_parts[39:44])/(num_part*0.01*5))

# print(sum(stb_3.num_undetected_true_parts[39:44])/(num_part*0.01*5))
# print(sum(ens_3.num_undetected_true_parts[39:44])/(num_part*0.01*5))

# print(sum(stb_4.num_undetected_true_parts[39:44])/(num_part*0.01*5))
# print(sum(ens_4.num_undetected_true_parts[39:44])/(num_part*0.01*5))

print(sum(stb_1.num_tracked_ghost[39:44])/(num_part*0.01*5))
print(sum(ens_1.num_tracked_ghost[39:44])/(num_part*0.01*5))

print(sum(stb_2.num_tracked_ghost[39:44])/(num_part*0.01*5))
print(sum(ens_2.num_tracked_ghost[39:44])/(num_part*0.01*5))

print(sum(stb_3.num_tracked_ghost[39:44])/(num_part*0.01*5))
print(sum(ens_3.num_tracked_ghost[39:44])/(num_part*0.01*5))

print(sum(stb_4.num_tracked_ghost[39:44])/(num_part*0.01*5))
print(sum(ens_4.num_tracked_ghost[39:44])/(num_part*0.01*5))
###########################################################################################
ncyc=maxt-4
end_idx=None
itcyc=np.arange(5,5+ncyc)

linewidth=3
markersize=20
textfont=24
titlefont=40
tickfont=24
legendfont=15

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)    # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(12,9))

################################################################################
if plottype=="undetected":
	ylabel='Fraction'
	if num_part==30000:
		title='Fraction of Undetected Particles, PPP = 0.075, time separation '+r'7.5$\delta t$'
	else:
		title='Fraction of Undetected Particles, PPP = 0.05, time separation '+r'7.5$\delta t$'

	np_stb_1_parts = np.array(stb_1.num_undetected_true_parts)/num_part
	np_ens_1_parts = np.array(ens_1.num_undetected_true_parts)/num_part

	line1,=plt.semilogy(itcyc,np_stb_1_parts[0:end_idx],'b-',lw=linewidth)
	line11,=plt.semilogy(itcyc,np_ens_1_parts[0:end_idx],'r-',lw=linewidth)

	np_stb_2_parts = np.array(stb_2.num_undetected_true_parts)/num_part
	np_ens_2_parts = np.array(ens_2.num_undetected_true_parts)/num_part

	line2,=plt.semilogy(itcyc,np_stb_2_parts[0:end_idx],'b--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line22,=plt.semilogy(itcyc,np_ens_2_parts[0:end_idx],'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)

	np_stb_3_parts = np.array(stb_3.num_undetected_true_parts)/num_part
	np_ens_3_parts = np.array(ens_3.num_undetected_true_parts)/num_part

	line3,=plt.semilogy(itcyc,np_stb_3_parts[0:end_idx],'b:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line33,=plt.semilogy(itcyc,np_ens_3_parts[0:end_idx],'r:',lw=linewidth, markerfacecolor='none', markersize=markersize)

	np_stb_4_parts = np.array(stb_4.num_undetected_true_parts)/num_part
	np_ens_4_parts = np.array(ens_4.num_undetected_true_parts)/num_part

	line4,=plt.semilogy(itcyc,np_stb_4_parts[0:end_idx],'b-.',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line44,=plt.semilogy(itcyc,np_ens_4_parts[0:end_idx],'r-.',lw=linewidth, markerfacecolor='none', markersize=markersize)
################################################################################
if plottype=="error":
	ylabel='Errors (px)'
	if num_part==30000:
		title='Mean error of detected particles, PPP = 0.075, time separation '+r'7.5$\delta t$'
	else:
		title='Mean error of detected particles, PPP = 0.05, time separation '+r'7.5$\delta t$'

	np_stb_1_parts = np.array(stb_1.mean_error_of_detected_true_parts)
	np_ens_1_parts = np.array(ens_1.mean_error_of_detected_true_parts)

	line1,=plt.semilogy(itcyc,np_stb_1_parts[0:end_idx],'b-',lw=linewidth)
	line11,=plt.semilogy(itcyc,np_ens_1_parts[0:end_idx],'r-',lw=linewidth)

	np_stb_2_parts = np.array(stb_2.mean_error_of_detected_true_parts)
	np_ens_2_parts = np.array(ens_2.mean_error_of_detected_true_parts)

	line2,=plt.semilogy(itcyc,np_stb_2_parts[0:end_idx],'b--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line22,=plt.semilogy(itcyc,np_ens_2_parts[0:end_idx],'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)

	np_stb_3_parts = np.array(stb_3.mean_error_of_detected_true_parts)
	np_ens_3_parts = np.array(ens_3.mean_error_of_detected_true_parts)

	line3,=plt.semilogy(itcyc,np_stb_3_parts[0:end_idx],'b:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line33,=plt.semilogy(itcyc,np_ens_3_parts[0:end_idx],'r:',lw=linewidth, markerfacecolor='none', markersize=markersize)

	np_stb_4_parts = np.array(stb_4.mean_error_of_detected_true_parts)
	np_ens_4_parts = np.array(ens_4.mean_error_of_detected_true_parts)

	line4,=plt.semilogy(itcyc,np_stb_4_parts[0:end_idx],'b-.',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line44,=plt.semilogy(itcyc,np_ens_4_parts[0:end_idx],'r-.',lw=linewidth, markerfacecolor='none', markersize=markersize)
#################################################################################
if plottype=="undetected" or plottype=="error":
	line1.set_label(r'STB,  Noise Free')
	line11.set_label(r'KLPT, Noise Free')

	line2.set_label(r'STB,  PSNR=40dB')
	line22.set_label(r'KLPT, PSNR=40dB')

	line3.set_label(r'STB,  PSNR=30dB')
	line33.set_label(r'KLPT, PSNR=30dB')

	line4.set_label(r'STB,  PSNR=20dB')
	line44.set_label(r'KLPT, PSNR=20dB')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width, box.height])

	if plottype=="error":
		plt.legend(bbox_to_anchor=(0.4, 1),loc='upper right',prop={'size':legendfont})
	if plottype=="undetected":
		plt.legend(bbox_to_anchor=(0.4, 1),loc='upper right',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel(ylabel)
	if plottype=="undetected":
		plt.yticks([0.005,0.01,0.05,0.1,0.5,1])
	else:
		plt.yticks([0.005,0.01,0.02,0.05,0.1,0.2,0.5])
	plt.title(title)
###############################################################################
if plottype=="trackedghost":
	if num_part==30000:
		title='Fraction of Tracked ghost particles, PPP = 0.075, time separation'+r'7.5$\delta t$'
	else:
		title='Fraction of Tracked ghost particles, PPP = 0.05, time separation'+r'7.5$\delta t$'

	np_stb_1 = np.array(stb_1.num_tracked_ghost)/num_part
	np_ens_1 = np.array(ens_1.num_tracked_ghost)/num_part

	np_stb_2 = np.array(stb_2.num_tracked_ghost)/num_part
	np_ens_2 = np.array(ens_2.num_tracked_ghost)/num_part

	np_stb_3 = np.array(stb_3.num_tracked_ghost)/num_part
	np_ens_3 = np.array(ens_3.num_tracked_ghost)/num_part

	np_stb_4 = np.array(stb_4.num_tracked_ghost)/num_part
	np_ens_4 = np.array(ens_4.num_tracked_ghost)/num_part

if plottype=="totalghost":
	if num_part==30000:
		title='Fraction of Total ghost particles, PPP = 0.075, time separation'+r'7.5$\delta t$'
	else:
		title='Fraction of Total ghost particles, PPP = 0.05, time separation'+r'7.5$\delta t$'

	np_stb_1 = np.array(stb_1.num_tot_ghost)/num_part
	np_ens_1 = np.array(ens_1.num_tot_ghost)/num_part

	np_stb_2 = np.array(stb_2.num_tot_ghost)/num_part
	np_ens_2 = np.array(ens_2.num_tot_ghost)/num_part

	np_stb_3 = np.array(stb_3.num_tot_ghost)/num_part
	np_ens_3 = np.array(ens_3.num_tot_ghost)/num_part

	np_stb_4 = np.array(stb_4.num_tot_ghost)/num_part
	np_ens_4 = np.array(ens_4.num_tot_ghost)/num_part
################################################################################
if plottype=="trackedghost" or plottype=="totalghost":
	line1,=plt.semilogy(itcyc,np_stb_1[0:end_idx],'b-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line11,=plt.semilogy(itcyc,np_ens_1[0:end_idx],'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line1.set_label(r'STB,  Noise Free')
	line11.set_label(r'KLPT, Noise Free')

	line2,=plt.semilogy(itcyc,np_stb_2[0:end_idx],'b--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line22,=plt.semilogy(itcyc,np_ens_2[0:end_idx],'r--',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line2.set_label(r'STB,  PSNR=40dB')
	line22.set_label(r'KLPT, PSNR=40dB')

	line3,=plt.semilogy(itcyc,np_stb_3[0:end_idx],'b:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line33,=plt.semilogy(itcyc,np_ens_3[0:end_idx],'r:',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line3.set_label(r'STB,  PSNR=30dB')
	line33.set_label(r'KLPT, PSNR=30dB')

	line4,=plt.semilogy(itcyc,np_stb_4[0:end_idx],'b-.',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line44,=plt.semilogy(itcyc,np_ens_4[0:end_idx],'r-.',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line4.set_label(r'STB,  PSNR=20dB')
	line44.set_label(r'KLPT, PSNR=20dB')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	ax.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width, box.height])

	plt.legend(bbox_to_anchor=(1, 0.8),loc='upper right',prop={'size':legendfont})
	# plt.legend(bbox_to_anchor=(1, 1),loc='upper left',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel('Fraction')
	plt.yticks([0.001,0.005,0.01,0.05,0.1,0.5])
	plt.title(title)
#############################################################################
res_filename='/home/yin/'+num_part_str+'_'+plottype+thin_str+'_noise.eps'
plt.savefig(res_filename, format='eps', dpi=1500, bbox_inches='tight')
plt.show()