import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import FormatStrFormatter
import argparse

class log_results:
	def __init__(self, num_detected_true_tracks, num_undetected_tracks, mean_error_of_detected_true_tracks, \
						num_undetected_true_parts, num_tracked_ghost, num_tot_ghost, mean_error_of_detected_true_parts):
		self.num_detected_true_tracks=num_detected_true_tracks
		self.num_undetected_tracks=num_undetected_tracks
		self.mean_error_of_detected_true_tracks=mean_error_of_detected_true_tracks
		self.num_undetected_true_parts=num_undetected_true_parts
		self.num_tracked_ghost=num_tracked_ghost
		self.num_tot_ghost=num_tot_ghost
		self.mean_error_of_detected_true_parts=mean_error_of_detected_true_parts

def read_log_file(log_path):

	num_detected_true_tracks=[]
	num_undetected_tracks=[]
	mean_error_of_detected_true_tracks=[]
	num_undetected_true_parts=[]
	num_tracked_ghost=[]
	num_tot_ghost=[]
	mean_error_of_detected_true_parts=[]

	read_track_flag=False
	read_parts_flag=False
	read_num_undetected_true_parts_flag=False
	read_num_tracked_ghost_flag=False
	read_num_tot_ghost_flag=False
	with open(log_path) as fp:
		for line in fp:
			if line.startswith("Compute track information"):
				read_track_flag=True
				read_parts_flag=False

			if read_track_flag:
				if line.startswith("Number of detected true particles"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					num_detected_true_tracks.append(int(tgt))

				if line.startswith("Number of undetected particles"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					num_undetected_tracks.append(int(tgt))

				if line.startswith("Mean error of detected true particles (px)"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					mean_error_of_detected_true_tracks.append(float(tgt))

			if line.startswith("Compute particle information"):
				read_track_flag=False
				read_parts_flag=True

			if line.startswith("number of undetected true particles"):
				read_num_undetected_true_parts_flag = True

			if read_num_undetected_true_parts_flag:
				tgt=line.strip()
				if tgt[0].isdigit():
					num_undetected_true_parts.append(int(tgt))

			if line.startswith("Convert Reconstructed Particles Tracks"):
				read_num_undetected_true_parts_flag = False

			if line.startswith("number of tracked ghost particles"):
				read_num_tracked_ghost_flag = True

			if read_num_tracked_ghost_flag:
				tgt=line.strip()
				if tgt[0].isdigit():
					num_tracked_ghost.append(int(tgt))

			if line.startswith("Convert Reconstructed Particles Candidates To Frame"):
				read_num_tracked_ghost_flag = False

			if line.startswith("number of total ghost particles"):
				read_num_tot_ghost_flag = True

			if read_num_tot_ghost_flag:
				tgt=line.strip()
				if tgt[0].isdigit():
					num_tot_ghost.append(int(tgt))

			if line.startswith("############################################"):
				read_num_tot_ghost_flag = False

			if read_parts_flag:
				if line.startswith("Mean error of detected true particles (px)"):
					lnstr=line.split(":")
					tgt=lnstr[1].strip()
					mean_error_of_detected_true_parts.append(float(tgt))

	return log_results(num_detected_true_tracks, num_undetected_tracks, mean_error_of_detected_true_tracks, 
						num_undetected_true_parts, num_tracked_ghost, num_tot_ghost, mean_error_of_detected_true_parts)

parser = argparse.ArgumentParser()
parser.add_argument("plottype", help="give the name of the plot, currently support error, undetected, trackedghost, totalghost")

args=parser.parse_args()

plottype=args.plottype

root_path=os.environ['STBENSPTV_PATH']

num_part_str='30000'
num_part = int(num_part_str)
num_part_str='{:06d}'.format(num_part)

maxt_str = '50'
maxt = int(maxt_str)
maxt_str='{:04d}'.format(maxt)

dtobs_str='0.1'
dtobs = float(dtobs_str)
dtobs_str='{:.2f}'.format(dtobs)

thin_str = '_part'
run_index_stb='debug_davis_OTF_R0.8_I1.1_IT10'
result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_1 = read_log_file(result_dir+'STBtrg'+run_index_stb+'/result'+thin_str+'.log')

thin_str = '_thin'
run_index_stb_inhouse='debug0'
run_index_ens='debug0'
result_dir=root_path+'/result/lavision_bkgonly/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_11= read_log_file(result_dir+'STBtrg'+run_index_stb_inhouse+'/result'+thin_str+'.log')
ens_1 = read_log_file(result_dir+'ENStrg'+run_index_ens+'/result'+thin_str+'.log')

dtobs_str='0.5'
dtobs = float(dtobs_str)
dtobs_str='{:.2f}'.format(dtobs)

thin_str = '_part_new'
run_index_stb='debug_davis_OTF_R0.8_I1.1_IT10'
result_dir=root_path+'/result/les3900rect/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_3 = read_log_file(result_dir+'STBtrg'+run_index_stb+'/result'+thin_str+'.log')

thin_str = '_thin'
run_index_stb_inhouse='debug0'
run_index_ens='debug4'
result_dir=root_path+'/result/lavision_bkgonly/part'+num_part_str+'_maxt'+maxt_str+'_fct00005_bkg0.000_psf1_dtobs'+dtobs_str+'_Emin1.0_dmt2.4/'
stb_31= read_log_file(result_dir+'STBtrg'+run_index_stb_inhouse+'/result'+thin_str+'.log')
ens_3 = read_log_file(result_dir+'ENStrg'+run_index_ens+'/result'+thin_str+'.log')

###########################################################################################
ncyc=maxt-4
end_idx=None
itcyc=np.arange(5,5+ncyc)

linewidth=3
markersize=20
textfont=24
titlefont=40
tickfont=24
legendfont=15

plt.rc('font', size=textfont)          # controls default text sizes
plt.rc('axes', titlesize=textfont)     # fontsize of the axes title
plt.rc('axes', labelsize=textfont)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('ytick', labelsize=tickfont)    # fontsize of the tick labels
plt.rc('legend', fontsize=legendfont)    # legend fontsize
plt.rc('figure', titlesize=titlefont)  

fig=plt.figure(figsize=(12,9))

################################################################################
if plottype=="undetected":
	ylabel='Fraction'
	title='Fraction of Undetected Tracks/Particles, '+str(num_part)
	# np_stb_1_tracks = 1-np.array(stb_1.num_detected_true_tracks)/num_part
	# np_ens_1_tracks = 1-np.array(ens_1.num_detected_true_tracks)/num_part
	np_stb_1_parts = np.array(stb_1.num_undetected_true_parts)/num_part
	np_stb_11_parts = np.array(stb_11.num_undetected_true_parts)/num_part
	np_ens_1_parts = np.array(ens_1.num_undetected_true_parts)/num_part

	# line0,=plt.plot(itcyc,np_stb_1_tracks[0:end_idx],'r-',lw=linewidth)
	line1,=plt.plot(itcyc,np_stb_1_parts[0:end_idx],'r-',lw=linewidth)
	line11,=plt.plot(itcyc,np_stb_11_parts[0:end_idx],'g-',lw=linewidth)
	# line00,=plt.plot(itcyc,np_ens_1_tracks[0:end_idx],'b-',lw=linewidth)
	line1e,=plt.plot(itcyc,np_ens_1_parts[0:end_idx],'b-',lw=linewidth)

	# np_stb_3_tracks = 1-np.array(stb_3.num_detected_true_tracks)/num_part
	# np_ens_3_tracks = 1-np.array(ens_3.num_detected_true_tracks)/num_part
	np_stb_3_parts = np.array(stb_3.num_undetected_true_parts)/num_part
	np_stb_31_parts = np.array(stb_31.num_undetected_true_parts)/num_part
	np_ens_3_parts = np.array(ens_3.num_undetected_true_parts)/num_part

	# line4,=plt.plot(itcyc,np_stb_3_tracks[0:end_idx],'r-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line5,=plt.plot(itcyc,np_stb_3_parts[0:end_idx],'r-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line51,=plt.plot(itcyc,np_stb_31_parts[0:end_idx],'g-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	# line44,=plt.plot(itcyc,np_ens_3_tracks[0:end_idx],'b-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line5e,=plt.plot(itcyc,np_ens_3_parts[0:end_idx],'b-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
################################################################################
if plottype=="error":
	ylabel='Errors (px)'
	title='Mean error of detected particles '+str(num_part)
	# np_stb_1_tracks = np.array(stb_1.mean_error_of_detected_true_tracks)
	# np_ens_1_tracks = np.array(ens_1.mean_error_of_detected_true_tracks)
	np_stb_1_parts = np.array(stb_1.mean_error_of_detected_true_parts)
	np_stb_11_parts = np.array(stb_11.mean_error_of_detected_true_parts)
	np_ens_1_parts = np.array(ens_1.mean_error_of_detected_true_parts)

	# line0,=plt.plot(itcyc,np_stb_1_tracks[0:end_idx],'r-',lw=linewidth)
	line1,=plt.plot(itcyc,np_stb_1_parts[0:end_idx],'r-',lw=linewidth)
	line11,=plt.plot(itcyc,np_stb_11_parts[0:end_idx],'g-',lw=linewidth)
	# line00,=plt.plot(itcyc,np_ens_1_tracks[0:end_idx],'b-',lw=linewidth)
	line1e,=plt.plot(itcyc,np_ens_1_parts[0:end_idx],'b-',lw=linewidth)

	# np_stb_3_tracks = np.array(stb_3.mean_error_of_detected_true_tracks)
	# np_ens_3_tracks = np.array(ens_3.mean_error_of_detected_true_tracks)
	np_stb_3_parts = np.array(stb_3.mean_error_of_detected_true_parts)
	np_stb_31_parts = np.array(stb_31.mean_error_of_detected_true_parts)
	np_ens_3_parts = np.array(ens_3.mean_error_of_detected_true_parts)

	# line4,=plt.plot(itcyc,np_stb_3_tracks[0:end_idx],'r-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line5,=plt.plot(itcyc,np_stb_3_parts[0:end_idx],'r-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line51,=plt.plot(itcyc,np_stb_31_parts[0:end_idx],'g-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	# line44,=plt.plot(itcyc,np_ens_3_tracks[0:end_idx],'b-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line5e,=plt.plot(itcyc,np_ens_3_parts[0:end_idx],'b-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
#################################################################################
if plottype=="undetected" or plottype=="error":
	# line0.set_label(r'STB Davis, 3$\delta t$, Tracks')
	line1.set_label(r'STB Davis, 3$\delta t$')
	line11.set_label(r'STB in-house, 3$\delta t$')
	# line00.set_label(r'Ours, 3$\delta t$, Tracks')
	line1e.set_label(r'Ours, 3$\delta t$')

	# line4.set_label(r'STB Davis, 15$\delta t$, Tracks')
	line5.set_label(r'STB Davis, 15$\delta t$')
	line51.set_label(r'STB in-house, 15$\delta t$')
	# line44.set_label(r'Ours, 15$\delta t$, Tracks')
	line5e.set_label(r'Ours, 15$\delta t$')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width, box.height])
	# ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

	if plottype=="error":
		plt.legend(bbox_to_anchor=(0.4, 1),loc='upper right',prop={'size':legendfont})
	if plottype=="undetected":
		plt.legend(bbox_to_anchor=(0.4, 1),loc='upper right',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel(ylabel)
	plt.title(title)
###############################################################################
if plottype=="trackedghost":
	title='Fraction of Tracked ghost particles '+str(num_part)

	np_stb_1 = np.array(stb_1.num_tracked_ghost)/num_part
	np_stb_11 = np.array(stb_11.num_tracked_ghost)/num_part
	np_ens_1 = np.array(ens_1.num_tracked_ghost)/num_part

	np_stb_3 = np.array(stb_3.num_tracked_ghost)/num_part
	np_stb_31 = np.array(stb_31.num_tracked_ghost)/num_part
	np_ens_3 = np.array(ens_3.num_tracked_ghost)/num_part
###############################################################################
if plottype=="totalghost":
	title='Fraction of Total ghost particles '+str(num_part)

	np_stb_1 = np.array(stb_1.num_tot_ghost)/num_part
	np_stb_11 = np.array(stb_11.num_tot_ghost)/num_part
	np_ens_1 = np.array(ens_1.num_tot_ghost)/num_part

	np_stb_3 = np.array(stb_3.num_tot_ghost)/num_part
	np_stb_31 = np.array(stb_31.num_tot_ghost)/num_part
	np_ens_3 = np.array(ens_3.num_tot_ghost)/num_part
################################################################################
if plottype=="trackedghost" or plottype=="totalghost":
	line0,=plt.plot(itcyc,np_stb_1[0:end_idx],'r-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line01,=plt.plot(itcyc,np_stb_11[0:end_idx],'g-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line0e,=plt.plot(itcyc,np_ens_1[0:end_idx],'b-',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line0.set_label(r'STB Davis, 3$\delta t$')
	line01.set_label(r'STB in-house, 3$\delta t$')
	line0e.set_label(r'Ours, 3$\delta t$')

	line2,=plt.plot(itcyc,np_stb_3[0:end_idx],'r-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line21,=plt.plot(itcyc,np_stb_31[0:end_idx],'g-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	# line21,=plt.plot(itcyc_40000_stb_0_5,np_stb_31[0:9],'g-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line2e,=plt.plot(itcyc,np_ens_3[0:end_idx],'b-o',lw=linewidth, markerfacecolor='none', markersize=markersize)
	line2.set_label(r'STB Davis, 15$\delta t$')
	line21.set_label(r'STB in-house, 15$\delta t$')
	line2e.set_label(r'Ours, 15$\delta t$')

	ax=plt.subplot(111)
	ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
	box=ax.get_position()
	ax.set_position([box.x0, box.y0, box.width, box.height])
	# ax.set_position([box.x0, box.y0, box.width*0.75, box.height])

	plt.legend(bbox_to_anchor=(1, 0.8),loc='upper right',prop={'size':legendfont})
	# plt.legend(bbox_to_anchor=(1, 1),loc='upper left',prop={'size':legendfont})

	plt.autoscale(enable=True, axis='x', tight=True)

	plt.xlabel('Snapshots')
	plt.ylabel('Fraction')
	plt.title(title)
#############################################################################
res_filename='/home/yin/'+num_part_str+'_'+plottype+thin_str+'_3methods.eps'
plt.savefig(res_filename, format='eps', dpi=1500, bbox_inches='tight')
plt.show()