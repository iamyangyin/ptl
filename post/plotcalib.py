import matplotlib.pyplot as plt
import numpy as np

nx=21
ny=51
nz=5

Nx=1280
Ny=800

ncam=4
npoint=nx*ny

calib_path='../data/les3900rect/part000001_maxt0050_fct00010_bkg0.000_psf1_dtobs0.50'

for i in range(0,ncam):
	
	cam_file=calib_path+'/calib/calib_cam'+str(i+1)+'.txt'

	f=np.loadtxt(cam_file)

	x=f[:,0]
	y=f[:,1]
	z=f[:,2]
	xc=f[:,3]
	yc=f[:,4]

	fig=plt.figure()
	fig.subplots_adjust(left=0.1,right=0.95,hspace=0.38,wspace=0.25)

	for k in range(0,nz):
		plt.subplot(1,nz,k+1)
		plt.scatter(xc[k*npoint:(k+1)*npoint], yc[k*npoint:(k+1)*npoint])
		plt.title('z'+str(k))
		plt.axis([1, Nx, 1, Ny])
		plt.grid(True)

	plt.show()