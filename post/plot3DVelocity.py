from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import os
from target_file import target_path

small_flag=False
dtobs='0.05'
over_lap='75%'
smooth_flag=True
suffix=''
# suffix='_finer'
fig_height=25#25,9
bottom_size=0.15

target=target_path(small_flag, dtobs, over_lap, suffix)

smooth_dir=''
if smooth_flag:
	smooth_dir='_smth'

velo_file_ref=target.result_dir+'/velo/Vorticity10ref.txt'
velo_file_bkg=target.result_dir+'/velo/Vorticity10bkg.txt'
velo_file_ana=target.result_dir+'/velo/Vorticity10ana'+smooth_dir+'.txt'

nx=target.nx
ny=target.ny
nz=target.nz

xx, yy, zz = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))
print('xx.shape {}'.format(xx.shape))

f=np.loadtxt(velo_file_ref)
print(f.shape)
x=f[:,0]
y=f[:,1]
z=f[:,2]
u=f[:,3]
v=f[:,4]
w=f[:,5]

f_bkg=np.loadtxt(velo_file_bkg)
u_bkg=f_bkg[:,3]
v_bkg=f_bkg[:,4]
w_bkg=f_bkg[:,5]

f_ana=np.loadtxt(velo_file_ana)
u_ana=f_ana[:,3]
v_ana=f_ana[:,4]
w_ana=f_ana[:,5]

UU = np.zeros( xx.shape )
VV = np.zeros( xx.shape )
WW = np.zeros( xx.shape )
UU_bkg=np.zeros( xx.shape )
VV_bkg=np.zeros( xx.shape )
WW_bkg=np.zeros( xx.shape )
UU_ana=np.zeros( xx.shape )
VV_ana=np.zeros( xx.shape )
WW_ana=np.zeros( xx.shape )

print('nx {}, ny {}, nz {}'.format(nx, ny, nz))

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			UU[j,i,k]=u[i+j*nx+k*nx*ny]
			VV[j,i,k]=v[i+j*nx+k*nx*ny]
			WW[j,i,k]=w[i+j*nx+k*nx*ny]
			UU_bkg[j,i,k]=u_bkg[i+j*nx+k*nx*ny]
			VV_bkg[j,i,k]=v_bkg[i+j*nx+k*nx*ny]
			WW_bkg[j,i,k]=w_bkg[i+j*nx+k*nx*ny]
			UU_ana[j,i,k]=u_ana[i+j*nx+k*nx*ny]
			VV_ana[j,i,k]=v_ana[i+j*nx+k*nx*ny]
			WW_ana[j,i,k]=w_ana[i+j*nx+k*nx*ny]

print('UU_ana.shape {}'.format(UU_ana.shape))

fig, ax = plt.subplots(figsize=(fig_height/2.,3))
cmap = plt.get_cmap('hot')
ax = fig.gca(projection='3d')
fig.subplots_adjust(bottom=bottom_size)
ax.set_title('3D Velocity')

q = ax.quiver(xx[0::5,0::5,:], yy[0::5,0::5,:], zz[0::5,0::5,:], UU_ana[0::5,0::5,:], VV_ana[0::5,0::5,:], WW_ana[0::5,0::5,:], length=0.1, normalize=True)
# ax.quiverkey(q, X=1.1, Y=0.6, U=0.1, label='Plane Velocity', labelpos='E')

plt.axis('scaled')
plt.xlabel('X[mm]')
plt.ylabel('Y[mm]')

plt.show()