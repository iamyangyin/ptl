import numpy as np
import xml.etree.ElementTree as ET

class poly:
	def __init__(self, Z0, so, to, nx, ny, a_o, a_s, a_s2, a_s3, a_t, a_t2, a_t3, a_st, a_s2t, a_st2, b_o, b_s, b_s2, b_s3, b_t, b_t2, b_t3, b_st, b_s2t, b_st2):
		self.Z0=Z0
		self.so=so
		self.to=to
		self.nx=nx
		self.ny=ny
		self.a_o=a_o
		self.a_s=a_s
		self.a_s2=a_s2
		self.a_s3=a_s3
		self.a_t=a_t
		self.a_t2=a_t2
		self.a_t3=a_t3
		self.a_st=a_st
		self.a_s2t=a_s2t
		self.a_st2=a_st2
		self.b_o=b_o
		self.b_s=b_s
		self.b_s2=b_s2
		self.b_s3=b_s3
		self.b_t=b_t
		self.b_t2=b_t2
		self.b_t3=b_t3
		self.b_st=b_st
		self.b_s2t=b_s2t
		self.b_st2=b_st2

class Camera:
	def __init__(self, i_cam):
		self.i_cam=i_cam
		self.poly=[]

	def assign_scale_para(self, alpha_X, alpha_Y, alpha_Z, X_offset, Y_offset, Z_offset):
		self.alpha_X=alpha_X
		self.alpha_Y=alpha_Y
		self.alpha_Z=alpha_Z
		self.k_X=1./alpha_X
		self.k_Y=1./alpha_Y
		self.k_Z=1./alpha_Z
		self.X_offset=X_offset
		self.Y_offset=Y_offset
		self.Z_offset=Z_offset

	def assign_mapping_function(self, Z0, so, to, nx, ny, a_o, a_s, a_s2, a_s3, a_t, a_t2, a_t3, a_st, a_s2t, a_st2, b_o, b_s, b_s2, b_s3, b_t, b_t2, b_t3, b_st, b_s2t, b_st2):
		self.poly.append(poly(Z0, so, to, nx, ny, a_o, a_s, a_s2, a_s3, a_t, a_t2, a_t3, a_st, a_s2t, a_st2, b_o, b_s, b_s2, b_s3, b_t, b_t2, b_t3, b_st, b_s2t, b_st2))

	def print_scale_para(self):
		print(self.alpha_X, self.alpha_Y, self.alpha_Z, self.X_offset, self.Y_offset, self.Z_offset)

	def print_mapping_function(self):
		for i in range(len(self.poly)):
			print('At Z= {}'.format(self.poly[i].Z0))
			print('so {0}, to {1}, nx {2}, ny {2}'.format(self.poly[i].so, self.poly[i].to, self.poly[i].nx, self.poly[i].ny))
			print(('a_o {0}\n a_s {1}\n a_s2 {2}\n a_s3 {3}\n a_t {4}\n a_t2 {5}\n a_t3 {6}\n a_st {7}\n a_s2t {8}\n a_st2 {9}\n'
				   'b_o {10}\n b_s {11}\n b_s2 {12}\n b_s3 {13}\n b_t {14}\n b_t2 {15}\n b_t3 {16}\n b_st {17}\n b_s2t {18}\n b_st2 {19}\n').
					format(self.poly[i].a_o, self.poly[i].a_s, self.poly[i].a_s2, self.poly[i].a_s3, self.poly[i].a_t, self.poly[i].a_t2, self.poly[i].a_t3, self.poly[i].a_st, self.poly[i].a_s2t, self.poly[i].a_st2,
					 	   self.poly[i].b_o, self.poly[i].b_s, self.poly[i].b_s2, self.poly[i].b_s3, self.poly[i].b_t, self.poly[i].b_t2, self.poly[i].b_t3, self.poly[i].b_st, self.poly[i].b_s2t, self.poly[i].b_st2))

	def project_particle(self, Xw, Yw, Zw, verbal=False):
		kX=self.k_X*Xw
		kY=self.k_Y*Yw
		kZ=self.k_Z*Zw
		if(verbal):
			print('kX {0}, kY {1}, kZ {2}'.format(kX,kY,kZ))
			print('so {0}, to {1}'.format(self.poly[0].so,self.poly[0].to))

		Xv=kX+self.poly[0].so
		Yv=kY+self.poly[0].to
		Zv=kZ

		if(verbal):
			print('Xv {0}, Yv {1}, Zv {2}'.format(Xv,Yv,Zv))

		dx_Z=[]
		dy_Z=[]

		pi=0
		if len(self.poly)==4:
			if(Zv<self.poly[0].Z0):
				pi=0
			elif(Zv>=self.poly[0].Z0 and Zv<self.poly[1].Z0):
				pi=0
			elif(Zv>=self.poly[1].Z0 and Zv<self.poly[2].Z0):
				pi=1
			elif(Zv>=self.poly[2].Z0 and Zv<self.poly[3].Z0):
				pi=2
			elif(Zv>=self.poly[3].Z0):
				pi=2

		print('use which Z plane out of {} planes? use {}'.format(len(self.poly), pi))

		s=2.*(Xv-self.poly[pi].so)/self.poly[pi].nx
		t=2.*(Yv-self.poly[pi].to)/self.poly[pi].ny

		if(verbal):
			print('s {0}, t {1}'.format(s,t))

		for i in range(2):
			dx_Z.append(self.poly[pi+i].a_o + self.poly[pi+i].a_s * s + self.poly[pi+i].a_s2 * s ** 2 + self.poly[pi+i].a_s3 * s ** 3\
											+ self.poly[pi+i].a_t * t + self.poly[pi+i].a_t2 * t ** 2 + self.poly[pi+i].a_t3 * t ** 3\
											+ self.poly[pi+i].a_st * s * t + self.poly[pi+i].a_s2t * s ** 2 * t + self.poly[pi+i].a_st2 * s * t ** 2)
			dy_Z.append(self.poly[pi+i].b_o + self.poly[pi+i].b_s * s + self.poly[pi+i].b_s2 * s ** 2 + self.poly[pi+i].b_s3 * s ** 3\
											+ self.poly[pi+i].b_t * t + self.poly[pi+i].b_t2 * t ** 2 + self.poly[pi+i].b_t3 * t ** 3\
											+ self.poly[pi+i].b_st * s * t + self.poly[pi+i].b_s2t * s ** 2 * t + self.poly[pi+i].b_st2 * s * t ** 2)

		Z_ratio=(Zv-self.poly[pi].Z0)/(self.poly[pi+1].Z0-self.poly[pi].Z0)
		if(verbal):
			print('Z_ratio {0}'.format(Z_ratio))

		dx=dx_Z[0]+(dx_Z[1]-dx_Z[0])*Z_ratio
		dy=dy_Z[0]+(dy_Z[1]-dy_Z[0])*Z_ratio

		if(verbal):
			print('dx {0}, dy {1}'.format(dx,dy))

		x=Xv-dx
		y=Yv-dy

		print('Davis projection for camera {0}:'.format(self.i_cam))
		print("xi {0}, yi {1}".format(x,y))
