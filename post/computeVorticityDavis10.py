import numpy as np
import os

VIC_flag=True
TOMO_flag=False

if VIC_flag:
	case_name='vic-4-tp/ExportTecplotDat/B00020.dat'
	nx=291
	ny=91
	nz=48
	Xc_topX=72.625
	Xc_topY=22.375
	Xc_topZ=11.875
	Xc_bottomX=0.125
	Xc_bottomY=-0.125
	Xc_bottomZ=0.125
	# case_name='vic-8-tp/ExportTecplotDat/B00020.dat'
	# nx=146
	# ny=46
	# nz=24
	# Xc_topX=72.75
	# Xc_topY=22.25
	# Xc_topZ=11.75
	# Xc_bottomX=0.25
	# Xc_bottomY=-0.25
	# Xc_bottomZ=0.25
	# case_name='vic-16-tp/ExportTecplotDat/B00020.dat'
	# nx=73
	# ny=23
	# nz=12
	# Xc_topX=72.5
	# Xc_topY=22
	# Xc_topZ=11.5
	# Xc_bottomX=0.5
	# Xc_bottomY=0
	# Xc_bottomZ=0.5
elif TOMO_flag:
	# case_name='tomo32/TomoExportAsTecplot/B00001.dat'
	# nx=145
	# ny=45
	# nz=24
	# Xc_topX=72.25
	# Xc_topY=22.25
	# Xc_topZ=11.75
	# Xc_bottomX=0.25
	# Xc_bottomY=0.25
	# Xc_bottomZ=0.25
	case_name='tomo16/ExportTecplotDat/B00001.dat'
	nx=290
	ny=90
	nz=48
	Xc_topX=72.375
	Xc_topY=22.375
	Xc_topZ=11.875
	Xc_bottomX=0.125
	Xc_bottomY=0.125
	Xc_bottomZ=0.125
else:
	case_name='stb-bin/ExportTecplotDat/B00020.dat'
	nx=73
	ny=23
	nz=12
	Xc_topX=72.5
	Xc_topY=22
	Xc_topZ=11.5
	Xc_bottomX=0.5
	Xc_bottomY=0
	Xc_bottomZ=0.5

data_path='/media/yin/Data/Google-Drive/lapiv-30000-0.1/'
data_file=data_path+case_name

xl=[]
yl=[]
zl=[]
ul=[]
vl=[]
wl=[]

with open(data_file) as fp:
	for line in fp:
		line=line.strip()
		if line:
			if line[0].isdigit():
				line=line.split()
				xl.append(float(line[0]))
				yl.append(float(line[1]))
				zl.append(float(line[2]))
				ul.append(float(line[3]))
				vl.append(float(line[4]))
				wl.append(float(line[5]))

xx, yy, zz = np.meshgrid(np.arange(0, nx, 1), np.arange(0, ny, 1), np.arange(0, nz, 1))
print('xx.shape {}'.format(xx.shape))

x=np.asarray(xl)
x=x[0:nx*ny*nz]
y=np.asarray(yl)
y=y[0:nx*ny*nz]
z=np.asarray(zl)
z=z[0:nx*ny*nz]
u=np.asarray(ul)
u=u[0:nx*ny*nz]
v=np.asarray(vl)
v=v[0:nx*ny*nz]
w=np.asarray(wl)
w=w[0:nx*ny*nz]

XX = np.zeros( xx.shape )
YY = np.zeros( xx.shape )
ZZ = np.zeros( xx.shape )
UU = np.zeros( xx.shape )
VV = np.zeros( xx.shape )
WW = np.zeros( xx.shape )

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			XX[j,i,k]=x[i+(ny-j-1)*nx+k*nx*ny]
			YY[j,i,k]=y[i+(ny-j-1)*nx+k*nx*ny]
			ZZ[j,i,k]=z[i+(ny-j-1)*nx+k*nx*ny]
			UU[j,i,k]=u[i+(ny-j-1)*nx+k*nx*ny]/1000.
			VV[j,i,k]=v[i+(ny-j-1)*nx+k*nx*ny]/1000.
			WW[j,i,k]=w[i+(ny-j-1)*nx+k*nx*ny]/1000.

D=1

dx=(Xc_topX-Xc_bottomX)/(nx-1)
dy=(Xc_topY-Xc_bottomY)/(ny-1)
dz=(Xc_topZ-Xc_bottomZ)/(nz-1)

print('{} {} {}'.format(dx,dy,dz))

wx=np.zeros( xx.shape )
wy=np.zeros( xx.shape )
wz=np.zeros( xx.shape )

Uy=np.zeros( xx.shape )
Uz=np.zeros( xx.shape )
Vx=np.zeros( xx.shape )
Vz=np.zeros( xx.shape )
Wx=np.zeros( xx.shape )
Wy=np.zeros( xx.shape )

Utest=UU;
Vtest=VV;
Wtest=WW;

for k in np.arange(0, nz, 1):
	for i in np.arange(0, nx, 1):
		for j in np.arange(0, ny, 1):
			if i>0:
				Vx[j,i,k]=(Vtest[j,i,k]-Vtest[j,i-1,k])/dx;
				Wx[j,i,k]=(Wtest[j,i,k]-Wtest[j,i-1,k])/dx;
			else:
				Vx[j,0,k]=(Vtest[j,1,k]-Vtest[j,0,k])/dx;
				Wx[j,0,k]=(Wtest[j,1,k]-Wtest[j,0,k])/dx;

			if j>0:
				Uy[j,i,k]=(Utest[j,i,k]-Utest[j-1,i,k])/dy;
				Wy[j,i,k]=(Wtest[j,i,k]-Wtest[j-1,i,k])/dy;
			else:
				Uy[0,i,k]=(Utest[1,i,k]-Utest[0,i,k])/dy;
				Wy[0,i,k]=(Wtest[1,i,k]-Wtest[0,i,k])/dy;

			if k>0:
				Uz[j,i,k]=(Utest[j,i,k]-Utest[j,i,k-1])/dz;
				Vz[j,i,k]=(Vtest[j,i,k]-Vtest[j,i,k-1])/dz;
			else:
				Uz[j,i,0]=(Utest[j,i,1]-Utest[j,i,0])/dz;
				Vz[j,i,0]=(Vtest[j,i,1]-Vtest[j,i,0])/dz;

wx=Wy-Vz;
wy=Uz-Wx;
wz=Vx-Uy;

vort_file_ana=data_path+'tomo.txt'
with open(vort_file_ana, "w") as the_file:
	the_file.write("#X Y Z NX NY NZ W_X W_Y W_Z W_n V_X V_Y V_Z V_n\n")
	for k in np.arange(0, nz, 1):
		for j in np.arange(0, ny, 1):
			for i in np.arange(0, nx, 1):
				X=XX[j,i,k]/D
				Y=YY[j,i,k]/D
				Z=ZZ[j,i,k]/D
				vn=(UU[j,i,k]**2+VV[j,i,k]**2+WW[j,i,k]**2)**(0.5)
				wn=(wx[j,i,k]**2+wy[j,i,k]**2+wz[j,i,k]**2)**(0.5)
				line="{0:1f} {1:1f} {2:.3f} {3:3f} {4:3f} {5:3f} {6:15.15f} {7:15.15f} {8:15.15f} {9:15.15f} {10:15.15f} {11:15.15f} {12:15.15f} {13:15.15f}".format(X,Y,Z,i,j,k,wx[j,i,k],wy[j,i,k],wz[j,i,k],wn,UU[j,i,k],VV[j,i,k],WW[j,i,k],vn)
				the_file.write(line+'\n')
