#script for make exe and
import argparse
import os
import platform
import shutil 
from subprocess import call

parser = argparse.ArgumentParser()
parser.add_argument("exptype", help="give the name of the experiment, currently support sph, sph2, les3900, les3900rect, dns300, lavision_jet, lavision_bkgonly, lavision_re300exp, lavision_re3900exp")
parser.add_argument("-j", "--processors", default='8', help="number of make -j")
parser.add_argument("-d", "--debug", action="store_true", help="debug build of cmake")
parser.add_argument("-p", "--parallelscheme", default="TBB", help="parallel scheme of cmake, support NO, TBB, CUDA")
parser.add_argument("-t", "--target", default="exes", help="targets of cmake, support all, klpt, exes, dev, core")

args=parser.parse_args()

test_dir=os.environ['STBENSPTV_PATH']+'/build/test/'
if not os.path.exists(test_dir):
    os.makedirs(test_dir)

if args.exptype=="sph" or args.exptype=="sph2" or args.exptype=="les3900" or args.exptype=="les3900rect" or args.exptype=="dns300" or args.exptype=="lavision_jet" or args.exptype=="lavision_bkgonly" or args.exptype=="lavision_re300exp" or args.exptype=="lavision_re3900exp" or args.exptype=="lptchal4500":
    print('This is a {} case, make sources and load para_config'.format(args.exptype))
else:
    print("Choose the right case.")
    os.abort()
    
build_dir=os.environ['STBENSPTV_PATH']+'/build/build_cmake/'
if not os.path.exists(build_dir):
    os.makedirs(build_dir)

os.chdir(build_dir)

if args.parallelscheme=="NO":
    cmake_suffix1='-DSINGLECORE=true'
    print("Single Core build")
else:
    cmake_suffix1='-DSINGLECORE=false'
    if args.parallelscheme=="TBB":
        cmake_suffix3='-DPARALLEL_SCHEME=TBB'
    elif args.parallelscheme=="CUDA":
        cmake_suffix3='-DPARALLEL_SCHEME=CUDA'

if args.debug:
    cmake_suffix2='-DCMAKE_BUILD_TYPE=Debug'
else:
    cmake_suffix2='-DCMAKE_BUILD_TYPE=Release'

if platform.system()=='Darwin':
    cmake_prefix="-DCMAKE_PREFIX_PATH=/anaconda3/lib/python3.7/site-packages/torch"
elif platform.system()=='Linux':
    cmake_prefix="-DCMAKE_PREFIX_PATH=/opt/libtorch/"

if not os.path.isfile('Makefile'):
    if args.parallelscheme=="NO":
        call(["cmake", cmake_prefix, "..", cmake_suffix1, cmake_suffix2])
    else:
        call(["cmake", cmake_prefix, "..", cmake_suffix1, cmake_suffix2, cmake_suffix3])

call(["make", "-j", args.processors, args.target])

parasrcpath=os.environ['STBENSPTV_PATH']+'/para_config/para_config_'+args.exptype

paradstpath=os.path.join(test_dir,'para_config')

shutil.copy(parasrcpath, paradstpath)