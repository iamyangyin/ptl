#script for pipeline exe
import argparse
import os
import shutil 
import subprocess

def parse_var(s):
    """
    Parse a key, value pair, separated by '='
    That's the reverse of ShellArgs.

    On the command line (argparse) a declaration will typically look like:
        foo=hello
    or
        foo="hello world"
    """
    items = s.split('=')
    key = items[0].strip() # we remove blanks around keys, as is logical
    if len(items) > 1:
        # rejoin the rest:
        value = '='.join(items[1:])
    return (key, value)


def parse_vars(items):
    """
    Parse a series of key-value pairs and return a dictionary
    """
    d = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            d[key] = value
    return d

parser = argparse.ArgumentParser()
parser.add_argument("--trans", action="store_true", help="rerun transport")
parser.add_argument("--rec", action="store_true", help="rerun make record")
parser.add_argument("--calib", action="store_true", help="rerun calibration")
parser.add_argument("--pre", action="store_true", help="rerun preproc method")
parser.add_argument("--syn", action="store_true", help="rerun transport, make record, calibration and preproc methods")
parser.add_argument("--ipr", action="store_true", help="rerun ipr method")
parser.add_argument("--init", action="store_true", help="rerun init method")
parser.add_argument("--klpt", action="store_true", help="rerun klpt method")
parser.add_argument("--post", action="store_true", help="rerun postprocess method")
parser.add_argument("--lapiv", action="store_true", help="rerun lapiv method")
parser.add_argument("--lapiv_tr", action="store_true", help="rerun lapiv_tr")
parser.add_argument("--lavision", action="store_true", help="rerun lavision method")
parser.add_argument("--lptchal", action="store_true", help="rerun lptchal method")
parser.add_argument("--dev", action="store_true", help="rerun dev method")

parser.add_argument("--set",
                        metavar="KEY=VALUE",
                        nargs='+',
                        help="Set a number of key-value pairs "
                             "(do not put spaces before or after the = sign). "
                             "If a value contains spaces, you should define "
                             "it with double quotes: "
                             'foo="this is a sentence". Note that '
                             "values are always treated as strings.")
args=parser.parse_args()

# parse the key-value pairs
values = parse_vars(args.set)

params={}
with open("test/para_config") as file:
    for line in file:
        line=line.partition("#")[0]
        line=line.rstrip()
        line=line.partition("//")[0]
        line=line.rstrip()

        if not line.strip(): continue

        key, value = line.split()
        params[key]=value

for key in list(values.keys()):
    params[key] = values[key]

if not args.lapiv_tr:
    f=open("test/para_config","w")
    for key in params.keys():
        f.write("{}  {}\n".format(key, params[key]))
    f.close()

if args.syn:
    args.trans = True
    args.rec   = True
    args.calib = True
    args.pre   = True    
    
if args.trans:
    print("run transport module")
    subprocess.call("bin/transexe")

if args.rec:
    print("run make records module")
    subprocess.call("bin/recexe")

if args.calib:
    print("run calibration module")
    subprocess.call("bin/calibexe")

if args.pre:
    print("run preprocess module")
    subprocess.call('bin/preprocexe', shell=True)

if args.ipr:
    print("run ipr module")
    subprocess.call('bin/iprexe', shell=True)

if args.init:
    print("run init Track module")
    subprocess.call('bin/initexe', shell=True)

if args.klpt:
    print("run klpt module")
    subprocess.call('bin/klptexe', shell=True)

if args.lapiv:
    print("run lapiv double frame module")
    subprocess.call('bin/lapivexe', shell=True)

if args.lapiv_tr:
    print("run lapiv time resolved module")
    for i in range(int(params["it_lapiv_deb"]), int(params["it_tot"])):

        para_config_file_name="test/para_config_"+"{:06d}".format(int(params["n_part"]))

        print('at cycle {}'.format(i))
        params["it_lapiv_deb"] = str(i)

        f=open(para_config_file_name,"w")
        for key in params.keys():
            f.write("{}  {}\n".format(key, params[key]))
        f.close()

        subprocess.call('build_cmake/lapivexe '+params["n_part"], shell=True)

if args.post:
    print("run postprocess module")
    subprocess.call('bin/postprocexe', shell=True)
    
if args.dev:
    print("run dev module")
    subprocess.call('bin/devexe', shell=True)
