void Calibration::generate3DPlanerPointsInObjectSpace(void){

	cv::Point3d Xc_top_round, Xc_bottom_round;
	Xc_top_round   =Xc_top_;
	Xc_bottom_round=Xc_bottom_;

	dXc_.x=(Xc_top_round.x-Xc_bottom_round.x)/static_cast<double>(nXc_rig_.x-1);
	dXc_.y=(Xc_top_round.y-Xc_bottom_round.y)/static_cast<double>(nXc_rig_.y-1);
	dXc_.z=(Xc_top_round.z-Xc_bottom_round.z)/static_cast<double>(nXc_rig_.z-1);

	cout<<"dXc_"<<endl<<dXc_<<endl;

	VectorXd Xgrid, Ygrid;

	Xgrid.setLinSpaced(nXc_rig_.x, 0., Xc_top_round.x-Xc_bottom_round.x);
	Ygrid.setLinSpaced(nXc_rig_.y, 0., Xc_top_round.y-Xc_bottom_round.y);

	cout<<"Xgrid"<<endl<<Xgrid.transpose()<<endl;
	cout<<"Ygrid"<<endl<<Ygrid.transpose()<<endl;

	MatrixXd XXgrid,YYgrid;
	XXgrid=Xgrid.transpose().replicate(nXc_rig_.y, 1);
	YYgrid=Ygrid.replicate(1, nXc_rig_.x);

	std::vector<cv::Point3d> Xcoord_grid_v;
	double theta=0.;

	//Rotation about Y axis
	// rMat=(cv::Mat_<double>(3,3)<< cos(theta), 0, sin(theta),
	// 							  0, 1, 0,
	// 							  -sin(theta), 0, cos(theta));
	//Rotation about X axis
	// rMat=(cv::Mat_<double>(3,3)<< 1, 0, 0,
	// 						  0, cos(theta), -sin(theta),
	// 						  0, sin(theta), cos(theta));

	MatrixXd XXgrid_rot, YYgrid_rot, ZZgrid_rot;

    VectorXd  theta_eigen1, theta_eigen2, theta_eigen3;
    theta_eigen1.setLinSpaced(7, 0, 60);
    theta_eigen2.setLinSpaced(7, 0, 30);
    theta_eigen3.setLinSpaced(7, 120, 180);

	for(int v=0; v<theta_eigen1.size(); v++){

		theta = theta_eigen1(v)/180.*M_PI;
		
		XXgrid_rot = cos(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.x;
		YYgrid_rot = YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.y;
		ZZgrid_rot = sin(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.z;

		for(int i=0; i<nXc_rig_.x; i++)
			for(int j=0; j<nXc_rig_.y; j++)
				Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

		Xcoord_obj_.push_back( Xcoord_grid_v );
		Xcoord_grid_v.clear();
	}

	// for(int v=0; v<theta_eigen1.size(); v++){

	// 	theta = theta_eigen1[v]/180.*M_PI*-1.;

	// 	XXgrid_rot = cos(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.x;
	// 	YYgrid_rot = YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.y;
	// 	ZZgrid_rot = sin(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_top_round.z;

	// 	for(int i=0; i<nXc_rig_.x; i++)
	// 		for(int j=0; j<nXc_rig_.y; j++)
	// 			Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

	// 	Xcoord_obj_.push_back( Xcoord_grid_v );
	// 	Xcoord_grid_v.clear();
	// }

	// for(int v=0; v<theta_eigen3.size(); v++){

	// 	theta = theta_eigen3[v]/180.*M_PI;
		
	// 	XXgrid_rot = cos(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*(Xc_top_round.x-Xc_bottom_round.x);
	// 	YYgrid_rot = YYgrid;
	// 	ZZgrid_rot = sin(theta)*XXgrid;

	// 	for(int i=0; i<nXc_rig_.x; i++)
	// 		for(int j=0; j<nXc_rig_.y; j++)
	// 			Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

	// 	Xcoord_obj_.push_back( Xcoord_grid_v );
	// 	Xcoord_grid_v.clear();
	// }

	// for(int v=0; v<theta_eigen3.size(); v++){

	// 	theta = theta_eigen3[v]/180.*M_PI*-1.;
		
	// 	XXgrid_rot = cos(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*(Xc_top_round.x-Xc_bottom_round.x);
	// 	YYgrid_rot = YYgrid;
	// 	ZZgrid_rot = sin(theta)*XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*(Xc_top_round.z-Xc_bottom_round.z);

	// 	for(int i=0; i<nXc_rig_.x; i++)
	// 		for(int j=0; j<nXc_rig_.y; j++)
	// 			Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

	// 	Xcoord_obj_.push_back( Xcoord_grid_v );
	// 	Xcoord_grid_v.clear();
	// }

	// for(int v=0; v<theta_eigen2.size(); v++){

	// 	theta = theta_eigen2[v]/180.*M_PI*-1.;
		
	// 	XXgrid_rot = XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.x;
	// 	YYgrid_rot = cos(theta)*YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.y;
	// 	ZZgrid_rot = -sin(theta)*YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.z;

	// 	for(int i=0; i<nXc_rig_.x; i++)
	// 		for(int j=0; j<nXc_rig_.y; j++)
	// 			Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

	// 	Xcoord_obj_.push_back( Xcoord_grid_v );
	// 	Xcoord_grid_v.clear();
	// }

	// for(int v=0; v<theta_eigen2.size(); v++){

	// 	theta = theta_eigen2[v]/180.*M_PI;
		
	// 	XXgrid_rot = XXgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.x;
	// 	YYgrid_rot = cos(theta)*YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_bottom_round.y;
	// 	ZZgrid_rot = -sin(theta)*YYgrid + MatrixXd::Ones(XXgrid.rows(), XXgrid.cols())*Xc_top_round.z;

	// 	for(int i=0; i<nXc_rig_.x; i++)
	// 		for(int j=0; j<nXc_rig_.y; j++)
	// 			Xcoord_grid_v.push_back( cv::Point3d( XXgrid_rot(j,i), YYgrid_rot(j,i), ZZgrid_rot(j,i)) );

	// 	Xcoord_obj_.push_back( Xcoord_grid_v );
	// 	Xcoord_grid_v.clear();
	// }

	nXc_rig_.z = Xcoord_obj_.size();
}

/*
void Calibration::getPinholeModelParametersFromPureTranslation(Camera* cam){

	assert(xcoord_img_.size()==n_cam_);

	std::vector<cv::Point2d> Xcoord_obj_2D;
	std::vector<cv::Mat> 	 homo_mat;
	std::vector<double> 	 Z_pos;
	std::vector<int> 		 Z_pose{0,1,2,3,4};

	for(int i=0; i<Z_pose.size(); i++){
		for(auto Xc: Xcoord_obj_[ Z_pose.at(i) ]){
			Xcoord_obj_2D.push_back(cv::Point2d( Xc.x, Xc.y ));
		}

		auto homo=cv::findHomography(Xcoord_obj_2D, xcoord_img_[cam->i_cam_][ Z_pose.at(i) ], cv::RANSAC);
		homo_mat.push_back( homo );

		Z_pos.push_back( Xcoord_obj_.at( Z_pose.at(i) ).front().z );

		Xcoord_obj_2D.clear();
		cout<<"use plane "<<i<<" at "<<Z_pos.at(i)<<endl;
		cout<<"homo mat for Z "<<Z_pose[i]<<" type is "<<homo_mat.back().type()<<endl<<homo_mat.back()<<endl;
	}

	cout<<"determine scale parameter between two homograph matrix"<<endl;

	std::vector<double> scale_para_vec;

	for(int i=0; i<Z_pose.size()-1; i++){
		cv::Mat scale_mat;
		cv::divide(homo_mat.at(0), homo_mat.at(i+1), scale_mat);
		cout<<"scale_mat"<<endl<<scale_mat<<endl;

		if(source_==FromVirtualCamera){
			scale_para_vec.push_back(scale_mat.at<double>(0,0));
		}
		else if(source_==FromLaVision){
			cout<<"I have no idea why this works!!"<<endl;
			if(cam->i_cam_==0 || cam->i_cam_==3)
				scale_para_vec.push_back(scale_mat.at<double>(0,0));
			else
				scale_para_vec.push_back(scale_mat.at<double>(1,1));
		}
	}

	for(auto scale_para: scale_para_vec)
		cout<<"scale_para "<<scale_para<<endl;

	cv::Mat rMat;
	this->calibrateCameraFromPureTranslation( homo_mat, scale_para_vec, Z_pos, cam->cameraMatrix_, rMat, cam->T_, true, true );

	cv::Rodrigues(rMat, cam->R_);

	cam->distCoeffs_=cv::Mat::zeros(8, 1, CV_64F);

	cout<<"Camera "<<cam->i_cam_<<endl;
	cout<<"cameraMatrix      "<<endl<<cam->cameraMatrix_<<endl;
	cout<<"rotationMatrix    "<<endl<<rMat<<endl;
	cout<<"rotationVector    "<<endl<<cam->R_<<endl;
	cout<<"translationVector "<<endl<<cam->T_<<endl;

	this->getDistortParameters(cam);
	this->testSamplePoints(cam, true);
}

cv::Point2d Calibration::getDistortPoint(const cv::Point2d& xc, Camera& cam){

	VectorXd design_mat(cam.n_distCoeff_);

	design_mat<<1, xc.x, xc.y,
				std::pow(xc.x,2), xc.x*xc.y, std::pow(xc.y,2), 
				std::pow(xc.x,3), std::pow(xc.x,2)*xc.y, xc.x*std::pow(xc.y,2), std::pow(xc.y,3);

	return cv::Point2d( cam.a_distCoeffs_.dot(design_mat), cam.b_distCoeffs_.dot(design_mat) );
}

void Calibration::getDistortParameters(Camera* cam){

	std::vector<cv::Point2d> xcoord_grid_v;
	std::vector<cv::Point3d> p3_in_cv;

	cv::Mat_<double> design_mat,b_x,b_y,wx,wy;
	cv::Mat design_vec;

	design_mat.create(nXc_rig_.x*nXc_rig_.y*nXc_rig_.z, cam->n_distCoeff_);

	for(int z=0; z<nXc_rig_.z; z++){
		for(auto& Xc: Xcoord_obj_[z])
			p3_in_cv.push_back( cv::Point3d(Xc.x, Xc.y, 0.) );

		cv::projectPoints(p3_in_cv, cam->rvecs_[z], cam->tvecs_[z], cam->cameraMatrix_, cam->distCoeffs_, xcoord_grid_v);

		int i=0;
		for(auto& xc: xcoord_grid_v){
			design_vec=(cv::Mat_<double>(1, cam->n_distCoeff_)<< 1, xc.x, xc.y, 
												 std::pow(xc.x,2), xc.x*xc.y, std::pow(xc.y,2), 
											     std::pow(xc.x,3), std::pow(xc.x,2)*xc.y, xc.x*std::pow(xc.y,2), std::pow(xc.y,3)
											    );

			design_vec.copyTo( design_mat.row(z*nXc_rig_.x*nXc_rig_.y+i) );
			i++;
		}
		
		xcoord_grid_v.clear();
		p3_in_cv.clear();
	}

	b_x.create(nXc_rig_.x*nXc_rig_.y*nXc_rig_.z, 1);
	b_y.create(nXc_rig_.x*nXc_rig_.y*nXc_rig_.z, 1);
	for(int z=0; z<nXc_rig_.z; z++){
		int i=0;
		for(auto& xc: xcoord_img_[cam->i_cam_][z]){
			b_x(z*nXc_rig_.x*nXc_rig_.y+i)=xc.x;
			b_y(z*nXc_rig_.x*nXc_rig_.y+i)=xc.y;
			i++;
		}
	}

	wx.create(cam->n_distCoeff_, 1);
	wy.create(cam->n_distCoeff_, 1);

	cv::solve(design_mat, b_x, wx, cv::DECOMP_SVD);
	cv::solve(design_mat, b_y, wy, cv::DECOMP_SVD);

	cv::cv2eigen(wx, cam->a_distCoeffs_);
	cv::cv2eigen(wy, cam->b_distCoeffs_);
}

cv::Mat Calibration::get_vij(const cv::Mat& h1, const cv::Mat& h2, bool skewness_flag){

	cv::Mat vij;
	int num_unknowns = skewness_flag ? 6 : 5;
	int skewness_idx = skewness_flag ? 1 : 0;

	vij.create(1, num_unknowns, CV_64FC1);

	vij.at<double>(0, 0) = h1.at<double>(0,0) * h2.at<double>(0,0);
	if(skewness_flag){
		vij.at<double>(0, 1) = h1.at<double>(0,0) * h2.at<double>(1,0) + h1.at<double>(1,0) * h2.at<double>(0,0);
	}
	vij.at<double>(0, skewness_idx + 1) = h1.at<double>(1,0) * h2.at<double>(1,0);
	vij.at<double>(0, skewness_idx + 2) = h1.at<double>(2,0) * h2.at<double>(0,0) + h1.at<double>(0,0) * h2.at<double>(2,0);
	vij.at<double>(0, skewness_idx + 3) = h1.at<double>(2,0) * h2.at<double>(1,0) + h1.at<double>(1,0) * h2.at<double>(2,0);
	vij.at<double>(0, skewness_idx + 4) = h1.at<double>(2,0) * h2.at<double>(2,0);

	return vij;
}

void Calibration::calibrateCameraFromPureTranslation(const std::vector<cv::Mat>& Homo, const std::vector<double>& scale_para_vec, const std::vector<double>& Z_pos, cv::Mat& K, cv::Mat& R, cv::Mat& T, bool skewness_flag, bool verbal){

	int num_views = Homo.size();
	int num_row = num_views*3;
	int num_unknowns = skewness_flag ? 6 : 5;

	cv::Mat h1 = Homo.at(0).col(0), 
			h2 = Homo.at(0).col(1),
			h3 = Homo.at(0).col(2);

	cv::Mat design_mat;
	design_mat.create(num_row, num_unknowns, CV_64FC1);

	cv::Mat v1 = this->get_vij(h1, h2, skewness_flag);
	v1.copyTo( design_mat.row(0) );
	
	cv::Mat v2 = this->get_vij(h1, h1, skewness_flag);
	v2.copyTo( design_mat.row(1) );

	cv::Mat v3 = this->get_vij(h2, h2, skewness_flag);
	v3.copyTo( design_mat.row(2) );

	for(int i=1; i<num_views; i++){

		cv::Mat H2_rescale = Homo.at(i)*scale_para_vec.at(i-1);

		cout<<"view "<<i<<" H2_rescale"<<endl
			<<H2_rescale<<endl;

		cv::Mat h3_hat = H2_rescale.col(2) - h3;

		cv::Mat v4 = this->get_vij(h1, h3_hat, skewness_flag);
		v4.copyTo( design_mat.row(i*3) );

		cv::Mat v5 = this->get_vij(h2, h3_hat, skewness_flag);
		v5.copyTo( design_mat.row(i*3+1) );

		cv::Mat v6 = this->get_vij(h3_hat, h3_hat, skewness_flag);
		v6.copyTo( design_mat.row(i*3+2) );
	}

	cv::Mat rhs;
	rhs.create(num_row, 1, CV_64FC1);

	double Z_pq = Z_pos.at(1)-Z_pos.at(0);
	double Z_p2 = Z_pos.at(2)-Z_pos.at(0);
	double Z_p3 = Z_pos.at(3)-Z_pos.at(0);
	double Z_p4 = Z_pos.at(4)-Z_pos.at(0);

	rhs = (cv::Mat_<double>(num_row, 1)<< 0, 1, 1, 
										  0, 0, Z_pq*Z_pq,
										  0, 0, Z_p2*Z_p2,
										  0, 0, Z_p3*Z_p3,
								          0, 0, Z_p4*Z_p4);

	cv::Mat b;
	b.create(num_unknowns, 1, CV_64FC1);

	cv::solve(design_mat, rhs, b, cv::DECOMP_SVD);

	MatrixXd B(3,3);
	if(skewness_flag)
		B<< b.at<double>(0), b.at<double>(1), b.at<double>(3), 
			b.at<double>(1), b.at<double>(2), b.at<double>(4),
			b.at<double>(3), b.at<double>(4), b.at<double>(5);
	else
		B<< b.at<double>(0), 0, 			  b.at<double>(2), 
			0, 				 b.at<double>(1), b.at<double>(3),
			b.at<double>(2), b.at<double>(3), b.at<double>(4);

	Eigen::LLT<MatrixXd> lltofB(B);
	MatrixXd 			 L=lltofB.matrixL();
	MatrixXd    		 Linv=L.inverse();
	MatrixXd    		 Kl=Linv.transpose();

	if(verbal)
		cout<<"B"<<endl<<B<<endl
			<<"The decomposition is"<<endl<<L<<endl
			<<"Linv"<<endl<<Linv<<endl
			<<"Kl"<<endl<<Kl<<endl;

	auto lambda=1/Kl(2,2);
	if(verbal) cout<<"lambda "<<lambda<<endl;

	MatrixXd K_eigen=Kl*lambda;
	cv::eigen2cv(K_eigen, K);

	MatrixXd h1_eigen, h2_eigen, h3_eigen, h3_hat_eigen;
	cv::cv2eigen(h1, h1_eigen);
	cv::cv2eigen(h2, h2_eigen);
	cv::cv2eigen(h3, h3_eigen);

	cv::Mat h3_hat = Homo.at(1).col(2)*scale_para_vec.at(0) - h3;	
	cv::cv2eigen(h3_hat, h3_hat_eigen);

	MatrixXd R_eigen(3,3), T_eigen;
	R_eigen.col(0) = lambda*K_eigen.inverse()*h1_eigen;
	R_eigen.col(1) = lambda*K_eigen.inverse()*h2_eigen;
	R_eigen.col(2) = lambda*K_eigen.inverse()*h3_hat_eigen/Z_pq;
	cv::eigen2cv(R_eigen, R);

	T_eigen = lambda*K_eigen.inverse()*h3_eigen - R_eigen.col(2)*Z_pos.at(0);
	cv::eigen2cv(T_eigen, T);

	if(verbal)
		cout<<"h1Bh2 "<<h1_eigen.transpose()*B*h2_eigen<<endl
			<<"h1Bh1 "<<h1_eigen.transpose()*B*h1_eigen<<endl
			<<"h2Bh2 "<<h2_eigen.transpose()*B*h2_eigen<<endl
			<<"h1Bh3 "<<h1_eigen.transpose()*B*h3_hat_eigen<<endl
			<<"h2Bh3 "<<h2_eigen.transpose()*B*h3_hat_eigen<<endl
			<<"h3Bh3 "<<h3_hat_eigen.transpose()*B*h3_hat_eigen<<endl;
}

//bug in stereoRectify
void Calibration::doStereoRectify(Camera* cam1, Camera* cam2){

	cv::Mat R1, R2, P1, P2, Q;
    cv::Rect validRoi[2];
	cv::stereoRectify(cam1->cameraMatrix_, cam1->distCoeffs_, cam2->cameraMatrix_, cam2->distCoeffs_, cv::Size(cam1->n_width_, cam1->n_height_),
						cam1->R_betw_cams_.at(cam2->i_cam_), cam1->T_betw_cams_.at(cam2->i_cam_), R1, R2, P1, P2, Q,
						cv::CALIB_ZERO_DISPARITY, 1, cv::Size(cam1->n_width_, cam1->n_height_), &validRoi[0], &validRoi[1]);

	cout<<"R1"<<endl<<R1<<endl
		<<"R2"<<endl<<R2<<endl
		<<"P1"<<endl<<P1<<endl
		<<"P2"<<endl<<P2<<endl
		<<"Q"<<endl<<Q<<endl
		<<"validRoi[0] "<<validRoi[0]<<endl
		<<"validRoi[1] "<<validRoi[1]<<endl;
}

void Calibration::solvePnP(Camera* cam){

	double skew=0.;
	if(source_==FromLaVision){
		cam->f_=120;
		cam->k_=50;
	}

	cv::Mat cameraMatrix = cv::Mat::eye(3, 3, CV_64F);

    cameraMatrix.at<double>(0,0)=cam->k_*cam->f_;
    cameraMatrix.at<double>(1,1)=cam->k_*cam->f_;
    cameraMatrix.at<double>(0,1)=skew;
    cameraMatrix.at<double>(0,2)=cam->n_width_/2;
    cameraMatrix.at<double>(1,2)=cam->n_height_/2;

    cv::Mat distCoeffs = (cv::Mat_<double>(8, 1)<< 0, 0, 0, 0, 0, 0, 0, 0);

    std::vector<cv::Mat> rVec, tVec, rMat;

	for(int i=0; i<nXc_rig_.z; i++){
		cv::Mat rvec, tvec, rmat;
		cv::solvePnP(Xcoord_obj_.at( i ), xcoord_img_[cam->i_cam_][ i ], cameraMatrix, distCoeffs,
					rvec, tvec);

		cv::Rodrigues(rvec, rmat);

		rVec.push_back(rvec);
		tVec.push_back(tvec);
		rMat.push_back(rmat);
	}

	cam->cameraMatrix_=cameraMatrix;
	cam->distCoeffs_=distCoeffs;
	cam->R_=rVec[0];
	cam->T_=tVec[0];

	cout<<"Prior"<<endl;
	cout<<"cam->cameraMatrix_"<<endl<<cam->cameraMatrix_<<endl
		<<"cam->distCoeffs_"<<endl<<cam->distCoeffs_<<endl
		<<"cam->R_"<<endl<<cam->R_<<endl
		<<"cam->T_"<<endl<<cam->T_<<endl;

	this->testSamplePoints(cam, false);
}
*/

cv::Mat_<double> Camera::interpAndSubSample(int psf_idx, const VectorXd& x_presmpl, const VectorXd& y_presmpl, const VectorXd& x_smpl, const VectorXd& y_smpl){

	cv::Mat_<double> Imgi_smpl=cv::Mat_<double>::zeros(x_smpl.size(), y_smpl.size());

	cv::Mat_<double> Imgi_presmpl=cv::Mat_<double>::zeros(x_presmpl.size(), y_presmpl.size());
	PSF_ImgPreSmpl_vec_[psf_idx].copyTo(Imgi_presmpl);

	double xp,yp,x1,y1,x2,y2;
	int k_init=0,l_init=0,k_target=0,l_target=0;

	for(int i=0; i<x_smpl.size(); i++){
		for(int j=0; j<y_smpl.size(); j++){
			xp=x_smpl(i);
			yp=y_smpl(j);

			if(x_presmpl(0)>xp || std::abs(x_presmpl(0)-xp)<1e-6) 
				k_target=0;
			else if(*(x_presmpl.tail(1).data())<xp || std::abs(*(x_presmpl.tail(1).data())-xp)<1e-6)
				k_target=x_presmpl.size();
			else{
				for(int k=k_init; k<x_presmpl.size(); k++)
					if(x_presmpl(k)>=xp){
						k_target=k;
						break;
					}
			}
		
			if(y_presmpl(0)>yp || std::abs(y_presmpl(0)-yp)<1e-6) 
				l_target=0;
			else if(*(y_presmpl.tail(1).data())<yp || std::abs(*(y_presmpl.tail(1).data())-yp)<1e-6)
				l_target=y_presmpl.size();
			else{
				for(int l=l_init; l<y_presmpl.size(); l++){
					if(y_presmpl(l)>=yp){
						l_target=l;
						break;
					}
				}
			}

			k_init=std::max(k_target-1,1);
			l_init=std::max(l_target-1,1);

			//constant boundary 
			if(k_target==0 &&  l_target==0)
                Imgi_smpl(i,j)=Imgi_presmpl(k_target,l_target);
            else if(k_target==0 &&  l_target==y_presmpl.size())
                Imgi_smpl(i,j)=Imgi_presmpl(k_target,l_target-1);
            else if(k_target==x_presmpl.size() &&  l_target==0)
                Imgi_smpl(i,j)=Imgi_presmpl(k_target-1,l_target);
            else if(k_target==x_presmpl.size() &&  l_target==y_presmpl.size())
                Imgi_smpl(i,j)=Imgi_presmpl(k_target-1,l_target-1);
            //linear interpolation
            else if(k_target==0 && l_target!=0 && l_target!=y_presmpl.size()){
				y1=y_presmpl(l_target-1);
				y2=y_presmpl(l_target);
                Imgi_smpl(i,j)=(Imgi_presmpl(k_target,l_target-1)*(y2-yp)+Imgi_presmpl(k_target,l_target)*(yp-y1))/(y2-y1);
            }
            else if(k_target==x_presmpl.size() && l_target!=0 && l_target!=y_presmpl.size()){
                y1=y_presmpl(l_target-1);
                y2=y_presmpl(l_target);

                Imgi_smpl(i,j)=(Imgi_presmpl(k_target-1,l_target-1)*(y2-yp)+Imgi_presmpl(k_target-1,l_target)*(yp-y1))/(y2-y1);
            }
            else if(k_target!=0 && k_target!=x_presmpl.size() && l_target==0){
                x1=x_presmpl(k_target-1);
                x2=x_presmpl(k_target);

                Imgi_smpl(i,j)=(Imgi_presmpl(k_target-1,l_target)*(x2-xp)+Imgi_presmpl(k_target,l_target)*(xp-x1))/(x2-x1);
            }
            else if(k_target!=0 && k_target!=x_presmpl.size() && l_target==y_presmpl.size()){
                x1=x_presmpl(k_target-1);
                x2=x_presmpl(k_target);

                Imgi_smpl(i,j)=(Imgi_presmpl(k_target-1,l_target-1)*(x2-xp)+Imgi_presmpl(k_target,l_target-1)*(xp-x1))/(x2-x1);
            }
			//bilinear interpolate
            else{
				x1=x_presmpl(k_target-1);
				x2=x_presmpl(k_target);
				y1=y_presmpl(l_target-1);
				y2=y_presmpl(l_target);

				Imgi_smpl(i,j)=( Imgi_presmpl(k_target-1,l_target-1)*(x2-xp)*(y2-yp) + Imgi_presmpl(k_target,l_target-1)*(xp-x1)*(y2-yp) + Imgi_presmpl(k_target-1,l_target)*(x2-xp)*(yp-y1) + Imgi_presmpl(k_target,l_target)*(xp-x1)*(yp-y1) )/( (x2-x1)*(y2-y1) );
			}
		}
	}

	return Imgi_smpl;
}

void Camera::computePSFImagePreSample(void){

	Eigen::Vector4f thetai;
	int psf_idx;
	cv::Mat_<float> Imgi_presmpl;

	double dpx=0.1;
	cv::Point2d xc=cv::Point2d(n_width_/2,n_height_/2);
	double min_mag, max_mag;

	double radius,x_bottom,x_top,y_bottom,y_top;
	double nx_presmpl,ny_presmpl;
	VectorXd x_presmpl,y_presmpl;

	for(int i=0; i<nXcPSF_.x; i++){
		for(int j=0; j<nXcPSF_.y; j++){
			for(int k=0; k<nXcPSF_.z; k++){
			    psf_idx=i*nXcPSF_.y*nXcPSF_.z + j*nXcPSF_.z + k;

				thetai=this->theta_[psf_idx];
				radius=std::ceil(1.5*( 1.0/std::sqrt( thetai(0) ) + 1.0/std::sqrt( thetai(2) ) ));

				x_bottom=xc.x-radius;
				x_top=xc.x+radius;

				y_bottom=xc.y-radius;
				y_top=xc.y+radius;

		        ny_presmpl=std::round( 2.*radius/dpx )+1;
		        nx_presmpl=std::round( 2.*radius/dpx )+1;

		        x_presmpl.setLinSpaced(ny_presmpl,x_bottom,x_top);
		        y_presmpl.setLinSpaced(nx_presmpl,y_bottom,y_top);

				Imgi_presmpl.create(nx_presmpl, ny_presmpl);

				auto det_theta=std::abs(thetai(0)*thetai(2)-thetai(1)*thetai(1));

				for(int ind_x=0; ind_x<nx_presmpl; ind_x++){
					for(int ind_y=0; ind_y<ny_presmpl; ind_y++){

						auto dx=x_presmpl(ind_x)-xc.x;
						auto dy=y_presmpl(ind_y)-xc.y;

						Imgi_presmpl(ind_x, ind_y)=thetai(3)*std::exp( -0.5*( thetai(0)*std::pow(dx,2) + thetai(1)*2.0f*dx*dy + thetai(2)*std::pow(dy,2) ) );
					}
				}

				PSF_ImgPreSmpl_vec_.push_back(Imgi_presmpl);
			}
		}
	} 
}

#ifndef NO_TORCH
torch::Tensor Camera::mappingFunction(const cv::Point3d& Xc, torch::Tensor& grad) const{

	auto mf_net=std::make_unique<Net::mappingFunctionNet>(a_, b_, z_order_);

    torch::Tensor Xc_tensor=torch::tensor({Xc.x, Xc.y, Xc.z}, torch::requires_grad(true).dtype(torch::kFloat64));

    cout<<"Xc_tensor"<<endl
    	<<Xc_tensor<<endl;

    auto xc_tensor = mf_net->forward(Xc_tensor);

    cout<<"xc_tensor"<<endl
    	<<xc_tensor<<endl;

    torch::Tensor target = torch::tensor({189., 582.}, torch::dtype(torch::kFloat64));

    cout<<"target"<<endl
    	<<target<<endl;

    torch::Tensor loss = torch::mse_loss(xc_tensor, target);

    cout<<"loss "<<endl<<loss.item<float>()<<endl;

    loss.backward();

    cout<<"Xc_tensor.grad()"<<endl
    	<<Xc_tensor.grad()<<endl;

    grad=Xc_tensor.grad();

    return xc_tensor;
}
#endif

void Transport::setBackgroundVelocityFields(const std::vector<cv::Point3d>& velocity_fields_bkg, int grid_level){

	if(overlap_flag_){
		int grid_stride=std::round(1./overlap_ratio_);

		if(grid_level==0){
			for(int k=0; k<nXc_.z; k++){
				for(int j=0; j<nXc_.y; j++){
					for(int i=0; i<nXc_.x; i++){
						auto k_grid_stride = k*grid_stride;
						auto j_grid_stride = j*grid_stride;
						auto i_grid_stride = i*grid_stride;

						auto grid_idx=k*nXc_.x*nXc_.y+j*nXc_.x+i;

						U_onfullgrid3D_bkg_[k_grid_stride](j_grid_stride,i_grid_stride)=velocity_fields_bkg[grid_idx].x;
						V_onfullgrid3D_bkg_[k_grid_stride](j_grid_stride,i_grid_stride)=velocity_fields_bkg[grid_idx].y;
						W_onfullgrid3D_bkg_[k_grid_stride](j_grid_stride,i_grid_stride)=velocity_fields_bkg[grid_idx].z;
					}
				}
			}
		}
		else{
			for(int k=0; k<nXc_overlap_vec_[grid_level-1].z; k++){
				for(int j=0; j<nXc_overlap_vec_[grid_level-1].y; j++){
					for(int i=0; i<nXc_overlap_vec_[grid_level-1].x; i++){
						auto k_grid_stride = k*grid_stride + grid_step_vec_[grid_level-1].z;
						auto j_grid_stride = j*grid_stride + grid_step_vec_[grid_level-1].y;
						auto i_grid_stride = i*grid_stride + grid_step_vec_[grid_level-1].x;

						auto grid_idx=k*nXc_overlap_vec_[grid_level-1].x*nXc_overlap_vec_[grid_level-1].y+j*nXc_overlap_vec_[grid_level-1].x+i;

						U_onfullgrid3D_bkg_[k_grid_stride](j_grid_stride,i_grid_stride)=velocity_fields_bkg[grid_idx].x;
						V_onfullgrid3D_bkg_[k_grid_stride](j_grid_stride,i_grid_stride)=velocity_fields_bkg[grid_idx].y;
						W_onfullgrid3D_bkg_[k_grid_stride](j_grid_stride,i_grid_stride)=velocity_fields_bkg[grid_idx].z;
					}
				}
			}
		}
	}
	else{
		for(int k=0; k<nXc_.z; k++){
			for(int j=0; j<nXc_.y; j++){
				for(int i=0; i<nXc_.x; i++){
					auto grid_idx=k*nXc_.x*nXc_.y+j*nXc_.x+i;

					U_onfullgrid3D_bkg_[k](j,i)=velocity_fields_bkg[grid_idx].x;
					V_onfullgrid3D_bkg_[k](j,i)=velocity_fields_bkg[grid_idx].y;
					W_onfullgrid3D_bkg_[k](j,i)=velocity_fields_bkg[grid_idx].z;
				}
			}
		}
	}

	if(verbal_){
		cout<<"Background velocity on FULL reconstructed grids:"<<endl;
		for(int k=0; k<U_onfullgrid3D_bkg_.size(); k++){
			cout<<"U_onfullgrid3D_bkg_[k] "<<k<<endl
				<<U_onfullgrid3D_bkg_[k]<<endl;
		}
	}
}

void Transport::makeZeroVelocityFields(int i){
	assert(i==0);
	velocity_fields_.assign(nXc_.z*nXc_.y*nXc_.x, cv::Point3d(0.,0.,0.));
}

double Transport::computeIntegratedDivergenceOfVelocityFields(void){

	double int_div=0.0;
	double ul,ur,vl,vr,wl,wr;

	for(int k=0; k<nXc_.z-1; k++){
		for(int j=0; j<nXc_.y-1; j++){
			for(int i=0; i<nXc_.x-1; i++){
				ul=(U_ongrid3D_[k](j,i)  +U_ongrid3D_[k](j+1,i)  +U_ongrid3D_[k+1](j,i)  +U_ongrid3D_[k+1](j+1,i))/4.0;
				ur=(U_ongrid3D_[k](j,i+1)+U_ongrid3D_[k](j+1,i+1)+U_ongrid3D_[k+1](j,i+1)+U_ongrid3D_[k+1](j+1,i+1))/4.0;
				vl=(V_ongrid3D_[k](j,i)  +V_ongrid3D_[k](j,i+1)  +V_ongrid3D_[k+1](j,i)  +V_ongrid3D_[k+1](j,i+1))/4.0;
				vr=(V_ongrid3D_[k](j+1,i)+V_ongrid3D_[k](j+1,i+1)+V_ongrid3D_[k+1](j+1,i)+V_ongrid3D_[k+1](j+1,i+1))/4.0;
				wl=(W_ongrid3D_[k](j,i)  +W_ongrid3D_[k](j,i+1)  +W_ongrid3D_[k](j+1,i)  +W_ongrid3D_[k](j+1,i+1))/4.0;
				wr=(W_ongrid3D_[k+1](j,i)+W_ongrid3D_[k+1](j,i+1)+W_ongrid3D_[k+1](j+1,i)+W_ongrid3D_[k+1](j+1,i+1))/4.0;

				int_div+=(ur-ul)*dXc_.x+(vr-vl)*dXc_.y+(wr-wl)*dXc_.z;
			}
		}
	}

	return int_div;
}

void Transport::computeLaplacianOfVelocityFields(void){

	std::vector<MatrixXd> Laplacian_U_ongrid3D, Laplacian_V_ongrid3D, Laplacian_W_ongrid3D;

	MatrixXd lap_u, lap_v, lap_w;
	lap_u.resize(nXc_.x, nXc_.y);
	lap_v.resize(nXc_.x, nXc_.y);
	lap_w.resize(nXc_.x, nXc_.y);

	double Kxx, Kyy, Kzz;
	Kxx=1.0/std::pow(dXc_.x,2);
	Kyy=1.0/std::pow(dXc_.y,2);
	Kzz=1.0/std::pow(dXc_.z,2);

	for(int k=1; k<nXc_.z-1; k++){
		for(int j=1; j<nXc_.y-1; j++){
			for(int i=1; i<nXc_.x-1; i++){

				lap_u(i,j)=( U_ongrid3D_[k](j,i+1)+U_ongrid3D_[k](j,i+1)-2.0*U_ongrid3D_[k](j,i) )*Kxx +
						   ( U_ongrid3D_[k](j-1,i)+U_ongrid3D_[k](j+1,i)-2.0*U_ongrid3D_[k](j,i) )*Kyy +
						   ( U_ongrid3D_[k-1](j,i)+U_ongrid3D_[k+1](j,i)-2.0*U_ongrid3D_[k](j,i) )*Kzz;
				lap_v(i,j)=( V_ongrid3D_[k](j,i+1)+V_ongrid3D_[k](j,i+1)-2.0*V_ongrid3D_[k](j,i) )*Kxx +
						   ( V_ongrid3D_[k](j-1,i)+V_ongrid3D_[k](j+1,i)-2.0*V_ongrid3D_[k](j,i) )*Kyy +
						   ( V_ongrid3D_[k-1](j,i)+V_ongrid3D_[k+1](j,i)-2.0*V_ongrid3D_[k](j,i) )*Kzz;
   				lap_w(i,j)=( W_ongrid3D_[k](j,i+1)+W_ongrid3D_[k](j,i+1)-2.0*W_ongrid3D_[k](j,i) )*Kxx +
						   ( W_ongrid3D_[k](j-1,i)+W_ongrid3D_[k](j+1,i)-2.0*W_ongrid3D_[k](j,i) )*Kyy +
						   ( W_ongrid3D_[k-1](j,i)+W_ongrid3D_[k+1](j,i)-2.0*W_ongrid3D_[k](j,i) )*Kzz;
			}
		}

		lap_u.block(1,0,nXc_.x-2,1)=lap_u.block(1,1,nXc_.x-2,1);
		lap_u.block(0,1,1,nXc_.y-2)=lap_u.block(1,1,1,nXc_.y-2);

		lap_u.block(1,nXc_.y-1,nXc_.x-2,1)=lap_u.block(1,nXc_.y-2,nXc_.x-2,1);
		lap_u.block(nXc_.x-1,1,1,nXc_.y-2)=lap_u.block(nXc_.x-2,1,1,nXc_.y-2);

		lap_v.block(1,0,nXc_.x-2,1)=lap_v.block(1,1,nXc_.x-2,1);
		lap_v.block(0,1,1,nXc_.y-2)=lap_v.block(1,1,1,nXc_.y-2);

		lap_v.block(1,nXc_.y-1,nXc_.x-2,1)=lap_v.block(1,nXc_.y-2,nXc_.x-2,1);
		lap_v.block(nXc_.x-1,1,1,nXc_.y-2)=lap_v.block(nXc_.x-2,1,1,nXc_.y-2);

		lap_w.block(1,0,nXc_.x-2,1)=lap_w.block(1,1,nXc_.x-2,1);
		lap_w.block(0,1,1,nXc_.y-2)=lap_w.block(1,1,1,nXc_.y-2);

		lap_w.block(1,nXc_.y-1,nXc_.x-2,1)=lap_w.block(1,nXc_.y-2,nXc_.x-2,1);
		lap_w.block(nXc_.x-1,1,1,nXc_.y-2)=lap_w.block(nXc_.x-2,1,1,nXc_.y-2);

		lap_u(0,0)=0.5*(lap_u(0,1)+lap_u(1,0));
		lap_u(nXc_.x-1,nXc_.y-1)=0.5*(lap_u(nXc_.x-1,nXc_.y-2)+lap_u(nXc_.x-2,nXc_.y-1));
		lap_u(nXc_.x-1,0)=0.5*(lap_u(nXc_.x-1,1)+lap_u(nXc_.x-2,0));
		lap_u(0,nXc_.y-1)=0.5*(lap_u(0,nXc_.y-2)+lap_u(1,nXc_.y-1));

		lap_v(0,0)=0.5*(lap_v(0,1)+lap_v(1,0));
		lap_v(nXc_.x-1,nXc_.y-1)=0.5*(lap_v(nXc_.x-1,nXc_.y-2)+lap_v(nXc_.x-2,nXc_.y-1));
		lap_v(nXc_.x-1,0)=0.5*(lap_v(nXc_.x-1,1)+lap_v(nXc_.x-2,0));
		lap_v(0,nXc_.y-1)=0.5*(lap_v(0,nXc_.y-2)+lap_v(1,nXc_.y-1));

		lap_w(0,0)=0.5*(lap_w(0,1)+lap_w(1,0));
		lap_w(nXc_.x-1,nXc_.y-1)=0.5*(lap_w(nXc_.x-1,nXc_.y-2)+lap_w(nXc_.x-2,nXc_.y-1));
		lap_w(nXc_.x-1,0)=0.5*(lap_w(nXc_.x-1,1)+lap_w(nXc_.x-2,0));
		lap_w(0,nXc_.y-1)=0.5*(lap_w(0,nXc_.y-2)+lap_w(1,nXc_.y-1));

		if(k==1){
			Laplacian_U_ongrid3D.push_back(lap_u);
			Laplacian_V_ongrid3D.push_back(lap_v);
			Laplacian_W_ongrid3D.push_back(lap_w);
		}

		Laplacian_U_ongrid3D.push_back(lap_u);
		Laplacian_V_ongrid3D.push_back(lap_v);
		Laplacian_W_ongrid3D.push_back(lap_w);
	}

	Laplacian_U_ongrid3D.push_back(lap_u);
	Laplacian_V_ongrid3D.push_back(lap_v);
	Laplacian_W_ongrid3D.push_back(lap_w);

	if(!Laplacian_velocity_fields_.empty())
		Laplacian_velocity_fields_.clear();

	cv::Point3d lap_velocity;
	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				lap_velocity=cv::Point3d(Laplacian_U_ongrid3D[k](i,j), Laplacian_V_ongrid3D[k](i,j), Laplacian_W_ongrid3D[k](i,j));
				Laplacian_velocity_fields_.push_back(lap_velocity);
			}
		}
	}

	assert(Laplacian_velocity_fields_.size()==velocity_fields_.size());
}

std::vector<cv::Point3d> Transport::computeGradientOfU(void){

	std::vector<cv::Point3d> grad_U_fields;

	double grad_Ux, grad_Uy, grad_Uz;
	cv::Point3d grad_U;

	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				if(i==0)
					grad_Ux=( U_ongrid3D_[k](j,i+1)-U_ongrid3D_[k](j,i) )/dXc_.x;	
				else if(i==nXc_.x-1)
					grad_Ux=( U_ongrid3D_[k](j,i)-U_ongrid3D_[k](j,i-1) )/dXc_.x;	
				else
					grad_Ux=( U_ongrid3D_[k](j,i+1)-U_ongrid3D_[k](j,i-1) )/dXc_.x*0.5;	
				
				if(j==0)
					grad_Uy=( U_ongrid3D_[k](j+1,i)-U_ongrid3D_[k](j,i) )/dXc_.y;	
				else if(j==nXc_.y-1)
					grad_Uy=( U_ongrid3D_[k](j,i)-U_ongrid3D_[k](j-1,i) )/dXc_.y;	
				else
					grad_Uy=( U_ongrid3D_[k](j+1,i)-U_ongrid3D_[k](j-1,i) )/dXc_.y*0.5;				
				
				if(k==0)
					grad_Uz=( U_ongrid3D_[k+1](j,i)-U_ongrid3D_[k](j,i) )/dXc_.z;	
				else if(j==nXc_.y-1)
					grad_Uz=( U_ongrid3D_[k](j,i)-U_ongrid3D_[k-1](j,i) )/dXc_.z;	
				else
					grad_Uz=( U_ongrid3D_[k+1](j,i)-U_ongrid3D_[k-1](j,i) )/dXc_.z*0.5;	

				grad_U=cv::Point3d(grad_Ux, grad_Uy, grad_Uz);
				grad_U_fields.push_back(grad_U);
			}
		}
	}

	return grad_U_fields;
}

std::vector<cv::Point3d> Transport::computeGradientOfV(void){

	std::vector<cv::Point3d> grad_V_fields;

	double grad_Vx, grad_Vy, grad_Vz;
	cv::Point3d grad_V;

	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				if(i==0)
					grad_Vx=( V_ongrid3D_[k](j,i+1)-V_ongrid3D_[k](j,i) )/dXc_.x;	
				else if(i==nXc_.x-1)
					grad_Vx=( V_ongrid3D_[k](j,i)-V_ongrid3D_[k](j,i-1) )/dXc_.x;	
				else
					grad_Vx=( V_ongrid3D_[k](j,i+1)-V_ongrid3D_[k](j,i-1) )/dXc_.x*0.5;	
				
				if(j==0)
					grad_Vy=( V_ongrid3D_[k](j+1,i)-V_ongrid3D_[k](j,i) )/dXc_.y;	
				else if(j==nXc_.y-1)
					grad_Vy=( V_ongrid3D_[k](j,i)-V_ongrid3D_[k](j-1,i) )/dXc_.y;	
				else
					grad_Vy=( V_ongrid3D_[k](j+1,i)-V_ongrid3D_[k](j-1,i) )/dXc_.y*0.5;				
				
				if(k==0)
					grad_Vz=( V_ongrid3D_[k+1](j,i)-V_ongrid3D_[k](j,i) )/dXc_.z;	
				else if(j==nXc_.y-1)
					grad_Vz=( V_ongrid3D_[k](j,i)-V_ongrid3D_[k-1](j,i) )/dXc_.z;	
				else
					grad_Vz=( V_ongrid3D_[k+1](j,i)-V_ongrid3D_[k-1](j,i) )/dXc_.z*0.5;	

				grad_V=cv::Point3d(grad_Vx, grad_Vy, grad_Vz);
				grad_V_fields.push_back(grad_V);
			}
		}
	}

	return grad_V_fields;
}

std::vector<cv::Point3d> Transport::computeGradientOfW(void){

	std::vector<cv::Point3d> grad_W_fields;

	double grad_Wx, grad_Wy, grad_Wz;
	cv::Point3d grad_W;

	for(int k=0; k<nXc_.z; k++){
		for(int j=0; j<nXc_.y; j++){
			for(int i=0; i<nXc_.x; i++){
				if(i==0)
					grad_Wx=( W_ongrid3D_[k](j,i+1)-W_ongrid3D_[k](j,i) )/dXc_.x;	
				else if(i==nXc_.x-1)
					grad_Wx=( W_ongrid3D_[k](j,i)-W_ongrid3D_[k](j,i-1) )/dXc_.x;	
				else
					grad_Wx=( W_ongrid3D_[k](j,i+1)-W_ongrid3D_[k](j,i-1) )/dXc_.x*0.5;	
				
				if(j==0)
					grad_Wy=( W_ongrid3D_[k](j+1,i)-W_ongrid3D_[k](j,i) )/dXc_.y;	
				else if(j==nXc_.y-1)
					grad_Wy=( W_ongrid3D_[k](j,i)-W_ongrid3D_[k](j-1,i) )/dXc_.y;	
				else
					grad_Wy=( W_ongrid3D_[k](j+1,i)-W_ongrid3D_[k](j-1,i) )/dXc_.y*0.5;				
				
				if(k==0)
					grad_Wz=( W_ongrid3D_[k+1](j,i)-W_ongrid3D_[k](j,i) )/dXc_.z;	
				else if(j==nXc_.y-1)
					grad_Wz=( W_ongrid3D_[k](j,i)-W_ongrid3D_[k-1](j,i) )/dXc_.z;	
				else
					grad_Wz=( W_ongrid3D_[k+1](j,i)-W_ongrid3D_[k-1](j,i) )/dXc_.z*0.5;	

				grad_W=cv::Point3d(grad_Wx, grad_Wy, grad_Wz);
				grad_W_fields.push_back(grad_W);
			}
		}
	}

	return grad_W_fields;
}
