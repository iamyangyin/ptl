#ifndef NO_TORCH
#ifndef NET
#define NET

#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <math.h>
#include <cassert>
#include <unordered_map>
#include "boost/format.hpp"
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include "boost/filesystem.hpp"
#undef BOOST_NO_CXX11_SCOPED_ENUMS

#include <Eigen/Core>
#include "opencv2/core/core.hpp"
#include "opencv2/calib3d.hpp"
#include <torch/torch.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;

using std::cout;
using std::endl;
	
namespace Net{	
	struct mappingFunctionNet : torch::nn::Module{

		VectorXd a_, b_;
		int z_order_;

		mappingFunctionNet(const VectorXd& a, const VectorXd& b, int z_order):a_(a), b_(b), z_order_(z_order){}

		torch::Tensor forward(torch::Tensor Xc_tensor){

			// if(model_type_==Camera::Polynomial || model_type_==Camera::PinholeAndPolynomial){	

		        torch::Tensor xy0=torch::tensor({a_[0], b_[0]}, torch::dtype(torch::kFloat64));

		        cout<<"xy0"<<endl
		        	<<xy0<<endl;

		        torch::Tensor xy1=torch::tensor({a_[1], a_[2], a_[3], b_[1], b_[2], b_[3]}, torch::dtype(torch::kFloat64));
		        xy1=xy1.view({2, 3});

		        cout<<"xy1"<<endl
		        	<<xy1<<endl;

		        torch::Tensor xy2=torch::tensor({a_[4], a_[5]/2., a_[7]/2., a_[5]/2., a_[6], a_[8]/2., a_[7]/2., a_[8]/2., a_[9],
		        								 b_[4], b_[5]/2., b_[7]/2., b_[5]/2., b_[6], b_[8]/2., b_[7]/2., b_[8]/2., b_[9]}, torch::dtype(torch::kFloat64));
		        xy2=xy2.view({6, 3});

		        cout<<"xy2"<<endl
		        	<<xy2<<endl;

		        torch::Tensor xyz=torch::tensor({a_[15], b_[15]}, torch::dtype(torch::kFloat64));

		        std::vector<double> xy3_v({a_[10], a_[11], a_[14], a_[12], a_[13], a_[16], a_[17], a_[18], a_[19],
		        						   b_[10], b_[11], b_[14], b_[12], b_[13], b_[16], b_[17], b_[18], b_[19]});

				if(z_order_==2){
					xy3_v[8]=0;
					xy3_v.back()=0;
		        }

	        	torch::Tensor xy3=torch::tensor(xy3_v, torch::dtype(torch::kFloat64));
		        xy3=xy3.view({6, 3});

		        cout<<"xy3"<<endl
		        	<<xy3<<endl;

		        cout<<"torch::mul(torch::ger(Xc_tensor, Xc_tensor).repeat({2, 1}), xy2)"<<endl
		        	<<torch::mul(torch::ger(Xc_tensor, Xc_tensor).repeat({2, 1}), xy2)<<endl;

		        cout<<"torch::mul(torch::ger(Xc_tensor, Xc_tensor).repeat({2, 1}), xy2).sum(0)"<<endl
		        	<<torch::mul(torch::ger(Xc_tensor, Xc_tensor).repeat({2, 1}), xy2).sum(0)<<endl;

		        Xc_tensor = xy0.view({2,1}) + torch::mm(xy1, Xc_tensor.view({3,1})) +  xyz.view({2,1})*torch::prod(Xc_tensor);
//		        				+ torch::mul(torch::ger(Xc_tensor, Xc_tensor).repeat({2,1}), xy2).sum(1)
//		        				+ torch::mul(torch::ger(Xc_tensor.pow(2), Xc_tensor).repeat({2,1}), xy3).sum();

			// }

			return Xc_tensor.view({2});
		}
	};
}
#endif
#endif