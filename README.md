# STB-ENS-PTV-C++

## Calibration module

### Camera calibration is done by

1. configure target coordinates in object space
2. generate target coordinates in image space
	* generate synthetic target coordinates using image warp function 
	* generate synthetic target coordinates using given camera intrinsic and extrinsic parameters
	* (Not yet implemented) Real image read target image and determine corresponding target coordinates using template matching 

3. fit those two sets of coordinates with a camera model:
	* Pinhole model
	* 3rd order polynomial model

## Observation module
### Synthetic Particle Image Generation module
1. read camera parameters
2. read reference data (in txt format) and generate background as well as ground truth.
3. generate synthetic image using ground truth 

### Real Particle Image Processing module

## Optimization method module
1. read camera parameters and records
2. read background, also read ground truth for evaluating purpose
3. predict the positions of ALL the particles for the next step (or next several steps in case of smoother, not yet implemented) 
4. correct the prediction by assimilating image observation
5. repeat 3-4 for time series data 

### Shake-The-Box module

### Ensemble PTV module

## Transport Module
Transport particles according to a given velocity field. Save particle position data to files.

## How to run

1. Prepare data files
2. Add your own para_config_xxx file
3. Go to build folder, change cmake_list accordingly
4. run 'python cat.py para_config_xxx' to make sources and load para_config
5. run 'python cat.py para_config_xxx -p TBB' to make TBB 
6. run 'python frog.py --syn' to launch transport, calibration, make_record and preproc module
7. run 'python frog.py --ipr' to launch ipr module, reconstruct particle field for background
8. run 'python frog.py --init' to launch initTrack module, initialize track
9. run 'python frog.py --opt' to launch opt module, using either STB or ENS method
10. run 'python frog.py --post' to launch post module, doing post processing

## Notes on choosing parameter
In para_config, here we discuss #ENS parameter
* n_ens:             		   (default 8)    larger value increases the accuracy, but also increase computational time.
* regul_const:       		   (default 1)    smaller value make converging to particle image observations
* MaxOuterIter:      		   (default 5)	  outer loop iteration after which we update the residual image for next iteration.
* sigmaXcRatioX(YZ): 		   (default 1)    larger value will produce less lost tracks.
* warm_start_flag:   		   (default true) provides a warm starting point.
* selectWarmStartMaxIter:      (default 10)	  increase if tracks are lost, but may effect accuracy
* selectWarmStart_pixel:	   (default 1)    increase if tracks are lost, but may effect accuracy
* selectWarmStart_relaxfactor: (default 0.5)

## Set environmental variables
STBENSPTV_PATH
PTVDATA_PATH
LAVISIONDATA_PATH

## Dependence
Note that under MAC OS X, please use brew to install.
* EIGEN
* OPENCV
* TINYXML2
* FFTW
* BOOST
* TBB
* OMP(optional)
* PYTORCH

   