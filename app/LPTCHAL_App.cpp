#include "App.hpp"

class LPTCHAL_App : public App
{
    private:
        std::string                     img_src_path_;

        std::string                     calib_path_;
        std::string                     calib_name_="calibpoints.txt";

        std::unique_ptr<Calibration>    calib_;
        
        bool                            process_img_flag_=false;

    public:
        LPTCHAL_App():App(){
            method_str_="LPTCHAL";

            this->loadCalibrationParams();

            process_img_flag_=stob(para_map_["process_img_flag"]);

            img_src_path_=dataDir_+data_path_;

            calib_path_=dataDir_+property_path_+calib_name_;
        }

        void initializeObject(void) final {

            for(int i_cam=0; i_cam<n_cam_; i_cam++)
                cam_.push_back( std::make_unique<Camera>( model_type_, n_cam_, i_cam, n_width_, n_height_, z_order_, f_, k_) );

            calib_ = std::make_unique<Calibration>( calibration_source_, n_cam_, Xc_top_, Xc_bottom_ );

            dataset_ = std::make_unique<LPTCHAL>(n_part_per_snapshot_, case_type_);
        }

        void printPara(void) final {
            App::printPara();
            cout<<std::boolalpha;
        }

        void run(void) final {
            if(case_name_.compare("lptchal4500")==0){
                cout<<"########### READ CALIBRATION CORRESPANDENCE FILE "<<endl;
                calib_->clearXcoord_obj_cams();
                calib_->clearxcoord_img();
                calib_->setZViews(std::vector<double>{0, 5, 10, 15, 20, 25, 30});              
                dataset_->readMarkPositionTable(calib_path_,
                                                calib_->nViews(), 
                                                calib_->get_Xcoord_obj_cams(),
                                                calib_->get_xcoord_img(), 24, 16);

                auto Xcp=cv::Point3d(0,0,15);
                for(auto& ic : cam_){
                    cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                    cout<<"###########GET PINHOLE MODEL PARAMETERS#####################"<<endl;
                    calib_->getPinholeModelParameters(ic.get(), 0);

                    cout<<"###########COMPUTE PINHOLE MAPPING FUNCTION ERROR###########"<<endl;
                    calib_->computeMappingFunctionError(*ic);

                    cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                    calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_top_, Xc_bottom_);

                    ic->checkSensitivityOfMappingFunction(Xcp, Xc_top_.x-Xc_bottom_.x);

                    calib_->setPSFParams(ic.get(), theta_);
                }

                cout<<"###########DO STEREO CALIBRATION###############################"<<endl;
                for(int ic1=0; ic1<n_cam_; ic1++){
                    for(int ic2=0; ic2<n_cam_; ic2++){
                        cout<<"Between "<<ic1<<"th camera and "<<ic2<<"th camera"<<endl;
                        calib_->getStereoCalibrationParameters( cam_[ic1].get(), cam_[ic2].get() );
                    }
                }

                for(auto& ic : cam_){
                    cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                    cout<<"###########CALIBRATE POLYNOMIAL COEFFICIENT#################"<<endl;
                    calib_->getPolynomialModelParameters(ic.get());

                    cout<<"###########COMPUTE POLYNOMIAL MAPPING FUNCTION ERROR########"<<endl;
                    calib_->computeMappingFunctionError(*ic);
                }

                cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                for(auto& ic : cam_) ic->saveParamsToYamlFile(imgDir_);
            }

            if(process_img_flag_){
                cout<<"########### PROCESSING LPTCHALLENGE PARTICLE IMAGE###############"<<endl;
                dataset_->setIMGData(img_src_path_, it_deb_, it_tot_, cam_, imgDir_);
            }
        }
};

int main(){
    std::unique_ptr<LPTCHAL_App> LPTCHAL_app=std::make_unique<LPTCHAL_App>();

    LPTCHAL_app->printPara();

    LPTCHAL_app->initializeObject();

    auto t_deb=Clock::now();
    LPTCHAL_app->run();
    auto t_fin=Clock::now();

    cout<<"CPU time of preparing LPT CHALLENGE 2020 data&configuration files: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}