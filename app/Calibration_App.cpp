#include "App.hpp"

class Calibration_App : public App
{
	private:
		std::unique_ptr<Calibration> 	calib_;

        bool                            process_img_flag_=false;

        std::string     				img_src_path_;

        std::string     				calib_path_;

        std::string     				MarkTable_path_;

        std::string     				OTF_path_;

        std::string     				mappingFunction_path_;

        std::string     				RigIMG_path_;

        bool                            read_davismf_flag_=false;
        bool                            make_init_track_flag_=false;

	public:
		Calibration_App():App(){
			method_str_="Calibration";
			this->loadCalibrationParams();

            process_img_flag_=stob(para_map_["process_img_flag"]);
		}

		void initialize(void) final {
		    for(int i_cam=0; i_cam<n_cam_; i_cam++)
                cam_.push_back( std::make_unique<Camera>( model_type_, n_cam_, i_cam, n_width_, n_height_, z_order_, f_, k_) );

            calib_ = std::make_unique<Calibration>( calibration_source_, n_cam_, Xc_top_, Xc_bottom_ );

			switch(data_source_){
				case Dataset::Source_Type::FromSynthetic:
		            img_src_path_ = obsDir_; 
		            calib_path_ = obsDir_+"/calib/";
		            MarkTable_path_ = calib_path_+ "calib_points.npz";
                    dataset_ = std::make_unique<SYNTHETIC>();
					break;
				case Dataset::Source_Type::FromLaVision:
		            img_src_path_ = dataDir_+data_path_+"B";
		            OTF_path_=dataDir_+property_path_+"OpticalTransferFunction/OpticalTransferFunction.xml";
		            mappingFunction_path_=dataDir_+property_path_+"Calibration/Calibration.xml";
		            calib_path_=dataDir_+property_path_+"Calibration_";
		            read_davismf_flag_ = case_name_.compare("lavision_jet")==0;
		            dataset_ = std::make_unique<LAVISION>();
					break;
				case Dataset::Source_Type::FromLPTChallenge:
		            img_src_path_ = dataDir_+data_path_;
		            calib_path_ = dataDir_+property_path_;
		            MarkTable_path_ = calib_path_+"calibpoints.txt";
		            dataset_ = std::make_unique<LPTCHAL>(n_part_per_snapshot_, case_type_);
					break;
			}

            cout<<"img_src_path_ "<<img_src_path_<<endl;
            cout<<"calib_path_   "<<calib_path_<<endl;
		}

		void run(void) final {
            if(read_only_bkgdata_flag_){
                double diameter = 6./sqrt(theta_);
                std::string original_img_dir=str(boost::format("%1%/data/%6%/part%2$06d_maxt%3$04d_fct0%4$04d_bkg%8$.3f_psf%5$d_dtobs%7$.2f_Emin%9$.1f_dmt%10$.1f") 
                                                % rootDir_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % static_cast<int>(Camera::psf_type_) % original_case_name_ % dtObs_ % noise_ratio_ % E_min_ % diameter);
                
                cout<<"Copy cameras' config files from "<<original_img_dir<<endl;
                for(int i=0; i<n_cam_; i++){
                    auto origial_cam_i_file=original_img_dir+"/camera"+std::to_string(i+1)+".yaml";
                    auto dst_cam_i_file=imgDir_+"/camera"+std::to_string(i+1)+".yaml";
                    boost::filesystem::copy_file(origial_cam_i_file, dst_cam_i_file, boost::filesystem::copy_option::overwrite_if_exists);
                    auto preproc_cam_i_file=original_img_dir+"/camera"+std::to_string(i+1)+"_preproc.yaml";
                    auto predst_cam_i_file=imgDir_+"/camera"+std::to_string(i+1)+"_preproc.yaml";
                    boost::filesystem::copy_file(preproc_cam_i_file,predst_cam_i_file,boost::filesystem::copy_option::overwrite_if_exists);
                }

                for(auto& ic : cam_) ic->readParamsYaml(imgDir_, false);
            }

		    std::string camera_model_type;
		    switch (model_type_){
		        case Camera::Model_Type::Pinhole:               camera_model_type="Pinhole"; break;
		        case Camera::Model_Type::Polynomial:            camera_model_type="Polynomial"; break;
		        case Camera::Model_Type::PinholeAndPolynomial:  camera_model_type="PinholeAndPolynomial"; break;
		    }

			if(case_name_.compare("lavision_re3900exp")==0){
                cout<<"########### READ CALIBRATION IMAGR FILE "<<endl;
                for(int i=0; i<cam_.size(); i++){
                    auto rig_img_i_file=calib_path_+"History/Calibration_201102_140219_original/camera"+std::to_string(i+1)+"/B";
                    dataset_->setRigIMGData(rig_img_i_file, i, imgDir_);
                }
            }

	        cout<<"###########CALIBRATION USING "<<camera_model_type<<" MODEL##################"<<endl;

            if(!read_davismf_flag_){
		        std::vector<double> ZViews;
				switch(data_source_){
					case Dataset::Source_Type::FromSynthetic:{
			    		auto dXc_rig_Z=(Xc_top_.z-Xc_bottom_.z)/static_cast<double>(nViews_-1);
			    		ZViews = std::vector<double>(nViews_);
			    		std::iota(ZViews.begin(), ZViews.end(), 0);
					    std::for_each(ZViews.begin(), ZViews.end(), [=](double &Z){ Z=Xc_bottom_.z+dXc_rig_Z*Z; });
						break;
					}
					case Dataset::Source_Type::FromLaVision:{
			            if(case_name_.compare("lavision_re3900exp")==0){
				    		ZViews = std::vector<double>{17, 20, 22, 25, 27, 30, 32, 35};
					    	MarkTable_path_ = calib_path_+"History/Calibration_201102_140219_original/MarkPositionTable.xml";
				    	}
				    	else if(case_name_.compare("lavision_re300exp")==0){
				    		ZViews = std::vector<double>{-50, -53, -25, -28, -10, -13, 0, -3, 10, 7, 25, 22, 50, 47};
				    		MarkTable_path_ = calib_path_+"191212_7plans+-50+-25+-10_pinhole/MarkPositionTable.xml";
				    	}
						break;
					}
					case Dataset::Source_Type::FromLPTChallenge:{
			    		ZViews = std::vector<double>{0, 5, 10, 15, 20, 25, 30};
			    		nXc_rig_ = cv::Point2i(24, 16);
						break;
					}
					default:
						break;
				}

	            cout<<"########### READ CALIBRATION CORRESPANDENCE FILE "<<endl;
	            calib_->clearXcoord_obj_cams();
	            calib_->clearxcoord_img();
	            calib_->setZViews(ZViews);
	            dataset_->readMarkPositionTable(MarkTable_path_,
	                                            calib_->nViews(), 
	                                            calib_->get_Xcoord_obj_cams(),
	                                            calib_->get_xcoord_img(), nXc_rig_.x, nXc_rig_.y);
        	
	            //pinhole
				for(auto& ic : cam_){
			        cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
			        cout<<"###########GET PINHOLE MODEL PARAMETERS#####################"<<endl;
					int 	view_zero = 0;
					if(case_name_.compare("lavision_re300exp")==0) view_zero = 6;

				    int opencv_calib_flag = 
				    	 cv::CALIB_FIX_ASPECT_RATIO
                        |cv::CALIB_FIX_PRINCIPAL_POINT
                        // |cv::CALIB_RATIONAL_MODEL
                        // |cv::CALIB_TILTED_MODEL
                        // |cv::CALIB_FIX_TAUX_TAUY
                        // |cv::CALIB_THIN_PRISM_MODEL
                        |cv::CALIB_FIX_K1|cv::CALIB_FIX_K2
                        |cv::CALIB_FIX_K3
                        |cv::CALIB_FIX_K4|cv::CALIB_FIX_K5|cv::CALIB_FIX_K6
                        |cv::CALIB_ZERO_TANGENT_DIST
                       	;

			        calib_->getPinholeModelParameters(ic.get(), opencv_calib_flag, view_zero);

	                cout<<"###########COMPUTE PINHOLE MAPPING FUNCTION ERROR###########"<<endl;
	                calib_->computeMappingFunctionError(*ic);
	            }

	            cout<<"###########DO STEREO CALIBRATION###############################"<<endl;
		        for(int ic1=0; ic1<n_cam_; ic1++){
			        for(int ic2=0; ic2<n_cam_; ic2++){
		        		cout<<"Between "<<ic1<<"th camera and "<<ic2<<"th camera"<<endl;
		        		calib_->getStereoCalibrationParameters(cam_[ic1].get(), cam_[ic2].get());
			        }
		        }

    			//Polynomial calib
		        if(model_type_==Camera::Model_Type::Polynomial || model_type_==Camera::Model_Type::PinholeAndPolynomial){
					for(auto& ic : cam_){
			       	 	cout<<"###########GET POLYNOMIAL COEFFICIENT#######################"<<endl;
				        double  PixelPerMmFactor = 0;
				        cv::Point3d normalizationFactor = cv::Point3d(0,0,0);
				        if(data_source_!=Dataset::Source_Type::FromLaVision){
							PixelPerMmFactor = std::abs(k_*f_/Z0_);
							normalizationFactor = cv::Point3d( double(n_width_), double(n_height_), double(n_width_)*((Xc_top_.z-Xc_bottom_.z)/(Xc_top_.x-Xc_bottom_.x)) )*1.1;
						}
						calib_->getPolynomialModelParameters(ic.get(), PixelPerMmFactor, normalizationFactor);

						cout<<"###########COMPUTE MAPPING FUNCTION ERROR###################"<<endl;
						calib_->computeMappingFunctionError(*ic);
					}
				}
			}
			else{
				//Polynomial read
				for(auto& ic : cam_){
                    cout<<"###########READ POLYNOMIAL COEFFICIENT#####################"<<endl;
                    calib_->getPolynomialModelParameters(mappingFunction_path_, ic.get(), dataset_.get());

					cout<<"###########COMPUTE MAPPING FUNCTION ERROR###################"<<endl;
					calib_->computeMappingFunctionError(*ic);
				}
			}

            //OTF
			for(auto& ic : cam_){
                cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_top_, Xc_bottom_);

				ic->checkSensitivityOfMappingFunction((Xc_top_-Xc_bottom_)/2., Xc_top_.x-Xc_bottom_.x);
			
                cout<<"########### READ OPTICAL TRANSFER FUNCTION##################"<<endl;
				if(data_source_==Dataset::Source_Type::FromLaVision) 
					calib_->readPSFParams(OTF_path_, ic.get(), dataset_.get());
				else
    				calib_->setPSFParams(ic.get(), theta_);
			}


	        cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
    	    if(!boost::filesystem::exists(imgDir_)){
        		boost::filesystem::create_directories(imgDir_);
    	    }

			for(auto& ic : cam_)
				ic->saveParamsToYamlFile(imgDir_);

			if(process_img_flag_){		
			    for(int i_cam=0; i_cam<n_cam_; i_cam++){
			        auto imgDir_icam=imgDir_+"/cam"+std::to_string(i_cam);
			        if(!boost::filesystem::exists(imgDir_icam))
			            boost::filesystem::create_directories(imgDir_icam);
			    }

                cout<<"########### PROCESSING LPTCHALLENGE PARTICLE IMAGE###############"<<endl;
                dataset_->setIMGData(img_src_path_, it_deb_, it_tot_, cam_, imgDir_);
            }

            if(data_source_==Dataset::Source_Type::FromLaVision && make_init_track_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE FIELD#################"<<endl;
				auto STB_data_name=dataDir_+data_path_+STB_data_path_+"/ExportToTecplot/"+STB_data_path_+".dat";
                cout<<STB_data_name<<endl;
                dataset_->readParticleData(STB_data_name, it_deb_, it_tot_, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);
            
                track::printTrackInformation(it_tot_, dataset_->particles_ref());

                dataset_->saveSyntheticData(imgDir_, it_tot_, it_bkg_);

                auto bkg_recon_dir=str(boost::format("%1%/bkg_recon") % imgDir_);
                if(!boost::filesystem::exists(bkg_recon_dir))
                    boost::filesystem::create_directories(bkg_recon_dir);

                for(int it=1; it<it_bkg_fin_+1; it++){
                    auto bkg_file=str(boost::format("%1%/bkg/bkg_it%2$03d.npz") % imgDir_ % it);
                    auto bkg_file_recon=str(boost::format("%1%/bkg_it%2$03d.npz") % bkg_recon_dir % it);
                    boost::filesystem::copy_file(bkg_file, bkg_file_recon, boost::filesystem::copy_option::overwrite_if_exists);
                }
            }   
		}
};

int main(){
	std::unique_ptr<Calibration_App> Calibration_app=std::make_unique<Calibration_App>();

	Calibration_app->initialize();

	auto t_deb=Clock::now();
	Calibration_app->run();
	auto t_fin=Clock::now();

	cout<<"CPU time of calibrate cameras: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
	return 0;
}