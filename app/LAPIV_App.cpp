#include "App.hpp"
#include "Python.h"
#include <stdio.h> 

class LAPIV_App : public App
{
    private:
        LAPIV::VelocityOutput_Type              velocity_output_type_;
        bool                                    full_lapiv_flag_=false;
        bool                                    lapiv_tr_flag_=false;
        bool                                    read_ipr_flag_=false;
        bool                                    ipr_file_txt_flag_=true;
        int                                     ipr_init_iter_;
        int                                     ipr_loop_iter_;
        std::unique_ptr<OptMethod>              ipr_front_opt_singlestep_;
        std::unique_ptr<PTV>                    ipr_front_ptv_;

        cv::Point3d                             meanVb_;
        cv::Point3d                             sigmaVb_=cv::Point3d(0,0,0);
        cv::Point3d                             sigmaV_;
        double                                  corr_length_;

        cv::Point3i                             nXc_recon_=cv::Point3i(0,0,0);
        double                                  resolution_;

        bool                                    coarse_to_fine_flag_;
        double                                  downsampling_ratio_;
        int                                     num_fine_grids_;
        bool                                    median_filter_flag_;
        Transport::VelocityInterpolation_Type   velocity_warping_type_;
        int                                     it_lapiv_deb_;

        int                                     grid_num_;
        int                                     grid_rstrt_;
        cv::Point3i                             nXc_rstrt_=cv::Point3i(0,0,0);
        std::string                             velo_str_;

        std::unique_ptr<LAPIV>                  lapiv_;
        std::unique_ptr<OptMethod>              klpt_back_optmethod_;
        std::unique_ptr<PTV>                    klpt_back_ipr_ptv_;
        std::unique_ptr<OptMethod>              klpt_back_ipr_opt_singlestep_;

        int                                     base_grid_=0;
        std::vector<int>                        it_pls_;

        int                                     max_nXc_x_=0;                 

        double                                  search_radius_in_pixel_=10;

        bool                                    overlap_flag_;
        double                                  overlap_ratio_; 
        int                                     num_overlap_grids_;

        // if(n_part_per_snapshot_ == 10000 )       max_nXc_x_ = 100;
        // else if(n_part_per_snapshot_ == 50000)   max_nXc_x_ = 200;
        // else if(n_part_per_snapshot_ == 100000)  max_nXc_x_ = 200;
        // else if(n_part_per_snapshot_ == 160000)  max_nXc_x_ = 200;
        // else if(n_part_per_snapshot_ == 240000)  max_nXc_x_ = 200;
        // else if(n_part_per_snapshot_ == 320000)  max_nXc_x_ = 150;
        // else if(n_part_per_snapshot_ == 400000)  max_nXc_x_ = 150;

        double                                  intensity_deletion_threshold_ratio_ = 0.1;

    public:
        LAPIV_App(const std::string& para_name_lapiv_tr):App(para_name_lapiv_tr){
            method_str_="LAPIV";

            this->loadTransportParams();
            this->loadPTVParams();
            this->loadIPRParams();
            this->loadENSParams();

            velocity_output_type_=static_cast<LAPIV::VelocityOutput_Type>(std::stoi(para_map_["velocity_output_type"]));
            full_lapiv_flag_=stob(para_map_["full_lapiv_flag"]);
            lapiv_tr_flag_=stob(para_map_["lapiv_tr_flag"]);
            read_ipr_flag_=stob(para_map_["read_ipr_flag"]);
            ipr_file_txt_flag_=stob(para_map_["ipr_file_txt_flag"]);
            ipr_init_iter_=std::stoi(para_map_["ipr_init_iter"]);
            ipr_loop_iter_=std::stoi(para_map_["ipr_loop_iter"]);

            meanVb_=cv::Point3d(std::stod(para_map_["meanVx"]),std::stod(para_map_["meanVy"]),std::stod(para_map_["meanVz"]));
            sigmaV_=cv::Point3d(std::stod(para_map_["sigmaVx"]),std::stod(para_map_["sigmaVy"]),std::stod(para_map_["sigmaVz"]));
            corr_length_=std::stod(para_map_["corr_length"]);

            resolution_=std::stod(para_map_["resolution"]);

            coarse_to_fine_flag_=stob(para_map_["coarse_to_fine_flag"]);
            downsampling_ratio_=std::stod(para_map_["downsampling_ratio"]);
            num_fine_grids_=std::stoi(para_map_["num_fine_grids"]);
            median_filter_flag_=stob(para_map_["median_filter_flag"]);
            velocity_warping_type_=static_cast<Transport::VelocityInterpolation_Type>(std::stoi(para_map_["velocity_warping_type"]));
            it_lapiv_deb_=std::stoi(para_map_["it_lapiv_deb"]);
            grid_rstrt_=std::stoi(para_map_["grid_rstrt"]);
            nXc_rstrt_=cv::Point3i(std::stoi(para_map_["nXc_x_rstrt"]),std::stoi(para_map_["nXc_y_rstrt"]),std::stoi(para_map_["nXc_z_rstrt"]));
            max_nXc_x_=std::stoi(para_map_["max_nXc_x"]);
            search_radius_in_pixel_=std::stod(para_map_["search_radius_in_pixel"]);

            overlap_flag_=stob(para_map_["overlap_flag"]);
            overlap_ratio_ = (overlap_flag_ ? std::stod(para_map_["overlap_ratio"]) : 1);
            num_overlap_grids_ = (overlap_flag_ ? std::round( std::pow(1./overlap_ratio_, 3) ) : 1);
        }

        void printPara(void) final {
            App::printPara();
            cout<<std::boolalpha;
            cout<<str(boost::format("Ensemble number              is %1$3d.") % n_ens_)<<endl;
            cout<<str(boost::format("Regularization constant      is %1$3d.") % regul_const_)<<endl;
            cout<<str(boost::format("Outer Loop Maximum iteration is %1$02d.") % outer_max_iter_)<<endl;
            cout<<str(boost::format("Ensemble position initial error      is %1% px.") % sigmaXcRatio_)<<endl;
            cout<<"local_analysis_flag          is "<<local_analysis_flag_<<endl;
            cout<<"resample_flag                is "<<resample_flag_<<endl;
            if(intensity_control_flag_){
                cout<<"joint_intensity_flag         is "<<joint_intensity_flag_<<endl;
                cout<<str(boost::format("Ensemble intensity initial error     is %1% count.") % sigmaE_)<<endl;
            }
            if(data_source_==Dataset::Source_Type::FromSynthetic){
                cout<<"reference grid               is "<<nXc_data_<<endl;
            }
            if(coarse_to_fine_flag_){
                cout<<"with down-sampling ratio        "<<downsampling_ratio_<<endl;
                cout<<"median_filter_flag_          is "<<median_filter_flag_<<endl;
                cout<<"velocity_warping_type_       is "<<static_cast<int>(velocity_warping_type_)<<endl;
            }
            if(overlap_flag_){
                cout<<"with overlapping ratio          "<<overlap_ratio_<<endl;
            }
        }

        void initialize(void) final {
            App::initialize();

            if(data_source_==Dataset::Source_Type::FromSynthetic){
                dataset_ = std::make_unique<SYNTHETIC>();
            }
            else if(data_source_==Dataset::Source_Type::FromLaVision){
               cout<<"not working!"<<endl;
               std::abort();
            }
            else if(data_source_==Dataset::Source_Type::FromLPTChallenge){
                dataset_ = std::make_unique<LPTCHAL>();
            }
            
            dynmodel_ = std::make_unique<DynModel>(DynModel::DynModel_Type::TransportModel);

            double min_intensity_ratio = 0.1;
            double initial_min_intensity_threshold_back = initial_min_intensity_threshold_;

            if(case_name_.compare("les3900rect")==0 || case_name_.compare("lavision_bkgonly")==0){
                if(n_part_per_snapshot_<=10000)
                    initial_min_intensity_threshold_*=0.25;
                else if(n_part_per_snapshot_==20000)
                    initial_min_intensity_threshold_*=0.25;
                else if(n_part_per_snapshot_==30000)
                    min_intensity_ratio=0.2;
                else if(n_part_per_snapshot_>=40000)
                    min_intensity_ratio=0.1;
            }
            else if(case_name_.compare("lavision_jet")==0){
                min_intensity_ratio = 0.015;
            }
            else if(case_name_.compare("lptchal4500")==0){
                if(n_part_per_snapshot_ <= 160000){
                    min_intensity_ratio = 0.01;
                    initial_min_intensity_threshold_back=initial_min_intensity_threshold_*0.333;
                }
                else if(n_part_per_snapshot_ == 240000){
                    min_intensity_ratio = 0.015;//max 0.03
                    initial_min_intensity_threshold_back=initial_min_intensity_threshold_*0.333;
                }
                else if(n_part_per_snapshot_ == 320000){
                    min_intensity_ratio = 0.02;//max 0.03
                    // intensity_deletion_threshold_ratio_=0.05;//default 0.1
                    initial_min_intensity_threshold_back=initial_min_intensity_threshold_*0.666;
                }
                else if(n_part_per_snapshot_ == 400000){
                    min_intensity_ratio = 0.03;//max 0.03
                    intensity_deletion_threshold_ratio_=0.05;//default 0.1
                    initial_min_intensity_threshold_back=initial_min_intensity_threshold_*0.9;
                }
            }

            cout<<"with min_intensity_ratio                     "<<min_intensity_ratio<<endl;
            cout<<"with intensity_deletion_threshold_ratio_     "<<intensity_deletion_threshold_ratio_<<endl;

            auto cam_seq = case_name_=="lptchal4500" ? std::vector<int>{0,3,1,2} : std::vector<int>{0,1,2,3}; 

            int eval_window = 2;

            intensity_control_flag_ = true; //redundant, lapiv actually do not control intensity for now

            klpt_back_optmethod_ = std::make_unique<ENS>(OptMethod::Method_Type::ENS, control_vector_type_, intensity_control_flag_,
                                                        n_part_per_snapshot_, n_part_per_snapshot_, Xc_top_, Xc_bottom_, 
                                                        pixel_lost_, dtObs_, 
                                                        dXc_px_, resDir_, eval_window,
                                                        false, warm_start_max_iter_, warm_start_pixel_, warm_start_relaxfactor_,
                                                        n_ens_, regul_const_, outer_max_iter_, local_analysis_flag_, 
                                                        true, sigmaXcRatio_, sigmaE_, joint_intensity_flag_);

            klpt_back_ipr_ptv_ = std::make_unique<PTV>(subPixel_type_, initial_min_intensity_threshold_back, filter_threshold_,
                                                        dilate_threshold_, dilate_surrounding_checker_threshold_, dilate_surrounding_difference_threshold_,
                                                        search_threshold_, Wieneke_threshold_, stereo_verbal_flag_, min_intensity_ratio,
                                                        Xc_top_, Xc_bottom_, dXc_px_, cam_seq);

            klpt_back_ipr_opt_singlestep_ = std::make_unique<ENS>(OptMethod::Method_Type::ENS, control_vector_type_, intensity_control_flag_, 
                                                                n_part_per_snapshot_, n_part_per_snapshot_, Xc_top_, Xc_bottom_,
                                                                dXc_px_, resDir_, eval_window,
                                                                n_ens_, regul_const_, IPR_max_iter_, local_analysis_flag_, 
                                                                true, sigmaXcRatio_, sigmaE_, joint_intensity_flag_);

            lapiv_ = std::make_unique<LAPIV>(OptMethod::Method_Type::LAPIV, control_vector_type_, intensity_control_flag_, 
                                            0, n_part_per_snapshot_, Xc_top_, Xc_bottom_,
                                            pixel_lost_, dtObs_,
                                            dXc_px_, resDir_, eval_window,
                                            n_ens_, regul_const_, outer_max_iter_, local_analysis_flag_, 
                                            resample_flag_, sigmaXcRatio_, sigmaE_, joint_intensity_flag_,
                                            sigmaV_, corr_length_);


            ipr_front_ptv_ = std::make_unique<PTV>( subPixel_type_, initial_min_intensity_threshold_, filter_threshold_,
                                                    dilate_threshold_, dilate_surrounding_checker_threshold_, dilate_surrounding_difference_threshold_,
                                                    search_threshold_, Wieneke_threshold_, stereo_verbal_flag_, min_intensity_ratio,
                                                    Xc_top_, Xc_bottom_, dXc_px_, cam_seq);

            ipr_front_opt_singlestep_ = std::make_unique<ENS>(  OptMethod::Method_Type::ENS, control_vector_type_, intensity_control_flag_, 
                                                                n_part_per_snapshot_, n_part_per_snapshot_, Xc_top_, Xc_bottom_,
                                                                dXc_px_, resDir_, eval_window,
                                                                n_ens_, regul_const_, IPR_max_iter_, local_analysis_flag_, 
                                                                true, sigmaXcRatio_, sigmaE_, joint_intensity_flag_);

            cout<<"####### SET TRANSPORT OBJECT AND RECONSTRUCTION GRID########"<<endl;
            nXc_recon_.x=static_cast<int>( std::round( (Xc_top_.x-Xc_bottom_.x)/resolution_ ) );
            nXc_recon_.y=static_cast<int>( std::round( (Xc_top_.y-Xc_bottom_.y)/resolution_ ) );
            nXc_recon_.z=static_cast<int>( std::round( (Xc_top_.z-Xc_bottom_.z)/resolution_ ) );

            dynmodel_->settransportRecon(true, Transport::VelocityInterpolation_Type::TriLinear, Xc_top_, Xc_bottom_, nXc_recon_, 
                                        time_scheme_, dt_, dtObs_, velocity_warping_type_);
        }
 
        void run(void) final {
            assert(coarse_to_fine_flag_);
            overlap_flag_=false;

            it_pls_=std::vector<int>{it_lapiv_deb_, it_lapiv_deb_+1};

            cout<<"############ AT CYCLE "<<it_pls_.front()<<"##############################"<<endl;
            std::vector<cv::Point3d>  velocity_interp_grid;
            std::vector<Particle_ptr> particles_base_grid;

            if(full_lapiv_flag_){//do full lapiv, full ipr & velo starting from zeros
                if(read_ipr_flag_){
                    cout<<"##### READ IPR PARTICLE AT "<<it_pls_.front()<<" ########################"<<endl;
                    assert(ipr_file_txt_flag_);
                    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> particles_ipr_in_frames;
                    track::readIPRDataIntoFrames( std::vector<int>{it_pls_.front()}, imgDir_, false, particles_ipr_in_frames );
                    auto particles_recon = track::convertFrameToParticles(it_pls_.front(), particles_ipr_in_frames);
                    particles_base_grid  = track::findTheBrightestParticles(n_part_per_snapshot_, it_pls_.front(), particles_recon, Xc_top_, Xc_bottom_);
                }
                else{
                    cout<<"##### DO INITIAL IPR AT "<<it_pls_.front()<<" ############################################"<<endl;
                    for(auto& ic : cam_)  ic->readRecord(it_pls_.front(), imgDir_);
                    ipr_front_ptv_->readRecordsFromCamera(cam_);

                    ipr_front_ptv_->setsearch_threshold(search_threshold_);
                    ipr_front_ptv_->setWieneke_threshold(Wieneke_threshold_);

                    for(int i=1; i<ipr_init_iter_+1; i++){
                        cout<<"#################### P4 Re-triangulation iteration "<<i<<"#################"<<endl;
                        auto dup_flag = true; 
                        auto adjust_threshold_flag = i>=4;

                        auto do_flag = KLPT::IPR_PTV_pipeline(ipr_front_ptv_.get(), it_pls_.front(), cam_, false, i, dup_flag);
                        KLPT::IPR_OPT_pipeline(ipr_front_ptv_.get(), ipr_front_opt_singlestep_.get(), it_pls_.front(), do_flag, cam_, i);

                        if(i>=4){
                            ipr_front_ptv_->setsearch_threshold(0.5*search_threshold_);
                            ipr_front_ptv_->setWieneke_threshold(0.5*Wieneke_threshold_);
                        }
                    }

                    particles_base_grid = track::findTheBrightestParticles(n_part_per_snapshot_, it_pls_.front(), ipr_front_ptv_->particles_bkg(), Xc_top_, Xc_bottom_);
                }//end if(read_ipr_flag_)

                cout<<"##### START LAPIV ###################################"<<endl;
                cout<<"##### GENERATE BACKGROUND VELOCITY ON BASE GRID #####"<<endl;
                cout<<"##### USING GAUSSIAN NOISE###########################"<<endl;
                velocity_interp_grid  = generateBackgroundVelocityFields(meanVb_, sigmaVb_, 
                                                        dynmodel_->transportRecon()->nXc(),
                                                        dynmodel_->transportRecon()->dXc_minimal(), corr_length_);
                
                grid_rstrt_ = 0;
            }
            else{//do lapiv with particles reconstructed and velo non-zeros
                int  it_seq = lapiv_tr_flag_ ? it_pls_.front() - 1 : it_pls_.front();

                std::string   data_path( str(boost::format("%1%/result%2$03d.dat") % resDir_ % it_seq) );
                cout<<"data_path: "<<data_path<<endl;

                dataset_->readParticleData(data_path, it_seq, it_seq+1, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);
                particles_base_grid = std::move(dataset_->get_particles_ref());

                cout<<"##### TRIM PARTICLES AT "<<it_pls_.front()<<" ##########################"<<endl;
                trimParticles(particles_base_grid, it_pls_.front(), !lapiv_tr_flag_);

                cout<<"##### START LAPIV ###################################"<<endl;
                cout<<"##### GENERATE BACKGROUND VELOCITY ON GRID "<<grid_rstrt_<<" USING VELOCITY FIELD AT PREVIOUS SNAPSHOT #####"<<endl;

                auto velo_file_path = str(boost::format("%1%/velo/Velocity_ana_%2$03d_%3$02d_raw.npz") % resDir_ % it_seq % grid_rstrt_ );                
                cout<<velo_file_path<<endl;
                dynmodel_->transportRecon()->setnXc(nXc_rstrt_);
                dynmodel_->transportRecon()->readVelocityFieldsFromNPZFile(velo_file_path);
                dynmodel_->transportRecon()->setVelocityOnGrids(dynmodel_->transportRecon()->velocity_fields());
                dynmodel_->transportRecon()->applyMedianFilter();
                dynmodel_->transportRecon()->makeVelocityToVector();

                velocity_interp_grid = dynmodel_->transportRecon()->velocity_fields(); 

                if(n_part_per_snapshot_ <= 160000) ipr_front_ptv_->setinitial_min_intensity_threshold(0.333*initial_min_intensity_threshold_);
            }

            dynmodel_->transportRecon()->makeBaseCoarseGrid();
            this->runCoarseToFineScheme(velocity_interp_grid, particles_base_grid);

            this->clearForNextCycle();
            klpt_back_optmethod_->clear();
            klpt_back_ipr_ptv_->clearDataForNextTimeLevel();
            klpt_back_ipr_opt_singlestep_->clear();
        }

        void runCoarseToFineScheme(std::vector<cv::Point3d>& velocity_interp_grid, 
                                    std::vector<Particle_ptr>& particles_base_grid){
            
            grid_num_=grid_rstrt_;

            for(; grid_num_<=num_fine_grids_; grid_num_++){
                cout<<"##### COMPUTING VELOCITY ON FINER GRID "<<grid_num_<<" #####"<<endl;

                lapiv_->initialization(particles_base_grid, it_pls_.back(), velocity_interp_grid,
                                        dynmodel_->transportRecon()->nXc(), dynmodel_->transportRecon()->dXc(), dynmodel_->transportRecon()->dXc());

                lapiv_->setVelocityNoiseLevel(grid_rstrt_, grid_num_);
                lapiv_->setregul_const(regul_const_, grid_rstrt_, grid_num_);

                cout<<"################START PREDICTION###############"<<endl;
                lapiv_->predict(dynmodel_.get(), it_pls_.back());

                cout<<"################PREPARE CORRECTION#############"<<endl;
                for(auto& ic: cam_) ic->readRecord(it_pls_.back(), imgDir_);

                lapiv_->prepare(it_pls_.back(), cam_, grid_num_);

                cout<<"################set particles_octree###########"<<endl;
                dynmodel_->transportRecon()->setparticles_octree(lapiv_->particles_field(), it_pls_.back());
                dynmodel_->transportRecon()->setsearch_radius_in_mm( cv::norm(dXc_px_)*search_radius_in_pixel_ );

                cout<<"################RUN CORRECTION#################"<<endl; 
                lapiv_->run(it_pls_.back(), cam_, dynmodel_.get(), grid_num_>=5);

                particles_base_grid = std::move( lapiv_->get_particles_field() );

                cout<<"################SAVE RAW VELOCITY##############"<<endl;
                velo_str_ = "raw";
                dynmodel_->transportRecon()->setVelocityOnGrids(lapiv_->velocity_fields_ens().back());
                this->saveResults(it_pls_.back());

                cout<<"################FILTERING RAW VELOCITY#########"<<endl;
                // dynmodel_->transportRecon()->getAvgVelocityFields();
                // if(median_filter_flag_) dynmodel_->transportRecon()->applyMedianFilter(); //apply median filter on pre-warped velocity

                auto exit_flag = dynmodel_->transportRecon()->makeCoarseToFineGrid(downsampling_ratio_, max_nXc_x_, velocity_interp_grid);
                exit_flag = exit_flag ? exit_flag : (grid_num_==num_fine_grids_);

                cout<<"################SAVE INTERPOLATED VELOCITY########"<<endl;
                velo_str_ = "warped";
                dynmodel_->transportRecon()->setVelocityOnGrids(velocity_interp_grid);
                this->saveResults(it_pls_.back());

                // if(median_filter_flag_) dynmodel_->transportRecon()->applyMedianFilter(); //apply median filter on post-warped velocity
                // dynmodel_->transportRecon()->makeVelocityToVector();
                // velocity_interp_grid=dynmodel_->transportRecon()->velocity_fields();
  
                //call python
                this->runGlobalRegularizationSchemeUsingPython();

                //read div velocity files
                cout<<"##### READ DIVERGENCE FREE VELOCITY FIELD FROM FILE##"<<endl;
                auto velo_file_path = str(boost::format("%1%/velo/Velocity_ana_%2$03d_%3$02d_div.npz") % resDir_ % it_pls_.front() % grid_num_ );
                cout<<velo_file_path<<endl;
                dynmodel_->transportRecon()->readVelocityFieldsFromNPZFile(velo_file_path);
                velocity_interp_grid.clear();
                velocity_interp_grid=dynmodel_->transportRecon()->velocity_fields();

                if(exit_flag){
                    cout<<"##### REFINE PARTICLES AT "<<it_pls_.back()<<" #####################################"<<endl;
                    lapiv_->postProcessing(particles_base_grid, it_pls_.back(), cam_, dynmodel_.get());

                    for(auto& ic: cam_) ic->readRecord(it_pls_.back(), imgDir_);

                    klpt_back_optmethod_->setaddNewParticles_before_final_iteration_flag(false);    
                    klpt_back_optmethod_->setptv_min_intensity_threshold_flag(true);
                    klpt_back_optmethod_->setptv_stereoMatching_reduced_flag(false);
                    klpt_back_optmethod_->setn_particles(particles_base_grid.size());
                    klpt_back_optmethod_->setuse_stb_or_ens_in_lapiv_tr_flag(true);
                    if(n_part_per_snapshot_ >= 320000){
                        // klpt_back_optmethod_->setscaling_intensity_type(OptMethod::ScalingIntensity_Type::Mild);
                        klpt_back_optmethod_->setintensity_deletion_threshold_ratio(intensity_deletion_threshold_ratio_);
                    }

                    klpt_back_optmethod_->initialization(particles_base_grid, it_pls_.back());

                    klpt_back_optmethod_->checkIfEnsembleParticlesAppearOnSnapshot(it_pls_.back());
                    klpt_back_optmethod_->maintainPositionInRange(it_pls_.back());

                    klpt_back_optmethod_->prepare(it_pls_.back(), cam_);

                    KLPT::runOptimization(klpt_back_optmethod_.get(), it_pls_.back(), cam_, nullptr, nullptr);

                    cout<<"save LAPIV PARTICLES file"<<endl;                    
                    cout<<"save particle fields at "<<it_pls_.front()<<" to txt..."<<endl;
                    track::saveFrameDataTXT(it_pls_.front(), klpt_back_optmethod_->particles_field(), resDir_, dtObs_, false, 1, str(boost::format("%1$03d") % it_pls_.front()));
                    cout<<"save particle fields at "<<it_pls_.front()<<" to vtk..."<<endl;
                    track::saveFrameDataVTK(it_pls_.front(), klpt_back_optmethod_->particles_field(), resDir_);
                }
                else{
                    cout<<"##### TRIM PARTICLES ##########################"<<endl;
                    trimParticles(particles_base_grid, it_pls_.front());

                    if(ipr_loop_iter_!=0){
                        cout<<"##### DO RESIDUAL IPR AT "<<it_pls_.front()<<" #####################################"<<endl;
                        for(auto& ic : cam_)  ic->readRecord(it_pls_.front(), imgDir_);
                        ipr_front_ptv_->readRecordsFromCamera(cam_);

                        ipr_front_ptv_->setsearch_threshold(search_threshold_);
                        ipr_front_ptv_->setWieneke_threshold(Wieneke_threshold_);

                        if(grid_num_==grid_rstrt_) ipr_front_ptv_->resetmin_intensity_threshold();

                        ipr_front_opt_singlestep_->setn_particles( particles_base_grid.size() );
                        if(n_part_per_snapshot_ >= 320000){
                            // ipr_front_opt_singlestep_->setscaling_intensity_type(OptMethod::Mild);
                            ipr_front_opt_singlestep_->setintensity_deletion_threshold_ratio(intensity_deletion_threshold_ratio_);
                        }

                        cout<<"################ COMPUTE RESIDUAL IMAGE USING ipr_front_opt_singlestep_########"<<endl;
                        ipr_front_opt_singlestep_->initialization( particles_base_grid, it_pls_.front() );
                        ipr_front_opt_singlestep_->prepare( it_pls_.front(), cam_ );

                        cout<<"################ SEND particles and residual images to ipr_front_ptv_##########"<<endl;
                        ipr_front_ptv_->setIPRParticleField(ipr_front_opt_singlestep_->get_particles_field());
                        ipr_front_ptv_->setIresToIrec(cam_);

                        ipr_front_opt_singlestep_->clearParticleData();
                        ipr_front_opt_singlestep_->clear();

                        for(int i=1; i<ipr_loop_iter_+1; i++){
                            auto reduced_flag = grid_num_>=0;
                            if(n_part_per_snapshot_ >= 240000)  reduced_flag = false;

                            cout<<"######## Grid "<<grid_num_<<" P"<<std::to_string(reduced_flag ? 3 : 4)<<" Re-triangulation iteration "<<i<<"#################"<<endl;
                            auto dup_flag = true; 
                            auto adjust_threshold_flag = true;

                            auto do_flag = KLPT::IPR_PTV_pipeline(ipr_front_ptv_.get(), it_pls_.front(), cam_, reduced_flag, i, dup_flag);
                            KLPT::IPR_OPT_pipeline(ipr_front_ptv_.get(), ipr_front_opt_singlestep_.get(), it_pls_.front(), do_flag, cam_, i);

                            if(reduced_flag && adjust_threshold_flag){
                                ipr_front_ptv_->setsearch_threshold(0.5*search_threshold_);
                                ipr_front_ptv_->setWieneke_threshold(0.5*Wieneke_threshold_);
                            }
                        }

                        particles_base_grid = track::findTheBrightestParticles(0, it_pls_.front(), ipr_front_ptv_->particles_bkg(), Xc_top_, Xc_bottom_);
                    }//end if(ipr_loop_iter_!=0)
                }//end if(exit_flag)

                if(exit_flag) break;
            }//end for(; grid_num_<=num_fine_grids_; grid_num_++)
        }

        void saveResults(int it_seq) final {
            if(save_result_){      
                cout<<"grid_num_ "<<grid_num_<<endl;

                std::string filtered_str = "_" + velo_str_;
                std::string ovelap_str   = (overlap_flag_  ? "_overlap"  : "");

                int it_velo = it_seq - 1;

                auto velo_file_path = str(boost::format("%1%/velo/Velocity_ana_%2$03d_%3$02d%4%%5%") 
                                                        % resDir_ % it_velo % grid_num_ % filtered_str % ovelap_str);

                cout<<"save velocity field at grid "<<grid_num_<<" to npz..."<<endl;
                dynmodel_->transportRecon()->saveVelocityFieldsToNPZFile(velo_file_path+".npz");

                cout<<"save velocity field at grid "<<grid_num_<<" to vtk..."<<endl;
                dynmodel_->transportRecon()->saveVelocityFieldsToVTKFile(velo_file_path+".vtk");
                            
                for(auto& ic : cam_) ic->saveImageResidual(it_seq, "Iresf"+std::to_string(grid_num_), resDir_);
            }
        }

        void runGlobalRegularizationSchemeUsingPython(void){
            cout<<"##### OBTAIN GLOBAL REGULARIZED VELOCITY FIELD#######"<<endl;
            FILE* file;
            int argc=4;
            wchar_t* argv[argc];

            std::string argv0 = "../post/refineUsingTorch.py";
            auto argv1 = str(boost::format("--res_dir=%1%") % resDir_);
            auto argv2 = str(boost::format("--it_velo=%1%") % it_pls_.front());
            auto argv3 = str(boost::format("--grid_num=%1%") % grid_num_);
           
            std::wstring argv0w = std::wstring(argv0.begin(), argv0.end());
            std::wstring argv1w = std::wstring(argv1.begin(), argv1.end());
            std::wstring argv2w = std::wstring(argv2.begin(), argv2.end());
            std::wstring argv3w = std::wstring(argv3.begin(), argv3.end());

            argv[0] = const_cast< wchar_t* >(argv0w.c_str());
            argv[1] = const_cast< wchar_t* >(argv1w.c_str());
            argv[2] = const_cast< wchar_t* >(argv2w.c_str());
            argv[3] = const_cast< wchar_t* >(argv3w.c_str());

            Py_SetProgramName(argv[0]);
            PySys_SetArgv(argc, argv);
            file = fopen("../post/refineUsingTorch.py","r");
            PyRun_SimpleFile(file, "../post/refineUsingTorch.py");
        }

        void clearForNextCycle(void) final {
            lapiv_->clear();
        }
};

int main(int argc, char* argv[]){
    std::string para_name_lapiv_tr("para_config");

    if(argc>1){
        int para_name_cmd_line = std::atoi(argv[1]);
        para_name_lapiv_tr = str(boost::format("%1%_%2$06d") % para_name_lapiv_tr % para_name_cmd_line);
    }

    Py_Initialize();

    std::unique_ptr<LAPIV_App> lapiv_app=std::make_unique<LAPIV_App>(para_name_lapiv_tr);

    lapiv_app->printPara();

    lapiv_app->initialize();

    auto t_deb=Clock::now();
    lapiv_app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of reconstruction Velocity fields with LAPIV method: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    
    Py_Finalize();

    return 0;
}
