#include "App.hpp"

class Dev_App : public App
{
    private:
        std::unique_ptr<Calibration> calib_;
   
        std::vector<int>             cam_seq_{0,1,2,3};
        std::unique_ptr<PTV>         ptv_;

        DSLPT::Stereo_Matching_Type  stereo_matching_type_;
        int                          disparity_max_;
        int                          BM_win_size_;
        DSLPT::Stereo_Cost_Type      stereo_cost_type_;

        std::unique_ptr<DSLPT>       dslpt_;

    public:
        Dev_App():App(){
            method_str_="Dev";

            stereo_matching_type_=static_cast<DSLPT::Stereo_Matching_Type>(std::stoi(para_map_["stereo_matching_type"]));
            disparity_max_=std::stoi(para_map_["disparity_max"]);
            BM_win_size_=std::stoi(para_map_["BM_win_size"]);
            stereo_cost_type_=static_cast<DSLPT::Stereo_Cost_Type>(std::stoi(para_map_["stereo_cost_type"]));

            this->loadCalibrationParams();
            this->loadPTVParams();
        }

        void initialize(void) final {
            App::initialize();

            if(data_source_==Dataset::Source_Type::FromSynthetic)
                dataset_ = std::make_unique<SYNTHETIC>();
            else
                std::abort();

            dslpt_ = std::make_unique<DSLPT>( subPixel_type_, initial_min_intensity_threshold_, filter_threshold_,
                                                dilate_threshold_, dilate_surrounding_checker_threshold_, dilate_surrounding_difference_threshold_,
                                                search_threshold_, Wieneke_threshold_, stereo_verbal_flag_, 0.1,
                                                Xc_top_, Xc_bottom_, dXc_px_, cam_seq_,
                                                stereo_matching_type_, disparity_max_, BM_win_size_, stereo_cost_type_);
        }

        void run(void) final {
            torch::NoGradGuard no_grad;

            pcl::PointXYZ a = pcl::PointXYZ(1, 1, 1);
            cout<<"a "<<a<<endl;

            pcl::PointXYZI b = pcl::PointXYZI({{1,1,1},{1}});
            cout<<"b "<<b<<endl;
            std::abort();
            // ///////////////////////////////////////////////////////////////////////
            // cout<<"Read Reference Particle Data"<<endl;
            // dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_);

            // std::string train_dir="/media/yin/Data/stereo_matching_pytorch/data/"+case_name_;
            // if(!boost::filesystem::exists(train_dir))
            //     boost::filesystem::create_directories(train_dir);

            // for(int it_seq=it_deb_; it_seq<=it_tot_; it_seq++){
            //     for(auto& ic : cam_)  ic->readRecord(it_seq, imgDir_);

            //     cout<<"############At it_seq "<<it_seq<<" ########################################"<<endl;
            //     utils::checkIfParticlesAppearOnSnapshot(it_seq, dataset_->particles_ref());

            //     std::vector<double> pa_v(n_part_per_snapshot_*25),
            //                         pp_v(n_part_per_snapshot_*25),
            //                         pn0_v(n_part_per_snapshot_*25),
            //                         pn1_v(n_part_per_snapshot_*25),
            //                         pn2_v(n_part_per_snapshot_*25);

            //     double window_size = 1.5;
            //     int p=0;
            //     for(const auto& ps: dataset_->particles_ref()){

            //         if(ps->is_ontrack()){
            //             auto pc1=cam_[0]->mappingFunction( ps->getCurrentXcoord(it_seq) );
            //             auto pc2=cam_[1]->mappingFunction( ps->getCurrentXcoord(it_seq) );

            //             auto roi1 = utils::getLocalPatchPos(pc1, cam_[0]->n_width(), cam_[0]->n_height(), window_size);
            //             if(roi1.area()!=25){
            //                 cout<<"roi1 "<<roi1<<endl;
            //                 std::abort(); 
            //             }
            //             auto pa = cam_[0]->Irec()(roi1);

            //             auto roi2 = utils::getLocalPatchPos(pc2, cam_[1]->n_width(), cam_[1]->n_height(), window_size);
            //             if(roi2.area()!=25) { cout<<"roi2 "<<roi2<<endl; std::abort(); }
            //             auto pp = cam_[1]->Irec()(roi2);

            //             for(int i=0; i<5; i++){
            //                 for(int j=0; j<5; j++){
            //                     pa_v.at(p*25+j*5+i) = static_cast<double>( unsigned(pa.at<uchar>(i,j)) );
            //                     pp_v.at(p*25+j*5+i) = static_cast<double>( unsigned(pp.at<uchar>(i,j)) );
            //                 }
            //             }

            //             //sample 3 negative points
            //             for(int n=0; n<3; n++){
            //                 double radius = Random::UniformDistributionNumber(1., 5.);
            //                 double angle  = Random::UniformDistributionNumber(0., 360.);
            //                 auto pc2_n = pc2 + cv::Point2d(radius*cos(angle), radius*sin(angle));
            //                 auto roi2_n = utils::getLocalPatchPos(pc2_n, cam_[1]->n_width(), cam_[1]->n_height(), window_size);
            //                 if(roi2_n.area()!=25) { cout<<"roi2_n "<<roi2_n<<endl; std::abort(); }
            //                 cv::Mat pn = cam_[1]->Irec()(roi2_n);

            //                 for(int i=0; i<5; i++){
            //                     for(int j=0; j<5; j++){
            //                         if(n==0)      pn0_v.at(p*25+j*5+i) = static_cast<double>( unsigned(pn.at<uchar>(i,j)) );
            //                         else if(n==1) pn1_v.at(p*25+j*5+i) = static_cast<double>( unsigned(pn.at<uchar>(i,j)) );
            //                         else if(n==2) pn2_v.at(p*25+j*5+i) = static_cast<double>( unsigned(pn.at<uchar>(i,j)) );
            //                     }
            //                 }
            //             }
                        
            //             p++;
            //         }
            //     }

            //     auto train_str = it_seq==it_tot_ ? "test" : "train";
            //     auto pc_file=str(boost::format("%1%/%2%%3$04d.npz") % train_dir % train_str % it_seq );
            //     auto Nx = static_cast<uint>(n_part_per_snapshot_);
            //     cnpy::npz_save(pc_file, "p_a", &pa_v[0], {Nx,25}, "w");
            //     cnpy::npz_save(pc_file, "p_p", &pp_v[0], {Nx,25}, "a");
            //     cnpy::npz_save(pc_file, "p_n0", &pn0_v[0], {Nx,25}, "a");
            //     cnpy::npz_save(pc_file, "p_n1", &pn1_v[0], {Nx,25}, "a");
            //     cnpy::npz_save(pc_file, "p_n2", &pn2_v[0], {Nx,25}, "a");
            // }
            // cout<<"Done !"<<endl;
            // std::abort();
            ///////////////////////////////////////////////////////////////////////
            cout<<"###########RE-GENERATE 3D GRIDS IN OBJECT SPACE################"<<endl;
            calib_->generate3DGridsInObjectSpace();

            std::string preproc_dir;
            for(auto& ic : cam_){
                cout<<"###########RE-GENERATE UNDISDORT 2D GRIDS IN IMAGE SPACE ON CAMERA "<<ic->i_cam()<<"#####"<<endl;
                calib_->generate2DGridsInImageSpaceFromCalibratedCamera(*ic);

                auto mvs_dir=str(boost::format("%1%/cam%2%_mvs") % imgDir_ % ic->i_cam());
                if(!boost::filesystem::exists(mvs_dir))
                    boost::filesystem::create_directories(mvs_dir);
            }

            auto xcoord_imgf_1=changexcoordDoubleToFloat(calib_->get_xcoord_img().at(cam_seq_[0]));
            auto xcoord_imgf_2=changexcoordDoubleToFloat(calib_->get_xcoord_img().at(cam_seq_[1]));
            auto xcoord_imgf_3=changexcoordDoubleToFloat(calib_->get_xcoord_img().at(cam_seq_[2]));
            auto xcoord_imgf_4=changexcoordDoubleToFloat(calib_->get_xcoord_img().at(cam_seq_[3]));

            dslpt_->getHorizontalCameraPairHomographyMatrix(xcoord_imgf_1, xcoord_imgf_2, nViews_, cam_);

            int ic1 = cam_seq_[0];
            int ic2 = cam_seq_[1];
            int ic3 = cam_seq_[2];
            int ic4 = cam_seq_[3];

            cout<<"Read Reference Particle Data"<<endl;
            dataset_->readBackgroundData(imgDir_, it_bkg_, false);

            int it_seq=it_bkg_.front();
            cout<<"#############Initialize A Simple LPT APP##############################################"<<endl;
            for(auto& ic : cam_)  ic->readRecord(it_seq, imgDir_);
            cout<<"############Start the stereo matching process#################"<<endl;
            cout<<"############At it_seq "<<it_seq<<" ########################################"<<endl;
            cv::Mat img1_warp, img2_warp;
            auto disparity = dslpt_->computeDisparity(cam_, img1_warp, img2_warp, 
                                                        true, true, imgDir_, it_seq);

            dslpt_->getImagePeaks(ic1, ic2, cam_, img1_warp, img2_warp);

            std::vector<cv::Point2d>  matched_pt1_warped, matched_pt2_warped;
   
            double window_size = 1.5;
            int    num_mismatched = 0;
            for(const auto& pc1 : dslpt_->Points_xy()[ic1]){

                auto roi1 = utils::getLocalPatchPos(pc1, cam_[ic1]->n_width(), cam_[ic1]->n_height(), window_size);

                auto disparity_roi = disparity(roi1);

                if(disparity_roi.type()==CV_8UC1){
                    cout<<"use CV_8UC3 disparity map instead!"<<endl;
                    std::abort();
                }
                else if(disparity_roi.type()==CV_8UC3){
                    cv::Mat disparity_roi_chn[3];
                    cv::split(disparity_roi, disparity_roi_chn);

                    for(int c=0; c<3; c++){
                        auto center_two = cv::Rect(2,2,2,2);
                        double min_disp, max_disp;
                        cv::minMaxLoc(disparity_roi_chn[c](center_two), &min_disp, &max_disp);

                        cv::Point2d pc2;
                        cv::Rect    roi2;
                        if(dslpt_->applyTracker(roi1, pc1, img1_warp, img2_warp, pc2, roi2, cv::Point2f(-max_disp,0)))
                        {
                            if(std::abs(pc1.y - pc2.y)<1.){
                                matched_pt1_warped.push_back(pc1);
                                matched_pt2_warped.push_back(pc2);
                            }
                            else{
                                cout<<"pc1      "<<pc1<<endl<<"roi1     "<<roi1<<endl;
                                cout<<"img1_warp(roi1)"<<endl<<img1_warp(roi1)<<endl;
                                cout<<"pc2      "<<pc2<<endl<<"roi2     "<<roi2<<endl;
                                cout<<"img2_warp(roi2)"<<endl<<img2_warp(roi2)<<endl; 
                                num_mismatched++;
                            }
                        }
                    }
                }
            }
            cout<<"number of mismatched (not meet stereo constraint) "<<num_mismatched<<endl;
            cout<<"###########Found "<<matched_pt1_warped.size()<<" stereo pairs!#######################"<<endl;
            
            cout<<"###########Search Correspondant On 3rd And 4th Cameras########"<<endl;
            std::vector<Particle_ptr> particles_warped;
            dslpt_->searchCorrespondantOn3rdAnd4thCameras(it_seq, cam_, matched_pt1_warped, matched_pt2_warped,
                                                            particles_warped, it_seq==it_bkg_[0], true, dataset_->particles_bkg());

            cout<<"###########Reconstruct "<<particles_warped.size()<<" particles in total!#############"<<endl;
            //
            // for(it_seq++; it_seq<=it_bkg_[0]; it_seq++){
            //     cout<<"############Start the temporal matching process#################"<<endl;
            //     cout<<"############At it_seq "<<it_seq<<" ########################################"<<endl;
            //     for(auto& ic : cam_)  ic->readRecord(it_seq, imgDir_, false);

            //     cv::Mat img1_next_warp, img2_next_warp;
            //     auto disparity = dslpt_->computeDisparity(cam_, img1_next_warp, img2_next_warp,
            //                                                 false, true, imgDir_, it_seq);

            //     num_mismatched = 0;
            //     cv::Point2d pt1_tgt, pt2_tgt;
            //     cv::Rect    roi1_tgt, roi2_tgt;
            //     for(int p=0; p<matched_pt1_warped.size(); p++){

            //         auto pt1 = matched_pt1_warped[p];
            //         auto pt2 = matched_pt2_warped[p];

            //         auto roi1 = utils::getLocalPatchPos(pt1, cam_[ic1]->n_width(), cam_[ic1]->n_height(), window_size);
            //         auto roi2 = utils::getLocalPatchPos(pt2, cam_[ic2]->n_width(), cam_[ic2]->n_height(), window_size);

            //         if(dslpt_->applyTracker(roi1, pt1, img1_warp, img1_next_warp, pt1_tgt, roi1_tgt) &&
            //            dslpt_->applyTracker(roi2, pt2, img1_warp, img2_next_warp, pt2_tgt, roi2_tgt)){

            //             if(std::abs(pt1_tgt.y - pt2_tgt.y)<1.){
            //                 particles_warped[p]->push_backxcoordElement(pt1_tgt, ic1, it_seq);
            //                 particles_warped[p]->push_backxcoordElement(pt2_tgt, ic2, it_seq);
            //                 particles_warped[p]->setit_seq_fin(it_seq);
            //             }
            //             else{
            //                 cout<<"pt1      "<<pt1<<endl;
            //                 cv::Rect roi_l = cv::Rect(roi1.x-2, roi1.y-2, roi1.width+4, roi1.height+4);
            //                 cout<<"roi1     "<<roi_l<<endl;
            //                 cout<<"img1_warp(roi1)"<<endl<<img1_warp(roi_l)<<endl;
            //                 cout<<"pt1_tgt  "<<pt1_tgt<<endl;
            //                 roi_l = cv::Rect(roi1_tgt.x-2, roi1_tgt.y-2, roi1_tgt.width+4, roi1_tgt.height+4);
            //                 cout<<"roi1_tgt "<<roi_l<<endl;
            //                 cout<<"img1_next_warp(roi1_tgt)"<<endl<<img1_next_warp(roi_l)<<endl;
                            
            //                 cout<<"pt2      "<<pt2<<endl;
            //                 roi_l = cv::Rect(roi2.x-2, roi2.y-2, roi2.width+4, roi2.height+4);
            //                 cout<<"roi2     "<<roi_l<<endl;
            //                 cout<<"img2_warp(roi2)"<<endl<<img2_warp(roi_l)<<endl;                            
            //                 cout<<"pt2_tgt  "<<pt2_tgt<<endl;
            //                 roi_l = cv::Rect(roi2_tgt.x-2, roi2_tgt.y-2, roi2_tgt.width+4, roi2_tgt.height+4);
            //                 cout<<"roi2_tgt "<<roi_l<<endl;
            //                 cout<<"img2_next_warp(roi2_tgt)"<<endl<<img2_next_warp(roi_l)<<endl;

            //                 num_mismatched++;
            //             }
            //         }    
            //     }
            //     cout<<"number of mismatched (temporal matching does not meet stereo constraint) "<<num_mismatched<<endl;

            //     std::vector<cv::Point2d>  matched_pt1_n_warped, matched_pt2_n_warped;
            //     for(auto& pw: particles_warped){
            //         if(pw->length(ic1)==pw->length(false)) matched_pt1_n_warped.push_back((pw->getCurrentxcoord(ic1, it_seq)));
            //         if(pw->length(ic2)==pw->length(false)) matched_pt2_n_warped.push_back((pw->getCurrentxcoord(ic2, it_seq)));
            //     }

            //     dslpt_->searchCorrespondantOn3rdAnd4thCameras(it_seq, cam_, matched_pt1_n_warped, matched_pt2_n_warped,
            //                                                     particles_warped, it_seq==it_bkg_[0], true, dataset_->particles_bkg());
            // }
            //######################Function examples on disparity#############################
            // cv::rectangle( img1_next_warp, temporal_res, cv::Scalar(255), 2, 1 );
            // cv::namedWindow( "kcf_tracker", cv::WINDOW_AUTOSIZE );
            // cv::imshow("kcf_tracker",img1_next_warp);
            // cv::waitKey(0); 

            // Working on disparity
            // cv::Mat cat_img1_img2_warp;
            // cv::hconcat(img1_warp, img2_warp, cat_img1_img2_warp);
            // cv::namedWindow( "cat_img1_img2_warp", cv::WINDOW_AUTOSIZE );
            // cv::imshow( "cat_img1_img2_warp", cat_img1_img2_warp );
            // cv::waitKey(0); 

            // cv::Mat imgL, imgR;
            // cv::convertScaleAbs(img1_warp, imgL, 1./256.);
            // cv::convertScaleAbs(img2_warp, imgR, 1./256.);

            // // cout<<"imgL"<<endl<<imgL(cv::Rect(703,275,5,5))<<endl;
            // // cout<<"imgR"<<endl<<imgR(cv::Rect(675,275,5,5))<<endl;

            // cv::Mat disparity(imgL.size(), CV_32FC1);
            // //####################GPU########################################################
            // cv::cuda::GpuMat d_left, d_right;
            // d_left.upload(imgL);
            // d_right.upload(imgR);

            // int ndisp = 128;
            // int iters = 10;
            // int levels = 1;
            // int nr_plane = 4;
            // int BM_win_size = 3;

            // auto matcher = cv::cuda::createStereoBM(ndisp, BM_win_size);
            // // auto matcher = cv::cuda::createStereoBeliefPropagation();
            // // auto matcher = cv::cuda::createStereoConstantSpaceBP(ndisp, iters, levels, nr_plane);

            // // matcher->estimateRecommendedParams(imgL.size().width, imgL.size().height, ndisp, iters, levels, nr_plane);
            // // cout<<"ndisp "<<ndisp<<endl
            // //     <<"iters "<<iters<<endl
            // //     <<"levels "<<levels<<endl
            // //     <<"nr_plane "<<nr_plane<<endl;

            // cv::cuda::GpuMat d_disp(imgL.size(), CV_32FC1);
            // matcher->compute(d_left, d_right, d_disp);
            // // matcher->compute(d_right, d_left, d_disp);
            // d_disp.download(disparity);

            // // cv::Mat_<double> Q = (cv::Mat_<double>(4,4)<<   1, 0, 0, -cx, 
            // //                                                 0, 1, 0, -cy,
            // //                                                 0, 0, 0, f,
            // //                                                 0, 0, -1./Tx, (cx-cx_r)/Tx);
            // // cv::cuda::GpuMat xyzw_cuda(imgL.size(), CV_32FC4);
            // // cv::cuda::reprojectImageTo3D(d_disp, xyzw_cuda, Q);
            // // cv::Mat_<cv::Vec3f> XYZ(disparity32F.rows,disparity32F.cols);   // Output point cloud
            // // cv::Mat_<float> vec_tmp(4,1);
            // // for(int y=0; y<disparity32F.rows; ++y) {
            // //     for(int x=0; x<disparity32F.cols; ++x) {
            // //         vec_tmp(0)=x; vec_tmp(1)=y; vec_tmp(2)=disparity32F.at<float>(y,x); vec_tmp(3)=1;
            // //         vec_tmp = Q*vec_tmp;
            // //         vec_tmp /= vec_tmp(3);
            // //         cv::Vec3f &point = XYZ.at<cv::Vec3f>(y,x);
            // //         point[0] = vec_tmp(0);
            // //         point[1] = vec_tmp(1);
            // //         point[2] = vec_tmp(2);
            // //     }
            // // }
            // // cv::Mat xyzw(imgL.size(), CV_32FC4);
            // // xyzw_cuda.download(xyzw);
            // //####################GPU########################################################
            // //####################CPU########################################################
            // // auto matcher = cv::StereoBM::create(ndisp, BM_win_size);
            // // auto matcher = cv::StereoSGBM::create(0, ndisp, BM_win_size);

            // // matcher->setPreFilterCap(63);
            // // matcher->setBlockSize(BM_win_size);
            // // // matcher->setP1(24*BM_win_size*BM_win_size);
            // // // matcher->setP2(96*BM_win_size*BM_win_size);
            // // matcher->setMinDisparity(0);
            // // matcher->setNumDisparities(ndisp);
            // // matcher->setUniquenessRatio(5);
            // // // matcher->setSpeckleWindowSize(0);
            // // // matcher->setSpeckleRange(32);
            // // matcher->setDisp12MaxDiff(-1);
            // // matcher->setMode(cv::StereoSGBM::MODE_HH);
            // // matcher->setMode(cv::StereoSGBM::MODE_SGBM);
            // // matcher->setMode(cv::StereoSGBM::MODE_HH4);
            // // matcher->setMode(cv::StereoSGBM::MODE_SGBM_3WAY);

            // // matcher->compute(imgL, imgR, disparity);
            // //####################CPU########################################################

            // // cout<<"disparity"<<endl<<disparity(cv::Rect(698,272,10,10))<<endl;

            // double min, max;
            // cv::minMaxLoc(disparity, &min, &max);

            // cv::Mat disparity_view;
            // disparity.convertTo(disparity_view, CV_8UC1, 255.0/(max-min), -min);

            // cv::namedWindow( "disparity_view", cv::WINDOW_AUTOSIZE );
            // cv::imshow( "disparity_view", disparity_view );
            // cv::waitKey(0);    

            // mvs_dir=str(boost::format("%1%/cam%2%_mvs") % imgDir_ % ic1);
            // fullname=str(boost::format("%1%/disparity_c%2%_it%3$03d.png") % mvs_dir % ic1 % it_seq );
            // cout<<"disparity_view "<<fullname<<endl;
            // cv::imwrite(fullname, disparity_view, utils::compression_params);  

            //#############################################################################
            // auto Xcpx=Random::UniformDistributionNumber(Xc_bottom_.x, Xc_top_.x);
            // auto Ycpx=Random::UniformDistributionNumber(Xc_bottom_.y, Xc_top_.y);
            // auto Zcpx=Random::UniformDistributionNumber(Xc_bottom_.z, Xc_top_.z);
            // auto Xc_vec = cv::Point3d(Xcpx, Ycpx, Zcpx);

            // auto Xc=Xc_vec;
            // cout<<"######################################################"<<endl;
            // cout<<"Xc true "<<Xc<<endl;

            // cv::Mat_<double>    homo_Xc = (cv::Mat_<double>(4,1)<< Xc.x, Xc.y, Xc.z, 1);

            // auto xd1=cam_[ic1]->mappingFunction(Xc);
            // auto xd2=cam_[ic2]->mappingFunction(Xc);

            // cout<<"xd1 "<<xd1<<endl;
            // cout<<"xd2 "<<xd2<<endl;

            // auto homo_xd1 = P1*homo_Xc;
            // auto homo_xd2 = P2*homo_Xc;
            
            // cout<<"homo_xd1 "<<homo_xd1<<endl;
            // cout<<"homo_xd2 "<<homo_xd2<<endl;
            
            // cv::Mat P1f, P2f;
            // P1.convertTo(P1f, CV_32F);
            // P2.convertTo(P2f, CV_32F);
            // cout<<"P1f.type() "<<P1f.type()<<endl;

            // cv::Mat_<float> out;
            // cv::triangulatePoints(P1f, P2f,
            //                         std::vector<cv::Point2f>{ cv::Point2f( float(xd1.x), float(xd1.y)) }, 
            //                         std::vector<cv::Point2f>{ cv::Point2f( float(xd2.x), float(xd2.y)) },
            //                         out);

            // cout<<"out "<<out<<endl;
            // cv::Point3d Xc_cv = cv::Point3d( out(0,0)/out(3,0), 
            //                                 out(1,0)/out(3,0), 
            //                                 out(2,0)/out(3,0) );
            // cout<<"Xc_cv   "<<Xc_cv<<endl;
            //#############################################################################
            //undistort & distort
            // std::vector<cv::Point2f>    xcoord_imgf_ic1_u, xcoord_imgf_ic2_u;
            // cout<<"xcoord_imgf_ic1[0].size() "<<xcoord_imgf_ic1[0].size()<<endl;
            // cv::undistortPoints(xcoord_imgf_ic1[0], xcoord_imgf_ic1_u, cam_[ic1]->cameraMatrix(), cam_[ic1]->distCoeffs(), cv::noArray(), cam_[ic1]->cameraMatrix());
            // cv::undistortPoints(xcoord_imgf_ic2[0], xcoord_imgf_ic2_u, cam_[ic2]->cameraMatrix(), cam_[ic2]->distCoeffs(), cv::noArray(), cam_[ic2]->cameraMatrix()); 

            // std::vector<cv::Point3f> p3_in;
            // std::vector<cv::Point2f> p2_out;
            // for(int i=0; i<xcoord_imgf_ic1_u.size(); i++){
            //      p3_in.push_back( cv::Point3f(  (xcoord_imgf_ic1_u[i].x - cam_[ic1]->cameraMatrix().at<double>(0,2))/cam_[ic1]->cameraMatrix().at<double>(0,0), 
            //                                     (xcoord_imgf_ic1_u[i].y - cam_[ic1]->cameraMatrix().at<double>(1,2))/cam_[ic1]->cameraMatrix().at<double>(1,1), 
            //                                     1 ) );
            // }

            // cv::Mat rvec( cv::Mat::zeros(cv::Size(3,1), CV_32FC1) );
            // cv::Mat tvec( cv::Mat::zeros(cv::Size(3,1), CV_32FC1) );        

            // cv::projectPoints(p3_in, rvec, tvec, cam_[ic1]->cameraMatrix(), cam_[ic1]->distCoeffs(), p2_out);
            
            // cout<<"xcoord_imgf_ic1[0][0] "<<xcoord_imgf_ic1[0][0]<<endl;
            // cout<<"xcoord_imgf_ic1_u[0]   "<<xcoord_imgf_ic1_u[0]<<endl;
            // cout<<"p3_in[0]  "<<p3_in[0]<<endl;
            // cout<<"p2_out[0] "<<p2_out[0]<<endl;

            // for(int i=0; i<xcoord_imgf_ic1_u.size(); i++){
            //     cout<<"err "<<cv::norm(xcoord_imgf_ic1[0][i] - p2_out[i])<<endl;
            // }
        }

};

int main(){
    std::unique_ptr<Dev_App> Dev_app=std::make_unique<Dev_App>();

    Dev_app->initialize();

    auto t_deb=Clock::now();
    Dev_app->run();
    auto t_fin=Clock::now();

    cout<<"CPU time of Dev: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}