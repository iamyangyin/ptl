#ifndef APP
#define APP

#include "../src/KLPT.hpp"
#include "../src/LAPIV.hpp"
#include "../src/DSLPT.hpp"
#include "../src/SYNTHETIC.hpp"
#include "../src/LAVISION.hpp"
#include "../src/LPTCHAL.hpp"
#include "../src/Evaluation.hpp"
#include "../src/Track.hpp"

class App
{
	protected:
        std::unordered_map<std::string, std::string> para_map_;
		std::string 								 rootDir_;

		int 							n_cam_;
		int 							opencv_mat_type_;
		int 							n_part_per_snapshot_;
		std::string 					case_name_;
		int 							it_tot_case_;
		int 							it_deb_;
		int 							it_tot_;
		int 							it_bkg_fin_;
		bool 							save_result_;
		OptMethod::ControlVector_Type 	control_vector_type_;
		DynModel::DynModel_Type 	  	dynmodel_type_;
		double 						  	pixel_lost_;
		std::string 				  	run_index_;
		Dataset::Source_Type 	    	data_source_;
		bool 						  	intensity_control_flag_;
		double      					noise_ratio_;

		std::string 		dataDir_;
		std::string 		data_path_;
		std::string 		velo_data_name_;
		std::string     	STB_data_path_;
		std::string			property_path_;

		cv::Point3d 		Xc_top_;
		cv::Point3d 		Xc_bottom_;

		bool 				read_only_bkgdata_flag_;
		std::string 		original_case_name_;
		
		double 				E_sd_;
		double 				E_max_;
		double 				E_min_;
		double 				theta_=6.;

        cv::Point3i 					 		 nXc_data_;
		Transport::TimeScheme_Type	     		 time_scheme_;
        double 									 dt_;
        double 									 dtObs_=0.0;
        double 									 tDeb_;
        double 									 tTotal_;
        bool 									 stationary_flag_;
		int 					   				 starting_index_;
		double 					   				 dtDyn_;
		Transport::VelocityInterpolation_Type	 velocity_to_scatter_interp_type_;

		Calibration::Source_Type  calibration_source_; 
		Camera::Model_Type 		  model_type_;
        cv::Point2i 			  nXc_rig_;
        int 					  nViews_;
		int 					  z_order_;
		bool 					  right_handed_flag_;

		int 					  n_width_;
		int 					  n_height_;
		int 					  n_width_mask_;
		int 					  n_height_mask_;

        std::vector<double> 	  beta_;
        std::vector<double> 	  alpha_;
        double 					  f_;
        double 					  k_;
        double 					  Z0_;
        cv::Mat 				  distCoeffs_;
		
        PTV::SubPixelMethod_Type        subPixel_type_;
        double                          initial_min_intensity_threshold_;
        double                          filter_threshold_;
        double                          dilate_threshold_;
        double                          dilate_surrounding_checker_threshold_;
        double                          dilate_surrounding_difference_threshold_;

        double                          search_threshold_;
        double                          Wieneke_threshold_;        
        bool                            stereo_verbal_flag_;

        int                             IPR_retriangulation_iter_;
        int                             IPR_reduced_retriangulation_iter_;
        bool                            IPR_flag_;
        int                             IPR_max_iter_;
        OptMethod::Method_Type          IPR_opt_method_type_;

		bool 				            initial_shake_flag_;
        int  				            initial_shake_max_iter_;
        double                          initial_shake_pixel_;
        double                          initial_shake_relaxfactor_;
        int  				            fine_shake_max_iter_;
        double                          fine_shake_pixel_;
        double                          fine_shake_relaxfactor_;

		int         		            n_ens_;
        double      		            regul_const_;
        int         		            outer_max_iter_;
        bool        		            local_analysis_flag_;
        bool        		            resample_flag_;
        cv::Point3d 		            sigmaXcRatio_;
        double      		            sigmaE_;
        bool        		            joint_intensity_flag_;
        bool        		            warm_start_flag_;
        int         		            warm_start_max_iter_;
        double      		            warm_start_pixel_;
        double      		            warm_start_relaxfactor_;

		std::string 		method_str_;
		std::string 		init_track_str_;
		std::string 		obsDir_;
		std::string 		imgDir_;
        std::string 		resDir_;

        cv::Point3d			dXc_px_;

		std::vector<int> 	it_bkg_;
		std::vector<int> 	it_fct_;

		std::unique_ptr<Dataset>     dataset_;

		std::vector<Camera_ptr> 	 cam_;

        std::unique_ptr<DynModel>    dynmodel_;

        std::optional<std::vector<Particle_ptr>> particles_syn_=std::nullopt;

        LPTCHAL::Chal_case_Type      case_type_=LPTCHAL::Chal_case_Type::TR;

	public:
		App(const std::string& para_name="para_config");

		virtual ~App() = 0;

		virtual void printPara(void);

		virtual void initialize(void);

		virtual void run(void){}

		virtual void saveResults(int){}

		virtual void clearForNextCycle(void){}

	protected:
		void loadENSParams(void);
		void loadSTBParams(void);
		void loadIPRParams(void);
		void loadPTVParams(void);
		void loadCalibrationParams(void);
		void loadTransportParams(void);

        bool stob(std::string str){
            std::istringstream is(str);
            bool b;
            is>>std::boolalpha>>b;
            return b;
        }
};

App::~App(){}

App::App(const std::string& para_name){
    rootDir_=std::getenv("STBENSPTV_PATH");

    std::string paraPath=boost::filesystem::current_path().string();
    paraPath=paraPath+"/test/"+para_name;
    cout<<"paraPath "<<paraPath<<endl;

	std::ifstream paraFile(paraPath);
    std::string   line,name,value;

	if(paraFile.is_open())
	{
		while(std::getline(paraFile,line)){
            std::string::size_type nc = line.find("//");
            if(nc != std::string::npos)
                line.erase(nc);

            std::istringstream is(line);
            if(is >> name >> value){
            	para_map_.insert({name, value});
            }
		}
	}	
	else{
        std::cerr<<"Error opening para_config file"<<endl;
        std::abort();
    }

    paraFile.close();

    n_cam_=std::stoi(para_map_["n_cam"]);
    opencv_mat_type_=std::stoi(para_map_["opencv_mat_type"]);
    n_part_per_snapshot_=std::stoi(para_map_["n_part"]);
    case_name_=para_map_["case_name"];
    it_tot_case_=std::stoi(para_map_["it_tot_case"]);
    it_deb_=std::stoi(para_map_["it_deb"]);
    it_tot_=std::stoi(para_map_["it_tot"]);
    it_bkg_fin_=std::stoi(para_map_["it_bkg_fin"]);
    save_result_=stob(para_map_["save_result"]);
    control_vector_type_=static_cast<OptMethod::ControlVector_Type>(std::stoi(para_map_["control_vector_type"]));
    dynmodel_type_=static_cast<DynModel::DynModel_Type>(std::stoi(para_map_["dynmodel_type"]));
    pixel_lost_=std::stod(para_map_["pixel_lost"]);
    run_index_=para_map_["run_index"];
    data_source_=static_cast<Dataset::Source_Type>(std::stoi(para_map_["data_source"]));
    noise_ratio_=std::stod(para_map_["noise_ratio"]);

    if(data_source_==Dataset::Source_Type::FromSynthetic){
        dataDir_=std::getenv("PTVDATA_PATH");
        data_path_=para_map_["data_path"];

        velo_data_name_=para_map_["velo_data_name"];
    }
    else if(data_source_==Dataset::Source_Type::FromLaVision){
        dataDir_=std::getenv("LAVISIONDATA_PATH");
        data_path_=para_map_["data_path"];

        STB_data_path_=para_map_["STB_data_path"];
        property_path_=para_map_["property_path"];
    }
    else if(data_source_==Dataset::Source_Type::FromLPTChallenge){
        dataDir_=std::getenv("PTVDATA_PATH");
        data_path_=para_map_["data_path"];

        property_path_=para_map_["property_path"];
    }

    Xc_top_=cv::Point3d(std::stod(para_map_["Xc_topX"]),std::stod(para_map_["Xc_topY"]),std::stod(para_map_["Xc_topZ"]));
    Xc_bottom_=cv::Point3d(std::stod(para_map_["Xc_bottomX"]),std::stod(para_map_["Xc_bottomY"]),std::stod(para_map_["Xc_bottomZ"]));

    dtObs_=std::stod(para_map_["dtObs"]);

    if(case_name_.substr(0, 8).compare("lavision")==0){
        read_only_bkgdata_flag_=stob(para_map_["read_only_bkgdata_flag"]);
        if(read_only_bkgdata_flag_)
            original_case_name_=para_map_["original_case_name"];
    }

    calibration_source_=static_cast<Calibration::Source_Type>(std::stoi(para_map_["calibration_source"]));
    model_type_=static_cast<Camera::Model_Type>(std::stoi(para_map_["model_type"]));

    if(calibration_source_==Calibration::Source_Type::FromVirtualCamera){
        nXc_rig_=cv::Point2i(std::stoi(para_map_["nXc_x_rig"]), std::stoi(para_map_["nXc_y_rig"]));
        nViews_=std::stoi(para_map_["nViews"]);
        if(case_name_!="lavision_jet" && case_name_!="lavision_re3900exp"){
            E_sd_=std::stod(para_map_["E_sd"]);
            E_max_=std::stod(para_map_["E_max"]);
            E_min_=std::stod(para_map_["E_min"]);
            theta_=std::stod(para_map_["theta"]);
            Particle::E_maximum_=E_max_+3.*E_sd_;
            Particle::E_minimum_=0.;
            Camera::opencv_mat_type_=opencv_mat_type_;
            if(opencv_mat_type_==CV_8UC1)       Camera::max_intensity_value_=255;
            else if(opencv_mat_type_==CV_16UC1) Camera::max_intensity_value_=65535;
        }
    }
    else if(calibration_source_==Calibration::Source_Type::FromLaVision){
        nXc_rig_=cv::Point2i(std::stoi(para_map_["nXc_x_rig"]), std::stoi(para_map_["nXc_y_rig"]));
        nViews_=std::stoi(para_map_["nViews"]);
        Camera::opencv_mat_type_=opencv_mat_type_;
        if(opencv_mat_type_==CV_16UC1){
            Particle::E_maximum_=65535;
            Particle::E_minimum_=0.;
        }
    }
    else if(calibration_source_==Calibration::Source_Type::FromLPTChallenge){
        theta_=std::stod(para_map_["theta"]);
        Camera::opencv_mat_type_=opencv_mat_type_;
        if(opencv_mat_type_==CV_16UC1){
            Particle::E_maximum_=65535;
            Particle::E_minimum_=0.;
        }
    }

    Camera::psf_type_=static_cast<Camera::psf_Type>(std::stoi(para_map_["psf_type"]));

    it_bkg_.resize(it_bkg_fin_-it_deb_+1);
    std::iota(it_bkg_.begin(), it_bkg_.end(), it_deb_);
    it_fct_.resize(it_tot_-it_bkg_fin_);
    std::iota(it_fct_.begin(), it_fct_.end(), it_bkg_fin_+1);

    if(data_source_==Dataset::Source_Type::FromSynthetic){
        obsDir_=str(boost::format("%1%%2%/image/%6%/part%3$06d_maxt%4$04d_fct0%5$04d_bkg0.0_dtobs%7$.2f") 
                                % dataDir_ % data_path_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % case_name_ % dtObs_);
    }

    if(case_name_=="lavision_jet" || case_name_=="lavision_re3900exp"){
        imgDir_=str(boost::format("%1%/data/%5%/part%2$06d_maxt%3$04d_fct0%4$04d") 
                            % rootDir_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % case_name_);
    }
    else if(case_name_.substr(0, 7).compare("lptchal")==0){
        case_type_=static_cast<LPTCHAL::Chal_case_Type>(std::stoi(para_map_["case_type"]));
        if(case_type_==LPTCHAL::Chal_case_Type::TP){
            it_deb_=1;
            it_tot_case_=2;
            it_tot_=2;
            it_bkg_fin_=1;

            it_bkg_.assign(1, 1);
            it_fct_.assign(1, 2);
        }

        imgDir_=str(boost::format("%1%/data/%5%/part%2$06d_maxt%3$04d_fct0%4$04d_dtobs%6$.4f") 
                            % rootDir_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % case_name_ % dtObs_);
    }
    else{
        double diameter = 6./sqrt(theta_);
        imgDir_=str(boost::format("%1%/data/%6%/part%2$06d_maxt%3$04d_fct0%4$04d_bkg%8$.3f_psf%5$d_dtobs%7$.2f_Emin%9$.1f_dmt%10$.1f") 
                            % rootDir_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % static_cast<int>(Camera::psf_type_) % case_name_ % dtObs_ % noise_ratio_ % E_min_ % diameter);
    }
}

void App::loadENSParams(void){
    n_ens_=std::stoi(para_map_["n_ens"]);
    regul_const_=std::stod(para_map_["regul_const"]);
    outer_max_iter_=std::stoi(para_map_["outer_max_iter"]);
    local_analysis_flag_=stob(para_map_["local_analysis_flag"]);
    resample_flag_=stob(para_map_["resample_flag"]);
    sigmaXcRatio_=cv::Point3d(std::stod(para_map_["sigmaXcRatioX"]),std::stod(para_map_["sigmaXcRatioY"]),std::stod(para_map_["sigmaXcRatioZ"]));
    sigmaE_=std::stod(para_map_["sigmaE"]);
    joint_intensity_flag_=stob(para_map_["joint_intensity_flag"]);
    warm_start_flag_=stob(para_map_["warm_start_flag"]);
    warm_start_max_iter_=std::stoi(para_map_["warm_start_max_iter"]);
    warm_start_pixel_=std::stod(para_map_["warm_start_pixel"]);
    warm_start_relaxfactor_=std::stod(para_map_["warm_start_relaxfactor"]);
}

void App::loadSTBParams(void){
    initial_shake_flag_=stob(para_map_["initial_shake_flag"]);
    initial_shake_max_iter_=std::stoi(para_map_["initial_shake_max_iter"]);
    fine_shake_max_iter_=std::stoi(para_map_["fine_shake_max_iter"]);
    initial_shake_pixel_=std::stod(para_map_["initial_shake_pixel"]);
    initial_shake_relaxfactor_=std::stod(para_map_["initial_shake_relaxfactor"]);
    fine_shake_pixel_=std::stod(para_map_["fine_shake_pixel"]);
    fine_shake_relaxfactor_=std::stod(para_map_["fine_shake_relaxfactor"]);
}

void App::loadIPRParams(void){
    IPR_retriangulation_iter_=std::stoi(para_map_["IPR_retriangulation_iter"]);
    IPR_reduced_retriangulation_iter_=std::stoi(para_map_["IPR_reduced_retriangulation_iter"]);
    IPR_flag_=stob(para_map_["IPR_flag"]);
    IPR_max_iter_=std::stoi(para_map_["IPR_max_iter"]);
    IPR_opt_method_type_=static_cast<OptMethod::Method_Type>(std::stoi(para_map_["IPR_opt_method"]));
}

void App::loadPTVParams(void){
    subPixel_type_=static_cast<PTV::SubPixelMethod_Type>(std::stoi(para_map_["subPixel_type"]));
    initial_min_intensity_threshold_=std::stod(para_map_["initial_min_intensity_threshold"]);
    filter_threshold_=std::stod(para_map_["filter_threshold"]);
    dilate_threshold_=std::stod(para_map_["dilate_threshold"]);
    dilate_surrounding_checker_threshold_=std::stod(para_map_["dilate_surrounding_checker_threshold"]);
    dilate_surrounding_difference_threshold_=std::stod(para_map_["dilate_surrounding_difference_threshold"]);
    
    search_threshold_=std::stod(para_map_["search_threshold"]);
    Wieneke_threshold_=std::stod(para_map_["Wieneke_threshold"]);
    stereo_verbal_flag_=stob(para_map_["stereo_verbal_flag"]);
}

void App::loadCalibrationParams(void){
    z_order_=std::stoi(para_map_["z_order"]);
    n_width_=std::stoi(para_map_["n_width"]);
    n_height_=std::stoi(para_map_["n_height"]);

    f_=std::stod(para_map_["f"]);
    k_=std::stod(para_map_["k"]);

    if(calibration_source_==Calibration::Source_Type::FromVirtualCamera && case_name_!="lavision_bkgonly" && case_name_!="lavision_jet"){
        std::string substr;

        std::stringstream beta_line(para_map_["beta"]);
        while(beta_line.good()){
            getline(beta_line, substr, ',');
            beta_.push_back(std::stod(substr));
        }
        std::stringstream alpha_line(para_map_["alpha"]);
        while(alpha_line.good()){
            getline(alpha_line, substr, ',');
            alpha_.push_back(std::stod(substr));
        }

        Z0_=std::stod(para_map_["Z0"]);

        std::vector<double>    distCoeffs;
        std::stringstream dist_line(para_map_["distCoeffs"]);
        while(dist_line.good()){
            getline(dist_line, substr, ',');
            distCoeffs.push_back(std::stod(substr));
        }
        distCoeffs_ = (cv::Mat(distCoeffs, true));
    }
}

void App::loadTransportParams(void){
    if( (case_name_=="les3900" || case_name_=="dns300" || case_name_=="les3900rect") && (calibration_source_==Calibration::Source_Type::FromVirtualCamera) ){

        nXc_data_=cv::Point3i(std::stoi(para_map_["nXc_x_data"]),std::stoi(para_map_["nXc_y_data"]),std::stoi(para_map_["nXc_z_data"]));

        tDeb_=std::stod(para_map_["tDeb"]);
        stationary_flag_=stob(para_map_["stationary_flag"]);
        dtDyn_=std::stod(para_map_["dtDyn"]);
        starting_index_=std::stod(para_map_["starting_index"]);
    }
    time_scheme_=static_cast<Transport::TimeScheme_Type>(std::stoi(para_map_["time_scheme"]));
    dt_=std::stod(para_map_["dt"]);
    velocity_to_scatter_interp_type_=static_cast<Transport::VelocityInterpolation_Type>(std::stoi(para_map_["velocity_to_scatter_interp_type"]));
}

void App::printPara(void){
    cout<<"This is STB-SPH-ENS run with "<<method_str_<<" method."<<endl;
    std::string source_type = (data_source_==Dataset::Source_Type::FromSynthetic ? 
                                "From Synthetic" : 
                                (data_source_==Dataset::Source_Type::FromLaVision ? 
                                    "From LaVision" : "From LPTChallenge"));
    cout<<str(boost::format("Test case with data %1%") % source_type )<<endl;
    cout<<str(boost::format("Test case with %1$5d particles, %2$1d cameras.") % n_part_per_snapshot_ % n_cam_)<<endl;
    std::string camera_model_type = (model_type_==Camera::Model_Type::Pinhole ? "Pinhole" : 
                                                                                (model_type_==Camera::Model_Type::Polynomial ? 
                                                                                    "Polynomial" : "PinholeAndPolynomial"));
    cout<<str(boost::format("Test case with %1% as camera model.") % camera_model_type)<<endl;
    cout<<str(boost::format("Test case starts at %1$03d time level, ends at %2$03d time level.") % it_deb_ % it_tot_)<<endl;
    cout<<str(boost::format("Background ends  at %1$03d time level.") % it_bkg_fin_)<<endl;
    cout<<str(boost::format("Analysis starts  at %1$03d time level, ends at %2$03d time level.") % it_fct_.at(0) % it_tot_)<<endl;
    cout<<str(boost::format("Image Background error  is %1$03d pixels.") % noise_ratio_)<<endl;
    cout<<std::boolalpha;
    cout<<"Save result flag        is "<<save_result_<<endl;
}

void App::initialize(void){

    if( method_str_=="ENS" || method_str_=="STB" || method_str_=="LAPIV" ){
        if(case_name_=="lavision_jet" || case_name_=="lavision_re3900exp" || case_name_=="lavision_re300exp" || case_name_.substr(0, 7).compare("lptchal")==0){
            resDir_=str(boost::format("%1%/result/%5%/part%2$06d_maxt%3$04d_fct0%4$04d/%6%%7%") 
                                % rootDir_ % n_part_per_snapshot_ % it_tot_ % it_fct_.at(0) % case_name_ % method_str_ % run_index_);
        }
        else{
            double diameter = 6./sqrt(theta_);
            resDir_=str(boost::format("%1%/result/%7%/part%2$06d_maxt%3$04d_fct0%4$04d_bkg%10$.3f_psf%5$d_dtobs%8$.2f_Emin%12$.1f_dmt%13$.1f/%6%%11%%9%")
                                % rootDir_ % n_part_per_snapshot_ % it_tot_ % it_fct_.at(0) % static_cast<int>(Camera::psf_type_) % method_str_ % case_name_ % dtObs_ % run_index_ % noise_ratio_ % init_track_str_ % E_min_ % diameter);
        }

        if(!boost::filesystem::exists(resDir_))
            boost::filesystem::create_directories(resDir_);

        for(int i_cam=0; i_cam<n_cam_; i_cam++){
            auto resDir_icam=resDir_+"/cam"+std::to_string(i_cam);
            if(!boost::filesystem::exists(resDir_icam))
                boost::filesystem::create_directories(resDir_icam);
        }
    }

    for(int i_cam=0; i_cam<n_cam_; i_cam++)
        cam_.push_back( std::make_unique<Camera>( model_type_, n_cam_, i_cam ) );
    
    bool pert_otf_flag =   case_name_.compare("lavision_bkgonly")==0
                         ||case_name_.compare("lptchal4500")==0      
                         ||case_name_.compare("lavision_re3900exp")==0
                         ||case_name_.compare("lavision_jet")==0
                         // ||case_name_.compare("les3900")==0
                           ;

    for(auto& ic : cam_)
        ic->readParamsYaml(imgDir_, true, pert_otf_flag);

    cam_[0]->setPSFGrids(Xc_top_, Xc_bottom_);

    dXc_px_=cv::Point3d(0,0,0);
    for(auto& ic : cam_) dXc_px_+=ic->dXc_px()/static_cast<double>(n_cam_);
}

#endif