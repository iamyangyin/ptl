#include "App.hpp"

class Transport_App : public App
{
	private:
		std::unique_ptr<Transport>  transport_;

	public:
		Transport_App():App(){
			method_str_="Transport";
			this->loadTransportParams();
			this->loadCalibrationParams();
		}

		void initialize(void) final {
			transport_=std::make_unique<Transport>(stationary_flag_, velocity_to_scatter_interp_type_, Xc_top_, Xc_bottom_, nXc_data_, 
													time_scheme_, dt_, dtDyn_, dtObs_, tDeb_, it_tot_case_, E_sd_, E_max_, E_min_);
		}

		void run(void) final {
			cv::Point3d dXc_px;

			if(case_name_.find("les3900") !=  std::string::npos)
				dXc_px=cv::Point3d(0.1, 0.1, 0.2);
			else if(case_name_.find("dns300") !=  std::string::npos)
				dXc_px=cv::Point3d(0.015, 0.015, 0.02);

			auto dX = (Xc_top_.x-Xc_bottom_.x)/static_cast<double>(n_width_);
			// dXc_px = cv::Point3d(dX, dX, dX*2.);

			cout<<"dXc_px "<<dXc_px<<endl;
	        
	        cout<<"###########MAKE GRIDS#####################################"<<endl;
	        transport_->makeGrids();

	        cout<<"###########SET INITIAL PARTICLES COORDS RANDOMLY#########"<<endl;
			transport_->setRandomParticles(n_part_per_snapshot_, dXc_px);

            if(!boost::filesystem::exists(dataDir_+data_path_)){ std::abort(); }
            
	        cout<<"###########TRANSPORT ACCORDING TO GIVEN VELOCITY#########"<<endl;
			transport_->predictSyntheticParticlePositionFieldFromScratch(dataDir_+data_path_+"/"+velo_data_name_, starting_index_);
			transport_->saveReferenceDataNPZ(obsDir_);
		}
};

int main(){
	std::unique_ptr<Transport_App> Transport_app=std::make_unique<Transport_App>();

	Transport_app->initialize();

	auto t_deb=Clock::now();
	Transport_app->run();
	auto t_fin=Clock::now();
	cout<<"CPU time of transport particles given velocity fields: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;

	return 0;
}