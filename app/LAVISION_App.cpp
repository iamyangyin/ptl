#include "App.hpp"

class LAVISION_App : public App
{
    private:

        std::string     img_path_;
        std::string     img_name_="B";

        std::string     STB_data_name_=".dat";

        std::string     out_img_name_="img_file";

        std::string     OTF_path_;
        std::string     OTF_name_="OpticalTransferFunction.xml";

        std::string     mappingFunction_path_;
        std::string     mappingFunction_name_="Calibration.xml";

        std::string     calib_path_;
        std::string     calib_name_="MarkPositionTable.xml";

        std::unique_ptr<Calibration>    calib_;
        
        bool                            read_davismf_flag_=false;
        bool                            process_img_flag_=false;
        bool                            make_init_track_flag_=false;

    public:
        LAVISION_App():App(){
            method_str_="LAVISION";

            this->loadPTVParams();
            this->loadCalibrationParams();

            read_davismf_flag_=stob(para_map_["read_davismf_flag"]);
            process_img_flag_=stob(para_map_["process_img_flag"]);
            make_init_track_flag_=stob(para_map_["make_init_track_flag"]);

            if(read_only_bkgdata_flag_){
                auto case_dir=str(boost::format("%1$06d_dtobs%2$.2f_bkg%3$.3f/") % n_part_per_snapshot_ % dtObs_ % noise_ratio_);
                data_path_=data_path_+case_dir;
            }
            
            img_path_=dataDir_+data_path_;

            STB_data_name_=dataDir_+data_path_+STB_data_path_+"/ExportToTecplot/"+STB_data_path_+STB_data_name_;

            OTF_path_=dataDir_+property_path_+"OpticalTransferFunction/";

            mappingFunction_path_=dataDir_+property_path_+"Calibration/";

            calib_path_=dataDir_+property_path_+"Calibration_";
        }

        void initializeObject(void) final {

            for(int i_cam=0; i_cam<n_cam_; i_cam++)
                cam_.push_back( std::make_unique<Camera>( model_type_, n_cam_, i_cam, n_width_, n_height_, z_order_, f_, k_) );

            if(!read_only_bkgdata_flag_)
                calib_ = std::make_unique<Calibration>( Calibration::Source_Type::FromLaVision, n_cam_, Xc_top_, Xc_bottom_ );

            dataset_ = std::make_unique<LAVISION>();
        }

        void printPara(void) final {
            App::printPara();
            cout<<std::boolalpha;
        }

        void run(void) final {
            if(read_only_bkgdata_flag_){
                double diameter = 6./sqrt(theta_);
                std::string original_img_dir=str(boost::format("%1%/data/%6%/part%2$06d_maxt%3$04d_fct0%4$04d_bkg%8$.3f_psf%5$d_dtobs%7$.2f_Emin%9$.1f_dmt%10$.1f") 
                                                % rootDir_ % n_part_per_snapshot_ % it_tot_case_ % it_fct_.at(0) % static_cast<int>(Camera::psf_type_) % original_case_name_ % dtObs_ % noise_ratio_ % E_min_ % diameter);
                
                cout<<"Copy cameras' config files from "<<original_img_dir<<endl;
                for(int i=0; i<n_cam_; i++){
                    auto origial_cam_i_file=original_img_dir+"/camera"+std::to_string(i+1)+".yaml";
                    auto dst_cam_i_file=imgDir_+"/camera"+std::to_string(i+1)+".yaml";
                    boost::filesystem::copy_file(origial_cam_i_file, dst_cam_i_file, boost::filesystem::copy_option::overwrite_if_exists);
                    auto preproc_cam_i_file=original_img_dir+"/camera"+std::to_string(i+1)+"_preproc.yaml";
                    auto predst_cam_i_file=imgDir_+"/camera"+std::to_string(i+1)+"_preproc.yaml";
                    boost::filesystem::copy_file(preproc_cam_i_file,predst_cam_i_file,boost::filesystem::copy_option::overwrite_if_exists);
                }

                for(auto& ic : cam_) ic->readParamsYaml(imgDir_, false);
            }


            if(case_name_.compare("lavision_re3900exp")==0){
                // cout<<calib_path_<<endl;
                // cout<<"########### READ CALIBRATION IMAGR FILE "<<endl;
                // for(int i=0; i<cam_.size(); i++){
                //     auto rig_img_i_file=calib_path_+"History/Calibration_201102_140219_original/camera"+std::to_string(i+1)+"/"+img_name_;
                //     dataset_->setRigIMGData(rig_img_i_file, i, imgDir_);
                // }

                // cout<<"########### READ CALIBRATION CORRESPANDENCE FILE "<<endl;
                // calib_->clearXcoord_obj_cams();
                // calib_->clearxcoord_img();
                // calib_->setZViews(std::vector<double>{17, 20, 22, 25, 27, 30, 32, 35});    

                // std::vector<std::vector<std::vector<cv::Point3d>>> Xcoord_obj_cams_read;
                // std::vector<std::vector<std::vector<cv::Point2d>>> xcoord_img_read;
                // dataset_->readMarkPositionTable(calib_path_+"History/Calibration_201102_140219_original/MarkPositionTable.xml",
                //                                 calib_->nViews(), Xcoord_obj_cams_read, xcoord_img_read);

                // std::vector<std::vector<std::vector<cv::Point3d>>> Xcoord_obj_cams;
                // std::vector<std::vector<std::vector<cv::Point2d>>> xcoord_img;
                // for(int i=0; i<Xcoord_obj_cams_read.size(); i++){
                //     Xcoord_obj_cams.push_back(std::vector<std::vector<cv::Point3d>>{Xcoord_obj_cams_read[i][1], Xcoord_obj_cams_read[i][0],
                //                                                                     Xcoord_obj_cams_read[i][3], Xcoord_obj_cams_read[i][2],
                //                                                                     Xcoord_obj_cams_read[i][5], Xcoord_obj_cams_read[i][4],
                //                                                                     Xcoord_obj_cams_read[i][7], Xcoord_obj_cams_read[i][6]});
                //     xcoord_img.push_back(std::vector<std::vector<cv::Point2d>>{xcoord_img_read[i][1], xcoord_img_read[i][0],
                //                                                                 xcoord_img_read[i][3], xcoord_img_read[i][2],
                //                                                                 xcoord_img_read[i][5], xcoord_img_read[i][4],
                //                                                                 xcoord_img_read[i][7], xcoord_img_read[i][6]});
                // }

                // calib_->setXcoord_obj_cams(Xcoord_obj_cams);                                     
                // calib_->setxcoord_img(xcoord_img);           

                for(auto& ic : cam_){
                    // cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                    // cout<<"###########GET PINHOLE MODEL PARAMETERS#####################"<<endl;
                    // calib_->getPinholeModelParameters(ic.get(), 0);

                    // cout<<"###########COMPUTE PINHOLE MAPPING FUNCTION ERROR###########"<<endl;
                    // calib_->computeMappingFunctionError(*ic, true);

                    if(model_type_!=Camera::Model_Type::Pinhole){
                        cout<<"###########READ POLYNOMIAL COEFFICIENT#####################"<<endl;
                        calib_->getPolynomialModelParameters(mappingFunction_path_+mappingFunction_name_, ic.get(), dataset_.get());

                        cout<<"###########COMPUTE POLYNOMIAL MAPPING FUNCTION ERROR########"<<endl;
                        calib_->computeMappingFunctionError(*ic);
                    }
                }

                auto Xcp=cv::Point3d(0,0,0);
                for(auto& ic : cam_){
                    cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                    calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_top_, Xc_bottom_);

                    ic->checkSensitivityOfMappingFunction(Xcp, Xc_top_.x-Xc_bottom_.x);

                    cout<<"########### READ OPTICAL TRANSFER FUNCTION##################"<<endl;
                    calib_->readPSFParams(OTF_path_+OTF_name_, ic.get(), dataset_.get());
                }

                // cout<<"###########DO STEREO CALIBRATION###############################"<<endl;
                // for(int ic1=0; ic1<n_cam_; ic1++){
                //     for(int ic2=0; ic2<n_cam_; ic2++){
                //         cout<<"Between "<<ic1<<"th camera and "<<ic2<<"th camera"<<endl;
                //         calib_->getStereoCalibrationParameters( cam_[ic1].get(), cam_[ic2].get() );
                //     }
                // }

                cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                for(auto& ic : cam_) ic->saveParamsToYamlFile(imgDir_);
            }

            if(case_name_.compare("lavision_re300exp")==0){
                cout<<calib_path_<<endl;
                cout<<"########### READ CALIBRATION CORRESPANDENCE FILE "<<endl;
                calib_->clearXcoord_obj_cams();
                calib_->clearxcoord_img();
                calib_->setZViews(std::vector<double>{-50, -53, -25, -28, -10, -13, 0, -3, 10, 7, 25, 22, 50, 47});    
                dataset_->readMarkPositionTable(calib_path_+"191212_7plans+-50+-25+-10_pinhole/MarkPositionTable.xml",
                                                calib_->nViews(), 
                                                calib_->get_Xcoord_obj_cams(),
                                                calib_->get_xcoord_img());

                int view_zero = 6;

                for(auto& ic : cam_){
                    cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                    cout<<"###########GET PINHOLE MODEL PARAMETERS#####################"<<endl;
                    calib_->getPinholeModelParameters(ic.get(), view_zero);

                    cout<<"###########COMPUTE PINHOLE MAPPING FUNCTION ERROR###########"<<endl;
                    calib_->computeMappingFunctionError(*ic, true);
                }

                cout<<"###########DO STEREO CALIBRATION###############################"<<endl;
                for(int ic1=0; ic1<n_cam_; ic1++){
                    for(int ic2=0; ic2<n_cam_; ic2++){
                        cout<<"Between "<<ic1<<"th camera and "<<ic2<<"th camera"<<endl;
                        calib_->getStereoCalibrationParameters( cam_[ic1].get(), cam_[ic2].get() );
                    }
                }

                // cout<<"########### READ CALIBRATION CORRESPANDENCE FILE "<<endl;            
                // calib_->readMarkPositionTable(calib_path_+"191212_3plans+-10_polynomial/MarkPositionTable.xml", 
                //                                 std::vector<double>{-10, -13, 0, -3, 10, 7});

                auto Xcp=cv::Point3d(0,0,0);
                for(auto& ic : cam_){
                    cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                    // cout<<"###########CALIBRATE POLYNOMIAL COEFFICIENT#################"<<endl;
                    // calib_->getPolynomialModelParameters(ic.get());

                    // cout<<"###########COMPUTE POLYNOMIAL MAPPING FUNCTION ERROR########"<<endl;
                    // calib_->computeMappingFunctionError(*ic);

                    cout<<"###########READ POLYNOMIAL COEFFICIENT#####################"<<endl;
                    // calib_->getPolynomialModelParameters(calib_path_+"191212_3plans+-10_polynomial/"+mappingFunction_name_, ic.get(), dataset_.get());
                    calib_->getPolynomialModelParameters(mappingFunction_path_+mappingFunction_name_, ic.get(), dataset_.get());

                    cout<<"###########COMPUTE POLYNOMIAL MAPPING FUNCTION ERROR########"<<endl;
                    calib_->computeMappingFunctionError(*ic);

                    cout<<"###########COMPUTE PINHOLE MAPPING FUNCTION ERROR###########"<<endl;
                    calib_->computeMappingFunctionError(*ic, true);

                    cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                    calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_top_, Xc_bottom_);

                    ic->checkSensitivityOfMappingFunction(Xcp, Xc_top_.x-Xc_bottom_.x);

                    cout<<"########### READ OPTICAL TRANSFER FUNCTION##################"<<endl;
                    calib_->readPSFParams(OTF_path_+OTF_name_, ic.get(), dataset_.get());
                }

                cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                for(auto& ic : cam_) ic->saveParamsToYamlFile(imgDir_);
            }

            if(case_name_.compare("lavision_jet")==0 && read_davismf_flag_){
                auto Xcp=cv::Point3d(0,0,0);
                for(auto& ic : cam_){
                    cout<<"###########CALIBRATE CAMERA "<<ic->i_cam()<<"###############################"<<endl;
                    cout<<"###########READ POLYNOMIAL COEFFICIENT#####################"<<endl;
                    calib_->getPolynomialModelParameters(mappingFunction_path_+mappingFunction_name_, ic.get(), dataset_.get());

                    cout<<"###########COMPUTE POLYNOMIAL MAPPING FUNCTION ERROR########"<<endl;
                    calib_->computeMappingFunctionError(*ic);

                    cout<<"########### CHECK DOMAIN BOUNDARY ON IMAGE SPACE############"<<endl;
                    calib_->checkDomainBoundaryOnImageSpace(*ic, Xc_top_, Xc_bottom_);

                    ic->checkSensitivityOfMappingFunction(Xcp, Xc_top_.x-Xc_bottom_.x);

                    cout<<"########### READ OPTICAL TRANSFER FUNCTION##################"<<endl;
                    calib_->readPSFParams(OTF_path_+OTF_name_, ic.get(), dataset_.get());
                }

                cout<<"########### SAVE CAMERA PARAMETERS##########################"<<endl;
                for(auto& ic : cam_) ic->saveParamsToYamlFile(imgDir_);
            }

            if(process_img_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE IMAGE###############"<<endl;
                dataset_->setIMGData(img_path_+img_name_, it_deb_, it_tot_, cam_, imgDir_);
            }

            if(make_init_track_flag_){
                cout<<"########### PROCESSING DAVIS PARTICLE FIELD#################"<<endl;
                cout<<STB_data_name_<<endl;
                dataset_->readParticleData(STB_data_name_, it_deb_, it_tot_, Dataset::Data_Format_Type::TXT_ONEFILE, Xc_top_, Xc_bottom_);
            
                track::printTrackInformation(it_tot_, dataset_->particles_ref());

                dataset_->saveSyntheticData(imgDir_, it_tot_, it_bkg_);

                auto bkg_recon_dir=str(boost::format("%1%/bkg_recon") % imgDir_);
                if(!boost::filesystem::exists(bkg_recon_dir))
                    boost::filesystem::create_directories(bkg_recon_dir);

                for(int it=1; it<it_bkg_fin_+1; it++){
                    auto bkg_file=str(boost::format("%1%/bkg/bkg_it%2$03d.npz") % imgDir_ % it);
                    auto bkg_file_recon=str(boost::format("%1%/bkg_it%2$03d.npz") % bkg_recon_dir % it);
                    boost::filesystem::copy_file(bkg_file, bkg_file_recon, boost::filesystem::copy_option::overwrite_if_exists);
                }
            }           
        }
};

int main(){
    std::unique_ptr<LAVISION_App> LAVISION_app=std::make_unique<LAVISION_App>();

    LAVISION_app->printPara();

    LAVISION_app->initializeObject();

    auto t_deb=Clock::now();
    LAVISION_app->run();
    auto t_fin=Clock::now();

    cout<<"CPU time of preparing LaVision data&configuration files: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    return 0;
}
