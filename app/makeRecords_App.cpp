#include "App.hpp"

class makeRecords_App : public App
{
    private:
        std::unique_ptr<Calibration> calib_;

    public:
        makeRecords_App():App(){
            method_str_="MakeRecord";
            this->loadCalibrationParams();
        }

        virtual void initialize(void) final {
            for(int i_cam=0; i_cam<n_cam_; i_cam++)
                cam_.push_back( std::make_unique<Camera>( Camera::Model_Type::Pinhole, n_cam_, i_cam, n_width_, n_height_, z_order_, f_, k_, beta_[i_cam], alpha_[i_cam], Z0_, distCoeffs_, noise_ratio_ ) );
            
            right_handed_flag_=(Z0_<0);
            calib_ = std::make_unique<Calibration>( calibration_source_, n_cam_,
                                                    Xc_top_, Xc_bottom_, nXc_rig_, nViews_, right_handed_flag_ );

            dataset_ = std::make_unique<Dataset>();
        }

        virtual void run(void) final {
            cout<<"###########GENERATE 3D GRIDS IN OBJECT SPACE################"<<endl;
            calib_->generate3DGridsInObjectSpace();
            for(auto& ic : cam_){
                cout<<"###########GENERATE 2D GRIDS IN IMAGE SPACE ON CAMERA "<<ic->i_cam()<<"#####"<<endl;
                calib_->generate2DGridsInImageSpaceFromKnownCamera(ic.get());
                calib_->setPSFParams(ic.get(), theta_);
            }
            calib_->saveMarkPositionTable(obsDir_);

            cout<<"###########READ SYNTHETIC PARTICEL DATASET##################"<<endl;
            dataset_->readParticleData(obsDir_, it_deb_, it_tot_case_, Dataset::Data_Format_Type::NPZ);

            cout<<"################CONSTRUCT SYNTHETIC PARTICLE IMAGES###############"<<endl;
            for(int it_rec=it_deb_; it_rec<=it_tot_case_; it_rec++){
                cout<<"it_rec "<<it_rec<<endl;
                utils::checkIfParticlesAppearOnSnapshot(it_rec, dataset_->get_particles_ref());
#ifdef INTELTBB
                tbb::parallel_for( tbb::blocked_range<int>(0, dataset_->get_particles_ref().size()),
                            [&](const tbb::blocked_range<int>& r){ for(int i=r.begin(); i!=r.end(); ++i){
#else
                for(int i=0; i<dataset_->get_particles_ref().size(); ++i){
#endif
                    if(dataset_->get_particles_ref()[i]->is_ontrack())
                        dataset_->get_particles_ref()[i]->projectToImageSpace(it_rec, cam_, true);
                }
#ifdef INTELTBB
                });
#endif
                cout<<"################SAVE SYNTHETIC RECORD IMAGE#######################"<<endl;
                for(int i_cam=0; i_cam<n_cam_; i_cam++){
                    auto obsDir_icam=obsDir_+"/cam"+std::to_string(i_cam);
                    if(!boost::filesystem::exists(obsDir_icam))
                        boost::filesystem::create_directories(obsDir_icam);
                }

                for(auto& ic : cam_){
                    if(!ic->Iproj().empty()) ic->clearIproj();
                    ic->setIprojElement( ic->sumParticleImage(dataset_->particles_ref(), Camera::sumParticle_Type::All, it_rec) );
                    ic->saveImageProjection(it_rec, "cam", obsDir_, true);
                }
            }
        }
};

int main(){
    std::unique_ptr<makeRecords_App> makeRecords_app=std::make_unique<makeRecords_App>();

    makeRecords_app->printPara();

    makeRecords_app->initialize();

    auto t_deb=Clock::now();
    makeRecords_app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of construction synthetic images: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;
    
    return 0;
}