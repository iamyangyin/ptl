#include "App.hpp"

class KLPT_App: public App
{
    protected:
        Dataset::Init_Track_Type        init_track_type_;
        OptMethod::Method_Type          method_type_;

        std::unique_ptr<OptMethod>      optmethod_;
        std::unique_ptr<PTV>            ptv_;

    public:
        KLPT_App():App(){
            init_track_type_=static_cast<Dataset::Init_Track_Type>(std::stoi(para_map_["init_track_type"]));
            init_track_str_ = (init_track_type_==Dataset::Init_Track_Type::Hacker ? "hck" : "trg");
            intensity_control_flag_ = (init_track_type_==Dataset::Init_Track_Type::Triangulation);

            method_type_=static_cast<OptMethod::Method_Type>(std::stoi(para_map_["opt_method_type"]));
            if(method_type_==OptMethod::Method_Type::STB)        method_str_="STB";
            else if(method_type_==OptMethod::Method_Type::ENS)   method_str_="ENS";

            this->loadPTVParams();
            this->loadSTBParams();
            this->loadENSParams();
        }

        void printPara(void) final{
            App::printPara();
            cout<<std::boolalpha;
            std::string init_track_type = (init_track_type_==Dataset::Init_Track_Type::Hacker ? "Hacker" : "Triangulation");
            cout<<"Initial Track Type      is "<<init_track_type<<endl;
            if(method_type_==OptMethod::Method_Type::STB){
                cout<<"initial_shake_flag           is "<<initial_shake_flag_<<endl;
                cout<<str(boost::format("Initial Shake Maximum iteration is %1$02d.") % initial_shake_max_iter_)<<endl;
                cout<<str(boost::format("Initial Shake Radius  pixel     is %1$f")    % initial_shake_pixel_)<<endl;
                cout<<str(boost::format("Initial Shake Relax   factor    is %1$f")    % initial_shake_relaxfactor_)<<endl;
                cout<<str(boost::format("Fine    Shake Maximum iteration is %1$02d.") % fine_shake_max_iter_)<<endl;
                cout<<str(boost::format("Fine    Shake Radius  pixel     is %1$f")    % fine_shake_pixel_)<<endl;
                cout<<str(boost::format("Fine    Shake Relax   factor    is %1$f")    % fine_shake_relaxfactor_)<<endl;
            }
            else if(method_type_==OptMethod::Method_Type::ENS){
                cout<<str(boost::format("Ensemble number              is %1$3d.") % n_ens_)<<endl;
                cout<<str(boost::format("Regularization constant      is %1$3d.") % regul_const_)<<endl;
                cout<<str(boost::format("Outer Loop Maximum iteration is %1$02d.") % outer_max_iter_)<<endl;
                cout<<str(boost::format("Ensemble position initial error      is %1% px.") % sigmaXcRatio_)<<endl;
                cout<<"local_analysis_flag          is "<<local_analysis_flag_<<endl;
                cout<<"resample_flag                is "<<resample_flag_<<endl;
                if(intensity_control_flag_){
                    cout<<"joint_intensity_flag         is "<<joint_intensity_flag_<<endl;
                    cout<<str(boost::format("Ensemble intensity initial error     is %1% count.") % sigmaE_)<<endl;
                }
                cout<<"warm_start_flag              is "<<warm_start_flag_<<endl;
                cout<<str(boost::format("Warm start Maximum iteration is %1$02d.") % warm_start_max_iter_)<<endl;
                cout<<str(boost::format("Warm start Radius  pixel     is %1$f")    % warm_start_pixel_)<<endl;
                cout<<str(boost::format("Warm start Relax   factor    is %1$f")    % warm_start_relaxfactor_)<<endl;
            }
        }

        void initialize(void) final{
            App::initialize();

            if(data_source_==Dataset::Source_Type::FromSynthetic){
                dataset_ = std::make_unique<SYNTHETIC>();
                if(init_track_type_==Dataset::Init_Track_Type::Hacker) dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_);
                dataset_->readBackgroundData(imgDir_, it_bkg_, init_track_type_!=Dataset::Init_Track_Type::Hacker);
            }
            else if(data_source_==Dataset::Source_Type::FromLaVision){
                dataset_ = std::make_unique<LAVISION>();
                dataset_->readSyntheticData(imgDir_, it_deb_, it_tot_, std::optional<cv::Point3d>{std::in_place, Xc_top_}, std::optional<cv::Point3d>{std::in_place, Xc_bottom_});
                dataset_->readBackgroundData(imgDir_, it_bkg_, true, std::optional<cv::Point3d>{std::in_place, Xc_top_}, std::optional<cv::Point3d>{std::in_place, Xc_bottom_});
            }
            else if(data_source_==Dataset::Source_Type::FromLPTChallenge){
                dataset_ = std::make_unique<LPTCHAL>();
                dataset_->readBackgroundData(imgDir_, it_bkg_, true, std::optional<cv::Point3d>{std::in_place, Xc_top_}, std::optional<cv::Point3d>{std::in_place, Xc_bottom_});
            }

            if(init_track_type_==Dataset::Init_Track_Type::Hacker) particles_syn_.emplace( std::move( dataset_->get_particles_ref() ) );

            dynmodel_ = std::make_unique<DynModel>(dynmodel_type_);

            for(int it_seq=it_bkg_[0]; it_seq<=it_bkg_.back(); it_seq++){
                track::saveFrameDataTXT(it_seq, dataset_->get_particles_bkg(), resDir_, dtObs_, it_seq!=it_bkg_[0]);
                track::saveFrameDataVTK(it_seq, dataset_->get_particles_bkg(), resDir_);
            }

            for(auto& ps: dataset_->particles_bkg()){
                if(init_track_type_==Dataset::Init_Track_Type::Hacker)
                    ps->checkIfParticleAppearsOnSnapshot(it_fct_[0]);
                else
                    ps->checkIfParticleAppearsOnSnapshot(it_bkg_.back());
                
                if(!ps->is_ontrack()){
                    ps->deleteData(init_track_type_!=Dataset::Init_Track_Type::Hacker); 
                    ps->markAsTerminated(); 
                }
            }
            utils::showInfoOnDeletedParticles();

            //Fine tuning PTV parameters#################################################################################
            double min_intensity_ratio = (noise_ratio_<=0.01 ? 0.1 : (noise_ratio_<=0.03 ? 0.2 : 0.3) );
            double intensity_deletion_threshold_ratio = 0.1;
            double intensity_min_threshold_ratio = 0.01;
            auto cam_seq = std::vector<int>{0,1,2,3}; 
            int num_pixels_radius_with_predictor = 4;
            int num_pixels_radius_without_predictor = 18;
            int num_avg_dist = 1;

            if(case_name_.compare("les3900rect")==0 || case_name_.compare("lavision_bkgonly")==0){
                if(n_part_per_snapshot_<=10000){
                    initial_min_intensity_threshold_*=0.25;
                }
                else if(n_part_per_snapshot_==20000){
                    // initial_min_intensity_threshold_*=0.25;
                }
                else if(n_part_per_snapshot_==30000){
                    // min_intensity_ratio=0.2;
                    // search_threshold_*=0.5;
                    // Wieneke_threshold_*=0.5;
                }
                else if(n_part_per_snapshot_>=40000){
                    // min_intensity_ratio=0.2;
                    search_threshold_*=0.5;
                    Wieneke_threshold_*=0.5;
                }
                num_avg_dist = 3;
                num_pixels_radius_with_predictor = 2;
                num_pixels_radius_without_predictor = 10;
                intensity_min_threshold_ratio = 0.0001;
            }
            else if(case_name_.compare("lavision_re300exp")==0){
                min_intensity_ratio=0.1;
            }
            else if(case_name_.compare("lavision_jet")==0){
                initial_min_intensity_threshold_*=0.25;
                // search_threshold_*=1.5;
                // Wieneke_threshold_*=1.5;
                min_intensity_ratio = 0.015;
                intensity_deletion_threshold_ratio=0.01;
                intensity_min_threshold_ratio = 0.15;
                cam_seq=std::vector<int>{1,2,0,3};
                num_pixels_radius_with_predictor = 18;
                num_avg_dist = 15;
            }
            else if(case_name_.compare("lptchal4500")==0){
                cam_seq = std::vector<int>{0,3,1,2};
                if(n_part_per_snapshot_==10000){
                    initial_min_intensity_threshold_*=0.333;
                    min_intensity_ratio = 0.01;
                }
                else if(n_part_per_snapshot_==50000){
                    initial_min_intensity_threshold_*=0.333;
                    min_intensity_ratio = 0.01;
                }
                else if(n_part_per_snapshot_==100000){
                    initial_min_intensity_threshold_*=0.333;
                    min_intensity_ratio = 0.01;
                }
                else if(n_part_per_snapshot_==160000){
                    initial_min_intensity_threshold_*=0.333;
                    // search_threshold_*=0.5;
                    // Wieneke_threshold_*=0.5;
                    min_intensity_ratio = 0.01;
                }
                else if(n_part_per_snapshot_==240000){
                    // initial_min_intensity_threshold_*=0.333;
                    // search_threshold_*=0.5;
                    // Wieneke_threshold_*=0.5;
                    min_intensity_ratio = 0.02;
                    // intensity_deletion_threshold_ratio = 0.075;
                }
                else{
                    cout<<"KLPT does not work for PPP larger than 0.12"<<endl
                        <<"switch to LAPIV_tr"<<endl;
                    std::abort();
                }
            }          
            //Fine tuning PTV parameters#################################################################################

            ptv_=std::make_unique<PTV>( subPixel_type_, initial_min_intensity_threshold_, filter_threshold_,
                                        dilate_threshold_, dilate_surrounding_checker_threshold_, dilate_surrounding_difference_threshold_,
                                        search_threshold_, Wieneke_threshold_, stereo_verbal_flag_, min_intensity_ratio,
                                        Xc_top_, Xc_bottom_, dXc_px_, cam_seq,
                                        (case_name_=="lavision_jet"), !(case_name_=="lavision_jet"));

            auto eval_window = (IPR_opt_method_type_==OptMethod::Method_Type::STB ? 3 : 2);

            if(method_type_==OptMethod::Method_Type::STB)
                optmethod_ = std::make_unique<STB>(OptMethod::Method_Type::STB, control_vector_type_, intensity_control_flag_,
                                                    dataset_->particles_bkg().size(), n_part_per_snapshot_, Xc_top_, Xc_bottom_,
                                                    pixel_lost_, dtObs_,
                                                    dXc_px_, resDir_, eval_window,
                                                    initial_shake_flag_, initial_shake_max_iter_, initial_shake_pixel_, initial_shake_relaxfactor_,
                                                    fine_shake_max_iter_, fine_shake_pixel_, fine_shake_relaxfactor_);
            else if(method_type_==OptMethod::Method_Type::ENS)
                optmethod_ = std::make_unique<ENS>(OptMethod::Method_Type::ENS, control_vector_type_, intensity_control_flag_,
                                                    dataset_->particles_bkg().size(), n_part_per_snapshot_, Xc_top_, Xc_bottom_, 
                                                    pixel_lost_, dtObs_, 
                                                    dXc_px_, resDir_, eval_window,
                                                    warm_start_flag_, warm_start_max_iter_, warm_start_pixel_, warm_start_relaxfactor_,
                                                    n_ens_, regul_const_, outer_max_iter_, local_analysis_flag_, 
                                                    resample_flag_, sigmaXcRatio_, sigmaE_, joint_intensity_flag_);

            if(case_name_=="lavision_jet"){
                cout<<"Re-Calibrate particle intensities of the last snapshot read from Davis 10"<<endl;
                for(auto& ic: cam_) ic->readRecord(it_bkg_.back(), imgDir_);
                ptv_->readRecordsFromCamera(cam_);
                optmethod_->recalibrateParticlesIntensities(dataset_->get_particles_bkg(), it_bkg_.back(), cam_, ptv_.get());
            }

            optmethod_->setintensity_deletion_threshold_ratio(intensity_deletion_threshold_ratio);
            optmethod_->setintensity_min_threshold_ratio(intensity_min_threshold_ratio);

            optmethod_->setnum_pixels_radius_with_predictor(num_pixels_radius_with_predictor);
            optmethod_->setnum_pixels_radius_without_predictor(num_pixels_radius_without_predictor);

            optmethod_->setaddNewParticles_before_final_iteration_flag(false);    
            optmethod_->setptv_min_intensity_threshold_flag(true);    //set as true if dense particle (ppp>=0.05), starting with high initial_min_intensity_threshold_
            optmethod_->setptv_stereoMatching_reduced_flag(false);    //set as true if dense particle (ppp>=0.1)
            optmethod_->setptv_FrameField_duplicate_check_flag(false); //set as true if initial_min_intensity_threshold_ is low
             
            optmethod_->setnum_avg_dist(num_avg_dist);

            //fine tune outlier removal
            optmethod_->setRemoveOutlierParams(20, 10, 5, 5, 5, CV_PI/2, CV_PI/2);
        
            optmethod_->initialization(dataset_->get_particles_bkg(), it_fct_.front());
        }

        void run(void) final{
            for(auto pt_seq=it_fct_.begin(); pt_seq<it_fct_.end(); ++pt_seq){
                cout<<"############ AT CYCLE "<<*pt_seq<<"#######################"<<endl;

                // ////////////////////////////////////////////////////////////
                if(case_name_=="lavision_jet"){
                    if(pt_seq !=  it_fct_.begin()){
                        cout<<"Cheat on new tracks with length 2!!!"<<endl;
                        auto particles_new = track::getTracksWithLength2AtCurrentSnapshot(*pt_seq, dataset_->particles_ref());
                        for(auto& ic: cam_) ic->readRecord(*pt_seq-1, imgDir_);
                        ptv_->readRecordsFromCamera(cam_);
                        optmethod_->recalibrateParticlesIntensities(particles_new, *pt_seq-1, cam_, ptv_.get());
                        optmethod_->setparticles_new(particles_new);
                    }
                }
                // ////////////////////////////////////////////////////////////

                cout<<"################START PREDICTION###############"<<endl;
                optmethod_->predict(dynmodel_.get(), *pt_seq, it_fct_[0]);

                cout<<"################PREPARE CORRECTION#############"<<endl;
                for(auto& ic: cam_) ic->readRecord(*pt_seq, imgDir_);
                optmethod_->prepare(*pt_seq, cam_);

                cout<<"################RUN CORRECTION#################"<<endl;
                auto rm_outlier_flag = (case_name_ == "lavision_jet");
                auto no_ipr_res_flag = (case_name_ == "lavision_jet");
                KLPT::runOptimization(optmethod_.get(), *pt_seq, cam_, ptv_.get(), nullptr, rm_outlier_flag, no_ipr_res_flag);

                cout<<"################SAVE RESULTS###################"<<endl;
                this->saveResults(*pt_seq);

                this->clearForNextCycle();
                ptv_->resetmin_intensity_threshold();
            }

            cout<<"##### PRINT OPT TRACK INFORMATION #############"<<endl;
            track::printTrackInformation(it_tot_, optmethod_->particles_field());

            if(particles_syn_){
                cout<<"##### PRINT DAVIS 10 TRACK INFORMATION ########"<<endl;
                track::printTrackInformation(it_tot_, particles_syn_.value());
            }

            for(auto pt_seq=it_fct_.begin(); pt_seq<it_fct_.end(); ++pt_seq){
                cout<<"save particle fields to txt..."<<endl;
                track::saveFrameDataTXT(*pt_seq, optmethod_->particles_field(), resDir_, dtObs_, false);
                cout<<"save particle fields to vtk..."<<endl;
                track::saveFrameDataVTK(*pt_seq, optmethod_->particles_field(), resDir_);
            }
        }

        void saveResults(int it_seq) final {
            if(save_result_){
                auto overwrite_result_file_flag = (it_seq==it_fct_.front());

                optmethod_->saveGlobalResidual(it_seq, overwrite_result_file_flag);

                for(auto& ic : cam_) ic->saveImageResidual(it_seq, "Iresf", resDir_);
            }
        }

        void clearForNextCycle(void) final {
            optmethod_->clear();
        }
};

int main(){
    std::unique_ptr<KLPT_App> klpt_app=std::make_unique<KLPT_App>();

    klpt_app->printPara();

    klpt_app->initialize();

    auto t_deb=Clock::now();
    klpt_app->run();
    auto t_fin=Clock::now();
    cout<<"CPU time of KLPT: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t_fin-t_deb).count()<<" ms"<<endl;

    return 0;
}